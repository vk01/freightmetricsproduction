﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Threading;
using System.Xml;
using Exis.Domain;
using Exis.ReportsEngine;
using Exis.ServiceInterfaces;
using Exis.SessionRegistry;
using Gurock.SmartInspect;

namespace Exis.ReportsEngineConsole
{
    public class Program
    {
        private static ServiceHost objServiceHost;

        private static int Main(string[] args)
        {
            if (!Thread.GetDomain().IsDefaultAppDomain())
            {
                HandleArguments(args);
                if (objServiceHost != null)
                    objServiceHost.Open();
                else
                    return 1;
            }
            else
            {
                objServiceHost = CreateServiceHost("9000", "User Id=crm4;Password=crm4;Data Source=artemis;Pooling=true");
                if (objServiceHost != null)
                {
                    objServiceHost.Open();
                    Console.ReadLine();
                    objServiceHost.Close();
                }
            }

            return 0;
        }

        public static void Stop()
        {
            objServiceHost.Close();
        }

        public static void Abort()
        {
            objServiceHost.Abort();
        }
        public static void ChangeDebugLevel()
        {
            var level = (string)AppDomain.CurrentDomain.GetData("LogLevel");
            SiAuto.Main.LogMessage("Changing debug level from " + SiAuto.Si.Level + " to " + level);
            SiAuto.Si.Level = (Level)Enum.Parse(typeof(Level), level, true);
        }

        private static ServiceHost CreateServiceHost(string port, string strDbCon)
        {
            DataContext dataContext = null;
            try
            {
                dataContext = new DataContext(strDbCon);
                // AppParameter appParameter = dataContext.AppParameters.Where(a => a.Code == "CLIENT_ID").Single();
                AppParameter appParameter2 =
                    dataContext.AppParameters.Where(a => a.Code == "SERVER_CULTURE_INFO_NAME").SingleOrDefault();
                // ClientId = Convert.ToInt64(appParameter.Value);
                if (appParameter2 == null)
                {
                    var myexc =
                        new ApplicationException("Failed to load 'SERVER_CULTURE_INFO_NAME' Application Parameter.");
                    SiAuto.Main.LogException(myexc);
                    throw myexc;
                }
                ServerSessionRegistry.ServerCultureInfo = new CultureInfo(appParameter2.Value, false);
            }
            catch (Exception exc)
            {
                SiAuto.Main.LogException("Unhandled Exception", exc);
                throw exc;
            }
            finally
            {
                if (dataContext != null) dataContext.Dispose();
            }
            
            var serviceHost = new ServiceHost(typeof(ReportsService));


            var netTcpBinding = new NetTcpBinding(SecurityMode.None)
                                    {
                                        MaxReceivedMessageSize = Int32.MaxValue,
                                        OpenTimeout = TimeSpan.MaxValue,
                                        CloseTimeout = TimeSpan.MaxValue,
                                        ReceiveTimeout = TimeSpan.MaxValue,
                                        SendTimeout = TimeSpan.MaxValue,
                                        ListenBacklog = 1000,
                                        PortSharingEnabled = true,
                                        ReaderQuotas =
                                            new XmlDictionaryReaderQuotas { MaxStringContentLength = Int32.MaxValue, MaxArrayLength = Int32.MaxValue },
                                    };
            netTcpBinding.ReliableSession.Enabled = true;
            netTcpBinding.ReliableSession.Ordered = true;
            netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromHours(11);
            serviceHost.AddServiceEndpoint(typeof (IReportsService), netTcpBinding,
                                           new Uri("net.tcp://localhost:" + port + "/Exis/FreightMetrics/ReportsEngine"));

            netTcpBinding = new NetTcpBinding(SecurityMode.None)
            {
                MaxReceivedMessageSize = Int32.MaxValue,
                OpenTimeout = TimeSpan.MaxValue,
                CloseTimeout = TimeSpan.MaxValue,
                ReceiveTimeout = TimeSpan.MaxValue,
                SendTimeout = TimeSpan.MaxValue,
                ListenBacklog = 100,
                PortSharingEnabled = true,
                ReaderQuotas =
                    new XmlDictionaryReaderQuotas
                    {
                        MaxStringContentLength = Int32.MaxValue,
                        MaxArrayLength = Int32.MaxValue
                    },
            };
            netTcpBinding.ReliableSession.Enabled = false;
            serviceHost.AddServiceEndpoint(typeof(IReportsService), netTcpBinding,
                                           new Uri("net.tcp://localhost:" + (Convert.ToInt32(port) + 2) + "/Exis/FreightMetrics/ReportsEngine"));

            var httpBinaryBinding =
                new CustomBinding(new BindingElementCollection()
                                      {
                                          new BinaryMessageEncodingBindingElement()
                                              {
                                                  ReaderQuotas =
                                                      new XmlDictionaryReaderQuotas
                                                          {
                                                              MaxStringContentLength = Int32.MaxValue,
                                                              MaxArrayLength = Int32.MaxValue
                                                          }
                                              },
                                          new HttpTransportBindingElement() {MaxReceivedMessageSize = Int32.MaxValue}
                                      })
                {
                    OpenTimeout = TimeSpan.MaxValue,
                    CloseTimeout = TimeSpan.MaxValue,
                    ReceiveTimeout = TimeSpan.MaxValue,
                    SendTimeout = TimeSpan.MaxValue
                };
            serviceHost.AddServiceEndpoint(typeof(IReportsService), httpBinaryBinding,
                                           new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) + "/Exis/FreightMetrics/ReportsEngine"));

            serviceHost.Description.Behaviors.Add(new ServiceThrottlingBehavior
            {
                MaxConcurrentCalls = 10, // For Singletons with Multiple Threads, the number of concurrent running threads
                // SMaxConcurrentInstances = 100,// Ignored for Singletons
                MaxConcurrentSessions = 1000 // No of Clients with a Transport session aka connected to this Service
            });
#if DEBUG
            var behavior = new ServiceMetadataBehavior { HttpGetEnabled = true, HttpGetUrl = new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) + "/Exis/FreightMetrics/ReportsEngine/" + "mex") };
            serviceHost.Description.Behaviors.Add(behavior);
#endif

            return serviceHost;
        }

        private static void HandleArguments(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine(
                    "Usage: ReportsEngine <Installation Name> <Connection String> <LogLevel> <Port> (<SmtpHost> <SmtpPort> <SupportAddress> <IssueTrackerAddress> <InstallationAddress>)");
                throw new ApplicationException("Invalid count of arguments.");
            }

            string InstallationName = args[0];
            string ConnectionString = args[1];

            string LogLevel = args[2];
            string Port = args[3];

            Console.WriteLine("Reports Engine Starting");
            Console.WriteLine("Arguments:");
            foreach (string s in args)
            {
                Console.WriteLine("\t- " + s);
            }
            SiAuto.Si.AppName = "Reports Engine - " + InstallationName;

            SiAuto.Si.Connections = "file(append=\"true\", filename=\"" +
                                    Thread.GetDomain().SetupInformation.ApplicationBase + "Logs\\ReportsEngineLog_" +
                                    InstallationName +
                                    ".sil\", maxsize=\"65536\", rotate=\"daily\", backlog=\"0\", caption=\"file\", flushon=\"error\", keepopen=\"true\", reconnect=\"false\")";
            SiAuto.Si.DefaultLevel = Level.Message;
            SiAuto.Si.Enabled = true;
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), LogLevel, true);

            objServiceHost = CreateServiceHost(Port, ConnectionString);
            ServerSessionRegistry.ConnectionString = ConnectionString;
            if (args.Length > 4)
            {
                string smtpHost = args[4];
                string smtpPort = args[5];
                string supportAddress = args[6];
                string issueTrackerAddress = args[7];
                string installationAddress = args[8];

                if (smtpHost != "0")
                {
                    //objService.smtpClient = new SmtpClient(smtpHost, Int32.Parse(smtpPort));
                    //objService.exisSupportEmailAddress = supportAddress;
                    //objService.exisIssueTrackerEmailAddress = issueTrackerAddress;
                    //objService.installationEmailAddress = installationAddress;
                }
            }
        }
    }
}