﻿using System;
using System.Collections.Generic;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    public class JobsEngine : AbstractEngine
    {
        public JobsEngine(int EngineId, string LogLevel, bool IsRunning, Installation installation)
            : base(EngineId, LogLevel, IsRunning, installation, EngineTypeEnum.JobsEngine, "Exis.JobsEngineConsole.exe")
        {
        }

        public override string[] GetEngineArguments()
        {
            List<string> args = new List<string>();

            args.Add(m_Installation.Name);
            args.Add(m_Installation.DBSettings.IsLocal() ? m_Installation.DBSettings.GetLocalConnectionString() : m_Installation.DBSettings.GetNetworkConnectionString());
            args.Add(m_LogLevel); 
            return args.ToArray();
        }
    }
}
