﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class Installation
    {
        [DataMember]
        private string m_Name;
        [DataMember]
        private string m_BaseFolder;
        [DataMember]
        private readonly List<WorkflowEngine> m_WorkflowEngines;
        [DataMember]
        private readonly WorkflowEngine m_SpecialWorkflowEngine;
        [DataMember]
        private JobsEngine m_JobsEngine;
        [DataMember]
        private InterfaceEngine m_InterfaceEngine;
        [DataMember]
        private ClientsEngine m_ClientsEngine;
        [DataMember]
        private ReportsEngine m_ReportsEngine;
        [DataMember]
        private DBSettings m_DBSettings;
        [DataMember]
        private LogType m_LogType;
        [DataMember]
        private SmtpSettings m_SmtpSettings;
        [DataMember]
        private string m_Port;
        [DataMember]
        private int m_ConcurrentCalls;
        [DataMember]
        private bool m_IsRunning;
        [DataMember] 
        private bool m_ProcessNewCases;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string BaseFolder
        {
            get { return m_BaseFolder; }
            set { m_BaseFolder = value; }
        }

        public DBSettings DBSettings
        {
            get { return m_DBSettings; }
            set { m_DBSettings = value; }
        }

        public LogType LogType
        {
            get { return m_LogType; }
            set { m_LogType = value; }
        }

        public SmtpSettings SmtpSettings
        {
            get { return m_SmtpSettings; }
            set { m_SmtpSettings = value; }
        }

        public bool HasWorkflowEngines
        {
            get { return m_WorkflowEngines != null && m_WorkflowEngines.Count > 0; }
        }

        public bool HasJobsEngine
        {
            get { return m_JobsEngine != null; }
        }

        public bool HasInterfaceEngine
        {
            get { return m_InterfaceEngine != null; }
        }

        public bool HasClientsEngine
        {
            get { return m_ClientsEngine != null; }
        }

        public bool HasReportsEngine
        {
            get { return m_ReportsEngine != null; }
        }

        public ReadOnlyCollection<WorkflowEngine> WorkflowEngines
        {
            get { return m_WorkflowEngines.AsReadOnly(); }
        }

        public WorkflowEngine SpecialWorkflowEngine
        {
            get { return m_SpecialWorkflowEngine; }
        }

        public JobsEngine JobsEngine
        {
            get { return m_JobsEngine; }
        }

        public InterfaceEngine InterfaceEngine
        {
            get { return m_InterfaceEngine; }
        }

        public ClientsEngine ClientsEngine
        {
            get { return m_ClientsEngine; }
        }

        public ReportsEngine ReportsEngine
        {
            get { return m_ReportsEngine; }
        }

        public string Port
        {
            get { return m_Port; }
            set { m_Port = value; }
        }

        public int ConcurrentCalls
        {
            get { return m_ConcurrentCalls; }
            set { m_ConcurrentCalls = value; }
        }

        public bool IsRunning
        {
            get { return m_IsRunning; }
            set { m_IsRunning = value; }
        }

        public bool ProcessNewCases
        {
            get { return m_ProcessNewCases; }
            set { m_ProcessNewCases = value; }
        }

     public List<AbstractEngine> GetAllEngines()
        {
            List<AbstractEngine> engines = new List<AbstractEngine>();
            engines.Add(m_SpecialWorkflowEngine);
            if (HasJobsEngine)
                engines.Add(m_JobsEngine);
            if (HasInterfaceEngine)
                engines.Add(m_InterfaceEngine);
            if (HasClientsEngine)
                engines.Add(m_ClientsEngine);
            if (HasReportsEngine)
                engines.Add(m_ReportsEngine);
            if (HasWorkflowEngines)
                foreach (WorkflowEngine engine in m_WorkflowEngines)
                {
                    engines.Add(engine);
                }
            return engines;
        }

        public bool HasWorkflowEngine(int EngineId)
        {
            foreach (WorkflowEngine engine in m_WorkflowEngines)
            {
                if (engine.EngineId == EngineId)
                    return true;
            }
            return false;
        }

    public Installation(string name, string baseFolder, DBSettings DBSettings, LogType logType, string port, int concurrentCalls, bool isRunning, bool processNewCases)
            : this(concurrentCalls, isRunning, processNewCases)
        {
            m_Name = name;
            m_BaseFolder = baseFolder;
            m_DBSettings = DBSettings;
            m_LogType = logType;
            m_Port = port;
        }

        private Installation(int? concurrentCalls, bool isRunning, bool processNewCases)
        {
            m_Name = "";
            m_BaseFolder = "";
            m_DBSettings = new DBSettings();

            m_WorkflowEngines = new List<WorkflowEngine>();
            m_JobsEngine = null;
            m_InterfaceEngine = null;
            m_ClientsEngine = null;
            m_ReportsEngine = null;

            m_LogType = LogType.NoLog;
            m_Port = 9000.ToString();
            m_ConcurrentCalls = concurrentCalls == null || concurrentCalls == 0 ? 5 : concurrentCalls.Value;
            m_IsRunning = isRunning;
            m_ProcessNewCases = processNewCases;

            m_SpecialWorkflowEngine = new WorkflowEngine(0, "Debug", isRunning, this, m_ConcurrentCalls)
                                          {ProcessNewCases = processNewCases};
        }

        public Installation()
            : this(null, true, true)
        {
        }

        private void AddWorkflowEngine(int EngineId, string LogLevel, bool IsRunning, int concurrentCalls)
        {
            foreach (WorkflowEngine workflowEngine in m_WorkflowEngines)
            {
                if (EngineId == workflowEngine.EngineId)
                {
                    ApplicationException exc = new ApplicationException("Workflow Engine with Id " + EngineId + " already exists in this installation");
                    exc.Source = "AddWorkflowEngine";
                    throw exc;
                }
            }
            m_WorkflowEngines.Add(new WorkflowEngine(EngineId, LogLevel, IsRunning, this, concurrentCalls == 0 ? 2 : concurrentCalls));
        }

        private void RemoveWorkflowEngine(int EngineId)
        {
            WorkflowEngine engine = GetWorkflowEngine(EngineId);
            if (engine == null || !m_WorkflowEngines.Remove(engine))
            {
                ApplicationException exc = new ApplicationException("Workflow Engine with Id " + EngineId + " does not exist in this installation");
                throw exc;
            }
        }

        public WorkflowEngine GetWorkflowEngine(int EngineId)
        {
            foreach (WorkflowEngine engine in m_WorkflowEngines)
            {
                if (engine.EngineId == EngineId)
                    return engine;
            }
            return null;
        }

        private void AddJobsEngine(string Level, bool IsRunning)
        {
            if (HasJobsEngine)
            {
                ApplicationException exc = new ApplicationException("Jobs Engine already exists in this installation");
                exc.Source = "AddJobsEngine";
                throw exc;
            }
            m_JobsEngine = new JobsEngine(1, Level, IsRunning, this);
        }

        private void RemoveJobsEngine()
        {
            m_JobsEngine = null;
        }

        private void AddInterfaceEngine(string Level, bool IsRunning)
        {
            if (HasInterfaceEngine)
            {
                ApplicationException exc = new ApplicationException("Interface Engine already exists in this installation");
                exc.Source = "AddInterfaceEngine";
                throw exc;
            }
            m_InterfaceEngine = new InterfaceEngine(1, Level, IsRunning, this);
        }

        private void RemoveInterfaceEngine()
        {
            m_InterfaceEngine = null;
        }

        private void AddClientsEngine(string Level, bool IsRunning)
        {
            if (HasClientsEngine)
            {
                ApplicationException exc = new ApplicationException("Clients Engine already exists in this installation");
                exc.Source = "AddClientsEngine";
                throw exc;
            }
            m_ClientsEngine = new ClientsEngine(1, Level, IsRunning, this);
        }

        private void AddReportsEngine(string Level, bool IsRunning)
        {
            if (HasReportsEngine)
            {
                ApplicationException exc = new ApplicationException("Reports Engine already exists in this installation");
                exc.Source = "AddReportsEngine";
                throw exc;
            }
            m_ReportsEngine = new ReportsEngine(1, Level, IsRunning, this);
        }

        private void RemoveClientsEngine()
        {
            m_ClientsEngine = null;
        }

        private void RemoveReportsEngine()
        {
            m_ReportsEngine = null;
        }


        public void AddEngine(AbstractEngine engine)
        {
            if (engine.Installation.Name != m_Name)
                throw new ApplicationException("Engine " + engine + " does not belong to Installation " + m_Name);

            switch (engine.EngineType)
            {
                case EngineTypeEnum.WorkflowEngine:
      AddWorkflowEngine(engine.EngineId, engine.LogLevel, engine.IsRunning, ((WorkflowEngine)engine).ConcurrentCalls);
                    break;
                case EngineTypeEnum.JobsEngine:
                    AddJobsEngine(engine.LogLevel, engine.IsRunning);
                    break;
                case EngineTypeEnum.InterfaceEngine:
                    AddInterfaceEngine(engine.LogLevel, engine.IsRunning);
                    break;
                case EngineTypeEnum.ClientsEngine:
                    ClientsEngine clientsEngine = (ClientsEngine)engine;
                    AddClientsEngine(clientsEngine.LogLevel, clientsEngine.IsRunning);
                    break;
                case EngineTypeEnum.ReportsEngine:
                    ReportsEngine reportsEngine = (ReportsEngine)engine;
                    AddReportsEngine(reportsEngine.LogLevel, reportsEngine.IsRunning);
                    break;
            }
        }

        public void EditEngine(AbstractEngine engine)
        {
            if (engine.Installation.Name != m_Name)
                throw new ApplicationException("Engine " + engine + " does not belong to Installation " + m_Name);

            switch (engine.EngineType)
            {
                case EngineTypeEnum.WorkflowEngine:
                    if(engine.EngineId == 0)
                    {
                        m_SpecialWorkflowEngine.LogLevel = engine.LogLevel;
                        m_SpecialWorkflowEngine.ConcurrentCalls = ((WorkflowEngine)engine).ConcurrentCalls;
                        ConcurrentCalls = ((WorkflowEngine)engine).ConcurrentCalls;
                    }
                    foreach (var workflowEngine in m_WorkflowEngines)
                    {
                        if (workflowEngine.EngineId == engine.EngineId)
                        {
                            workflowEngine.LogLevel = engine.LogLevel;
                            workflowEngine.ConcurrentCalls = ((WorkflowEngine) engine).ConcurrentCalls;
                        }
                    }
                    break;
            }
        }

        public void RemoveEngine(AbstractEngine engine)
        {
            if (!engine.Installation.Equals(this))
                throw new ApplicationException("Engine " + engine + " does not belong to Installation " + m_Name);

            switch (engine.EngineType)
            {
                case EngineTypeEnum.WorkflowEngine:
                    RemoveWorkflowEngine(engine.EngineId);
                    break;
                case EngineTypeEnum.JobsEngine:
                    RemoveJobsEngine();
                    break;
                case EngineTypeEnum.InterfaceEngine:
                    RemoveInterfaceEngine();
                    break;
                case EngineTypeEnum.ClientsEngine:
                    RemoveClientsEngine();
                    break;
                case EngineTypeEnum.ReportsEngine:
                    RemoveReportsEngine();
                    break;
            }
        }

        public override int GetHashCode()
        {
            return m_Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != GetType()) return false;
            return m_Name == ((Installation) obj).Name;
        }

        public override string ToString()
        {
            return "Installation " + m_Name + " (" +
                   (m_DBSettings.IsLocal()
                        ? m_DBSettings.Service
                        : m_DBSettings.HostList[0].Host + ":" + m_DBSettings.HostList[0].Port) + ", " +
                   m_BaseFolder + ")";
        }
    }
}