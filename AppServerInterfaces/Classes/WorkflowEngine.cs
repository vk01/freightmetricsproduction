﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    public class WorkflowEngine : AbstractEngine
    {
        [DataMember]
        private int _ConcurrentCalls;
        public bool _ProcessNewCases;

        public int ConcurrentCalls
        {
            get { return _ConcurrentCalls; }
            set { _ConcurrentCalls = value; }
        }

    public bool ProcessNewCases
        {
            get { return _ProcessNewCases; }
            set { _ProcessNewCases = value; }
        }
        public WorkflowEngine(int EngineId, string LogLevel, bool IsRunning, Installation installation)
            : this(
                EngineId, LogLevel, IsRunning, installation, 0)
        {
        }

        public WorkflowEngine(int EngineId, string LogLevel, bool IsRunning, Installation installation, int concurrentCalls)
            : base(
                EngineId, LogLevel, IsRunning, installation, EngineTypeEnum.WorkflowEngine,
                "Exis.WorkflowEngineConsole.exe")
        {
            _ConcurrentCalls = concurrentCalls;
        }

        public override string[] GetEngineArguments()
        {
            var args = new List<string>();

            args.Add(m_Installation.Name);
            args.Add(m_Installation.DBSettings.IsLocal()
                         ? m_Installation.DBSettings.GetLocalConnectionString()
                         : m_Installation.DBSettings.GetNetworkConnectionString());
            args.Add(m_LogLevel);
            args.Add(m_Installation.Port);
            args.Add(m_EngineId.ToString());
            args.Add(_ConcurrentCalls.ToString());
            args.Add(_ProcessNewCases ? "1" : "0");
            if (m_Installation.SmtpSettings != null)
            {
                args.Add(m_Installation.SmtpSettings.SmtpHost);
                args.Add(m_Installation.SmtpSettings.SmtpPort);
                args.Add(m_Installation.SmtpSettings.SupportEmailAddress);
                args.Add(m_Installation.SmtpSettings.IssueTrackerEmailAddress);
                args.Add(m_Installation.SmtpSettings.InstallationEmailAddress);
            }
            return args.ToArray();
        }
    }
}