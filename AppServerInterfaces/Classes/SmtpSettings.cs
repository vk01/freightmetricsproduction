﻿using System.Runtime.Serialization;

namespace AppServerInterfaces.Classes
{
    [DataContract]
    public class SmtpSettings
    {
        [DataMember]
        private string m_SmtpHost;
        [DataMember]
        private string m_SmtpPort;
        [DataMember]
        private string m_SupportEmailAddress;
        [DataMember]
        private string m_IssueTrackerEmailAddress;
        [DataMember]
        private string m_InstallationEmailAddress;

        public string SmtpHost
        {
            get { return m_SmtpHost; }
            set { m_SmtpHost = value; }
        }

        public string SmtpPort
        {
            get { return m_SmtpPort; }
            set { m_SmtpPort = value; }
        }

        public string SupportEmailAddress
        {
            get { return m_SupportEmailAddress; }
            set { m_SupportEmailAddress = value; }
        }

        public string IssueTrackerEmailAddress
        {
            get { return m_IssueTrackerEmailAddress; }
            set { m_IssueTrackerEmailAddress = value; }
        }

        public string InstallationEmailAddress
        {
            get { return m_InstallationEmailAddress; }
            set { m_InstallationEmailAddress = value; }
        }

        public SmtpSettings(string mSmtpHost, string mSmtpPort, string mSupportEmailAddress, string mIssueTrackerEmailAddress, string mInstallationEmailAddress)
        {
            m_SmtpHost = mSmtpHost;
            m_SmtpPort = mSmtpPort;
            m_SupportEmailAddress = mSupportEmailAddress;
            m_IssueTrackerEmailAddress = mIssueTrackerEmailAddress;
            m_InstallationEmailAddress = mInstallationEmailAddress;
        }

        public SmtpSettings Clone()
        {
            return new SmtpSettings(m_SmtpHost, m_SmtpPort, m_SupportEmailAddress, m_IssueTrackerEmailAddress, m_InstallationEmailAddress);
        }
    }
}
