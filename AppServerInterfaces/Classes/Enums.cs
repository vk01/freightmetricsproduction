﻿using System.Runtime.Serialization;

namespace AppServerInterfaces.Classes
{
    [DataContract]
    public enum EngineTypeEnum
    {
        [EnumMember]
        Null = 0,
        [EnumMember]
        WorkflowEngine = 1,
        [EnumMember]
        JobsEngine = 2,
        [EnumMember]
        InterfaceEngine = 3,
        [EnumMember]
        ClientsEngine = 5,
        [EnumMember]
        ReportsEngine = 7
    }

    [DataContract]
    public enum ClientType
    {
        WinClient,
        WebClient,
        AdminClient
    }

    [DataContract]
    public enum LogType
    {
        [EnumMember]
        Null = 0,
        [EnumMember]
        NoLog = 1,
        [EnumMember]
        Message = 2,
        [EnumMember]
        Debug = 3
    }

    [DataContract]
    public enum EngineState
    {
        [EnumMember]
        Stopped = 0,
        [EnumMember]
        Starting = 1,
        [EnumMember]
        Running = 2,
        [EnumMember]
        Stopping = 3,
        [EnumMember]
        Terminating = 4
    }
}
