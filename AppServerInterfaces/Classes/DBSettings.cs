﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    [DataContract]
    public class DBSettings
    {
        [DataMember]
        private string m_User;
        [DataMember]
        private string m_Pass;
        [DataMember]
        private string m_Service;
        [DataMember]
        private List<HostPortPair> m_HostList;
        [DataMember]
        private string m_ExtraOptions;

        public string User
        {
            get { return m_User; }
            set { m_User = value; }
        }

        public string Pass
        {
            get { return m_Pass; }
            set { m_Pass = value; }
        }

        public string Service
        {
            get { return m_Service; }
            set { m_Service = value; }
        }

        public List<HostPortPair> HostList
        {
            get { return m_HostList; }
            set { m_HostList = value; }
        }

        public string ExtraOptions
        {
            get { return m_ExtraOptions; }
            set { m_ExtraOptions = value; }
        }

        public DBSettings(string m_User, string m_Pass, string m_Service, List<HostPortPair> m_HostList, string m_ExtraOptions)
        {
            this.m_User = m_User;
            this.m_Pass = m_Pass;
            this.m_Service = m_Service;
            this.m_HostList = m_HostList;
            this.m_ExtraOptions = m_ExtraOptions;
        }

        public DBSettings()
        {
            m_User = "";
            m_Pass = "";
            m_Service = "";
            m_HostList = new List<HostPortPair>();
            m_ExtraOptions = "";
        }

        public DBSettings Clone()
        {
            return new DBSettings(m_User, m_Pass, m_Service, new List<HostPortPair>(m_HostList), m_ExtraOptions);
        }

        public bool IsLocal()
        {
            return m_HostList.Count == 0;
        }

        public bool IsNetwork()
        {
            return m_HostList.Count > 0;
        }

        public bool IsEmpty()
        {
            return String.IsNullOrEmpty(m_User) && String.IsNullOrEmpty(m_Pass) && String.IsNullOrEmpty(m_Service) &&
                   m_HostList.Count == 0;
        }

        public string GetNetworkConnectionString()
        {
            string strConnectionString = "Data Source=" +
                    @"  (DESCRIPTION =
                        (ADDRESS_LIST =";

            foreach (HostPortPair host in m_HostList)
            {
                strConnectionString += "(ADDRESS = (PROTOCOL = TCP)(HOST = " + host.Host + ")(PORT = " + host.Port + "))";
            }

            if (m_HostList.Count > 1)
            {
                strConnectionString += ")(LOAD_BALANCE = yes)(CONNECT_DATA = (SERVER=DEDICATED) (SERVICE_NAME = " + m_Service + ")" +
                    "(FAILOVER_MODE = (TYPE = SELECT)(METHOD = BASIC)(RETRIES = 180)(DELAY = 5))));";
            }
            else
            {
                strConnectionString += ")(CONNECT_DATA = (SERVICE_NAME = " + m_Service + ")));";
            }

//            strConnectionString += "User Id=" + m_User + ";Password=" + m_Pass + ";Pooling=true";
            strConnectionString += "User Id=" + m_User + ";Password=" + m_Pass +
                                   (String.IsNullOrEmpty(m_ExtraOptions) ? "" : ";" + m_ExtraOptions);

            return strConnectionString;
        }

        public string GetLocalConnectionString()
        {
            string strConnectionString = "Data Source=" + m_Service + ";User Id=" + m_User + ";Password=" + m_Pass +
                                         (String.IsNullOrEmpty(m_ExtraOptions) ? "" : ";" + m_ExtraOptions);
//                             ";Pooling=true";

            return strConnectionString;
        }

        public string GetConnectionString()
        {
            if (IsNetwork())
                return GetNetworkConnectionString();
            return GetLocalConnectionString();
        }
    }
}
