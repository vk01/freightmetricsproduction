﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Gurock.SmartInspect;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    public class ExisLogEntry
    {
        #region Variables

        private readonly string m_Title;
        private readonly string m_AppName;
        private readonly string m_SessionName;
        private readonly string m_HostName;
        private readonly int m_ProcessId;
        private readonly int m_ThreadId;
        private readonly ExisLogEntry m_ParentEntry;
        private readonly string m_DataString;

        private readonly DateTime m_Timestamp;
        private readonly LogEntryType m_LogEntryType;
        private readonly Stream m_Data;

        private readonly List<ExisLogEntry> m_DetailEntries;

        #endregion

        #region Get Properties

        public string Title
        {
            get { return m_Title; }
        }

        public string AppName
        {
            get { return m_AppName; }
        }

        public string SessionName
        {
            get { return m_SessionName; }
        }

        public string HostName
        {
            get { return m_HostName; }
        }

        public int ProcessId
        {
            get { return m_ProcessId; }
        }

        public int ThreadId
        {
            get { return m_ThreadId; }
        }

        public DateTime Timestamp
        {
            get { return m_Timestamp; }
        }

        public LogEntryType LogEntryType
        {
            get { return m_LogEntryType; }
        }

        public Stream Data
        {
            get { return m_Data; }
        }

        public string DataString
        {
            get
            {
                return m_DataString;
            }
        }

        public ReadOnlyCollection<ExisLogEntry> DetailEntries
        {
            get { return m_DetailEntries.AsReadOnly(); }
        }

        public ExisLogEntry ParentEntry
        {
            get { return m_ParentEntry; }
        }

        #endregion

        #region Constructor

        public ExisLogEntry(LogEntry entry)
        {
            m_Title = entry.Title;
            m_AppName = entry.AppName;
            m_SessionName = entry.SessionName;
            m_HostName = entry.HostName;
            m_ProcessId = entry.ProcessId;
            m_ThreadId = entry.ThreadId;
            m_Timestamp = entry.Timestamp;
            m_LogEntryType = entry.LogEntryType;
            m_Data = entry.Data;
            if(m_Data != null)
            {
                StreamReader reader = new StreamReader(m_Data);
                m_DataString = reader.ReadToEnd();
            }
            m_ParentEntry = null;
            m_DetailEntries = new List<ExisLogEntry>();
        }

        public ExisLogEntry(string Title, string AppName, string SessionName, string HostName, DateTime Timestamp, LogEntryType LogEntryType)
        {
            m_Title = Title;
            m_AppName = AppName;
            m_SessionName = SessionName;
            m_HostName = HostName;
            m_Timestamp = Timestamp;
            m_LogEntryType = LogEntryType;
            m_DetailEntries = new List<ExisLogEntry>();
            m_ParentEntry = null;
        }

        private ExisLogEntry(ExisLogEntry entry, ExisLogEntry ParentEntry)
        {
            m_Title = entry.Title;
            m_AppName = entry.AppName;
            m_SessionName = entry.SessionName;
            m_HostName = entry.HostName;
            m_ProcessId = entry.ProcessId;
            m_ThreadId = entry.ThreadId;
            m_Timestamp = entry.Timestamp;
            m_LogEntryType = entry.LogEntryType;
            m_Data = entry.Data;
            m_DataString = entry.DataString;
            m_DetailEntries = new List<ExisLogEntry>();
            m_ParentEntry = ParentEntry;
        }

        public ExisLogEntry AddChildEntry(ExisLogEntry entry)
        {
            ExisLogEntry newEntry = new ExisLogEntry(entry, this);
            m_DetailEntries.Add(newEntry);
            return newEntry;
        }

        public ExisLogEntry Clone()
        {
            ExisLogEntry logEntry = new ExisLogEntry(this, ParentEntry);
            return logEntry;
        }
        #endregion
    }
}
