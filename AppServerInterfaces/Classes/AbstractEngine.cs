﻿using System;
using System.Runtime.Serialization;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    [DataContract]
    [KnownType(typeof (ReportsEngine))]
    [KnownType(typeof (ClientsEngine))]
    [KnownType(typeof (WorkflowEngine))]
    [KnownType(typeof (JobsEngine))]
    [KnownType(typeof (InterfaceEngine))]
    public abstract class AbstractEngine
    {
        [DataMember]
        protected int m_EngineId;
        [DataMember]
        protected Installation m_Installation;
        [DataMember]
        protected EngineTypeEnum m_EngineType;
        [DataMember]
        protected string m_LogLevel;

        [DataMember]
        protected bool m_IsRunning;
        [DataMember]
        protected bool m_IsStarting;
        [DataMember]
        protected bool m_IsStopping;

        [DataMember] 
        protected EngineState m_State;


        [DataMember]
        protected string m_ExecutableName;

        public Installation Installation
        {
            get { return m_Installation; }
        }

        public int EngineId
        {
            get { return m_EngineId; }
        }

        public EngineTypeEnum EngineType
        {
            get { return m_EngineType; }
            set { m_EngineType = value; }
        }

        public int EngineTypeId
        {
            get { return Convert.ToInt32(m_EngineType.ToString("d")); }
        }

        public string EngineTypeName
        {
            get { return m_EngineType.ToString("g"); }
        }

        public string ExecutableName
        {
            get { return m_ExecutableName; }
        }

        public string ConfigName
        {
            get { return m_ExecutableName + ".config"; }
        }

        public string EngineTypeCanonicalName
        {
            get
            {
                switch (m_EngineType)
                {
                    case EngineTypeEnum.JobsEngine:
                        return "Jobs Engine";
                    case EngineTypeEnum.InterfaceEngine:
                        return "Interface Engine";
                    case EngineTypeEnum.WorkflowEngine:
                        if (EngineId != 0)
                            return string.Format("Workflow Engine #{0}", EngineId);
                        return "Special Workflow Engine";
                    case EngineTypeEnum.ClientsEngine:
                        return "Clients Engine";
                    case EngineTypeEnum.ReportsEngine:
                        return "Reports Engine";
                    default:
                        return null;
                }
            }
        }

        public String LogLevel
        {
            get { return m_LogLevel; }
            set { m_LogLevel = value; }
        }

        public bool IsRunning
        {
            get { return m_IsRunning; }
            set { m_IsRunning = value; }
        }

        public bool IsStarting
        {
            get { return m_IsStarting; }
            set { m_IsStarting = value; }
        }

        public bool IsStopping
        {
            get { return m_IsStopping; }
            set { m_IsStopping = value; }
        }

        public EngineState State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        private AbstractEngine()
        {
            m_IsRunning = false;
            m_IsStarting = false;
            m_IsStopping = false;
            m_LogLevel = "Message";
            m_State = EngineState.Stopped;
        }

        protected AbstractEngine(int engineId, string LogLevel, bool IsRunning, Installation installation, EngineTypeEnum engineTypeEnum, string executableName) : this()
        {
            m_EngineId = engineId;
            m_Installation = installation;
            m_EngineType = engineTypeEnum;
            m_IsRunning = IsRunning;
            m_LogLevel = LogLevel;
            m_ExecutableName = executableName;
        }

        public abstract string[] GetEngineArguments();

   public override bool Equals(object obj)
        {
            if (obj.GetType() != GetType()) return false;
            AbstractEngine engine = (AbstractEngine)obj;
            return m_EngineId == engine.EngineId && m_EngineType == engine.EngineType &&
                   m_Installation.Name == engine.Installation.Name;
        }

        public override int GetHashCode()
        {
            return m_Installation.Name.GetHashCode() ^ m_EngineType.GetHashCode() ^ m_EngineId.GetHashCode();
        }

        public override string ToString()
        {
            return EngineTypeCanonicalName + " with engine Id " + EngineId + " for installation " + Installation.Name;
        }
    }
}