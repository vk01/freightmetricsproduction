using System;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    public class HostPortPair
    {
        private string host;
        private string port;

        public HostPortPair()
        {
        }

        public HostPortPair(string host, string port) : this()
        {
            this.host = host;
            this.port = port;
        }

        public string Host
        {
            get { return host; }
            set { host = value; }
        }

        public string Port
        {
            get { return port; }
            set { port = value; }
        }
    }
}