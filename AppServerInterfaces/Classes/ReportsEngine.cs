﻿using System;
using System.Collections.Generic;

namespace AppServerInterfaces.Classes
{
    [Serializable]
    public class ReportsEngine : AbstractEngine
    {
        public ReportsEngine(int EngineId, string LogLevel, bool IsRunning,
                             Installation installation)
            : base(EngineId, LogLevel, IsRunning, installation, EngineTypeEnum.ReportsEngine, "Exis.ReportsEngineConsole.exe"
                )
        {
        }

        public override string[] GetEngineArguments()
        {
            var args = new List<string>();

            args.Add(m_Installation.Name);
            args.Add(m_Installation.DBSettings.IsLocal()
                         ? m_Installation.DBSettings.GetLocalConnectionString()
                         : m_Installation.DBSettings.GetNetworkConnectionString());
            args.Add(m_LogLevel);
            args.Add(m_Installation.Port);
            if (m_Installation.SmtpSettings != null)
            {
                args.Add(m_Installation.SmtpSettings.SmtpHost);
                args.Add(m_Installation.SmtpSettings.SmtpPort);
                args.Add(m_Installation.SmtpSettings.SupportEmailAddress);
                args.Add(m_Installation.SmtpSettings.IssueTrackerEmailAddress);
                args.Add(m_Installation.SmtpSettings.InstallationEmailAddress);
            }
            return args.ToArray();
        }
    }
}