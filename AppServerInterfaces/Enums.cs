﻿namespace AppServerInterfaces
{
    public enum LicenseThresholdEnum
    {
        Normal,
        Warning,
        Error
    }

    public enum CtiAgentStatus
    {
        Unknown = 0,
        NotReady = 1,
        Ready = 2,
        ActiveInCtiCall = 3,
        ActiveInNonCtiCall = 4,
        Logging = 5,
        TaskWaiting = 6,
        Busy
    }
}