﻿using System.Threading;

namespace AppServerInterfaces
{
    public interface IRemoteAppServer
    {
        Mutex GetWorkflowEngineLock(int EngineId);
    }
}