﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel;
using AppServerInterfaces.Classes;

namespace AppServerInterfaces
{
    [ServiceContract]
    public interface IAppServer
    {
        [OperationContract]
        bool CheckAlive();
        [OperationContract]
        void SetLicenseData(string File, int Users, int Customers);
        [OperationContract]
        bool ProcessOnDemandCase(long CaseId);
        [OperationContract]
        bool RegisterOnDemandCases(List<long> CaseIds);
        [OperationContract]
        bool ProcessCustomerCases(List<long> customerIds);
        [OperationContract]
        bool UnregisterCase(long CaseId);
        [OperationContract]
        void StartEngine(AbstractEngine engine);
        [OperationContract]
        void StopEngine(AbstractEngine engine);
        [OperationContract]
        void TerminateEngine(AbstractEngine engine);
        [OperationContract]
        List<ExisLogEntry> GetEngineLogEntries(DateTime fromDate, DateTime toDate, AbstractEngine engine);
        [OperationContract]
        List<ExisLogEntry> GetServiceLogEntries(DateTime fromDate, DateTime toDate);
        [OperationContract]
        void ChangeEngineDebugLevel(AbstractEngine engine, string level);
        [OperationContract]
        long? GetNextCase(long EngineId);
/*
        [OperationContract]
        [UseNetDataContractSerializer]
        Mutex GetWorkflowEngineLock(int EngineId);
*/

        [OperationContract]
        Installation GetInstallation();
        [OperationContract]
        List<HostPortPair> GetInstallations();

        [OperationContract]
        bool AddEngine(AbstractEngine engine);
        [OperationContract]
        bool EditEngine(AbstractEngine engine);
        [OperationContract]
        bool RemoveEngine(AbstractEngine engine);

        [OperationContract]
        void LogClientException(ClientType clientType, string userName, string message, Exception e);
        [OperationContract]
        void LogClientError(ClientType clientType, string userName, string message);
        [OperationContract]
        string GetLogRouterURL();
        [OperationContract]
        LogType GetInstallationLogType(Installation installation);
    }
}