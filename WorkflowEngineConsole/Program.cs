﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Threading;
using System.Xml;
using Exis.Server.ServiceInterfaces;
using Exis.ServiceInterfaces;
using Exis.WorkflowEngine;
using Exis.SessionRegistry;
using Gurock.SmartInspect;

namespace Exis.WorkflowEngineConsole
{
    [Serializable]
    public class Program
    {
        private static string ConnectionString;
        private static int EngineId;
        private static ServiceHost objServiceHost;
        private static WorkflowEngine.WorkflowEngine workflowEngine;
        private static bool processNewCases;
        public long CaseId;

        private static int Main(string[] args)
        {
            if (!Thread.GetDomain().IsDefaultAppDomain())
            {
                try
                {
                    ServerSessionRegistry.initialize();
                    ServerSessionRegistry.ApplicationDirectory =
                        Thread.GetDomain().SetupInformation.ApplicationBase;
                    ServerSessionRegistry.User = "WorkflowEngine";
                    string port = HandleArguments(args);
                    objServiceHost.Open();
                    workflowEngine = new WorkflowEngine.WorkflowEngine(EngineId, ConnectionString, processNewCases);
                    var ipcBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                                         {
                                             MaxReceivedMessageSize = Int32.MaxValue,
                                             OpenTimeout = TimeSpan.MaxValue,
                                             CloseTimeout = TimeSpan.MaxValue,
                                             ReceiveTimeout = TimeSpan.MaxValue,
                                             SendTimeout = TimeSpan.MaxValue,
                                             ReaderQuotas =
                                                 new XmlDictionaryReaderQuotas
                                                     {
                                                         MaxStringContentLength = Int32.MaxValue,
                                                         MaxArrayLength = Int32.MaxValue,
                                                         MaxDepth = Int32.MaxValue,
                                                         MaxBytesPerRead = Int32.MaxValue
                                                     }
                                         };
                    var address =
                        new EndpointAddress("net.pipe://localhost/Exis/FreightMetrics/WorkflowEngine/" + EngineId + "/" + port);
                    IWorkflowServiceOneWay proxy = ChannelFactory<IWorkflowServiceOneWay>.CreateChannel(ipcBinding, address);
                    workflowEngine.address = address;
                    workflowEngine.ipcBinding = ipcBinding;
                    workflowEngine.workflowService = proxy;
                    var workerThread = new Thread(workflowEngine.Run);
                    workerThread.Start();
                }
          catch(Exception e)
                {
                    SiAuto.Main.LogException(e);
                    return 1;
                }
            }
            else
            {
                try
                {
                    objServiceHost = CreateServiceHost("9000", "1", 1);
                    objServiceHost.Open();
                }
                catch
                {
                    return 1;
                }
                Console.ReadLine();
                objServiceHost.Close();
            }
            return 0;
        }

        private static ServiceHost CreateServiceHost(string port, string strEngineId, int concurrentCalls)
        {
            ServerSessionRegistry.ConnectionString = "User Id=crm3;Password=crm3;Data Source=artemis;Pooling=true";

            var serviceHost = new ServiceHost(typeof (WorkflowService));

            var ipcBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                                 {
                                     MaxReceivedMessageSize = Int32.MaxValue,
                                     OpenTimeout = TimeSpan.MaxValue,
                                     CloseTimeout = TimeSpan.MaxValue,
                                     ReceiveTimeout = TimeSpan.MaxValue,
                                     SendTimeout = TimeSpan.MaxValue,
                                     ReaderQuotas =
                                         new XmlDictionaryReaderQuotas
                                             {
                                                 MaxStringContentLength = Int32.MaxValue,
                                                 MaxArrayLength = Int32.MaxValue,
                                                 MaxDepth = Int32.MaxValue,
                                                 MaxBytesPerRead = Int32.MaxValue
                                             }
                                 };
            serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceOneWay), ipcBinding,
                                           "net.pipe://localhost/Exis/FreightMetrics/WorkflowEngine/" + EngineId + "/" + port);

            if (strEngineId == "0")
            {
                /*
                var netTcpBinding = new NetTcpBinding(SecurityMode.None)
                                        {
                                            MaxReceivedMessageSize = Int32.MaxValue,
                                            OpenTimeout = TimeSpan.MaxValue,
                                            CloseTimeout = TimeSpan.MaxValue,
                                            ReceiveTimeout = TimeSpan.MaxValue,
                                            SendTimeout = TimeSpan.MaxValue,
                                            PortSharingEnabled = true,
                                            ListenBacklog = 1000,
                                            ReaderQuotas =
                                                new XmlDictionaryReaderQuotas
                                                    {
                                                        MaxStringContentLength = Int32.MaxValue,
                                                        MaxArrayLength = Int32.MaxValue,
                                                        MaxDepth = Int32.MaxValue,
                                                        MaxBytesPerRead = Int32.MaxValue
                                                    }
                                        };
                netTcpBinding.ReliableSession.Enabled = true;
                netTcpBinding.ReliableSession.Ordered = true;
                netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromHours(11);
                serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceReqRep), netTcpBinding,
                                               new Uri("net.tcp://localhost:" + port + "/WorkflowEngine"));

                var basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None)
                                           {
                                               MaxReceivedMessageSize = Int32.MaxValue,
                                               OpenTimeout = TimeSpan.MaxValue,
                                               CloseTimeout = TimeSpan.MaxValue,
                                               ReceiveTimeout = TimeSpan.MaxValue,
                                               SendTimeout = TimeSpan.MaxValue,
                                               ReaderQuotas =
                                                   new XmlDictionaryReaderQuotas
                                                       {
                                                           MaxStringContentLength = Int32.MaxValue,
                                                           MaxArrayLength = Int32.MaxValue,
                                                           MaxDepth = Int32.MaxValue,
                                                           MaxBytesPerRead = Int32.MaxValue
                                                       }
                                           };
                serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceReqRep), basicHttpBinding,
                                               new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) +
                                                       "/WorkflowEngine"));
                */
                var netTcpBinding = new NetTcpBinding(SecurityMode.None)
                                    {
                                        MaxReceivedMessageSize = Int32.MaxValue,
                                        OpenTimeout = TimeSpan.MaxValue,
                                        CloseTimeout = TimeSpan.MaxValue,
                                        ReceiveTimeout = TimeSpan.MaxValue,
                                        SendTimeout = TimeSpan.MaxValue,
                                        PortSharingEnabled = true,
                                        ListenBacklog = 1000,
                                        ReaderQuotas =
                                            new XmlDictionaryReaderQuotas
                                                {
                                                    MaxStringContentLength = Int32.MaxValue,
                                                    MaxArrayLength = Int32.MaxValue,
                                                    MaxDepth = Int32.MaxValue,
                                                    MaxBytesPerRead = Int32.MaxValue
                                                }
                                    };

                netTcpBinding.ReliableSession.Enabled = true;
                netTcpBinding.ReliableSession.Ordered = true;
                netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromHours(11);

                serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceReqRep), netTcpBinding,
                                               new Uri("net.tcp://localhost:" + port + "/Exis/FreightMetrics/WorkflowEngine/ReqRep"));

                //var basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None)
                //                       {
                //                           MaxReceivedMessageSize = Int32.MaxValue,
                //                           OpenTimeout = TimeSpan.MaxValue,
                //                           CloseTimeout = TimeSpan.MaxValue,
                //                           ReceiveTimeout = TimeSpan.MaxValue,
                //                           SendTimeout = TimeSpan.MaxValue,
                //                           ReaderQuotas =
                //                               new XmlDictionaryReaderQuotas
                //                                   {
                //                                       MaxStringContentLength = Int32.MaxValue,
                //                                       MaxArrayLength = Int32.MaxValue,
                //                                       MaxDepth = Int32.MaxValue,
                //                                       MaxBytesPerRead = Int32.MaxValue
                //                                   }
                //                       };
                //serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceReqRep), basicHttpBinding,
                //                               new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) +
                //                                       "/WorkflowEngine/ReqRep"));

                netTcpBinding = new NetTcpBinding(SecurityMode.None)
                                    {
                                        MaxReceivedMessageSize = Int32.MaxValue,
                                        OpenTimeout = TimeSpan.MaxValue,
                                        CloseTimeout = TimeSpan.MaxValue,
                                        ReceiveTimeout = TimeSpan.MaxValue,
                                        SendTimeout = TimeSpan.MaxValue,
                                        PortSharingEnabled = true,
                                        ListenBacklog = 1000,
                                        ReaderQuotas =
                                            new XmlDictionaryReaderQuotas
                                                {
                                                    MaxStringContentLength = Int32.MaxValue,
                                                    MaxArrayLength = Int32.MaxValue,
                                                    MaxDepth = Int32.MaxValue,
                                                    MaxBytesPerRead = Int32.MaxValue
                                                }
                                    };

                netTcpBinding.ReliableSession.Enabled = true;
                netTcpBinding.ReliableSession.Ordered = true;
                netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromHours(11);

                serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceOneWay), netTcpBinding,
                                               new Uri("net.tcp://localhost:" + port + "/Exis/FreightMetrics/WorkflowEngine/OneWay"));

                //basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None)
                //                       {
                //                           MaxReceivedMessageSize = Int32.MaxValue,
                //                           OpenTimeout = TimeSpan.MaxValue,
                //                           CloseTimeout = TimeSpan.MaxValue,
                //                           ReceiveTimeout = TimeSpan.MaxValue,
                //                           SendTimeout = TimeSpan.MaxValue,
                //                           ReaderQuotas =
                //                               new XmlDictionaryReaderQuotas
                //                                   {
                //                                       MaxStringContentLength = Int32.MaxValue,
                //                                       MaxArrayLength = Int32.MaxValue,
                //                                       MaxDepth = Int32.MaxValue,
                //                                       MaxBytesPerRead = Int32.MaxValue
                //                                   }
                //                       };
                //serviceHost.AddServiceEndpoint(typeof (IWorkflowServiceOneWay), basicHttpBinding,
                //                               new Uri("http://localhost:" + (Convert.ToInt32(port) + 2) +
                //                                       "/WorkflowEngine/OneWay"));
                var httpBinaryBinding =
                new CustomBinding(new BindingElementCollection()
                                      {
                                          new BinaryMessageEncodingBindingElement()
                                              {
                                                  ReaderQuotas =
                                                      new XmlDictionaryReaderQuotas
                                                          {
                                                              MaxStringContentLength = Int32.MaxValue,
                                                              MaxArrayLength = Int32.MaxValue
                                                          }
                                              },
                                          new HttpTransportBindingElement() {MaxReceivedMessageSize = Int32.MaxValue}
                                      })
                {
                    OpenTimeout = TimeSpan.MaxValue,
                    CloseTimeout = TimeSpan.MaxValue,
                    ReceiveTimeout = TimeSpan.MaxValue,
                    SendTimeout = TimeSpan.MaxValue
                };
                serviceHost.AddServiceEndpoint(typeof(IWorkflowServiceReqRep), httpBinaryBinding,
                                               new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) +
                                                       "/Exis/FreightMetrics/WorkflowEngine/ReqRep"));
            
                httpBinaryBinding =
                new CustomBinding(new BindingElementCollection()
                                      {
                                          new BinaryMessageEncodingBindingElement()
                                              {
                                                  ReaderQuotas =
                                                      new XmlDictionaryReaderQuotas
                                                          {
                                                              MaxStringContentLength = Int32.MaxValue,
                                                              MaxArrayLength = Int32.MaxValue
                                                          }
                                              },
                                          new HttpTransportBindingElement() {MaxReceivedMessageSize = Int32.MaxValue}
                                      })
                {
                    OpenTimeout = TimeSpan.MaxValue,
                    CloseTimeout = TimeSpan.MaxValue,
                    ReceiveTimeout = TimeSpan.MaxValue,
                    SendTimeout = TimeSpan.MaxValue
                };
                serviceHost.AddServiceEndpoint(typeof(IWorkflowServiceOneWay), httpBinaryBinding,
                                               new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) +
                                                       "/Exis/FreightMetrics/WorkflowEngine/OneWay"));
            }


            serviceHost.Description.Behaviors.Add(new ServiceThrottlingBehavior
                                                      {
                                                          MaxConcurrentCalls = concurrentCalls,
                                                          // For Singletons with Multiple Threads, the number of concurrent running threads
                                                          //MaxConcurrentInstances = 100,// Ignored for Singletons
                                                          MaxConcurrentSessions = (strEngineId == "0") ? 1000 : 1
                                                          // No of Clients with a Transport session aka connected to this Service
                                                      });
#if DEBUG
            if (strEngineId == "0")
            {
                var behavior = new ServiceMetadataBehavior
                                   {
                                       HttpGetEnabled = true,
                                       HttpGetUrl =
                                           new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) + "/Exis/FreightMetrics/WorkflowEngine/" +
                                                   "mex")
                                   };
                serviceHost.Description.Behaviors.Add(behavior);
            }
#endif

            return serviceHost;
        }

        public static void Stop()
        {
            workflowEngine.IsRequestStopEngine = true;
            while (true)
            {
                Thread.Sleep(1000);
                if (workflowEngine.IsReadyStopEngine) break;
            }
            objServiceHost.Close();
        }

     public static void Abort()
        {
            objServiceHost.Abort();
        }

        public static void ChangeDebugLevel()
        {
            var level = (string) AppDomain.CurrentDomain.GetData("LogLevel");
            SiAuto.Main.LogMessage("Changing debug level from " + SiAuto.Si.Level + " to " + level);
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), level, true);
        }

        private static string HandleArguments(string[] args)
        {
            if (args.Length < 7)
            {
                Console.WriteLine(
                    "Usage: WorkflowEngine <Installation Name> <Connection String> <LogLevel> <Port> <Workflow Engine Id> <Concurrent Calls> (<SmtpHost> <SmtpPort> <SupportAddress> <IssueTrackerAddress> <InstallationAddress>)");
                throw new ApplicationException("Invalid count of arguments.");
            }

            string InstallationName = args[0];
            ConnectionString = args[1];
            string LogLevel = args[2];
            string Port = args[3];
            EngineId = Convert.ToInt32(args[4]);
            string ConcurrentCalls = args[5];
  processNewCases = args[6] == "1";

            Console.WriteLine("Workflow Engine Starting");
            Console.WriteLine("Arguments:");
            foreach (string s in args)
            {
                Console.WriteLine("\t- " + s);
            }
            SiAuto.Si.AppName = "Workflow Engine - " + InstallationName;
            SiAuto.Si.Connections = "file(append=\"true\", filename=\"" +
                                    ServerSessionRegistry.ApplicationDirectory + "Logs\\WorkflowEngineLog_" +
                                    EngineId + "_" + InstallationName +
                                    ".sil\", maxsize=\"65536\", rotate=\"daily\", backlog=\"0\", caption=\"file\", flushon=\"error\", keepopen=\"true\", reconnect=\"false\")";
            SiAuto.Si.DefaultLevel = Level.Message;
            SiAuto.Si.Enabled = true;
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), LogLevel, true);
            SiAuto.Main.LogDebug("Logging configured successfuly");

            objServiceHost = CreateServiceHost(Port, EngineId.ToString(), Convert.ToInt32(ConcurrentCalls));
            ServerSessionRegistry.ConnectionString = ConnectionString;
            ServerSessionRegistry.InstallationPort = Convert.ToInt32(Port);
            WorkflowService.ServicePort = Port;

  if (args.Length > 7)
            {
                string smtpHost = args[7];
                string smtpPort = args[8];
                string supportAddress = args[9];
                string issueTrackerAddress = args[10];
                string installationAddress = args[11];

                if (smtpHost != "0")
                {
                    //objService.smtpClient = new SmtpClient(smtpHost, Int32.Parse(smtpPort));
                    //objService.exisSupportEmailAddress = supportAddress;
                    //objService.exisIssueTrackerEmailAddress = issueTrackerAddress;
                    //objService.installationEmailAddress = installationAddress;
                }
            }
            return Port;
        }
    }
}