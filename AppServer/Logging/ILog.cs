using System;
using System.Collections.Generic;
using Gurock.SmartInspect;

namespace AppServer.Logging
{
    public interface ILog: IEnumerable<Packet>, IDisposable
    {
    }
}