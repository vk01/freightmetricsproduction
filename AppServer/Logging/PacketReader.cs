using System;
using System.IO;
using System.Text;
using Gurock.SmartInspect;

namespace AppServer.Logging
{
    internal abstract class PacketReader
    {
        private const int DAY_OFFSET = 693593;
        private const long TICKS_PER_DAY = 864000000000L;

        protected static void ThrowException()
        {
            throw new SmartInspectException("Invalid packet format detected");
        }

        protected static string ReadString(BinaryReader reader, int size)
        {
            byte[] buffer = new byte[size];

            if (reader.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                ThrowException();
            }

            return Encoding.UTF8.GetString(buffer);
        }

/*
        protected static Color ReadColor(BinaryReader reader)
        {
            int color = reader.ReadInt32();
            int r = color & 0xff;
            int g = (color >> 8) & 0xff;
            int b = (color >> 16) & 0xff;
            int a = (color >> 24) & 0xff;
            return Color.FromArgb(a, r, g, b);
        }
*/

        protected static DateTime ReadTimestamp(BinaryReader reader)
        {
            double timestamp = reader.ReadDouble();
            timestamp += DAY_OFFSET; /* For year 0001 */
            long ticks = (long)(TICKS_PER_DAY * timestamp);
            return new DateTime(ticks);
        }

        protected static void ReadData(BinaryReader from, Stream to, 
                                       int size)
        {
            byte[] buffer = new byte[0x2000];

            while (size > 0)
            {
                int toRead = buffer.Length > size ? size : buffer.Length;

                int n = from.Read(buffer, 0, toRead);

                if (n <= 0)
                {
                    break;
                }
                to.Write(buffer, 0, n);
                size -= n;
            }

            if (size > 0)
            {
                ThrowException();
            }
        }

        public abstract Packet Read(BinaryReader reader);
    }

    internal class ControlCommandReader: PacketReader
    {
        private static bool IsValidControlCommand(int type)
        {
            return Enum.IsDefined(typeof(ControlCommandType), type);
        }

        public override Packet Read(BinaryReader reader)
        {
            ControlCommand controlCommand = new ControlCommand();

            int type = reader.ReadInt32();
            int dataSize = reader.ReadInt32();

            if (!IsValidControlCommand(type))
            {
                ThrowException();
            }

            controlCommand.Level = Level.Control;
            controlCommand.ControlCommandType =
                (ControlCommandType) type;

            if (dataSize > 0)
            {
                Stream data = new MemoryStream();
                ReadData(reader, data, dataSize);
                controlCommand.Data = data;
                controlCommand.Data.Position = 0;
            }

            return controlCommand;
        }
    }

    internal class LogEntryReader: PacketReader
    {
        private static bool IsValidLogEntry(int type)
        {
            return Enum.IsDefined(typeof(LogEntryType), type);
        }

        private static bool IsValidViewer(int id)
        {
            return Enum.IsDefined(typeof(ViewerId), id);
        }

        public override Packet Read(BinaryReader reader)
        {
            LogEntry logEntry = new LogEntry();

            int type = reader.ReadInt32();
            int id = reader.ReadInt32();

            if (!IsValidLogEntry(type) || !IsValidViewer(id))
            {
                ThrowException();
            }

            logEntry.LogEntryType = (LogEntryType) type;
            logEntry.ViewerId = (ViewerId) id;

            int appNameSize = reader.ReadInt32();
            int sessionNameSize = reader.ReadInt32();
            int titleSize = reader.ReadInt32();
            int hostNameSize = reader.ReadInt32();
            int dataSize = reader.ReadInt32();

            if (appNameSize < 0 || sessionNameSize < 0 ||
                titleSize < 0 || hostNameSize < 0 || dataSize < 0)
            {
                ThrowException();
            }

            logEntry.ProcessId = reader.ReadInt32();
            logEntry.ThreadId = reader.ReadInt32();
            logEntry.Timestamp = ReadTimestamp(reader);
//            logEntry.Color = ReadColor(reader);
            logEntry.AppName = ReadString(reader, appNameSize);
            logEntry.SessionName = ReadString(reader, sessionNameSize);
            logEntry.Title = ReadString(reader, titleSize);
            logEntry.HostName = ReadString(reader, hostNameSize);

            if (dataSize > 0)
            {
                Stream data = new MemoryStream();
                ReadData(reader, data, dataSize);
                logEntry.Data = data;
                logEntry.Data.Position = 0;
            }

            return logEntry;
        }
    }

    internal class ProcessFlowReader: PacketReader
    {
        private static bool IsValidProcessFlow(int type)
        {
            return Enum.IsDefined(typeof(ProcessFlowType), type);
        }

        public override Packet Read(BinaryReader reader)
        {
            ProcessFlow processFlow = new ProcessFlow();

            int type = reader.ReadInt32();

            if (!IsValidProcessFlow(type))
            {
                ThrowException();
            }

            int titleSize = reader.ReadInt32();
            int hostNameSize = reader.ReadInt32();

            if (titleSize < 0 || hostNameSize < 0)
            {
                ThrowException();
            }

            processFlow.ProcessFlowType = (ProcessFlowType) type;
            processFlow.ProcessId = reader.ReadInt32();
            processFlow.ThreadId = reader.ReadInt32();
            processFlow.Timestamp = ReadTimestamp(reader);
            processFlow.Title = ReadString(reader, titleSize);
            processFlow.HostName = ReadString(reader, hostNameSize);

            return processFlow;
        }
    }

    internal class WatchReader: PacketReader
    {
        private static bool IsValidWatch(int type)
        {
            return Enum.IsDefined(typeof(WatchType), type);
        }

        public override Packet Read(BinaryReader reader)
        {
            Watch watch = new Watch();

            int nameSize = reader.ReadInt32();
            int valueSize = reader.ReadInt32();

            if (nameSize < 0 || valueSize < 0)
            {
                ThrowException();
            }

            int type = reader.ReadInt32();

            if (!IsValidWatch(type))
            {
                ThrowException();
            }

            watch.WatchType = (WatchType) type;
            watch.Timestamp = ReadTimestamp(reader);
            watch.Name = ReadString(reader, nameSize);
            watch.Value = ReadString(reader, valueSize);

            return watch;
        }
    }
}