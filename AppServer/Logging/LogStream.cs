using System.Collections;
using System.Collections.Generic;
using System.IO;
using Gurock.SmartInspect;

namespace AppServer.Logging
{
    public abstract class LogStream : ILog
    {
        private readonly Stream m_Stream;

        protected LogStream(Stream stream)
        {
            m_Stream = new BufferedStream(stream);
        }

        #region ILog Members

        public IEnumerator<Packet> GetEnumerator()
        {
            Initialize(m_Stream);
            var reader = new BinaryReader(m_Stream);

            while (true)
            {
                Packet packet = null;

                try
                {
                    BeforePacket(m_Stream);
                    packet = ReadPacket(reader);
                }
                catch (EndOfStreamException)
                {
                }

                if (packet == null)
                {
                    break;
                }
                AfterPacket(m_Stream);
                yield return packet;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        protected virtual void Initialize(Stream stream)
        {
        }

        protected virtual void BeforePacket(Stream stream)
        {
        }

        private static Packet ReadPacket(BinaryReader reader)
        {
            short type = reader.ReadInt16();
            int size = reader.ReadInt32();
            return PacketFactory.GetPacket(reader, type, size);
        }

        protected virtual void AfterPacket(Stream stream)
        {
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_Stream.Close();
            }
        }
    }
}