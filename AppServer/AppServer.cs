﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml;
using AppServer.Logging;
using AppServerInterfaces;
using AppServerInterfaces.Classes;
using Exis.AppServer.Configuration;
using Exis.Domain;
using Gurock.SmartInspect;

namespace Exis.AppServer
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
    public class AppServer : MarshalByRefObject, IAppServer, IRemoteAppServer
    {
        #region Variables

        private readonly Dictionary<int, Mutex> m_WorkflowEnginesLocks = new Dictionary<int, Mutex>();
        public string DBConnectionString;

        /*
                private int m_LicensedCustomers;
                private int m_LicensedUsers;
                private LicenseThresholdEnum m_LicenseThreshold = LicenseThresholdEnum.Normal;
                private DateTime m_LicenseThresholdTimeStamp = DateTime.MinValue;

        */

        private string m_LogRouterURL;
        private LogType m_LogType;

        #endregion

        #region IAppServer Members

        public bool CheckAlive()
        {
            return true;
        }

        public void SetLicenseData(string File, int users, int Customers)
        {
            var objXmlDocument = new XmlDocument();
            objXmlDocument.LoadXml(File);
            objXmlDocument.Save("lic.xml");
            /*
                        m_LicensedUsers = users;
                        m_LicensedCustomers = Customers;
            */
        }

        public bool RegisterOnDemandCases(List<long> CaseIds)
        {
            CasesManager.RegisterOnDemandCases(CaseIds);
            return false;
        }

        public bool ProcessCustomerCases(List<long> customerIds)
        {
            var context = new DataContext(DBConnectionString);
            var caseIds = new List<long>();
            try
            {
                foreach (long customerId in customerIds)
                {
                    List<Case> cases = context.Cases.Where(a => a.CustomerId == customerId).ToList();
                    foreach (Case objCase in cases)
                    {
                        if (objCase.Status == CaseEventStatusEnum.Opened &&
                            !caseIds.Contains(objCase.Id))
                        {
                            caseIds.Add(objCase.Id);
                        }
                    }
                }
            }
            finally
            {
                context.Dispose();
            }
            return RegisterOnDemandCases(caseIds);
        }

        public bool ProcessOnDemandCase(long CaseId)
        {
            return EnginesManager.ProcessOnDemandCase(CaseId);
        }

        public bool UnregisterCase(long CaseId)
        {
            CasesManager.UnregisterCase(CaseId);
            return false;
        }

        public long? GetNextCase(long EngineId)
        {
            return CasesManager.GetNextCase(EngineId);
        }

        public void StartEngine(AbstractEngine engine)
        {
            EnginesManager.StartEngineAsync(engine);
        }

        public void StopEngine(AbstractEngine engine)
        {
            EnginesManager.StopEngineAsync(engine);
        }

        public void TerminateEngine(AbstractEngine engine)
        {
            EnginesManager.TerminateEngineAsync(engine);
        }
        public List<ExisLogEntry> GetEngineLogEntries(DateTime fromDate, DateTime toDate, AbstractEngine engine)
        {
            var logEntries = new List<ExisLogEntry>();
            try
            {
                string searchPattern;
                switch (engine.EngineType)
                {
                    case EngineTypeEnum.WorkflowEngine:
                        searchPattern = "WorkflowEngineLog_" + engine.EngineId + "_" + engine.Installation.Name +
                                        "-*.sil";
                        break;
                    case EngineTypeEnum.JobsEngine:
                        searchPattern = "JobsEngineLog_" + engine.Installation.Name + "-*.sil";
                        break;
                    case EngineTypeEnum.InterfaceEngine:
                        searchPattern = "InterfaceEngineLog_" + engine.Installation.Name + "-*.sil";
                        break;
                    default:
                        searchPattern = "";
                        break;
                }
                if (String.IsNullOrEmpty(searchPattern)) return logEntries;

                string[] logFileList =
                    Directory.GetFiles(Thread.GetDomain().SetupInformation.ApplicationBase + @"\Logs\",
                                       searchPattern, SearchOption.AllDirectories);
                logEntries = GetLogEntriesByPeriod(fromDate, toDate, logFileList);
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException(e);
            }
            return logEntries;
        }

        public List<ExisLogEntry> GetServiceLogEntries(DateTime fromDate, DateTime toDate)
        {
            var logEntries = new List<ExisLogEntry>();
            try
            {
                const string searchPattern = "MonitorLog-*.sil";
                string[] logFileList =
                    Directory.GetFiles(Thread.GetDomain().SetupInformation.ApplicationBase + @"\Logs\",
                                       searchPattern, SearchOption.AllDirectories);
                logEntries = GetLogEntriesByPeriod(fromDate, toDate, logFileList);
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException("Exception occurred during GetServiceLogEntries", e);
            }
            return logEntries;
        }

        public void ChangeEngineDebugLevel(AbstractEngine engine, string level)
        {
            /*
                        try
                        {
                            if (!CheckEngine(engine))
                                return;
                            AppDomain objAppDomain = GetEngineDomainPair(engine).Value;
                            objAppDomain.SetData("LogLevel", level);
                            switch (engine.EngineType)
                            {
                                case EngineTypeEnum.WorkflowEngine:
                                    objAppDomain.DoCallBack(Program.ChangeDebugLevel);
                                    break;
                                case EngineTypeEnum.CasesGenerationEngine:
                                    objAppDomain.DoCallBack(CRMWorkflowCasesEngineConsole.Program.ChangeDebugLevel);
                                    break;
                                case EngineTypeEnum.InterfaceEngine:
                                    objAppDomain.DoCallBack(CRMInterfaceEngineConsole.Program.ChangeDebugLevel);
                                    break;
                                case EngineTypeEnum.CommissionEngine:
                                    objAppDomain.DoCallBack(CRMCommissionsServiceConsole.Program.ChangeDebugLevel);
                                    break;
                            }
                            GetEngine(engine).LogLevel = level;
                            ConfigurationHelper.GetInstance().SaveConfiguration();
                        }
                        catch (Exception e)
                        {
                            SiAuto.Main.LogException(e);
                        }
            */
            EnginesManager.ChangeEngineDebugLevel(engine, level);
        }

        public Installation GetInstallation()
        {
            return ConfigurationHelper.GetInstance().Installation;
        }

        public bool AddEngine(AbstractEngine engine)
        {
            SiAuto.Main.LogMessage("Adding engine: " + engine);
            try
            {
                if (ConfigurationHelper.GetInstance().Installation != null)
                {
                    ConfigurationHelper.GetInstance().Installation.AddEngine(engine);
                    ConfigurationHelper.GetInstance().SaveConfiguration();
                    return true;
                }
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException(e);
            }
            return false;
        }

       public bool EditEngine(AbstractEngine engine)
        {
            SiAuto.Main.LogMessage("Editing engine: " + engine);
            try
            {
                if (ConfigurationHelper.GetInstance().Installation != null)
                {
                    ConfigurationHelper.GetInstance().Installation.EditEngine(engine);
                    ConfigurationHelper.GetInstance().SaveConfiguration();
                    return true;
                }
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException(e);
            }
            return false;
        }
        public bool RemoveEngine(AbstractEngine engine)
        {
            SiAuto.Main.LogMessage("Removing engine: " + engine);
            try
            {
                if (ConfigurationHelper.GetInstance().Installation.Name == engine.Installation.Name)
                {
                    ConfigurationHelper.GetInstance().Installation.RemoveEngine(engine);
                    ConfigurationHelper.GetInstance().SaveConfiguration();
                    return true;
                }
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException(e);
            }
            return false;
        }

        public void LogClientException(ClientType clientType, string userName, string message, Exception e)
        {
            if (userName == null)
            {
                AppService.GetClientSiByType(clientType).GetSession("Main").LogException(
                    "Unidentified client threw exception with message: '" + message + "'", e);
            }
            else
            {
                Session logSession = AppService.GetClientSiByType(clientType).GetSession(userName) ??
                                     AppService.GetClientSiByType(clientType).AddSession(userName);
                logSession.LogException("User '" + userName + "' sent exception with message: '" + message + "'", e);
            }
        }

        public void LogClientError(ClientType clientType, string userName, string message)
        {
            if (userName == null)
            {
                AppService.GetClientSiByType(clientType).GetSession("Main").LogError(
                    "Unidentified client threw exception with message: '" + message + "'");
            }
            else
            {
                Session logSession = AppService.GetClientSiByType(clientType).GetSession(userName) ??
                                     AppService.GetClientSiByType(clientType).AddSession(userName);
                logSession.LogError("User '{0}' sent error with message: {1}", userName, message);
            }
        }

        public string GetLogRouterURL()
        {
            return m_LogRouterURL;
        }

        public LogType GetInstallationLogType(Installation installation)
        {
            return m_LogType;
        }

        public List<HostPortPair> GetInstallations()
        {
            return InstallationsManager.Installations;
        }

        #endregion

        #region Internal Properties

        /*
        internal int LicensedUsers
        {
            get { return m_LicensedUsers; }
        }

        internal int LicensedCustomers
        {
            get { return m_LicensedCustomers; }
        }

        internal LicenseThresholdEnum LicenseThreshold
        {
            get { return m_LicenseThreshold; }
            set { m_LicenseThreshold = value; }
        }

        internal DateTime LicenseThresholdTimeStamp
        {
            get { return m_LicenseThresholdTimeStamp; }
            set { m_LicenseThresholdTimeStamp = value; }
        }

        internal string LogRouterURL
        {
            get { return m_LogRouterURL; }
            set { m_LogRouterURL = value; }
        }

        internal LogType LogType
        {
            set { m_LogType = value; }
        }
*/

        #endregion

        #region IRemoteAppServer Members

        public Mutex GetWorkflowEngineLock(int EngineId)
        {
            if (!m_WorkflowEnginesLocks.ContainsKey(EngineId)) m_WorkflowEnginesLocks[EngineId] = new Mutex(false);
            return m_WorkflowEnginesLocks[EngineId];
        }

        #endregion

        private List<ExisLogEntry> GetLogEntriesByPeriod(DateTime fromDate, DateTime toDate, string[] fileList)
        {
            var fileDates = new SortedList<DateTime, string>();
            var filesToParse = new List<string>();
            var logEntries = new List<ExisLogEntry>();

            foreach (string file in fileList)
            {
                try
                {
                    DateTime fileDate = DateTime.ParseExact(file.Substring(file.LastIndexOf(".sil") - 19, 19),
                                                            "yyyy-MM-dd-HH-mm-ss", null);
                    fileDates.Add(fileDate, file);
                }
                catch (Exception e)
                {
                    SiAuto.Main.LogException(e);
                }
            }
            var filePeriods = new List<FilePeriod>();
            FilePeriod lastFilePeriod = null;
            foreach (var fileDate in fileDates)
            {
                if (lastFilePeriod != null)
                {
                    lastFilePeriod.FileDateTo = fileDate.Key;
                }
                lastFilePeriod = new FilePeriod(fileDate.Value, fileDate.Key, DateTime.MaxValue);
                filePeriods.Add(lastFilePeriod);
            }

            foreach (FilePeriod filePeriod in filePeriods)
            {
                if (fromDate < filePeriod.FileDateFrom)
                {
                    if (toDate >= filePeriod.FileDateFrom)
                        filesToParse.Add(filePeriod.Filename);
                }
                if (fromDate >= filePeriod.FileDateFrom && fromDate < filePeriod.FileDateTo)
                {
                    if (toDate > fromDate)
                        filesToParse.Add(filePeriod.Filename);
                }
            }
            foreach (string file in filesToParse)
            {
                using (ILog log = new LogFile(File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    foreach (Packet packet in log)
                    {
                        switch (packet.PacketType)
                        {
                            case PacketType.LogEntry:
                                var entry = (LogEntry) packet;
                                if (entry.Timestamp >= fromDate &&
                                    (toDate == DateTime.MinValue || entry.Timestamp <= toDate) &&
                                    entry.LogEntryType != LogEntryType.Separator)
                                    logEntries.Add(new ExisLogEntry(entry));
                                break;
                        }
                    }
                }
            }

            return logEntries;
        }
    }

    #region Internal Classes

    internal class FilePeriod
    {
        public FilePeriod(string filename, DateTime fileDateFrom, DateTime fileDateTo)
        {
            Filename = filename;
            FileDateFrom = fileDateFrom;
            FileDateTo = fileDateTo;
        }

        public string Filename { get; set; }

        public DateTime FileDateFrom { get; set; }

        public DateTime FileDateTo { get; set; }
    }

    #endregion
}