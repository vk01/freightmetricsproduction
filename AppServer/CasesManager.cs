﻿using System.Collections.Generic;
using System.Linq;
using AppServerInterfaces.Classes;
using Exis.Domain;
using Gurock.SmartInspect;

namespace Exis.AppServer
{
    internal static class CasesManager
    {
        private static readonly Dictionary<long, List<long>> m_RegisteredCases = new Dictionary<long, List<long>>();

        internal static void RegisterWorkflowEngine(AbstractEngine engine)
        {
            lock (m_RegisteredCases)
            {
                SiAuto.Main.LogDebug("Registering workflow engine slot for engine {0}.", engine.ToString());
                m_RegisteredCases[engine.EngineId] = new List<long>();
            }
        }

        internal static void UnregisterWorkflowEngine(AbstractEngine engine)
        {
            lock (m_RegisteredCases)
            {
                if (m_RegisteredCases.ContainsKey(engine.EngineId))
                {
                    SiAuto.Main.LogDebug("Unregistering workflow engine slot for engine {0}.", engine.ToString());
                    m_RegisteredCases.Remove(engine.EngineId);
                }
            }
        }

        internal static void RegisterOnDemandCases(List<long> caseIds)
        {
            lock (m_RegisteredCases)
            {
                if (m_RegisteredCases.ContainsKey(0))
                {
                    List<long> alreadyRegisteredCases = m_RegisteredCases[0];
                    foreach (long caseId in caseIds)
                    {
                        if (alreadyRegisteredCases.Contains(caseId))
                        {
                            SiAuto.Main.LogDebug("Case with Id " + caseId + " is already registered");
                        }
                        else
                        {
                            alreadyRegisteredCases.Add(caseId);
                            SiAuto.Main.LogDebug("Case with Id " + caseId + " registered");
                        }
                    }
                }
            }
        }

        internal static long? GetNextCase(long engineId)
        {
            lock (m_RegisteredCases)
            {
                List<long> objWorkflowCases;
                if (!m_RegisteredCases.TryGetValue(engineId, out objWorkflowCases))
                    return null;

                if (objWorkflowCases.Count > 0)
                {
                    SiAuto.Main.LogDebug("GetNextCase: " + objWorkflowCases[0]);
                    return objWorkflowCases[0];
                }
            }
            return null;
        }

        internal static void UnregisterCase(long caseId)
        {
            lock (m_RegisteredCases)
            {
                foreach (var keyValuePair in m_RegisteredCases)
                {
                    List<long> objWorkflowCases = keyValuePair.Value;
                    if (objWorkflowCases.Contains(caseId))
                    {
                        SiAuto.Main.LogDebug("Unregister case {0} from engine {1}", caseId, keyValuePair.Key);
                        objWorkflowCases.Remove(caseId);
                    }
                }
            }
        }

        internal static void RefreshWorkflows(DataContext dataContext)
        {
            lock (m_RegisteredCases)
            {
                foreach (KeyValuePair<long, List<long>> pair in m_RegisteredCases)
                {
                    List<long> objWorkflowCases = pair.Value;
                    long lngEngineId = pair.Key;

                    if (lngEngineId == 0) continue;
                    if (objWorkflowCases.Count == 0)
                    {
                        var cases = (from objCase in dataContext.Cases
                                     join objCaseTypeWFEngineAssoc in dataContext.CaseTypesWfEngines on objCase.CaseTypeId
                                         equals objCaseTypeWFEngineAssoc.CaseTypeId
                                     where
                                         objCase.Status_Str == CaseEventStatusEnum.Opened.ToString("d") &&
                                         objCaseTypeWFEngineAssoc.WorkflowEngineId == lngEngineId &&
                                         (from objEvent in dataContext.Events
                                          where objEvent.CaseId == objCase.Id
                                          select objEvent).Count() > 0
                                     select objCase).ToList();

                        foreach (Case Case in cases)
                        {
                            objWorkflowCases.Add(Case.Id);
                        }
                    }
                }
            }
        }
    }
}