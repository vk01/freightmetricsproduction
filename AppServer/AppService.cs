﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Threading;
using System.Xml;
using AppServerInterfaces;
using AppServerInterfaces.Classes;
using Exis.AppServer.Configuration;
using Exis.Domain;
using Exis.SessionRegistry;
using Exis.WCFExtensions;
using Gurock.SmartInspect;
using ProtocolException = Gurock.SmartInspect.ProtocolException;

namespace Exis.AppServer
{
    public class AppService
    {
        private static SmartInspect winClientSi;
        private static SmartInspect webClientSi;
        private static SmartInspect adminClientSi;

        private static string ConnectionString;
        private AppServer appServer;
        private ServiceHost appServerHost;

        private bool m_StopRequest;
        private int maxRetries;
        private Dictionary<string, int> retryCounter;
        private int retryDelay;

        public AppService()
        {
            LoadLogConfiguration();
        }

        public void Stop()
        {
            m_StopRequest = true;
            EnginesManager.Stop(true);
        }

        internal static SmartInspect GetClientSiByType(ClientType clientType)
        {
            switch (clientType)
            {
                case ClientType.WinClient:
                    return winClientSi;
                case ClientType.WebClient:
                    return webClientSi;
                case ClientType.AdminClient:
                    return adminClientSi;
            }
            return null;
        }

        private void LoadLogConfiguration()
        {
            SiAuto.Si.LoadConfiguration(@"Logs\AppServerLogConfig.sic");

            var builder = new ConnectionsBuilder();
            builder.BeginProtocol("pipe");
            builder.AddOption("pipename", "sirouter");
            builder.AddOption("reconnect", true);
            builder.AddOption("reconnect.interval", "10s");
            builder.EndProtocol();

            winClientSi = new SmartInspect("Win Client");
            winClientSi.Enabled = true;
            winClientSi.Connections = builder.Connections;
            winClientSi.Error += LogErrorHandler;
            winClientSi.Level = Level.Message;
            winClientSi.DefaultLevel = Level.Message;

            webClientSi = new SmartInspect("Web Client");
            webClientSi.Enabled = true;
            webClientSi.Connections = builder.Connections;
            webClientSi.Error += LogErrorHandler;
            webClientSi.Level = Level.Message;
            webClientSi.DefaultLevel = Level.Message;

            adminClientSi = new SmartInspect("Admin Client");
            adminClientSi.Enabled = true;
            adminClientSi.Connections = builder.Connections;
            adminClientSi.Error += LogErrorHandler;
            adminClientSi.Level = Level.Message;
            adminClientSi.DefaultLevel = Level.Message;
#if DEBUG
            winClientSi.Level = Level.Debug;
            webClientSi.Level = Level.Debug;
            adminClientSi.Level = Level.Debug;
#endif
        }

        private void LogErrorHandler(object sender, ErrorEventArgs e)
        {
            var si = (SmartInspect) sender;
            if (e.Exception is ProtocolException)
            {
                if (si.AppName.StartsWith("Win"))
                {
                    si.LoadConfiguration(@"Logs\WinLogConfig.sic");
                }
                if (si.AppName.StartsWith("Web"))
                {
                    si.LoadConfiguration(@"Logs\WebLogConfig.sic");
                }
                if (si.AppName.StartsWith("Admin"))
                {
                    si.LoadConfiguration(@"Logs\AdminLogConfig.sic");
                }
            }
        }

        /*
                private void RefreshUserTasks(string User)
                {
                    DateTime currentDate = DB.GetServerDate();
                    User objUser = ((UserDM)SessionRegistry.getMapper(typeof(User))).GetUserByLogin(User);
                    string segmentation = "customers";
                    if(objUser.Segmentation != null)
                    {
                        segmentation = objUser.Segmentation.ViewName;
                    }
                    DomainObjectCollection objUserTasks =
                        ((AllJobDM)SessionRegistry.getMapper(typeof(AllJob))).GetAllJobsByUserId(objUser.Id, segmentation);
                    List<AllJob> objAllJobs = new List<AllJob>();
                    foreach (AllJob task in objUserTasks)
                    {
                        if (task.EventDatf.Date <= currentDate.Date) objAllJobs.Add(task);
                    }

                    objRemoteMonitor.UserTasks[User] = objAllJobs;
                    objRemoteMonitor.UserTaskPointers[User] = 0;
                    objRemoteMonitor.UserTaskUpdateDates[User] = DateTime.Now;
                }

        */

        public void Initialize()
        {
            SiAuto.Main.EnterMethod(Level.Message, "Initializing App Server");

            try
            {
                SiAuto.Main.EnterMethod(Level.Message, "Initialize listener");
                try
                {
                    appServer = new AppServer();
                    appServerHost = new ServiceHost(appServer);
                }
                finally
                {
                    SiAuto.Main.LeaveMethod(Level.Message, "Initialize listener");
                }
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException(e);
            }
            finally
            {
                SiAuto.Main.LeaveMethod(Level.Message, "Initializing App Server");
            }
        }

        private bool LoadConfiguration()
        {
            SiAuto.Main.LogMessage("Loading configuration...");
            try
            {
                ConfigurationHelper.GetInstance().LoadConfiguration();
                var installation = ConfigurationHelper.GetInstance().Installation;
               if (installation == null)
                {
                    SiAuto.Main.LogMessage("No installations found. Could not initialize connection to database");
                    return false;
                }
                SiAuto.Main.LogMessage("Installation found: " + installation.Name +". Initializing connection to database.");
                SiAuto.Main.LogDebug("Connection string: {0}", installation.DBSettings.GetConnectionString());
                ServerSessionRegistry.ApplicationDirectory = Thread.GetDomain().SetupInformation.ApplicationBase;
                ServerSessionRegistry.User = "AppServer";
                ConnectionString = installation.DBSettings.GetConnectionString();
                    var dataContext = new DataContext(ConnectionString);
                    try
                    {
                        dataContext.GetNextId(typeof (Case));
                    }
                    catch (Exception e)
                    {
                        SiAuto.Main.LogException("Could not connect to database", e);
                        return false;
                    }
                    finally
                    {
                        dataContext.Dispose();
                    }
                SiAuto.Main.LogMessage("Initializing listener...");

                appServer = new AppServer();
                appServerHost = new ServiceHost(appServer);
                appServer.DBConnectionString = installation.DBSettings.GetConnectionString();

                SiAuto.Main.LogDebug("Initializing App Server endpoints...");
                    string appServerPipeName = "AppServerPipe";
                    if (ConfigurationManager.AppSettings["PipeName"] != null)
                    {
                        try
                        {
                            appServerPipeName = ConfigurationManager.AppSettings["PipeName"];
                        }
                        catch
                        {
                        }
                    }

                    var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
                    binding.MaxReceivedMessageSize = Int32.MaxValue;
                    binding.MaxBufferSize = Int32.MaxValue;
                    binding.MaxBufferPoolSize = Int32.MaxValue;
                    binding.ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
                    appServerHost.AddServiceEndpoint(typeof (IAppServer), binding,
                                                     "net.pipe://localhost/Exis/FreightMetrics/" + appServerPipeName + "/" + installation.Port);

                    var netTcpBinding = new NetTcpBinding(SecurityMode.None)
                                            {
                                                MaxReceivedMessageSize = Int32.MaxValue,
                                                OpenTimeout = TimeSpan.MaxValue,
                                                CloseTimeout = TimeSpan.MaxValue,
                                                ReceiveTimeout = TimeSpan.MaxValue,
                                                SendTimeout = TimeSpan.MaxValue,
                                                PortSharingEnabled = true,
                                                ListenBacklog = 100,
                                                ReaderQuotas =
                                                    new XmlDictionaryReaderQuotas {MaxStringContentLength = 10000000}
                                            };
                    netTcpBinding.ReliableSession.Enabled = true;
                    netTcpBinding.ReliableSession.Ordered = true;
                    netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromDays(1);
                    appServerHost.AddServiceEndpoint(typeof (IAppServer), netTcpBinding,
                                                     new Uri("net.tcp://localhost:" + installation.Port +
                                                             "/Exis/FreightMetrics/AppServer"));
                    appServerHost.Open();

                SiAuto.Main.LogMessage("Listener Initialized.");


                maxRetries = 3;
                if (ConfigurationManager.AppSettings["retries"] != null)
                {
                    try
                    {
                        maxRetries = Convert.ToInt32(ConfigurationManager.AppSettings["retries"]);
                    }
                    catch
                    {
                    }
                }

                retryDelay = 5000;
                if (ConfigurationManager.AppSettings["retryDelay"] != null)
                {
                    try
                    {
                        retryDelay = Convert.ToInt32(ConfigurationManager.AppSettings["retryDelay"]);
                    }
                    catch (Exception)
                    {
                    }
                }

                retryCounter = new Dictionary<string, int>();

                var proxyHosts = new List<HostPortPair>();
                if (ConfigurationManager.AppSettings["ProxyHosts"] != null)
                {
                    string[] proxyHostTokens = ConfigurationManager.AppSettings["ProxyHosts"].Split(',');
                    foreach (string proxyHostToken in proxyHostTokens)
                    {
                        proxyHosts.Add(new HostPortPair(proxyHostToken.Split(':')[0], proxyHostToken.Split(':')[1]));
                    }
                }

                InstallationsManager.AddInstallations(proxyHosts);
                // CreateRoutingService(installation.Port, proxyHosts);

                SiAuto.Main.LogMessage("Configuration loaded successfuly.");
            }
            catch(Exception e)
            {
                SiAuto.Main.LogException("Exception during configuration loading.", e);
                return false;
            }
            return true;
        }

        public void Run()
        {
            SiAuto.Main.LogMessage("Initializing App Server...");
            try
            {
                while (!LoadConfiguration() && !m_StopRequest)
                {
                    try
                    {
                        Thread.Sleep(10000);
                    }
                    catch (ThreadInterruptedException)
                    {
                    }
                }


                if (m_StopRequest) return;

                SiAuto.Main.LogMessage("Starting Threads");
                m_StopRequest = false;
                try
                {
                    EnginesManager.Start();
                    SiAuto.Main.LogMessage("Threads started.");
                }
                catch (Exception e)
                {
                    SiAuto.Main.LogException(e);
    return;
                }

                SiAuto.Main.LogMessage("App Server Initialized");
                CheckThreads();
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException("Main thread exception", e);
            }
            finally
            {
                SiAuto.Main.LogMessage("Service stop requested. Exiting main monitor thread");
            }
        }

        private void CheckThreads()
        {
            bool enginesThreadShouldRetryRestart = true;

            while (!m_StopRequest)
            {
                try
                {
                    if (!EnginesManager.IsRunning())
                    {
                        if (enginesThreadShouldRetryRestart)
                        {
                            SiAuto.Main.LogError("Engines thread was stopped unexpectedly.");
                            int retries;
                            if (!retryCounter.TryGetValue("EnginesThread", out retries))
                            {
                                retries = 0;
                            }
                            retryCounter["EnginesThread"] = ++retries;

                            if (retries <= maxRetries)
                            {
                                SiAuto.Main.LogMessage(
                                    "Restarting engines thread. Will check if it was started on next iteration. Retry: {0}, Delay: {1}",
                                    retries, retryDelay);
                                Thread.Sleep(retryDelay);
                                EnginesManager.Start();
                            }
                            else
                            {
                                enginesThreadShouldRetryRestart = false;
                                SiAuto.Main.LogError(
                                    "Engines thread could not be successfuly restarted. Stopping retries.");
                            }
                        }
                    }
                    else
                    {
                        retryCounter["EnginesThread"] = 0;
                    }

                    Thread.Sleep(30000);
                }
                catch (ThreadInterruptedException)
                {
                }
            }
        }
    }

    [DataContract]
    public class PartialActionMessageFilter : MessageFilter
    {
        private readonly string m_FilterData;

        public PartialActionMessageFilter(string filterData)
        {
            if (String.IsNullOrEmpty(filterData)) throw new ArgumentNullException("filterData");
            m_FilterData = filterData;
        }

        public override bool Match(MessageBuffer buffer)
        {
            Message message = buffer.CreateMessage();
            if (message.Headers.Action.Contains(m_FilterData)) return true;
            return false;
        }

        public override bool Match(Message message)
        {
            if (message.Headers.Action.Contains(m_FilterData)) return true;
            return false;
        }
    }

    [DataContract]
    public class VersionNumberMessageFilter : MessageFilter
    {
        private readonly string m_Version;

        public VersionNumberMessageFilter(string version)
        {
            if (String.IsNullOrEmpty(version)) throw new ArgumentNullException("version");
            m_Version = version;
        }

        public override bool Match(MessageBuffer buffer)
        {
            Message message = buffer.CreateMessage();
            //GenericContext<ProgramInfo> context = message.Headers.GetHeader<GenericContext<ProgramInfo>>("GenericContext",
            //                                                          "net.clr:" + typeof(ProgramInfo).FullName);
            ProgramInfo programInfo = GenericContext<ProgramInfo>.Current.Value;
            return false;
        }

        public override bool Match(Message message)
        {
            //GenericContext<ProgramInfo> context = message.Headers.GetHeader<GenericContext<ProgramInfo>>("GenericContext",
            //                                                          "net.clr:" + typeof(ProgramInfo).FullName);
            ProgramInfo programInfo = GenericContext<ProgramInfo>.Current.Value;
            if (programInfo.Version == m_Version) return true;
            return false;
        }
    }
}