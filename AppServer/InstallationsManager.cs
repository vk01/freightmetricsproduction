﻿using System.Collections.Generic;
using AppServerInterfaces.Classes;

namespace Exis.AppServer
{
    internal static class InstallationsManager
    {
        private static readonly List<HostPortPair> _Installations = new List<HostPortPair>();

        public static List<HostPortPair> Installations
        {
            get { return _Installations; }
        }

        internal static void AddInstallation(HostPortPair hostPortPair)
        {
            if (_Installations.Contains(hostPortPair))
                return;

            lock (_Installations)
            {
                try
                {
                }
                catch
                {
                }
                _Installations.Add(hostPortPair);
            }
        }

        internal static void AddInstallations(IEnumerable<HostPortPair> hostPortPairs)
        {
            foreach (var hostPortPair in hostPortPairs)
            {
                AddInstallation(hostPortPair);
            }
        }
    }
}