using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class SmtpSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("SmtpHost",
            IsRequired = true, IsKey = false)]
        public string SmtpHost
        {
            get { return (string)this["SmtpHost"]; }
            set { this["SmtpHost"] = value; }
        }

        [ConfigurationProperty("SmtpPort",
            IsRequired = true, IsKey = false)]
        public string SmtpPort
        {
            get { return (string)this["SmtpPort"]; }
            set { this["SmtpPort"] = value; }
        }

        [ConfigurationProperty("SupportEmailAddress",
            IsRequired = true, IsKey = false)]
        public string SupportEmailAddress
        {
            get { return (string)this["SupportEmailAddress"]; }
            set { this["SupportEmailAddress"] = value; }
        }

        [ConfigurationProperty("IssueTrackerEmailAddress",
            IsRequired = true, IsKey = false)]
        public string IssueTrackerEmailAddress
        {
            get { return (string)this["IssueTrackerEmailAddress"]; }
            set { this["IssueTrackerEmailAddress"] = value; }
        }

        [ConfigurationProperty("InstallationEmailAddress",
            IsRequired = true, IsKey = false)]
        public string InstallationEmailAddress
        {
            get { return (string)this["InstallationEmailAddress"]; }
            set { this["InstallationEmailAddress"] = value; }
        }
    }
}