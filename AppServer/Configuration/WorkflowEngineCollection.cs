using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class WorkflowEngineCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new WorkflowEngineElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            WorkflowEngineElement e = (WorkflowEngineElement) element;
            return e.EngineId;
        }

        public WorkflowEngineElement this[object EngineId]
        {
            get
            {
                return (WorkflowEngineElement) BaseGet(EngineId);
            }
            set
            {
                // If it exists, remove first, then add
                if (BaseGet(EngineId) != null)
                    BaseRemove(EngineId);

                BaseAdd(value);
            }
        }

        public void Add(WorkflowEngineElement element)
        {
            BaseAdd(element);
        }
    }
}