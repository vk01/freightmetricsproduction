using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class HostListElement : ConfigurationElement
    {
        [ConfigurationProperty("Host",
            IsRequired = true, IsKey = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        [ConfigurationProperty("Port",
            IsRequired = true, IsKey = false)]
        public string Port
        {
            get { return (string)this["Port"]; }
            set { this["Port"] = value; }
        }
    }
}