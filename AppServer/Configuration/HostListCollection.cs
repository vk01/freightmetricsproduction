using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class HostListCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new HostListElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            HostListElement e = (HostListElement) element;
            return e.Host + ":" + e.Port;
        }

        public HostListElement this[int index]
        {
            get
            {
                return (HostListElement)BaseGet(index);
            }
            set
            {
                // If it exists, remove first, then add
                if (BaseGet(index) != null)
                    BaseRemove(index);

                BaseAdd(value);
            }
        }

        new public HostListElement this[string host]
        {
            get
            {
                return (HostListElement) BaseGet(host);
            }
            set
            {
                // If it exists, remove first, then add
                if (BaseGet(host) != null)
                    BaseRemove(host);

                BaseAdd(value);
            }
        }

        public void Add(HostListElement element)
        {
            BaseAdd(element);
        }
    }
}