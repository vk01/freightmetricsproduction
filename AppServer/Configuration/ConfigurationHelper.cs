﻿using System;
using System.Collections.Generic;
using System.Configuration;
using AppServerInterfaces.Classes;

namespace Exis.AppServer.Configuration
{
    public class ConfigurationHelper
    {
        private static ConfigurationHelper instance;

        private Installation m_Installation;

   private object lockObject = new object();

        private ConfigurationHelper()
        {
        }

        public static ConfigurationHelper GetInstance()
        {
            if (instance == null)
                instance = new ConfigurationHelper();
            return instance;
        }

  public Installation Installation
        {
            get { return m_Installation; }
        }

        public void LoadConfiguration()
        {
            // Get the current configuration file.
            System.Configuration.Configuration config =
                ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None);

            ConfigurationSectionGroup userSettings = config.SectionGroups["userSettings"];
            InstallationSection installationSection;

            if (userSettings != null)
            {
                installationSection = (InstallationSection) userSettings.Sections.Get("Installation");
            }
            else
            {
                installationSection =
                    (InstallationSection) config.Sections.Get("Installation");
            }

            if (installationSection == null)
            {
                return;
            }

            List<HostPortPair> hostList = new List<HostPortPair>();
            if (installationSection.DBSettings.HostList.ElementInformation.IsPresent)
            {
                foreach (HostListElement element in installationSection.DBSettings.HostList)
                {
                    HostPortPair hostPortPair = new HostPortPair { Host = element.Host, Port = element.Port };
                    hostList.Add(hostPortPair);
                }
            }
            DBSettings dbSettings = new DBSettings(installationSection.DBSettings.User,
                                                   installationSection.DBSettings.Pass,
                                                   installationSection.DBSettings.Service, hostList,
                                                   installationSection.DBSettings.ExtraOptions);
            Installation installation = new Installation(installationSection.Name, installationSection.BaseFolder,
                                                         dbSettings,
                                                         (LogType)
                                                         Enum.Parse(typeof (LogType), installationSection.LogType),
                                                         installationSection.Port, installationSection.ConcurrentCalls,
                                                         installationSection.IsRunning,
                                                         installationSection.ProcessNewCases);
            if (installationSection.SmtpSettings.ElementInformation.IsPresent)
            {
                SmtpSettingsElement smtpSettingsElement = installationSection.SmtpSettings;
                SmtpSettings smtpSettings = new SmtpSettings(smtpSettingsElement.SmtpHost,
                                                             smtpSettingsElement.SmtpPort,
                                                             smtpSettingsElement.SupportEmailAddress,
                                                             smtpSettingsElement.IssueTrackerEmailAddress,
                                                             smtpSettingsElement.InstallationEmailAddress);
                installation.SmtpSettings = smtpSettings;
            }

            AbstractEngine engine;
            if (installationSection.JobsEngine.ElementInformation.IsPresent)
            {
                engine = new JobsEngine(1, installationSection.JobsEngine.LogLevel,
                                                   installationSection.JobsEngine.IsRunning, installation);
                installation.AddEngine(engine);
            }
            if (installationSection.InterfaceEngine.ElementInformation.IsPresent)
            {
                engine = new InterfaceEngine(1, installationSection.InterfaceEngine.LogLevel,
                                                installationSection.InterfaceEngine.IsRunning, installation);
                installation.AddEngine(engine);
            }
            if (installationSection.WorkflowEngines.ElementInformation.IsPresent)
            {
                foreach (WorkflowEngineElement workflowEngine in installationSection.WorkflowEngines)
                {
                    engine = new WorkflowEngine(workflowEngine.EngineId, workflowEngine.LogLevel,
                                                workflowEngine.IsRunning, installation, workflowEngine.ConcurrentCalls);
                    installation.AddEngine(engine);
                }
            }
            if (installationSection.ClientsEngine.ElementInformation.IsPresent)
            {
                engine = new ClientsEngine(installationSection.ClientsEngine.EngineId,
                                           installationSection.ClientsEngine.LogLevel,
                                           installationSection.ClientsEngine.IsRunning, installation);
                installation.AddEngine(engine);
            }
            if (installationSection.ReportsEngine.ElementInformation.IsPresent)
            {
                engine = new ReportsEngine(installationSection.ReportsEngine.EngineId,
                                           installationSection.ReportsEngine.LogLevel,
                                           installationSection.ReportsEngine.IsRunning, installation);
                installation.AddEngine(engine);
            }
            m_Installation = installation;
        }

        public void SaveConfiguration()
        {
            lock (lockObject)
            {
                // Get the current configuration file.
                System.Configuration.Configuration config =
                    ConfigurationManager.OpenExeConfiguration(
                        ConfigurationUserLevel.None);

                ConfigurationSectionGroup userSettings = config.SectionGroups["userSettings"];
                InstallationSection installationSection;

                if (userSettings != null)
                {
                    installationSection =
                        (InstallationSection)userSettings.Sections.Get("Installation");
                    if (installationSection != null)
                    {
                        userSettings.Sections.Remove("Installation");
                    }
                }
                else
                {
                    installationSection =
                        (InstallationSection)config.Sections.Get("Installation");
                    if (installationSection != null)
                    {
                        config.Sections.Remove("Installation");
                    }
                }

                installationSection = new InstallationSection();
                installationSection.SectionInformation.ForceSave = true;
                if (userSettings != null)
                    userSettings.Sections.Add("Installation", installationSection);
                else
                    config.Sections.Add("Installation", installationSection);

                installationSection.Name = m_Installation.Name;
                installationSection.BaseFolder = m_Installation.BaseFolder;
                installationSection.DBSettings.User = m_Installation.DBSettings.User;
                installationSection.DBSettings.Pass = m_Installation.DBSettings.Pass;
                installationSection.DBSettings.Service = m_Installation.DBSettings.Service;
                installationSection.DBSettings.ExtraOptions = m_Installation.DBSettings.ExtraOptions;
                installationSection.LogType = m_Installation.LogType.ToString("g");
                installationSection.Port = m_Installation.Port;
                installationSection.ConcurrentCalls = m_Installation.ConcurrentCalls;
       installationSection.IsRunning = m_Installation.IsRunning;
                installationSection.ProcessNewCases = m_Installation.ProcessNewCases;

                if (m_Installation.DBSettings.HostList.Count > 0)
                    foreach (HostPortPair host in m_Installation.DBSettings.HostList)
                    {
                        HostListElement hle = new HostListElement { Host = host.Host, Port = host.Port };
                        installationSection.DBSettings.HostList.Add(hle);
                    }
                else
                {
                    installationSection.DBSettings.HostList = null;
                }
                if (installationSection.SmtpSettings != null)
                {
                    SmtpSettingsElement smtpSettingsElement = new SmtpSettingsElement();
                    smtpSettingsElement.SmtpHost = m_Installation.SmtpSettings.SmtpHost;
                    smtpSettingsElement.SmtpPort = m_Installation.SmtpSettings.SmtpPort;
                    smtpSettingsElement.SupportEmailAddress = m_Installation.SmtpSettings.SupportEmailAddress;
                    smtpSettingsElement.IssueTrackerEmailAddress =
                        m_Installation.SmtpSettings.IssueTrackerEmailAddress;
                    smtpSettingsElement.InstallationEmailAddress =
                        m_Installation.SmtpSettings.InstallationEmailAddress;
                    installationSection.SmtpSettings = smtpSettingsElement;
                }
                installationSection.JobsEngine = m_Installation.HasJobsEngine
                                                                ? new JobsEngineElement
                                                                      {
                                                                          LogLevel =
                                                                              m_Installation.JobsEngine.
                                                                              LogLevel,
                                                                          IsRunning =
                                                                              m_Installation.JobsEngine.
                                                                              IsRunning
                                                                      }
                                                                : null;


                installationSection.InterfaceEngine = m_Installation.HasInterfaceEngine
                                                          ? new InterfaceEngineElement
                                                                {
                                                                    LogLevel = m_Installation.InterfaceEngine.LogLevel,
                                                                    IsRunning =
                                                                        m_Installation.InterfaceEngine.IsRunning
                                                                }
                                                          : null;

                installationSection.ClientsEngine = m_Installation.HasClientsEngine
                                                        ? new ClientsEngineElement
                                                              {
                                                                  LogLevel = m_Installation.ClientsEngine.LogLevel,
                                                                  IsRunning = m_Installation.ClientsEngine.IsRunning
                                                              }
                                                        : null;
                installationSection.ReportsEngine = m_Installation.HasReportsEngine
                                                        ? new ReportsEngineElement
                                                              {
                                                                  LogLevel = m_Installation.ReportsEngine.LogLevel,
                                                                  IsRunning = m_Installation.ReportsEngine.IsRunning,
                                                              }
                                                        : null;

                if (m_Installation.HasWorkflowEngines)
                {
                    foreach (WorkflowEngine engine in m_Installation.WorkflowEngines)
                    {
                        WorkflowEngineElement wee = new WorkflowEngineElement
                                                        {
                                                            EngineId = engine.EngineId,
                                                            LogLevel = engine.LogLevel,
                                                            IsRunning = engine.IsRunning,
                                                            ConcurrentCalls = engine.ConcurrentCalls
                                                        };
                        installationSection.WorkflowEngines.Add(wee);
                    }
                }
                else
                {
                    installationSection.WorkflowEngines = null;
                }
                config.Save(ConfigurationSaveMode.Full);
            }
        }

   public void EditInstallation(Installation installation)
        {
            if (!InstallationExists(installation))
            {
                ApplicationException exc = new ApplicationException("Installation with Name " + installation.Name + " does not exist in the configuration.") { Source = "EditInstallation" };
                throw exc;
            }
        }

        private bool InstallationExists(Installation installation)
        {
            if (installation.Name == m_Installation.Name)
                return true;
            return false;
        }
    }
}