using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class InstallationSection : ConfigurationSection
    {
        [ConfigurationProperty("Name",
            IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)this["Name"]; }
            set { this["Name"] = value; }
        }

        [ConfigurationProperty("BaseFolder",
            IsRequired = true, IsKey = false)]
        public string BaseFolder
        {
            get { return (string) this["BaseFolder"]; }
            set { this["BaseFolder"] = value; }
        }

        [ConfigurationProperty("LogType",
            IsRequired = true, IsKey = false)]
        public string LogType
        {
            get { return (string)this["LogType"]; }
            set { this["LogType"] = value; }
        }

        [ConfigurationProperty("Port", IsRequired = true, DefaultValue = "9000")]
        public string Port
        {
            get { return (string)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("WorkflowEngines", IsRequired = false)]
        public WorkflowEngineCollection WorkflowEngines
        {
            get
            {
                return (WorkflowEngineCollection) this["WorkflowEngines"];
            }
            set
            {
                this["WorkflowEngines"] = value;
            }
        }

        [ConfigurationProperty("JobsEngine", IsRequired = false)]
        public JobsEngineElement JobsEngine
        {
            get { return (JobsEngineElement)this["JobsEngine"]; }
            set { this["JobsEngine"] = value; }
        }

        [ConfigurationProperty("InterfaceEngine", IsRequired = false)]
        public InterfaceEngineElement InterfaceEngine
        {
            get { return (InterfaceEngineElement)this["InterfaceEngine"]; }
            set { this["InterfaceEngine"] = value; }
        }

        [ConfigurationProperty("ClientsEngine", IsRequired = false)]
        public ClientsEngineElement ClientsEngine
        {
            get { return (ClientsEngineElement)this["ClientsEngine"]; }
            set { this["ClientsEngine"] = value; }
        }

        [ConfigurationProperty("ReportsEngine", IsRequired = false)]
        public ReportsEngineElement ReportsEngine
        {
            get { return (ReportsEngineElement)this["ReportsEngine"]; }
            set { this["ReportsEngine"] = value; }
        }

        [ConfigurationProperty("DBSettings", IsRequired = true)]
        public DBSettingsElement DBSettings
        {
            get { return (DBSettingsElement)this["DBSettings"]; }
            set { this["DBSettings"] = value; }
        }

        [ConfigurationProperty("SmtpSettings", IsRequired = false)]
        public SmtpSettingsElement SmtpSettings
        {
            get { return (SmtpSettingsElement)this["SmtpSettings"]; }
            set { this["SmtpSettings"] = value; }
        }

        [ConfigurationProperty("ConcurrentCalls", IsRequired = false, DefaultValue = 0)]
        public int ConcurrentCalls
        {
            get { return (int)this["ConcurrentCalls"]; }
            set { this["ConcurrentCalls"] = value; }
        }

        [ConfigurationProperty("IsRunning", IsRequired = false, DefaultValue = "true")]
        public bool IsRunning
        {
            get { return (bool)this["IsRunning"]; }
            set { this["IsRunning"] = value; }
        }

        [ConfigurationProperty("ProcessNewCases", IsRequired = false, DefaultValue = "true")]
        public bool ProcessNewCases
        {
            get { return (bool)this["ProcessNewCases"]; }
            set { this["ProcessNewCases"] = value; }
        }
    }
}