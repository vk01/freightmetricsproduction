﻿using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class ReportsEngineElement : ConfigurationElement
    {
        [ConfigurationProperty("EngineId", IsRequired = true, DefaultValue = 1, IsKey = true)]
        public int EngineId
        {
            get { return (int) this["EngineId"]; }
            set { this["EngineId"] = value; }
        }

        [ConfigurationProperty("LogLevel", IsRequired = true, DefaultValue = "Message")]
        public string LogLevel
        {
            get { return (string) this["LogLevel"]; }
            set { this["LogLevel"] = value; }
        }

        [ConfigurationProperty("IsRunning", IsRequired = true, DefaultValue = "false")]
        public bool IsRunning
        {
            get { return (bool) this["IsRunning"]; }
            set { this["IsRunning"] = value; }
        }

        public override string ToString()
        {
            return "Reports Engine " + EngineId;
        }
    }
}