using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class DBSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("User", IsRequired = true, IsKey = false)]
        public string User
        {
            get { return (string)this["User"]; }
            set { this["User"] = value; }
        }

        [ConfigurationProperty("Pass", IsRequired = true, IsKey = false)]
        public string Pass
        {
            get { return (string)this["Pass"]; }
            set { this["Pass"] = value; }
        }

        [ConfigurationProperty("Service", IsRequired = true, IsKey = false)]
        public string Service
        {
            get { return (string)this["Service"]; }
            set { this["Service"] = value; }
        }

        [ConfigurationProperty("HostList", IsRequired = false)]
        public HostListCollection HostList
        {
            get
            {
                return (HostListCollection)this["HostList"];
            }
            set
            {
                this["HostList"] = value;
            }
        }

        [ConfigurationProperty("ExtraOptions", IsRequired = false, IsKey = false)]
        public string ExtraOptions
        {
            get { return (string)this["ExtraOptions"]; }
            set { this["ExtraOptions"] = value; }
        }
    }
}