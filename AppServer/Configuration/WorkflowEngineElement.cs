using System.Configuration;

namespace Exis.AppServer.Configuration
{
    public sealed class WorkflowEngineElement : ConfigurationElement
    {
        [ConfigurationProperty("EngineId", IsRequired = true, DefaultValue = 1, IsKey = true)]
        public int EngineId
        {
            get { return (int)this["EngineId"]; }
            set { this["EngineId"] = value; }
        }

        [ConfigurationProperty("LogLevel", IsRequired = true, DefaultValue = "Message")]
        public string LogLevel
        {
            get { return (string)this["LogLevel"]; }
            set { this["LogLevel"] = value; }
        }

        [ConfigurationProperty("IsRunning", IsRequired = true, DefaultValue = "false")]
        public bool IsRunning
        {
            get { return (bool)this["IsRunning"]; }
            set { this["IsRunning"] = value; }
        }

        [ConfigurationProperty("ConcurrentCalls", IsRequired = false, DefaultValue = 0)]
        public int ConcurrentCalls
        {
            get { return (int)this["ConcurrentCalls"]; }
            set { this["ConcurrentCalls"] = value; }
        }

        public override string ToString()
        {
            return "Workflow Engine " + EngineId;
        }
    }
}