﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AppServerInterfaces.Classes;
using Exis.AppServer.Configuration;
using Exis.WorkflowEngineConsole;
using Gurock.SmartInspect;

namespace Exis.AppServer
{
    [Serializable]
    internal static class EnginesManager
    {
        private static readonly Session logSession = SiAuto.Si.GetSession("Engines") ?? SiAuto.Si.AddSession("Engines");
        private static readonly Dictionary<AbstractEngine, AppDomain> m_RunningEngines = new Dictionary<AbstractEngine, AppDomain>();
        private static bool m_StopRequest;
        private static Thread m_EnginesThread;

        public static Session LogSession
        {
            get { return logSession; }
        }

        internal static void Start()
        {
            m_StopRequest = false;
            m_EnginesThread = new Thread(EnginesThread);
            m_EnginesThread.Start();
        }

        internal static void Stop(bool serviceStop)
        {
            m_StopRequest = true;
            logSession.LogMessage("Stopping Engines Thread...");
            if (IsRunning())
            {
                m_EnginesThread.Join();
            }
            StopAllEngines(serviceStop);
        }

        internal static void Interrupt()
        {
            m_EnginesThread.Interrupt();
        }

        internal static bool IsRunning()
        {
            return m_EnginesThread != null && m_EnginesThread.IsAlive;
        }

        internal static bool IsEngineRunning(AbstractEngine engine)
        {
            lock (m_RunningEngines)
            {
                return m_RunningEngines.ContainsKey(engine);
            }
        }

        internal static void StartEngineAsync(AbstractEngine engine)
        {
            var thread = new Thread(StartEngine);
            thread.Start(engine);
        }

        private static void StartEngine(object parameter)
        {
            var engine = (AbstractEngine)parameter;
            engine = GetEngine(engine);
            engine.IsStarting = true;
  engine.State = EngineState.Starting;

            lock (m_RunningEngines)
            {
                logSession.LogMessage("Starting {0}", engine);
                try
                {
                    if (m_RunningEngines.ContainsKey(engine))
                        throw new ArgumentException(engine + " is already running.");

                    var objAppDomainSetupEngine = new AppDomainSetup();
                    objAppDomainSetupEngine.ApplicationName = engine.EngineTypeName +
                                                              (engine.EngineType == EngineTypeEnum.WorkflowEngine
                                                                   ? "_" + engine.EngineId
                                                                   : "");
                    objAppDomainSetupEngine.ConfigurationFile = engine.ConfigName;
                    objAppDomainSetupEngine.ShadowCopyFiles = "true";
                    objAppDomainSetupEngine.CachePath = "Cache";

                    AppDomain objAppDomain =
                        AppDomain.CreateDomain(
                            engine.Installation.Name + "_" + objAppDomainSetupEngine.ApplicationName + "_" +
                            engine.EngineId,
                            null,
                            objAppDomainSetupEngine);

                    logSession.EnterMethod(Level.Debug, "Engine Arguments");
                    logSession.LogDebug("{0} with {1} arguments",
                                        engine, engine.GetEngineArguments().Length);

                    for (int i = 0; i < engine.GetEngineArguments().Length; i++)
                    {
                        string s = engine.GetEngineArguments()[i];
                        logSession.LogDebug("Argument {0}: {1}", i + 1, s);
                    }
                    logSession.LeaveMethod(Level.Debug, "Engine Arguments");

                    try
                    {
                        string path = objAppDomain.SetupInformation.ApplicationBase;
                        path += engine.ExecutableName;

                        if (objAppDomain.ExecuteAssembly(path, engine.GetEngineArguments()) == 0)
                        {
              engine.IsRunning = true;
                            engine.State = EngineState.Running;
                            m_RunningEngines.Add(engine, objAppDomain);
                            if (engine.EngineType == EngineTypeEnum.WorkflowEngine)
                            {
                                CasesManager.RegisterWorkflowEngine(engine);
                  }
                            logSession.LogMessage("{0} started successfuly.", engine);
                        }
                        else
                        {
                            logSession.LogError(
                              "AppDomain for {0} did not start correctly. Trying to unload app domain.", engine);

                            engine.State = EngineState.Terminating;
                            try
                            {
                                if (!String.IsNullOrEmpty(objAppDomain.FriendlyName))
                                {
                                    AppDomain.Unload(objAppDomain);
                  logSession.LogMessage("AppDomain for {0} was successfuly unloaded.", engine);
                                }
                            }
                            catch (AppDomainUnloadedException)
                            {
                                logSession.LogWarning("AppDomain for {0} was already unloaded", engine);
                            }
                            finally
                            {
                                engine.State = EngineState.Stopped;
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        logSession.LogException(exc);
                        return;
                    }
                }
                catch (Exception exc)
                {
                    logSession.LogException(exc);
                    return;
                }
                finally
                {
                    engine.IsStarting = false;
                    ConfigurationHelper.GetInstance().SaveConfiguration();
                }
            }
        }

        internal static void StopEngineAsync(AbstractEngine engine)
        {
            var thread = new Thread(StopEngine);
            thread.Start(engine);
        }

        private static void StopEngine(object parameter)
        {
            var engine = (AbstractEngine)parameter;
            engine = GetEngine(engine);

            try
            {
                AppDomain objAppDomain;
                lock (m_RunningEngines)
                {
                    if (!IsEngineRunning(engine))
                        throw new ArgumentException(engine + " is not running.");
                    objAppDomain = m_RunningEngines[engine];
                }

                logSession.LogMessage("Stopping {0}", engine);
                engine.IsStopping = true;
                engine.State = EngineState.Stopping;

                        switch (engine.EngineType)
                        {
                            case EngineTypeEnum.WorkflowEngine:
                                logSession.LogDebug("AppDomain {0} is Workflow engine. Calling stop...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(Program.Stop);
                                break;
                            case EngineTypeEnum.JobsEngine:
                                logSession.LogDebug("AppDomain {0} is Jobs engine. Calling stop...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(Exis.JobsEngineConsole.Program.Stop);
                                break;
                            case EngineTypeEnum.InterfaceEngine:
                                logSession.LogDebug("AppDomain {0} is Interface engine. Calling stop...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(Exis.InterfaceEngineConsole.Program.Stop);
                                break;
                            case EngineTypeEnum.ClientsEngine:
                                logSession.LogDebug("AppDomain {0} is Clients engine. Calling stop...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(Exis.ClientsEngineConsole.Program.Stop);
                                break;
                            case EngineTypeEnum.ReportsEngine:
                                logSession.LogDebug("AppDomain {0} is Reports engine. Calling stop...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(Exis.ReportsEngineConsole.Program.Stop);
                                break;

                }

                AppDomain.Unload(objAppDomain);

                lock (m_RunningEngines)
                    m_RunningEngines.Remove(engine);
                engine.IsRunning = false;
                engine.State = EngineState.Stopped;

                    ConfigurationHelper.GetInstance().SaveConfiguration();

                    if (engine.EngineType == EngineTypeEnum.WorkflowEngine)
                    {
                        CasesManager.UnregisterWorkflowEngine(engine);
            }
                logSession.LogMessage("{0} stopped successfuly.", engine);
            }
            catch (AppDomainUnloadedException)
            {
                lock (m_RunningEngines)
                    m_RunningEngines.Remove(engine);
                engine.IsRunning = false;
                engine.State = EngineState.Stopped;

                ConfigurationHelper.GetInstance().SaveConfiguration();

                if (engine.EngineType == EngineTypeEnum.WorkflowEngine)
                {
                    CasesManager.UnregisterWorkflowEngine(engine);
                }
                logSession.LogWarning("AppDomain for {0} was unloaded before stopping could complete.", engine);
            }
            catch (Exception e)
            {
                logSession.LogException(e);
            }
            engine.IsStopping = false;
        }

        internal static void TerminateEngineAsync(AbstractEngine engine)
        {
            var thread = new Thread(TerminateEngine);
            thread.Start(engine);
        }

        internal static void TerminateEngine(object parameter)
        {
            var engine = (AbstractEngine)parameter;
            engine = GetEngine(engine);

            try
            {
                logSession.LogMessage("Terminating {0}...", engine);
                lock (m_RunningEngines)
                {
                    if (!IsEngineRunning(engine))
                        throw new ArgumentException(engine + " is not running.");
                    AppDomain objAppDomain = m_RunningEngines[engine];

                    engine.State = EngineState.Terminating;
                    try
                    {
                        switch (engine.EngineType)
                        {
                            case EngineTypeEnum.WorkflowEngine:
                                logSession.LogDebug("AppDomain {0} is Workflow engine. Calling abort...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(Program.Abort);
                                break;
                            case EngineTypeEnum.InterfaceEngine:
                                logSession.LogDebug("AppDomain {0} is Interface engine. Calling abort...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(InterfaceEngineConsole.Program.Abort);
                                break;
                            case EngineTypeEnum.ClientsEngine:
                                logSession.LogDebug("AppDomain {0} is Clients engine. Calling abort...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(ClientsEngineConsole.Program.Abort);
                                break;
                          
                            case EngineTypeEnum.ReportsEngine:
                                logSession.LogDebug("AppDomain {0} is Reports engine. Calling abort...",
                                                    objAppDomain.FriendlyName);
                                objAppDomain.DoCallBack(ReportsEngineConsole.Program.Abort);
                                break;
                           
                        }
                        AppDomain.Unload(objAppDomain);
                        engine.IsRunning = false;
                        engine.State = EngineState.Stopped;
                        logSession.LogMessage("AppDomain for {0} terminated successfuly.", engine);
                    }
                    catch (CannotUnloadAppDomainException e)
                    {
                        logSession.LogException("AppDomain for " + engine + " could not unload. Please retry.", e);
                    }
                    catch (AppDomainUnloadedException e)
                    {
                        logSession.LogException("AppDomain for " + engine + " is already unloaded.", e);
                        engine.IsRunning = false;
                        engine.State = EngineState.Stopped;
                    }
                }
            }
            catch (Exception e)
            {
                logSession.LogException(e);
            }
        }

        public static void ChangeEngineDebugLevel(AbstractEngine engine, string level)
        {
            try
            {
                engine = GetEngine(engine);
                if (!IsEngineRunning(engine))
                    throw new Exception(engine + " is not running.");

                AppDomain objAppDomain = m_RunningEngines[engine];
                objAppDomain.SetData("LogLevel", level);
                switch (engine.EngineType)
                {
                    case EngineTypeEnum.WorkflowEngine:
                        objAppDomain.DoCallBack(Program.ChangeDebugLevel);
                        break;
                    case EngineTypeEnum.JobsEngine:
                        objAppDomain.DoCallBack(JobsEngineConsole.Program.ChangeDebugLevel);
                        break;
                    case EngineTypeEnum.InterfaceEngine:
                        objAppDomain.DoCallBack(InterfaceEngineConsole.Program.ChangeDebugLevel);
                        break;
                    case EngineTypeEnum.ClientsEngine:
                        objAppDomain.DoCallBack(ClientsEngineConsole.Program.ChangeDebugLevel);
                        break;
                    case EngineTypeEnum.ReportsEngine:
                        objAppDomain.DoCallBack(ReportsEngineConsole.Program.ChangeDebugLevel);
                        break;
                }
                engine.LogLevel = level;
                ConfigurationHelper.GetInstance().SaveConfiguration();
            }
            catch (Exception e)
            {
                logSession.LogException(e);
            }
        }

        private static AbstractEngine GetEngine(AbstractEngine engine)
        {
            var installation = ConfigurationHelper.GetInstance().Installation;
            if (installation.Equals(engine.Installation))
            {
                switch (engine.EngineType)
                {
                    case EngineTypeEnum.JobsEngine:
                        return installation.JobsEngine;
                    case EngineTypeEnum.InterfaceEngine:
                        return installation.InterfaceEngine;
                    case EngineTypeEnum.WorkflowEngine:
                        if (engine.EngineId == 0)
                        {
                            return installation.SpecialWorkflowEngine;
                        }
                        foreach (WorkflowEngine workflowEngine in installation.WorkflowEngines)
                        {
                            if (workflowEngine.Equals(engine))
                            {
                                return workflowEngine;
                            }
                        }
                        break;
                    case EngineTypeEnum.ClientsEngine:
                        return installation.ClientsEngine;
                    case EngineTypeEnum.ReportsEngine:
                        return installation.ReportsEngine;
                }
            }
            return null;
        }


        internal static bool ProcessOnDemandCase(long caseId)
        {
            SiAuto.Main.EnterMethod(Level.Debug, "Register Case");
            try
            {
                lock (m_RunningEngines)
                {
                    var installation = ConfigurationHelper.GetInstance().Installation;
                    AbstractEngine specialEngine = GetEngine(installation.SpecialWorkflowEngine);
                    if (specialEngine != null)
                    {
                        //                        AppDomain specialDomain = m_RunningEngines[specialEngine];
                        var objProgram = new Program();
                        objProgram.CaseId = caseId;
                        // specialDomain.DoCallBack(objProgram.ProcessWorkflow);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException(e);
            }
            finally
            {
                SiAuto.Main.LeaveMethod(Level.Debug, "Register Case");
            }
            return false;
        }

        internal static void ClearAndRefreshEngines()
        {
            lock (m_RunningEngines)
            {
                var unloadedEngines = new List<AbstractEngine>();
                foreach (var pair in m_RunningEngines)
                {
                    try
                    {
                        if(pair.Key.State == EngineState.Running)
                        {
                            string s = pair.Value.FriendlyName;
                        }
                    }
                    catch (AppDomainUnloadedException)
                    {
                        logSession.LogWarning("AppDomain for {0} threw AppDomainUnloadedException",
                                              pair.Key.ToString());
                        unloadedEngines.Add(pair.Key);
                    }
                }
                foreach (AbstractEngine engine in unloadedEngines)
                {
                    m_RunningEngines.Remove(engine);
                }
            }
        }

        internal static void StopAllEngines(bool ServiceStop)
        {
           lock (m_RunningEngines)
            {
                logSession.LogMessage("Stopping All Engines...");
                var unloadedEngines = m_RunningEngines.Select(a => a.Key).ToList();
                foreach (var engine in unloadedEngines)
                {
                    StopEngine(engine);
                    if (ServiceStop)
                    {
                        AbstractEngine abstractEngine = GetEngine(engine);
                        abstractEngine.IsRunning = true;
                    }
                }
                ConfigurationHelper.GetInstance().SaveConfiguration();
                logSession.LogMessage("All engines are stopped.");
            }
        }

        private static void EnginesThread()
        {
           while (!m_StopRequest)
            {
                    try
                    {
                        lock (m_RunningEngines)
                        {
                            ClearAndRefreshEngines();

                            var installation = ConfigurationHelper.GetInstance().Installation;
                            foreach (AbstractEngine abstractEngine in installation.GetAllEngines())
                            {
                                if (abstractEngine.IsRunning && !abstractEngine.IsStarting &&
                                    !abstractEngine.IsStopping && !IsEngineRunning(abstractEngine))
                                {
                                logSession.LogMessage("Found {0} that should be running. Starting...",
                                                        abstractEngine.ToString());
                                StartEngineAsync(abstractEngine);
                                }
                            }
                        }
                        Thread.Sleep(1000);
                    }
                    catch (ThreadInterruptedException)
                    {
                        if (m_StopRequest)
                        {
                            logSession.LogMessage("Service stop requested. Exiting thread for Engines");
                            break;
                        }
                        logSession.LogMessage("Engines thread interrupted. Resuming loop.");
                    }
                    catch (Exception e)
                    {
                        logSession.LogException(e);
                        break;
                    }
                }
      logSession.LogMessage("Engine Thread stopped.");
        }
    }
}