﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Threading;
using System.Xml;
using Exis.ClientsEngine;
using Exis.Domain;
using Exis.ServiceInterfaces;
using Exis.SessionRegistry;
using Exis.WCFExtensions;
using Gurock.SmartInspect;

namespace Exis.ClientsEngineConsole
{
    public class Program
    {
        private static ClientsService objService;
        private static ServiceHost objServiceHost;

        private static int Main(string[] args)
        {
            if (!Thread.GetDomain().IsDefaultAppDomain())
            {
                try
                {
                    ServerSessionRegistry.initialize();
                    ServerSessionRegistry.ApplicationDirectory =
                        Thread.GetDomain().SetupInformation.ApplicationBase;
                    ServerSessionRegistry.User = "ClientsEngine";
                    HandleArguments(args);

                    var dataContext = new DataContext(objService.DBConnectionString);
                    AppParameter appParameter =
                        dataContext.AppParameters.Where(a => a.Code == "SERVER_CULTURE_INFO_NAME").SingleOrDefault();

                    if (appParameter == null)
                    {
                        var myexc =
                            new ApplicationException("Failed to load 'SERVER_CULTURE_INFO_NAME' Application Parameter.");
                        SiAuto.Main.LogException(myexc);
                        throw myexc;
                    }
                    ServerSessionRegistry.ServerCultureInfo = new CultureInfo(appParameter.Value, false);

                    objServiceHost.Open();
                }
                catch
                {
                    return 1;
                }
            }
            else
            {
                try
                {
                    objServiceHost = CreateServiceHost("9000");
                    objServiceHost.Open();
                }
                catch
                {
                    return 1;
                }
                Console.ReadLine();
                objServiceHost.Close();
            }
            return 0;
        }

        public static void Stop()
        {
            objServiceHost.Close();
        }

        public static void Abort()
        {
            objServiceHost.Abort();
        }

        public static void ChangeDebugLevel()
        {
            var level = (string)AppDomain.CurrentDomain.GetData("LogLevel");
            SiAuto.Main.LogMessage("Changing debug level from " + SiAuto.Si.Level + " to " + level);
            SiAuto.Si.Level = (Level)Enum.Parse(typeof(Level), level, true);
        }

        private static ServiceHost CreateServiceHost(string port)
        {
            objService = new ClientsService();

            objService.DBConnectionString = "User Id=crm3;Password=crm3;Data Source=artemis;Pooling=true";

            var serviceHost = new ServiceHost(objService);
            serviceHost.Authorization.ServiceAuthorizationManager = new MyServiceAuthorizationManager();

            var ipcBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
            {
                MaxReceivedMessageSize = Int32.MaxValue,
                OpenTimeout = TimeSpan.MaxValue,
                CloseTimeout = TimeSpan.MaxValue,
                ReceiveTimeout = TimeSpan.MaxValue,
                SendTimeout = TimeSpan.MaxValue,
                ReaderQuotas =
                    new XmlDictionaryReaderQuotas
                    {
                        MaxStringContentLength = Int32.MaxValue,
                        MaxArrayLength = Int32.MaxValue,
                        MaxDepth = Int32.MaxValue,
                        MaxBytesPerRead = Int32.MaxValue
                    }
            };
            serviceHost.AddServiceEndpoint(typeof(IClientsService), ipcBinding,
                                           "net.pipe://localhost/Exis/FreightMetrics/ClientsEngine/" + port);

            var netTcpBinding = new NetTcpBinding
                                    {
                                        MaxReceivedMessageSize = Int32.MaxValue,
                                        OpenTimeout = TimeSpan.MaxValue,
                                        CloseTimeout = TimeSpan.MaxValue,
                                        ReceiveTimeout = TimeSpan.MaxValue,
                                        SendTimeout = TimeSpan.MaxValue,
                                        ListenBacklog = 100,
                                        PortSharingEnabled = true,
                                        ReaderQuotas =
                                            new XmlDictionaryReaderQuotas
                                                {
                                                    MaxStringContentLength = Int32.MaxValue,
                                                    MaxArrayLength = Int32.MaxValue
                                                },
                                    };
            netTcpBinding.Security.Mode = SecurityMode.None;
            //netTcpBinding.Security.Mode = SecurityMode.Message;
            //netTcpBinding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            netTcpBinding.ReliableSession.Enabled = true;
            netTcpBinding.ReliableSession.Ordered = true;
            netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(60);
            // netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromHours(11);
            serviceHost.AddServiceEndpoint(typeof (IClientsService), netTcpBinding,
                                           new Uri("net.tcp://localhost:" + port + "/Exis/FreightMetrics/ClientsEngine"));

            netTcpBinding = new NetTcpBinding(SecurityMode.None)
            {
                MaxReceivedMessageSize = Int32.MaxValue,
                OpenTimeout = TimeSpan.MaxValue,
                CloseTimeout = TimeSpan.MaxValue,
                ReceiveTimeout = TimeSpan.MaxValue,
                SendTimeout = TimeSpan.MaxValue,
                ListenBacklog = 100,
                PortSharingEnabled = true,
                ReaderQuotas =
                    new XmlDictionaryReaderQuotas
                    {
                        MaxStringContentLength = Int32.MaxValue,
                        MaxArrayLength = Int32.MaxValue
                    },
            };
            netTcpBinding.ReliableSession.Enabled = false;
            serviceHost.AddServiceEndpoint(typeof(IClientsService), netTcpBinding,
                                           new Uri("net.tcp://localhost:" + (Convert.ToInt32(port) + 2) + "/Exis/FreightMetrics/ClientsEngine"));


            var httpBinaryBinding =
                new CustomBinding(new BindingElementCollection
                                      {
                                          new BinaryMessageEncodingBindingElement
                                              {
                                                  ReaderQuotas =
                                                      new XmlDictionaryReaderQuotas
                                                          {
                                                              MaxStringContentLength = Int32.MaxValue,
                                                              MaxArrayLength = Int32.MaxValue
                                                          }
                                              },
                                          new HttpTransportBindingElement {MaxReceivedMessageSize = Int32.MaxValue}
                                      })
                    {
                        OpenTimeout = TimeSpan.MaxValue,
                        CloseTimeout = TimeSpan.MaxValue,
                        ReceiveTimeout = TimeSpan.MaxValue,
                        SendTimeout = TimeSpan.MaxValue
                    };
            serviceHost.AddServiceEndpoint(typeof (IClientsService), httpBinaryBinding,
                                           new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) + "/Exis/FreightMetrics/ClientsEngine"));


            //serviceHost.AddServiceEndpoint(typeof (IPolicyRetriever), new WebHttpBinding(),
            //                               "http://localhost:" + port).Behaviors.Add(new WebHttpBehavior());

            serviceHost.Description.Behaviors.Add(new ServiceThrottlingBehavior
                                                      {
                                                          MaxConcurrentCalls = 100,
                                                          // For Singletons with Multiple Threads, the number of concurrent threads
                                                          // MaxConcurrentInstances = 100,// Ignored for Singletons
                                                          MaxConcurrentSessions = 100
                                                          // No of Clients with a Transport session (Reliable) connected to this Service
                                                      });

#if DEBUG
            var behavior = new ServiceMetadataBehavior
                               {
                                   HttpGetEnabled = true,
                                   HttpGetUrl =
                                       new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) + "/Exis/FreightMetrics/ClientsEngine/" +
                                               "mex")
                               };
            serviceHost.Description.Behaviors.Add(behavior);
#endif
            return serviceHost;
        }

        private static void HandleArguments(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine(
                    "Usage: ClientsEngine <Installation Name> <Connection String> <LogLevel> <Port> (<SmtpHost> <SmtpPort> <SupportAddress> <IssueTrackerAddress> <InstallationAddress>)");
                throw new ApplicationException("Invalid count of arguments.");
            }

            string InstallationName = args[0];
            string ConnectionString = args[1];

            string LogLevel = args[2];
            string Port = args[3];

            Console.WriteLine("Clients Engine Starting");
            Console.WriteLine("Arguments:");
            foreach (string s in args)
            {
                Console.WriteLine("\t- " + s);
            }
            SiAuto.Si.AppName = "Clients Engine - " + InstallationName;

            SiAuto.Si.Connections = "file(append=\"true\", filename=\"" +
                                    Thread.GetDomain().SetupInformation.ApplicationBase + "Logs\\ClientsEngineLog_" +
                                    InstallationName +
                                    ".sil\", maxsize=\"65536\", rotate=\"daily\", caption=\"file\", keepopen=\"true\", reconnect=\"true\")";
            SiAuto.Si.DefaultLevel = Level.Message;
            SiAuto.Si.Enabled = true;
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), LogLevel, true);

            objServiceHost = CreateServiceHost(Port);
            objService.DBConnectionString = ConnectionString;
            if (args.Length > 4)
            {
                string smtpHost = args[4];
                string smtpPort = args[5];
                string supportAddress = args[6];
                string issueTrackerAddress = args[7];
                string installationAddress = args[8];

                if (smtpHost != "0")
                {
                    objService.smtpClient = new SmtpClient(smtpHost, Int32.Parse(smtpPort));
                    objService.exisSupportEmailAddress = supportAddress;
                    objService.exisIssueTrackerEmailAddress = issueTrackerAddress;
                    objService.installationEmailAddress = installationAddress;
                }
            }
        }
    }
}