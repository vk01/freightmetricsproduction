﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exis.Domain;
using Exis.WCFExtensions;
using Extreme.Mathematics;
using Extreme.Mathematics.Curves;
using Extreme.Mathematics.LinearAlgebra;
using Extreme.Statistics;
using Extreme.Statistics.Distributions;
using Extreme.Statistics.Random;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;

namespace Exis.ClientsEngine
{
    public partial class ClientsService
    {
        #region IClientsService Members

        public int? GeneratePosition(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, List<string> tradeIdentifiers, out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage)
        {
            return GeneratePosition(curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, "1,2,3,4,5", tradeIdentifiers, out results, out resultsWithoutWeight, out errorMessage);
        }
        public int? GeneratePosition(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, string tradeTypeItems, List<string> tradeIdentifiers, out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage)
        {
            results = null;
            resultsWithoutWeight = null;
            errorMessage = "";

            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            var listOfTradeIdBatches = new List<List<string>>();
            int skip = 0;

            // Batches of 500 trade legs are created and processed in order to overcome the IN SQL clause limit of numbers.
            while (true)
            {
                List<string> tradeIdsBatch = tradeIdentifiers.Skip(skip).Take(500).ToList();
                if (tradeIdsBatch.Count == 0) break;
                listOfTradeIdBatches.Add(tradeIdsBatch);
                skip = skip + tradeIdsBatch.Count;
            }

            var allResults = new List<List<Dictionary<string, Dictionary<DateTime, decimal>>>>();
            var allResultsWithoutWeight = new List<List<Dictionary<string, Dictionary<DateTime, decimal>>>>();

            foreach (var listOfTradeIdBatch in listOfTradeIdBatches)
            {
                List<TradeInformation> tradeInformations;

                // Trade TC and Cargo Legs should be handled each as a different Trade. Therefore a special notation is used at the client: 
                // A string representing the type of Trade along with "|" and a DB identifier that identifies the specific VERSION of the Leg of a 
                // Physical trade or the specific VERSION of the Financial Trade.
                List<long> tradeTcInfoLegIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.TC).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "TC").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeFfaInfoIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.FFA).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "FFA").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeCargoInfoLegIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.Cargo).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "CARGO").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeOptionInfoIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.Put).ToString()) || tradeTypeItems.Contains(TradeTypeWithTypeOptionInfoEnum.Call.ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "OPTION").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();

                var dataContexts = new List<DataContext>();

                try
                {
                    var dataContextRoot = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                    dataContexts.Add(dataContextRoot);

                    if (SiAuto.Si.Level == Level.Debug)
                    {
                        if (SiAuto.Si.GetSession(userName) == null)
                            SiAuto.Si.AddSession(userName, true);
                        dataContextRoot.Log =
                            new SmartInspectLinqToSqlAdapter(
                                SiAuto.Si.GetSession(userName))
                            {
                                TitleLimit = 0,
                                TitlePrefix = "GeneratePosition: "
                            };
                    }

                    // All Trades with corresponding information are selected according to their Ids. Trade TC and Cargo Legs should be handled each as a different Trade. 

                    tradeInformations =
                        (from objTrade in dataContextRoot.Trades
                         from objTradeInfo in dataContextRoot.TradeInfos
                         from objTradeTCInfo in dataContextRoot.TradeTcInfos
                         from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs

                         where
                             objTrade.Id == objTradeInfo.TradeId
                             && objTrade.Type == TradeTypeEnum.TC
                             && objTrade.Status == ActivationStatusEnum.Active
                             && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                             && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                             && tradeTcInfoLegIds.Contains(objTradeTCInfoLeg.Id)

                         select
                             new TradeInformation
                             {
                                 Trade = objTrade,
                                 TradeInfo = objTradeInfo,
                                 TradeTCInfo = objTradeTCInfo,
                                 TradeTCInfoLeg = objTradeTCInfoLeg
                             }).ToList().
                            Union((from objTrade in dataContextRoot.Trades
                                   from objTradeInfo in dataContextRoot.TradeInfos
                                   from objTradeFFAInfo in dataContextRoot.TradeFfaInfos

                                   from objTradeInfoBook in dataContextRoot.TradeInfoBooks
                                   from objBook in dataContextRoot.Books

                                   where
                                      objTrade.Id == objTradeInfo.TradeId
                                      && objTrade.Type == TradeTypeEnum.FFA
                                      && objTrade.Status == ActivationStatusEnum.Active
                                      && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                      && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)
                                      && objTradeInfoBook.TradeInfoId == objTradeInfo.Id
                                      && objBook.Id == objTradeInfoBook.BookId
                                      && objBook.ParentBookId == null

                                   select
                                       new TradeInformation
                                       {
                                           Trade = objTrade,
                                           TradeInfo = objTradeInfo,
                                           TradeFFAInfo = objTradeFFAInfo,
                                           BookPositionCalcByDays = objBook.PositionCalculatedByDays
                                       }
                                    ).ToList()).Union((from objTrade in dataContextRoot.Trades
                                                       from objTradeInfo in dataContextRoot.TradeInfos
                                                       from objTradeCargoInfo in dataContextRoot.TradeCargoInfos
                                                       from objTradeCargoInfoLeg in dataContextRoot.TradeCargoInfoLegs

                                                       where
                                                           objTrade.Id == objTradeInfo.TradeId
                                                           && objTrade.Type == TradeTypeEnum.Cargo
                                                           && objTrade.Status == ActivationStatusEnum.Active
                                                           && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                           && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                           && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)

                                                       select
                                                           new TradeInformation
                                                           {
                                                               Trade = objTrade,
                                                               TradeInfo = objTradeInfo,
                                                               TradeCargoInfo = objTradeCargoInfo,
                                                               TradeCargoInfoLeg = objTradeCargoInfoLeg
                                                           }).ToList()).Union((
                                                                                from objTrade in dataContextRoot.Trades
                                                                                from objTradeInfo in dataContextRoot.TradeInfos
                                                                                from objTradeOptionInfo in dataContextRoot.TradeOptionInfos

                                                                                where
                                                                                    objTrade.Id == objTradeInfo.TradeId
                                                                                    && objTrade.Type == TradeTypeEnum.Option
                                                                                    && objTrade.Status == ActivationStatusEnum.Active
                                                                                    && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                                    && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)

                                                                                select
                                                                                    new TradeInformation
                                                                                    {
                                                                                        Trade = objTrade,
                                                                                        TradeInfo = objTradeInfo,
                                                                                        TradeOptionInfo = objTradeOptionInfo
                                                                                    }
                                                                            ).ToList()
                            ).ToList();

                    // The parallel library is used to send the trade legs of each batch for processing to different threads for performance reasons.
                    // Calling thread awaits the end of processing of all spawned threads before it goes further on.
                    List<List<Dictionary<string, Dictionary<DateTime, decimal>>>> tradesResults = tradeInformations.
                        AsParallel().
                        WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                        WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                        Select(b =>
                        {
                            var dataContext2 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                            dataContexts.Add(dataContext2);

                            //code used when necessary
                            if (SiAuto.Si.Level == Level.Debug)
                            {
                                if (SiAuto.Si.GetSession(userName) == null)
                                    SiAuto.Si.AddSession(userName, true);
                                dataContext2.Log =
                                    new SmartInspectLinqToSqlAdapter(
                                        SiAuto.Si.GetSession(userName))
                                    {
                                        TitleLimit = 0,
                                        TitlePrefix = "GeneratePosition: "
                                    };
                            }

                            var tradeResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                            var tradeResultsWithoutWeights = new Dictionary<string, Dictionary<DateTime, decimal>>();
                            string identifier = null;
                            var monthResults = new Dictionary<DateTime, decimal>();
                            var monthResultsWithoutWeights = new Dictionary<DateTime, decimal>();


                            DateTime currentDateFrom = periodFrom;
                            DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                            if (periodTo < currentDateTo) currentDateTo = periodTo;
                            // All results are generated per calendar Month, therefore processing is based on a relevant model. Start of period as well as the End of period do not have to be whole months.
                            while (currentDateFrom <= periodTo)
                            {
                                //get last bunisess date 
                                DateTime lastBusinessDate = GetLastBusinessDay(currentDateTo.Year, currentDateTo.Month);

                                decimal positionDays;
                                decimal periodMarketPrice;
                                decimal periodPrice;
                                decimal positionDaysNoWeights;
                                Dictionary<DateTime, decimal> finalFFAValues;
                                Dictionary<DateTime, decimal> marketPrices;

                                switch (b.Trade.Type)
                                {
                                    case TradeTypeEnum.TC:

                                        positionDaysNoWeights = positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeTCInfoLeg.IsOptional && b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                            {
                                                positionDays = 0;
                                            }
                                            else
                                            {
                                                if (positionMethod.ToUpper() == "STATIC")
                                                {
                                                    positionDays = positionDays * b.TradeTCInfo.VesselIndex / 100;
                                                }
                                                else if (positionMethod.ToUpper() == "DYNAMIC")
                                                {
                                                    if (!b.TradeTCInfoLeg.IsOptional || (b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                    {
                                                        positionDays = positionDays * b.TradeTCInfo.VesselIndex / 100;
                                                    }
                                                    else if (b.TradeTCInfoLeg.IsOptional && b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                    {
                                                        if (!IsOptionalTradeLegInTheMoney(b, currentDateFrom, currentDateTo, b.Trade.Type, curveDate, useSpot, userName, dataContext2, marketSensitivityType, marketSensitivityValue))
                                                        {
                                                            positionDays = 0;
                                                        }
                                                        else
                                                        {
                                                            positionDays = positionDays * b.TradeTCInfo.VesselIndex / 100;
                                                        }
                                                    }
                                                }
                                                else if (positionMethod.ToUpper() == "DELTA")
                                                {
                                                    if (!b.TradeTCInfoLeg.IsOptional || (b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                    {
                                                        positionDays = positionDays * b.TradeTCInfo.VesselIndex / 100;
                                                    }
                                                    else if (b.TradeTCInfoLeg.IsOptional && b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeTCInfoLeg.PeriodFrom && a.Date <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                        }
                                                        else
                                                        {
                                                            if (marketSensitivityType.ToUpper() == "STRESS")
                                                            {
                                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                            }
                                                            else
                                                            {
                                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                            }
                                                            periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeTCInfoLeg.PeriodFrom && a.Key <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Value).Average();

                                                            if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                            }
                                                            else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                            }
                                                        }
                                                        periodMarketPrice = periodMarketPrice * b.TradeTCInfo.VesselIndex / 100;
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        if (b.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                        {
                                                            if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                            {
                                                                periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeTCInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeTCInfoLeg.PeriodFrom && a.Date <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                marketPrices = CalculateForwardCurveValues(b.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeTCInfoLeg.PeriodFrom && a.Key <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                            }

                                                            periodPrice = periodPrice * b.TradeTCInfoLeg.IndexPercentage.Value / 100;
                                                            periodPrice = Math.Round(periodPrice, 4);
                                                        }
                                                        else
                                                        {
                                                            periodPrice = b.TradeTCInfoLeg.Rate.Value;
                                                        }
                                                        if (periodMarketPrice < periodPrice) positionDays = 0;
                                                        positionDays = positionDays * b.TradeTCInfo.VesselIndex / 100;
                                                    }
                                                }
                                            }
                                        }
                                        positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDays);
                                        monthResultsWithoutWeights.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDaysNoWeights);
                                        identifier = b.TradeTCInfoLeg.Id + "|TC";
                                        break;
                                    case TradeTypeEnum.FFA:

                                        positionDaysNoWeights = positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeInfo.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeInfo.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        decimal factor = 0;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty && b.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1;
                                                }
                                                else
                                                {
                                                    factor = positionDays / 30;
                                                }
                                            }
                                            else if (b.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty && b.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1M / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                                else
                                                {
                                                    factor = (positionDays / 30) / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                            }
                                            else if (b.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual && b.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                factor =
                                                    Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                                    Convert.ToDecimal(((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days) + 1);
                                            }
                                        }
                                        //JIRA:FREIG-11
                                        // positionDays = factor * b.TradeFFAInfo.QuantityDays;
                                        //JIRA:FREIG-17
                                        //positionDays = factor * b.TradeFFAInfo.TotalDaysOfTrade;
                                        positionDays = b.BookPositionCalcByDays == BookPositionCalculatedByDaysEnum.Total ? (factor * b.TradeFFAInfo.TotalDaysOfTrade) : (factor * b.TradeFFAInfo.QuantityDays);
                                        positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDays);
                                        monthResultsWithoutWeights.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDaysNoWeights);
                                        identifier = b.TradeFFAInfo.Id + "|FFA";
                                        break;
                                    case TradeTypeEnum.Cargo:

                                        positionDaysNoWeights = positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeCargoInfoLeg.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeCargoInfoLeg.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeCargoInfoLeg.IsOptional && b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                            {
                                                positionDays = 0;
                                            }
                                            else
                                            {
                                                if (positionMethod.ToUpper() == "STATIC")
                                                {
                                                    positionDays = positionDays * b.TradeCargoInfo.VesselIndex / 100;
                                                }
                                                else if (positionMethod.ToUpper() == "DYNAMIC")
                                                {
                                                    if (!b.TradeCargoInfoLeg.IsOptional || (b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                    {
                                                        positionDays = positionDays * b.TradeCargoInfo.VesselIndex / 100;
                                                    }
                                                    else if (b.TradeCargoInfoLeg.IsOptional && b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                    {
                                                        if (!IsOptionalTradeLegInTheMoney(b, currentDateFrom, currentDateTo, b.Trade.Type, curveDate, useSpot, userName, dataContext2, marketSensitivityType, marketSensitivityValue))
                                                        {

                                                            positionDays = 0;
                                                        }
                                                        else
                                                        {
                                                            positionDays = positionDays * b.TradeCargoInfo.VesselIndex / 100;
                                                        }
                                                    }
                                                }
                                                else if (positionMethod.ToUpper() == "DELTA")
                                                {
                                                    if (!b.TradeCargoInfoLeg.IsOptional || (b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                    {
                                                        positionDays = positionDays * b.TradeCargoInfo.VesselIndex / 100;
                                                    }
                                                    else if (b.TradeCargoInfoLeg.IsOptional && b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeCargoInfoLeg.PeriodFrom && a.Date <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                        }
                                                        else
                                                        {
                                                            if (marketSensitivityType.ToUpper() == "STRESS")
                                                            {
                                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                            }
                                                            else
                                                            {
                                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                            }
                                                            periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeCargoInfoLeg.PeriodFrom && a.Key <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                            if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                            }
                                                            else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                            }
                                                        }

                                                        periodMarketPrice = periodMarketPrice * b.TradeCargoInfo.VesselIndex / 100;
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        if (b.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                        {
                                                            if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                            {
                                                                periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeCargoInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeCargoInfoLeg.PeriodFrom && a.Date <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                marketPrices = CalculateForwardCurveValues(b.TradeCargoInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeCargoInfoLeg.PeriodFrom && a.Key <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                            }

                                                            periodPrice = periodPrice * b.TradeCargoInfoLeg.IndexPercentage.Value / 100;
                                                            periodPrice = Math.Round(periodPrice, 4);
                                                        }
                                                        else
                                                        {
                                                            periodPrice = b.TradeCargoInfoLeg.Rate.Value;
                                                        }
                                                        if (periodMarketPrice < periodPrice) positionDays = 0;
                                                        positionDays = positionDays * b.TradeCargoInfo.VesselIndex / 100;
                                                    }
                                                }
                                            }
                                        }
                                        positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? positionDays : -positionDays;
                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDays);
                                        monthResultsWithoutWeights.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDaysNoWeights);
                                        identifier = b.TradeCargoInfoLeg.Id + "|CARGO";
                                        break;
                                    case TradeTypeEnum.Option:
                                        positionDaysNoWeights = positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeInfo.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeInfo.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        factor = 0;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1;
                                                }
                                                else
                                                {
                                                    factor = positionDays / 30;
                                                }
                                            }
                                            else if (b.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                     b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1M / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                                else
                                                {
                                                    factor = (positionDays / 30) / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                            }
                                            else if (b.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual &&
                                                     b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                factor =
                                                    Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                                    Convert.ToDecimal(((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days) + 1);
                                            }


                                            positionDays = factor * b.TradeOptionInfo.QuantityDays;

                                            if (positionMethod.ToUpper() == "STATIC")
                                            {
                                                // Already calculated above
                                            }
                                            else if (positionMethod.ToUpper() == "DYNAMIC")
                                            {


                                                if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    if (marketSensitivityType.ToUpper() == "STRESS")
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                    }
                                                    else
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                    }
                                                    periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeInfo.PeriodFrom && a.Key <= b.TradeInfo.PeriodTo).Select(a => a.Value).Average();
                                                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                    }
                                                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                    }
                                                }
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                periodPrice = b.TradeOptionInfo.Strike;
                                                if (b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call && periodMarketPrice < periodPrice) positionDays = 0;
                                                if (b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Put && periodMarketPrice > periodPrice) positionDays = 0;
                                            }
                                            else if (positionMethod.ToUpper() == "DELTA")
                                            {
                                                if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    if (marketSensitivityType.ToUpper() == "STRESS")
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                    }
                                                    else
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                    }
                                                    periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeInfo.PeriodFrom && a.Key <= b.TradeInfo.PeriodTo).Select(a => a.Value).Average();
                                                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                    }
                                                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                    }
                                                }
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                periodPrice = b.TradeOptionInfo.Strike;
                                                if (b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call && periodMarketPrice < periodPrice) positionDays = 0;
                                                if (b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Put && periodMarketPrice > periodPrice) positionDays = 0;
                                                positionDays = !(b.TradeOptionInfo.UserDelta.HasValue) ? 0 : positionDays * b.TradeOptionInfo.UserDelta.Value;
                                            }
                                        }
                                        if (b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call)
                                        {
                                            positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                        }
                                        else
                                        {
                                            positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? positionDays : -positionDays;
                                        }
                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDays);
                                        monthResultsWithoutWeights.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDaysNoWeights);
                                        identifier = b.TradeOptionInfo.Id + "|OPTION";
                                        break;
                                }
                                currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                if (periodTo < currentDateTo) currentDateTo = periodTo;
                            }
                            tradeResults.Add(identifier, monthResults);
                            tradeResultsWithoutWeights.Add(identifier, monthResultsWithoutWeights);
                            var resultLists = new List<Dictionary<string, Dictionary<DateTime, decimal>>>();
                            resultLists.Add(tradeResults);
                            resultLists.Add(tradeResultsWithoutWeights);
                            return resultLists;
                        }).ToList();

                    allResults.Add(tradesResults.Select(a=> { return a[0]; }).ToList());
                    allResultsWithoutWeight.Add(tradesResults.Select(a => { return a[1]; }).ToList());
                }
                catch (AggregateException exc)
                {
                    foreach (Exception exception in exc.InnerExceptions)
                    {
                        HandleException(userName, exception);
                    }
                    errorMessage = exc.InnerExceptions[0].Message;
                    return null;
                }
                catch (Exception exc)
                {
                    HandleException(userName, exc);
                    errorMessage = exc.Message;
                    return null;
                }
                finally
                {
                    foreach (DataContext context in dataContexts)
                    {
                        if (context != null)
                            context.Dispose();
                    }
                }
            }

            results = allResults.Aggregate((list1, list2) => list1.Union(list2).ToList()).ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
            resultsWithoutWeight = allResultsWithoutWeight.Aggregate((list1, list2) => list1.Union(list2).ToList()).ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
            return 0;
        }

        public int? GenerateMarkToMarket(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue,
            bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType,string tradeTypeItems, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, out Dictionary<string, Dictionary<DateTime, decimal>> results,
                                         out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage)
        {
            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            return GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, optionEmbeddedValue, optionNullify, MTMType, tradeTypeItems, tradeIdentifiers, tradeIdsByPhysicalFinancialType, out results, out embeddedValues, out indexesValues, out errorMessage);
        }
        public int? GenerateMarkToMarket(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue,
            bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, out Dictionary<string, Dictionary<DateTime, decimal>> results,
                                         out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage)
        {
            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            return GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, optionEmbeddedValue, optionNullify, MTMType, tradeIdentifiers, tradeIdsByPhysicalFinancialType, out results, out embeddedValues, out indexesValues, out errorMessage);
        }

        public int? GetIndexValues(long indexId, DateTime date, int absoluteOffset, int percentageOffset, out List<IndexSpotValue> spotValues, out List<IndexFFAValue> originalBFAValues, out Dictionary<DateTime, decimal> normalizedBFAValues, out Dictionary<DateTime, decimal> curveValues)
        {
            spotValues = null;
            originalBFAValues = null;
            normalizedBFAValues = null;
            curveValues = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetIndexValues: " };
                }

                if (absoluteOffset == 0 && percentageOffset == 0)
                {
                    spotValues = Queries.GetSpotValuesByDateByIndex(dataContext, indexId, date).ToList();
                    originalBFAValues = Queries.GetBFAValuesByDateByIndex(dataContext, indexId, date).ToList();
                    try
                    {
                        curveValues = CalculateForwardCurveValues(indexId, date, 0, 0, userName, dataContext, out normalizedBFAValues);
                    }
                    catch
                    {

                    }                    
                }
                else
                {
                    spotValues = new List<IndexSpotValue>();
                    originalBFAValues = new List<IndexFFAValue>();
                    try
                    {
                        curveValues = CalculateForwardCurveValues(indexId, date, absoluteOffset, percentageOffset, userName, dataContext, out normalizedBFAValues);
                    }
                    catch
                    {

                    }                    
                }
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public void GenerateMonteCarloSimulation(string mcName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo,
                                                 string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot,
                                                 bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns)
        {
            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;

            try
            {
                //Step 1: Get MC Info
                DateTime start = DateTime.Now;

                List<TradeInformation> tradeInformations;
                List<long> tradeTcInfoLegIds =
                    tradeIdentifiers.Where(a => a.Split('|')[1] == "TC").Select(a => Convert.ToInt64(a.Split('|')[0])).
                        ToList();
                List<long> tradeFfaInfoIds =
                    tradeIdentifiers.Where(a => a.Split('|')[1] == "FFA").Select(a => Convert.ToInt64(a.Split('|')[0])).
                        ToList();
                List<long> tradeCargoInfoLegIds =
                    tradeIdentifiers.Where(a => a.Split('|')[1] == "CARGO").Select(a => Convert.ToInt64(a.Split('|')[0]))
                        .ToList();
                List<long> tradeOptionInfoIds =
                    tradeIdentifiers.Where(a => a.Split('|')[1] == "OPTION").Select(
                        a => Convert.ToInt64(a.Split('|')[0])).ToList();

                //var dataContexts = new List<DataContext>();

                DataContext dataContextRoot = null;
                var mcSimulationInfo = new MCSimulationInfo();
                try
                {
                    dataContextRoot = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };
                    //dataContexts.Add(dataContextRoot);

                    if (SiAuto.Si.Level == Level.Debug)
                    {
                        if (SiAuto.Si.GetSession(userName) == null)
                            SiAuto.Si.AddSession(userName, true);
                        dataContextRoot.Log =
                            new SmartInspectLinqToSqlAdapter(
                                SiAuto.Si.GetSession(userName))
                            {
                                TitleLimit = 0,
                                TitlePrefix = "GenerateMonteCarloSimulation: "
                            };
                    }

                    #region Trade (Get trade)
                    // All Trades with corresponding information are selected according to their Ids. Trade TC and Cargo Legs should be handled each as a different Trade. 
                    tradeInformations =
                        (from objTrade in dataContextRoot.Trades
                         from objTradeInfo in dataContextRoot.TradeInfos
                         from objTradeTCInfo in dataContextRoot.TradeTcInfos
                         from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs
                         where
                             objTrade.Id == objTradeInfo.TradeId
                             && objTrade.Type == TradeTypeEnum.TC
                             && objTrade.Status == ActivationStatusEnum.Active
                             && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                             && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                             && tradeTcInfoLegIds.Contains(objTradeTCInfoLeg.Id)
                         select
                             new TradeInformation
                             {
                                 Trade = objTrade,
                                 TradeInfo = objTradeInfo,
                                 TradeTCInfo = objTradeTCInfo,
                                 TradeTCInfoLeg = objTradeTCInfoLeg
                             }).ToList().
                            Union
                            ((
                                 from objTrade in dataContextRoot.Trades
                                 from objTradeInfo in dataContextRoot.TradeInfos
                                 from objTradeFFAInfo in dataContextRoot.TradeFfaInfos
                                 where
                                     objTrade.Id == objTradeInfo.TradeId
                                     && objTrade.Type == TradeTypeEnum.FFA
                                     && objTrade.Status == ActivationStatusEnum.Active
                                     && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                     && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)
                                 select
                                     new TradeInformation
                                     {
                                         Trade = objTrade,
                                         TradeInfo = objTradeInfo,
                                         TradeFFAInfo = objTradeFFAInfo
                                     }
                             ).ToList()).
                            Union((from objTrade in dataContextRoot.Trades
                                   from objTradeInfo in dataContextRoot.TradeInfos
                                   from objTradeCargoInfo in dataContextRoot.TradeCargoInfos
                                   from objTradeCargoInfoLeg in dataContextRoot.TradeCargoInfoLegs
                                   where
                                       objTrade.Id == objTradeInfo.TradeId
                                       && objTrade.Type == TradeTypeEnum.Cargo
                                       && objTrade.Status == ActivationStatusEnum.Active
                                       && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                       && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                       && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                   select
                                       new TradeInformation
                                       {
                                           Trade = objTrade,
                                           TradeInfo = objTradeInfo,
                                           TradeCargoInfo = objTradeCargoInfo,
                                           TradeCargoInfoLeg = objTradeCargoInfoLeg
                                       }).ToList()).Union((
                                                                  from objTrade in dataContextRoot.Trades
                                                                  from objTradeInfo in dataContextRoot.TradeInfos
                                                                  from objTradeOptionInfo in
                                                                      dataContextRoot.TradeOptionInfos
                                                                  where
                                                                      objTrade.Id == objTradeInfo.TradeId
                                                                      && objTrade.Type == TradeTypeEnum.Option
                                                                      && objTrade.Status == ActivationStatusEnum.Active
                                                                      &&
                                                                      objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                      &&
                                                                      tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                                  select
                                                                      new TradeInformation
                                                                      {
                                                                          Trade = objTrade,
                                                                          TradeInfo = objTradeInfo,
                                                                          TradeOptionInfo = objTradeOptionInfo
                                                                      }
                                                              ).ToList()
                            )
                            .ToList();
                    #endregion

                    string externalCodes = String.Join("| ", tradeInformations.Select(n => n.TradeInfo.ExternalCode).ToArray());



                    //Step 2: Save MCInfo
                    int? saveMcInfo = SaveMCInfo(dataContextRoot, userName, mcName, positionDate,
                                                 curveDate, periodFrom, periodTo, positionMethod,
                                                 marketSensitivityType, marketSensitivityValue, useSpot, optionPremium,
                                                 MTMType, externalCodes,
                                                 noOfSimulationRuns, out mcSimulationInfo);

                    //Step 3: Generate Values 
                    List<Dictionary<DateTime, decimal>> allValues;
                    Dictionary<DateTime, List<double>> monthFrequency;
                    List<double> cashBounds;

                    var generateResults = GenerateMCValues(tradeInformations, userName, mcSimulationInfo.Id, positionDate, curveDate, periodFrom, periodTo,
                                                 positionMethod, marketSensitivityType, marketSensitivityValue, useSpot,
                                                 optionPremium, MTMType, noOfSimulationRuns,
                                                 out monthFrequency, out cashBounds, out allValues);
                    if (generateResults == 0)
                    {
                        string outMessage;

                        //Update status & Save Results
                        int? saveResult = SaveMCResults(dataContextRoot, mcSimulationInfo, userName, monthFrequency, cashBounds, allValues,
                                                        out outMessage);

                        if (saveResult == 0)
                            dataContextRoot.SubmitChanges();
                        else
                        {
                            UpdateMCStatusFailed(mcSimulationInfo);

                        }
                    }
                    else
                    {
                        UpdateMCStatusFailed(mcSimulationInfo);
                    }
                }
                catch (AggregateException exc)
                {
                    UpdateMCStatusFailed(mcSimulationInfo);
                    foreach (Exception exception in exc.InnerExceptions)
                    {
                        HandleException(userName, exception);
                    }

                }
                catch (Exception exc)
                {
                    UpdateMCStatusFailed(mcSimulationInfo);
                    HandleException(userName, exc);

                }
                finally
                {
                    if (dataContextRoot != null)
                        dataContextRoot.Dispose();
                }


            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
            }
        }

        public int GenerateMCValues(List<TradeInformation> tradeInformations, string userName, long mcId, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo,
                                                 string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot,
                                                 bool optionPremium, string MTMType, int noOfSimulationRuns,
            out Dictionary<DateTime, List<double>> monthFrequency, out List<double> cashBounds, out List<Dictionary<DateTime, decimal>> allValues)
        {
            var dataContexts = new List<DataContext>();

            allValues = new List<Dictionary<DateTime, decimal>>();
            monthFrequency = new Dictionary<DateTime, List<double>>();
            cashBounds = new List<double>();

            //throw new Exception("test exception");
            try
            {
                var dataContextRoot = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };
                dataContexts.Add(dataContextRoot);

                var simulations = new List<int>();
                for (int i = 1; i <= noOfSimulationRuns; i++)
                {
                    simulations.Add(i);
                }

                //List<long> distinctIndexIds = tradeInformations.Select(a => a.TradeInfo.MTMFwdIndexId).Union(tradeInformations.Select(a => a.TradeInfo.MTMStressIndexId)).Distinct().OrderBy(a => a).ToList();
                //Cholesky stress indexes not needed in Monte Carlo 

                #region begining calculations

                List<long> distinctIndexIds =
                    tradeInformations.Select(a => a.TradeInfo.MTMFwdIndexId).Distinct().OrderBy(a => a).ToList();

                var forwardCurveValuesList = new List<List<decimal>>();
                var forwardCurveDatesList = new List<DateTime>();
                var curveValues = new Dictionary<DateTime, decimal>();
                foreach (long indexId in distinctIndexIds)
                {
                    Dictionary<DateTime, decimal> finalIndexValues;
                    curveValues = CalculateForwardCurveValues(indexId, curveDate, 0, 0, userName, dataContextRoot,
                                                              out finalIndexValues);
                    forwardCurveValuesList.Add(curveValues.OrderBy(b => b.Key).Select(b => b.Value).ToList());
                    forwardCurveDatesList = (curveValues.Count > forwardCurveDatesList.Count)
                                                ? curveValues.OrderBy(b => b.Key).Select(b => b.Key).ToList()
                                                : forwardCurveDatesList;
                }
                int maxNoOfCurveValues = forwardCurveValuesList.Max(a => a.Count);
                for (int j = 0; j <= forwardCurveValuesList.Count - 1; j++)
                {
                    if (forwardCurveValuesList[j].Count < maxNoOfCurveValues)
                    {
                        decimal valueToReplicate = forwardCurveValuesList[j][forwardCurveValuesList[j].Count - 1];
                        for (int i = forwardCurveValuesList[j].Count; i <= maxNoOfCurveValues - 1; i++)
                        {
                            forwardCurveValuesList[j].Add(valueToReplicate);
                        }
                    }
                }


                DenseMatrix curvesMatrix = Matrix.Create(distinctIndexIds.Count,
                                                         forwardCurveValuesList.First().Count);
                for (int i = 0; i <= distinctIndexIds.Count - 1; i++)
                {
                    for (int j = 0; j <= forwardCurveValuesList.First().Count - 1; j++)
                    {
                        curvesMatrix.SetValue(Convert.ToDouble(forwardCurveValuesList[i][j]), i, j);
                    }
                }

                List<RiskVolatility> riskVolatilities = (from objRiskVolatility in dataContextRoot.RiskVolatilities
                                                         where distinctIndexIds.Contains(objRiskVolatility.IndexId)
                                                         orderby objRiskVolatility.IndexId
                                                         select objRiskVolatility).ToList();

                List<RiskCorrelation> riskCorrelations =
                    (from objRiskCorrelation in dataContextRoot.RiskCorrelations
                     where distinctIndexIds.Contains(objRiskCorrelation.SourceIndexId)
                           && distinctIndexIds.Contains(objRiskCorrelation.TargetIndexId)
                     select objRiskCorrelation).ToList();


                DenseMatrix volatilityMatrix = Matrix.Create(1, distinctIndexIds.Count());
                for (int i = 0; i <= distinctIndexIds.Count - 1; i++)
                {
                    volatilityMatrix.SetValue(
                        Convert.ToDouble(
                            riskVolatilities.Where(a => a.IndexId == distinctIndexIds[i]).Select(a => a.Value).
                                Single() / 100) / Math.Sqrt(250), 0, i);
                }

                SymmetricMatrix correlationMatrix = Matrix.CreateSymmetric(distinctIndexIds.Count());
                //  List<NormalDistribution> distributionsList = new List<NormalDistribution>();
                for (int i = 0; i <= distinctIndexIds.Count - 1; i++)
                {
                    // distributionsList.Add(new NormalDistribution(0, 1));
                    for (int j = 0; j <= distinctIndexIds.Count - 1; j++)
                    {
                        double value =
                            Convert.ToDouble(
                                riskCorrelations.Where(
                                    a =>
                                    a.SourceIndexId == distinctIndexIds[i] && a.TargetIndexId == distinctIndexIds[j])
                                    .Select(a => a.Value).Single() / 100);
                        correlationMatrix.SetValue(value, i, j);
                    }
                }

                CholeskyDecomposition choleskyDecomposition = correlationMatrix.GetCholeskyDecomposition();
                var choleskyMatrix = (TriangularMatrix)choleskyDecomposition.LowerTriangularFactor;

                // MersenneTwister random = new MersenneTwister();
                // double variate = NormalDistribution.Sample(random, 0, 1);

                // Histogram h = normalDistribution.GetExpectedHistogram(3.0, 10.0, 5, 10000);

                // NumericalVariable[] variables = CorrelatedRandomNumberGenerator.Generate(10000, distributionsList.ToArray(), correlationMatrix, new MersenneTwister());

                Dictionary<long, long> ffaTradeBookPositionCalc = GetTradeBookPositionCalcByDays(tradeInformations.Where(ti => ti.Trade.Type == TradeTypeEnum.FFA).ToList()); 
                
                #endregion

                #region simulations

                allValues = simulations.
                    AsParallel().
                    WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                    WithDegreeOfParallelism(4).
                    WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                    Select(c =>
                    {
                        DenseMatrix uncorrelatedVariablesMatrix = Matrix.Create(distinctIndexIds.Count, forwardCurveValuesList.First().Count);
                        //FaureSequence sequence = new FaureSequence(distinctMarketIds.Count, forwardCurveValuesList.First().Count, true);
                        //sequence.Reset();
                        //int m = 0;
                        //while (sequence.MoveNext())
                        //{
                        //    DenseVector currentVector = sequence.Current;
                        //     for(int j = 0; j < currentVector.Length; j++)
                        //     {
                        //         uncorrelatedVariablesMatrix.SetValue(currentVector[j], j, m);
                        //     }
                        //    m++;
                        //}

                        var normalDistribution = new NormalDistribution(0, 1);
                        var randomGenerator = new MersenneTwister();
                        for (int n = 0; n <= distinctIndexIds.Count - 1; n++)
                        {
                            DenseVector samples = Vector.Create(forwardCurveValuesList.First().Count);
                            normalDistribution.Sample(randomGenerator, samples);
                            for (int j = 0; j <= forwardCurveValuesList.First().Count - 1; j++)
                            {
                                uncorrelatedVariablesMatrix.SetValue(samples[j], n, j);
                            }
                        }


                        DenseMatrix correlatedVariablesMatrix = Matrix.Create(distinctIndexIds.Count, forwardCurveValuesList.First().Count);
                        for (int j = 0; j <= distinctIndexIds.Count - 1; j++)
                        {
                            for (int n = 0; n <= forwardCurveValuesList.First().Count - 1; n++)
                            {
                                for (int k = 0; k <= j; k++)
                                {
                                    correlatedVariablesMatrix.SetValue(
                                        correlatedVariablesMatrix.GetValue(j, n) +
                                        choleskyMatrix.GetValue(j, k) * uncorrelatedVariablesMatrix.GetValue(k, n), j, n);
                                }
                            }
                        }

                        DenseMatrix pricesMatrix = Matrix.Create(distinctIndexIds.Count,
                                                                 Convert.ToInt32(Math.Floor((double)(forwardCurveDatesList.Count / 30))));
                        double prevSpot;
                        double newPrice;

                        for (int j = 0; j <= distinctIndexIds.Count - 1; j++)
                        {
                            int currentMonth = -1;
                            int monthCounter = 0;
                            double currentMonthSum = 0;
                            int currentMonthCount = 0;
                            prevSpot = curvesMatrix.GetValue(j, 0);
                            for (int t = 0; t <= forwardCurveValuesList.First().Count - 1; t++)
                            {
                                currentMonth = forwardCurveDatesList[t].Month;
                                if (t == 0)
                                {
                                    newPrice = prevSpot;
                                }
                                else
                                {
                                    newPrice = prevSpot * curvesMatrix.GetValue(j, t) / curvesMatrix.GetValue(j, t - 1) *
                                               Math.Exp(-0.5 * Math.Pow(volatilityMatrix.GetValue(0, j), 2) +
                                                        volatilityMatrix.GetValue(0, j) * correlatedVariablesMatrix.GetValue(j, t));
                                }
                                prevSpot = newPrice;
                                currentMonthSum = currentMonthSum + newPrice;
                                currentMonthCount++;

                                if (t == forwardCurveValuesList.First().Count - 1 || currentMonth != forwardCurveDatesList[t + 1].Month)
                                {
                                    if (forwardCurveDatesList[t] >= periodFrom)
                                    {
                                        pricesMatrix.SetValue(currentMonthSum / currentMonthCount, j, monthCounter);
                                        monthCounter++;
                                    }
                                    currentMonthSum = 0;
                                    currentMonthCount = 0;
                                }
                            }
                        }

                        #region tradeResults

                        List<Dictionary<string, Dictionary<DateTime, decimal>>> tradesResults = tradeInformations.
                            AsParallel().
                            WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                            WithDegreeOfParallelism(4).
                            WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                            Select(d =>
                            {
                                var dataContext2 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                                dataContexts.Add(dataContext2);


                                if (SiAuto.Si.Level == Level.Debug)
                                {
                                    if (SiAuto.Si.GetSession(userName) == null)
                                        SiAuto.Si.AddSession(userName, true);
                                    dataContext2.Log =
                                        new SmartInspectLinqToSqlAdapter(
                                            SiAuto.Si.GetSession(userName))
                                        {
                                            TitleLimit = 0,
                                            TitlePrefix = "GenerateMonteCarloSimulation:"
                                        };
                                }

                                var tradeResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                string identifier = null;
                                var monthResults = new Dictionary<DateTime, decimal>();
                                int monthCounter = -1;
                                DateTime currentDateFrom = periodFrom;
                                DateTime currentDateTo =
                                    new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                while (currentDateFrom <= periodTo)
                                {
                                    monthCounter++;
                                    decimal positionDays;
                                    decimal periodMarketPrice = 0;
                                    decimal periodPrice = 0;
                                    decimal cash = 0;
                                    Dictionary<DateTime, decimal> finalFFAValues;
                                    Dictionary<DateTime, decimal> marketPrices;

                                    switch (d.Trade.Type)
                                    {
                                        case TradeTypeEnum.TC:
                                            positionDays = (decimal)(new TimeSpan(
                                                                         Math.Max(
                                                                             Math.Min(currentDateTo.Ticks, d.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                             Math.Max(currentDateFrom.Ticks, d.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                             TimeSpan.TicksPerDay, 0))).Days;

                                            if (positionDays > 0)
                                            {

                                                if (d.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                {
                                                    if ((currentDateTo <= curveDate.Date) ||
                                                        (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                    {
                                                        periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeTCInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeTCInfoLeg.PeriodFrom && a.Date <= d.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                    }
                                                    else
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(d.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                        periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= d.TradeTCInfoLeg.PeriodFrom && a.Key <= d.TradeTCInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                    }
                                                    periodPrice = periodPrice * d.TradeTCInfoLeg.IndexPercentage.Value / 100;
                                                }
                                                else
                                                {
                                                    periodPrice = d.TradeTCInfoLeg.Rate.Value;
                                                }
                                                periodPrice = Math.Round(periodPrice, 4);
                                                if ((currentDateTo <= curveDate.Date) ||
                                                    (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodMarketPrice =
                                                        Convert.ToDecimal(
                                                            dataContext2.IndexSpotValues.Where(
                                                                a =>
                                                                a.IndexId == d.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom &&
                                                                a.Date <= currentDateTo && a.Date >= d.TradeTCInfoLeg.PeriodFrom &&
                                                                a.Date <= d.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    if (marketSensitivityType.ToUpper() == "STRESS")
                                                    {
                                                        periodMarketPrice =
                                                            Convert.ToDecimal(
                                                                pricesMatrix.GetValue(
                                                                    distinctIndexIds.IndexOf(d.TradeInfo.MTMStressIndexId),
                                                                    monthCounter));
                                                    }
                                                    else
                                                    {
                                                        periodMarketPrice =
                                                            Convert.ToDecimal(
                                                                pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMFwdIndexId),
                                                                                      monthCounter));
                                                    }

                                                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                    }
                                                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice +
                                                                            (periodMarketPrice * marketSensitivityValue / 100);
                                                    }
                                                }
                                                periodMarketPrice = periodMarketPrice * d.TradeTCInfo.VesselIndex / 100;
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                if (d.TradeTCInfoLeg.IsOptional &&
                                                    d.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                                {
                                                    positionDays = 0;
                                                }
                                                else
                                                {
                                                    if (positionMethod.ToUpper() == "STATIC")
                                                    {
                                                        // Already calculated above
                                                    }
                                                    else if (positionMethod.ToUpper() == "DYNAMIC")
                                                    {
                                                        if (!d.TradeTCInfoLeg.IsOptional || (d.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                        {
                                                            // Already calculated above
                                                        }
                                                        else if (d.TradeTCInfoLeg.IsOptional && d.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                        {
                                                            if (periodMarketPrice < periodPrice) positionDays = 0;
                                                        }
                                                    }
                                                    else if (positionMethod.ToUpper() == "DELTA")
                                                    {
                                                        // Already calculated above
                                                    }
                                                }
                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                                if (MTMType.ToUpper() == "CASH FLOW")
                                                {
                                                    cash = -positionDays * periodPrice;
                                                }
                                                else if (MTMType.ToUpper() == "P&L")
                                                {
                                                    cash = positionDays * (periodMarketPrice - periodPrice);
                                                }
                                            }
                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeTCInfoLeg.Id + "|TC";
                                            break;
                                        case TradeTypeEnum.FFA:

                                            positionDays = (decimal)(new TimeSpan(
                                                                         Math.Max(
                                                                             Math.Min(currentDateTo.Ticks, d.TradeInfo.PeriodTo.Ticks) -
                                                                             Math.Max(currentDateFrom.Ticks, d.TradeInfo.PeriodFrom.Ticks) +
                                                                             TimeSpan.TicksPerDay, 0))).Days;


                                            decimal factor = 0;

                                            if (positionDays != 0)
                                            {
                                                if (d.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty && d.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1;
                                                    }
                                                    else
                                                    {
                                                        factor = positionDays / 30;
                                                    }
                                                }
                                                else if (d.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty && d.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1M / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                    else
                                                    {
                                                        factor = (positionDays / 30) / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                }
                                                else if (d.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual && d.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    factor = Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) / Convert.ToDecimal(((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days) + 1);
                                                }

                                                //JIRA:FREIG-11
                                                //positionDays = factor * d.TradeFFAInfo.QuantityDays;
                                                //JIRA:FREIG-17
                                                //positionDays = factor * b.TradeFFAInfo.TotalDaysOfTrade;
                                                if(ffaTradeBookPositionCalc.ContainsKey(d.TradeInfo.Id))
                                                {
                                                    positionDays = ffaTradeBookPositionCalc[d.TradeInfo.Id] == 1 ? (factor * d.TradeFFAInfo.TotalDaysOfTrade) : (factor * d.TradeFFAInfo.QuantityDays);
                                                }
                                                else
                                                {
                                                    positionDays = factor * d.TradeFFAInfo.QuantityDays;
                                                }
                                                
                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                                if ((currentDateTo <= curveDate.Date) ||
                                                    (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    if (d.TradeFFAInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeFFAInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                    }
                                                    else
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeFFAInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                    }
                                                }
                                                else
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeFFAInfo.IndexId), monthCounter));
                                                }
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                periodPrice = d.TradeFFAInfo.Price;
                                                cash = positionDays * (periodMarketPrice - periodPrice);
                                            }
                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeFFAInfo.Id + "|FFA";
                                            break;
                                        case TradeTypeEnum.Cargo:

                                            positionDays = (decimal)(new TimeSpan(
                                                                         Math.Max(
                                                                             Math.Min(currentDateTo.Ticks, d.TradeCargoInfoLeg.PeriodTo.Ticks) -
                                                                             Math.Max(currentDateFrom.Ticks, d.TradeCargoInfoLeg.PeriodFrom.Ticks) +
                                                                             TimeSpan.TicksPerDay, 0))).Days;

                                            if (positionDays > 0)
                                            {

                                                if (d.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                {
                                                    if ((currentDateTo <= curveDate.Date) ||
                                                        (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                    {
                                                        periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeCargoInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeCargoInfoLeg.PeriodFrom && a.Date <= d.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                    }
                                                    else
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(d.TradeCargoInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                        periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= d.TradeCargoInfoLeg.PeriodFrom && a.Key <= d.TradeCargoInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                    }
                                                    periodPrice = periodPrice * d.TradeCargoInfoLeg.IndexPercentage.Value / 100;
                                                }
                                                else
                                                {
                                                    periodPrice = d.TradeCargoInfoLeg.Rate.Value;
                                                }
                                                periodPrice = Math.Round(periodPrice, 4);
                                                if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeCargoInfoLeg.PeriodFrom && a.Date <= d.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    if (marketSensitivityType.ToUpper() == "STRESS")
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMStressIndexId), monthCounter));
                                                    }
                                                    else
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMFwdIndexId), monthCounter));
                                                    }
                                                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                    }
                                                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                    }
                                                }

                                                periodMarketPrice = periodMarketPrice * d.TradeCargoInfo.VesselIndex / 100;
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                if (d.TradeCargoInfoLeg.IsOptional &&
                                                    d.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                                {
                                                    positionDays = 0;
                                                }
                                                else
                                                {
                                                    if (positionMethod.ToUpper() == "STATIC")
                                                    {
                                                        // Already calculated above
                                                    }
                                                    else if (positionMethod.ToUpper() == "DYNAMIC")
                                                    {
                                                        if (!d.TradeCargoInfoLeg.IsOptional ||
                                                            (d.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                        {
                                                            // Already calculated above
                                                        }
                                                        else if (d.TradeCargoInfoLeg.IsOptional &&
                                                                 d.TradeCargoInfoLeg.OptionalStatus ==
                                                                 TradeInfoLegOptionalStatusEnum.Pending)
                                                        {
                                                            if (periodMarketPrice < periodPrice) positionDays = 0;
                                                        }
                                                    }
                                                    else if (positionMethod.ToUpper() == "DELTA")
                                                    {
                                                        // Already calculated above
                                                    }
                                                }
                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                                   ? positionDays
                                                                   : -positionDays;
                                                if (MTMType.ToUpper() == "CASH FLOW")
                                                {
                                                    cash = -positionDays * periodPrice;
                                                }
                                                else if (MTMType.ToUpper() == "P&L")
                                                {
                                                    cash = positionDays * (periodMarketPrice - periodPrice);
                                                }
                                            }
                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeCargoInfoLeg.Id + "|CARGO";
                                            break;
                                        case TradeTypeEnum.Option:
                                            positionDays = (decimal)(new TimeSpan(
                                                                         Math.Max(
                                                                             Math.Min(currentDateTo.Ticks, d.TradeInfo.PeriodTo.Ticks) -
                                                                             Math.Max(currentDateFrom.Ticks, d.TradeInfo.PeriodFrom.Ticks) +
                                                                             TimeSpan.TicksPerDay, 0))).Days;

                                            factor = 0;

                                            if (positionDays > 0)
                                            {
                                                if (d.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty && d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1;
                                                    }
                                                    else
                                                    {
                                                        factor = positionDays / 30;
                                                    }
                                                }
                                                else if (d.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty && d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1M / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                    else
                                                    {
                                                        factor = (positionDays / 30) / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                }
                                                else if (d.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual && d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    factor = Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) / Convert.ToDecimal(((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days) + 1);
                                                }


                                                positionDays = factor * d.TradeOptionInfo.QuantityDays;

                                                if (MTMType.ToUpper() == "CASH FLOW")
                                                {
                                                    if (positionDays != 0)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) ||
                                                            (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            if (d.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeOptionInfo.IndexId), monthCounter));
                                                        }
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        cash = positionDays * ((d.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call) ? Math.Max(0, periodMarketPrice - d.TradeOptionInfo.Strike) : Math.Max(0, d.TradeOptionInfo.Strike - periodMarketPrice));
                                                        cash = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                    }
                                                }
                                                else if (MTMType.ToUpper() == "P&L")
                                                {
                                                    if (positionDays != 0)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) ||
                                                            (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            if (d.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeOptionInfo.IndexId), monthCounter));
                                                        }
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        decimal T;
                                                        decimal T2;
                                                        T = ((currentDateTo - positionDate).Days + 1) / 365M;
                                                        T2 = ((currentDateTo - currentDateFrom).Days + 1) / 365M;

                                                        decimal mtd = 0;
                                                        if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date))
                                                        {
                                                            try
                                                            {
                                                                mtd = dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average();
                                                            }
                                                            catch (Exception)
                                                            {
                                                                throw new ApplicationException(
                                                                    "Failed to find proper Index Spot Value for MTD calculation");
                                                            }

                                                        }

                                                        Dictionary<DateTime, decimal> marketVolatilities = CalculateForwardVolatilityValues(d.TradeOptionInfo.IndexId, curveDate, userName, dataContext2);
                                                        decimal periodVolatility = marketVolatilities.Where(a => a.Key >= currentDateFrom).OrderBy(a => a.Key).First().Value;

                                                        InterestReferencePeriodTypeEnum? interestReferencePeriodType = null;
                                                        if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 25 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 35)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Month1;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 55 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 65)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months2;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 85 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 95)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months3;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 115 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 125)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months4;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 145 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 155)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months5;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 175 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 185)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months6;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 205 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 215)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months7;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 235 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 245)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months8;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 265 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 275)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months9;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 295 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 305)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months10;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 325 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 335)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months11;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 6 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 8)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Week1;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 13 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 15)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks2;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 20 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 22)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks3;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 355 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 370)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year1;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 720 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 740)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year2;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 1080 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 1110)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year3;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 1810 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 1840)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year5;
                                                        else if (
                                                            (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 2530 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 2580)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year7;
                                                        else if (
                                                            (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 3620 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 3680)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year10;
                                                        else if (
                                                            (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 7250 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 7350)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year20;
                                                        else if
                                                            (
                                                            (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 10900 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 11000)
                                                            interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year30;


                                                        var interestRate =
                                                            dataContext2.InterestReferenceRates.Where(rt1 => rt1.Type == InterestReferenceRateTypeEnum.RiskFree && rt1.PeriodType == interestReferencePeriodType && rt1.Date <= positionDate)
                                                                .OrderByDescending(rt1 => rt1.Date).FirstOrDefault();
                                                        if (interestRate == null)
                                                        {
                                                            throw new ApplicationException(
                                                                "Failed to find proper Interest Reference Rate for OPTION Trade: " +
                                                                d.TradeOptionInfo.Id);
                                                        }

                                                        cash =
                                                            Asian_TurnbullWakeman(userName, d.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call, Convert.ToDouble(
                                                                                      periodMarketPrice), Convert.ToDouble(mtd), Convert.ToDouble(d.TradeOptionInfo.Strike), Convert.ToDouble(T),
                                                                                  Convert.ToDouble(T2), Convert.ToDouble(interestRate.Rate / 100), 0, Convert.ToDouble(periodVolatility / 100)) - d.TradeOptionInfo.Premium;
                                                        cash = positionDays * cash;
                                                        cash = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                    }
                                                }

                                            }

                                            // Premium calculation
                                            if (MTMType.ToUpper() == "CASH FLOW" && optionPremium &&
                                                currentDateFrom <= d.TradeInfo.SignDate.Date && currentDateTo >= d.TradeInfo.SignDate.Date)
                                            {
                                                if (d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    positionDays = d.TradeOptionInfo.QuantityDays;
                                                }
                                                else
                                                {
                                                    positionDays = 30 * (d.TradeInfo.PeriodTo.Month + 1 - d.TradeInfo.PeriodFrom.Month);
                                                }

                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                                   ? -positionDays
                                                                   : positionDays;
                                                cash = cash + positionDays * d.TradeOptionInfo.Premium;
                                            }

                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeOptionInfo.Id + "|OPTION";
                                            break;
                                    }
                                    currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                    currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                    if (periodTo < currentDateTo) currentDateTo = periodTo;
                                }
                                tradeResults.Add(identifier, monthResults);
                                return tradeResults;
                            }).ToList();

                        #endregion

                        var cashPerMonthDict = new Dictionary<DateTime, decimal>();
                        Dictionary<string, Dictionary<DateTime, decimal>> tradeResultsDict =
                            tradesResults.ToDictionary(tradesResult => tradesResult.First().Key, tradesResult => (tradesResult.First().Value));
                        List<DateTime> months = tradeResultsDict.First().Value.Keys.ToList();
                        foreach (DateTime month in months)
                        {
                            decimal cashSumPerMonth = tradeResultsDict.Aggregate<KeyValuePair<string, Dictionary<DateTime, decimal>>, decimal>(0, (current1, keyValuePair) => keyValuePair.Value.Where(valuePair => valuePair.Key == month).Aggregate(current1, (current, valuePair) => current + valuePair.Value));
                            cashPerMonthDict.Add(month, cashSumPerMonth);
                        }
                        return cashPerMonthDict;
                    }).ToList();

                #endregion

                #region calculating Values

                decimal minimumPrice = allValues.Select(a => a.Values.Min()).Min();
                decimal maximumPrice = allValues.Select(a => a.Values.Max()).Max();

                if (minimumPrice == maximumPrice)
                {
                    throw new ApplicationException("Monte Carlo Simulation: Minimum Value = Mamimum Value");
                }

                var histograms = new Dictionary<DateTime, Histogram>();
                List<DateTime> months2 = allValues.First().Keys.ToList();

                foreach (DateTime month2 in months2)
                {
                    // TODO. Algorithm for Bin number and capacity calculation
                    var histogram = new Histogram((double)minimumPrice, (double)maximumPrice, 10);

                    foreach (var finalPrice in allValues)
                    {
                        histogram.Increment((double)finalPrice[month2]);
                    }
                    histograms.Add(month2, histogram);
                }

                foreach (var histogram in histograms)
                {
                    var monthValues = new List<double>();
                    foreach (HistogramBin histogramBin in histogram.Value.Bins)
                    {
                        monthValues.Add(histogramBin.Value);
                    }
                    monthFrequency.Add(histogram.Key, monthValues);
                }

                foreach (HistogramBin histogramBin in histograms.First().Value.Bins)
                {
                    if (!cashBounds.Exists(a => a == histogramBin.LowerBound))
                        cashBounds.Add(histogramBin.LowerBound);
                    if (!cashBounds.Exists(a => a == histogramBin.UpperBound))
                        cashBounds.Add(histogramBin.UpperBound);
                }
                cashBounds = cashBounds.OrderBy(a => a).ToList();

                #endregion

                return 0;
            }
            catch (Exception exc)
            {

                HandleException(userName, exc);
                return -1;
            }
            finally
            {
                foreach (DataContext dataContext in dataContexts)
                {
                    if (dataContext != null)
                        dataContext.Dispose();
                }
            }
        }

        private Dictionary<long, long> GetTradeBookPositionCalcByDays(List<TradeInformation> ffaTrades)
        {
            
            DataContext dataContext = null;
            string userName = null;
            Dictionary<long, long> tradeBookPositionCalcByDays = new Dictionary<long, long>();
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "UpdateTrade: " };
                }
                foreach(TradeInformation tradeInformation in ffaTrades)
                {
                    string sql = @"select t.pos_calc_by_days from books t,trade_info_books tib where tib.book_id = t.id and  tib.trade_info_id = " +
                             tradeInformation.TradeInfo.Id + @" and t.PARENT_BOOK_ID is null";

                    long posCalcByDays = dataContext.ExecuteQuery<long>(sql).FirstOrDefault();
                    tradeBookPositionCalcByDays.Add(tradeInformation.TradeInfo.Id, posCalcByDays);
                }
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }

            return tradeBookPositionCalcByDays;
        }

        public void UpdateMCStatusFailed(MCSimulationInfo mcSimulationInfo)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "UpdateTrade: " };
                }
                //update status with failed.
                //Update Simulations Info
                DateTime now = DateTime.Now;

                mcSimulationInfo.Status = MCStatusEnum.Failed;
                mcSimulationInfo.EndTime = now;
                dataContext.MCSimulationInfo.Attach(mcSimulationInfo, true);
                dataContext.SubmitChanges();
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? SaveMCInfo(DataContext dataContext, string userName, string mcName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, string externalCodes,
            int noOfSimulationRuns, out MCSimulationInfo mcSimulationInfo)
        {
            //Save to Simulation Info


            //Update Simulations Info
            DateTime now = DateTime.Now;

            //Table MCRuns
            mcSimulationInfo = new MCSimulationInfo();
            mcSimulationInfo.Id = dataContext.GetNextId(typeof(MCSimulationInfo));
            mcSimulationInfo.Name = mcName;
            mcSimulationInfo.Status = MCStatusEnum.Running;
            mcSimulationInfo.StartTime = now;

            mcSimulationInfo.PeriodFrom = periodFrom;
            mcSimulationInfo.PeriodTo = periodTo;
            mcSimulationInfo.CurveDate = curveDate;
            mcSimulationInfo.NoOfRuns = noOfSimulationRuns;
            mcSimulationInfo.PosMethod = positionMethod;
            mcSimulationInfo.MSensType = marketSensitivityType;
            mcSimulationInfo.MSensValue = marketSensitivityValue;
            mcSimulationInfo.UseSpot = Convert.ToInt16((useSpot) ? 1 : 0);
            mcSimulationInfo.IsOptionPremium = Convert.ToInt16((optionPremium) ? 1 : 0); ;
            mcSimulationInfo.MTMType = MTMType;
            mcSimulationInfo.Cruser = userName;
            mcSimulationInfo.Crd = now;


            mcSimulationInfo.ExternalCodes = externalCodes;

            try
            {
                dataContext.MCSimulationInfo.InsertOnSubmit(mcSimulationInfo);
                dataContext.SubmitChanges();
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
            }

            return 0;
        }

        public int? SaveMCResults(DataContext dataContext, MCSimulationInfo mcSimulationInfo, string userName, Dictionary<DateTime, List<double>> monthFrequency,
                                                 List<double> cashBounds, List<Dictionary<DateTime, decimal>> allValues, out string outMessage)
        {
            try
            {
                var message = "";
                //var result = InsertMCRuns(out message);

                DateTime now = DateTime.Now;

                MCSimulationInfo mcSimulationInfoToUpdate = dataContext.MCSimulationInfo.Where(n => n.Id == mcSimulationInfo.Id).FirstOrDefault();
                //Update Simulations Info
                mcSimulationInfoToUpdate.Status = MCStatusEnum.Completed;
                mcSimulationInfoToUpdate.EndTime = now;

                //dataContext.MCSimulationInfo.Attach(mcSimulationInfo, true);
                //dataContext.MCSimulationInfo


                ////Table MCRuns
                //var mcSimulationInfo = new MCSimulationInfo();
                //mcSimulationInfo.Id = dataContext.GetNextId(typeof(MCSimulationInfo));
                //mcSimulationInfo.Name = mcName;
                //mcSimulationInfo.ExternalCodes = "from trade Identifiers loop and get the codes";
                //mcSimulationInfo.Status = MCStatusEnum.Running;
                //mcSimulationInfo.StartTime = startTime;
                //mcSimulationInfo.EndTime = endTime;
                //mcSimulationInfo.PeriodFrom = periodFrom;
                //mcSimulationInfo.PeriodTo = periodTo;
                //mcSimulationInfo.CurveDate = curveDate;
                //mcSimulationInfo.NoOfRuns = noOfSimulationRuns;
                //mcSimulationInfo.PosMethod = positionMethod;
                //mcSimulationInfo.MSensType = marketSensitivityType;
                //mcSimulationInfo.MSensValue = marketSensitivityValue;
                //mcSimulationInfo.UseSpot = Convert.ToInt16((useSpot)? 1 : 0);
                //mcSimulationInfo.IsOptionPremium = Convert.ToInt16((optionPremium) ? 1 : 0); ;
                //mcSimulationInfo.MTMType = MTMType;
                //mcSimulationInfo.Cruser = userName;
                //mcSimulationInfo.Crd = now;
                //dataContext.MCSimulationInfo.InsertOnSubmit(mcSimulationInfo);


                //Table MCMonthFrequency
                var mcMonthFrequences = new List<MCMonthFrequency>();
                foreach (var mf in monthFrequency)
                {
                    short hist_no = 0;
                    foreach (var value in mf.Value)
                    {
                        var mcMonthFrequency = new MCMonthFrequency();
                        mcMonthFrequency.Id = dataContext.GetNextId(typeof(MCMonthFrequency));
                        mcMonthFrequency.Date = mf.Key;
                        mcMonthFrequency.MCSimId = mcSimulationInfo.Id;
                        mcMonthFrequency.HistNo = hist_no;
                        mcMonthFrequency.Value = value;
                        hist_no++;

                        dataContext.MCMonthFrequency.InsertOnSubmit(mcMonthFrequency);
                        mcMonthFrequences.Add(mcMonthFrequency);
                    }
                }


                //Table MCCashBounds
                var mcCashBounds = new List<MCCashBounds>();
                foreach (var cb in cashBounds)
                {
                    var mcCashBound = new MCCashBounds();
                    mcCashBound.Id = dataContext.GetNextId(typeof(MCCashBounds));
                    mcCashBound.MCSimId = mcSimulationInfo.Id;
                    mcCashBound.Value = cb;

                    dataContext.MCCashBounds.InsertOnSubmit(mcCashBound);
                    mcCashBounds.Add(mcCashBound);

                }

                //Table MCAllValues
                short sim_no = 0;
                var mcAllValues = new List<MCAllValues>();
                foreach (var mcSimValuesPerDate in allValues)
                {
                    foreach (KeyValuePair<DateTime, decimal> d in mcSimValuesPerDate)
                    {
                        var mcAllValue = new MCAllValues();
                        mcAllValue.Id = dataContext.GetNextId(typeof(MCAllValues));
                        mcAllValue.SimNo = sim_no;
                        mcAllValue.MCSimId = mcSimulationInfo.Id;
                        mcAllValue.Date = d.Key;
                        mcAllValue.Value = d.Value;

                        dataContext.MCAllValues.InsertOnSubmit(mcAllValue);
                        mcAllValues.Add(mcAllValue);
                    }

                    sim_no++;
                }

                outMessage = message;
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                outMessage = exc.Message;
                return null;
            }

        }



        public int? GenerateMonteCarloSimulation_BCK(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns, out Dictionary<DateTime, List<double>> monthFrequency,
                                                 out List<double> cashBounds, out List<Dictionary<DateTime, decimal>> allValues)
        {
            allValues = new List<Dictionary<DateTime, decimal>>();
            monthFrequency = new Dictionary<DateTime, List<double>>();
            cashBounds = new List<double>();
            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            var simulations = new List<int>();
            for (int i = 1; i <= noOfSimulationRuns; i++)
            {
                simulations.Add(i);
            }
            List<TradeInformation> tradeInformations;

            List<long> tradeTcInfoLegIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "TC").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();
            List<long> tradeFfaInfoIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "FFA").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();
            List<long> tradeCargoInfoLegIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "CARGO").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();
            List<long> tradeOptionInfoIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "OPTION").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();

            var dataContexts = new List<DataContext>();

            try
            {
                var dataContextRoot = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                dataContexts.Add(dataContextRoot);

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null)
                        SiAuto.Si.AddSession(userName, true);
                    dataContextRoot.Log =
                        new SmartInspectLinqToSqlAdapter(
                            SiAuto.Si.GetSession(userName))
                        {
                            TitleLimit = 0,
                            TitlePrefix = "GenerateMonteCarloSimulation: "
                        };
                }

                // All Trades with corresponding information are selected according to their Ids. Trade TC and Cargo Legs should be handled each as a different Trade. 

                tradeInformations =
                    (from objTrade in dataContextRoot.Trades
                     from objTradeInfo in dataContextRoot.TradeInfos
                     from objTradeTCInfo in dataContextRoot.TradeTcInfos
                     from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs
                     where
                         objTrade.Id == objTradeInfo.TradeId
                         && objTrade.Type == TradeTypeEnum.TC
                         && objTrade.Status == ActivationStatusEnum.Active
                         && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                         && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                         && tradeTcInfoLegIds.Contains(objTradeTCInfoLeg.Id)
                     select
                         new TradeInformation
                         {
                             Trade = objTrade,
                             TradeInfo = objTradeInfo,
                             TradeTCInfo = objTradeTCInfo,
                             TradeTCInfoLeg = objTradeTCInfoLeg
                         }).ToList().
                        Union
                        ((
                             from objTrade in dataContextRoot.Trades
                             from objTradeInfo in dataContextRoot.TradeInfos
                             from objTradeFFAInfo in dataContextRoot.TradeFfaInfos
                             from objTradeInfoBook in dataContextRoot.TradeInfoBooks
                             from objBook in dataContextRoot.Books

                             where
                                 objTrade.Id == objTradeInfo.TradeId
                                 && objTrade.Type == TradeTypeEnum.FFA
                                 && objTrade.Status == ActivationStatusEnum.Active
                                 && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                 && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)

                                  && objTradeInfoBook.TradeInfoId == objTradeInfo.Id
                                  && objBook.Id == objTradeInfoBook.BookId
                                  && objBook.ParentBookId == null

                             select
                                 new TradeInformation
                                 {
                                     Trade = objTrade,
                                     TradeInfo = objTradeInfo,
                                     TradeFFAInfo = objTradeFFAInfo,
                                     BookPositionCalcByDays = objBook.PositionCalculatedByDays
                                 }
                         ).ToList()).
                        Union((from objTrade in dataContextRoot.Trades
                               from objTradeInfo in dataContextRoot.TradeInfos
                               from objTradeCargoInfo in dataContextRoot.TradeCargoInfos
                               from objTradeCargoInfoLeg in dataContextRoot.TradeCargoInfoLegs
                               where
                                   objTrade.Id == objTradeInfo.TradeId
                                   && objTrade.Type == TradeTypeEnum.Cargo
                                   && objTrade.Status == ActivationStatusEnum.Active
                                   && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                   && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                   && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                               select
                                   new TradeInformation
                                   {
                                       Trade = objTrade,
                                       TradeInfo = objTradeInfo,
                                       TradeCargoInfo = objTradeCargoInfo,
                                       TradeCargoInfoLeg = objTradeCargoInfoLeg
                                   }).ToList()).Union((
                                                              from objTrade in dataContextRoot.Trades
                                                              from objTradeInfo in dataContextRoot.TradeInfos
                                                              from objTradeOptionInfo in dataContextRoot.TradeOptionInfos
                                                              where
                                                                  objTrade.Id == objTradeInfo.TradeId
                                                                  && objTrade.Type == TradeTypeEnum.Option
                                                                  && objTrade.Status == ActivationStatusEnum.Active
                                                                  && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                  && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                              select
                                                                  new TradeInformation
                                                                  {
                                                                      Trade = objTrade,
                                                                      TradeInfo = objTradeInfo,
                                                                      TradeOptionInfo = objTradeOptionInfo
                                                                  }
                                                          ).ToList()
                        )
                        .ToList();

                //List<long> distinctIndexIds = tradeInformations.Select(a => a.TradeInfo.MTMFwdIndexId).Union(tradeInformations.Select(a => a.TradeInfo.MTMStressIndexId)).Distinct().OrderBy(a => a).ToList();
                //Cholesky stress indexes not needed in Monte Carlo 
                List<long> distinctIndexIds =
                    tradeInformations.Select(a => a.TradeInfo.MTMFwdIndexId).Distinct().OrderBy(a => a).ToList();

                var forwardCurveValuesList = new List<List<decimal>>();
                var forwardCurveDatesList = new List<DateTime>();
                var curveValues = new Dictionary<DateTime, decimal>();
                foreach (long indexId in distinctIndexIds)
                {
                    Dictionary<DateTime, decimal> finalIndexValues;
                    curveValues = CalculateForwardCurveValues(indexId, curveDate, 0, 0, userName, dataContextRoot, out finalIndexValues);
                    forwardCurveValuesList.Add(curveValues.OrderBy(b => b.Key).Select(b => b.Value).ToList());
                    forwardCurveDatesList = (curveValues.Count > forwardCurveDatesList.Count) ? curveValues.OrderBy(b => b.Key).Select(b => b.Key).ToList() : forwardCurveDatesList;
                }
                int maxNoOfCurveValues = forwardCurveValuesList.Max(a => a.Count);
                for (int j = 0; j <= forwardCurveValuesList.Count - 1; j++)
                {
                    if (forwardCurveValuesList[j].Count < maxNoOfCurveValues)
                    {
                        decimal valueToReplicate = forwardCurveValuesList[j][forwardCurveValuesList[j].Count - 1];
                        for (int i = forwardCurveValuesList[j].Count; i <= maxNoOfCurveValues - 1; i++)
                        {
                            forwardCurveValuesList[j].Add(valueToReplicate);
                        }
                    }
                }


                DenseMatrix curvesMatrix = Matrix.Create(distinctIndexIds.Count, forwardCurveValuesList.First().Count);
                for (int i = 0; i <= distinctIndexIds.Count - 1; i++)
                {
                    for (int j = 0; j <= forwardCurveValuesList.First().Count - 1; j++)
                    {
                        curvesMatrix.SetValue(Convert.ToDouble(forwardCurveValuesList[i][j]), i, j);
                    }
                }

                List<RiskVolatility> riskVolatilities = (from objRiskVolatility in dataContextRoot.RiskVolatilities
                                                         where distinctIndexIds.Contains(objRiskVolatility.IndexId)
                                                         orderby objRiskVolatility.IndexId
                                                         select objRiskVolatility).ToList();

                List<RiskCorrelation> riskCorrelations = (from objRiskCorrelation in dataContextRoot.RiskCorrelations
                                                          where distinctIndexIds.Contains(objRiskCorrelation.SourceIndexId)
                                                                && distinctIndexIds.Contains(objRiskCorrelation.TargetIndexId)
                                                          select objRiskCorrelation).ToList();


                DenseMatrix volatilityMatrix = Matrix.Create(1, distinctIndexIds.Count());
                for (int i = 0; i <= distinctIndexIds.Count - 1; i++)
                {
                    volatilityMatrix.SetValue(Convert.ToDouble(riskVolatilities.Where(a => a.IndexId == distinctIndexIds[i]).Select(a => a.Value).Single() / 100) / Math.Sqrt(250), 0, i);
                }

                SymmetricMatrix correlationMatrix = Matrix.CreateSymmetric(distinctIndexIds.Count());
                //  List<NormalDistribution> distributionsList = new List<NormalDistribution>();
                for (int i = 0; i <= distinctIndexIds.Count - 1; i++)
                {
                    // distributionsList.Add(new NormalDistribution(0, 1));
                    for (int j = 0; j <= distinctIndexIds.Count - 1; j++)
                    {
                        double value = Convert.ToDouble(riskCorrelations.Where(a => a.SourceIndexId == distinctIndexIds[i] && a.TargetIndexId == distinctIndexIds[j]).Select(a => a.Value).Single() / 100);
                        correlationMatrix.SetValue(value, i, j);
                    }
                }

                CholeskyDecomposition choleskyDecomposition = correlationMatrix.GetCholeskyDecomposition();
                var choleskyMatrix = (TriangularMatrix)choleskyDecomposition.LowerTriangularFactor;

                // MersenneTwister random = new MersenneTwister();
                // double variate = NormalDistribution.Sample(random, 0, 1);

                // Histogram h = normalDistribution.GetExpectedHistogram(3.0, 10.0, 5, 10000);

                // NumericalVariable[] variables = CorrelatedRandomNumberGenerator.Generate(10000, distributionsList.ToArray(), correlationMatrix, new MersenneTwister());


                allValues = simulations.
                    AsParallel().
                    WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                    WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                    Select(c =>
                    {
                        DenseMatrix uncorrelatedVariablesMatrix = Matrix.Create(distinctIndexIds.Count, forwardCurveValuesList.First().Count);
                        //FaureSequence sequence = new FaureSequence(distinctMarketIds.Count, forwardCurveValuesList.First().Count, true);
                        //sequence.Reset();
                        //int m = 0;
                        //while (sequence.MoveNext())
                        //{
                        //    DenseVector currentVector = sequence.Current;
                        //     for(int j = 0; j < currentVector.Length; j++)
                        //     {
                        //         uncorrelatedVariablesMatrix.SetValue(currentVector[j], j, m);
                        //     }
                        //    m++;
                        //}

                        var normalDistribution = new NormalDistribution(0, 1);
                        var randomGenerator = new MersenneTwister();
                        for (int n = 0; n <= distinctIndexIds.Count - 1; n++)
                        {
                            DenseVector samples = Vector.Create(forwardCurveValuesList.First().Count);
                            normalDistribution.Sample(randomGenerator, samples);
                            for (int j = 0; j <= forwardCurveValuesList.First().Count - 1; j++)
                            {
                                uncorrelatedVariablesMatrix.SetValue(samples[j], n, j);
                            }
                        }


                        DenseMatrix correlatedVariablesMatrix = Matrix.Create(distinctIndexIds.Count, forwardCurveValuesList.First().Count);
                        for (int j = 0; j <= distinctIndexIds.Count - 1; j++)
                        {
                            for (int n = 0; n <= forwardCurveValuesList.First().Count - 1; n++)
                            {
                                for (int k = 0; k <= j; k++)
                                {
                                    correlatedVariablesMatrix.SetValue(correlatedVariablesMatrix.GetValue(j, n) + choleskyMatrix.GetValue(j, k) * uncorrelatedVariablesMatrix.GetValue(k, n), j, n);
                                }
                            }
                        }

                        DenseMatrix pricesMatrix = Matrix.Create(distinctIndexIds.Count, Convert.ToInt32(Math.Floor((double)(forwardCurveDatesList.Count / 30))));
                        double prevSpot;
                        double newPrice;

                        for (int j = 0; j <= distinctIndexIds.Count - 1; j++)
                        {
                            int currentMonth = -1;
                            int monthCounter = 0;
                            double currentMonthSum = 0;
                            int currentMonthCount = 0;
                            prevSpot = curvesMatrix.GetValue(j, 0);
                            for (int t = 0; t <= forwardCurveValuesList.First().Count - 1; t++)
                            {
                                currentMonth = forwardCurveDatesList[t].Month;
                                if (t == 0)
                                {
                                    newPrice = prevSpot;
                                }
                                else
                                {
                                    newPrice = prevSpot * curvesMatrix.GetValue(j, t) / curvesMatrix.GetValue(j, t - 1) * Math.Exp(-0.5 * Math.Pow(volatilityMatrix.GetValue(0, j), 2) + volatilityMatrix.GetValue(0, j) * correlatedVariablesMatrix.GetValue(j, t));
                                }
                                prevSpot = newPrice;
                                currentMonthSum = currentMonthSum + newPrice;
                                currentMonthCount++;

                                if (t == forwardCurveValuesList.First().Count - 1 || currentMonth != forwardCurveDatesList[t + 1].Month)
                                {
                                    if (forwardCurveDatesList[t] >= periodFrom)
                                    {
                                        pricesMatrix.SetValue(currentMonthSum / currentMonthCount, j, monthCounter);
                                        monthCounter++;
                                    }
                                    currentMonthSum = 0;
                                    currentMonthCount = 0;
                                }
                            }
                        }

                        List<Dictionary<string, Dictionary<DateTime, decimal>>> tradesResults = tradeInformations.
                            AsParallel().
                            WithExecutionMode(ParallelExecutionMode.ForceParallelism).
             WithDegreeOfParallelism(4).
                            WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                            Select(d =>
                            {
                                var dataContext2 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                                dataContexts.Add(dataContext2);

                                if (SiAuto.Si.Level == Level.Debug)
                                {
                                    if (SiAuto.Si.GetSession(userName) == null)
                                        SiAuto.Si.AddSession(userName, true);
                                    dataContext2.Log =
                                                   new SmartInspectLinqToSqlAdapter(
                                                       SiAuto.Si.GetSession(userName))
                                                   {
                                                       TitleLimit = 0,
                                                       TitlePrefix = "GenerateMonteCarloSimulation:"
                                                   };
                                }

                                var tradeResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                string identifier = null;
                                var monthResults = new Dictionary<DateTime, decimal>();
                                int monthCounter = -1;
                                DateTime currentDateFrom = periodFrom;
                                DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                while (currentDateFrom <= periodTo)
                                {
                                    monthCounter++;
                                    decimal positionDays;
                                    decimal periodMarketPrice = 0;
                                    decimal periodPrice = 0;
                                    decimal cash = 0;
                                    Dictionary<DateTime, decimal> finalFFAValues;
                                    Dictionary<DateTime, decimal> marketPrices;

                                    switch (d.Trade.Type)
                                    {
                                        case TradeTypeEnum.TC:
                                            positionDays = (decimal)(new TimeSpan(
                                                                                    Math.Max(
                                                                                        Math.Min(currentDateTo.Ticks, d.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                                        Math.Max(currentDateFrom.Ticks, d.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                                        TimeSpan.TicksPerDay, 0))).Days;

                                            if (positionDays > 0)
                                            {

                                                if (d.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                {
                                                    if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                    {
                                                        periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeTCInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeTCInfoLeg.PeriodFrom && a.Date <= d.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                    }
                                                    else
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(d.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                        periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= d.TradeTCInfoLeg.PeriodFrom && a.Key <= d.TradeTCInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                    }
                                                    periodPrice = periodPrice * d.TradeTCInfoLeg.IndexPercentage.Value / 100;
                                                }
                                                else
                                                {
                                                    periodPrice = d.TradeTCInfoLeg.Rate.Value;
                                                }
                                                periodPrice = Math.Round(periodPrice, 4);
                                                if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeTCInfoLeg.PeriodFrom && a.Date <= d.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    if (marketSensitivityType.ToUpper() == "STRESS")
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMStressIndexId), monthCounter));
                                                    }
                                                    else
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMFwdIndexId), monthCounter));
                                                    }

                                                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                    }
                                                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                    }
                                                }
                                                periodMarketPrice = periodMarketPrice * d.TradeTCInfo.VesselIndex / 100;
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                if (d.TradeTCInfoLeg.IsOptional && d.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                                {
                                                    positionDays = 0;
                                                }
                                                else
                                                {
                                                    if (positionMethod.ToUpper() == "STATIC")
                                                    {
                                                                   // Already calculated above
                                                               }
                                                    else if (positionMethod.ToUpper() == "DYNAMIC")
                                                    {
                                                        if (!d.TradeTCInfoLeg.IsOptional || (d.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                        {
                                                                       // Already calculated above
                                                                   }
                                                        else if (d.TradeTCInfoLeg.IsOptional && d.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                        {
                                                            if (periodMarketPrice < periodPrice) positionDays = 0;
                                                        }
                                                    }
                                                    else if (positionMethod.ToUpper() == "DELTA")
                                                    {
                                                                   // Already calculated above
                                                               }
                                                }
                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                                if (MTMType.ToUpper() == "CASH FLOW")
                                                {
                                                    cash = -positionDays * periodPrice;
                                                }
                                                else if (MTMType.ToUpper() == "P&L")
                                                {
                                                    cash = positionDays * (periodMarketPrice - periodPrice);
                                                }
                                            }
                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeTCInfoLeg.Id + "|TC";
                                            break;
                                        case TradeTypeEnum.FFA:

                                            positionDays = (decimal)(new TimeSpan(
                                                                                    Math.Max(
                                                                                        Math.Min(currentDateTo.Ticks, d.TradeInfo.PeriodTo.Ticks) -
                                                                                        Math.Max(currentDateFrom.Ticks, d.TradeInfo.PeriodFrom.Ticks) +
                                                                                        TimeSpan.TicksPerDay, 0))).Days;


                                            decimal factor = 0;

                                            if (positionDays != 0)
                                            {
                                                if (d.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                               d.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1;
                                                    }
                                                    else
                                                    {
                                                        factor = positionDays / 30;
                                                    }
                                                }
                                                else if (d.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                                    d.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1M / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                    else
                                                    {
                                                        factor = (positionDays / 30) / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                }
                                                else if (d.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual &&
                                                                    d.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    factor =
                                                                   Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                                                   Convert.ToDecimal(((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days) + 1);
                                                }


                                                //JIRA:FREIG-11
                                                //positionDays = factor * d.TradeFFAInfo.QuantityDays;
                                                //JIRA:FREIG-17
                                                //positionDays = factor * b.TradeFFAInfo.TotalDaysOfTrade;
                                                positionDays = d.BookPositionCalcByDays == BookPositionCalculatedByDaysEnum.Total ? (factor * d.TradeFFAInfo.TotalDaysOfTrade) : (factor * d.TradeFFAInfo.QuantityDays);
                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                                if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    if (d.TradeFFAInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeFFAInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                    }
                                                    else
                                                    {
                                                        periodMarketPrice =
                                                                       Convert.ToDecimal(
                                                                           dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeFFAInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                    }
                                                }
                                                else
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeFFAInfo.IndexId), monthCounter));
                                                }
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                periodPrice = d.TradeFFAInfo.Price;
                                                cash = positionDays * (periodMarketPrice - periodPrice);
                                            }
                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeFFAInfo.Id + "|FFA";
                                            break;
                                        case TradeTypeEnum.Cargo:

                                            positionDays = (decimal)(new TimeSpan(
                                                                                    Math.Max(
                                                                                        Math.Min(currentDateTo.Ticks, d.TradeCargoInfoLeg.PeriodTo.Ticks) -
                                                                                        Math.Max(currentDateFrom.Ticks, d.TradeCargoInfoLeg.PeriodFrom.Ticks) +
                                                                                        TimeSpan.TicksPerDay, 0))).Days;

                                            if (positionDays > 0)
                                            {

                                                if (d.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                {
                                                    if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                    {
                                                        periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeCargoInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeCargoInfoLeg.PeriodFrom && a.Date <= d.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                    }
                                                    else
                                                    {
                                                        marketPrices = CalculateForwardCurveValues(d.TradeCargoInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                        periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= d.TradeCargoInfoLeg.PeriodFrom && a.Key <= d.TradeCargoInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                    }
                                                    periodPrice = periodPrice * d.TradeCargoInfoLeg.IndexPercentage.Value / 100;
                                                }
                                                else
                                                {
                                                    periodPrice = d.TradeCargoInfoLeg.Rate.Value;
                                                }
                                                periodPrice = Math.Round(periodPrice, 4);
                                                if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeCargoInfoLeg.PeriodFrom && a.Date <= d.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    if (marketSensitivityType.ToUpper() == "STRESS")
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMStressIndexId), monthCounter));
                                                    }
                                                    else
                                                    {
                                                        periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeInfo.MTMFwdIndexId), monthCounter));
                                                    }
                                                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                    }
                                                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                    {
                                                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                    }
                                                }

                                                periodMarketPrice = periodMarketPrice * d.TradeCargoInfo.VesselIndex / 100;
                                                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                if (d.TradeCargoInfoLeg.IsOptional && d.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                                {
                                                    positionDays = 0;
                                                }
                                                else
                                                {
                                                    if (positionMethod.ToUpper() == "STATIC")
                                                    {
                                                                   // Already calculated above
                                                               }
                                                    else if (positionMethod.ToUpper() == "DYNAMIC")
                                                    {
                                                        if (!d.TradeCargoInfoLeg.IsOptional || (d.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                        {
                                                                       // Already calculated above
                                                                   }
                                                        else if (d.TradeCargoInfoLeg.IsOptional && d.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                        {
                                                            if (periodMarketPrice < periodPrice) positionDays = 0;
                                                        }
                                                    }
                                                    else if (positionMethod.ToUpper() == "DELTA")
                                                    {
                                                                   // Already calculated above
                                                               }
                                                }
                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? positionDays : -positionDays;
                                                if (MTMType.ToUpper() == "CASH FLOW")
                                                {
                                                    cash = -positionDays * periodPrice;
                                                }
                                                else if (MTMType.ToUpper() == "P&L")
                                                {
                                                    cash = positionDays * (periodMarketPrice - periodPrice);
                                                }
                                            }
                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeCargoInfoLeg.Id + "|CARGO";
                                            break;
                                        case TradeTypeEnum.Option:
                                            positionDays = (decimal)(new TimeSpan(
                                                                                    Math.Max(
                                                                                        Math.Min(currentDateTo.Ticks, d.TradeInfo.PeriodTo.Ticks) -
                                                                                        Math.Max(currentDateFrom.Ticks, d.TradeInfo.PeriodFrom.Ticks) +
                                                                                        TimeSpan.TicksPerDay, 0))).Days;

                                            factor = 0;

                                            if (positionDays > 0)
                                            {
                                                if (d.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                               d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1;
                                                    }
                                                    else
                                                    {
                                                        factor = positionDays / 30;
                                                    }
                                                }
                                                else if (d.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                                    d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                    {
                                                        factor = 1M / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                    else
                                                    {
                                                        factor = (positionDays / 30) / (d.TradeInfo.PeriodTo.Month - d.TradeInfo.PeriodFrom.Month + 1);
                                                    }
                                                }
                                                else if (d.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual &&
                                                                    d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    factor =
                                                                   Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                                                   Convert.ToDecimal(((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days) + 1);
                                                }


                                                positionDays = factor * d.TradeOptionInfo.QuantityDays;

                                                if (MTMType.ToUpper() == "CASH FLOW")
                                                {
                                                    if (positionDays != 0)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            if (d.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice =
                                                                               Convert.ToDecimal(
                                                                                   dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).
                                                                                       Average());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeOptionInfo.IndexId), monthCounter));
                                                        }
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        cash = positionDays * ((d.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call) ? Math.Max(0, periodMarketPrice - d.TradeOptionInfo.Strike) : Math.Max(0, d.TradeOptionInfo.Strike - periodMarketPrice));
                                                        cash = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                    }
                                                }
                                                else if (MTMType.ToUpper() == "P&L")
                                                {
                                                    if (positionDays != 0)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            if (d.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice =
                                                                               Convert.ToDecimal(
                                                                                   dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).
                                                                                       Average());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            periodMarketPrice = Convert.ToDecimal(pricesMatrix.GetValue(distinctIndexIds.IndexOf(d.TradeOptionInfo.IndexId), monthCounter));
                                                        }
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        decimal T;
                                                        decimal T2;
                                                        T = ((currentDateTo - positionDate).Days + 1) / 365M;
                                                        T2 = ((currentDateTo - currentDateFrom).Days + 1) / 365M;

                                                        decimal mtd = 0;
                                                        if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date))
                                                        {
                                                            try
                                                            {
                                                                mtd = dataContext2.IndexSpotValues.Where(a => a.IndexId == d.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date <= currentDateTo && a.Date >= d.TradeInfo.PeriodFrom && a.Date <= d.TradeInfo.PeriodTo).Select(a => a.Rate).Average();
                                                            }
                                                            catch (Exception)
                                                            {
                                                                throw new ApplicationException("Failed to find proper Index Spot Value for MTD calculation");
                                                            }

                                                        }

                                                        Dictionary<DateTime, decimal> marketVolatilities = CalculateForwardVolatilityValues(d.TradeOptionInfo.IndexId, curveDate, userName, dataContext2);
                                                        decimal periodVolatility = marketVolatilities.Where(a => a.Key >= currentDateFrom).OrderBy(a => a.Key).First().Value;

                                                        InterestReferencePeriodTypeEnum? interestReferencePeriodType = null;
                                                        if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 25 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 35) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Month1;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 55 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 65) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months2;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 85 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 95) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months3;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 115 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 125) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months4;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 145 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 155) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months5;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 175 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 185) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months6;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 205 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 215) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months7;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 235 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 245) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months8;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 265 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 275) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months9;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 295 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 305) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months10;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 325 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 335) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months11;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 6 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 8) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Week1;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 13 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 15) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks2;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 20 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 22) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks3;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 355 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 370) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year1;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 720 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 740) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year2;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 1080 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 1110) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year3;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 1810 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 1840) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year5;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 2530 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 2580) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year7;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 3620 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 3680) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year10;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 7250 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 7350) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year20;
                                                        else if ((d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days >= 10900 && (d.TradeInfo.PeriodTo - d.TradeInfo.PeriodFrom).Days <= 11000) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year30;


                                                        var interestRate = dataContext2.InterestReferenceRates.Where(rt1 => rt1.Type == InterestReferenceRateTypeEnum.RiskFree && rt1.PeriodType == interestReferencePeriodType && rt1.Date <= positionDate).OrderByDescending(rt1 => rt1.Date).FirstOrDefault();
                                                        if (interestRate == null)
                                                        {
                                                            throw new ApplicationException("Failed to find proper Interest Reference Rate for OPTION Trade: " + d.TradeOptionInfo.Id);
                                                        }

                                                        cash =
                                                                       Asian_TurnbullWakeman(userName,
                                                                                             d.TradeOptionInfo.
                                                                                                 Type ==
                                                                                             TradeOPTIONInfoTypeEnum
                                                                                                 .Call,
                                                                                             Convert.ToDouble(
                                                                                                 periodMarketPrice),
                                                                                             Convert.ToDouble(mtd),
                                                                                             Convert.ToDouble(
                                                                                                 d.
                                                                                                     TradeOptionInfo
                                                                                                     .Strike),
                                                                                             Convert.ToDouble(T),
                                                                                             Convert.ToDouble(T2),
                                                                                             Convert.ToDouble(
                                                                                                 interestRate.
                                                                                                     Rate / 100), 0,
                                                                                             Convert.ToDouble(
                                                                                                 periodVolatility /
                                                                                                 100)) -
                                                                       d.TradeOptionInfo.Premium;
                                                        cash = positionDays * cash;
                                                        cash = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                    }
                                                }

                                            }

                                                       // Premium calculation
                                                       if (MTMType.ToUpper() == "CASH FLOW" && optionPremium && currentDateFrom <= d.TradeInfo.SignDate.Date && currentDateTo >= d.TradeInfo.SignDate.Date)
                                            {
                                                if (d.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                {
                                                    positionDays = d.TradeOptionInfo.QuantityDays;
                                                }
                                                else
                                                {
                                                    positionDays = 30 * (d.TradeInfo.PeriodTo.Month + 1 - d.TradeInfo.PeriodFrom.Month);
                                                }

                                                positionDays = d.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                                cash = cash + positionDays * d.TradeOptionInfo.Premium;
                                            }

                                            monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                            identifier = d.TradeOptionInfo.Id + "|OPTION";
                                            break;
                                    }
                                    currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                    currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                    if (periodTo < currentDateTo) currentDateTo = periodTo;
                                }
                                tradeResults.Add(identifier, monthResults);
                                return tradeResults;
                            }).ToList();

                        var cashPerMonthDict = new Dictionary<DateTime, decimal>();
                        Dictionary<string, Dictionary<DateTime, decimal>> tradeResultsDict = tradesResults.ToDictionary(tradesResult => tradesResult.First().Key, tradesResult => (tradesResult.First().Value));
                        List<DateTime> months = tradeResultsDict.First().Value.Keys.ToList();
                        foreach (DateTime month in months)
                        {
                            decimal cashSumPerMonth = tradeResultsDict.Aggregate<KeyValuePair<string, Dictionary<DateTime, decimal>>, decimal>(0, (current1, keyValuePair) => keyValuePair.Value.Where(valuePair => valuePair.Key == month).Aggregate(current1, (current, valuePair) => current + valuePair.Value));
                            cashPerMonthDict.Add(month, cashSumPerMonth);
                        }
                        return cashPerMonthDict;
                    }).ToList();

                decimal minimumPrice = allValues.Select(a => a.Values.Min()).Min();
                decimal maximumPrice = allValues.Select(a => a.Values.Max()).Max();

                if (minimumPrice == maximumPrice)
                {
                    throw new ApplicationException("Monte Carlo Simulation: Minimum Value = Mamimum Value");
                }

                var histograms = new Dictionary<DateTime, Histogram>();
                List<DateTime> months2 = allValues.First().Keys.ToList();

                foreach (DateTime month2 in months2)
                {
                    // TODO. Algorithm for Bin number and capacity calculation
                    var histogram = new Histogram((double)minimumPrice, (double)maximumPrice, 10);

                    foreach (var finalPrice in allValues)
                    {
                        histogram.Increment((double)finalPrice[month2]);
                    }
                    histograms.Add(month2, histogram);
                }

                foreach (var histogram in histograms)
                {
                    var monthValues = new List<double>();
                    foreach (HistogramBin histogramBin in histogram.Value.Bins)
                    {
                        monthValues.Add(histogramBin.Value);
                    }
                    monthFrequency.Add(histogram.Key, monthValues);
                }

                foreach (HistogramBin histogramBin in histograms.First().Value.Bins)
                {
                    if (!cashBounds.Exists(a => a == histogramBin.LowerBound)) cashBounds.Add(histogramBin.LowerBound);
                    if (!cashBounds.Exists(a => a == histogramBin.UpperBound)) cashBounds.Add(histogramBin.UpperBound);
                }
                cashBounds = cashBounds.OrderBy(a => a).ToList();
            }
            catch (AggregateException exc)
            {
                foreach (Exception exception in exc.InnerExceptions)
                {
                    HandleException(userName, exception);
                }
                return null;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                foreach (DataContext dataContext in dataContexts)
                {
                    if (dataContext != null)
                        dataContext.Dispose();
                }
            }

            return 0;
        }

        public int? GetAverageIndexesValues(List<long> indexes, DateTime curveDate, DateTime periodFrom, DateTime periodTo, out Dictionary<long, Dictionary<DateTime, decimal>> forwardIndexesValues, out Dictionary<long, Dictionary<DateTime, decimal>> spotIndexesValues)
        {
            forwardIndexesValues = null;
            spotIndexesValues = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetAverageIndexesValues: " };
                }

                var forwardValues = new Dictionary<long, Dictionary<DateTime, decimal>>();
                var spotValues = new Dictionary<long, Dictionary<DateTime, decimal>>();

                Dictionary<DateTime, decimal> finalFFAValues;
                Dictionary<DateTime, decimal> forwardPrices;
                List<IndexSpotValue> indexSpotPrices;
                Dictionary<DateTime, decimal> spotPrices;

                foreach (var index in indexes)
                {
                    spotPrices = new Dictionary<DateTime, decimal>();
                    if (periodFrom < curveDate)
                    {
                        indexSpotPrices = dataContext.IndexSpotValues.Where(
                                a => a.IndexId == index && a.Date >= periodFrom && a.Date < curveDate).OrderBy(a => a.Date).ToList();
                        foreach (var indexSpotValue in indexSpotPrices)
                        {
                            spotPrices.Add(indexSpotValue.Date, indexSpotValue.Rate);
                        }
                        spotValues.Add(index, spotPrices);
                    }
                    forwardPrices = CalculateForwardCurveValues(index, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                    forwardValues.Add(index, forwardPrices);
                }

                forwardIndexesValues = forwardValues;
                spotIndexesValues = spotValues;

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        #endregion
        private int? GenerateMarkToMarket(string userName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType,
                                         out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indxValues, out string errorMessage)
        {
            return GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, optionEmbeddedValue, optionNullify, MTMType, "1,2,3,4,5", tradeIdentifiers, tradeIdsByPhysicalFinancialType, out  results, out  embeddedValues, out  indxValues,out errorMessage);
        }
        private int? GenerateMarkToMarket(string userName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, string tradeTypeItems, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType,
                                     out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indxValues, out string errorMessage)
        {
            errorMessage = "";
            results = null;
            embeddedValues = null;
            indxValues = null;

            DataContext dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
            if (SiAuto.Si.Level == Level.Debug)
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GenerateMarkToMarket: " };
            }

            List<Index> indexes = dataContext.Indexes.ToList();

            var listOfTradeIdBatches = new List<List<string>>();
            int skip = 0;
            while (true)
            {
                List<string> tradeIdsBatch = tradeIdentifiers.Skip(skip).Take(500).ToList();
                if (tradeIdsBatch.Count == 0) break;
                listOfTradeIdBatches.Add(tradeIdsBatch);
                skip = skip + tradeIdsBatch.Count;
            }

            var allResults = new List<List<Dictionary<string, Dictionary<DateTime, decimal>>>>();
            var allEmbeddedValues = new List<List<Dictionary<string, decimal>>>();
            var allIndexesValues = new Dictionary<DateTime, Dictionary<string, decimal>>();

            foreach (var listOfTradeIdBatch in listOfTradeIdBatches)
            {
                var dataContexts = new List<DataContext>();

                List<TradeInformation> tradeInformations;

                List<long> tradeTcInfoLegIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.TC).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "TC").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeFfaInfoIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.FFA).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "FFA").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeCargoInfoLegIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.Cargo).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "CARGO").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeOptionInfoIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.Put).ToString()) || tradeTypeItems.Contains(TradeTypeWithTypeOptionInfoEnum.Call.ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "OPTION").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();

                try
                {
                    var dataContextRoot = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                    dataContexts.Add(dataContextRoot);


                    if (SiAuto.Si.Level == Level.Debug)
                    {
                        if (SiAuto.Si.GetSession(userName) == null)
                            SiAuto.Si.AddSession(userName, true);
                        dataContextRoot.Log =
                            new SmartInspectLinqToSqlAdapter(
                                SiAuto.Si.GetSession(userName))
                            {
                                TitleLimit = 0,
                                TitlePrefix = "GenerateMarkToMarket: "
                            };
                    }

                    tradeInformations =
                        (from objTrade in dataContextRoot.Trades
                         from objTradeInfo in dataContextRoot.TradeInfos
                         from objTradeTCInfo in dataContextRoot.TradeTcInfos
                         from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs
                         where
                             objTrade.Id == objTradeInfo.TradeId
                             && objTrade.Type == TradeTypeEnum.TC
                             && objTrade.Status == ActivationStatusEnum.Active
                             && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                             && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                             && tradeTcInfoLegIds.Contains(objTradeTCInfoLeg.Id)
                         select
                             new TradeInformation
                             {
                                 Trade = objTrade,
                                 TradeInfo = objTradeInfo,
                                 TradeTCInfo = objTradeTCInfo,
                                 TradeTCInfoLeg = objTradeTCInfoLeg
                             }).ToList().
                            Union
                            ((
                                 from objTrade in dataContextRoot.Trades
                                 from objTradeInfo in dataContextRoot.TradeInfos
                                 from objTradeFFAInfo in dataContextRoot.TradeFfaInfos
                                 from objTradeInfoBook in dataContextRoot.TradeInfoBooks
                                 from objBook in dataContextRoot.Books
                                 where
                                     objTrade.Id == objTradeInfo.TradeId
                                     && objTrade.Type == TradeTypeEnum.FFA
                                     && objTrade.Status == ActivationStatusEnum.Active
                                     && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                     && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)

                                     && objTradeInfoBook.TradeInfoId == objTradeInfo.Id
                                           && objBook.Id == objTradeInfoBook.BookId
                                           && objBook.ParentBookId == null
                                 select
                                     new TradeInformation
                                     {
                                         Trade = objTrade,
                                         TradeInfo = objTradeInfo,
                                         TradeFFAInfo = objTradeFFAInfo,
                                         BookPositionCalcByDays = objBook.PositionCalculatedByDays
                                     }
                             ).ToList()).
                            Union((from objTrade in dataContextRoot.Trades
                                   from objTradeInfo in dataContextRoot.TradeInfos
                                   from objTradeCargoInfo in dataContextRoot.TradeCargoInfos
                                   from objTradeCargoInfoLeg in dataContextRoot.TradeCargoInfoLegs
                                   where
                                       objTrade.Id == objTradeInfo.TradeId
                                       && objTrade.Type == TradeTypeEnum.Cargo
                                       && objTrade.Status == ActivationStatusEnum.Active
                                       && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                       && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                       && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                   select
                                       new TradeInformation
                                       {
                                           Trade = objTrade,
                                           TradeInfo = objTradeInfo,
                                           TradeCargoInfo = objTradeCargoInfo,
                                           TradeCargoInfoLeg = objTradeCargoInfoLeg
                                       }).ToList()).Union((
                                                                  from objTrade in dataContextRoot.Trades
                                                                  from objTradeInfo in dataContextRoot.TradeInfos
                                                                  from objTradeOptionInfo in dataContextRoot.TradeOptionInfos
                                                                  where
                                                                      objTrade.Id == objTradeInfo.TradeId
                                                                      && objTrade.Type == TradeTypeEnum.Option
                                                                      && objTrade.Status == ActivationStatusEnum.Active
                                                                      && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                      && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                                  select
                                                                      new TradeInformation
                                                                      {
                                                                          Trade = objTrade,
                                                                          TradeInfo = objTradeInfo,
                                                                          TradeOptionInfo = objTradeOptionInfo
                                                                      }
                                                              ).ToList()
                            )
                            .ToList();

                    #region Embedded Values Calculation

                    if (optionEmbeddedValue)
                    {
                        List<Dictionary<string, decimal>> tradesEmbeddedValues = tradeInformations.Where(a => (a.Trade.Type == TradeTypeEnum.TC && a.TradeTCInfoLeg.IsOptional) || (a.Trade.Type == TradeTypeEnum.Cargo && a.TradeCargoInfoLeg.IsOptional)).ToList().
                            AsParallel().
                            WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                            WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                            Select(b =>
                            {
                                var dataContext2 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                                dataContexts.Add(dataContext2);


                                if (SiAuto.Si.Level == Level.Debug)
                                {
                                    if (SiAuto.Si.GetSession(userName) == null)
                                        SiAuto.Si.AddSession(userName, true);
                                    dataContext2.Log =
                                        new SmartInspectLinqToSqlAdapter(
                                            SiAuto.Si.GetSession(userName))
                                        {
                                            TitleLimit = 0,
                                            TitlePrefix = "GenerateMarkToMarket: "
                                        };
                                }

                                var tradeEmbeddedValue = new Dictionary<string, decimal>();
                                decimal periodMarketPrice;
                                decimal periodPrice;
                                Dictionary<DateTime, decimal> finalFFAValues;
                                Dictionary<DateTime, decimal> marketPrices;
                                decimal days;
                                Dictionary<DateTime, decimal> marketVolatilities;
                                decimal periodVolatility;
                                InterestReferencePeriodTypeEnum? interestReferencePeriodType = null;
                                // Option Calculator
                                switch (b.Trade.Type)
                                {
                                    case TradeTypeEnum.TC:
                                        days = (b.TradeTCInfoLeg.PeriodTo.Date - b.TradeTCInfoLeg.PeriodFrom.Date).Days + 1;

                                        if (b.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                        {
                                            if ((b.TradeTCInfoLeg.PeriodTo.Date <= curveDate.Date) || (b.TradeTCInfoLeg.PeriodTo.Date > curveDate && b.TradeTCInfoLeg.PeriodTo.Date < DateTime.Now.Date && useSpot))
                                            {
                                                periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeTCInfoLeg.IndexId.Value && a.Date >= periodFrom && a.Date <= periodTo && a.Date >= b.TradeTCInfoLeg.PeriodFrom.Date && a.Date <= b.TradeTCInfoLeg.PeriodTo.Date).Select(a => a.Rate).Average());
                                            }
                                            else
                                            {
                                                marketPrices = CalculateForwardCurveValues(b.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                periodPrice = marketPrices.Where(a => a.Key >= b.TradeTCInfoLeg.PeriodFrom.Date && a.Key <= b.TradeTCInfoLeg.PeriodTo.Date && a.Key >= periodFrom && a.Key <= periodTo).Select(a => a.Value).Average();
                                            }

                                            periodPrice = periodPrice * b.TradeTCInfoLeg.IndexPercentage.Value / 100;
                                        }
                                        else
                                        {
                                            periodPrice = b.TradeTCInfoLeg.Rate.Value;
                                        }
                                        periodPrice = Math.Round(periodPrice, 4);
                                        if ((b.TradeTCInfoLeg.PeriodTo.Date <= curveDate.Date) || (b.TradeTCInfoLeg.PeriodTo.Date > curveDate && b.TradeTCInfoLeg.PeriodTo.Date < DateTime.Now.Date && useSpot))
                                        {
                                            periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= periodFrom && a.Date <= periodTo && a.Date >= b.TradeTCInfoLeg.PeriodFrom.Date && a.Date <= b.TradeTCInfoLeg.PeriodTo.Date).Select(a => a.Rate).Average());
                                        }
                                        else
                                        {
                                            if (marketSensitivityType.ToUpper() == "STRESS")
                                            {
                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                            }
                                            else
                                            {
                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                            }
                                            periodMarketPrice = marketPrices.Where(a => a.Key >= b.TradeTCInfoLeg.PeriodFrom.Date && a.Key <= b.TradeTCInfoLeg.PeriodTo.Date && a.Key >= periodFrom && a.Key <= periodTo).Select(a => a.Value).Average();
                                            if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                            {
                                                periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                            }
                                            else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                            {
                                                periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                            }
                                        }

                                        periodMarketPrice = periodMarketPrice * b.TradeTCInfo.VesselIndex / 100;
                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);

                                        marketVolatilities = CalculateForwardVolatilityValues(b.TradeInfo.MTMFwdIndexId, curveDate, userName, dataContext2);
                                        periodVolatility = marketVolatilities.Where(a => a.Key >= b.TradeTCInfoLeg.PeriodFrom).OrderBy(a => a.Key).First().Value;

                                        if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 25 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 35) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Month1;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 55 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 65) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months2;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 85 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 95) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months3;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 115 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 125) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months4;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 145 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 155) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months5;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 175 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 185) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months6;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 205 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 215) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months7;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 235 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 245) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months8;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 265 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 275) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months9;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 295 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 305) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months10;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 325 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 335) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months11;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 6 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 8) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Week1;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 13 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 15) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks2;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 20 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 22) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks3;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 355 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 370) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year1;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 720 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 740) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year2;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 1080 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 1110) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year3;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 1810 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 1840) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year5;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 2530 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 2580) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year7;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 3620 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 3680) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year10;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 7250 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 7350) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year20;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 10900 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 11000) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year30;

                                        var interestRate = dataContext2.InterestReferenceRates.Where(rt1 => rt1.Type == InterestReferenceRateTypeEnum.RiskFree && rt1.PeriodType == interestReferencePeriodType && rt1.Date <= positionDate).OrderByDescending(rt1 => rt1.Date).FirstOrDefault();
                                        if (interestRate == null)
                                        {
                                            throw new ApplicationException("Failed to find proper Interest Reference Rate for TC Trade: " + b.TradeTCInfoLeg.Id);
                                        }

                                        tradeEmbeddedValue.Add(b.TradeTCInfoLeg.Id + "|TC",
                                                               b76CallValue(userName,
                                                                            Convert.ToDouble(
                                                                                periodMarketPrice),
                                                                            Convert.ToDouble(periodPrice),
                                                                            Convert.ToDouble(
                                                                                interestRate.Rate / 100),
                                                                            Convert.ToDouble(
                                                                                periodVolatility / 100),
                                                                            Convert.ToDouble(
                                                                                ((b.TradeTCInfoLeg.PeriodFrom
                                                                                      .Date - positionDate).
                                                                                     Days + 1) / 365)) * days *
                                                               b.TradeTCInfo.VesselIndex / 100);
                                        break;
                                    case TradeTypeEnum.Cargo:
                                        days = (b.TradeCargoInfoLeg.PeriodTo.Date - b.TradeCargoInfoLeg.PeriodFrom.Date).Days + 1;

                                        if (b.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                        {
                                            if ((b.TradeCargoInfoLeg.PeriodTo.Date <= curveDate.Date) || (b.TradeCargoInfoLeg.PeriodTo.Date > curveDate && b.TradeCargoInfoLeg.PeriodTo.Date < DateTime.Now.Date && useSpot))
                                            {
                                                periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeCargoInfoLeg.IndexId.Value && a.Date >= periodFrom && a.Date <= periodTo && a.Date >= b.TradeCargoInfoLeg.PeriodFrom.Date && a.Date <= b.TradeCargoInfoLeg.PeriodTo.Date).Select(a => a.Rate).Average());
                                            }
                                            else
                                            {
                                                marketPrices = CalculateForwardCurveValues(b.TradeCargoInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                periodPrice = marketPrices.Where(a => a.Key >= b.TradeCargoInfoLeg.PeriodFrom.Date && a.Key <= b.TradeCargoInfoLeg.PeriodTo.Date && a.Key >= periodFrom && a.Key <= periodTo).Select(a => a.Value).Average();
                                            }
                                            periodPrice = periodPrice * b.TradeCargoInfoLeg.IndexPercentage.Value / 100;
                                        }
                                        else
                                        {
                                            periodPrice = b.TradeCargoInfoLeg.Rate.Value;
                                        }
                                        periodPrice = Math.Round(periodPrice, 4);
                                        if ((b.TradeCargoInfoLeg.PeriodTo.Date <= curveDate.Date) || (b.TradeCargoInfoLeg.PeriodTo.Date > curveDate && b.TradeCargoInfoLeg.PeriodTo.Date < DateTime.Now.Date && useSpot))
                                        {
                                            periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= periodFrom && a.Date <= periodTo && a.Date >= b.TradeCargoInfoLeg.PeriodFrom.Date && a.Date <= b.TradeCargoInfoLeg.PeriodTo.Date).Select(a => a.Rate).Average());
                                        }
                                        else
                                        {
                                            if (marketSensitivityType.ToUpper() == "STRESS")
                                            {
                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                            }
                                            else
                                            {
                                                marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                            }
                                            periodMarketPrice = marketPrices.Where(a => a.Key >= b.TradeCargoInfoLeg.PeriodFrom.Date && a.Key <= b.TradeCargoInfoLeg.PeriodTo.Date && a.Key >= periodFrom && a.Key <= periodTo).Select(a => a.Value).Average();

                                            if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                            {
                                                periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                            }
                                            else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                            {
                                                periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                            }
                                        }

                                        periodMarketPrice = periodMarketPrice * b.TradeCargoInfo.VesselIndex / 100;
                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                        marketVolatilities = CalculateForwardVolatilityValues(b.TradeInfo.MTMFwdIndexId, curveDate, userName, dataContext2);
                                        periodVolatility = marketVolatilities.Where(a => a.Key >= b.TradeCargoInfoLeg.PeriodFrom).OrderBy(a => a.Key).First().Value;


                                        if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 25 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 35) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Month1;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 55 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 65) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months2;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 85 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 95) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months3;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 115 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 125) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months4;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 145 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 155) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months5;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 175 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 185) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months6;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 205 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 215) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months7;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 235 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 245) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months8;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 265 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 275) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months9;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 295 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 305) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months10;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 325 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 335) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months11;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 6 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 8) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Week1;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 13 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 15) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks2;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 20 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 22) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks3;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 355 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 370) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year1;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 720 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 740) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year2;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 1080 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 1110) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year3;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 1810 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 1840) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year5;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 2530 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 2580) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year7;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 3620 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 3680) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year10;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 7250 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 7350) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year20;
                                        else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 10900 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 11000) interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year30;

                                        var interestRate2 = dataContext2.InterestReferenceRates.Where(rt1 => rt1.Type == InterestReferenceRateTypeEnum.RiskFree && rt1.PeriodType == interestReferencePeriodType && rt1.Date <= positionDate).OrderByDescending(rt1 => rt1.Date).FirstOrDefault();
                                        if (interestRate2 == null)
                                        {
                                            throw new ApplicationException("Failed to find proper Interest Reference Rate for CARGO Trade: " + b.TradeCargoInfoLeg.Id);
                                        }
                                        tradeEmbeddedValue.Add(b.TradeCargoInfoLeg.Id + "|CARGO",
                                                               b76CallValue(userName,
                                                                            Convert.ToDouble(
                                                                                periodMarketPrice),
                                                                            Convert.ToDouble(periodPrice),
                                                                            Convert.ToDouble(
                                                                                interestRate2.Rate / 100),
                                                                            Convert.ToDouble(
                                                                                periodVolatility / 100),
                                                                            Convert.ToDouble(
                                                                                ((b.TradeCargoInfoLeg.
                                                                                      PeriodFrom.Date -
                                                                                  positionDate).Days + 1) / 365)) *
                                                               days * b.TradeCargoInfo.VesselIndex / 100);
                                        break;
                                }

                                return tradeEmbeddedValue;
                            }).ToList();
                        allEmbeddedValues.Add(tradesEmbeddedValues);
                    }

                    #endregion


                    //List<MTMReturnValue> tradesResults = tradeInformations.Select(b =>
                    List<MTMReturnValue> tradesResults = tradeInformations.
                        AsParallel().
                        WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                        WithMergeOptions(ParallelMergeOptions.FullyBuffered).Select(b =>
                        {
                            var dataContext3 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                            dataContexts.Add(dataContext3);


                            if (SiAuto.Si.Level == Level.Debug)
                            {
                                if (SiAuto.Si.GetSession(userName) == null)
                                    SiAuto.Si.AddSession(userName, true);
                                dataContext3.Log =
                                    new SmartInspectLinqToSqlAdapter(
                                        SiAuto.Si.GetSession(userName))
                                    {
                                        TitleLimit = 0,
                                        TitlePrefix = "GenerateMarkToMarket: "
                                    };
                            }

                            var currentIndexValues = new Dictionary<DateTime, Dictionary<string, decimal>>();
                            var tradeResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                            string identifier = null;
                            var monthResults = new Dictionary<DateTime, decimal>();

                            DateTime currentDateFrom = periodFrom;
                            DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);

                            if (periodTo < currentDateTo) currentDateTo = periodTo;

                            while (currentDateFrom <= periodTo)
                            {

                                //get last bunisess date 
                                DateTime lastBusinessDate = GetLastBusinessDay(currentDateTo.Year, currentDateTo.Month);

                                decimal positionDays;
                                decimal periodMarketPrice = 0;
                                decimal periodPrice = 0;
                                decimal cash = 0;
                                Dictionary<DateTime, decimal> finalFFAValues;
                                Dictionary<DateTime, decimal> marketPrices;

                                decimal indexValue = 0;
                                string indexName = "";


                                switch (b.Trade.Type)
                                {
                                    #region TC
                                    case TradeTypeEnum.TC:
                                        positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        if (marketSensitivityType.ToUpper() == "STRESS")
                                            indexName = indexes.Single(a => a.Id == b.TradeInfo.MTMStressIndexId).Name;
                                        else
                                            indexName = indexes.Single(a => a.Id == b.TradeInfo.MTMFwdIndexId).Name;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                            {
                                                if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeTCInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeTCInfoLeg.PeriodFrom && a.Date <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                    periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeTCInfoLeg.PeriodFrom && a.Key <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                }
                                                periodPrice = periodPrice * b.TradeTCInfoLeg.IndexPercentage.Value / 100;
                                            }
                                            else
                                            {
                                                periodPrice = b.TradeTCInfoLeg.Rate.Value;
                                            }
                                            periodPrice = Math.Round(periodPrice, 4);


                                            if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                            {
                                                periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeTCInfoLeg.PeriodFrom && a.Date <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Rate).Average());

                                                indexValue = periodMarketPrice;
                                            }
                                            else
                                            {
                                                if (marketSensitivityType.ToUpper() == "STRESS")
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                }
                                                else
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                }
                                                periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeTCInfoLeg.PeriodFrom && a.Key <= b.TradeTCInfoLeg.PeriodTo).Select(a => a.Value).Average();

                                                indexValue = periodMarketPrice;

                                                if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                {
                                                    periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                }
                                                else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                {
                                                    periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                }
                                            }

                                            periodMarketPrice = periodMarketPrice * b.TradeTCInfo.VesselIndex / 100;
                                            periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                            if (b.TradeTCInfoLeg.IsOptional && b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                            {
                                                positionDays = 0;
                                            }
                                            else
                                            {
                                                if (positionMethod.ToUpper() == "STATIC")
                                                {
                                                    // Already calculated above
                                                }
                                                else if (positionMethod.ToUpper() == "DYNAMIC")
                                                {
                                                    if (!b.TradeTCInfoLeg.IsOptional || (b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                    {
                                                        // Already calculated above
                                                    }
                                                    else if (b.TradeTCInfoLeg.IsOptional && b.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                    {
                                                        if (!IsOptionalTradeLegInTheMoney(b, currentDateFrom, currentDateTo, b.Trade.Type, curveDate, useSpot, userName, dataContext3, marketSensitivityType, marketSensitivityValue))
                                                            positionDays = 0;
                                                    }
                                                }
                                                else if (positionMethod.ToUpper() == "DELTA")
                                                {
                                                    // Already calculated above
                                                }
                                            }
                                        }
                                        positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                        if (MTMType.ToUpper() == "CASH FLOW" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Financial"))
                                        {
                                            cash = -positionDays * periodPrice;
                                        }
                                        else if (MTMType.ToUpper() == "P&L" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Physical"))
                                        {
                                            cash = positionDays * (periodMarketPrice - periodPrice);
                                        }

                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                        identifier = b.TradeTCInfoLeg.Id + "|TC";
                                        break;
                                    #endregion

                                    #region FFA
                                    case TradeTypeEnum.FFA:

                                        positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeInfo.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeInfo.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;


                                        decimal factor = 0;
                                        indexName = indexes.Single(a => a.Id == b.TradeFFAInfo.IndexId).Name;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                b.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1;
                                                }
                                                else
                                                {
                                                    factor = positionDays / 30;
                                                }
                                            }
                                            else if (b.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                     b.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1M / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                                else
                                                {
                                                    factor = (positionDays / 30) / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                            }
                                            else if (b.TradeFFAInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual &&
                                                     b.TradeFFAInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                factor =
                                                    Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                                    Convert.ToDecimal(((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days) + 1);
                                            }

                                            //JIRA:FREIG-11
                                            //positionDays = factor * b.TradeFFAInfo.QuantityDays;
                                            //JIRA:FREIG-17
                                            //positionDays = factor * b.TradeFFAInfo.TotalDaysOfTrade;
                                            positionDays = b.BookPositionCalcByDays == BookPositionCalculatedByDaysEnum.Total ? (factor * b.TradeFFAInfo.TotalDaysOfTrade) : (factor * b.TradeFFAInfo.QuantityDays);
                                            positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;

                                            //In Case of FFAs curDate from is used only for calculating position Days for each trade
                                            //Since position days have been calculated then currentFrom is set to 1st day of month
                                            currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1);


                                            if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                            {
                                                if (b.TradeFFAInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeFFAInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeFFAInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                    indexName = indexName + " Last 7 Days";
                                                }
                                                indexValue = periodMarketPrice;
                                            }
                                            else
                                            {

                                                if (b.TradeFFAInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                {
                                                    var indexId = b.TradeFFAInfo.IndexId;
                                                    if (b.TradeFFAInfo.IndexId == -16) indexId = 5;//P1EA
                                                    else if (b.TradeFFAInfo.IndexId == 2) indexId = 18;//P2EA
                                                    else if (b.TradeFFAInfo.IndexId == 3) indexId = 19;//P3EA

                                                    marketPrices = CalculateForwardCurveValues(indexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                    periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo).Select(a => a.Value).Average();
                                                }
                                                else
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeFFAInfo.IndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);


                                                    if (b.TradeFFAInfo.IndexId == -16 || b.TradeFFAInfo.IndexId == 2 || b.TradeFFAInfo.IndexId == 3)
                                                        periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo).Select(a => a.Value).Average();
                                                    else
                                                    {
                                                        periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo).OrderByDescending(a => a.Key).Take(7).Select(a => a.Value).Average();
                                                    }
                                                    indexName = indexName + " Last 7 Days";
                                                }

                                                indexValue = periodMarketPrice;

                                                if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                {
                                                    periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                }
                                                else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                {
                                                    periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                }
                                            }
                                            periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                            periodPrice = b.TradeFFAInfo.Price;
                                            cash = positionDays * (periodMarketPrice - periodPrice);
                                        }
                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                        identifier = b.TradeFFAInfo.Id + "|FFA";
                                        break;
                                    #endregion

                                    #region Cargo
                                    case TradeTypeEnum.Cargo:

                                        positionDays = (decimal)(new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeCargoInfoLeg.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeCargoInfoLeg.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        if (marketSensitivityType.ToUpper() == "STRESS")
                                            indexName = indexes.Single(a => a.Id == b.TradeInfo.MTMStressIndexId).Name;
                                        else
                                            indexName = indexes.Single(a => a.Id == b.TradeInfo.MTMFwdIndexId).Name;


                                        if (positionDays > 0)
                                        {
                                            if (b.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                            {
                                                if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                {
                                                    periodPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeCargoInfoLeg.IndexId.Value && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeCargoInfoLeg.PeriodFrom && a.Date <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                }
                                                else
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeCargoInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                    periodPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeCargoInfoLeg.PeriodFrom && a.Key <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Value).Average();
                                                }
                                                periodPrice = periodPrice * b.TradeCargoInfoLeg.IndexPercentage.Value / 100;
                                            }
                                            else
                                            {
                                                periodPrice = b.TradeCargoInfoLeg.Rate.Value;
                                            }
                                            periodPrice = Math.Round(periodPrice, 4);

                                            if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                            {
                                                periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeInfo.MTMFwdIndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeCargoInfoLeg.PeriodFrom && a.Date <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Rate).Average());
                                                indexValue = periodMarketPrice;
                                            }
                                            else
                                            {
                                                if (marketSensitivityType.ToUpper() == "STRESS")
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                }
                                                else
                                                {
                                                    marketPrices = CalculateForwardCurveValues(b.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                }
                                                periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeCargoInfoLeg.PeriodFrom && a.Key <= b.TradeCargoInfoLeg.PeriodTo).Select(a => a.Value).Average();

                                                indexValue = periodMarketPrice;

                                                if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                {
                                                    periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                }
                                                else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                {
                                                    periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                }
                                            }

                                            periodMarketPrice = periodMarketPrice * b.TradeCargoInfo.VesselIndex / 100;
                                            periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                            if (b.TradeCargoInfoLeg.IsOptional && b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                            {
                                                positionDays = 0;
                                            }
                                            else
                                            {
                                                if (positionMethod.ToUpper() == "STATIC")
                                                {
                                                    // Already calculated above
                                                }
                                                else if (positionMethod.ToUpper() == "DYNAMIC")
                                                {
                                                    if (!b.TradeCargoInfoLeg.IsOptional || (b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                    {
                                                        // Already calculated above
                                                    }
                                                    else if (b.TradeCargoInfoLeg.IsOptional && b.TradeCargoInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                    {
                                                        if (!IsOptionalTradeLegInTheMoney(b, currentDateFrom, currentDateTo, b.Trade.Type, curveDate, useSpot, userName, dataContext3, marketSensitivityType, marketSensitivityValue)) positionDays = 0;
                                                    }
                                                }
                                                else if (positionMethod.ToUpper() == "DELTA")
                                                {
                                                    // Already calculated above
                                                }
                                            }
                                        }
                                        positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? positionDays : -positionDays;
                                        if (MTMType.ToUpper() == "CASH FLOW" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Financial"))
                                        {
                                            cash = -positionDays * periodPrice;
                                        }
                                        else if (MTMType.ToUpper() == "P&L" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Physical"))
                                        {
                                            cash = positionDays * (periodMarketPrice - periodPrice);
                                        }

                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                        identifier = b.TradeCargoInfoLeg.Id + "|CARGO";
                                        break;

                                    #endregion

                                    #region Option
                                    case TradeTypeEnum.Option:
                                        positionDays = (new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, b.TradeInfo.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, b.TradeInfo.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                        factor = 0;

                                        indexName = indexes.Single(a => a.Id == b.TradeOptionInfo.IndexId).Name;

                                        if (positionDays > 0)
                                        {
                                            if (b.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.PerMonth)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1;
                                                }
                                                else
                                                {
                                                    factor = positionDays / 30;
                                                }
                                            }
                                            else if (b.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                                     b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                                {
                                                    factor = 1M / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                                else
                                                {
                                                    factor = (positionDays / 30) / (b.TradeInfo.PeriodTo.Month - b.TradeInfo.PeriodFrom.Month + 1);
                                                }
                                            }
                                            else if (b.TradeOptionInfo.PeriodDayCountType == TradeInfoPeriodDayCountTypeEnum.Actual &&
                                                     b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                factor =
                                                    Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                                    Convert.ToDecimal(((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days) + 1);
                                            }


                                            positionDays = factor * b.TradeOptionInfo.QuantityDays;

                                            if (MTMType.ToUpper() == "CASH FLOW" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Financial"))
                                            {
                                                if (!optionNullify)
                                                {
                                                    if (positionDays != 0)
                                                    {
                                                        if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Year == curveDate.Year && currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            if (b.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                                indexName = indexName + " Last 7 Days";
                                                            }
                                                            indexValue = periodMarketPrice;
                                                        }
                                                        else
                                                        {
                                                            marketPrices = CalculateForwardCurveValues(b.TradeOptionInfo.IndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                            if (b.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeInfo.PeriodFrom && a.Key <= b.TradeInfo.PeriodTo).Select(a => a.Value).Average();
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeInfo.PeriodFrom && a.Key <= b.TradeInfo.PeriodTo).OrderByDescending(a => a.Key).Take(7).Select(a => a.Value).Average();
                                                                indexName = indexName + " Last 7 Days";
                                                            }

                                                            indexValue = periodMarketPrice;

                                                            if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                            }
                                                            else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                            }
                                                        }
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                        cash = positionDays * ((b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call) ? Math.Max(0, periodMarketPrice - b.TradeOptionInfo.Strike) : Math.Max(0, b.TradeOptionInfo.Strike - periodMarketPrice));
                                                        cash = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                    }
                                                }
                                            }
                                            #region P&L
                                            else if (MTMType.ToUpper() == "P&L" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Physical"))
                                            {
                                                if (positionDays != 0)
                                                {
                                                    bool userMTM = false;
                                                    AppParameter appParameter = dataContext3.AppParameters.Where(dfg1 => dfg1.Code == "OPTION_TRADES_USER_MTM").SingleOrDefault();
                                                    if (appParameter != null)
                                                    {
                                                        if (appParameter.Value == "1") userMTM = true;
                                                    }

                                                    if (userMTM)
                                                    {
                                                        cash = b.TradeOptionInfo.UserMtm.HasValue ? b.TradeOptionInfo.UserMtm.Value : 0;
                                                    }
                                                    else
                                                    {

                                                        if ((currentDateTo <= curveDate.Date) || (curveDate.Date == lastBusinessDate.Date) || (currentDateTo.Month == 12 && curveDate.Month == 12 && curveDate.Day >= 24) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                        {
                                                            if (b.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).Select(a => a.Rate).Average());
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice = Convert.ToDecimal(dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).OrderByDescending(a => a.Date).Take(7).Select(a => a.Rate).Average());
                                                                indexName = indexName + " Last 7 Days";
                                                            }
                                                            indexValue = periodMarketPrice;
                                                        }
                                                        else
                                                        {
                                                            marketPrices = CalculateForwardCurveValues(b.TradeOptionInfo.IndexId, curveDate, 0, 0, userName, dataContext3, out finalFFAValues);
                                                            if (b.TradeOptionInfo.SettlementType == TradeInfoSettlementTypeEnum.AllDays)
                                                            {
                                                                periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeInfo.PeriodFrom && a.Key <= b.TradeInfo.PeriodTo).Select(a => a.Value).Average();
                                                            }
                                                            else
                                                            {
                                                                periodMarketPrice = marketPrices.Where(a => a.Key >= currentDateFrom && a.Key <= currentDateTo && a.Key >= b.TradeInfo.PeriodFrom && a.Key <= b.TradeInfo.PeriodTo).OrderByDescending(a => a.Key).Take(7).Select(a => a.Value).Average();
                                                                indexName = indexName + " Last 7 Days";
                                                            }

                                                            indexValue = periodMarketPrice;

                                                            if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                            }
                                                            else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                            {
                                                                periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                            }
                                                        }
                                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);

                                                        decimal T;
                                                        decimal T2;

                                                        T2 = ((currentDateTo - positionDate).Days + 1) / 365M;
                                                        T = ((currentDateTo - currentDateFrom).Days + 1) / 365M;

                                                        if (T < 0)
                                                        //It means that we have monthly average because current date to is earlier than positionDate. No point in calculating based on TurnbullWakeman 
                                                        {
                                                            cash = positionDays * (((b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call) ? Math.Max(0, periodMarketPrice - b.TradeOptionInfo.Strike) : Math.Max(0, b.TradeOptionInfo.Strike - periodMarketPrice)) - b.TradeOptionInfo.Premium);
                                                            cash = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                        }
                                                        else
                                                        {
                                                            decimal mtd;

                                                            try
                                                            {
                                                                mtd = dataContext3.IndexSpotValues.Where(a => a.IndexId == b.TradeOptionInfo.IndexId && a.Date >= currentDateFrom && a.Date <= currentDateTo && a.Date >= b.TradeInfo.PeriodFrom && a.Date <= b.TradeInfo.PeriodTo).Select(a => a.Rate).Average();
                                                            }
                                                            catch (Exception)
                                                            {
                                                                throw new ApplicationException(
                                                                    "Failed to find proper Index Spot Value for MTD calculation");
                                                            }

                                                            Dictionary<DateTime, decimal> marketVolatilities = CalculateForwardVolatilityValues(b.TradeOptionInfo.IndexId, curveDate, userName, dataContext3);
                                                            decimal periodVolatility = marketVolatilities.Where(a => a.Key >= currentDateFrom).OrderBy(a => a.Key).First().Value;
                                                            InterestReferencePeriodTypeEnum? interestReferencePeriodType = null;
                                                            if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 25 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 35)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Month1;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 55 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 65)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months2;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 85 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 95)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months3;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 115 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 125)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months4;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 145 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 155)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months5;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 175 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 185)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months6;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 205 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 215)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months7;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 235 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 245)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months8;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 265 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 275)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months9;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 295 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 305)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months10;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 325 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 335)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Months11;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 6 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 8)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Week1;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 13 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 15)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks2;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 20 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 22)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Weeks3;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 355 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 370)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year1;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 720 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 740)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year2;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 1080 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 1110)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year3;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 1810 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 1840)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year5;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 2530 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 2580)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year7;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 3620 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 3680)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year10;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 7250 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 7350)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year20;
                                                            else if ((b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days >= 10900 && (b.TradeInfo.PeriodTo - b.TradeInfo.PeriodFrom).Days <= 11000)
                                                                interestReferencePeriodType = InterestReferencePeriodTypeEnum.Year30;

                                                            var interestRate =
                                                                dataContext3.InterestReferenceRates.Where(rt1 => rt1.Type == InterestReferenceRateTypeEnum.RiskFree && rt1.PeriodType == interestReferencePeriodType && rt1.Date <= positionDate).OrderByDescending(rt1 => rt1.Date).FirstOrDefault();
                                                            if (interestRate == null)
                                                            {
                                                                throw new ApplicationException(
                                                                    "Failed to find proper Interest Reference Rate for OPTION Trade: " +
                                                                    b.TradeOptionInfo.Id);
                                                            }
                                                            cash =
                                                                Asian_TurnbullWakeman(userName,
                                                                                      b.TradeOptionInfo.Type == TradeOPTIONInfoTypeEnum.Call,
                                                                                      Convert.ToDouble(periodMarketPrice), Convert.ToDouble(mtd),
                                                                                      Convert.ToDouble(b.TradeOptionInfo.Strike),
                                                                                      Convert.ToDouble(T),
                                                                                      Convert.ToDouble(T2),
                                                                                      Convert.ToDouble(interestRate.Rate / 100),
                                                                                      0,
                                                                                      Convert.ToDouble(periodVolatility / 100)) -
                                                                b.TradeOptionInfo.Premium;
                                                            cash = positionDays * cash;
                                                            cash = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -cash : cash;
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }

                                        // Premium calculation
                                        if ((MTMType.ToUpper() == "CASH FLOW" || (MTMType.ToUpper() == "COMBINED" && tradeIdsByPhysicalFinancialType[b.Trade.Id] == "Financial")) && optionPremium && currentDateFrom <= b.TradeInfo.SignDate.Date && currentDateTo >= b.TradeInfo.SignDate.Date)
                                        {
                                            if (b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                            {
                                                positionDays = b.TradeOptionInfo.QuantityDays;
                                            }
                                            else
                                            {
                                                positionDays = 30 * (b.TradeInfo.PeriodTo.Month + 1 - b.TradeInfo.PeriodFrom.Month);
                                            }

                                            positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? -positionDays : positionDays;
                                            cash = cash + positionDays * b.TradeOptionInfo.Premium;
                                        }

                                        monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                        identifier = b.TradeOptionInfo.Id + "|OPTION";
                                        break;
                                        #endregion
                                }

                                //currentDate from is always 1st day of month only on FFAs. On others it might me other than 1st day of month
                                var firstDayOfMonth = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1);
                                if (!currentIndexValues.ContainsKey(firstDayOfMonth))
                                    currentIndexValues.Add(firstDayOfMonth, new Dictionary<string, decimal>());
                                if (!currentIndexValues[firstDayOfMonth].ContainsKey(indexName))
                                    currentIndexValues[firstDayOfMonth].Add(indexName, indexValue);
                                else
                                    if (currentIndexValues[firstDayOfMonth][indexName] < indexValue)
                                    currentIndexValues[firstDayOfMonth][indexName] = indexValue;

                                currentDateFrom = new DateTime(firstDayOfMonth.Year, firstDayOfMonth.Month, 1).AddMonths(1);
                                currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                if (periodTo < currentDateTo) currentDateTo = periodTo;

                            }
                            tradeResults.Add(identifier, monthResults);

                            var returnValue = new MTMReturnValue
                            {
                                tradeResults = tradeResults,
                                indexesValues = currentIndexValues
                            };

                            return returnValue;
                        }).ToList();

                    allResults.Add(tradesResults.Select(a => a.tradeResults).ToList());
                    var listOfdict = tradesResults.Select(a => a.indexesValues).ToList();

                    var dates = listOfdict[0].Keys;
                    indxValues = new Dictionary<DateTime, Dictionary<string, decimal>>();
                    foreach (var date in dates)
                    {
                        var indexDict = new Dictionary<string, decimal>();
                        foreach (var currentIndex in tradesResults.Select(a => a.indexesValues[date]))
                        {
                            var iName = currentIndex.First().Key;
                            var iValue = currentIndex.First().Value;
                            //if (currentIndex.Count == 0) continue;
                            if (!indexDict.ContainsKey(iName))
                                indexDict.Add(iName, iValue);
                            else
                            {
                                if (indexDict[iName] < currentIndex[iName])
                                    indexDict[iName] = currentIndex[iName];
                            }
                        }
                        indxValues.Add(date, indexDict);
                    }
                }
                catch (AggregateException exc)
                {
                    foreach (Exception exception in exc.InnerExceptions)
                    {
                        HandleException(userName, exception);
                    }
                    errorMessage = exc.InnerExceptions[0].Message;
                    return null;
                }
                catch (Exception exc)
                {
                    HandleException(userName, exc);
                    errorMessage = exc.Message;
                    return null;
                }
                finally
                {
                    if (dataContext != null)
                        dataContext.Dispose();

                    foreach (DataContext context in dataContexts)
                    {
                        if (context != null)
                            context.Dispose();
                    }
                }

                // OPTION Embedded Value
                if (tradeInformations != null && MTMType.ToUpper() == "P&L")
                {
                    var res = allResults.Aggregate((list1, list2) => list1.Union(list2).ToList())
                            .ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
                    Dictionary<string, Dictionary<DateTime, decimal>> optionsResults = res.Where(a => a.Key.Contains("OPTION")).ToDictionary(a => a.Key, a => a.Value);

                    foreach (var optionResult in optionsResults)
                    {
                        TradeInformation optionTradeInfo = tradeInformations.SingleOrDefault(fg1 => fg1.Trade.Type == TradeTypeEnum.Option
                            && (fg1.TradeOptionInfo.Id + "|OPTION") == optionResult.Key);

                        if (optionTradeInfo != null)
                        {
                            DateTime perFrom = periodFrom < optionTradeInfo.TradeInfo.PeriodFrom
                                ? optionTradeInfo.TradeInfo.PeriodFrom
                                : periodFrom;
                            DateTime perTo = periodTo < optionTradeInfo.TradeInfo.PeriodTo
                                ? periodTo
                                : optionTradeInfo.TradeInfo.PeriodTo;
                            DateTime currentDateFrom = perFrom;
                            DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                            if (perTo < currentDateTo) currentDateTo = perTo;

                            decimal totalPositionDays = 0;
                            while (currentDateFrom <= perTo)
                            {

                                var positionDays = (decimal)(new TimeSpan(
                                    Math.Max(
                                        Math.Min(currentDateTo.Ticks, optionTradeInfo.TradeInfo.PeriodTo.Ticks) -
                                        Math.Max(currentDateFrom.Ticks, optionTradeInfo.TradeInfo.PeriodFrom.Ticks) +
                                        TimeSpan.TicksPerDay, 0))).Days;

                                decimal factor = 0;



                                if (positionDays > 0)
                                {
                                    if (optionTradeInfo.TradeOptionInfo.PeriodDayCountType ==
                                        TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                        optionTradeInfo.TradeOptionInfo.QuantityType ==
                                        TradeInfoQuantityTypeEnum.PerMonth)
                                    {
                                        if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                        {
                                            factor = 1;
                                        }
                                        else
                                        {
                                            factor = positionDays / 30;
                                        }
                                    }
                                    else if (optionTradeInfo.TradeOptionInfo.PeriodDayCountType ==
                                             TradeInfoPeriodDayCountTypeEnum.Thirty &&
                                             optionTradeInfo.TradeOptionInfo.QuantityType ==
                                             TradeInfoQuantityTypeEnum.Total)
                                    {
                                        if (currentDateFrom.AddMonths(1).AddDays(-1) == currentDateTo)
                                        {
                                            factor = 1M /
                                                     (optionTradeInfo.TradeInfo.PeriodTo.Month -
                                                      optionTradeInfo.TradeInfo.PeriodFrom.Month + 1);
                                        }
                                        else
                                        {
                                            factor = (positionDays / 30) /
                                                     (optionTradeInfo.TradeInfo.PeriodTo.Month -
                                                      optionTradeInfo.TradeInfo.PeriodFrom.Month + 1);
                                        }
                                    }
                                    else if (optionTradeInfo.TradeOptionInfo.PeriodDayCountType ==
                                             TradeInfoPeriodDayCountTypeEnum.Actual &&
                                             optionTradeInfo.TradeOptionInfo.QuantityType ==
                                             TradeInfoQuantityTypeEnum.Total)
                                    {
                                        factor =
                                            Convert.ToDecimal(((currentDateTo - currentDateFrom).Days) + 1) /
                                            Convert.ToDecimal(
                                                ((optionTradeInfo.TradeInfo.PeriodTo -
                                                  optionTradeInfo.TradeInfo.PeriodFrom).Days) + 1);
                                    }


                                    positionDays = factor * optionTradeInfo.TradeOptionInfo.QuantityDays;
                                    totalPositionDays = totalPositionDays + positionDays;
                                }
                                currentDateFrom =
                                    new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                if (periodTo < currentDateTo) currentDateTo = periodTo;
                            }
                            var totalValue = optionResult.Value.Sum(a => a.Value);
                            if (optionTradeInfo.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell)
                            {
                                totalValue = totalValue - (optionTradeInfo.TradeOptionInfo.Premium * totalPositionDays);
                            }
                            else
                            {
                                totalValue = totalValue + (optionTradeInfo.TradeOptionInfo.Premium * totalPositionDays);
                            }
                            totalValue = Math.Abs(totalValue);
                            var dict = new Dictionary<string, decimal>();
                            dict.Add(optionResult.Key, totalValue / totalPositionDays);
                            allEmbeddedValues.Add(new List<Dictionary<string, decimal>> { dict });
                        }
                    }
                }
            }
            results = allResults.Aggregate((list1, list2) => list1.Union(list2).ToList()).ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
            embeddedValues = new Dictionary<string, decimal>();
            if (optionEmbeddedValue) embeddedValues = allEmbeddedValues.Aggregate((list1, list2) => list1.Union(list2).ToList()).ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);

            return 0;
        }

        private decimal Asian_TurnbullWakeman(string userName, bool isCall, double S, double SA, double X, double T, double T2, double r, double b, double v)
        {
            double M1;
            double M2;
            double t1;
            double tau;
            double bA;
            double vA;
            double result;

            t1 = T2 > T ? 0 : T - T2;
            tau = T2 - T;

            M1 = b == 0 ? 1 : (Math.Exp(b * T) - Math.Exp(b * t1)) / (b * (T - t1));

            if (tau > 0)
            {
                if (T2 / T * X - tau / T * SA < 0)
                {
                    if (isCall)
                    {
                        SA = SA * (T2 - T) / T2 + S * M1 * T / T2;
                        result = ((X > SA) ? 0 : SA - X) * Math.Exp(-r * T);
                    }
                    else
                    {
                        result = 0;
                    }
                    return Convert.ToDecimal(result);
                }
            }

            if (b == 0)
            {
                M2 = 2 * Math.Exp(v * v * T) / (Math.Pow(v, 4) * Math.Pow(T - t1, 2)) - 2 * Math.Exp(v * v * t1) * (1 + v * v * (T - t1)) / (Math.Pow(v, 4) * Math.Pow(T - t1, 2));
            }
            else
            {
                M2 = 2 * Math.Exp((2 * b + v * v) * T) / ((b + v * v) * (2 * b + v * v) * Math.Pow(T - t1, 2)) + 2 * Math.Exp((2 * b + v * v) * t1) / (b * Math.Pow(T - t1, 2)) * (1 / (2 * b + v * v) - Math.Exp(b * (T - t1)) / (b + v * v));
            }


            bA = Math.Log(M1) / T;
            vA = Math.Sqrt(Math.Log(M2) / T - 2 * bA);

            if (tau > 0)
            {
                X = T2 / T * X - tau / T * SA;
                result = GBlackScholes(userName, isCall, S, X, T, r, bA, vA) * T / T2;
            }
            else
            {
                result = GBlackScholes(userName, isCall, S, X, T, r, bA, vA);
            }
            SiAuto.Si.GetSession(userName).LogDebug("Asian_TurnbullWakeman: S: " + S + ". SA: " + SA + ".X: " + X + ". T: " + T + ". T2: " + T2 + ".r: " + r + ". b: " + b + ". v: " + v + " M1: " + M1 + ". M2: " + M2 + ". t1: " + t1 + ". tau: " + tau + ". bA: " + bA + ". vA: " + vA + ". Result: " + result);

            return Convert.ToDecimal(result);
        }

        private double GBlackScholes(string userName, bool isCall, double S, double X, double T, double r, double b, double v)
        {
            double d1;
            double d2;
            double result;

            d1 = (Math.Log(S / X) + (b + Math.Pow(v, 2) / 2) * T) / (v * Math.Sqrt(T));
            d2 = d1 - v * Math.Sqrt(T);

            if (isCall)
            {
                result = S * Math.Exp((b - r) * T) * CND(userName, d1) - X * Math.Exp(-r * T) * CND(userName, d2);
            }
            else
            {
                result = X * Math.Exp(-r * T) * CND(userName, -d2) - S * Math.Exp((b - r) * T) * CND(userName, -d1);
            }
            SiAuto.Si.GetSession(userName).LogDebug("GBlackScholes: d1: " + d1 + ". d2: " + d2 + ". Result: " + result);
            return result;
        }

        private double CND(string userName, double X)
        {
            double y, Exponential = 0, As, SumA = 0, SumB = 0, result;
            y = Math.Abs(X);

            if (y > 37)
            {
                result = 0;
            }
            else
            {
                Exponential = Math.Exp(-Math.Pow(y, 2) / 2);
                if (y < Convert.ToDouble(7.07106781186547M))
                {
                    SumA = Convert.ToDouble(3.52624965998911E-02M) * y + Convert.ToDouble(0.700383064443688M);
                    SumA = SumA * y + Convert.ToDouble(6.37396220353165M);
                    SumA = SumA * y + Convert.ToDouble(33.912866078383M);
                    SumA = SumA * y + Convert.ToDouble(112.079291497871M);
                    SumA = SumA * y + Convert.ToDouble(221.213596169931M);
                    SumA = SumA * y + Convert.ToDouble(220.206867912376M);
                    SumB = Convert.ToDouble(8.83883476483184E-02M) * y + Convert.ToDouble(1.75566716318264M);
                    SumB = SumB * y + Convert.ToDouble(16.064177579207M);
                    SumB = SumB * y + Convert.ToDouble(86.7807322029461M);
                    SumB = SumB * y + Convert.ToDouble(296.564248779674M);
                    SumB = SumB * y + Convert.ToDouble(637.333633378831M);
                    SumB = SumB * y + Convert.ToDouble(793.826512519948M);
                    SumB = SumB * y + Convert.ToDouble(440.413735824752M);
                    result = Exponential * SumA / SumB;
                }
                else
                {
                    SumA = y + Convert.ToDouble(0.65M);
                    SumA = y + 4 / SumA;
                    SumA = y + 3 / SumA;
                    SumA = y + 2 / SumA;
                    SumA = y + 1 / SumA;
                    result = Exponential / (SumA * Convert.ToDouble(2.506628274631M));
                }
            }
            if (X > 0) result = 1 - result;
            SiAuto.Si.GetSession(userName).LogDebug("CND: X: " + X + " Exponential: " + Exponential + ". y: " + y + ". SumA: " + SumA + ". SumB: " + SumB + " Result: " + result);
            return result;
        }

        private decimal b76CallValue(string userName, double spot0, double strike, double rate, double vol, double expiry)
        {
            double d1;
            double d2;
            double result;

            if (spot0 > 0)
            {
                d1 = Math.Log(spot0 / strike) + (vol * vol * expiry / 2);
                d1 = d1 / (vol * Math.Sqrt(expiry));
                d2 = d1 - vol * Math.Sqrt(expiry);
                result = Math.Exp(-rate * expiry) * (spot0 * CND(userName, d1) - strike * CND(userName, d2));
            }
            else
            {
                return 0;
            }

            SiAuto.Si.GetSession(userName).LogDebug("b76CallValue: d1: " + d1 + ". d2: " + d2 + ". Result: " + result);
            return Convert.ToDecimal(result);
        }

        private Dictionary<DateTime, decimal> CalculateForwardVolatilityValues(long indexId, DateTime positionDate, string userName, DataContext dataContext)
        {
            if (dataContext == null)
                throw new ApplicationException("CalculateForwardVolatilityValues : dataContext is null");

            Index index = (from objIndex in dataContext.Indexes
                           where objIndex.Id == indexId
                           select objIndex).SingleOrDefault();

            if (index == null)
                throw new ApplicationException("CalculateForwardVolatilityValues : Index is null. IndexId = " + indexId);

            if (index.IsCustom)
            {
                if (index.CustBaseIndexId == null)
                    throw new ApplicationException("CalculateForwardVolatilityValues: Index.CustBaseIndexId is null");
                return CalculateForwardVolatilityValues(index.CustBaseIndexId.Value, positionDate, userName, dataContext);
            }

            List<IndexBOAValue> indexBOAValues = (from objIndexBOAValue in dataContext.IndexBOAValues
                                                  where objIndexBOAValue.Date == new DateTime(positionDate.Year, positionDate.Month, positionDate.Day)
                                                        && objIndexBOAValue.IndexId == indexId
                                                  select objIndexBOAValue).ToList();

            if (indexBOAValues == null || indexBOAValues.Count == 0) throw new ApplicationException("Failed to find BOA Index values for Date : " + positionDate.Date.ToShortDateString());

            DateTime periodFrom = new DateTime(positionDate.Year, positionDate.Month, 1);
            //DateTime periodTo = indexBOAValues.Max(a => a.PeriodTo);
            DateTime periodTo = indexBOAValues.Select(a => a.PeriodTo).Max();

            var finalBOAValuesDict = new Dictionary<DateTime, decimal>();

            var currentDate = new DateTime(periodFrom.Year, periodFrom.Month, 1);
            while (currentDate <= periodTo)
            {
                IndexBOAValue indexBOAValue = indexBOAValues.SingleOrDefault(a => a.PeriodTo.Year == currentDate.Year && a.PeriodTo.Month == currentDate.Month && a.PeriodType == PeriodTypeEnum.Month);
                if (indexBOAValue != null)
                {
                    finalBOAValuesDict.Add(indexBOAValue.PeriodTo, indexBOAValue.Rate);
                    currentDate = currentDate.AddMonths(1);
                    continue;
                }

                indexBOAValue = indexBOAValues.SingleOrDefault(a => a.PeriodTo.Year == currentDate.Year && a.PeriodTo.Month == currentDate.Month && a.PeriodType == PeriodTypeEnum.Quarter);
                if (indexBOAValue != null)
                {
                    List<IndexBOAValue> indexBOAValuesForMonthsInSameQuarter = indexBOAValues.Where(a => a.PeriodType == PeriodTypeEnum.Month && indexBOAValue.PeriodTo > a.PeriodFrom && a.PeriodTo > indexBOAValue.PeriodFrom).OrderBy(a => a.PeriodFrom).ToList();
                    if (indexBOAValuesForMonthsInSameQuarter.Count == 0)
                    {
                        finalBOAValuesDict.Add(indexBOAValue.PeriodTo, indexBOAValue.Rate);
                    }
                    else
                    {
                        var noOfDaysInQuarter = (int)((indexBOAValue.PeriodTo - indexBOAValue.PeriodFrom).Days + 1);
                        var noOfDaysInMissingPeriod = (int)(indexBOAValue.PeriodTo - indexBOAValuesForMonthsInSameQuarter.OrderByDescending(a => a.PeriodTo).First().PeriodTo).Days;
                        decimal rateOfQuarter = indexBOAValue.Rate;
                        decimal averageSumOfMonths = 0;

                        foreach (IndexBOAValue boaValue in indexBOAValuesForMonthsInSameQuarter)
                        {
                            var noOfDaysInMonth = (int)((boaValue.PeriodTo - boaValue.PeriodFrom).Days + 1);
                            decimal rateOfMonth = boaValue.Rate;
                            decimal averageSumOfMonth = Convert.ToDecimal((rateOfMonth * noOfDaysInMonth) / noOfDaysInQuarter);

                            averageSumOfMonths = averageSumOfMonths + averageSumOfMonth;
                        }

                        finalBOAValuesDict.Add(indexBOAValue.PeriodTo, ((rateOfQuarter - averageSumOfMonths) * noOfDaysInQuarter) / noOfDaysInMissingPeriod);
                    }
                    currentDate = currentDate.AddMonths(1);
                    continue;
                }

                indexBOAValue = indexBOAValues.SingleOrDefault(a => a.PeriodTo.Year == currentDate.Year && a.PeriodTo.Month == currentDate.Month && a.PeriodType == PeriodTypeEnum.Calendar);
                if (indexBOAValue != null)
                {
                    List<IndexBOAValue> indexBOAValuesForQuartersInSameYear = indexBOAValues.Where(a => a.PeriodType == PeriodTypeEnum.Quarter && indexBOAValue.PeriodTo > a.PeriodFrom && a.PeriodTo > indexBOAValue.PeriodFrom).OrderBy(a => a.PeriodFrom).ToList();
                    if (indexBOAValuesForQuartersInSameYear.Count == 0)
                    {
                        finalBOAValuesDict.Add(indexBOAValue.PeriodTo, indexBOAValue.Rate);
                    }
                    else
                    {
                        var noOfDaysInYear = (int)((indexBOAValue.PeriodTo - indexBOAValue.PeriodFrom).Days + 1);
                        var noOfDaysInMissingPeriod = (int)(indexBOAValue.PeriodTo - indexBOAValuesForQuartersInSameYear.OrderByDescending(a => a.PeriodTo).First().PeriodTo).Days;
                        decimal rateOfYear = indexBOAValue.Rate;
                        decimal averageSumOfQuarters = 0;

                        foreach (IndexBOAValue boaValue in indexBOAValuesForQuartersInSameYear)
                        {
                            var noOfDaysInQuarter = (int)((boaValue.PeriodTo - boaValue.PeriodFrom).Days + 1);
                            decimal rateOfQuarter = boaValue.Rate;
                            decimal averageSumOfQuarter = Convert.ToDecimal((rateOfQuarter * noOfDaysInQuarter) / noOfDaysInYear);

                            averageSumOfQuarters = averageSumOfQuarters + averageSumOfQuarter;
                        }

                        finalBOAValuesDict.Add(indexBOAValue.PeriodTo, ((rateOfYear - averageSumOfQuarters) * noOfDaysInYear) / noOfDaysInMissingPeriod);
                    }
                    currentDate = currentDate.AddMonths(1);
                    continue;
                }
                currentDate = currentDate.AddMonths(1);
            }
            return finalBOAValuesDict;
        }

        private Dictionary<DateTime, decimal> CalculateForwardCurveValues(long indexId, DateTime positionDate, int absoluteOffset, int percentageOffset, string userName, DataContext dataContext, out Dictionary<DateTime, decimal> finalIndexValues)
        {
            if (dataContext == null)
                throw new ApplicationException("CalculateForwardCurveValues : dataContext is null");

            Index index = (from objIndex in dataContext.Indexes
                           where objIndex.Id == indexId
                           select objIndex).SingleOrDefault();

            if (index == null)
                throw new ApplicationException("CalculateForwardCurveValues: Index is null. IndexId = " + indexId);

            if (index.IsCustom)
            {
                if (index.CustBaseIndexId == null)
                    throw new ApplicationException("index.CustBaseIndexId is null");
                return CalculateForwardCurveValues(index.CustBaseIndexId.Value, positionDate, index.CustAbsOffset.Value, index.CustPercOffset.Value, userName, dataContext, out finalIndexValues);
            }

            List<IndexFFAValue> indexFFAValues = (from objIndexFFAValue in dataContext.IndexFFAValues
                                                  where objIndexFFAValue.Date == new DateTime(positionDate.Year, positionDate.Month, positionDate.Day)
                                                        && objIndexFFAValue.IndexId == indexId
                                                  select objIndexFFAValue).ToList();

            if (indexFFAValues == null || indexFFAValues.Count == 0)
                throw new ApplicationException("Failed to find FFA Index values for Index Code: " + index.Code +
                                               ", Index Id: " + index.Id + " for Date : " +
                                               positionDate.Date.ToShortDateString());

            DateTime firstDay = positionDate;
            // Pairnei ypopsi mono an einai hmerologiaka h teleutaia mera tou mhn kai oxi praktika! old code
            //if (positionDate.Date == GetMonthLastDay(positionDate.Year, positionDate.Month).Date)
            //    firstDay = positionDate.AddDays(1);



            //if last businessDate skip position date current month in calculations, begin calculation value from next month
            DateTime lastBusinessDay = GetLastBusinessDay(positionDate.Year, positionDate.Month);
            if (positionDate.Date == lastBusinessDay.Date || (positionDate.Month == 12 && positionDate.Day >= 24))
                firstDay = new DateTime(positionDate.Year, positionDate.Month, 1).AddMonths(1);

            // DateTime periodFrom = indexFFAValues.Min(a => a.PeriodFrom);
            DateTime periodFrom = new DateTime(firstDay.Year, firstDay.Month, 1);
            //DateTime periodTo = indexFFAValues.Max(a => a.PeriodTo);
            DateTime periodTo = indexFFAValues.Select(a => a.PeriodTo).Max();

            //IndexSpotValue indexSpotValueForDayBeforeStartDate = (from objIndexSpotValue in dataContext.IndexSpotValues
            //                                                      where objIndexSpotValue.IndexId == indexId
            //                                                            && objIndexSpotValue.Date == periodFrom.AddDays(-1)
            //                                                      select objIndexSpotValue).SingleOrDefault();

            var indexIdSpot = indexId;

            //P1A
            if (indexId == 5) indexIdSpot = 16; //P1A
            else if (indexId == 18) indexIdSpot = 2; //P2A
            else if (indexId == 19) indexIdSpot = 3; //P3A

            IndexSpotValue indexSpotValueForDayBeforeStartDate = (from objIndexSpotValue in dataContext.IndexSpotValues
                                                                  where objIndexSpotValue.IndexId == indexIdSpot
                                                                        && objIndexSpotValue.Date <= periodFrom.AddDays(-1)
                                                                  select objIndexSpotValue).OrderByDescending(a => a.Date).FirstOrDefault();

            if (indexSpotValueForDayBeforeStartDate == null) throw new ApplicationException("Failed to find Spot Index Value for Date: " + periodFrom.AddDays(-1).ToString("d"));

            var finalFFAValuesDict = new Dictionary<DateTime, decimal>();

            var currentDate = new DateTime(periodFrom.Year, periodFrom.Month, 1);
            while (currentDate <= periodTo)
            {
                IndexFFAValue indexFFAValue = indexFFAValues.SingleOrDefault(a => a.PeriodTo.Year == currentDate.Year && a.PeriodTo.Month == currentDate.Month && a.PeriodType == PeriodTypeEnum.Month);
                if (indexFFAValue != null)
                {
                    finalFFAValuesDict.Add(indexFFAValue.PeriodTo, indexFFAValue.Rate);
                    currentDate = currentDate.AddMonths(1);
                    continue;
                }

                indexFFAValue = indexFFAValues.SingleOrDefault(a => a.PeriodTo.Year == currentDate.Year && a.PeriodTo.Month == currentDate.Month && a.PeriodType == PeriodTypeEnum.Quarter);
                if (indexFFAValue != null)
                {
                    List<IndexFFAValue> indexFFAValuesForMonthsInSameQuarter = indexFFAValues.Where(a => a.PeriodType == PeriodTypeEnum.Month && indexFFAValue.PeriodTo > a.PeriodFrom && a.PeriodTo > indexFFAValue.PeriodFrom).OrderBy(a => a.PeriodFrom).ToList();
                    if (indexFFAValuesForMonthsInSameQuarter.Count == 0)
                    {
                        finalFFAValuesDict.Add(indexFFAValue.PeriodTo, indexFFAValue.Rate);
                    }
                    else
                    {
                        var noOfDaysInQuarter = (int)((indexFFAValue.PeriodTo - indexFFAValue.PeriodFrom).Days + 1);
                        var noOfDaysInMissingPeriod = (int)(indexFFAValue.PeriodTo - indexFFAValuesForMonthsInSameQuarter.OrderByDescending(a => a.PeriodTo).First().PeriodTo).Days;
                        decimal rateOfQuarter = indexFFAValue.Rate;
                        decimal averageSumOfMonths = 0;

                        foreach (IndexFFAValue ffaValue in indexFFAValuesForMonthsInSameQuarter)
                        {
                            var noOfDaysInMonth = (int)((ffaValue.PeriodTo - ffaValue.PeriodFrom).Days + 1);
                            decimal rateOfMonth = ffaValue.Rate;
                            decimal averageSumOfMonth = Convert.ToDecimal((rateOfMonth * noOfDaysInMonth) / noOfDaysInQuarter);

                            averageSumOfMonths = averageSumOfMonths + averageSumOfMonth;
                        }

                        finalFFAValuesDict.Add(indexFFAValue.PeriodTo, ((rateOfQuarter - averageSumOfMonths) * noOfDaysInQuarter) / noOfDaysInMissingPeriod);
                    }
                    currentDate = currentDate.AddMonths(1);
                    continue;
                }

                indexFFAValue = indexFFAValues.SingleOrDefault(a => a.PeriodTo.Year == currentDate.Year && a.PeriodTo.Month == currentDate.Month && a.PeriodType == PeriodTypeEnum.Calendar);
                if (indexFFAValue != null)
                {
                    List<IndexFFAValue> indexFFAValuesForQuartersInSameYear = indexFFAValues.Where(a => a.PeriodType == PeriodTypeEnum.Quarter && indexFFAValue.PeriodTo > a.PeriodFrom && a.PeriodTo > indexFFAValue.PeriodFrom).OrderBy(a => a.PeriodFrom).ToList();
                    if (indexFFAValuesForQuartersInSameYear.Count == 0)
                    {
                        finalFFAValuesDict.Add(indexFFAValue.PeriodTo, indexFFAValue.Rate);
                    }
                    else
                    {
                        var noOfDaysInYear = (int)((indexFFAValue.PeriodTo - indexFFAValue.PeriodFrom).Days + 1);
                        var noOfDaysInMissingPeriod = (int)(indexFFAValue.PeriodTo - indexFFAValuesForQuartersInSameYear.OrderByDescending(a => a.PeriodTo).First().PeriodTo).Days;
                        decimal rateOfYear = indexFFAValue.Rate;
                        decimal averageSumOfQuarters = 0;

                        foreach (IndexFFAValue ffaValue in indexFFAValuesForQuartersInSameYear)
                        {
                            var noOfDaysInQuarter = (int)((ffaValue.PeriodTo - ffaValue.PeriodFrom).Days + 1);
                            decimal rateOfQuarter = ffaValue.Rate;
                            decimal averageSumOfQuarter = Convert.ToDecimal((rateOfQuarter * noOfDaysInQuarter) / noOfDaysInYear);

                            averageSumOfQuarters = averageSumOfQuarters + averageSumOfQuarter;
                        }

                        finalFFAValuesDict.Add(indexFFAValue.PeriodTo, ((rateOfYear - averageSumOfQuarters) * noOfDaysInYear) / noOfDaysInMissingPeriod);
                    }
                    currentDate = currentDate.AddMonths(1);
                    continue;
                }
                currentDate = currentDate.AddMonths(1);
            }

            // SiAuto.Si.GetSession(userName).LogDebug("CalculateMTMValues: Index Id: " + indexId + ". Date: " + date.ToString("d") + ". Period From: " + periodFrom.Date.ToString("d") + ". Period To: " + periodTo.Date.ToString("d") + ". Final FFA Values: " + Environment.NewLine + finalFFAValuesDict.Select(a => a.Key.ToString("d") + "^" + a.Value).Aggregate((a1, a2) => a1 + Environment.NewLine + a2));

            if (absoluteOffset != 0) finalFFAValuesDict = finalFFAValuesDict.ToDictionary(a => a.Key, b => b.Value + absoluteOffset);
            if (percentageOffset != 0) finalFFAValuesDict = finalFFAValuesDict.ToDictionary(a => a.Key, b => b.Value + b.Value * percentageOffset / 100);

            var zeroRatesValuesDict = new Dictionary<DateTime, decimal>();

            DateTime currentStartDate = periodFrom.AddDays(-1);
            DateTime currentFromDate = currentStartDate;
            decimal currentFromRateSum = 0;

            //zeroRatesValuesDict.Add(indexSpotValueForDayBeforeStartDate.Date, indexSpotValueForDayBeforeStartDate.Rate);
            zeroRatesValuesDict.Add(currentStartDate.Date, indexSpotValueForDayBeforeStartDate.Rate);

            foreach (var keyValuePair in finalFFAValuesDict)
            {
                DateTime currentToDate = keyValuePair.Key;
                decimal rate = keyValuePair.Value;
                decimal currentToRateSum = Convert.ToDecimal((currentToDate - currentFromDate).Days) * rate;

                zeroRatesValuesDict.Add(currentToDate, (currentFromRateSum + currentToRateSum) / Convert.ToDecimal((currentToDate - currentStartDate).Days));

                currentFromDate = currentToDate;
                currentFromRateSum = currentFromRateSum + currentToRateSum;
            }
            //  SiAuto.Si.GetSession(userName).LogDebug("CalculateMTMValues: Index Id: " + indexId + ". Date: " + date.ToString("d") + ". Period From: " + periodFrom.Date.ToString("d") + ". Period To: " + periodTo.Date.ToString("d") + ". Zero Rate Values: " + Environment.NewLine + zeroRatesValuesDict.Select(a => a.Key.ToString("d") + "^" + a.Value).Aggregate((a1, a2) => a1 + Environment.NewLine + a2));

            double[] oldXValues = zeroRatesValuesDict.Keys.Select(a => Convert.ToDouble(a.Ticks)).ToArray();
            double[] oldYValues = zeroRatesValuesDict.Values.Select(a => Convert.ToDouble(a)).ToArray();

            CubicSpline naturalCubicSpline;
            try
            {
                naturalCubicSpline = new CubicSpline(oldXValues, oldYValues);
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                throw exc;
            }

            var cubicSplineValuesDict = new Dictionary<DateTime, decimal>();

            currentDate = new DateTime(periodFrom.Year, periodFrom.Month, 1);
            while (currentDate <= periodTo)
            {
                cubicSplineValuesDict.Add(new DateTime(currentDate.Ticks), Convert.ToDecimal(naturalCubicSpline.ValueAt(currentDate.Ticks)));
                currentDate = currentDate.AddDays(1);
            }

            //  SiAuto.Si.GetSession(userName).LogDebug("CalculateMTMValues: Index Id: " + indexId + ". Date: " + date.ToString("d") + ". Period From: " + periodFrom.Date.ToString("d") + ". Period To: " + periodTo.Date.ToString("d") + ". Cubic Spline Values: " + Environment.NewLine + cubicSplineValuesDict.Select(a => a.Key.ToString("d") + "^" + a.Value).Aggregate((a1, a2) => a1 + Environment.NewLine + a2));

            var forwardCurveValuesDict = new Dictionary<DateTime, decimal>();

            List<decimal> rates = cubicSplineValuesDict.OrderBy(a => a.Key).Select(a => a.Value).ToList();
            List<DateTime> dates = cubicSplineValuesDict.OrderBy(a => a.Key).Select(a => a.Key).ToList();

            for (int i = rates.Count() - 1; i >= 1; i--)
            {
                DateTime currentDay = dates[i];
                decimal currentZeroRate = rates[i];
                decimal previousZeroRate = rates[i - 1];

                var currentPeriodDays = ((currentDay - periodFrom).Days + 1);
                int previousPeriodDays = currentPeriodDays - 1;

                decimal currentRate = (currentZeroRate * currentPeriodDays) - (previousZeroRate * previousPeriodDays);
                forwardCurveValuesDict.Add(currentDay, currentRate);
            }

            forwardCurveValuesDict.Add(dates[0], rates[0]);
            forwardCurveValuesDict = forwardCurveValuesDict.OrderBy(a => a.Key).ToDictionary(a => a.Key, a => a.Value);

            // SiAuto.Si.GetSession(userName).LogDebug("CalculateMTMValues: Index Id: " + indexId + ". Date: " + date.ToString("d") + ". Period From: " + periodFrom.Date.ToString("d") + ". Period To: " + periodTo.Date.ToString("d") + ". Final Average Values: " + Environment.NewLine + finalRateValuesDict.Select(a => a.Key.ToString("d") + "^" + a.Value).Aggregate((a1, a2) => a1 + Environment.NewLine + a2));
            // SiAuto.Si.GetSession(userName).LogDebug("CalculateMTMValues: END");
            finalIndexValues = finalFFAValuesDict;
            return forwardCurveValuesDict;
        }

        private DateTime GetMonthLastDay(int year, int month)
        {
            return new DateTime(year, month, DateTime.DaysInMonth(year, month));
        }

        private DateTime GetLastBusinessDay(int year, int month)
        {
            //old code
            //DateTime lastBusinessDay;
            //var lastOfMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            //if (lastOfMonth.DayOfWeek == DayOfWeek.Sunday)
            //    lastBusinessDay = lastOfMonth.AddDays(-2);
            //else if (lastOfMonth.DayOfWeek == DayOfWeek.Saturday)
            //    lastBusinessDay = lastOfMonth.AddDays(-1);
            //else
            //    lastBusinessDay = lastOfMonth;

            //return lastBusinessDay;


            //LAST WORKING DAY DEFINITION (LWD)

            ////bank holiday array
            var bankHolidays = new List<DateTime>();
            bankHolidays.Add(new DateTime(2015, 8, 31)); //Summer bank Holiday
            bankHolidays.Add(new DateTime(2018, 3, 30)); //Good Friday
            bankHolidays.Add(new DateTime(2020, 8, 31)); //Summer bank Holiday
            bankHolidays.Add(new DateTime(2021, 5, 31)); //Spring bank Holiday
            bankHolidays.Add(new DateTime(2024, 3, 29)); //Good Friday
            bankHolidays.Add(new DateTime(2026, 8, 31)); //Summer bank Holiday
            bankHolidays.Add(new DateTime(2027, 5, 31)); //Spring bank Holiday
            bankHolidays.Add(new DateTime(2029, 3, 30)); //Good Friday

            DateTime lastOfMonth;
            DateTime lastBusinessDay;

            //Input month dec => LWD 23/12
            if (month == 12)
                lastOfMonth = new DateTime(year, month, 24);
            else
                //    lastOfMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                lastOfMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            //Define if lastOfMonth is holiday
            bool IsHoliday = bankHolidays.Contains(lastOfMonth.Date) ? true : false;

            //Case 1: Input month dec => LWD 23/12

            //Case 2: If last day of month is Monday and it is a bank holiday => LWD Friday (4 days back)
            if (lastOfMonth.DayOfWeek == DayOfWeek.Monday && IsHoliday)
                lastBusinessDay = lastOfMonth.AddDays(-3);
            //Case 3: If last day of     month is Friday and it is a bank holiday => LWD Thurday (1 day back)
            else if (lastOfMonth.DayOfWeek == DayOfWeek.Friday && IsHoliday)
                lastBusinessDay = lastOfMonth.AddDays(-1);
            //Case 4: If last day of month is in Weekend
            else if (lastOfMonth.DayOfWeek == DayOfWeek.Sunday)
            {
                //IF Sunday => LWD Friday (2 days back)
                lastBusinessDay = lastOfMonth.AddDays(-2);

                //Friday bank holiday => LWD Thursday (3 days back)
                if (bankHolidays.IndexOf(lastBusinessDay) > 0)
                    lastBusinessDay = lastOfMonth.AddDays(-1);
            }
            //If Saturday => LWD Friday (1 day back)
            else if (lastOfMonth.DayOfWeek == DayOfWeek.Saturday)
            {

                lastBusinessDay = lastOfMonth.AddDays(-1);

                //Friday bank holiday => LWD Thursday (2 days back)
                if (bankHolidays.IndexOf(lastBusinessDay) > 0)
                    lastBusinessDay = lastOfMonth.AddDays(-1);
            }
            else
                lastBusinessDay = lastOfMonth;

            return lastBusinessDay;
        }


        public int? GeneratePhysicalDays(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, string tradeTypeItems, List<string> tradeIdentifiers, out Dictionary<string, Dictionary<DateTime, decimal>> results, out string errorMessage)
        {
            results = null;
            errorMessage = "";

            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            var listOfTradeIdBatches = new List<List<string>>();
            int skip = 0;

            // Batches of 500 trade legs are created and processed in order to overcome the IN SQL clause limit of numbers.
            while (true)
            {
                List<string> tradeIdsBatch = tradeIdentifiers.Skip(skip).Take(500).ToList();
                if (tradeIdsBatch.Count == 0) break;
                listOfTradeIdBatches.Add(tradeIdsBatch);
                skip = skip + tradeIdsBatch.Count;
            }

            var allResults = new List<List<Dictionary<string, Dictionary<DateTime, decimal>>>>();

            foreach (var listOfTradeIdBatch in listOfTradeIdBatches)
            {
                List<TradeInformation> tradeInformations;

                // Trade TC and Cargo Legs should be handled each as a different Trade. Therefore a special notation is used at the client: 
                // A string representing the type of Trade along with "|" and a DB identifier that identifies the specific VERSION of the Leg of a 
                // Physical trade or the specific VERSION of the Financial Trade.
                List<long> tradeTcInfoLegIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.TC).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "TC").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeFfaInfoIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.FFA).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "FFA").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeCargoInfoLegIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.Cargo).ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "CARGO").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();
                List<long> tradeOptionInfoIds = (tradeTypeItems.Contains(((int)TradeTypeWithTypeOptionInfoEnum.Put).ToString()) || tradeTypeItems.Contains(TradeTypeWithTypeOptionInfoEnum.Call.ToString())) ? listOfTradeIdBatch.Where(a => a.Split('|')[1] == "OPTION").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList() : new List<long>();

                var dataContexts = new List<DataContext>();

                try
                {
                    var dataContextRoot = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                    dataContexts.Add(dataContextRoot);

                    if (SiAuto.Si.Level == Level.Debug)
                    {
                        if (SiAuto.Si.GetSession(userName) == null)
                            SiAuto.Si.AddSession(userName, true);
                        dataContextRoot.Log =
                            new SmartInspectLinqToSqlAdapter(
                                SiAuto.Si.GetSession(userName))
                            {
                                TitleLimit = 0,
                                TitlePrefix = "GenerateCalendarDays: "
                            };
                    }

                    // All Trades with corresponding information are selected according to their Ids. Trade TC and Cargo Legs should be handled each as a different Trade. 

                    tradeInformations =
                        (from objTrade in dataContextRoot.Trades
                         from objTradeInfo in dataContextRoot.TradeInfos
                         from objTradeTCInfo in dataContextRoot.TradeTcInfos
                         from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs

                         where
                             objTrade.Id == objTradeInfo.TradeId
                             && objTrade.Type == TradeTypeEnum.TC
                             && objTrade.Status == ActivationStatusEnum.Active
                             && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                             && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                             && tradeTcInfoLegIds.Contains(objTradeTCInfoLeg.Id)

                         select
                             new TradeInformation
                             {
                                 Trade = objTrade,
                                 TradeInfo = objTradeInfo,
                                 TradeTCInfo = objTradeTCInfo,
                                 TradeTCInfoLeg = objTradeTCInfoLeg
                             }//).ToList()
                            //.Union((from objTrade in dataContextRoot.Trades
                            //       from objTradeInfo in dataContextRoot.TradeInfos
                            //       from objTradeFFAInfo in dataContextRoot.TradeFfaInfos

                            //       from objTradeInfoBook in dataContextRoot.TradeInfoBooks
                            //       from objBook in dataContextRoot.Books

                            //       where
                            //          objTrade.Id == objTradeInfo.TradeId
                            //          && objTrade.Type == TradeTypeEnum.FFA
                            //          && objTrade.Status == ActivationStatusEnum.Active
                            //          && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                            //          && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)
                            //          && objTradeInfoBook.TradeInfoId == objTradeInfo.Id
                            //          && objBook.Id == objTradeInfoBook.BookId
                            //          && objBook.ParentBookId == null

                            //       select
                            //           new TradeInformation
                            //           {
                            //               Trade = objTrade,
                            //               TradeInfo = objTradeInfo,
                            //               TradeFFAInfo = objTradeFFAInfo,
                            //               BookPositionCalcByDays = objBook.PositionCalculatedByDays
                            //           }
                            //        ).ToList()).Union((from objTrade in dataContextRoot.Trades
                            //                           from objTradeInfo in dataContextRoot.TradeInfos
                            //                           from objTradeCargoInfo in dataContextRoot.TradeCargoInfos
                            //                           from objTradeCargoInfoLeg in dataContextRoot.TradeCargoInfoLegs

                            //                           where
                            //                               objTrade.Id == objTradeInfo.TradeId
                            //                               && objTrade.Type == TradeTypeEnum.Cargo
                            //                               && objTrade.Status == ActivationStatusEnum.Active
                            //                               && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                            //                               && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                            //                               && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)

                            //                           select
                            //                               new TradeInformation
                            //                               {
                            //                                   Trade = objTrade,
                            //                                   TradeInfo = objTradeInfo,
                            //                                   TradeCargoInfo = objTradeCargoInfo,
                            //                                   TradeCargoInfoLeg = objTradeCargoInfoLeg
                            //                               }).ToList()).Union((
                            //                                                    from objTrade in dataContextRoot.Trades
                            //                                                    from objTradeInfo in dataContextRoot.TradeInfos
                            //                                                    from objTradeOptionInfo in dataContextRoot.TradeOptionInfos

                            //                                                    where
                            //                                                        objTrade.Id == objTradeInfo.TradeId
                            //                                                        && objTrade.Type == TradeTypeEnum.Option
                            //                                                        && objTrade.Status == ActivationStatusEnum.Active
                            //                                                        && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                            //                                                        && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)

                            //                                                    select
                            //                                                        new TradeInformation
                            //                                                        {
                            //                                                            Trade = objTrade,
                            //                                                            TradeInfo = objTradeInfo,
                            //                                                            TradeOptionInfo = objTradeOptionInfo
                            //                                                        }
                            //                                                ).ToList()
                            ).ToList();

                    // The parallel library is used to send the trade legs of each batch for processing to different threads for performance reasons.
                    // Calling thread awaits the end of processing of all spawned threads before it goes further on.
                    List<Dictionary<string, Dictionary<DateTime, decimal>>> tradesResults = tradeInformations.
                        AsParallel().
                        WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                        WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                        Select(b =>
                        {
                            var dataContext2 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                            dataContexts.Add(dataContext2);

                            //code used when necessary
                            if (SiAuto.Si.Level == Level.Debug)
                            {
                                if (SiAuto.Si.GetSession(userName) == null)
                                    SiAuto.Si.AddSession(userName, true);
                                dataContext2.Log =
                                    new SmartInspectLinqToSqlAdapter(
                                        SiAuto.Si.GetSession(userName))
                                    {
                                        TitleLimit = 0,
                                        TitlePrefix = "GeneratePhysicalDays: "
                                    };
                            }

                            var tradeResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                            string identifier = null;
                            var monthResults = new Dictionary<DateTime, decimal>();


                            DateTime currentDateFrom = periodFrom;
                            DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                            if (periodTo < currentDateTo) currentDateTo = periodTo;
                            // All results are generated per calendar Month, therefore processing is based on a relevant model. Start of period as well as the End of period do not have to be whole months.
                            while (currentDateFrom <= periodTo)
                            {
                                decimal positionDays;

                                if (b.Trade.Type == TradeTypeEnum.TC)
                                {

                                    positionDays = (decimal)(new TimeSpan(
                                                                 Math.Max(
                                                                     Math.Min(currentDateTo.Ticks, b.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                     Math.Max(currentDateFrom.Ticks, b.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                     TimeSpan.TicksPerDay, 0))).Days;


                                    monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), positionDays);
                                    identifier = b.TradeTCInfoLeg.Id + "|TC";


                                    currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                    currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                    if (periodTo < currentDateTo) currentDateTo = periodTo;
                                }
                            }
                            tradeResults.Add(identifier, monthResults);
                            return tradeResults;
                        }).ToList();

                    allResults.Add(tradesResults);
                }
                catch (AggregateException exc)
                {
                    foreach (Exception exception in exc.InnerExceptions)
                    {
                        HandleException(userName, exception);
                    }
                    errorMessage = exc.InnerExceptions[0].Message;
                    return null;
                }
                catch (Exception exc)
                {
                    HandleException(userName, exc);
                    errorMessage = exc.Message;
                    return null;
                }
                finally
                {
                    foreach (DataContext context in dataContexts)
                    {
                        if (context != null)
                            context.Dispose();
                    }
                }
            }
            results = allResults.Aggregate((list1, list2) => list1.Union(list2).ToList()).ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
            return 0;
        }
    }
}