﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exis.Domain;
using Exis.WCFExtensions;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;

namespace Exis.ClientsEngine
{
    public partial class ClientsService
    {
        public int? GetCFModels(out List<CashFlowModel> models)
        {
            models = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetCFModels: " };
                }

                long userId =
                    dataContext.Users.Single(a => a.Login == GenericContext<ProgramInfo>.Current.Value.UserName).Id;

                models = Queries.GetCashFlowModelsByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GenerateCashFlow(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, List<string> tradeIdentifiers, long cashFlowModelId,
                                     bool calculateBreakEven, bool calculateValuation, bool calculateIRR, bool calculateBEBalloon, bool calculateBEEquity, bool calculateBEScrapValue, decimal valuationDiscountRate,
                                     out Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowGroupItem> cashFlowGroupItems,
                                    out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsAll, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsSpot, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsBalloon, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsEquity, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsScrappings,
                                    out Dictionary<string, Dictionary<DateTime, decimal>> valuationResults, out Dictionary<string, Dictionary<DateTime, decimal>> valuationDiscountFactor, out Dictionary<string, Dictionary<DateTime, decimal>> valuationPresentValue, out Dictionary<string, Dictionary<DateTime, decimal>> irrResults)
        {
            // TODO Requested Currency is probably an other parameter. Exchange rates should be performed where applicable.
            cashFlowItemResults = new Dictionary<long, Dictionary<DateTime, decimal>>();
            cashFlowGroups = new List<CashFlowGroup>();
            cashFlowModelGroups = new List<CashFlowModelGroup>();
            cashFlowItems = new List<CashFlowItem>();
            cashFlowGroupItems = new List<CashFlowGroupItem>();
            breakEvenResultsAll = new Dictionary<string, Dictionary<DateTime, decimal>>();
            breakEvenResultsSpot = new Dictionary<string, Dictionary<DateTime, decimal>>();
            breakEvenResultsBalloon = new Dictionary<string, Dictionary<DateTime, decimal>>();
            breakEvenResultsEquity = new Dictionary<string, Dictionary<DateTime, decimal>>();
            breakEvenResultsScrappings = new Dictionary<string, Dictionary<DateTime, decimal>>();
            valuationResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
            valuationDiscountFactor = new Dictionary<string, Dictionary<DateTime, decimal>>();
            valuationPresentValue = new Dictionary<string, Dictionary<DateTime, decimal>>();
            irrResults = new Dictionary<string, Dictionary<DateTime, decimal>>();

            Dictionary<DateTime, Dictionary<string, decimal>> indexesValues;

            // TODO Check if 2 or more TC/CARGO INs or TC/CARGO OUTs are defined in the system for the same Vessel for the same overlapping period, however small this is. This is an error.
            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            List<ClientsService.TradeInformation> tradeInformations;

            List<long> tradeTcInfoLegIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "TC").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();
            List<long> tradeFfaInfoIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "FFA").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();
            List<long> tradeCargoInfoLegIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "CARGO").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();
            List<long> tradeOptionInfoIds = tradeIdentifiers.Where(a => a.Split('|')[1] == "OPTION").Select(a => Convert.ToInt64(a.Split('|')[0])).ToList();

            var dataContexts = new List<DataContext>();

            try
            {
                var dataContextRoot = new DataContext(DBConnectionString) {ObjectTrackingEnabled = false};
                dataContexts.Add(dataContextRoot);

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null)
                        SiAuto.Si.AddSession(userName, true);
                    dataContextRoot.Log =
                        new SmartInspectLinqToSqlAdapter(
                            SiAuto.Si.GetSession(userName))
                            {
                                TitleLimit = 0,
                                TitlePrefix = "GenerateCashFlow: "
                            };
                }

                cashFlowItems = (from objCashFlowModelGroup in dataContextRoot.CashFlowModelsGroups
                                 from objCashFlowGroupItem in dataContextRoot.CashFlowGroupsItems
                                 from objCashFlowItem in dataContextRoot.CashFlowItems
                                 where objCashFlowModelGroup.ModelId == cashFlowModelId
                                       && objCashFlowGroupItem.GroupId == objCashFlowModelGroup.GroupId
                                       && objCashFlowGroupItem.ItemId == objCashFlowItem.Id
                                 select objCashFlowItem).Distinct().ToList();

                var vesselInfoList = (from objCompany in dataContextRoot.Companies
                                      from objVessel in dataContextRoot.Vessels
                                      from objVesselInfo in dataContextRoot.VesselInfos
                                      where objCompany.Type == CompanyTypeEnum.Internal
                                            && objVessel.CompanyId == objCompany.Id
                                            && objVesselInfo.VesselId == objVessel.Id
                                            && ((from objTrade in dataContextRoot.Trades
                                                 from objTradeInfo in dataContextRoot.TradeInfos
                                                 from objTradeTcInfo in dataContextRoot.TradeTcInfos
                                                 from objTradeTcInfoLeg in dataContextRoot.TradeTcInfoLegs
                                                 where
                                                     objTrade.Id == objTradeInfo.TradeId
                                                     && objTrade.Type == TradeTypeEnum.TC
                                                     && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                     && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                     && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                 select objTradeTcInfo.VesselId).Contains(objVessel.Id)
                                                ||
                                                (from objTrade in dataContextRoot.Trades
                                                 from objTradeInfo in dataContextRoot.TradeInfos
                                                 from objTradeCargoInfo in dataContextRoot.TradeCargoInfos
                                                 from objTradeCargoInfoLeg in dataContextRoot.TradeCargoInfoLegs
                                                 where
                                                     objTrade.Id == objTradeInfo.TradeId
                                                     && objTrade.Type == TradeTypeEnum.Cargo
                                                     && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                     && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                     && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                                 select objTradeCargoInfo.VesselId).Contains(objVessel.Id)
                                                ||
                                                (from objTrade in dataContextRoot.Trades
                                                 from objTradeInfo in dataContextRoot.TradeInfos
                                                 from objTradeFfaInfo in dataContextRoot.TradeFfaInfos
                                                 where
                                                     objTrade.Id == objTradeInfo.TradeId
                                                     && objTrade.Type == TradeTypeEnum.FFA
                                                     && objTradeFfaInfo.TradeInfoId == objTradeInfo.Id
                                                     && tradeFfaInfoIds.Contains(objTradeFfaInfo.Id)
                                                 select objTradeFfaInfo.VesselId).Contains(objVessel.Id)
                                                ||
                                                (from objTrade in dataContextRoot.Trades
                                                 from objTradeInfo in dataContextRoot.TradeInfos
                                                 from objTradeOptionInfo in dataContextRoot.TradeOptionInfos
                                                 where
                                                     objTrade.Id == objTradeInfo.TradeId
                                                     && objTrade.Type == TradeTypeEnum.Option
                                                     && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                     && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                 select objTradeOptionInfo.VesselId).Contains(objVessel.Id)
                                               )
                                      select new {Vessel = objVessel, VesselInfo = objVesselInfo}).Distinct().ToList();

                List<Dictionary<long, Dictionary<DateTime, decimal>>> cashFlowItemsResults = cashFlowItems.
                    AsParallel().
                    WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                    WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                    Select(a =>
                               {
                                   var dataContext2 = new DataContext(DBConnectionString) {ObjectTrackingEnabled = false};
                                   dataContexts.Add(dataContext2);
                                   var cashFlowItemResult = new Dictionary<long, Dictionary<DateTime, decimal>>();
                                   if (!a.IsCustom)
                                   {
                                       List<string> tradeIds;
                                       List<ClientsService.TradeInformation> tradeInfos;
                                       switch (a.SystemType)
                                       {
                                           case CashFlowItemSystemTypeEnum.SpotRevenue:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalSpot = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal spot = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               List<ClientsService.TradeInformation> tradeInfoos = (from objTrade in dataContextRoot.Trades
                                                                                                            from objTradeInfo in dataContextRoot.TradeInfos
                                                                                                            from objTradeTCInfo in dataContextRoot.TradeTcInfos
                                                                                                            from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs
                                                                                                            where
                                                                                                                objTrade.Id == objTradeInfo.TradeId
                                                                                                                && objTrade.Status == ActivationStatusEnum.Active
                                                                                                                && objTrade.Type == TradeTypeEnum.TC
                                                                                                                && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                                                                                && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                                                                                                                && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                                                                                                                && objTradeTCInfo.VesselId == vesselInfo.Vessel.Id
                                                                                                                && objTradeTCInfoLeg.PeriodTo >= currentDateFrom
                                                                                                                && objTradeTCInfoLeg.PeriodFrom <= currentDateTo
                                                                                                            select
                                                                                                                new ClientsService.TradeInformation
                                                                                                                    {
                                                                                                                        Trade = objTrade,
                                                                                                                        TradeInfo = objTradeInfo,
                                                                                                                        TradeTCInfo = objTradeTCInfo,
                                                                                                                        TradeTCInfoLeg = objTradeTCInfoLeg
                                                                                                                    }).ToList();

                                                               var tradeInfo = tradeInfoos.Where(rf => rf.TradeInfo.Id == tradeInfoos.Max(er => er.TradeInfo.Id)).SingleOrDefault();

                                                               decimal periodPrice;
                                                               decimal periodMarketPrice;
                                                               Dictionary<DateTime, decimal> finalFFAValues;
                                                               Dictionary<DateTime, decimal> marketPrices;

                                                               if (tradeInfo != null)
                                                               {
                                                                   var positionDays = (decimal)(new TimeSpan(
                                                                                                    Math.Max(
                                                                                                        Math.Min(currentDateTo.Ticks, tradeInfo.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                                                        Math.Max(currentDateFrom.Ticks, tradeInfo.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                                                        TimeSpan.TicksPerDay, 0))).Days;

                                                                   if (tradeInfo.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                                                   {
                                                                       if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                                       {
                                                                           periodPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeTCInfoLeg.IndexId.Value && a100.Date >= currentDateFrom && a100.Date <= currentDateTo && a100.Date >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Rate).Average());
                                                                       }
                                                                       else
                                                                       {
                                                                           marketPrices = CalculateForwardCurveValues(tradeInfo.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                           periodPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();
                                                                       }
                                                                       periodPrice = periodPrice * tradeInfo.TradeTCInfoLeg.IndexPercentage.Value/100;
                                                                   }
                                                                   else
                                                                   {
                                                                       periodPrice = tradeInfo.TradeTCInfoLeg.Rate.Value;
                                                                   }
                                                                   periodPrice = Math.Round(periodPrice, 4);
                                                                   if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                                                   {
                                                                       periodMarketPrice = Convert.ToDecimal(dataContext2.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeInfo.MTMFwdIndexId && a100.Date >= currentDateFrom && a100.Date <= currentDateTo && a100.Date >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Rate).Average());
                                                                   }
                                                                   else
                                                                   {
                                                                       if (marketSensitivityType.ToUpper() == "STRESS")
                                                                       {
                                                                           marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                       }
                                                                       else
                                                                       {
                                                                           marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                       }
                                                                       periodMarketPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();

                                                                       if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                                                       {
                                                                           periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                                                       }
                                                                       else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                                                       {
                                                                           periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                                                                       }
                                                                   }
                                                                   
                                                                   periodMarketPrice = periodMarketPrice * tradeInfo.TradeTCInfo.VesselIndex / 100;
                                                                   periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                                   if (tradeInfo.TradeTCInfoLeg.IsOptional && tradeInfo.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                                                   {
                                                                       positionDays = 0;
                                                                   }
                                                                   else
                                                                   {
                                                                       if (positionMethod.ToUpper() == "STATIC")
                                                                       {
                                                                           // Already calculated above
                                                                       }
                                                                       else if (positionMethod.ToUpper() == "DYNAMIC")
                                                                       {
                                                                           if (!tradeInfo.TradeTCInfoLeg.IsOptional || (tradeInfo.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                                                           {
                                                                               // Already calculated above
                                                                           }
                                                                           else if (tradeInfo.TradeTCInfoLeg.IsOptional && tradeInfo.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                                                           {
                                                                               if (!IsOptionalTradeLegInTheMoney(tradeInfo,currentDateFrom, currentDateTo, tradeInfo.Trade.Type, curveDate, useSpot, userName, dataContext2, marketSensitivityType, marketSensitivityValue)) positionDays = 0;
                                                                           }
                                                                       }
                                                                       else if (positionMethod.ToUpper() == "DELTA")
                                                                       {
                                                                           // Already calculated above
                                                                       }
                                                                   }

                                                                   if (positionDays == 0)
                                                                   {
                                                                       // no trade leg for this month or out of the money. Spot is calculated.
                                                                       marketPrices = CalculateForwardCurveValues(dataContext2.Indexes.Single(h1 => h1.MarketId == vesselInfo.Vessel.MarketId && h1.IsMarketDefault).Id, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                       periodMarketPrice = marketPrices.Where(a100 => a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodTo && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();
                                                                       periodMarketPrice = periodMarketPrice * (dataContext2.VesselBenchmarkings.Where(g1 => tradeInfo.TradeTCInfoLeg.PeriodTo >= g1.PeriodFrom && tradeInfo.TradeTCInfoLeg.PeriodTo <= g1.PeriodTo && g1.VesselId == vesselInfo.Vessel.Id).ToList().First().Benchmark / 100);
                                                                       periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                                       spot = ((currentDateTo - tradeInfo.TradeTCInfoLeg.PeriodTo).Days + 1) * periodMarketPrice;
                                                                   }
                                                                   else
                                                                   {
                                                                       // In the money TC OUT Trade. Spot is not calculated.
                                                                       spot = 0;
                                                                   }
                                                               }
                                                               else
                                                               {
                                                                   // There is no TC OUT Trade for this Vessel. Spot is calculated.
                                                                   marketPrices = CalculateForwardCurveValues(dataContext2.Indexes.Single(h2 => h2.MarketId == vesselInfo.Vessel.MarketId && h2.IsMarketDefault).Id, curveDate, 0, 0, userName, dataContext2, out finalFFAValues);
                                                                   periodMarketPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();
                                                                   
                                                                   periodMarketPrice = periodMarketPrice * (dataContext2.VesselBenchmarkings.Where(g1 => currentDateFrom >= g1.PeriodFrom && currentDateFrom <= g1.PeriodTo && g1.VesselId == vesselInfo.Vessel.Id).ToList().First().Benchmark / 100);
                                                                   periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                                                   spot = ((currentDateTo - currentDateFrom).Days + 1) * periodMarketPrice;
                                                               }
                                                           }
                                                           totalSpot = totalSpot + Convert.ToDecimal(spot);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalSpot);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }

                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueMinimum:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeCargoInfo in dataContext2.TradeCargoInfos
                                                           from objTradeCargoInfoLeg in dataContext2.TradeCargoInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.Cargo
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                               && objTradeCargoInfoLeg.IsOptional == false
                                                               && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                                           select objTradeCargoInfoLeg.Id + "|CARGO").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueOptional:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeCargoInfo in dataContext2.TradeCargoInfos
                                                           from objTradeCargoInfoLeg in dataContext2.TradeCargoInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.Cargo
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                               && objTradeCargoInfoLeg.IsOptional
                                                               && objTradeCargoInfoLeg.OptionalStatus != TradeInfoLegOptionalStatusEnum.NonDeclared
                                                               && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                                           select objTradeCargoInfoLeg.Id + "|CARGO").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueFixedHireMinimum:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional == false
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.Fixed
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueFixedHireOptional:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional
                                                               && objTradeTcInfoLeg.OptionalStatus != TradeInfoLegOptionalStatusEnum.NonDeclared
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.Fixed
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueIndexLinkedHireMinimum:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional == false
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueIndexLinkedHireOptional:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional
                                                               && objTradeTcInfoLeg.OptionalStatus != TradeInfoLegOptionalStatusEnum.NonDeclared
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageRevenueProfitSharing:
                                               List<long> tradeTCIds = (from objTrade in dataContext2.Trades
                                                                        from objTradeInfo in dataContext2.TradeInfos
                                                                        from objTradeOptionInfo in dataContext2.TradeOptionInfos
                                                                        where
                                                                            objTrade.Id == objTradeInfo.TradeId
                                                                            && objTrade.Type == TradeTypeEnum.Option
                                                                            && objTrade.Status == ActivationStatusEnum.Active
                                                                            && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                            && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                                            && objTradeOptionInfo.ProfitSharing != null
                                                                        select objTradeOptionInfo.ProfitSharing.Value).ToList();

                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTradeInfo.DateTo == null
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && tradeTCIds.Contains(objTrade.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsMinimum:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeCargoInfo in dataContext2.TradeCargoInfos
                                                           from objTradeCargoInfoLeg in dataContext2.TradeCargoInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.Cargo
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                               && objTradeCargoInfoLeg.IsOptional == false
                                                               && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                                           select objTradeCargoInfoLeg.Id + "|CARGO").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsOptional:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeCargoInfo in dataContext2.TradeCargoInfos
                                                           from objTradeCargoInfoLeg in dataContext2.TradeCargoInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.Cargo
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                               && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                               && objTradeCargoInfoLeg.IsOptional
                                                               && objTradeCargoInfoLeg.OptionalStatus != TradeInfoLegOptionalStatusEnum.NonDeclared
                                                               && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                                           select objTradeCargoInfoLeg.Id + "|CARGO").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsFixedHireMinimum:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional == false
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.Fixed
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsFixedHireOptional:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional
                                                               && objTradeTcInfoLeg.OptionalStatus != TradeInfoLegOptionalStatusEnum.NonDeclared
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.Fixed
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireMinimum:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional == false
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireOptional:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && objTradeTcInfoLeg.IsOptional
                                                               && objTradeTcInfoLeg.OptionalStatus != TradeInfoLegOptionalStatusEnum.NonDeclared
                                                               && objTradeTcInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked
                                                               && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VoyageCostsProfitSharing:
                                               List<long> tradeTcIds = (from objTrade in dataContext2.Trades
                                                                        from objTradeInfo in dataContext2.TradeInfos
                                                                        from objTradeOptionInfo in dataContext2.TradeOptionInfos
                                                                        where
                                                                            objTrade.Id == objTradeInfo.TradeId
                                                                            && objTrade.Type == TradeTypeEnum.Option
                                                                            && objTrade.Status == ActivationStatusEnum.Active
                                                                            && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                            && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                                            && objTradeOptionInfo.ProfitSharing != null
                                                                        select objTradeOptionInfo.ProfitSharing.Value).ToList();

                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeTcInfo in dataContext2.TradeTcInfos
                                                           from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.TC
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeInfo.DateTo == null
                                                               && objTradeTcInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy
                                                               && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                               && tradeTcIds.Contains(objTrade.Id)
                                                           select objTradeTcInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.FFAsSettlement:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeFFAInfo in dataContext2.TradeFfaInfos
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.FFA
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                                               && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)
                                                           select objTradeFFAInfo.Id + "|FFA").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.FreightOptionsSettlement:
                                               tradeIds = (from objTrade in dataContext2.Trades
                                                           from objTradeInfo in dataContext2.TradeInfos
                                                           from objTradeOptionInfo in dataContext2.TradeOptionInfos
                                                           where
                                                               objTrade.Id == objTradeInfo.TradeId
                                                               && objTrade.Type == TradeTypeEnum.Option
                                                               && objTrade.Status == ActivationStatusEnum.Active
                                                               && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                               && objTradeOptionInfo.ProfitSharing == null
                                                               && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                           select objTradeOptionInfo.Id + "|OPTION").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.FreightOptionsPremium:

                                               tradeInfos = (from objTrade in dataContext2.Trades
                                                             from objTradeInfo in dataContext2.TradeInfos
                                                             from objTradeOptionInfo in dataContext2.TradeOptionInfos
                                                             where
                                                                 objTrade.Id == objTradeInfo.TradeId
                                                                 && objTrade.Type == TradeTypeEnum.Option
                                                                 && objTrade.Status == ActivationStatusEnum.Active
                                                                 && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                 && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                             select
                                                                 new ClientsService.TradeInformation
                                                                     {
                                                                         Trade = objTrade,
                                                                         TradeInfo = objTradeInfo,
                                                                         TradeOptionInfo = objTradeOptionInfo
                                                                     }).ToList();

                                               if (tradeInfos.Count > 0)
                                               {
                                                   var allResults = new List<List<Dictionary<string, Dictionary<DateTime, decimal>>>>();
                                                   List<Dictionary<string, Dictionary<DateTime, decimal>>> tradesResults = tradeInfos.
                                                       AsParallel().
                                                       WithExecutionMode(ParallelExecutionMode.ForceParallelism).
                        WithDegreeOfParallelism(4).
                                                       WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                                                       Select(b =>
                                                                  {
                                                                      DataContext dataContext3 = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };
                                                                      dataContexts.Add(dataContext3);

                                                                      if (SiAuto.Si.Level == Level.Debug)
                                                                      {
                                                                          if (SiAuto.Si.GetSession(userName) == null)
                                                                              SiAuto.Si.AddSession(userName, true);
                                                                          dataContext3.Log =
                                                                              new SmartInspectLinqToSqlAdapter(
                                                                                  SiAuto.Si.GetSession(userName))
                                                                              {
                                                                                  TitleLimit = 0,
                                                                                  TitlePrefix = "GenerateCashFlow: "
                                                                              };
                                                                      }

                                                                      var tradeResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                                                      string identifier = null;
                                                                      var monthResults = new Dictionary<DateTime, decimal>();

                                                                      DateTime currentDateFrom = periodFrom;
                                                                      DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                                      while (currentDateFrom <= periodTo)
                                                                      {
                                                                          decimal positionDays;
                                                                          decimal cash = 0;

                                                                          if (currentDateFrom <= b.TradeInfo.SignDate.Date && currentDateTo >= b.TradeInfo.SignDate.Date)
                                                                          {
                                                                              if (b.TradeOptionInfo.QuantityType == TradeInfoQuantityTypeEnum.Total)
                                                                              {
                                                                                  positionDays = b.TradeOptionInfo.QuantityDays;
                                                                              }
                                                                              else
                                                                              {
                                                                                  positionDays = 30 * (b.TradeInfo.PeriodTo.Month + 1 - b.TradeInfo.PeriodFrom.Month);
                                                                              }

                                                                              positionDays = b.TradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell ? positionDays : -positionDays;
                                                                              cash = cash + positionDays * b.TradeOptionInfo.Premium;
                                                                          }
                                                                          monthResults.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), cash);
                                                                          identifier = b.TradeOptionInfo.Id + "|OPTION";
                                                                          currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                                          currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                                          if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                                      }
                                                                      tradeResults.Add(identifier, monthResults);
                                                                      return tradeResults;
                                                                  }).ToList();
                                                   allResults.Add(tradesResults);
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results = allResults.Aggregate((list1, list2) => list1.Union(list2).ToList()).ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
                                                   List<DateTime> months = results.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => results.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.TCCommissions:

                                               tradeInfos = (from objTrade in dataContext2.Trades
                                                             from objTradeInfo in dataContext2.TradeInfos
                                                             from objTradeTCInfo in dataContext2.TradeTcInfos
                                                             from objTradeTCInfoLeg in dataContext2.TradeTcInfoLegs
                                                             where
                                                                 objTrade.Id == objTradeInfo.TradeId
                                                                 && objTrade.Type == TradeTypeEnum.TC
                                                                 && objTrade.Status == ActivationStatusEnum.Active
                                                                 && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                                                                 && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                                                                 && tradeTcInfoLegIds.Contains(objTradeTCInfoLeg.Id)
                                                             select
                                                                 new ClientsService.TradeInformation
                                                                     {
                                                                         Trade = objTrade,
                                                                         TradeInfo = objTradeInfo,
                                                                         TradeTCInfo = objTradeTCInfo,
                                                                         TradeTCInfoLeg = objTradeTCInfoLeg
                                                                     }).ToList();

                                               tradeIds = tradeInfos.Select(a5 => a5.TradeTCInfoLeg.Id + "|TC").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> lastResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();

                                                   foreach (KeyValuePair<string, Dictionary<DateTime, decimal>> keyValuePair in results)
                                                   {
                                                       ClientsService.TradeInformation tradeInfo = tradeInfos.Where(a6 => a6.TradeTCInfoLeg.Id + "|TC" == keyValuePair.Key).Single();

                                                       var totalCommissionPercentage = dataContext2.TradeBrokerInfos.Where(a7 => a7.TradeInfoId == tradeInfo.TradeInfo.Id).Select(a7 => a7.Commission).Sum() / 100;

                                                       var result = keyValuePair.Value;
                                                       var totalCommission = result.Select(a6 => a6.Value * totalCommissionPercentage).Sum();
                                                       Dictionary<DateTime, decimal> lastMonthValues = new Dictionary<DateTime, decimal>();
                                                       foreach (KeyValuePair<DateTime, decimal> valuePair in result)
                                                       {
                                                           if (valuePair.Key.Year == tradeInfo.TradeInfo.SignDate.Year && valuePair.Key.Month == tradeInfo.TradeInfo.SignDate.Month)
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, totalCommission);
                                                           }
                                                           else
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, 0M);
                                                           }
                                                       }
                                                       lastResults.Add(keyValuePair.Key, lastMonthValues);
                                                   }

                                                   List<DateTime> months = lastResults.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => lastResults.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }

                                               break;

                                           case CashFlowItemSystemTypeEnum.CargoCommissions:
                                               tradeInfos = (from objTrade in dataContext2.Trades
                                                             from objTradeInfo in dataContext2.TradeInfos
                                                             from objTradeCargoInfo in dataContext2.TradeCargoInfos
                                                             from objTradeCargoInfoLeg in dataContext2.TradeCargoInfoLegs
                                                             where
                                                                 objTrade.Id == objTradeInfo.TradeId
                                                                 && objTrade.Type == TradeTypeEnum.Cargo
                                                                 && objTrade.Status == ActivationStatusEnum.Active
                                                                 && objTradeCargoInfo.TradeInfoId == objTradeInfo.Id
                                                                 && objTradeCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id
                                                                 && tradeCargoInfoLegIds.Contains(objTradeCargoInfoLeg.Id)
                                                             select
                                                                 new ClientsService.TradeInformation
                                                                     {
                                                                         Trade = objTrade,
                                                                         TradeInfo = objTradeInfo,
                                                                         TradeCargoInfo = objTradeCargoInfo,
                                                                         TradeCargoInfoLeg = objTradeCargoInfoLeg
                                                                     }).ToList();

                                               tradeIds = tradeInfos.Select(a5 => a5.TradeCargoInfoLeg.Id + "|CARGO").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> lastResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();

                                                   foreach (KeyValuePair<string, Dictionary<DateTime, decimal>> keyValuePair in results)
                                                   {
                                                       ClientsService.TradeInformation tradeInfo = tradeInfos.Where(a6 => a6.TradeCargoInfoLeg.Id + "|CARGO" == keyValuePair.Key).Single();

                                                       var totalCommissionPercentage = dataContext2.TradeBrokerInfos.Where(a7 => a7.TradeInfoId == tradeInfo.TradeInfo.Id).Select(a7 => a7.Commission).Sum() / 100;

                                                       var result = keyValuePair.Value;
                                                       var totalCommission = result.Select(a6 => a6.Value * totalCommissionPercentage).Sum();
                                                       Dictionary<DateTime, decimal> lastMonthValues = new Dictionary<DateTime, decimal>();
                                                       foreach (KeyValuePair<DateTime, decimal> valuePair in result)
                                                       {
                                                           if (valuePair.Key.Year == tradeInfo.TradeInfo.SignDate.Year && valuePair.Key.Month == tradeInfo.TradeInfo.SignDate.Month)
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, totalCommission);
                                                           }
                                                           else
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, 0M);
                                                           }
                                                       }
                                                       lastResults.Add(keyValuePair.Key, lastMonthValues);
                                                   }

                                                   List<DateTime> months = lastResults.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => lastResults.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.FFACommissions:
                                               tradeInfos = (
                                                                from objTrade in dataContext2.Trades
                                                                from objTradeInfo in dataContext2.TradeInfos
                                                                from objTradeFFAInfo in dataContext2.TradeFfaInfos
                                                                where
                                                                    objTrade.Id == objTradeInfo.TradeId
                                                                    && objTrade.Type == TradeTypeEnum.FFA
                                                                    && objTrade.Status == ActivationStatusEnum.Active
                                                                    && objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                                                    && tradeFfaInfoIds.Contains(objTradeFFAInfo.Id)
                                                                select
                                                                    new ClientsService.TradeInformation
                                                                        {
                                                                            Trade = objTrade,
                                                                            TradeInfo = objTradeInfo,
                                                                            TradeFFAInfo = objTradeFFAInfo
                                                                        }
                                                            ).ToList();

                                               tradeIds = tradeInfos.Select(a5 => a5.TradeFFAInfo.Id + "|FFA").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> lastResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();

                                                   foreach (KeyValuePair<string, Dictionary<DateTime, decimal>> keyValuePair in results)
                                                   {
                                                       ClientsService.TradeInformation tradeInfo = tradeInfos.Where(a6 => a6.TradeFFAInfo.Id + "|FFA" == keyValuePair.Key).Single();

                                                       var totalCommissionPercentage = dataContext2.TradeBrokerInfos.Where(a7 => a7.TradeInfoId == tradeInfo.TradeInfo.Id).Select(a7 => a7.Commission).Sum() / 100;

                                                       var result = keyValuePair.Value;
                                                       var totalCommission = result.Select(a6 => a6.Value * totalCommissionPercentage).Sum();
                                                       Dictionary<DateTime, decimal> lastMonthValues = new Dictionary<DateTime, decimal>();
                                                       foreach (KeyValuePair<DateTime, decimal> valuePair in result)
                                                       {
                                                           if (valuePair.Key.Year == tradeInfo.TradeInfo.SignDate.Year && valuePair.Key.Month == tradeInfo.TradeInfo.SignDate.Month)
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, totalCommission);
                                                           }
                                                           else
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, 0M);
                                                           }
                                                       }
                                                       lastResults.Add(keyValuePair.Key, lastMonthValues);
                                                   }

                                                   List<DateTime> months = lastResults.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => lastResults.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.OptionsCommissions:
                                               tradeInfos = (
                                                                from objTrade in dataContext2.Trades
                                                                from objTradeInfo in dataContext2.TradeInfos
                                                                from objTradeOptionInfo in dataContext2.TradeOptionInfos
                                                                where
                                                                    objTrade.Id == objTradeInfo.TradeId
                                                                    && objTrade.Type == TradeTypeEnum.Option
                                                                    && objTrade.Status == ActivationStatusEnum.Active
                                                                    && objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                                                    && tradeOptionInfoIds.Contains(objTradeOptionInfo.Id)
                                                                select
                                                                    new ClientsService.TradeInformation
                                                                        {
                                                                            Trade = objTrade,
                                                                            TradeInfo = objTradeInfo,
                                                                            TradeOptionInfo = objTradeOptionInfo
                                                                        }
                                                            ).ToList();

                                               tradeIds = tradeInfos.Select(a5 => a5.TradeOptionInfo.Id + "|OPTION").ToList();

                                               if (tradeIds.Count > 0)
                                               {
                                                   Dictionary<string, decimal> embeddedValues;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> results;
                                                   Dictionary<string, Dictionary<DateTime, decimal>> lastResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
                                                   string errorMessage;
                                                   int? res = GenerateMarkToMarket(userName, positionDate, curveDate, periodFrom, periodTo, "DYNAMIC", marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, false, false, "CASH FLOW", tradeIds, new Dictionary<long, string>(), out results, out embeddedValues, out indexesValues, out errorMessage);
                                                   if (res == null) throw new ApplicationException();

                                                   foreach (KeyValuePair<string, Dictionary<DateTime, decimal>> keyValuePair in results)
                                                   {
                                                       ClientsService.TradeInformation tradeInfo = tradeInfos.Where(a6 => a6.TradeOptionInfo.Id + "|OPTION" == keyValuePair.Key).Single();

                                                       var totalCommissionPercentage = dataContext2.TradeBrokerInfos.Where(a7 => a7.TradeInfoId == tradeInfo.TradeInfo.Id).Select(a7 => a7.Commission).Sum() / 100;

                                                       var result = keyValuePair.Value;
                                                       var totalCommission = result.Select(a6 => a6.Value * totalCommissionPercentage).Sum();
                                                       Dictionary<DateTime, decimal> lastMonthValues = new Dictionary<DateTime, decimal>();
                                                       foreach (KeyValuePair<DateTime, decimal> valuePair in result)
                                                       {
                                                           if (valuePair.Key.Year == tradeInfo.TradeInfo.SignDate.Year && valuePair.Key.Month == tradeInfo.TradeInfo.SignDate.Month)
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, totalCommission);
                                                           }
                                                           else
                                                           {
                                                               lastMonthValues.Add(valuePair.Key, 0M);
                                                           }
                                                       }
                                                       lastResults.Add(keyValuePair.Key, lastMonthValues);
                                                   }

                                                   List<DateTime> months = lastResults.First().Value.Keys.ToList();
                                                   Dictionary<DateTime, decimal> monthsValues = months.ToDictionary(month => month, month => lastResults.Select(r => r.Value[month]).Sum());
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsOperationalExpenses:
                                               var vesselInfoList18 = (from objCompany in dataContext2.Companies
                                                                       from objVessel in dataContext2.Vessels
                                                                       from objVesselInfo in dataContext2.VesselInfos
                                                                       from objTradeTcInfoLeg in dataContext2.TradeTcInfoLegs
                                                                       from objTradeTcInfo in dataContext2.TradeTcInfos
                                                                       from objTradeInfo in dataContext2.TradeInfos
                                                                       from objTrade in dataContext2.Trades
                                                                       where objVessel.CompanyId == objCompany.Id
                                                                             && objVesselInfo.VesselId == objVessel.Id
                                                                             && objTradeTcInfo.VesselId == objVessel.Id
                                                                             && objTradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                                             && objTrade.Id == objTradeInfo.TradeId
                                                                             && objTradeTcInfoLeg.TradeTcInfoId == objTradeTcInfo.Id
                                                                             && tradeTcInfoLegIds.Contains(objTradeTcInfoLeg.Id)
                                                                             && objTrade.Type == TradeTypeEnum.TC
                                                                             && objTrade.Status == ActivationStatusEnum.Active
                                                                             && 
                                                                             (
                                                                             (objCompany.Type == CompanyTypeEnum.Internal && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy)
                                                                             ||
                                                                             (objCompany.Type == CompanyTypeEnum.Internal && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell && objTradeTcInfo.IsBareboat == false)
                                                                             ||
                                                                             (objCompany.Type != CompanyTypeEnum.Internal && objTradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy && objTradeTcInfo.IsBareboat == true)
                                                                             ||
                                                                             (objCompany.Type != CompanyTypeEnum.Internal && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell && objTradeTcInfo.IsBareboat == false)
                                                                             )
                                                                       select new { Vessel = objVessel, VesselInfo = objVesselInfo }).Distinct().ToList();
                                               if (vesselInfoList18.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalOpex = 0M;
                                                       foreach (var vesselInfo in vesselInfoList18)
                                                       {
                                                           decimal opex = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               int currentAge = 0;
                                                               DateTime curDateFrom = vesselInfo.Vessel.DateBuilt;
                                                               DateTime curDateTo = currentDateFrom;

                                                               while (curDateFrom <= curDateTo)
                                                               {
                                                                   currentAge++;
                                                                   curDateFrom = new DateTime(curDateFrom.Year + 1, curDateFrom.Month, curDateFrom.Day);
                                                               }

                                                               if (currentAge > 30) currentAge = 30;

                                                               opex = (from objVesselExpenses in dataContext2.VesselExpenses
                                                                        where objVesselExpenses.Age == currentAge
                                                                              && objVesselExpenses.VesselId == vesselInfo.Vessel.Id
                                                                        select objVesselExpenses.OperationalExpenses).Single() * ((currentDateTo - currentDateFrom).Days + 1);
                                                           }
                                                           totalOpex = totalOpex + opex;
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalOpex);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsDrydockingExpenses:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalExpenses = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           double expenses = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               if (vesselInfo.Vessel.DateBuilt.Year > currentDateFrom.Year && vesselInfo.Vessel.DateBuilt.Month == currentDateFrom.Month)
                                                               {
                                                                   int currentAge = 0;
                                                                   DateTime curDateFrom = vesselInfo.Vessel.DateBuilt;
                                                                   DateTime curDateTo = currentDateFrom;

                                                                   while (curDateFrom <= curDateTo)
                                                                   {
                                                                       currentAge++;
                                                                       curDateFrom = new DateTime(curDateFrom.Year + 1, curDateFrom.Month, curDateFrom.Day);
                                                                   }

                                                                   if (currentAge > 30) currentAge = 30;

                                                                   expenses = Convert.ToDouble((from objVesselExpenses in dataContext2.VesselExpenses
                                                                               where objVesselExpenses.Age == currentAge
                                                                                     && objVesselExpenses.VesselId == vesselInfo.Vessel.Id
                                                                               select objVesselExpenses.DryDockExpenses).Single());
                                                               }
                                                           }
                                                           totalExpenses = totalExpenses + Convert.ToDecimal(expenses);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalExpenses);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsManagementFees:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalFees = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           double fees = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               fees = (vesselInfo.VesselInfo.ManagementFee.HasValue ? vesselInfo.VesselInfo.ManagementFee.Value : 0) * ((currentDateTo - currentDateFrom).Days + 1);
                                                           }
                                                           totalFees = totalFees + Convert.ToDecimal(fees);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalFees);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsCommercialFees:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalFees = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           double fees = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               fees = (vesselInfo.VesselInfo.CommercialFee.HasValue ? vesselInfo.VesselInfo.CommercialFee.Value : 0) * ((currentDateTo - currentDateFrom).Days + 1);
                                                           }
                                                           totalFees = totalFees + Convert.ToDecimal(fees);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalFees);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsScrappings:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalScrappings = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           double scrappings = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               DateTime usefulLifeEnd = vesselInfo.Vessel.DateBuilt.AddYears(vesselInfo.VesselInfo.UsefulLife.HasValue ? vesselInfo.VesselInfo.UsefulLife.Value : 25);
                                                               if (currentDateFrom.Year == usefulLifeEnd.Year && currentDateFrom.Month == usefulLifeEnd.Month)
                                                               {
                                                                   double scrapPrice = 0;
                                                                   AppParameter parameter = dataContext2.AppParameters.SingleOrDefault(var1 => var1.Code == "SCRAP_PRICE");
                                                                   try
                                                                   {
                                                                       scrapPrice = Convert.ToDouble(parameter.Value);
                                                                   }
                                                                   catch (Exception)
                                                                   {

                                                                   }
                                                                   scrappings = (vesselInfo.VesselInfo.LightWeight.HasValue ? vesselInfo.VesselInfo.LightWeight.Value : 0) * scrapPrice;
                                                               }
                                                           }
                                                           totalScrappings = totalScrappings + Convert.ToDecimal(scrappings);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalScrappings);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsSales:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalSales = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           double sales = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate.Value.Year == currentDateFrom.Year && vesselInfo.VesselInfo.SaleDate.Value.Month == currentDateFrom.Month)
                                                           {
                                                               sales = vesselInfo.VesselInfo.SaleAmount.HasValue ? vesselInfo.VesselInfo.SaleAmount.Value : 0;
                                                           }
                                                           totalSales = totalSales + Convert.ToDecimal(sales);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalSales);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsAcquisitionCosts:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalPrice = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           double price = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               if (vesselInfo.VesselInfo.DeliveryDate != null && currentDateFrom.Year == vesselInfo.VesselInfo.DeliveryDate.Value.Year && currentDateFrom.Month == vesselInfo.VesselInfo.DeliveryDate.Value.Month)
                                                               {
                                                                   price = vesselInfo.Vessel.AcquisitionPrice;
                                                               }
                                                           }
                                                           totalPrice = totalPrice + Convert.ToDecimal(price);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalPrice);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.VesselsEquityInjection:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   Dictionary<DateTime, decimal> monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalPrice = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal price = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               if (vesselInfo.VesselInfo.DeliveryDate != null && currentDateFrom.Year == vesselInfo.VesselInfo.DeliveryDate.Value.Year && currentDateFrom.Month == vesselInfo.VesselInfo.DeliveryDate.Value.Month)
                                                               {
                                                                   price = vesselInfo.VesselInfo.Equity.HasValue ? vesselInfo.VesselInfo.Equity.Value : 0;
                                                               }
                                                           }
                                                           totalPrice = totalPrice + Convert.ToDecimal(price);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalPrice);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansCapitalRepayments:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               List<LoanPrincipalInstallment> loanPrincipalInstallments = (from objLoanPrincipalInstallment in dataContext2.LoanPrincipalInstallments
                                                                                                                           from objLoanTranche in dataContext2.LoanTranches
                                                                                                                           from objLoan in dataContext2.Loans
                                                                                                                           where objLoanPrincipalInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                                                                 && objLoan.Id == objLoanTranche.LoanId
                                                                                                                                 && objLoan.Status == ActivationStatusEnum.Active
                                                                                                                                 && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                                                                     where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                                                                     select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                                                           select objLoanPrincipalInstallment).ToList();

                                                               amount = loanPrincipalInstallments.Where(a102 => a102.Date.Year == currentDateFrom.Year && a102.Date.Month == currentDateFrom.Month).Select(a102 => a102.Amount).Sum();
                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansInterestExpenses:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               List<LoanInterestInstallment> loanInterestInstallments = (from objLoanInterestInstallment in dataContext2.LoanInterestInstallments
                                                                                                                         from objLoanTranche in dataContext2.LoanTranches
                                                                                                                         from objLoan in dataContext2.Loans
                                                                                                                         where objLoanInterestInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                                                               && objLoan.Id == objLoanTranche.LoanId
                                                                                                                               && objLoan.Status == ActivationStatusEnum.Active
                                                                                                                               && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                                                                   where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                                                                   select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                                                         select objLoanInterestInstallment).ToList();

                                                               foreach (var loanInterestInstallment in loanInterestInstallments.Where(a103 => a103.Date.Year == currentDateFrom.Year && a103.Date.Month == currentDateFrom.Month))
                                                               {
                                                                   long remainingPrincipal = (from objLoanPrincipalInstallment in dataContext2.LoanPrincipalInstallments
                                                                                              where objLoanPrincipalInstallment.LoanTrancheId == loanInterestInstallment.LoanTrancheId
                                                                                                    && objLoanPrincipalInstallment.Date >= loanInterestInstallment.Date
                                                                                              select objLoanPrincipalInstallment.Amount).Sum();

                                                                   var corrFactor = (from objLoanTranche in dataContext2.LoanTranches
                                                                                               where loanInterestInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                               select objLoanTranche.InterestCorFactor).SingleOrDefault();

                                                                   decimal correctionFactor = corrFactor.HasValue ? corrFactor.Value : 0;

                                                                   if (loanInterestInstallment.InterestRateType == LoanInterestRateTypeEnum.Fixed)
                                                                   {
                                                                       // TODO Exchange Rates and Currencies
                                                                       amount = amount + loanInterestInstallment.InterestRate / 100 * remainingPrincipal;
                                                                   }
                                                                   else
                                                                   {
                                                                       var rates = (from objInterestRate in dataContext2.InterestReferenceRates
                                                                                    where objInterestRate.PeriodType == loanInterestInstallment.PeriodType.Value
                                                                                          && objInterestRate.Date <= loanInterestInstallment.Date
                                                                                          && objInterestRate.Type == InterestReferenceRateTypeEnum.Libor
                                                                                    orderby objInterestRate.Date descending
                                                                                    select objInterestRate).ToList();

                                                                       if (rates.Count != 0)
                                                                       {
                                                                           // TODO Exchange Rates and Currencies
                                                                           decimal days = 0;
                                                                           InterestReferenceRate rate = rates.First();
                                                                           if(rate.PeriodType == InterestReferencePeriodTypeEnum.Months3)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(3).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months2)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(2).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months4)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(4).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months5)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(5).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months6)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(6).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months7)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(7).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months8)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(8).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months9)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(9).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months10)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(10).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Months11)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddMonths(11).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year1)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(1).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year2)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(2).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year3)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(3).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year5)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(5).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year7)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(7).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year10)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(10).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year20)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(20).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Year30)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddYears(30).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Week1)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddDays(7).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Weeks2)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddDays(14).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }
                                                                           else if (rate.PeriodType == InterestReferencePeriodTypeEnum.Weeks3)
                                                                           {
                                                                               days = Convert.ToDecimal((new DateTime(rate.Date.Year, rate.Date.Month, 1).AddDays(21).AddDays(-1) - new DateTime(rate.Date.Year, rate.Date.Month, 1)).Days + 1);
                                                                           }

                                                                           amount = amount + (remainingPrincipal * (loanInterestInstallment.InterestRate / 100 + rate.Rate / 100 + correctionFactor/100) * days/360);
                                                                       }
                                                                   }
                                                               }
                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansBaloonPayments:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               var loanInfos = (from objLoanPrincipalInstallment in dataContext2.LoanPrincipalInstallments
                                                                                from objLoanTranche in dataContext2.LoanTranches
                                                                                from objLoan in dataContext2.Loans
                                                                                where objLoanPrincipalInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                      && objLoan.Id == objLoanTranche.LoanId
                                                                                      && objLoan.Status == ActivationStatusEnum.Active
                                                                                      && objLoanPrincipalInstallment.Amount > 0
                                                                                      && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                          where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                          select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                select new { LoanPrincipalInstallment = objLoanPrincipalInstallment, LoanTranche = objLoanTranche }).ToList();

                                                               var lastLoanInfos = (from objLoanInfo in loanInfos
                                                                                    orderby objLoanInfo.LoanPrincipalInstallment.Date descending
                                                                                    group objLoanInfo by objLoanInfo.LoanTranche.LoanId
                                                                                        into myGroup
                                                                                        select myGroup.First()).ToList();


                                                               foreach (var lastLoanInfo in lastLoanInfos)
                                                               {
                                                                   if (currentDateFrom.Year == lastLoanInfo.LoanPrincipalInstallment.Date.Year && currentDateFrom.Month == lastLoanInfo.LoanPrincipalInstallment.Date.Month)
                                                                   {
                                                                       amount = amount + lastLoanInfo.LoanTranche.BalloonAmount;
                                                                   }
                                                               }

                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansPrepayments:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate == null)
                                                           {
                                                               // Vessel is not sold
                                                           }
                                                           else
                                                           {
                                                               var loanInfos = (from objLoanPrincipalInstallment in dataContext2.LoanPrincipalInstallments
                                                                                from objLoanTranche in dataContext2.LoanTranches
                                                                                from objLoan in dataContext2.Loans
                                                                                where objLoanPrincipalInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                      && objLoan.Id == objLoanTranche.LoanId
                                                                                      && objLoan.Status == ActivationStatusEnum.Active
                                                                                      && objLoanPrincipalInstallment.Amount > 0
                                                                                      && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                          where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                          select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                select new { LoanPrincipalInstallment = objLoanPrincipalInstallment, LoanTranche = objLoanTranche }).ToList();

                                                               var loanInfosAfterSaleDate = loanInfos.Where(b103 => b103.LoanPrincipalInstallment.Date > vesselInfo.VesselInfo.SaleDate).ToList();

                                                               if (loanInfosAfterSaleDate.Count > 0)
                                                               {
                                                                   var interestInstallmentsAfterSaleDate = (from objLoanInterestInstallment in dataContext2.LoanInterestInstallments
                                                                                                            from objLoanTranche in dataContext2.LoanTranches
                                                                                                            from objLoan in dataContext2.Loans
                                                                                                            where objLoanInterestInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                                                  && objLoan.Id == objLoanTranche.LoanId
                                                                                                                  && objLoan.Status == ActivationStatusEnum.Active
                                                                                                                  && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                                                      where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                                                      select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                                                  && objLoanInterestInstallment.Date > vesselInfo.VesselInfo.SaleDate
                                                                                                            select objLoanInterestInstallment).ToList();
                                                                   if (interestInstallmentsAfterSaleDate.Count > 0)
                                                                   {
                                                                       var firstInterestInstallmentAfterSaleDate = interestInstallmentsAfterSaleDate.OrderBy(b104 => b104.Date).First();
                                                                       if (currentDateFrom.Year == firstInterestInstallmentAfterSaleDate.Date.Year && currentDateFrom.Month == firstInterestInstallmentAfterSaleDate.Date.Month)
                                                                       {
                                                                           amount = amount + loanInfosAfterSaleDate.Select(c23 => c23.LoanPrincipalInstallment.Amount).Sum() + loanInfosAfterSaleDate.Select(c23 => c23.LoanTranche.BalloonAmount).Sum();
                                                                       }
                                                                   }
                                                               }
                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansAdministrationFees:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               var loans = (from objLoan in dataContext2.Loans
                                                                            where objLoan.Status == ActivationStatusEnum.Active
                                                                                  && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                      where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                      select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                            select objLoan).ToList();

                                                               foreach (var loan in loans)
                                                               {
                                                                   if (loan.SignDate.Year >= currentDateFrom.Year && loan.SignDate.Month == currentDateFrom.Month)
                                                                   {
                                                                       amount = amount + (loan.AdminFee.HasValue ? loan.AdminFee.Value : 0);
                                                                   }
                                                               }

                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansArrangementFees:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               var loans = (from objLoan in dataContext2.Loans
                                                                            where objLoan.Status == ActivationStatusEnum.Active
                                                                                  && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                      where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                      select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                            select objLoan).ToList();

                                                               foreach (var loan in loans)
                                                               {
                                                                   if (loan.SignDate.Year == currentDateFrom.Year && loan.SignDate.Month == currentDateFrom.Month)
                                                                   {
                                                                       amount = amount + (loan.ArrangeFee.HasValue ? loan.ArrangeFee.Value : 0);
                                                                   }
                                                               }

                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansPrepaymentFees:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate == null)
                                                           {
                                                               // Vessel is not sold
                                                           }
                                                           else
                                                           {
                                                               var loanInfos = (from objLoanPrincipalInstallment in dataContext2.LoanPrincipalInstallments
                                                                                from objLoanTranche in dataContext2.LoanTranches
                                                                                from objLoan in dataContext2.Loans
                                                                                where objLoanPrincipalInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                      && objLoan.Id == objLoanTranche.LoanId
                                                                                      && objLoan.Status == ActivationStatusEnum.Active
                                                                                      && objLoanPrincipalInstallment.Amount > 0
                                                                                      && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                          where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                          select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                select new { LoanPrincipalInstallment = objLoanPrincipalInstallment, LoanTranche = objLoanTranche }).ToList();

                                                               var loanInfosAfterSaleDate = loanInfos.Where(b103 => b103.LoanPrincipalInstallment.Date > vesselInfo.VesselInfo.SaleDate).ToList();

                                                               if (loanInfosAfterSaleDate.Count > 0)
                                                               {
                                                                   var interestInstallmentsAfterSaleDate = (from objLoanInterestInstallment in dataContext2.LoanInterestInstallments
                                                                                                            from objLoanTranche in dataContext2.LoanTranches
                                                                                                            from objLoan in dataContext2.Loans
                                                                                                            where objLoanInterestInstallment.LoanTrancheId == objLoanTranche.Id
                                                                                                                  && objLoan.Id == objLoanTranche.LoanId
                                                                                                                  && objLoan.Status == ActivationStatusEnum.Active
                                                                                                                  && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                                                      where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                                                      select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                                                  && objLoanInterestInstallment.Date > vesselInfo.VesselInfo.SaleDate
                                                                                                            select new { LoanInterestInstallment = objLoanInterestInstallment, Loan = objLoan }).ToList();
                                                                   if (interestInstallmentsAfterSaleDate.Count > 0)
                                                                   {
                                                                       var firstInterestInstallmentAfterSaleDate = interestInstallmentsAfterSaleDate.OrderBy(b104 => b104.LoanInterestInstallment.Date).First();
                                                                       if (currentDateFrom.Year == firstInterestInstallmentAfterSaleDate.LoanInterestInstallment.Date.Year && currentDateFrom.Month == firstInterestInstallmentAfterSaleDate.LoanInterestInstallment.Date.Month)
                                                                       {
                                                                           amount = amount + (loanInfosAfterSaleDate.Select(c23 => c23.LoanPrincipalInstallment.Amount).Sum() + loanInfosAfterSaleDate.Select(c23 => c23.LoanTranche.BalloonAmount).Sum()) * (firstInterestInstallmentAfterSaleDate.Loan.PrepayFee.HasValue ? firstInterestInstallmentAfterSaleDate.Loan.PrepayFee.Value / 100 : 0);
                                                                       }
                                                                   }
                                                               }
                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansCommitmentFees:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               var loanInfos = (from objLoanDrawdown in dataContext2.LoanDrawdowns
                                                                                from objLoanTranche in dataContext2.LoanTranches
                                                                                from objLoan in dataContext2.Loans
                                                                                where objLoanDrawdown.LoanTrancheId == objLoanTranche.Id
                                                                                      && objLoan.Id == objLoanTranche.LoanId
                                                                                      && objLoan.Status == ActivationStatusEnum.Active
                                                                                      && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                          where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                          select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                select new { LoanDrawdown = objLoanDrawdown, LoanTranche = objLoanTranche }).ToList();

                                                               var loanTranches = loanInfos.Select(b105 => b105.LoanTranche).Distinct().ToList();

                                                               foreach (var loanTranche in loanTranches)
                                                               {
                                                                   long undrawnTrancheAmount = loanInfos.Where(b104 => b104.LoanDrawdown.Date > currentDateTo && b104.LoanTranche.Id == loanTranche.Id).Select(b104 => b104.LoanDrawdown.Amount).Sum();
                                                                   amount = amount + (loanTranche.CommitmentFee.HasValue ? (undrawnTrancheAmount * (((currentDateTo - currentDateFrom).Days + 1) / 365) * (loanTranche.CommitmentFee.Value / 100)) : 0);
                                                               }
                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;

                                           case CashFlowItemSystemTypeEnum.LoansDrawdowns:

                                               if (vesselInfoList.Count > 0)
                                               {
                                                   var monthsValues = new Dictionary<DateTime, decimal>();
                                                   DateTime currentDateFrom = periodFrom;
                                                   DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                                   while (currentDateFrom <= periodTo)
                                                   {
                                                       decimal totalAmount = 0M;
                                                       foreach (var vesselInfo in vesselInfoList)
                                                       {
                                                           decimal amount = 0;

                                                           if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                                           {
                                                               // Vessel is sold
                                                           }
                                                           else
                                                           {
                                                               var loanDrawdowns = (from objLoanDrawdown in dataContext2.LoanDrawdowns
                                                                                    from objLoanTranche in dataContext2.LoanTranches
                                                                                    from objLoan in dataContext2.Loans
                                                                                    where objLoanDrawdown.LoanTrancheId == objLoanTranche.Id
                                                                                          && objLoan.Id == objLoanTranche.LoanId
                                                                                          && objLoan.Status == ActivationStatusEnum.Active
                                                                                          && (from objLoanCollateralAsset in dataContext2.LoanCollateralAssets
                                                                                              where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                                                              select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                                                    select objLoanDrawdown).ToList();

                                                               amount = amount + loanDrawdowns.Where(d34 => d34.Date.Year == currentDateFrom.Year && d34.Date.Month == currentDateFrom.Month).Select((d34 => d34.Amount)).Sum();
                                                           }
                                                           totalAmount = totalAmount + Convert.ToDecimal(amount);
                                                       }

                                                       monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                                                       currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                                       currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                                       if (periodTo < currentDateTo) currentDateTo = periodTo;
                                                   }
                                                   cashFlowItemResult.Add(a.Id, monthsValues);
                                               }
                                               break;
                                       }
                                   }
                                   else
                                   {
                                       var monthsValues = new Dictionary<DateTime, decimal>();
                                       DateTime currentDateFrom = periodFrom;
                                       DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                       while (currentDateFrom <= periodTo)
                                       {
                                           if (currentDateFrom.Year == a.CustomDueDate.Value.Year && currentDateFrom.Month == a.CustomDueDate.Value.Month)
                                           {
                                               // TODO Exchange Rate Calculation for the selected currency if implemented.
                                               monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), a.CustomAmount.Value);
                                           }
                                           else
                                           {
                                               monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), 0M);
                                           }
                                           currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                           currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                           if (periodTo < currentDateTo) currentDateTo = periodTo;
                                       }
                                       cashFlowItemResult.Add(a.Id, monthsValues);
                                   }
                                   if (cashFlowItemResult.Count == 0)
                                   {
                                       var months = new List<DateTime>();
                                       DateTime currentDateFrom = periodFrom;
                                       DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                                       while (currentDateFrom <= periodTo)
                                       {
                                           months.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1));
                                           currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                                           currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                                           if (periodTo < currentDateTo) currentDateTo = periodTo;
                                       }

                                       cashFlowItemResult.Add(a.Id, months.ToDictionary(month => month, month => 0M));
                                   }
                                   return cashFlowItemResult;
                               }).ToList();

                #region Break Even

                if (calculateBreakEven)
                {
                    List<long> breakEvenRelatedCashFlowItems = cashFlowItems.Where(
                        f12 => f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsMinimum
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsOptional
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsFixedHireMinimum
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsFixedHireOptional
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireMinimum
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireOptional
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsProfitSharing
                               || f12.SystemType == CashFlowItemSystemTypeEnum.FFAsSettlement
                               || f12.SystemType == CashFlowItemSystemTypeEnum.FreightOptionsSettlement
                               || f12.SystemType == CashFlowItemSystemTypeEnum.FreightOptionsPremium
                               || f12.SystemType == CashFlowItemSystemTypeEnum.TCCommissions
                               || f12.SystemType == CashFlowItemSystemTypeEnum.CargoCommissions
                               || f12.SystemType == CashFlowItemSystemTypeEnum.FFACommissions
                               || f12.SystemType == CashFlowItemSystemTypeEnum.OptionsCommissions
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsOperationalExpenses
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansCapitalRepayments
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansInterestExpenses
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansPrepayments
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansAdministrationFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansArrangementFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansPrepaymentFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansCommitmentFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsDrydockingExpenses
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsManagementFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsCommercialFees
                        ).Select(f12 => f12.Id).ToList();

                    var monthsValues = new Dictionary<DateTime, decimal>();
                    DateTime currentDateFrom = periodFrom;
                    DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                    while (currentDateFrom <= periodTo)
                    {
                        decimal totalAmount = 0M;
                        decimal totalDays = 0M;
                        int totalOffhireDays = 0;

                        foreach (long breakEvenRelatedCashFlowItem in breakEvenRelatedCashFlowItems)
                        {
                            if (cashFlowItemResults.ContainsKey(breakEvenRelatedCashFlowItem)) totalAmount = totalAmount + (cashFlowItemResults[breakEvenRelatedCashFlowItem][currentDateFrom]);
                        }

                        foreach (var vesselInfo in vesselInfoList)
                        {
                            int offHireDays = 0;

                            if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                            {
                                // Vessel is sold
                            }
                            else
                            {
                                if (vesselInfo.Vessel.DateBuilt.Year > currentDateFrom.Year && vesselInfo.Vessel.DateBuilt.Month == currentDateFrom.Month)
                                {
                                    int currentAge = 0;
                                    DateTime curDateFrom = vesselInfo.Vessel.DateBuilt;
                                    DateTime curDateTo = currentDateFrom;

                                    while (curDateFrom <= curDateTo)
                                    {
                                        currentAge++;
                                        curDateFrom = new DateTime(curDateFrom.Year + 1, curDateFrom.Month, curDateFrom.Day);
                                    }

                                    if (currentAge > 30) currentAge = 30;

                                    offHireDays = (from objVesselExpenses in dataContextRoot.VesselExpenses
                                                   where objVesselExpenses.Age == currentAge
                                                         && objVesselExpenses.VesselId == vesselInfo.Vessel.Id
                                                   select objVesselExpenses.DryDockOffHire).Single();
                                }
                            }
                            totalOffhireDays = totalOffhireDays + offHireDays;
                        }

                        totalDays = Convert.ToDecimal((currentDateTo - currentDateFrom).Days + 1);

                        monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount/(totalDays - totalOffhireDays));

                        currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                        currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                        if (periodTo < currentDateTo) currentDateTo = periodTo;
                    }
                    breakEvenResultsAll.Add("BreakEvenAll", monthsValues);

                    monthsValues = new Dictionary<DateTime, decimal>();
                    currentDateFrom = periodFrom;
                    currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                    while (currentDateFrom <= periodTo)
                    {
                        decimal totalAmount = 0M;
                        decimal totalDays = Convert.ToDecimal((currentDateTo - currentDateFrom).Days + 1);
                        int totalOffhireDays = 0;
                        decimal totalSpot = 0M;
                        decimal fraction = 1M;

                        foreach (var vesselInfo in vesselInfoList)
                        {
                            decimal spot = 0;

                            if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                            {
                                // Vessel is sold
                            }
                            else
                            {
                                List<ClientsService.TradeInformation> tradeInfoos = (from objTrade in dataContextRoot.Trades
                                                                                     from objTradeInfo in dataContextRoot.TradeInfos
                                                                                     from objTradeTCInfo in dataContextRoot.TradeTcInfos
                                                                                     from objTradeTCInfoLeg in dataContextRoot.TradeTcInfoLegs
                                                                                     where
                                                                                         objTrade.Id == objTradeInfo.TradeId
                                                                                         && objTrade.Status == ActivationStatusEnum.Active
                                                                                         && objTrade.Type == TradeTypeEnum.TC
                                                                                         && objTradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell
                                                                                         && objTradeTCInfo.TradeInfoId == objTradeInfo.Id
                                                                                         && objTradeTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id
                                                                                         && objTradeTCInfo.VesselId == vesselInfo.Vessel.Id
                                                                                         && objTradeTCInfoLeg.PeriodTo >= currentDateFrom
                                                                                         && objTradeTCInfoLeg.PeriodFrom <= currentDateTo
                                                                                     select
                                                                                         new ClientsService.TradeInformation
                                                                                         {
                                                                                             Trade = objTrade,
                                                                                             TradeInfo = objTradeInfo,
                                                                                             TradeTCInfo = objTradeTCInfo,
                                                                                             TradeTCInfoLeg = objTradeTCInfoLeg
                                                                                         }).ToList();

                                var tradeInfo = tradeInfoos.Where(rf => rf.TradeInfo.Id == tradeInfoos.Max(er => er.TradeInfo.Id)).SingleOrDefault();

                                decimal periodPrice;
                                decimal periodMarketPrice;
                                Dictionary<DateTime, decimal> finalFFAValues;
                                Dictionary<DateTime, decimal> marketPrices;

                                if (tradeInfo != null)
                                {
                                    var positionDays = (decimal) (new TimeSpan(
                                                                     Math.Max(
                                                                         Math.Min(currentDateTo.Ticks, tradeInfo.TradeTCInfoLeg.PeriodTo.Ticks) -
                                                                         Math.Max(currentDateFrom.Ticks, tradeInfo.TradeTCInfoLeg.PeriodFrom.Ticks) +
                                                                         TimeSpan.TicksPerDay, 0))).Days;

                                    if (tradeInfo.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                                    {
                                        if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                        {
                                            periodPrice = Convert.ToDecimal(dataContextRoot.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeTCInfoLeg.IndexId.Value && a100.Date >= currentDateFrom && a100.Date <= currentDateTo && a100.Date >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Rate).Average());
                                        }
                                        else
                                        {
                                            marketPrices = CalculateForwardCurveValues(tradeInfo.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContextRoot, out finalFFAValues);
                                            periodPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();
                                        }
                                        periodPrice = periodPrice*tradeInfo.TradeTCInfoLeg.IndexPercentage.Value/100;
                                    }
                                    else
                                    {
                                        periodPrice = tradeInfo.TradeTCInfoLeg.Rate.Value;
                                    }
                                    periodPrice = Math.Round(periodPrice, 4);
                                    if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                                    {
                                        periodMarketPrice = Convert.ToDecimal(dataContextRoot.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeInfo.MTMFwdIndexId && a100.Date >= currentDateFrom && a100.Date <= currentDateTo && a100.Date >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Rate).Average());
                                    }
                                    else
                                    {
                                        if (marketSensitivityType.ToUpper() == "STRESS")
                                        {
                                            marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContextRoot, out finalFFAValues);
                                        }
                                        else
                                        {
                                            marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContextRoot, out finalFFAValues);
                                        }
                                        periodMarketPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();

                                        if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                                        {
                                            periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                                        }
                                        else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                                        {
                                            periodMarketPrice = periodMarketPrice + (periodMarketPrice*marketSensitivityValue/100);
                                        }
                                    }
                                    
                                    periodMarketPrice = periodMarketPrice*tradeInfo.TradeTCInfo.VesselIndex/100;
                                    periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                    if (tradeInfo.TradeTCInfoLeg.IsOptional && tradeInfo.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.NonDeclared)
                                    {
                                        positionDays = 0;
                                    }
                                    else
                                    {
                                        if (positionMethod.ToUpper() == "STATIC")
                                        {
                                            // Already calculated above
                                        }
                                        else if (positionMethod.ToUpper() == "DYNAMIC")
                                        {
                                            if (!tradeInfo.TradeTCInfoLeg.IsOptional || (tradeInfo.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Declared))
                                            {
                                                // Already calculated above
                                            }
                                            else if (tradeInfo.TradeTCInfoLeg.IsOptional && tradeInfo.TradeTCInfoLeg.OptionalStatus == TradeInfoLegOptionalStatusEnum.Pending)
                                            {
                                                if (!IsOptionalTradeLegInTheMoney(tradeInfo, currentDateFrom, currentDateTo, tradeInfo.Trade.Type, curveDate, useSpot, userName, dataContextRoot, marketSensitivityType, marketSensitivityValue)) positionDays = 0;
                                            }
                                        }
                                        else if (positionMethod.ToUpper() == "DELTA")
                                        {
                                            // Already calculated above
                                        }
                                    }

                                    if (positionDays == 0)
                                    {
                                        // no trade leg for this month or out of the money. Spot is calculated.
                                        marketPrices = CalculateForwardCurveValues(dataContextRoot.Indexes.Single(h2 => h2.MarketId == vesselInfo.Vessel.MarketId && h2.IsMarketDefault).Id, curveDate, 0, 0, userName, dataContextRoot, out finalFFAValues);
                                        periodMarketPrice = marketPrices.Where(a100 => a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodTo && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();
                                        
                                        periodMarketPrice = periodMarketPrice*(dataContextRoot.VesselBenchmarkings.Where(g1 => tradeInfo.TradeTCInfoLeg.PeriodTo >= g1.PeriodFrom && tradeInfo.TradeTCInfoLeg.PeriodTo <= g1.PeriodTo && g1.VesselId == vesselInfo.Vessel.Id).ToList().First().Benchmark/100);
                                        periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                        spot = ((currentDateTo - tradeInfo.TradeTCInfoLeg.PeriodTo).Days + 1)*periodMarketPrice;
                                        fraction = ((currentDateTo - tradeInfo.TradeTCInfoLeg.PeriodTo).Days + 1)/((currentDateTo - currentDateFrom).Days + 1);
                                        totalDays = (currentDateTo - tradeInfo.TradeTCInfoLeg.PeriodTo).Days + 1;
                                    }
                                    else
                                    {
                                        // In the money TC OUT Trade. Spot is not calculated.
                                        spot = 0;
                                    }
                                }
                                else
                                {
                                    // There is no TC OUT Trade for this Vessel. Spot is calculated.
                                    marketPrices = CalculateForwardCurveValues(dataContextRoot.Indexes.Single(h1 => h1.MarketId == vesselInfo.Vessel.MarketId && h1.IsMarketDefault).Id, curveDate, 0, 0, userName, dataContextRoot, out finalFFAValues);
                                    periodMarketPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Key <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Value).Average();
                                    
                                    periodMarketPrice = periodMarketPrice*(dataContextRoot.VesselBenchmarkings.Where(g1 => currentDateFrom >= g1.PeriodFrom && currentDateFrom <= g1.PeriodTo && g1.VesselId == vesselInfo.Vessel.Id).ToList().First().Benchmark/100);
                                    periodMarketPrice = Math.Round(periodMarketPrice, 4);
                                    spot = ((currentDateTo - currentDateFrom).Days + 1)*periodMarketPrice;
                                }
                            }
                            totalSpot = totalSpot + Convert.ToDecimal(spot);
                        }

                        if (totalSpot > 0)
                        {
                            foreach (long breakEvenRelatedCashFlowItem in breakEvenRelatedCashFlowItems)
                            {
                                if (cashFlowItemResults.ContainsKey(breakEvenRelatedCashFlowItem) != null) totalAmount = totalAmount + (cashFlowItemResults[breakEvenRelatedCashFlowItem][currentDateFrom]);
                            }

                            foreach (var vesselInfo in vesselInfoList)
                            {
                                int offHireDays = 0;

                                if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                {
                                    // Vessel is sold
                                }
                                else
                                {
                                    if (vesselInfo.Vessel.DateBuilt.Year > currentDateFrom.Year && vesselInfo.Vessel.DateBuilt.Month == currentDateFrom.Month)
                                    {
                                        int currentAge = 0;
                                        DateTime curDateFrom = vesselInfo.Vessel.DateBuilt;
                                        DateTime curDateTo = currentDateFrom;

                                        while (curDateFrom <= curDateTo)
                                        {
                                            currentAge++;
                                            curDateFrom = new DateTime(curDateFrom.Year + 1, curDateFrom.Month, curDateFrom.Day);
                                        }

                                        if (currentAge > 30) currentAge = 30;

                                        offHireDays = (from objVesselExpenses in dataContextRoot.VesselExpenses
                                                       where objVesselExpenses.Age == currentAge
                                                             && objVesselExpenses.VesselId == vesselInfo.Vessel.Id
                                                       select objVesselExpenses.DryDockOffHire).Single();
                                    }
                                }
                                totalOffhireDays = totalOffhireDays + offHireDays;
                            }
                        }

                        monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount*fraction/(totalDays - totalOffhireDays));

                        currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                        currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                        if (periodTo < currentDateTo) currentDateTo = periodTo;
                    }
                    breakEvenResultsSpot.Add("BreakEvenSpot", monthsValues);

                    if (calculateBEBalloon)
                    {
                        monthsValues = new Dictionary<DateTime, decimal>();
                        currentDateFrom = periodFrom;
                        currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                        while (currentDateFrom <= periodTo)
                        {

                            decimal totalAmount = 0M;
                            foreach (var vesselInfo in vesselInfoList)
                            {
                                decimal amount = 0;

                                if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                {
                                    // Vessel is sold
                                }
                                else
                                {
                                    var loanInfos = (from objLoanPrincipalInstallment in dataContextRoot.LoanPrincipalInstallments
                                                     from objLoanTranche in dataContextRoot.LoanTranches
                                                     from objLoan in dataContextRoot.Loans
                                                     where objLoanPrincipalInstallment.LoanTrancheId == objLoanTranche.Id
                                                           && objLoan.Id == objLoanTranche.LoanId
                                                           && objLoan.Status == ActivationStatusEnum.Active
                                                           && objLoanPrincipalInstallment.Amount > 0
                                                           && (from objLoanCollateralAsset in dataContextRoot.LoanCollateralAssets
                                                               where objLoanCollateralAsset.VesselId == vesselInfo.Vessel.Id
                                                               select objLoanCollateralAsset.LoanId).Contains(objLoan.Id)
                                                     select new {LoanPrincipalInstallment = objLoanPrincipalInstallment, LoanTranche = objLoanTranche}).ToList();

                                    var lastLoanInfos = (from objLoanInfo in loanInfos
                                                         orderby objLoanInfo.LoanPrincipalInstallment.Date descending
                                                         group objLoanInfo by objLoanInfo.LoanTranche.LoanId
                                                         into myGroup
                                                         select myGroup.First()).ToList();


                                    foreach (var lastLoanInfo in lastLoanInfos)
                                    {
                                        if (currentDateFrom.Year == lastLoanInfo.LoanPrincipalInstallment.Date.Year && currentDateFrom.Month == lastLoanInfo.LoanPrincipalInstallment.Date.Month)
                                        {
                                            amount = amount + lastLoanInfo.LoanTranche.BalloonAmount;
                                        }
                                    }

                                }
                                totalAmount = totalAmount + Convert.ToDecimal(amount);
                            }

                            monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                            currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                            currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                            if (periodTo < currentDateTo) currentDateTo = periodTo;
                        }
                        breakEvenResultsBalloon.Add("BreakEvenBalloon", monthsValues);
                    }

                    if (calculateBEEquity)
                    {
                        monthsValues = new Dictionary<DateTime, decimal>();
                        currentDateFrom = periodFrom;
                        currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                        while (currentDateFrom <= periodTo)
                        {
                            decimal totalPrice = 0M;
                            foreach (var vesselInfo in vesselInfoList)
                            {
                                decimal price = 0;

                                if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                {
                                    // Vessel is sold
                                }
                                else
                                {
                                    if (vesselInfo.VesselInfo.DeliveryDate != null && currentDateFrom.Year == vesselInfo.VesselInfo.DeliveryDate.Value.Year && currentDateFrom.Month == vesselInfo.VesselInfo.DeliveryDate.Value.Month)
                                    {
                                        price = vesselInfo.VesselInfo.Equity.HasValue ? vesselInfo.VesselInfo.Equity.Value : 0;
                                    }
                                }
                                totalPrice = totalPrice + Convert.ToDecimal(price);
                            }

                            monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalPrice);

                            currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                            currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                            if (periodTo < currentDateTo) currentDateTo = periodTo;
                        }
                        breakEvenResultsEquity.Add("BreakEvenEquity", monthsValues);

                    }

                    if (calculateBEScrapValue)
                    {
                        monthsValues = new Dictionary<DateTime, decimal>();
                        currentDateFrom = periodFrom;
                        currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                        while (currentDateFrom <= periodTo)
                        {
                            decimal totalScrappings = 0M;
                            foreach (var vesselInfo in vesselInfoList)
                            {
                                double scrappings = 0;

                                if (vesselInfo.VesselInfo.SaleDate != null && vesselInfo.VesselInfo.SaleDate < currentDateFrom)
                                {
                                    // Vessel is sold
                                }
                                else
                                {
                                    DateTime usefulLifeEnd = vesselInfo.Vessel.DateBuilt.AddYears(vesselInfo.VesselInfo.UsefulLife.HasValue ? vesselInfo.VesselInfo.UsefulLife.Value : 25);
                                    if (currentDateFrom.Year == usefulLifeEnd.Year && currentDateFrom.Month == usefulLifeEnd.Month)
                                    {
                                        double scrapPrice = 0;
                                        AppParameter parameter = dataContextRoot.AppParameters.SingleOrDefault(var1 => var1.Code == "SCRAP_PRICE");
                                        try
                                        {
                                            scrapPrice = Convert.ToDouble(parameter.Value);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        scrappings = (vesselInfo.VesselInfo.LightWeight.HasValue ? vesselInfo.VesselInfo.LightWeight.Value : 0)*scrapPrice;
                                    }
                                }
                                totalScrappings = totalScrappings + Convert.ToDecimal(scrappings);
                            }

                            monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalScrappings);

                            currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                            currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                            if (periodTo < currentDateTo) currentDateTo = periodTo;
                        }
                        breakEvenResultsScrappings.Add("BreakEvenScrappings", monthsValues);

                    }
                }
            


            #endregion

            #region Valuation

            if (calculateValuation)
            {
                List<long> valuationRelatedCashFlowItems = cashFlowItems.Where(
                    f12 => f12.SystemType == CashFlowItemSystemTypeEnum.SpotRevenue
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueMinimum
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueOptional
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueFixedHireMinimum
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueFixedHireOptional
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueIndexLinkedHireMinimum
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueIndexLinkedHireOptional
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueProfitSharing
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsMinimum
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsOptional
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsFixedHireMinimum
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsFixedHireOptional
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireMinimum
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireOptional
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsProfitSharing
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FFAsSettlement
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FreightOptionsSettlement
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FreightOptionsPremium
                            || f12.SystemType == CashFlowItemSystemTypeEnum.TCCommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.CargoCommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FFACommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.OptionsCommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsOperationalExpenses
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsDrydockingExpenses
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsManagementFees
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsCommercialFees
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsScrappings
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsSales
                    ).Select(f12 => f12.Id).ToList();

                var monthsValues = new Dictionary<DateTime, decimal>();
                DateTime currentDateFrom = periodFrom;
                DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                while (currentDateFrom <= periodTo)
                {
                    decimal totalAmount = 0M;

                    foreach (long valuationRelatedCashFlowItem in valuationRelatedCashFlowItems)
                    {
                        if (cashFlowItemResults.ContainsKey(valuationRelatedCashFlowItem)) totalAmount = totalAmount + (cashFlowItemResults[valuationRelatedCashFlowItem][currentDateFrom]);
                    }

                    monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                    currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                    currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                    if (periodTo < currentDateTo) currentDateTo = periodTo;
                }
                valuationResults.Add("ValuationResults", monthsValues);

                monthsValues = new Dictionary<DateTime, decimal>();
                currentDateFrom = periodFrom;
                currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                int monthNo = 0;
                while (currentDateFrom <= periodTo)
                {
                    monthNo++;
                    decimal totalAmount = Convert.ToDecimal(1/Math.Pow((1 + Convert.ToDouble(valuationDiscountRate/100/12)), monthNo));

                    monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                    currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                    currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                    if (periodTo < currentDateTo) currentDateTo = periodTo;
                }
                valuationDiscountFactor.Add("ValuationDiscountFactor", monthsValues);

                monthsValues = new Dictionary<DateTime, decimal>();
                currentDateFrom = periodFrom;
                currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);

                while (currentDateFrom <= periodTo)
                {
                    decimal totalAmount = valuationResults["ValuationResults"][currentDateFrom]*valuationDiscountFactor["ValuationDiscountFactor"][currentDateFrom];

                    monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                    currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                    currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                    if (periodTo < currentDateTo) currentDateTo = periodTo;
                }
                valuationPresentValue.Add("ValuationPresentValue", monthsValues);
            }

            #endregion

            #region IRR

                if(calculateIRR)
                {
                    List<long> irrRelatedCashFlowItems = cashFlowItems.Where(
                    f12 => f12.SystemType == CashFlowItemSystemTypeEnum.SpotRevenue
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueMinimum
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueOptional
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueFixedHireMinimum
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueFixedHireOptional
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueIndexLinkedHireMinimum
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueIndexLinkedHireOptional
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageRevenueProfitSharing
                        || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsMinimum
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsOptional
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsFixedHireMinimum
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsFixedHireOptional
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireMinimum
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsIndexLinkedHireOptional
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VoyageCostsProfitSharing
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FFAsSettlement
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FreightOptionsSettlement
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FreightOptionsPremium
                            || f12.SystemType == CashFlowItemSystemTypeEnum.TCCommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.CargoCommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.FFACommissions
                            || f12.SystemType == CashFlowItemSystemTypeEnum.OptionsCommissions
                               || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsOperationalExpenses
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansCapitalRepayments
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansInterestExpenses
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansBaloonPayments
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansPrepayments
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansAdministrationFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansArrangementFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansPrepaymentFees
                               || f12.SystemType == CashFlowItemSystemTypeEnum.LoansCommitmentFees
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsDrydockingExpenses
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsManagementFees
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsCommercialFees
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsScrappings
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsSales
                            || f12.SystemType == CashFlowItemSystemTypeEnum.VesselsEquityInjection

                    ).Select(f12 => f12.Id).ToList();

                    var monthsValues = new Dictionary<DateTime, decimal>();
                    DateTime currentDateFrom = periodFrom;
                    DateTime currentDateTo = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1).AddDays(-1);
                    while (currentDateFrom <= periodTo)
                    {
                        decimal totalAmount = 0M;

                        foreach (long irrRelatedCashFlowItem in irrRelatedCashFlowItems)
                        {
                            if (cashFlowItemResults.ContainsKey(irrRelatedCashFlowItem)) totalAmount = totalAmount + (cashFlowItemResults[irrRelatedCashFlowItem][currentDateFrom]);
                        }

                        monthsValues.Add(new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1), totalAmount);

                        currentDateFrom = new DateTime(currentDateFrom.Year, currentDateFrom.Month, 1).AddMonths(1);
                        currentDateTo = currentDateFrom.AddMonths(1).AddDays(-1);
                        if (periodTo < currentDateTo) currentDateTo = periodTo;
                    }
                    irrResults.Add("IRRResults", monthsValues);
                }

            #endregion

            cashFlowModelGroups = (from objCashFlowModelGroup in dataContextRoot.CashFlowModelsGroups
                                       where objCashFlowModelGroup.ModelId == cashFlowModelId
                                       select objCashFlowModelGroup).ToList();

                cashFlowGroups = (from objCashFlowModelGroup in dataContextRoot.CashFlowModelsGroups
                                  from objCashFlowGroup in dataContextRoot.CashFlowGroups
                                  where objCashFlowModelGroup.ModelId == cashFlowModelId
                                        && objCashFlowModelGroup.GroupId == objCashFlowGroup.Id
                                  select objCashFlowGroup).ToList();

                cashFlowGroupItems = (from objCashFlowModelGroup in dataContextRoot.CashFlowModelsGroups
                                      from objCashFlowGroupItem in dataContextRoot.CashFlowGroupsItems
                                      where objCashFlowModelGroup.ModelId == cashFlowModelId
                                            && objCashFlowGroupItem.GroupId == objCashFlowModelGroup.GroupId
                                      select objCashFlowGroupItem).ToList();


                if (cashFlowItemsResults.Count > 0) cashFlowItemResults = cashFlowItemsResults.ToDictionary(dictionary => dictionary.First().Key, dictionary => dictionary.First().Value);
            }
            catch (AggregateException exc)
            {
                foreach (Exception exception in exc.InnerExceptions)
                {
                    HandleException(userName, exception);
                }
                return null;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                foreach (DataContext dataContext in dataContexts)
                {
                    if (dataContext != null)
                        dataContext.Dispose();
                }
            }


            return 0;
        }

        private bool IsOptionalTradeLegInTheMoney(TradeInformation tradeInfo, DateTime currentDateFrom, DateTime currentDateTo, TradeTypeEnum type, DateTime curveDate, bool useSpot, string userName, DataContext dataContext, string marketSensitivityType, decimal marketSensitivityValue)
        {
            if(type == TradeTypeEnum.TC)
            {
                decimal periodPrice;
                decimal periodMarketPrice;
                Dictionary<DateTime, decimal> marketPrices;
                Dictionary<DateTime, decimal> finalFFAValues;
                if (tradeInfo.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                {
                    if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                    {
                        periodPrice = Convert.ToDecimal(dataContext.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeTCInfoLeg.IndexId.Value && a100.Date >= currentDateFrom && a100.Date <= currentDateTo && a100.Date >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeTCInfoLeg.PeriodTo).Select(a100 => a100.Rate).Average());
                    }
                    else
                    {
                        marketPrices = CalculateForwardCurveValues(tradeInfo.TradeTCInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                        periodPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= currentDateFrom && a100.Key <= currentDateTo).Select(a100 => a100.Value).Average();
                    }
                    periodPrice = periodPrice * tradeInfo.TradeTCInfoLeg.IndexPercentage.Value/100;
                }
                else
                {
                    periodPrice = tradeInfo.TradeTCInfoLeg.Rate.Value;
                }
                periodPrice = Math.Round(periodPrice, 4);
                if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                {
                    periodMarketPrice = Convert.ToDecimal(dataContext.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeInfo.MTMFwdIndexId && a100.Date >= tradeInfo.TradeTCInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeTCInfoLeg.PeriodTo && a100.Date >= currentDateFrom && a100.Date <= currentDateTo).Select(a100 => a100.Rate).Average());
                }
                else
                {
                    if (marketSensitivityType.ToUpper() == "STRESS")
                    {
                        marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                    }
                    else
                    {
                        marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                    }
                    periodMarketPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= currentDateFrom && a100.Key <= currentDateTo).Select(a100 => a100.Value).Average();

                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                    {
                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                    }
                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                    {
                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                    }
                }
                periodMarketPrice = periodMarketPrice * tradeInfo.TradeTCInfo.VesselIndex / 100;
                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                if (periodMarketPrice == periodPrice && tradeInfo.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked) return true;
                if (periodMarketPrice == periodPrice && tradeInfo.TradeTCInfoLeg.RateType == TradeInfoLegRateTypeEnum.Fixed) return false;
                if (periodMarketPrice < periodPrice) return false;
                return true;
            }
            else
            {
                decimal periodPrice;
                decimal periodMarketPrice;
                Dictionary<DateTime, decimal> marketPrices;
                Dictionary<DateTime, decimal> finalFFAValues;
                if (tradeInfo.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked)
                {
                    if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                    {
                        periodPrice = Convert.ToDecimal(dataContext.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeCargoInfoLeg.IndexId.Value && a100.Date >= tradeInfo.TradeCargoInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeCargoInfoLeg.PeriodTo && a100.Date >= currentDateFrom && a100.Date <= currentDateTo).Select(a100 => a100.Rate).Average());
                    }
                    else
                    {
                        marketPrices = CalculateForwardCurveValues(tradeInfo.TradeCargoInfoLeg.IndexId.Value, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                        periodPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= currentDateFrom && a100.Key <= currentDateTo).Select(a100 => a100.Value).Average();
                    }
                    periodPrice = periodPrice * tradeInfo.TradeCargoInfoLeg.IndexPercentage.Value/100;
                }
                else
                {
                    periodPrice = tradeInfo.TradeCargoInfoLeg.Rate.Value;
                }
                periodPrice = Math.Round(periodPrice, 4);
                if ((currentDateTo <= curveDate.Date) || (currentDateTo > curveDate && currentDateTo < DateTime.Now.Date && useSpot))
                {
                    periodMarketPrice = Convert.ToDecimal(dataContext.IndexSpotValues.Where(a100 => a100.IndexId == tradeInfo.TradeInfo.MTMFwdIndexId && a100.Date >= tradeInfo.TradeCargoInfoLeg.PeriodFrom && a100.Date <= tradeInfo.TradeCargoInfoLeg.PeriodTo && a100.Date >= currentDateFrom && a100.Date <= currentDateTo).Select(a100 => a100.Rate).Average());
                }
                else
                {
                    if (marketSensitivityType.ToUpper() == "STRESS")
                    {
                        marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMStressIndexId, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                    }
                    else
                    {
                        marketPrices = CalculateForwardCurveValues(tradeInfo.TradeInfo.MTMFwdIndexId, curveDate, 0, 0, userName, dataContext, out finalFFAValues);
                    }
                    periodMarketPrice = marketPrices.Where(a100 => a100.Key >= currentDateFrom && a100.Key <= currentDateTo && a100.Key >= currentDateFrom && a100.Key <= currentDateTo).Select(a100 => a100.Value).Average();

                    if (marketSensitivityType.ToUpper() == "ABSOLUTE")
                    {
                        periodMarketPrice = periodMarketPrice + marketSensitivityValue;
                    }
                    else if (marketSensitivityType.ToUpper() == "PERCENTAGE")
                    {
                        periodMarketPrice = periodMarketPrice + (periodMarketPrice * marketSensitivityValue / 100);
                    }
                }

                periodMarketPrice = periodMarketPrice * tradeInfo.TradeCargoInfo.VesselIndex / 100;
                periodMarketPrice = Math.Round(periodMarketPrice, 4);
                if (periodMarketPrice == periodPrice && tradeInfo.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.IndexLinked) return true;
                if (periodMarketPrice == periodPrice && tradeInfo.TradeCargoInfoLeg.RateType == TradeInfoLegRateTypeEnum.Fixed) return false;
                if (periodMarketPrice < periodPrice) return false;
                return true;
            }
        }
    }

    

}