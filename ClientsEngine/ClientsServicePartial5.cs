﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using Exis.Domain;
using Exis.WCFExtensions;
using Extreme.Mathematics.Curves;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;
using System.Data.Common;
using System.Globalization;

namespace Exis.ClientsEngine
{
    public partial class ClientsService
    {
        public int? EnergyGenerateMarkToMarket(DateTime curveDate, DateTime periodFrom, DateTime periodTo, out Dictionary<DateTime, decimal> results, out string errorMessage)
        {
            string userName = GenericContext<ProgramInfo>.Current.Value.UserName;

            errorMessage = "";
            results = null;

            var dataContexts = new List<DataContext>();

            try
            {
                var dataContextRoot = new DataContext(DBConnectionString) {ObjectTrackingEnabled = false};

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null)
                        SiAuto.Si.AddSession(userName, true);
                    dataContextRoot.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                                              {TitleLimit = 0, TitlePrefix = "GenerateMarkToMarket: "};
                }

                var monthResults = new Dictionary<DateTime, decimal>();

                DateTime currentDateFrom =
                    new DateTime(periodFrom.Year, periodFrom.Month, periodFrom.Day, 13, 0, 0).AddHours(-1);
                    //21/6/2013 12
                DateTime currentDateTo =
                    new DateTime(currentDateFrom.Year, currentDateFrom.Month, currentDateFrom.Day, 0, 0, 0).AddDays(1).
                        AddHours(-1); //22/6/2013 23

                if (periodTo < currentDateTo) currentDateTo = periodTo;

                Dictionary<DateTime, decimal> finalFFAValues = new Dictionary<DateTime, decimal>();
                Dictionary<DateTime, decimal> marketPrices = new Dictionary<DateTime, decimal>();

                marketPrices = CalculateForwardCurveValues(curveDate, userName, dataContextRoot,
                                                                   out finalFFAValues);
                /*
                while (currentDateFrom <= periodTo)
                {
                    decimal positionHours;
                    decimal periodMarketPrice = 0;
                    decimal periodPrice = 0;
                    decimal cash = 0;

                    positionHours = (decimal) (new TimeSpan(
                                                  Math.Max(
                                                      currentDateTo.Ticks -
                                                      currentDateFrom.Ticks +
                                                      TimeSpan.TicksPerDay, 0))).Hours;


                    if ((currentDateTo <= curveDate.Date) ||
                        (currentDateTo > curveDate && currentDateTo < new DateTime(2012, 4, 1)))
                    {
                        periodMarketPrice =
                            Convert.ToDecimal(
                                dataContextRoot.TestEnergySpotValues.Where(
                                    a =>
                                    a.PriceDate.Date >= currentDateFrom.Date &&
                                    a.PriceDate.Date <= currentDateTo.Date && a.Hour == curveDate.Hour).Select(
                                        a => a.Price).Average());
                    }
                    else
                    {
                        marketPrices = CalculateForwardCurveValues(curveDate, userName, dataContextRoot,
                                                                   out finalFFAValues);
                        periodMarketPrice =
                            marketPrices.Where(
                                a =>
                                a.Key >= currentDateFrom && a.Key <= currentDateTo).Select(a => a.Value).Average();
                    }

                    //periodMarketPrice = Math.Round(periodMarketPrice, 4);
                    //cash = positionHours*periodMarketPrice;

                    //monthResults.Add(
                    //    new DateTime(currentDateFrom.Year, currentDateFrom.Month, currentDateFrom.Day, 0, 0, 0), cash);

                    //currentDateFrom =
                    //    new DateTime(currentDateFrom.Year, currentDateFrom.Month, currentDateFrom.Day, 0, 0, 0).AddDays(
                    //        1);
                    //currentDateTo = currentDateFrom.AddDays(1).AddHours(-1);
                    //if (periodTo < currentDateTo) currentDateTo = periodTo;
                }
                 * */

                //results = monthResults;
                results = marketPrices;

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                errorMessage = exc.Message;
                return null;
            }
            finally
            {
                foreach (DataContext context in dataContexts)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
        }

        private Dictionary<DateTime, decimal> CalculateForwardCurveValues(DateTime positionDate, string userName, DataContext dataContext, out Dictionary<DateTime, decimal> finalIndexValues)
        {

            List<TestEnergyForwardValue> indexFFAValues = dataContext.TestEnergyForwardValues.Where(a=>a.Hour == 1 && a.PriceDate < new DateTime(2011, 8, 1)).ToList();

            if (indexFFAValues == null || indexFFAValues.Count == 0)
                throw new ApplicationException("Failed to find FFA Index values for Date : " + positionDate.Date.ToShortDateString());

            DateTime firstHour = new DateTime(positionDate.Year, positionDate.Month, positionDate.Day, 1, 0, 0);

            DateTime periodFrom = firstHour;
            DateTime periodTo = indexFFAValues.Select(a => a.PriceDate).Max();


            TestEnergySpotValue indexSpotValueForDayBeforeStartDate = (from objIndexSpotValue in dataContext.TestEnergySpotValues
                                                                  where objIndexSpotValue.PriceDate <= periodFrom.AddHours(-1)
                                                                  select objIndexSpotValue).OrderByDescending(a => a.PriceDate).FirstOrDefault();

            if (indexSpotValueForDayBeforeStartDate == null) throw new ApplicationException("Failed to find Spot Index Value for Date: " + periodFrom.AddHours(-1).ToString("d"));

            var finalFFAValuesDict = new Dictionary<DateTime, decimal>();

            var currentDate = new DateTime(positionDate.Year, positionDate.Month, positionDate.Day, 1, 0, 0);
            while (currentDate <= periodTo)
            {
                TestEnergyForwardValue indexFFAValue =
                    indexFFAValues.SingleOrDefault(
                        a =>
                        a.PriceDate.Year == currentDate.Year && a.PriceDate.Month == currentDate.Month && a.PriceDate.Day == currentDate.Day &&
                        a.Hour == currentDate.Hour);

                if (indexFFAValue != null)
                {
                    finalFFAValuesDict.Add(new DateTime(indexFFAValue.PriceDate.Year, indexFFAValue.PriceDate.Month, indexFFAValue.PriceDate.Day, 1, 0, 0), Convert.ToDecimal(indexFFAValue.Price));
                    currentDate = currentDate.AddDays(1);
                    continue;
                }
                currentDate = currentDate.AddDays(1);
            }

            var zeroRatesValuesDict = new Dictionary<DateTime, decimal>();

            DateTime currentStartDate = periodFrom.AddDays(-1);
            DateTime currentFromDate = currentStartDate;
            decimal currentFromRateSum = 0;

            //zeroRatesValuesDict.Add(indexSpotValueForDayBeforeStartDate.PriceDate, Convert.ToDecimal(indexSpotValueForDayBeforeStartDate.Price));

            foreach (var keyValuePair in finalFFAValuesDict)
            {
                DateTime currentToDate = keyValuePair.Key;
                decimal rate = keyValuePair.Value;
                
                //TODO was days
                decimal currentToRateSum = Convert.ToDecimal((currentToDate - currentFromDate).TotalHours) * rate;

                decimal totalhours = Convert.ToDecimal((currentToDate - currentStartDate).TotalHours);

                zeroRatesValuesDict.Add(currentToDate, (currentFromRateSum + currentToRateSum) / totalhours);

                currentFromDate = currentToDate;
                currentFromRateSum = currentFromRateSum + currentToRateSum;
            }

            double[] oldXValues = zeroRatesValuesDict.Keys.Select(a => Convert.ToDouble(a.Ticks)).ToArray();
            double[] oldYValues = zeroRatesValuesDict.Values.Select(a => Convert.ToDouble(a)).ToArray();

            CubicSpline naturalCubicSpline;
            try
            {
                naturalCubicSpline = new CubicSpline(oldXValues, oldYValues);
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                throw exc;
            }

            var cubicSplineValuesDict = new Dictionary<DateTime, decimal>();

            currentDate = new DateTime(periodFrom.Year, periodFrom.Month, periodFrom.Day, 1, 0, 0);
            while (currentDate <= periodTo)
            {
                cubicSplineValuesDict.Add(new DateTime(currentDate.Ticks), Convert.ToDecimal(naturalCubicSpline.ValueAt(currentDate.Ticks)));
                //currentDate = currentDate.AddHours(1);
                currentDate = currentDate.AddDays(1);
            }


            var forwardCurveValuesDict = new Dictionary<DateTime, decimal>();

            List<decimal> rates = cubicSplineValuesDict.OrderBy(a => a.Key).Select(a => a.Value).ToList();
            List<DateTime> dates = cubicSplineValuesDict.OrderBy(a => a.Key).Select(a => a.Key).ToList();

            for (int i = rates.Count() - 1; i >= 1; i--)
            {
                DateTime currentDay = dates[i];
                decimal currentZeroRate = rates[i];
                decimal previousZeroRate = rates[i - 1];

                var currentPeriodDays = (int)((currentDay - periodFrom).Hours + 1);
                int previousPeriodDays = currentPeriodDays - 1;

                decimal currentRate = (currentZeroRate * currentPeriodDays) - (previousZeroRate * previousPeriodDays);
                forwardCurveValuesDict.Add(currentDay, currentRate);
            }

            forwardCurveValuesDict.Add(dates[0], rates[0]);
            forwardCurveValuesDict = forwardCurveValuesDict.OrderBy(a => a.Key).ToDictionary(a => a.Key, a => a.Value);

            finalIndexValues = finalFFAValuesDict;
            return forwardCurveValuesDict;
        }
    }
}
