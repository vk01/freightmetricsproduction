﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using Devart.Data.Linq;
using Exis.Domain;
using Exis.ServiceInterfaces;
using Exis.WCFExtensions;
using DataContext = Exis.Domain.DataContext;
using System.Globalization;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;

namespace Exis.ClientsEngine
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any, MaxItemsInObjectGraph = Int32.MaxValue,
        InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple,
        IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    [ErrorHandlerBehavior]
    [MessageInspectorBehavior]
    public partial class ClientsService : IClientsService
    {
        private readonly object smtpClientSynchObject = new object();
        public string DBConnectionString;

        public string exisIssueTrackerEmailAddress;
        public string exisSupportEmailAddress;
        public string installationEmailAddress;

        public SmtpClient smtpClient;
        public object importInProgressLock = new object();
        public bool importInProgress;

        public class FileName : IComparable<FileName>
        {
            public string fName { get; set; }
            public int CompareTo(FileName other)
            {
                return fName.CompareTo(other.fName);
            }
        }

          
        #region Administration

        public int? AEVVesselInitializationData(out List<Company> companies, out List<Market> markets, out List<Contact> contacts)
        {
            companies = null;
            markets = null;
            contacts = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVVesselInitializationData: " };
                }

                companies = Queries.GetCompaniesByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                markets = Queries.GetMarketSegments(dataContext).ToList();
                contacts = Queries.GetContactsAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVIndexInitializationData(long? indexId, out List<Market> markets, out List<Index> indexes, out List<IndexCustomValue> customIndexValues, out List<AverageIndexesAssoc> averageIndexesAssocs)
        {
            markets = null;
            indexes = null;
            customIndexValues = null;
            averageIndexesAssocs = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVIndexInitializationData: " };
                }

                markets = Queries.GetMarketSegments(dataContext).ToList();
                indexes = Queries.GetIndexesByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                if (indexId != null)
                {
                    Index selectedIndex = dataContext.Indexes.Where(a => a.Id == indexId).Single();
                    if (selectedIndex.IsCustom)
                        customIndexValues = Queries.GetCustomIndexValuesByIndex(dataContext, indexId.Value).ToList();
                    //if (selectedIndex.IsMarketDefault)
                    //    averageIndexesAssocs =
                    //        dataContext.AverageIndexesAssocs.Where(a => a.AvgIndexId == indexId).ToList();
                    if (selectedIndex.IsAssocIndex)
                        averageIndexesAssocs =
                            dataContext.AverageIndexesAssocs.Where(a => a.AvgIndexId == indexId).ToList();
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVCompanyInitializationData(out List<Company> companies, out List<CompanySubtype> companySubtypes, out List<Contact> managers)
        {
            companies = null;
            companySubtypes = null;
            managers = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "AEVCompanyInitializationData: " };
                }

                companies = Queries.GetCompaniesAll(dataContext).ToList();
                managers =
                    Queries.GetContactsByTypeStatus(dataContext, ActivationStatusEnum.Active,
                                                    ContactTypeEnum.BankRelationshipManager).ToList();
                companySubtypes =
                    Queries.GetCompanySubtypesByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVTraderInitializationData(out List<Company> companies, out List<Market> markets, out List<ProfitCentre> profitCentres, out List<Desk> desks, out List<Book> books)
        {
            companies = null;
            markets = null;
            profitCentres = null;
            desks = null;
            books = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "AEVTraderInitializationData: " };
                }

                companies =
                    Queries.GetCompaniesByNotTypeStatus(dataContext, CompanyTypeEnum.External, ActivationStatusEnum.Active).ToList();
                markets = Queries.GetMarketSegments(dataContext).ToList();
                profitCentres = Queries.GetProfitCentresByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                desks = Queries.GetDesksByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                books = Queries.GetBooksByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVBookInitializationData(out List<Book> books)
        {
            books = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "EndAEVBookInitializationData: " };
                }

                books = Queries.GetBooksAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVBankAccountInitializationData(out List<Company> companies, out List<Company> banks, out List<Currency> currencies)
        {
            companies = null;
            banks = null;
            currencies = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVBankAccountInitializationData: " };
                }

                companies =
                    Queries.GetCompaniesByTypeStatus(dataContext, CompanyTypeEnum.Internal,
                                                            ActivationStatusEnum.Active).ToList();
                banks = Queries.GetCompaniesBySubtypeStatus(dataContext, CompanySubtypeEnum.Bank,
                                                            ActivationStatusEnum.Active).ToList();
                currencies = Queries.GetCurrenciesAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVVesselPoolInitializationData(out List<Vessel> vessels)
        {
            vessels = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVVesselPoolInitializationData: " };
                }

                vessels =
                    Queries.GetVesselsNotInPoolByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVUserInitializationData(out List<Trader> traders, out List<Book> books)
        {
            traders = null;
            books = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVUserInitializationData: " };
                }

                traders =
                    Queries.GetTradersByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                books = Queries.GetBooksByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVLoanInitializationData(out List<Company> banks, out List<Vessel> vessels, out List<Company> obligors, out List<Company> guarantors,
            out List<Currency> currencies, out List<Company> borrowers)
        {
            banks = null;
            vessels = null;
            obligors = null;
            guarantors = null;
            currencies = null;
            borrowers = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVLoanInitializationData: " };
                }

                banks = Queries.GetCompaniesBySubtypeStatus(dataContext, CompanySubtypeEnum.Bank, ActivationStatusEnum.Active).ToList();
                vessels = Queries.GetVesselsByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                obligors = Queries.GetCompaniesAll(dataContext).ToList();
                guarantors = Queries.GetCompaniesAll(dataContext).ToList();
                currencies = Queries.GetCurrenciesAll(dataContext).ToList();
                borrowers = Queries.GetCompaniesAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVVesselPoolHistory(long vesselPoolId, out List<VesselPoolInfo> vesselPoolInfos)
        {
            vesselPoolInfos = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVVesselPoolHistory: " };
                }

                List<VesselPoolInformation> vesselPoolInformations = Queries.GetVesselPoolInfosHistoryByVessel(dataContext, vesselPoolId).ToList();

                vesselPoolInfos = vesselPoolInformations.Select(a => a.VesselPoolInfo).Distinct().ToList();

                foreach (VesselPoolInfo vesselPoolInfo in vesselPoolInfos)
                {
                    vesselPoolInfo.Vessels =
                        vesselPoolInformations.Where(a => a.VesselPoolInfo.Id == vesselPoolInfo.Id).Select(a => a.Vessel)
                            .ToList();
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEEntity(bool isEdit, DomainObject entity)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEEntity." + entity.GetType().Name + ": " };
                }

                DateTime now = DateTime.Now;

                #region Company

                if (entity.GetType() == typeof(Company))
                {
                    var company = entity as Company;

                    bool isNameUnique = (from objCompany in dataContext.Companies
                                         where objCompany.Id != company.Id
                                               && objCompany.Name.ToUpper() == company.Name.ToUpper()
                                         select objCompany).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (company.SubtypeId == 0)
                    {
                        var companySubtype = new CompanySubtype
                        {
                            Id = dataContext.GetNextId(typeof(CompanySubtype)),
                            Name = company.Subtype.Name,
                            Subtype = CompanySubtypeEnum.Other,
                            Status = ActivationStatusEnum.Active,
                            Cruser = userName,
                            Chuser = userName,
                            Crd = now,
                            Chd = now
                        };
                        company.SubtypeId = companySubtype.Id;
                        dataContext.CompanySubtypes.InsertOnSubmit(companySubtype);
                    }

                    if (!isEdit)
                    {
                        company.Id = dataContext.GetNextId(typeof(Company));
                        company.Cruser = userName;
                        company.Chuser = userName;
                        company.Crd = now;
                        company.Chd = now;

                        dataContext.Companies.InsertOnSubmit(company);
                    }
                    else
                    {
                        bool companyOwnerOfBankAccount = dataContext.Accounts.Where(a => a.CompanyId == company.Id).Any();
                        bool companyBankOfBankAccount = dataContext.Accounts.Where(a => a.BankId == company.Id).Any();
                        bool companyInTrader =
                            dataContext.TraderCompanyAssocs.Where(a => a.CompanyId == company.Id).Any();

                        CompanySubtypeEnum subType =
                            dataContext.CompanySubtypes.Where(a => a.Id == company.SubtypeId).Select(a => a.Subtype).
                                Single();

                        if (companyOwnerOfBankAccount && company.Type != CompanyTypeEnum.Internal)
                            return 2;
                        if (companyBankOfBankAccount && subType != CompanySubtypeEnum.Bank)
                            return 3;
                        if (companyInTrader && company.Type == CompanyTypeEnum.External)
                            return 4;

                        company.Chuser = userName;
                        company.Chd = now;

                        dataContext.Companies.Attach(company, true);
                    }
                }
                #endregion

                #region Trader

                else if (entity.GetType() == typeof(Trader))
                {
                    var trader = entity as Trader;

                    bool isNameUnique = (from objTrader in dataContext.Traders
                                         where objTrader.Id != trader.Id
                                               && objTrader.Name.ToUpper() == trader.Name.ToUpper()
                                         select objTrader).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        trader.Id = dataContext.GetNextId(typeof(Trader));
                        trader.Cruser = userName;
                        trader.Chuser = userName;
                        trader.Crd = now;
                        trader.Chd = now;

                        dataContext.Traders.InsertOnSubmit(trader);
                    }
                    else
                    {
                        trader.Chuser = userName;
                        trader.Chd = now;

                        dataContext.Traders.Attach(trader, true);

                        List<TraderMarketAssoc> traderMarkets = Queries.GetTraderMarketsByTrader(dataContext, trader.Id).ToList();
                        List<TraderCompanyAssoc> traderCompanies = Queries.GetTraderCompaniesByTrader(dataContext, trader.Id).ToList();
                        List<TraderBook> traderBooks = Queries.GetTraderBooksByTrader(dataContext, trader.Id).ToList();

                        foreach (TraderMarketAssoc traderMarket in traderMarkets)
                        {
                            dataContext.TraderMarketAssocs.DeleteOnSubmit(traderMarket);
                        }

                        foreach (TraderCompanyAssoc traderCompany in traderCompanies)
                        {
                            dataContext.TraderCompanyAssocs.DeleteOnSubmit(traderCompany);
                        }

                        foreach (TraderBook traderBook in traderBooks)
                        {
                            dataContext.TraderBooks.DeleteOnSubmit(traderBook);
                        }
                    }


                    foreach (Market market in trader.Markets)
                    {
                        dataContext.TraderMarketAssocs.InsertOnSubmit(new TraderMarketAssoc
                        {
                            Id =
                                                                                  dataContext.GetNextId(
                                                                                      typeof(TraderMarketAssoc)),
                            MarketId = market.Id,
                            TraderId = trader.Id
                        });
                    }

                    foreach (Company company in trader.Companies)
                    {
                        dataContext.TraderCompanyAssocs.InsertOnSubmit(new TraderCompanyAssoc
                        {
                            Id =
                                                                                   dataContext.GetNextId(
                                                                                       typeof(TraderCompanyAssoc)),
                            CompanyId = company.Id,
                            TraderId = trader.Id
                        });
                    }

                    foreach (CustomBook book in trader.Books)
                    {
                        dataContext.TraderBooks.InsertOnSubmit(new TraderBook()
                        {
                            Id =
                                                                           dataContext.GetNextId(
                                                                               typeof(TraderBook)),
                            BookId = book.Id,
                            TraderId = trader.Id
                        });
                    }
                }
                #endregion

                #region Vessel

                else if (entity.GetType() == typeof(Vessel))
                {
                    var vessel = entity as Vessel;

                    bool isNameUnique = (from objVessel in dataContext.Vessels
                                         where objVessel.Id != vessel.Id
                                               && objVessel.Name.ToUpper() == vessel.Name.ToUpper()
                                         select objVessel).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        vessel.Id = dataContext.GetNextId(typeof(Vessel));
                        vessel.Cruser = userName;
                        vessel.Chuser = userName;
                        vessel.Crd = now;
                        vessel.Chd = now;

                        vessel.Info.Id = dataContext.GetNextId(typeof(VesselInfo));
                        vessel.Info.VesselId = vessel.Id;

                        dataContext.Vessels.InsertOnSubmit(vessel);
                        dataContext.VesselInfos.InsertOnSubmit(vessel.Info);

                        var vmp = new VesselMarketPrice()
                        {
                            Id = dataContext.GetNextId(typeof(VesselMarketPrice)),
                            VesselId = vessel.Id,
                            MarketPrice = vessel.MarketPrice,
                            DateFrom = now,
                            DateTo = null,
                            Cruser = userName,
                            Crd = now,
                            Chuser = userName,
                            Chd = now
                        };
                        dataContext.VesselMarketPrices.InsertOnSubmit(vmp);

                        foreach (VesselExpense vesselExpense in vessel.VesselExpenses)
                        {
                            vesselExpense.Id = dataContext.GetNextId(typeof(VesselExpense));
                            vesselExpense.VesselId = vessel.Id;

                            dataContext.VesselExpenses.InsertOnSubmit(vesselExpense);
                        }
                        foreach (VesselBenchmarking vesselBenchmarking in vessel.VesselBenchmarkings)
                        {
                            vesselBenchmarking.Id = dataContext.GetNextId(typeof(VesselBenchmarking));
                            vesselBenchmarking.VesselId = vessel.Id;

                            dataContext.VesselBenchmarkings.InsertOnSubmit(vesselBenchmarking);
                        }
                    }
                    else
                    {
                        vessel.Chuser = userName;
                        vessel.Chd = now;

                        dataContext.Vessels.Attach(vessel, true);
                        dataContext.VesselInfos.Attach(vessel.Info, true);

                        VesselMarketPrice oldVmp =
                            dataContext.VesselMarketPrices.Where(a => a.VesselId == vessel.Id && a.DateTo == null).
                                SingleOrDefault();

                        if (oldVmp != null)
                            oldVmp.DateTo = now;

                        var vmp = new VesselMarketPrice()
                        {
                            Id = dataContext.GetNextId(typeof(VesselMarketPrice)),
                            VesselId = vessel.Id,
                            MarketPrice = vessel.MarketPrice,
                            DateFrom = now,
                            DateTo = null,
                            Cruser = userName,
                            Crd = now,
                            Chuser = userName,
                            Chd = now
                        };
                        dataContext.VesselMarketPrices.InsertOnSubmit(vmp);

                        List<VesselExpense> existingVesselExpenses =
                            dataContext.VesselExpenses.Where(a => a.VesselId == vessel.Id).ToList();
                        dataContext.VesselExpenses.DeleteAllOnSubmit(existingVesselExpenses);

                        foreach (VesselExpense vesselExpense in vessel.VesselExpenses)
                        {
                            vesselExpense.Id = dataContext.GetNextId(typeof(VesselExpense));
                            vesselExpense.VesselId = vessel.Id;

                            dataContext.VesselExpenses.InsertOnSubmit(vesselExpense);
                        }

                        List<VesselBenchmarking> existingVesselBenchmarkings =
                            dataContext.VesselBenchmarkings.Where(a => a.VesselId == vessel.Id).ToList();
                        dataContext.VesselBenchmarkings.DeleteAllOnSubmit(existingVesselBenchmarkings);

                        foreach (VesselBenchmarking vesselBenchmarking in vessel.VesselBenchmarkings)
                        {
                            vesselBenchmarking.Id = dataContext.GetNextId(typeof(VesselBenchmarking));
                            vesselBenchmarking.VesselId = vessel.Id;
                            dataContext.VesselBenchmarkings.InsertOnSubmit(vesselBenchmarking);
                        }
                    }
                }
                #endregion

                #region Book

                else if (entity.GetType() == typeof(Book))
                {
                    var book = entity as Book;

                    bool isNameUnique = (from objBook in dataContext.Books
                                         where objBook.Id != book.Id
                                               && objBook.Name.ToUpper() == book.Name.ToUpper()
                                         select objBook).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        book.Id = dataContext.GetNextId(typeof(Book));
                        book.Cruser = userName;
                        book.Chuser = userName;
                        book.Crd = now;
                        book.Chd = now;

                        if (book.ParentBookId != null)
                        {
                            string sql = @"select POS_CALC_BY_DAYS  from(select t.id, T.PARENT_BOOK_ID, T.POS_CALC_BY_DAYS from books t connect by t.id = prior t.parent_book_id start with t.id = " +
                                         book.ParentBookId + @") where PARENT_BOOK_ID is null";

                            long posCalcByDays = dataContext.ExecuteQuery<long>(sql).FirstOrDefault();
                            book.PositionCalculatedByDays = (BookPositionCalculatedByDaysEnum)posCalcByDays;

                        }

                            dataContext.Books.InsertOnSubmit(book);
                    }
                    else
                    {
                        Book existingBook = dataContext.Books.Where(a => a.Id == book.Id).Single();
                        long? existingBookParentId = existingBook.ParentBookId;
                        var prevStatus = existingBook.Status;
                        var prevPositionCalculatedByDays = existingBook.PositionCalculatedByDays;

                        existingBook.Name = book.Name;
                        existingBook.ParentBookId = book.ParentBookId;
                        existingBook.Status = book.Status;
                        existingBook.PositionCalculatedByDays = book.PositionCalculatedByDays;
                        existingBook.Chuser = userName;
                        existingBook.Chd = now;

                        //If book status has changed, then apply new status to all sub-books
                        if (prevStatus != book.Status)
                        {
                            string sqlChildrenBooks =
                                "select t.id from books t connect by t.parent_book_id =  prior t.id start with t.id = " +
                                book.Id;

                            List<long> childrenBookIds = dataContext.Query<long>(sqlChildrenBooks).ToList();
                            List<Book> childrenBooks =
                                dataContext.Books.Where(a => childrenBookIds.Contains(a.Id)).ToList();
                            foreach (var childBook in childrenBooks)
                            {
                                childBook.Status = book.Status;
                            }
                        }


                        //If book PositionCalculatedByDays has changed, then apply new PositionCalculatedByDays to all sub-books
                        if (prevPositionCalculatedByDays != book.PositionCalculatedByDays)
                        {
                            string sqlChildrenBooks =
                                "select t.id from books t connect by t.parent_book_id =  prior t.id start with t.id = " +
                                book.Id;

                            List<long> childrenBookIds = dataContext.Query<long>(sqlChildrenBooks).ToList();
                            List<Book> childrenBooks =
                                dataContext.Books.Where(a => childrenBookIds.Contains(a.Id)).ToList();
                            foreach (var childBook in childrenBooks)
                            {
                                childBook.PositionCalculatedByDays = book.PositionCalculatedByDays;
                            }
                        }

                        //If parent changed, then update all trader-books associations
                        if (book.ParentBookId != null && existingBookParentId != book.ParentBookId)
                        {
                            string sql = @"select POS_CALC_BY_DAYS  from(select t.id, T.PARENT_BOOK_ID, T.POS_CALC_BY_DAYS from books t connect by t.id = prior t.parent_book_id start with t.id = " +
                                         book.ParentBookId + @") where PARENT_BOOK_ID is null";

                            long posCalcByDays = dataContext.ExecuteQuery<long>(sql).FirstOrDefault();
                            existingBook.PositionCalculatedByDays = (BookPositionCalculatedByDaysEnum)posCalcByDays;

                            sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                         book.ParentBookId;

                            List<long> parentBookIds = dataContext.Query<long>(sql).ToList();

                            List<long> traderIdsWithUpdatedBook =
                                dataContext.TraderBooks.Where(a => a.BookId == book.Id).Select(a => a.TraderId).ToList();

                            TraderBook traderBook;
                            foreach (long traderId in traderIdsWithUpdatedBook)
                            {
                                List<long> traderBookIds =
                                    dataContext.TraderBooks.Where(a => a.TraderId == traderId).Select(a => a.BookId).
                                        ToList();

                                foreach (long parentBookId in parentBookIds)
                                {
                                    if (!traderBookIds.Contains(parentBookId))
                                    {
                                        traderBook = new TraderBook()
                                        {
                                            BookId = parentBookId,
                                            Id = dataContext.GetNextId(typeof(TraderBook)),
                                            TraderId = traderId
                                        };
                                        dataContext.TraderBooks.InsertOnSubmit(traderBook);
                                    }
                                }
                            }

                            List<long> traderInfoIdsWithUpdatedBook =
                                (from objTradeInfoBook in dataContext.TradeInfoBooks
                                 from objTradeInfo in dataContext.TradeInfos
                                 where objTradeInfoBook.BookId == book.Id &&
                                       objTradeInfoBook.TradeInfoId == objTradeInfo.Id &&
                                       objTradeInfo.DateTo == null
                                 select objTradeInfo.Id).ToList();

                            foreach (long tradeInfoId in traderInfoIdsWithUpdatedBook)
                            {
                                List<long> tradeInfoBookIds =
                                    dataContext.TradeInfoBooks.Where(a => a.TradeInfoId == tradeInfoId).Select(
                                        a => a.BookId).ToList();

                                foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                                {
                                    if (!tradeInfoBookIds.Contains(parentBookId))
                                    {
                                        var newTradeInfoBook = new TradeInfoBook()
                                        {
                                            BookId = parentBookId,
                                            Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                            TradeInfoId = tradeInfoId
                                        };
                                        dataContext.TradeInfoBooks.InsertOnSubmit(newTradeInfoBook);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Account

                else if (entity.GetType() == typeof(Account))
                {
                    var bankAccount = entity as Account;

                    bool isNameUnique = (from objBankAccount in dataContext.Accounts
                                         where objBankAccount.Id != bankAccount.Id
                                               && objBankAccount.Name.ToUpper() == bankAccount.Name.ToUpper()
                                         select objBankAccount).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        bankAccount.Id = dataContext.GetNextId(typeof(Account));
                        bankAccount.Cruser = userName;
                        bankAccount.Chuser = userName;
                        bankAccount.Crd = now;
                        bankAccount.Chd = now;

                        dataContext.Accounts.InsertOnSubmit(bankAccount);
                    }
                    else
                    {
                        bankAccount.Chuser = userName;
                        bankAccount.Chd = now;

                        dataContext.Accounts.Attach(bankAccount, true);
                    }
                }
                #endregion

                #region ProfitCentre

                else if (entity.GetType() == typeof(ProfitCentre))
                {
                    var profitCentre = entity as ProfitCentre;

                    bool isNameUnique = (from objProfitCentre in dataContext.ProfitCentres
                                         where objProfitCentre.Id != profitCentre.Id
                                               && objProfitCentre.Name.ToUpper() == profitCentre.Name.ToUpper()
                                         select objProfitCentre).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        profitCentre.Id = dataContext.GetNextId(typeof(ProfitCentre));
                        profitCentre.Cruser = userName;
                        profitCentre.Chuser = userName;
                        profitCentre.Crd = now;
                        profitCentre.Chd = now;

                        dataContext.ProfitCentres.InsertOnSubmit(profitCentre);
                    }
                    else
                    {
                        profitCentre.Chuser = userName;
                        profitCentre.Chd = now;

                        dataContext.ProfitCentres.Attach(profitCentre, true);
                    }
                }

                #endregion

                #region Desk

                else if (entity.GetType() == typeof(Desk))
                {
                    var desk = entity as Desk;

                    bool isNameUnique = (from objDesk in dataContext.Desks
                                         where objDesk.Id != desk.Id
                                               && objDesk.Name.ToUpper() == desk.Name.ToUpper()
                                         select objDesk).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        desk.Id = dataContext.GetNextId(typeof(Desk));
                        desk.Cruser = userName;
                        desk.Chuser = userName;
                        desk.Crd = now;
                        desk.Chd = now;

                        dataContext.Desks.InsertOnSubmit(desk);
                    }
                    else
                    {
                        desk.Chuser = userName;
                        desk.Chd = now;

                        dataContext.Desks.Attach(desk, true);
                    }
                }
                #endregion

                #region Clearing House

                else if (entity.GetType() == typeof(ClearingHouse))
                {
                    var clearingHouse = entity as ClearingHouse;

                    bool isNameUnique = (from objClearingHouse in dataContext.Clearinghouses
                                         where objClearingHouse.Id != clearingHouse.Id
                                               && objClearingHouse.Name.ToUpper() == clearingHouse.Name.ToUpper()
                                         select objClearingHouse).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        clearingHouse.Id = dataContext.GetNextId(typeof(ClearingHouse));
                        clearingHouse.Cruser = userName;
                        clearingHouse.Chuser = userName;
                        clearingHouse.Crd = now;
                        clearingHouse.Chd = now;

                        dataContext.Clearinghouses.InsertOnSubmit(clearingHouse);
                    }
                    else
                    {
                        clearingHouse.Chuser = userName;
                        clearingHouse.Chd = now;

                        dataContext.Clearinghouses.Attach(clearingHouse, true);
                    }
                }
                #endregion

                #region CompanySubType

                else if (entity.GetType() == typeof(CompanySubtype))
                {
                    var companySubtype = entity as CompanySubtype;

                    bool isNameUnique = (from objCompanySubtype in dataContext.CompanySubtypes
                                         where objCompanySubtype.Id != companySubtype.Id
                                               && objCompanySubtype.Name.ToUpper() == companySubtype.Name.ToUpper()
                                         select objCompanySubtype).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        companySubtype.Id = dataContext.GetNextId(typeof(CompanySubtype));
                        companySubtype.Cruser = userName;
                        companySubtype.Chuser = userName;
                        companySubtype.Crd = now;
                        companySubtype.Chd = now;

                        dataContext.CompanySubtypes.InsertOnSubmit(companySubtype);
                    }
                    else
                    {
                        companySubtype.Chuser = userName;
                        companySubtype.Chd = now;

                        dataContext.CompanySubtypes.Attach(companySubtype, true);
                    }
                }
                #endregion

                #region Vessel Pool
                else if (entity.GetType() == typeof(VesselPool))
                {
                    var vesselPool = entity as VesselPool;

                    bool isNameUnique = (from objVesselPool in dataContext.VesselPools
                                         where objVesselPool.Id != vesselPool.Id
                                               && objVesselPool.Name.ToUpper() == vesselPool.Name.ToUpper()
                                         select objVesselPool).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        vesselPool.Id = dataContext.GetNextId(typeof(VesselPool));
                        vesselPool.Cruser = userName;
                        vesselPool.Chuser = userName;
                        vesselPool.Crd = now;
                        vesselPool.Chd = now;

                        var vesselPoolInfo = new VesselPoolInfo
                        {
                            Id = dataContext.GetNextId(typeof(VesselPoolInfo)),
                            VesselPoolId = vesselPool.Id,
                            PeriodFrom = now,
                            Cruser = userName,
                            Chuser = userName,
                            Crd = now,
                            Chd = now
                        };

                        dataContext.VesselPools.InsertOnSubmit(vesselPool);
                        dataContext.VesselPoolInfos.InsertOnSubmit(vesselPoolInfo);

                        foreach (Vessel vessel in vesselPool.CurrentVessels)
                        {
                            var poolInfoVessel = new PoolInfoVessel
                            {
                                Id = dataContext.GetNextId(typeof(PoolInfoVessel)),
                                VesselPoolInfoId = vesselPoolInfo.Id,
                                VesselId = vessel.Id
                            };
                            dataContext.PoolInfoVessels.InsertOnSubmit(poolInfoVessel);
                        }
                    }
                    else
                    {
                        vesselPool.Chuser = userName;
                        vesselPool.Chd = now;

                        dataContext.VesselPools.Attach(vesselPool, true);

                        List<VesselPoolInformation> vesselPoolInformations =
                            Queries.GetCurrentPoolInfoVesselsByVesselPool(dataContext, vesselPool.Id).ToList();

                        IEnumerable<long> value1 = vesselPoolInformations.Select(a => a.PoolInfoVessel.Id);
                        IEnumerable<long> value2 = vesselPool.CurrentVessels.Select(b => b.Id);

                        if (vesselPoolInformations.Select(a => a.PoolInfoVessel).Count() !=
                            vesselPool.CurrentVessels.Count ||
                            vesselPoolInformations.Select(a => a.PoolInfoVessel.VesselId).Except(
                                vesselPool.CurrentVessels.Select(b => b.Id)).Count() != 0)
                        {
                            VesselPoolInfo vesselPoolInfo =
                                vesselPoolInformations.Select(a => a.VesselPoolInfo).Distinct().Single();

                            vesselPoolInfo.PeriodTo = now;

                            var vesselPoolInfoNew = new VesselPoolInfo
                            {
                                Id = dataContext.GetNextId(typeof(VesselPoolInfo)),
                                VesselPoolId = vesselPool.Id,
                                PeriodFrom = now,
                                Cruser = userName,
                                Chuser = userName,
                                Crd = now,
                                Chd = now
                            };

                            dataContext.VesselPoolInfos.InsertOnSubmit(vesselPoolInfoNew);

                            foreach (Vessel vessel in vesselPool.CurrentVessels)
                            {
                                var poolInfoVessel = new PoolInfoVessel
                                {
                                    Id = dataContext.GetNextId(typeof(PoolInfoVessel)),
                                    VesselPoolInfoId = vesselPoolInfoNew.Id,
                                    VesselId = vessel.Id
                                };
                                dataContext.PoolInfoVessels.InsertOnSubmit(poolInfoVessel);
                            }
                        }
                    }
                }
                #endregion

                #region User

                else if (entity.GetType() == typeof(User))
                {
                    var user = entity as User;

                    bool isLoginUnique = (from objUser in dataContext.Users
                                          where objUser.Id != user.Id
                                                && objUser.Login.ToUpper() == user.Login.ToUpper()
                                          select objUser).SingleOrDefault() == null;

                    if (!isLoginUnique) return 1;

                    if (!isEdit)
                    {
                        user.Id = dataContext.GetNextId(typeof(User));
                        user.Password = Convert.ToBase64String(Encoding.ASCII.GetBytes(user.Password));
                        user.Cruser = userName;
                        user.Chuser = userName;
                        user.Crd = now;
                        user.Chd = now;

                        dataContext.Users.InsertOnSubmit(user);
                    }
                    else
                    {
                        user.Password = Convert.ToBase64String(Encoding.ASCII.GetBytes(user.Password));
                        user.Chuser = userName;
                        user.Chd = now;

                        dataContext.Users.Attach(user, true);

                        List<UserTrader> userTraders = Queries.GetUserTraderAssocsByUserId(dataContext, user.Id).ToList();
                        List<UserBook> userBooks = Queries.GetUserBookAssocsByUserId(dataContext, user.Id).ToList();

                        foreach (UserTrader userTrader in userTraders)
                        {
                            dataContext.UserTraders.DeleteOnSubmit(userTrader);
                        }

                        foreach (UserBook userBook in userBooks)
                        {
                            dataContext.UserBooks.DeleteOnSubmit(userBook);
                        }
                    }

                    foreach (Trader trader in user.Traders)
                    {
                        dataContext.UserTraders.InsertOnSubmit(new UserTrader
                        {
                            Id =
                                dataContext.GetNextId(
                                    typeof(UserTrader)),
                            UserId = user.Id,
                            TraderId = trader.Id
                        });
                    }

                    foreach (Book book in user.Books)
                    {
                        dataContext.UserBooks.InsertOnSubmit(new UserBook()
                        {
                            Id =
                                dataContext.GetNextId(
                                    typeof(UserBook)),
                            UserId = user.Id,
                            BookId = book.Id
                        });
                    }
                }
                #endregion

                #region Contact

                else if (entity.GetType() == typeof(Contact))
                {
                    var contact = entity as Contact;

                    bool isNameUnique = (from objContact in dataContext.Contacts
                                         where objContact.Id != contact.Id
                                               && objContact.Name.ToUpper() == contact.Name.ToUpper()
                                         select objContact).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        contact.Id = dataContext.GetNextId(typeof(Contact));
                        contact.Cruser = userName;
                        contact.Chuser = userName;
                        contact.Crd = now;
                        contact.Chd = now;

                        dataContext.Contacts.InsertOnSubmit(contact);
                    }
                    else
                    {
                        contact.Chuser = userName;
                        contact.Chd = now;

                        dataContext.Contacts.Attach(contact, true);
                    }
                }

                #endregion

                #region Index

                else if (entity.GetType() == typeof(Index))
                {
                    var index = entity as Index;

                    bool isNameUnique = (from objIndex in dataContext.Indexes
                                         where objIndex.Id != index.Id
                                               && (objIndex.Name.ToUpper() == index.Name.ToUpper()
                                               || objIndex.Code.ToUpper() == index.Code.ToUpper())
                                         select objIndex).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    RiskVolatility riskVolatility = null;
                    List<RiskCorrelation> riskSourceCorrelations = null;
                    List<RiskCorrelation> riskTargetCorrelations = null;
                    List<Index> indexes = Queries.GetAllIndexes(dataContext).ToList();

                    if (index.IsCustom)
                    {
                        riskSourceCorrelations = dataContext.RiskCorrelations.Where(
                            a =>
                            a.SourceIndexId == index.CustBaseIndexId && a.TargetIndexId != index.CustBaseIndexId).
                            ToList();

                        riskTargetCorrelations = dataContext.RiskCorrelations.Where(
                            a =>
                            a.TargetIndexId == index.CustBaseIndexId && a.SourceIndexId != index.CustBaseIndexId).
                            ToList();

                        riskVolatility =
                            dataContext.RiskVolatilities.Where(a => a.IndexId == index.CustBaseIndexId).SingleOrDefault();
                    }

                    #region Add
                    if (!isEdit)
                    {
                        index.Id = dataContext.GetNextId(typeof(Index));
                        index.Cruser = userName;
                        index.Chuser = userName;
                        index.Crd = now;
                        index.Chd = now;

                        foreach (Index exisitingIndex in indexes)
                        {
                            var newRiskSourceCorrelation = new RiskCorrelation()
                            {
                                Id = dataContext.GetNextId(typeof(RiskCorrelation)),
                                SourceIndexId = index.Id,
                                TargetIndexId = exisitingIndex.Id,
                                Value =
                                                                       index.IsCustom
                                                                           ? (riskSourceCorrelations.Where(
                                                                               a =>
                                                                               a.SourceIndexId == index.CustBaseIndexId &&
                                                                               a.TargetIndexId == exisitingIndex.Id).Any
                                                                                  ()
                                                                                  ? riskSourceCorrelations.Where(
                                                                                      a =>
                                                                                      a.SourceIndexId ==
                                                                                      index.CustBaseIndexId &&
                                                                                      a.TargetIndexId ==
                                                                                      exisitingIndex.Id).Single().Value
                                                                                  : 0)
                                                                           : 0
                            };
                            dataContext.RiskCorrelations.InsertOnSubmit(newRiskSourceCorrelation);

                            var newTargetSourceCorrelation = new RiskCorrelation()
                            {
                                Id =
                                                                         dataContext.GetNextId(typeof(RiskCorrelation)),
                                SourceIndexId = exisitingIndex.Id,
                                TargetIndexId = index.Id,
                                Value =
                                                                         index.IsCustom
                                                                             ? (riskTargetCorrelations.Where(
                                                                                 a =>
                                                                                 a.TargetIndexId ==
                                                                                 index.CustBaseIndexId &&
                                                                                 a.SourceIndexId == exisitingIndex.Id).
                                                                                    Any
                                                                                    ()
                                                                                    ? riskTargetCorrelations.Where(
                                                                                        a =>
                                                                                        a.TargetIndexId ==
                                                                                        index.CustBaseIndexId &&
                                                                                        a.SourceIndexId ==
                                                                                        exisitingIndex.Id).Single().
                                                                                          Value
                                                                                    : 0)
                                                                             : 0
                            };
                            dataContext.RiskCorrelations.InsertOnSubmit(newTargetSourceCorrelation);
                        }

                        if (index.IsCustom)
                        {
                            dataContext.RiskVolatilities.InsertOnSubmit(new RiskVolatility()
                            {
                                Id =
                                                                                    dataContext.GetNextId(
                                                                                        typeof(RiskVolatility)),
                                IndexId = index.Id,
                                Value =
                                                                                    riskVolatility == null
                                                                                        ? 0
                                                                                        : riskVolatility.Value
                            });
                            dataContext.RiskCorrelations.InsertOnSubmit(new RiskCorrelation()
                            {
                                Id =
                                    dataContext.GetNextId(
                                        typeof(RiskCorrelation)),
                                TargetIndexId = index.Id,
                                SourceIndexId = index.Id,
                                Value = 100
                            });
                        }

                        dataContext.Indexes.InsertOnSubmit(index);
                    }
                    #endregion

                    #region Edit
                    else
                    {
                        Index existingIndex = dataContext.Indexes.Where(a => a.Id == index.Id).Single();
                        if (!existingIndex.IsCustom && index.IsCustom)
                        {
                            //Delete any existing correlations/volatilities the index may have from previous edits
                            dataContext.RiskVolatilities.DeleteAllOnSubmit(
                                dataContext.RiskVolatilities.Where(a => a.IndexId == index.Id).ToList());
                            dataContext.RiskCorrelations.DeleteAllOnSubmit(
                                dataContext.RiskCorrelations.Where(
                                    a => a.TargetIndexId == index.Id || a.SourceIndexId == index.Id).ToList());

                            riskSourceCorrelations = riskSourceCorrelations.Where(a => a.SourceIndexId != index.Id).ToList();

                            riskTargetCorrelations = riskTargetCorrelations.Where(a => a.TargetIndexId != index.Id).ToList();

                            foreach (Index exisitingIndex in indexes.Where(a => a.Id != index.Id))
                            {
                                var newRiskSourceCorrelation = new RiskCorrelation()
                                {
                                    Id = dataContext.GetNextId(typeof(RiskCorrelation)),
                                    SourceIndexId = index.Id,
                                    TargetIndexId = exisitingIndex.Id,
                                    Value =
                                        index.IsCustom
                                            ? (riskSourceCorrelations.Where(
                                                a =>
                                                a.SourceIndexId == index.CustBaseIndexId &&
                                                a.TargetIndexId == exisitingIndex.Id).Any
                                                   ()
                                                   ? riskSourceCorrelations.Where(
                                                       a =>
                                                       a.SourceIndexId ==
                                                       index.CustBaseIndexId &&
                                                       a.TargetIndexId ==
                                                       exisitingIndex.Id).Single().Value
                                                   : 0)
                                            : 0
                                };
                                dataContext.RiskCorrelations.InsertOnSubmit(newRiskSourceCorrelation);

                                var newTargetSourceCorrelation = new RiskCorrelation()
                                {
                                    Id = dataContext.GetNextId(typeof(RiskCorrelation)),
                                    SourceIndexId = exisitingIndex.Id,
                                    TargetIndexId = index.Id,
                                    Value =
                                        index.IsCustom
                                            ? (riskTargetCorrelations.Where(
                                                a =>
                                                a.TargetIndexId == index.CustBaseIndexId &&
                                                a.SourceIndexId == exisitingIndex.Id).Any
                                                   ()
                                                   ? riskTargetCorrelations.Where(
                                                       a =>
                                                       a.TargetIndexId ==
                                                       index.CustBaseIndexId &&
                                                       a.SourceIndexId ==
                                                       exisitingIndex.Id).Single().Value
                                                   : 0)
                                            : 0
                                };
                                dataContext.RiskCorrelations.InsertOnSubmit(newTargetSourceCorrelation);
                            }

                            dataContext.RiskVolatilities.InsertOnSubmit(new RiskVolatility()
                            {
                                Id =
                                                                                    dataContext.GetNextId(
                                                                                        typeof(RiskVolatility)),
                                IndexId = index.Id,
                                Value =
                                                                                    riskVolatility == null
                                                                                        ? 0
                                                                                        : riskVolatility.Value
                            });

                            dataContext.RiskCorrelations.InsertOnSubmit(new RiskCorrelation()
                            {
                                Id =
                                    dataContext.GetNextId(
                                        typeof(RiskCorrelation)),
                                TargetIndexId = index.Id,
                                SourceIndexId = index.Id,
                                Value = 100
                            });
                        }

                        existingIndex.Chuser = userName;
                        existingIndex.Chd = now;
                        existingIndex.Code = index.Code;
                        existingIndex.CustAbsOffset = index.CustAbsOffset;
                        existingIndex.CustBaseIndexId = index.CustBaseIndexId;
                        existingIndex.CustPercOffset = index.CustPercOffset;
                        existingIndex.IsCustom = index.IsCustom;
                        existingIndex.IsMarketDefault = index.IsMarketDefault;
                        existingIndex.MarketId = index.MarketId;
                        existingIndex.Name = index.Name;
                        existingIndex.Status = index.Status;
                        existingIndex.AssocOffset = index.AssocOffset;
                        existingIndex.IsAssocIndex = index.IsAssocIndex;
                        existingIndex.FFAMappingName = index.FFAMappingName;
                        existingIndex.BOAMappingName = index.BOAMappingName;
                        existingIndex.SpotMappingName = index.SpotMappingName;
                        existingIndex.DerivedFromIndexId = index.DerivedFromIndexId;
                        existingIndex.DerivedOffset = index.DerivedOffset;
                        existingIndex.IsReportsMarketDefault = index.IsReportsMarketDefault;
                        existingIndex.CoverAssocIndexId = index.CoverAssocIndexId;
                        existingIndex.CoverAssocOffset = index.CoverAssocOffset;
                    }
                    #endregion
                }
                #endregion

                #region Loan

                else if (entity.GetType() == typeof(Loan))
                {
                    var loan = entity as Loan;

                    bool isCodeUnique = (from objLoan in dataContext.Loans
                                         where objLoan.Id != loan.Id
                                               && objLoan.ExternalCode.ToUpper() == loan.ExternalCode.ToUpper()
                                         select objLoan).SingleOrDefault() == null;

                    if (!isCodeUnique) return 1;

                    if (!isEdit)
                    {
                        loan.Id = dataContext.GetNextId(typeof(Loan));
                        loan.Cruser = userName;
                        loan.Chuser = userName;
                        loan.Crd = now;
                        loan.Chd = now;

                        dataContext.Loans.InsertOnSubmit(loan);

                        foreach (LoanBank loanBank in loan.LoanBanks)
                        {
                            loanBank.Id = dataContext.GetNextId(typeof(LoanBank));
                            loanBank.LoanId = loan.Id;

                            dataContext.LoanBanks.InsertOnSubmit(loanBank);
                        }
                        foreach (LoanCollateralAsset loanCollateralAsset in loan.LoanCollateralAssets)
                        {
                            loanCollateralAsset.Id = dataContext.GetNextId(typeof(LoanCollateralAsset));
                            loanCollateralAsset.LoanId = loan.Id;

                            dataContext.LoanCollateralAssets.InsertOnSubmit(loanCollateralAsset);
                        }
                        foreach (LoanBorrower loanBorrower in loan.LoanBorrowers)
                        {
                            loanBorrower.Id = dataContext.GetNextId(typeof(LoanBorrower));
                            loanBorrower.LoanId = loan.Id;

                            dataContext.LoanBorrowers.InsertOnSubmit(loanBorrower);
                        }
                        foreach (LoanTranche loanTranche in loan.LoanTranches)
                        {
                            loanTranche.Id = dataContext.GetNextId(typeof(LoanTranche));
                            loanTranche.LoanId = loan.Id;

                            foreach (LoanDrawdown loanDrawdown in loanTranche.LoanDrawdowns)
                            {
                                loanDrawdown.Id = dataContext.GetNextId(typeof(LoanDrawdown));
                                loanDrawdown.LoanTrancheId = loanTranche.Id;

                                dataContext.LoanDrawdowns.InsertOnSubmit(loanDrawdown);
                            }
                            foreach (LoanPrincipalInstallment loanPrincipalInstallment in loanTranche.LoanPrincipalInstallments)
                            {
                                loanPrincipalInstallment.Id = dataContext.GetNextId(typeof(LoanPrincipalInstallment));
                                loanPrincipalInstallment.LoanTrancheId = loanTranche.Id;

                                dataContext.LoanPrincipalInstallments.InsertOnSubmit(loanPrincipalInstallment);
                            }
                            foreach (LoanInterestInstallment loanInterestInstallment in loanTranche.LoanInterestInstallments)
                            {
                                loanInterestInstallment.Id = dataContext.GetNextId(typeof(LoanInterestInstallment));
                                loanInterestInstallment.LoanTrancheId = loanTranche.Id;

                                dataContext.LoanInterestInstallments.InsertOnSubmit(loanInterestInstallment);
                            }
                            dataContext.LoanTranches.InsertOnSubmit(loanTranche);
                        }
                    }
                    else
                    {
                        loan.Chuser = userName;
                        loan.Chd = now;

                        dataContext.Loans.Attach(loan, true);
                        dataContext.LoanTranches.AttachAll(loan.LoanTranches.Where(a => a.Id != 0).ToList(), true);

                        List<LoanBank> existingLoanBanks =
                            dataContext.LoanBanks.Where(a => a.LoanId == loan.Id).ToList();
                        dataContext.LoanBanks.DeleteAllOnSubmit(existingLoanBanks);

                        foreach (LoanBank loanBank in loan.LoanBanks)
                        {
                            loanBank.Id = dataContext.GetNextId(typeof(LoanBank));
                            loanBank.LoanId = loan.Id;

                            dataContext.LoanBanks.InsertOnSubmit(loanBank);
                        }

                        List<LoanCollateralAsset> existingLoanCollateralAssets =
                            dataContext.LoanCollateralAssets.Where(a => a.LoanId == loan.Id).ToList();
                        dataContext.LoanCollateralAssets.DeleteAllOnSubmit(existingLoanCollateralAssets);

                        foreach (LoanCollateralAsset loanCollateralAsset in loan.LoanCollateralAssets)
                        {
                            loanCollateralAsset.Id = dataContext.GetNextId(typeof(LoanCollateralAsset));
                            loanCollateralAsset.LoanId = loan.Id;

                            dataContext.LoanCollateralAssets.InsertOnSubmit(loanCollateralAsset);
                        }

                        List<LoanBorrower> existingLoanBorrowers =
                            dataContext.LoanBorrowers.Where(a => a.LoanId == loan.Id).ToList();
                        dataContext.LoanBorrowers.DeleteAllOnSubmit(existingLoanBorrowers);
                        foreach (LoanBorrower loanBorrower in loan.LoanBorrowers)
                        {
                            loanBorrower.Id = dataContext.GetNextId(typeof(LoanBorrower));
                            loanBorrower.LoanId = loan.Id;

                            dataContext.LoanBorrowers.InsertOnSubmit(loanBorrower);
                        }

                        List<LoanTranche> existingLoanTranches =
                            dataContext.LoanTranches.Where(a => a.LoanId == loan.Id).ToList();

                        #region Manage possibly deleted tranches
                        //For the tranches that do not no longer exist (were deleted) delete installments
                        List<LoanTranche> deletedTranches =
                            existingLoanTranches.Where(a => !loan.LoanTranches.Select(b => b.Id).Contains(a.Id)).ToList();
                        dataContext.LoanTranches.DeleteAllOnSubmit(deletedTranches);

                        List<LoanPrincipalInstallment> deletedLoanPrincipalInstallments =
                            dataContext.LoanPrincipalInstallments.Where(
                                a => deletedTranches.Select(b => b.Id).Contains(a.LoanTrancheId)).ToList();
                        dataContext.LoanPrincipalInstallments.DeleteAllOnSubmit(deletedLoanPrincipalInstallments);

                        List<LoanInterestInstallment> deletedLoanInterestInstallments =
                            dataContext.LoanInterestInstallments.Where(
                                a => deletedTranches.Select(b => b.Id).Contains(a.LoanTrancheId)).ToList();
                        dataContext.LoanInterestInstallments.DeleteAllOnSubmit(deletedLoanInterestInstallments);

                        #endregion

                        List<LoanDrawdown> existingloanDrawdowns =
                            dataContext.LoanDrawdowns.Where(
                                a => existingLoanTranches.Select(b => b.Id).Contains(a.LoanTrancheId)).ToList();
                        dataContext.LoanDrawdowns.DeleteAllOnSubmit(existingloanDrawdowns);

                        foreach (LoanTranche loanTranche in loan.LoanTranches)
                        {
                            long loanTrancheId = loanTranche.Id;
                            if (loanTranche.Id == 0)//If it is a newly added tranche
                            {
                                loanTrancheId = dataContext.GetNextId(typeof(LoanTranche));
                                loanTranche.Id = loanTrancheId;
                                dataContext.LoanTranches.InsertOnSubmit(loanTranche);

                                foreach (LoanPrincipalInstallment loanPrincipalInstallment in loanTranche.LoanPrincipalInstallments)
                                {
                                    loanPrincipalInstallment.Id = dataContext.GetNextId(typeof(LoanPrincipalInstallment));
                                    loanPrincipalInstallment.LoanTrancheId = loanTrancheId;

                                    dataContext.LoanPrincipalInstallments.InsertOnSubmit(loanPrincipalInstallment);
                                }
                                foreach (LoanInterestInstallment loanInterestInstallment in loanTranche.LoanInterestInstallments)
                                {
                                    loanInterestInstallment.Id = dataContext.GetNextId(typeof(LoanInterestInstallment));
                                    loanInterestInstallment.LoanTrancheId = loanTrancheId;

                                    dataContext.LoanInterestInstallments.InsertOnSubmit(loanInterestInstallment);
                                }
                            }
                            foreach (LoanDrawdown loanDrawdown in loanTranche.LoanDrawdowns)
                            {
                                loanDrawdown.Id = dataContext.GetNextId(typeof(LoanDrawdown));
                                loanDrawdown.LoanTrancheId = loanTrancheId;

                                dataContext.LoanDrawdowns.InsertOnSubmit(loanDrawdown);
                            }
                        }
                    }
                }
                #endregion

                #region CashFlowItem

                else if (entity.GetType() == typeof(CashFlowItem))
                {
                    var cashFlowItem = entity as CashFlowItem;

                    bool isNameUnique = (from objItem in dataContext.CashFlowItems
                                         where objItem.Id != cashFlowItem.Id
                                               && objItem.Name.ToUpper() == cashFlowItem.Name.ToUpper()
                                         select objItem).SingleOrDefault() == null;

                    if (!isNameUnique) return 1;

                    if (!isEdit)
                    {
                        cashFlowItem.Id = dataContext.GetNextId(typeof(CashFlowItem));
                        cashFlowItem.Cruser = userName;
                        cashFlowItem.Chuser = userName;
                        cashFlowItem.Crd = now;
                        cashFlowItem.Chd = now;

                        dataContext.CashFlowItems.InsertOnSubmit(cashFlowItem);
                    }
                    else
                    {
                        cashFlowItem.Chuser = userName;
                        cashFlowItem.Chd = now;

                        dataContext.CashFlowItems.Attach(cashFlowItem, true);
                    }
                }

                #endregion

                #region ExchangeRate

                else if (entity.GetType() == typeof(ExchangeRate))
                {
                    var exchangeRate = entity as ExchangeRate;

                    ExchangeRate existingRate = dataContext.ExchangeRates.Where(
                        a =>
                        a.TargetCurrencyId == exchangeRate.TargetCurrencyId &&
                        a.SourceCurrencyId == exchangeRate.SourceCurrencyId && a.Date == exchangeRate.Date &&
                        a.Id != exchangeRate.Id).SingleOrDefault();

                    if (existingRate != null)
                        return 1;

                    if (!isEdit)
                    {
                        exchangeRate.Id = dataContext.GetNextId(typeof(ExchangeRate));

                        dataContext.ExchangeRates.InsertOnSubmit(exchangeRate);

                        //The reverse exchange rate
                        var rate = new ExchangeRate()
                        {
                            Id = dataContext.GetNextId(typeof(ExchangeRate)),
                            Date = exchangeRate.Date,
                            Rate = 1 / exchangeRate.Rate,
                            SourceCurrencyId = exchangeRate.TargetCurrencyId,
                            TargetCurrencyId = exchangeRate.SourceCurrencyId
                        };
                        dataContext.ExchangeRates.InsertOnSubmit(rate);
                    }
                    else
                    {
                        ExchangeRate rateToDelete =
                            dataContext.ExchangeRates.Where(a => a.Id == exchangeRate.Id).Single();
                        ExchangeRate reverseRateToDelete =
                            dataContext.ExchangeRates.Where(
                                a =>
                                a.SourceCurrencyId == rateToDelete.TargetCurrencyId &&
                                a.TargetCurrencyId == rateToDelete.SourceCurrencyId).Single();

                        dataContext.ExchangeRates.DeleteOnSubmit(rateToDelete);
                        dataContext.ExchangeRates.DeleteOnSubmit(reverseRateToDelete);

                        exchangeRate.Id = dataContext.GetNextId(typeof(ExchangeRate));
                        dataContext.ExchangeRates.InsertOnSubmit(exchangeRate);

                        //The reverse exchange rate
                        var rate = new ExchangeRate()
                        {
                            Id = dataContext.GetNextId(typeof(ExchangeRate)),
                            Date = exchangeRate.Date,
                            Rate = 1 / exchangeRate.Rate,
                            SourceCurrencyId = exchangeRate.TargetCurrencyId,
                            TargetCurrencyId = exchangeRate.SourceCurrencyId
                        };
                        dataContext.ExchangeRates.InsertOnSubmit(rate);
                    }
                }

                #endregion

                #region Application Parameter

                else if (entity.GetType() == typeof(AppParameter))
                {
                    var appParameter = entity as AppParameter;

                    bool isCodeUnique = (from objAppParameter in dataContext.AppParameters
                                         where objAppParameter.Id != appParameter.Id
                                               && objAppParameter.Code.ToUpper() == appParameter.Code.ToUpper()
                                         select objAppParameter).SingleOrDefault() == null;

                    if (!isCodeUnique) return 1;

                    if (!isEdit)
                    {
                        appParameter.Id = dataContext.GetNextId(typeof(AppParameter));
                        dataContext.AppParameters.InsertOnSubmit(appParameter);
                    }
                    else
                    {
                        dataContext.AppParameters.Attach(appParameter, true);
                    }
                }

                #endregion

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEIndex(bool isEdit, Index index, List<AverageIndexesAssoc> averageIndexesAssocs)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "AEIndex" };
                }

                DateTime now = DateTime.Now;

                bool isNameUnique = (from objIndex in dataContext.Indexes
                                     where objIndex.Id != index.Id
                                           && (objIndex.Name.ToUpper() == index.Name.ToUpper()
                                           || objIndex.Code.ToUpper() == index.Code.ToUpper())
                                     select objIndex).SingleOrDefault() == null;

                if (!isNameUnique) return 1;

                RiskVolatility riskVolatility = null;
                List<RiskCorrelation> riskSourceCorrelations = null;
                List<RiskCorrelation> riskTargetCorrelations = null;
                List<Index> indexes = Queries.GetAllIndexes(dataContext).ToList();

                if (index.IsCustom)
                {
                    riskSourceCorrelations = dataContext.RiskCorrelations.Where(
                        a =>
                        a.SourceIndexId == index.CustBaseIndexId && a.TargetIndexId != index.CustBaseIndexId).
                        ToList();

                    riskTargetCorrelations = dataContext.RiskCorrelations.Where(
                        a =>
                        a.TargetIndexId == index.CustBaseIndexId && a.SourceIndexId != index.CustBaseIndexId).
                        ToList();

                    riskVolatility =
                        dataContext.RiskVolatilities.Where(a => a.IndexId == index.CustBaseIndexId).SingleOrDefault();
                }

                if (index.IsMarketDefault)
                {
                    bool defaultExists = (from objAvgIndexAssoc in dataContext.AverageIndexesAssocs
                                          from objIndex in dataContext.Indexes
                                          where objAvgIndexAssoc.AvgIndexId == objIndex.Id &&
                                                objIndex.IsMarketDefault &&
                                                objIndex.MarketId == index.MarketId &&
                                                objIndex.Id != index.Id
                                          select objIndex).Any();

                    if (defaultExists)
                        return 2;
                }                

                if (index.IsReportsMarketDefault)
                {
                    bool defaultReportsExists = (from objIndex in dataContext.Indexes
                                          where objIndex.IsReportsMarketDefault &&
                                                objIndex.MarketId == index.MarketId &&
                                                objIndex.Id != index.Id
                                          select objIndex).Any();

                    if (defaultReportsExists)
                        return 3;
                }

                #region Add
                if (!isEdit)
                {
                    index.Id = dataContext.GetNextId(typeof(Index));
                    index.Cruser = userName;
                    index.Chuser = userName;
                    index.Crd = now;
                    index.Chd = now;

                    foreach (Index exisitingIndex in indexes)
                    {
                        var newRiskSourceCorrelation = new RiskCorrelation()
                        {
                            Id = dataContext.GetNextId(typeof(RiskCorrelation)),
                            SourceIndexId = index.Id,
                            TargetIndexId = exisitingIndex.Id,
                            Value =
                                index.IsCustom
                                    ? (riskSourceCorrelations.Where(
                                        a =>
                                        a.SourceIndexId == index.CustBaseIndexId &&
                                        a.TargetIndexId == exisitingIndex.Id).Any
                                           ()
                                           ? riskSourceCorrelations.Where(
                                               a =>
                                               a.SourceIndexId ==
                                               index.CustBaseIndexId &&
                                               a.TargetIndexId ==
                                               exisitingIndex.Id).Single().Value
                                           : 0)
                                    : 0
                        };
                        dataContext.RiskCorrelations.InsertOnSubmit(newRiskSourceCorrelation);

                        var newTargetSourceCorrelation = new RiskCorrelation()
                        {
                            Id =
                                dataContext.GetNextId(typeof(RiskCorrelation)),
                            SourceIndexId = exisitingIndex.Id,
                            TargetIndexId = index.Id,
                            Value =
                                index.IsCustom
                                    ? (riskTargetCorrelations.Where(
                                        a =>
                                        a.TargetIndexId ==
                                        index.CustBaseIndexId &&
                                        a.SourceIndexId == exisitingIndex.Id).
                                           Any
                                           ()
                                           ? riskTargetCorrelations.Where(
                                               a =>
                                               a.TargetIndexId ==
                                               index.CustBaseIndexId &&
                                               a.SourceIndexId ==
                                               exisitingIndex.Id).Single().
                                                 Value
                                           : 0)
                                    : 0
                        };
                        dataContext.RiskCorrelations.InsertOnSubmit(newTargetSourceCorrelation);
                    }

                    if (index.IsCustom)
                    {
                        dataContext.RiskVolatilities.InsertOnSubmit(new RiskVolatility()
                        {
                            Id =
                                dataContext.GetNextId(
                                    typeof(RiskVolatility)),
                            IndexId = index.Id,
                            Value =
                                riskVolatility == null
                                    ? 0
                                    : riskVolatility.Value
                        });
                        dataContext.RiskCorrelations.InsertOnSubmit(new RiskCorrelation()
                        {
                            Id =
                                dataContext.GetNextId(
                                    typeof(RiskCorrelation)),
                            TargetIndexId = index.Id,
                            SourceIndexId = index.Id,
                            Value = 100
                        });
                    }

                    //if(index.IsMarketDefault)
                    if (index.IsAssocIndex)
                    {
                        foreach (AverageIndexesAssoc assoc in averageIndexesAssocs)
                        {
                            assoc.Id = dataContext.GetNextId(typeof(AverageIndexesAssoc));
                            assoc.AvgIndexId = index.Id;
                            dataContext.AverageIndexesAssocs.InsertOnSubmit(assoc);
                        }
                    }

                    dataContext.Indexes.InsertOnSubmit(index);
                }
                #endregion

                #region Edit
                else
                {
                    Index existingIndex = dataContext.Indexes.Where(a => a.Id == index.Id).Single();
                    if (!existingIndex.IsCustom && index.IsCustom)
                    {
                        //Delete any existing correlations/volatilities the index may have from previous edits
                        dataContext.RiskVolatilities.DeleteAllOnSubmit(
                            dataContext.RiskVolatilities.Where(a => a.IndexId == index.Id).ToList());
                        dataContext.RiskCorrelations.DeleteAllOnSubmit(
                            dataContext.RiskCorrelations.Where(
                                a => a.TargetIndexId == index.Id || a.SourceIndexId == index.Id).ToList());

                        riskSourceCorrelations = riskSourceCorrelations.Where(a => a.SourceIndexId != index.Id).ToList();

                        riskTargetCorrelations = riskTargetCorrelations.Where(a => a.TargetIndexId != index.Id).ToList();

                        foreach (Index exisitingIndex in indexes.Where(a => a.Id != index.Id))
                        {
                            var newRiskSourceCorrelation = new RiskCorrelation()
                            {
                                Id = dataContext.GetNextId(typeof(RiskCorrelation)),
                                SourceIndexId = index.Id,
                                TargetIndexId = exisitingIndex.Id,
                                Value =
                                    index.IsCustom
                                        ? (riskSourceCorrelations.Where(
                                            a =>
                                            a.SourceIndexId == index.CustBaseIndexId &&
                                            a.TargetIndexId == exisitingIndex.Id).Any
                                               ()
                                               ? riskSourceCorrelations.Where(
                                                   a =>
                                                   a.SourceIndexId ==
                                                   index.CustBaseIndexId &&
                                                   a.TargetIndexId ==
                                                   exisitingIndex.Id).Single().Value
                                               : 0)
                                        : 0
                            };
                            dataContext.RiskCorrelations.InsertOnSubmit(newRiskSourceCorrelation);

                            var newTargetSourceCorrelation = new RiskCorrelation()
                            {
                                Id = dataContext.GetNextId(typeof(RiskCorrelation)),
                                SourceIndexId = exisitingIndex.Id,
                                TargetIndexId = index.Id,
                                Value =
                                    index.IsCustom
                                        ? (riskTargetCorrelations.Where(
                                            a =>
                                            a.TargetIndexId == index.CustBaseIndexId &&
                                            a.SourceIndexId == exisitingIndex.Id).Any
                                               ()
                                               ? riskTargetCorrelations.Where(
                                                   a =>
                                                   a.TargetIndexId ==
                                                   index.CustBaseIndexId &&
                                                   a.SourceIndexId ==
                                                   exisitingIndex.Id).Single().Value
                                               : 0)
                                        : 0
                            };
                            dataContext.RiskCorrelations.InsertOnSubmit(newTargetSourceCorrelation);
                        }

                        dataContext.RiskVolatilities.InsertOnSubmit(new RiskVolatility()
                        {
                            Id =
                                dataContext.GetNextId(
                                    typeof(RiskVolatility)),
                            IndexId = index.Id,
                            Value =
                                riskVolatility == null
                                    ? 0
                                    : riskVolatility.Value
                        });

                        dataContext.RiskCorrelations.InsertOnSubmit(new RiskCorrelation()
                        {
                            Id =
                                dataContext.GetNextId(
                                    typeof(RiskCorrelation)),
                            TargetIndexId = index.Id,
                            SourceIndexId = index.Id,
                            Value = 100
                        });
                    }

                    existingIndex.Chuser = userName;
                    existingIndex.Chd = now;
                    existingIndex.Code = index.Code;
                    existingIndex.CustAbsOffset = index.CustAbsOffset;
                    existingIndex.CustBaseIndexId = index.CustBaseIndexId;
                    existingIndex.CustPercOffset = index.CustPercOffset;
                    existingIndex.IsCustom = index.IsCustom;
                    existingIndex.IsMarketDefault = index.IsMarketDefault;
                    existingIndex.MarketId = index.MarketId;
                    existingIndex.Name = index.Name;
                    existingIndex.Status = index.Status;
                    existingIndex.AssocOffset = index.AssocOffset;
                    existingIndex.IsAssocIndex = index.IsAssocIndex;
                    existingIndex.FFAMappingName = index.FFAMappingName;
                    existingIndex.BOAMappingName = index.BOAMappingName;
                    existingIndex.SpotMappingName = index.SpotMappingName;
                    existingIndex.DerivedFromIndexId = index.DerivedFromIndexId;
                    existingIndex.DerivedOffset = index.DerivedOffset;
                    existingIndex.IsReportsMarketDefault = index.IsReportsMarketDefault;
                    existingIndex.CoverAssocIndexId = index.CoverAssocIndexId;
                    existingIndex.CoverAssocOffset = index.CoverAssocOffset;



                    List<AverageIndexesAssoc> existingAssocs =
                            dataContext.AverageIndexesAssocs.Where(a => a.AvgIndexId == existingIndex.Id).ToList();
                    dataContext.AverageIndexesAssocs.DeleteAllOnSubmit(existingAssocs);

                    if (index.IsAssocIndex)
                    //if(index.IsMarketDefault)
                    {
                        foreach (AverageIndexesAssoc assoc in averageIndexesAssocs)
                        {
                            assoc.Id = dataContext.GetNextId(typeof(AverageIndexesAssoc));
                            assoc.AvgIndexId = index.Id;
                            dataContext.AverageIndexesAssocs.InsertOnSubmit(assoc);
                        }
                    }
                }
                #endregion

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetEntities(DomainObject entity, out List<DomainObject> entities)
        {
            entities = null;
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetEntities." + entity.GetType().Name + ": " };
                }

                if (entity.GetType() == typeof(Company))
                {
                    List<CompanyInformation> companyInfos = Queries.GetCompaniesWithManagersSubtypes(dataContext).ToList();
                    foreach (CompanyInformation companyInfo in companyInfos)
                    {
                        companyInfo.Company.ManagerName = companyInfo.Contact == null
                                                              ? null
                                                              : companyInfo.Contact.Name;
                        companyInfo.Company.SubtypeName = companyInfo.Subtype.Name;
                    }
                    entities = companyInfos.Select(a => a.Company).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Trader))
                {
                    List<TraderInformation> traderInformations = Queries.GetTradersWithProfitCentresDesks(dataContext).ToList();
                    List<TraderInformation> traderMarkets = Queries.GetTradersWithMarkets(dataContext).ToList();
                    List<TraderInformation> traderCompanies = Queries.GetTradersWithCompanies(dataContext).ToList();
                    List<TraderInformation> traderBooks = Queries.GetTradersWithBooks(dataContext).ToList();

                    foreach (TraderInformation traderInformation in traderInformations)
                    {
                        traderInformation.Trader.ProfitCentreName = traderInformation.ProfitCentre == null
                                                                        ? null
                                                                        : traderInformation.ProfitCentre.Name;
                        traderInformation.Trader.DeskName = traderInformation.Desk == null
                                                                ? null
                                                                : traderInformation.Desk.Name;
                        traderInformation.Trader.Markets =
                            traderMarkets.Where(a => a.Trader.Id == traderInformation.Trader.Id).Select(b => b.Market).
                                ToList();

                        traderInformation.Trader.Companies =
                            traderCompanies.Where(a => a.Trader.Id == traderInformation.Trader.Id).Select(b => b.Company).
                                ToList();

                        traderInformation.Trader.Books =
                            traderBooks.Where(a => a.Trader.Id == traderInformation.Trader.Id).Select(
                                b =>
                                new CustomBook()
                                {
                                    Id = b.Book.Id,
                                    Name = b.Book.Name,
                                    ParentBookId = b.Book.ParentBookId,
                                    Status = b.Book.Status,
                                    PositionCalculatedByDays = b.Book.PositionCalculatedByDays
                                }).
                                ToList();
                    }
                    entities = traderInformations.Select(a => a.Trader).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Vessel))
                {
                    List<VesselInformation> vesselInformation = Queries.GetVesselsWithCompaniesAndMarkets(dataContext).ToList();
                    List<VesselExpense> allExpenses = Queries.GetVesselsExpenses(dataContext).ToList();

                    foreach (VesselInformation vesselInfo in vesselInformation)
                    {
                        vesselInfo.Vessel.Info = vesselInfo.VesselInfo;
                        vesselInfo.Vessel.CompanyName = vesselInfo.Company.Name;
                        vesselInfo.Vessel.MarketName = vesselInfo.Market.Name;
                        vesselInfo.Vessel.VesselExpenses =
                            allExpenses.Where(a => a.VesselId == vesselInfo.Vessel.Id).ToList();
                    }

                    entities = vesselInformation.Select(a => a.Vessel).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Book))
                {
                    entities = Queries.GetBooksAll(dataContext).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Account))
                {
                    List<AccountInformation> accounts = Queries.GetAccountsAll(dataContext).ToList();
                    foreach (AccountInformation accountInformation in accounts)
                    {
                        accountInformation.Account.CompanyName = accountInformation.Company.Name;
                        accountInformation.Account.BankName = accountInformation.Bank.Name;
                        accountInformation.Account.CurrencyCode = accountInformation.Currency.Code;
                    }
                    entities = Queries.GetAccountsAll(dataContext).Select(a => a.Account).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(ProfitCentre))
                {
                    entities = Queries.GetProfitCentresAll(dataContext).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Desk))
                {
                    entities = Queries.GetDesksAll(dataContext).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(ClearingHouse))
                {
                    entities = Queries.GetClearingHousesAll(dataContext).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(CompanySubtype))
                {
                    entities = Queries.GetCompanySubtypesAll(dataContext).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Index))
                {
                    List<IndexInformation> indexInfos = Queries.GetIndexesAllWithMarkets(dataContext).ToList();
                    foreach (IndexInformation indexInfo in indexInfos)
                    {
                        indexInfo.Index.MarketName = indexInfo.Market.Name;
                    }
                    entities = indexInfos.Select(a => a.Index).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(VesselPool))
                {
                    List<VesselPoolInformation> vesselPoolInformations = Queries.GetVesselPoolsWithVessels(dataContext).ToList();

                    List<VesselPool> vesselPools = vesselPoolInformations.Select(a => a.VesselPool).Distinct().ToList();
                    foreach (VesselPool vesselPool in vesselPools)
                    {
                        vesselPool.CurrentVessels =
                            vesselPoolInformations.Where(a => a.VesselPoolInfo.VesselPoolId == vesselPool.Id).Select(
                                a => a.Vessel).ToList();
                    }
                    entities = vesselPools.Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(User))
                {
                    List<User> users = Queries.GetUsers(dataContext).ToList();
                    List<TraderInformation> usersTraders = Queries.GetTradersWithUsers(dataContext).ToList();
                    List<BookInformation> usersBooks = Queries.GetBooksWithUsers(dataContext).ToList();

                    foreach (User user in users)
                    {
                        user.Traders =
                            usersTraders.Where(a => a.UserTrader.UserId == user.Id).Select(b => b.Trader).ToList();

                        user.Books =
                            usersBooks.Where(a => a.UserBook.UserId == user.Id).Select(b => b.Book).ToList();
                    }
                    entities = users.Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Contact))
                {
                    entities = Queries.GetContactsAll(dataContext).Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(Loan))
                {
                    List<Loan> loans = Queries.GetLoansAll(dataContext).ToList();

                    List<LoanBank> loanBanks = Queries.GetLoanBanks(dataContext).ToList();
                    List<LoanCollateralAsset> loanAssets = Queries.GetLoanAssets(dataContext).ToList();
                    List<LoanInformation> loanTranches = Queries.GetLoanTranches(dataContext).ToList();
                    List<LoanBorrower> loanBorrowers = Queries.GetLoanBorrowers(dataContext).ToList();
                    List<LoanPrincipalInstallment> loanPrincipalInstallments =
                        Queries.GetLoanPrincipalInstallmentsAll(dataContext).ToList();
                    List<LoanInterestInstallment> loanInterestInstallments =
                        Queries.GetLoanInterestInstallmentsAll(dataContext).ToList();
                    foreach (Loan loan in loans)
                    {
                        loan.LoanBanks = loanBanks.Where(a => a.LoanId == loan.Id).ToList();
                        loan.LoanCollateralAssets =
                            loanAssets.Where(a => a.LoanId == loan.Id).ToList();
                        loan.LoanBorrowers = loanBorrowers.Where(a => a.LoanId == loan.Id).ToList();

                        List<LoanTranche> tranches = loanTranches.Where(a => a.LoanTranche.LoanId == loan.Id).Select(a => a.LoanTranche).Distinct().ToList();
                        foreach (LoanTranche loanTranche in tranches)
                        {
                            loanTranche.LoanDrawdowns = loanTranches.Where(a => a.LoanTranche.Id == loanTranche.Id).Select(a => a.LoanDrawdown).ToList();
                            loanTranche.LoanPrincipalInstallments =
                                loanPrincipalInstallments.Where(a => a.LoanTrancheId == loanTranche.Id).ToList();
                            loanTranche.LoanInterestInstallments =
                                loanInterestInstallments.Where(a => a.LoanTrancheId == loanTranche.Id).ToList();
                        }
                        loan.LoanTranches = tranches;
                    }
                    entities = loans.Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(InterestReferenceRate))
                {
                    List<InterestReferenceRate> interestReferenceRates =
                        Queries.GetInterestReferenceRates(dataContext).ToList();
                    List<Currency> currencies = Queries.GetCurrenciesAll(dataContext).ToList();

                    foreach (InterestReferenceRate interestReferenceRate in interestReferenceRates)
                    {
                        interestReferenceRate.Currency = currencies.Where(a => a.Id == interestReferenceRate.CurrencyId).First().Name;
                    }

                    entities = interestReferenceRates.Cast<DomainObject>().ToList();
                }
                else if (entity.GetType() == typeof(ExchangeRate))
                {
                    List<ExchangeRate> exchangeRates =
                        Queries.GetExchangeRates(dataContext).ToList();
                    List<Currency> currencies = Queries.GetCurrenciesAll(dataContext).ToList();

                    foreach (ExchangeRate exchangeRate in exchangeRates)
                    {
                        exchangeRate.SourceCurrency = currencies.Where(a => a.Id == exchangeRate.SourceCurrencyId).First().Name;
                        exchangeRate.TargetCurrency = currencies.Where(a => a.Id == exchangeRate.TargetCurrencyId).First().Name;
                    }

                    entities = exchangeRates.Cast<DomainObject>().ToList();
                }
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? FtpBatchProcessingFiles(string ftpsource, string ftpArchiveUri, string ftpUri, string ftpXmlFolder, string ftpUsername, string ftpPassword,
            bool isFtpDownload, List<Index> indexes,
            Dictionary<string, int> monthStr, out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages)
        {
            errorMessages = new List<string>();
            lastDownLoadDate = DateTime.MinValue;
            lastImportDate = DateTime.MinValue;

            var greekCultureInfo = new CultureInfo("el-GR", false);

            DataContext dataContext = null;
            string userName = null;

            userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

            if (SiAuto.Si.Level == Level.Debug)
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "ProcessingFilesFromFtpServer." + ftpUri };
            }

            AppParameter lastDownloadedFileDate =
                     dataContext.AppParameters.Where(a => a.Code == "LAST_DOWNLOADED_BALTIC_XML_DATE").Single();
            lastDownLoadDate = Convert.ToDateTime(lastDownloadedFileDate.Value, greekCultureInfo).Date;

            AppParameter lastImportFileDate =
                   dataContext.AppParameters.Where(a => a.Code == "LAST_IMPORT_INDEXES_VALUES_DATE").Single();
            lastImportDate = Convert.ToDateTime(lastImportFileDate.Value, greekCultureInfo).Date;

            ////ftp TEST
            //ftpArchiveUri = @"ftp://exis.com.gr/archive/";
            //ftpUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities/Risk";
            //ftpXmlFolder = @"C:\FreightMetrics\Data\";
            //ftpUsername = "vaso";
            //ftpPassword = "agilis2014";


            Uri serverArchiveUri = new Uri(ftpArchiveUri);
            Uri serverUri = new Uri(ftpUri); //Uri serverUri = new Uri("ftp://ftp.balticexchange.org/");
            string downloadPath = ftpXmlFolder; //const string downloadPath = @"C:\FreightMetrics\";

            if (!Directory.Exists(downloadPath))
            {
                errorMessages.Add("Path '" + downloadPath +
                                  "' on Server is not valid. Either create it or change the value of application parameter '"
                                  + ftpXmlFolder + "'.");

                return 3;
            }

            var filesInArchiveFolder = false;
            var filesInFolder = false;

            if (ftpsource == "NASDAQ")
            {
                //ftp Nasdaq VOl
                var ftpNasdaqUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities";
                ftpUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities/Risk";
                var ftpFileVolCorr = @"\Vol and Corr matrices Dry Freight.xlsx"; //"\\Vol%20and%20Corr%20matrices%20Dry%20Freight.xlsx";
                var filenewfilename = @"\Vol_and_Corr_matrices_Dry_Freight" + DateTime.Today.ToString("yyMMdd") + ".xlsx";
                var ftpFolder = @"C:\FreightMetrics\Data\";
                var ftpFileVolCorrUri = ftpUri + ftpFileVolCorr;

                var ftpfilelist = new List<FileName>();
                getFileList(ftpUri, "", "", ftpfilelist);

                WebClient client = new WebClient();
                //client.Credentials = new NetworkCredential("username", "password");
                client.DownloadFile(ftpFileVolCorrUri, ftpFolder + ftpFileVolCorr);
                if (!File.Exists(ftpFolder + filenewfilename))
                {
                    File.Move(ftpFolder + ftpFileVolCorr, ftpFolder + filenewfilename);
                }
                //ftp Nasdaq Span
                //Create folder name
                //var spanfolder = DateTime.Today.ToString("yyMMdd");
                var spantoday = DateTime.Today;
                if (spantoday.DayOfWeek == DayOfWeek.Monday)
                {
                    DateTime lastFriday = DateTime.Now.AddDays(-1);
                    while (lastFriday.DayOfWeek != DayOfWeek.Friday)
                        lastFriday = lastFriday.AddDays(-1);
                    spantoday = lastFriday;
                }
                var spanfolder = spantoday.AddDays(-1).ToString("yyMMdd");
                var ftpNasdaqUriSpan = ftpNasdaqUri + "/PROD/Common/" + spanfolder;
                var request = (FtpWebRequest)WebRequest.Create(ftpNasdaqUriSpan.ToString());
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                try
                {
                    var response = (FtpWebResponse)request.GetResponse();

                    if (response != null)
                    {
                        Stream responseStream = response.GetResponseStream();
                        if (responseStream != null)
                        {
                            var reader = new StreamReader(responseStream);
                            string datastring = reader.ReadToEnd();

                            FileStruct[] list = (new ParseListDirectory()).GetList(datastring);
                            var webClient = new WebClient();
                            webClient.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                            var spanfilenames = list.Where(a => !a.IsDirectory && a.Name.Contains("NSPANPAR")).ToList();
                            var spnfilename = spanfilenames.OrderByDescending(f => f.Name).First();
                            //var nspanfilenames = list.Where(a => !a.IsDirectory && a.Name.Contains("NSPANPAR")).ToList();                            
                            //var nspnfilename = nspanfilenames.OrderByDescending(f => f.Name).First();
                            string fileName = spnfilename.Name;
                            //string fileNameSpan = nspnfilename.Name;
                            //set full file path to download (Path+fileName)
                            string strDownloadFilepath = ftpFolder + fileName;
                            //string strDownloadFilepath2 = ftpFolder + fileNameSpan;
                            //set the URI
                            var uri = new Uri(ftpNasdaqUriSpan.ToString() + "/" + fileName);
                            //var uri2 = new Uri(ftpNasdaqUriSpan.ToString() + "/" + fileNameSpan);
                            try
                            {   //Download file
                                webClient.DownloadFile(uri, strDownloadFilepath);
                                //webClient.DownloadFile(uri2, strDownloadFilepath2);
                            }
                            catch (WebException ex)
                            {
                                errorMessages.Add("There was an error while downloading file " + fileName +
                                                          ". Error was: " + ex.InnerException.Message);
                                return 2;
                            }

                            reader.Close();

                        }

                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                    HandleException(userName, ex);
                    //errorMessages.Add(ex.Message);
                    errorMessages.Add("There was an error while trying to connect to the ftp site. " + ex.Message);
                    return 3;
                }

            }
            else
            {
                #region Download from Archive Folder
                HandleMessageLog(userName, "Download from Archive folder");
                //First connect to archive uri to see if xmls were added after the last download date
                var request = (FtpWebRequest)WebRequest.Create(serverArchiveUri.ToString());
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);


                try
                {
                    var response = request.GetResponse() as FtpWebResponse;

                    if (response != null)
                    {
                        Stream responseStream = response.GetResponseStream();
                        if (responseStream != null)
                        {
                            string datastring;
                            using (var reader = new StreamReader(responseStream))
                            {
                                datastring = reader.ReadToEnd();
                            }

                            FileStruct[] list = (new ParseListDirectory()).GetList(datastring);
                            using (var webClient = new WebClient())
                            {

                                webClient.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                                foreach (FileStruct strFile in list.Where(a => !a.IsDirectory && !a.Name.Contains("CMSI")))
                                {
                                    string fileName = strFile.Name;

                                    //Get file date
                                    string fileDateStr = fileName.Split('_')[1].Substring(0, 8);
                                    string[] format = { "yyyyMMdd" };
                                    DateTime fileDate;

                                    if (DateTime.TryParseExact(fileDateStr, format, greekCultureInfo, DateTimeStyles.None,
                                        out fileDate))
                                    {
                                        if (fileDate >= lastDownLoadDate) //Then have to download file
                                        {
                                            filesInArchiveFolder = true;
                                            //set full file path to download (Path+fileName)
                                            string strDownloadFilepath = downloadPath + fileName;
                                            //set the URI
                                            var uri = new Uri(serverArchiveUri.ToString() + fileName);
                                            try
                                            {
                                                HandleMessageLog(userName,"Attempt to download file : " + strDownloadFilepath + "...");
                                                
                                                //Download file
                                                webClient.DownloadFile(uri, strDownloadFilepath);

                                                #region openFile

                                                try
                                                {
                                                    isFtpDownload = true;

                                                    var xmlDocument = new XmlDocument();
                                                    XmlElement objRequestElement;
                                                    StreamReader isoStreamReader = null;

                                                    #region try-catch streamReader

                                                    try
                                                    {
                                                        isoStreamReader = new StreamReader(strDownloadFilepath);
                                                    }
                                                    catch (Exception exc)
                                                    {
                                                        HandleException(userName, exc);
                                                        errorMessages.Add("There was an error trying to read file " +
                                                                          strDownloadFilepath);
                                                    }

                                                    #endregion

                                                    if (isoStreamReader != null && isoStreamReader.BaseStream.Length > 0)
                                                    {
                                                        isoStreamReader.BaseStream.Position = 0;
                                                        xmlDocument.Load(isoStreamReader);
                                                        objRequestElement = xmlDocument.DocumentElement;

                                                        isoStreamReader.Close();

                                                        if (objRequestElement != null)
                                                        {

                                                            try
                                                            {

                                                                ParseAndImportXML(xmlDocument.DocumentElement, fileName,
                                                                    indexes, monthStr,
                                                                    isFtpDownload, out lastDownLoadDate, out lastImportDate);

                                                            }
                                                            catch (ValidationException exc)
                                                            {
                                                                foreach (Exception myexc in exc.Exceptions)
                                                                {
                                                                    errorMessages.Add(myexc.Source);

                                                                }
                                                            }
                                                        }
                                                        else
                                                        {

                                                            errorMessages.Add("There was an error parsing file " +
                                                                              strDownloadFilepath);
                                                        } //objRequestElement != null
                                                    }
                                                    else
                                                    {

                                                        errorMessages.Add("There was an error trying to read file " +
                                                                          strDownloadFilepath);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    HandleException(userName, ex);
                                                    errorMessages.Add("There was an error parsing file " +
                                                                      strDownloadFilepath);
                                                }

                                                #endregion                                                
                                            }
                                            catch (WebException ex)
                                            {
                                                HandleException(userName, ex);
                                                //errorMessage = "There was an exception during import. Exception Message: " + exc.Message;

                                                errorMessages.Add("There was an error while downloading file " + fileName +
                                                                  ". Error was: " + ex.InnerException.Message);
                                                //File downloading errors

                                                return 2;
                                            }
                                        }
                                    }
                                }
                            }
                            //reader.Close();
                        }
                        response.Close();
                    }
                }
                catch (Exception ex)
                {                    
                    HandleException(userName, ex);
                    errorMessages.Add("There was an error while trying to connect to the ftp site. " + ex.Message);

                    //Connection Errors
                    return 3;
                }

                #endregion

                #region Download from Root folder
                HandleMessageLog(userName, "Download from Root folder");
                //Root uri
                request = (FtpWebRequest)WebRequest.Create(serverUri.ToString());
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

                try
                {
                    var response = (FtpWebResponse)request.GetResponse();

                    if (response != null)
                    {
                        Stream responseStream = response.GetResponseStream();
                        if (responseStream != null)
                        {
                            var reader = new StreamReader(responseStream);
                            string datastring = reader.ReadToEnd();

                            FileStruct[] list = (new ParseListDirectory()).GetList(datastring);
                            var webClient = new WebClient();
                            webClient.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                            foreach (FileStruct strFile in list.Where(a => !a.IsDirectory && !a.Name.Contains("CMSI")))
                            {
                                string fileName = strFile.Name;
                                //Get file date
                                string fileDateStr = fileName.Split('_')[1].Substring(0, 8);
                                string[] format = { "yyyyMMdd" };
                                DateTime fileDate;

                                if (DateTime.TryParseExact(fileDateStr, format,
                                                           greekCultureInfo,
                                                           DateTimeStyles.None, out fileDate))
                                {
                                    if (fileDate >= lastDownLoadDate) //Then have to download file
                                    {
                                        filesInFolder = true;
                                        //set full file path to download (Path+fileName)
                                        string strDownloadFilepath = downloadPath + fileName;
                                        //set the URI
                                        var uri = new Uri(serverUri.ToString() + fileName);
                                        try
                                        {
                                            HandleMessageLog(userName,"Attempt to download file : " + strDownloadFilepath + "...");
                                            //Download file
                                            webClient.DownloadFile(uri, strDownloadFilepath);

                                            #region openFile

                                            try
                                            {
                                                isFtpDownload = true;

                                                var xmlDocument = new XmlDocument();
                                                XmlElement objRequestElement;
                                                StreamReader isoStreamReader = null;

                                                #region try-catch streamReader

                                                try
                                                {
                                                    isoStreamReader = new StreamReader(strDownloadFilepath);
                                                }
                                                catch (Exception exc)
                                                {
                                                    HandleException(userName, exc);
                                                    errorMessages.Add("There was an error trying to read file " + strDownloadFilepath);
                                                }

                                                #endregion

                                                if (isoStreamReader != null && isoStreamReader.BaseStream.Length > 0)

                                                {
                                                    isoStreamReader.BaseStream.Position = 0;
                                                    xmlDocument.Load(isoStreamReader);
                                                    objRequestElement = xmlDocument.DocumentElement;

                                                    isoStreamReader.Close();

                                                    if (objRequestElement != null)
                                                    {

                                                        try
                                                        {
                                                            ParseAndImportXML(xmlDocument.DocumentElement, fileName,
                                                                              indexes, monthStr,
                                                                              isFtpDownload, out lastDownLoadDate, out lastImportDate);

                                                        }
                                                        catch (ValidationException exc)
                                                        {
                                                            foreach (Exception myexc in exc.Exceptions)
                                                            {
                                                                errorMessages.Add(myexc.Source);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        errorMessages.Add("There was an error parsing file " + strDownloadFilepath);
                                                    } //objRequestElement != null
                                                }
                                                else
                                                {
                                                    errorMessages.Add("There was an error trying to read file " + strDownloadFilepath);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                HandleException(userName, ex);
                                                errorMessages.Add("There was an error parsing file " + strDownloadFilepath);
                                            }

                                            #endregion                                            
                                        }
                                        catch (WebException ex)
                                        {
                                            //HandleException(userName, ex);
                                            //errorMessage = "There was an exception during import. Exception Message: " + exc.Message;

                                            errorMessages.Add("There was an error while downloading file " + fileName +
                                                              ". Error was: " + ex.InnerException.Message);

                                            return 2;
                                        }
                                    }
                                }
                            }
                            reader.Close();
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                    HandleException(userName, ex);
                    //errorMessages.Add(ex.Message);
                    errorMessages.Add("There was an error while trying to connect to the ftp site. " + ex.Message);
                    return 3;
                }

                #endregion

                /*if (!filesInFolder && !filesInArchiveFolder)
                {
                    //information message
                    errorMessages.Add("There were no new files in main Forlder or in Archive Folder to import.");
                    return 1;
                }

                if(!filesInArchiveFolder)
                {
                    //information message
                    errorMessages.Add("There were no new files in Archive Folder to import.");
                    return 1;
                }

                if (!filesInFolder)
                {
                    //information message
                    errorMessages.Add("There were no new files in main Folder to import.");
                    return 1;
                }*/
            }

            var errorMsg = new List<string>();
            //parseExcelFiles(out errorMsg);

            return 0;
        }

        public int? LocalProcessingFileXML(XmlElement objRequestElement, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages)
        {
            errorMessages = new List<string>();
            lastDownLoadDate = DateTime.MinValue;
            lastImportDate = DateTime.MinValue;

            DataContext dataContext = null;
            string userName = null;

            userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

            if (SiAuto.Si.Level == Level.Debug)
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "ProcessingLocalXMLFile" + fileName };
            }
            
            bool isFtpDownload = false;

            try
            {
                ParseAndImportXML(objRequestElement, fileName, indexes, monthStr, isFtpDownload, out lastDownLoadDate, out lastImportDate);

            }
            catch (ValidationException exc)
            {
                foreach (Exception myexc in exc.Exceptions)
                {
                    errorMessages.Add(myexc.Source);

                }
            }
            catch (Exception exc)
            {

                HandleException(userName, exc);
                errorMessages.Add(exc.Message);

                return null;
            }
            try
            {
                var greekCultureInfo = new CultureInfo("el-GR", false);
                if (lastDownLoadDate == DateTime.MinValue)
                {
                    AppParameter lastDownloadedFileDate =
                           dataContext.AppParameters.Where(a => a.Code == "LAST_DOWNLOADED_BALTIC_XML_DATE").Single();
                    lastDownLoadDate = Convert.ToDateTime(lastDownloadedFileDate.Value, greekCultureInfo).Date;
                }

                if (lastImportDate == DateTime.MinValue)
                {
                    AppParameter lastImportFileDate =
                        dataContext.AppParameters.Where(a => a.Code == "LAST_IMPORT_INDEXES_VALUES_DATE").Single();
                    lastImportDate = Convert.ToDateTime(lastImportFileDate.Value, greekCultureInfo).Date;
                }
            }
            catch
            {

            }
            
            return 0;
        }

        public void ParseAndImportXML(XmlElement objRequestElement, string fileName, List<Index> indexes, Dictionary<string, int> monthStr, bool isFtpDownload, out DateTime lastDownLoadDate, out DateTime lastImportDate)
        {
            var objValidationException = new ValidationException();
            lastDownLoadDate = DateTime.MinValue;
            lastImportDate = DateTime.MinValue;

            string cmsRouteIdNodeValue = "";
            string routeAverageNodeValue = "";
            string archiveDateNodeValue = "";
            string reportDescNodeValue = "";

            DateTime archiveDate;
            var indexSpotValues = new List<IndexSpotValue>();
            var indexFfaValues = new List<IndexFFAValue>();
            var indexBoaValues = new List<IndexBOAValue>();
            IndexSpotValue indexSpotValue;
            IndexFFAValue indexFfaValue;
            IndexBOAValue indexBoaValue;
            Index index;

            var dateInfo = new DateTimeFormatInfo { ShortDatePattern = "yyyy-MM-dd" };            

            XmlNodeList nodeList = objRequestElement.GetElementsByTagName("RouteUpdate");
            foreach (XmlNode node in nodeList)
            {
                if (node["CMSRouteId"] != null)
                    cmsRouteIdNodeValue = node["CMSRouteId"].InnerText;
                if (node["RouteAverage"] != null)
                    routeAverageNodeValue = node["RouteAverage"].InnerText;
                if (node["ArchiveDate"] != null)
                    archiveDateNodeValue = node["ArchiveDate"].InnerText;
                if (node["ReportDesc"] != null)
                    reportDescNodeValue = node["ReportDesc"].InnerText;

                if (reportDescNodeValue == "NR")
                {
                    var myexc = new ApplicationException { Source = "Data of file '" + fileName + ", CMSRoouteId:" + cmsRouteIdNodeValue + "* ReportDesc Exception *" };
                    objValidationException.Exceptions.Add(myexc);
                    continue;
                }

                if (routeAverageNodeValue == "NR")
                {
                    var myexc = new ApplicationException { Source = "Data of file '" + fileName + ", CMSRoouteId:" + cmsRouteIdNodeValue + "* RouteAverage Exception *" };
                    objValidationException.Exceptions.Add(myexc);
                    continue;
                }


                //If there is no <ReportDesc> and the <CMSRouteId> does not contain "MON" or "CURQ" or "Q" or "CAL" then it is a SPOT VALUE
                if (string.IsNullOrEmpty(reportDescNodeValue) && !cmsRouteIdNodeValue.Contains("MON")
                    && !cmsRouteIdNodeValue.Contains("CURQ") && !cmsRouteIdNodeValue.Contains("Q") &&
                    !cmsRouteIdNodeValue.Contains("CAL"))
                {                    
                    index = indexes.Where(a => a.SpotMappingName == cmsRouteIdNodeValue).SingleOrDefault();
                    
                    if (index != null)
                    {
                        indexSpotValue = new IndexSpotValue()
                        {
                            IndexId = index.Id,
                            Date =
                                Convert.ToDateTime(archiveDateNodeValue, dateInfo).Date,
                            Rate =
                                Convert.ToDecimal(routeAverageNodeValue,
                                                  new CultureInfo("en-US"))
                        };
                        indexSpotValues.Add(indexSpotValue);
                    }
                    else
                        continue;
                }
                else
                {
                    DateTime firstDay;
                    DateTime lastDay;
                    string descrDate;

                    archiveDate = Convert.ToDateTime(archiveDateNodeValue, dateInfo);
                    var originalArchiveDate = archiveDate;

                    //Special case: if archive date is the last business day of month then the value refers to next month)
                    //var lastBusinessDate = GetLastBusinessDay(archiveDate.Year, archiveDate.Month);
                    //if (archiveDate.Date == lastBusinessDate.Date)
                    //    archiveDate = archiveDate.AddMonths(1);

                    #region CURMON

                    if (cmsRouteIdNodeValue.Contains("CURMON"))
                    {
                        descrDate = reportDescNodeValue.Trim(' ').Substring(0, 3);
                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || archiveDate.Month != monthStr[descrDate])
                            archiveDate = archiveDate.AddMonths(1);

                        firstDay = new DateTime(archiveDate.Year, archiveDate.Month, 1);
                        //first day of next month minus one day   
                        lastDay = firstDay.AddMonths(1).AddDays(-1);

                        string indexCode;
                        if (cmsRouteIdNodeValue.Contains("IV_"))
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("IV_", "");
                            indexCode = indexCode.Replace("CURMON", "");
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();                               
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("CURMON", "");
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }
                    #endregion CURMON

                    #region + X CURMON

                    else if (cmsRouteIdNodeValue.Contains("MON") && cmsRouteIdNodeValue.Contains("+"))
                    {
                        descrDate = reportDescNodeValue.Trim(' ').Substring(0, 3);

                        int plusIndex = cmsRouteIdNodeValue.IndexOf('+');
                        int periodValue = Convert.ToInt16(cmsRouteIdNodeValue.Substring(plusIndex + 1, 1));

                        DateTime date = archiveDate;
                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || date.AddMonths(periodValue).Month != monthStr[descrDate])
                            archiveDate = archiveDate.AddMonths(1);

                        firstDay = new DateTime(archiveDate.AddMonths(periodValue).Year,
                                                    archiveDate.AddMonths(periodValue).Month, 1);
                        lastDay = firstDay.AddMonths(1).AddDays(-1);

                        string indexCode;
                        if (cmsRouteIdNodeValue.Contains("IV_"))
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("IV_", "");
                            plusIndex = indexCode.IndexOf('+');
                            indexCode = indexCode.Substring(0, plusIndex);
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = cmsRouteIdNodeValue.Substring(0, plusIndex);
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }

                    #endregion

                    #region CURQ

                    else if (cmsRouteIdNodeValue.Contains("CURQ"))
                    {
                        if (!reportDescNodeValue.Contains("Q1") && !reportDescNodeValue.Contains("Q2") && !reportDescNodeValue.Contains("Q3") &&
                            !reportDescNodeValue.Contains("Q4"))
                            continue;

                        int intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;

                        ///////////////////////////////
                        int quarterDesc = Convert.ToInt32(reportDescNodeValue.Trim(' ').Substring(1, 1));

                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || intCurQuarter != quarterDesc)
                        {
                            archiveDate = archiveDate.AddMonths(1);
                            intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;
                        }
                        //////////////////////////////

                        firstDay = new DateTime(archiveDate.Year, 3 * intCurQuarter - 2, 1);
                        lastDay = new DateTime(firstDay.Year, firstDay.Month + 2, 1).AddMonths(1).AddDays(-1);

                        string indexCode;
                        if (cmsRouteIdNodeValue.Contains("IV_"))
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("IV_", "");
                            indexCode = indexCode.Replace("_CURQ", "");
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("CURQ", "");
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }

                    #endregion

                    #region + X CURQ

                    else if (cmsRouteIdNodeValue.Contains("Q") && cmsRouteIdNodeValue.Contains("+"))
                    {
                        int plusIndex = cmsRouteIdNodeValue.IndexOf('+');
                        int periodValue = Convert.ToInt16(cmsRouteIdNodeValue.Substring(plusIndex + 1, 1));

                        int intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;

                        var firstCurQuarterDate = new DateTime(archiveDate.Year, 3 * intCurQuarter - 2, 1);
                        DateTime lastCurQuarterDate = new DateTime(firstCurQuarterDate.Year, firstCurQuarterDate.Month + 2, 1).AddMonths(1).AddDays(-1);

                        DateTime nextQuarterDate = lastCurQuarterDate.AddMonths(periodValue * 3);
                        int quarter = ((nextQuarterDate.Month - 1) / 3) + 1;

                        firstDay = new DateTime(nextQuarterDate.Year, 3 * quarter - 2, 1);
                        lastDay = new DateTime(firstDay.Year, firstDay.Month + 2, 1).AddMonths(1).AddDays(-1);

                        ////////////////////////////////////
                        int quarterDesc = Convert.ToInt32(reportDescNodeValue.Trim(' ').Substring(1, 1));
                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || quarter != quarterDesc)
                        {
                            archiveDate = archiveDate.AddMonths(1);

                            intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;

                            firstCurQuarterDate = new DateTime(archiveDate.Year, 3 * intCurQuarter - 2, 1);
                            lastCurQuarterDate = new DateTime(firstCurQuarterDate.Year, firstCurQuarterDate.Month + 2, 1).AddMonths(1).AddDays(-1);

                            nextQuarterDate = lastCurQuarterDate.AddMonths(periodValue * 3);
                            quarter = ((nextQuarterDate.Month - 1) / 3) + 1;

                            firstDay = new DateTime(nextQuarterDate.Year, 3 * quarter - 2, 1);
                            lastDay = new DateTime(firstDay.Year, firstDay.Month + 2, 1).AddMonths(1).AddDays(-1);
                        }
                        /////////////////////////////////

                        string indexCode;
                        if (cmsRouteIdNodeValue.Contains("IV_"))
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("IV_", "");
                            plusIndex = indexCode.IndexOf('+');
                            indexCode = indexCode.Substring(0, plusIndex);
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = cmsRouteIdNodeValue.Substring(0, plusIndex);
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }
                    #endregion

                    #region + X CAL

                    else if (cmsRouteIdNodeValue.Contains("CAL") && cmsRouteIdNodeValue.Contains("+"))
                    {
                        int plusIndex = cmsRouteIdNodeValue.IndexOf('+');
                        int periodValue = Convert.ToInt16(cmsRouteIdNodeValue.Substring(plusIndex + 1, 1));

                        firstDay = new DateTime(archiveDate.AddYears(periodValue).Year, 1, 1);
                        lastDay = new DateTime(archiveDate.AddYears(periodValue).Year, 12, 31);

                        ////////////////////////////////////
                        string yearDesc = reportDescNodeValue.Trim(' ').Substring(4, 2);

                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || firstDay.Year.ToString().Substring(2, 2) != yearDesc)
                        {
                            archiveDate = archiveDate.AddMonths(1);
                            firstDay = new DateTime(archiveDate.AddYears(periodValue).Year, 1, 1);
                            lastDay = new DateTime(archiveDate.AddYears(periodValue).Year, 12, 31);
                        }
                        /////////////////////////////////

                        string indexCode;
                        if (cmsRouteIdNodeValue.Contains("IV_"))
                        {
                            indexCode = cmsRouteIdNodeValue.Replace("IV_", "");
                            plusIndex = indexCode.IndexOf('+');
                            indexCode = indexCode.Substring(0, plusIndex);
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Calendar,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = cmsRouteIdNodeValue.Substring(0, plusIndex);
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Calendar,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageNodeValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }

                    #endregion
                }
            }

            if (indexFfaValues.Count == 0 && indexSpotValues.Count == 0 && indexBoaValues.Count == 0)
            {
                var myexc = new ApplicationException { Source = "There were no data to import for file " + fileName };
                objValidationException.Exceptions.Add(myexc);
            }
            else
            {
                DateTime lastDownLoadDate2;
                DateTime lastImportDate2;
                //Insert Data
                var insertResults = InsertBalticValues(indexFfaValues, indexSpotValues, indexBoaValues, isFtpDownload, false, out lastDownLoadDate2, out lastImportDate2);

                lastDownLoadDate = lastDownLoadDate2;
                lastImportDate = lastImportDate2;

                //Insertion Results
                if (insertResults == null) ////Ffa/Spot data have already been inserted in database
                {
                    var myexc = new ApplicationException { Source = "Error occurred during saving '" + fileName + " in database." };
                    objValidationException.Exceptions.Add(myexc);
                }
                else if (insertResults == 1 || insertResults == 2) //file exists
                {
                    var myexc = new ApplicationException { Source = "Data of file '" + fileName + " already exist in database." };
                    objValidationException.Exceptions.Add(myexc);
                }
                else if (insertResults == 3)//Failure in average index values calculation. The values were inserted successfully.
                {
                    var myexc = new ApplicationException { Source = "File '" + fileName + " was imported successfully. However, the calculation of average index values failed." };
                    objValidationException.Exceptions.Add(myexc);
                }
                else if (insertResults == 0)//everything is OK
                {
                    var myexc = new ApplicationException { Source = "File '" + fileName + " was imported successfully." };
                    objValidationException.Exceptions.Add(myexc);
                }
            }


            if (objValidationException.Exceptions.Count > 0) throw objValidationException;

        }

        public int? LocalProcessingFileExcel(List<ParseExcelIndexValue> indexValues, string fileName, List<Index> indexes,
           Dictionary<string, int> monthStr, out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages)
        {
            errorMessages = new List<string>();
            lastDownLoadDate = DateTime.MinValue;
            lastImportDate = DateTime.MinValue;

            DataContext dataContext = null;
            string userName = null;            

            userName = GenericContext<ProgramInfo>.Current.Value.UserName;
            dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

            if (SiAuto.Si.Level == Level.Debug)
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "ProcessingLocalExcelFile" + fileName };
            }

            bool isFtpDownload = false;

            try
            {
                ParseAndImportExcel(indexValues, fileName, indexes, monthStr, isFtpDownload, out lastDownLoadDate, out lastImportDate);

            }
            catch (ValidationException exc)
            {
                foreach (Exception myexc in exc.Exceptions)
                {
                    errorMessages.Add(myexc.Source);

                }
            }
            catch (Exception exc)
            {

                HandleException(userName, exc);
                errorMessages.Add(exc.Message);

                return null;
            }

            try
            {
                var greekCultureInfo = new CultureInfo("el-GR", false);
                if (lastDownLoadDate == DateTime.MinValue)
                {
                    AppParameter lastDownloadedFileDate =
                           dataContext.AppParameters.Where(a => a.Code == "LAST_DOWNLOADED_BALTIC_XML_DATE").Single();
                    lastDownLoadDate = Convert.ToDateTime(lastDownloadedFileDate.Value, greekCultureInfo).Date;
                }

                if (lastImportDate == DateTime.MinValue)
                {
                    AppParameter lastImportFileDate =
                        dataContext.AppParameters.Where(a => a.Code == "LAST_IMPORT_INDEXES_VALUES_DATE").Single();
                    lastImportDate = Convert.ToDateTime(lastImportFileDate.Value, greekCultureInfo).Date;
                }
            }
            catch
            {

            }

            return 0;
        }

        public void ParseAndImportExcel(List<ParseExcelIndexValue> indexValues, string fileName, List<Index> indexes, Dictionary<string, int> monthStr, bool isFtpDownload, out DateTime lastDownLoadDate, out DateTime lastImportDate)
        {
            var objValidationException = new ValidationException();
            lastDownLoadDate = DateTime.MinValue;
            lastImportDate = DateTime.MinValue;

            string routeIdValue = "";
            string routeAverageValue = "";
            string archiveDateValue = "";
            string reportDescValue = "";

            DateTime archiveDate;
            var indexSpotValues = new List<IndexSpotValue>();
            var indexFfaValues = new List<IndexFFAValue>();
            var indexBoaValues = new List<IndexBOAValue>();
            IndexSpotValue indexSpotValue;
            IndexFFAValue indexFfaValue;
            IndexBOAValue indexBoaValue;
            Index index;

            //var dateInfo = new DateTimeFormatInfo { ShortDatePattern = "yyyy-MM-dd" };
            var dateInfo = new DateTimeFormatInfo { ShortDatePattern = "dd-MM-yyyy" };

            foreach (ParseExcelIndexValue indexValue in indexValues)
            {
                if (indexValue.RouteId != null)
                    routeIdValue = indexValue.RouteId;
                if (indexValue.ClosingPrice != null)
                    routeAverageValue = indexValue.ClosingPrice;
                if (indexValue.Date != null)
                    archiveDateValue = indexValue.Date;
                if (indexValue.ReportDesc != null)
                    reportDescValue = indexValue.ReportDesc;

                if (reportDescValue == "NR")
                {
                    var myexc = new ApplicationException { Source = "Data of file '" + fileName + ", CMSRoouteId:" + routeIdValue + "* ReportDesc Exception *" };
                    objValidationException.Exceptions.Add(myexc);
                    continue;
                }

                if (routeAverageValue == "NR")
                {
                    var myexc = new ApplicationException { Source = "Data of file '" + fileName + ", CMSRoouteId:" + routeIdValue + "* RouteAverage Exception *" };
                    objValidationException.Exceptions.Add(myexc);
                    continue;
                }


                //If there is no <ReportDesc> and the <CMSRouteId> does not contain "MON" or "CURQ" or "Q" or "CAL" then it is a SPOT VALUE
                if (string.IsNullOrEmpty(reportDescValue) && !routeIdValue.Contains("MON")
                    && !routeIdValue.Contains("CURQ") && !routeIdValue.Contains("Q") &&
                    !routeIdValue.Contains("CAL") && !routeIdValue.Contains("Physical"))
                {
                    index = indexes.Where(a => a.SpotMappingName == routeIdValue).SingleOrDefault();

                    if (index != null)
                    {
                        indexSpotValue = new IndexSpotValue()
                        {
                            IndexId = index.Id,
                            Date =
                                Convert.ToDateTime(archiveDateValue, dateInfo).Date,
                            Rate =
                                Convert.ToDecimal(routeAverageValue,
                                                  new CultureInfo("en-US"))
                        };
                        indexSpotValues.Add(indexSpotValue);
                    }
                    else
                        continue;
                }
                else
                {
                    DateTime firstDay;
                    DateTime lastDay;
                    string descrDate;

                    archiveDate = Convert.ToDateTime(archiveDateValue, dateInfo);
                    var originalArchiveDate = archiveDate;                   

                    #region CURMON

                    if (routeIdValue.Contains("CURMON"))
                    {
                        descrDate = reportDescValue.Trim(' ').Substring(0, 3);
                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || archiveDate.Month != monthStr[descrDate])
                            archiveDate = archiveDate.AddMonths(1);

                        firstDay = new DateTime(archiveDate.Year, archiveDate.Month, 1);
                        //first day of next month minus one day   
                        lastDay = firstDay.AddMonths(1).AddDays(-1);

                        string indexCode;
                        if (routeIdValue.Contains("IV_"))
                        {
                            indexCode = routeIdValue.Replace("IV_", "");
                            indexCode = indexCode.Replace("CURMON", "");
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = routeIdValue.Replace("CURMON", "");
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }
                    #endregion CURMON

                    #region + X CURMON

                    else if (routeIdValue.Contains("MON") && routeIdValue.Contains("+"))
                    {
                        descrDate = reportDescValue.Trim(' ').Substring(0, 3);

                        int plusIndex = routeIdValue.IndexOf('+');
                        int periodValue = Convert.ToInt16(routeIdValue.Substring(plusIndex + 1, 1));

                        DateTime date = archiveDate;
                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || date.AddMonths(periodValue).Month != monthStr[descrDate])
                            archiveDate = archiveDate.AddMonths(1);

                        firstDay = new DateTime(archiveDate.AddMonths(periodValue).Year,
                                                    archiveDate.AddMonths(periodValue).Month, 1);
                        lastDay = firstDay.AddMonths(1).AddDays(-1);

                        string indexCode;
                        if (routeIdValue.Contains("IV_"))
                        {
                            indexCode = routeIdValue.Replace("IV_", "");
                            plusIndex = indexCode.IndexOf('+');
                            indexCode = indexCode.Substring(0, plusIndex);
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = routeIdValue.Substring(0, plusIndex);
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Month,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }

                    #endregion

                    #region CURQ

                    else if (routeIdValue.Contains("CURQ"))
                    {
                        if (!reportDescValue.Contains("Q1") && !reportDescValue.Contains("Q2") && !reportDescValue.Contains("Q3") &&
                            !reportDescValue.Contains("Q4"))
                            continue;

                        int intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;

                        ///////////////////////////////
                        int quarterDesc = Convert.ToInt32(reportDescValue.Trim(' ').Substring(1, 1));

                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || intCurQuarter != quarterDesc)
                        {
                            archiveDate = archiveDate.AddMonths(1);
                            intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;
                        }
                        //////////////////////////////

                        firstDay = new DateTime(archiveDate.Year, 3 * intCurQuarter - 2, 1);
                        lastDay = new DateTime(firstDay.Year, firstDay.Month + 2, 1).AddMonths(1).AddDays(-1);

                        string indexCode;
                        if (routeIdValue.Contains("IV_"))
                        {
                            indexCode = routeIdValue.Replace("IV_", "");
                            indexCode = indexCode.Replace("_CURQ", "");
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = routeIdValue.Replace("CURQ", "");
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = 0,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }

                    #endregion

                    #region + X CURQ

                    else if (routeIdValue.Contains("Q") && routeIdValue.Contains("+"))
                    {
                        int plusIndex = routeIdValue.IndexOf('+');
                        int periodValue = Convert.ToInt16(routeIdValue.Substring(plusIndex + 1, 1));

                        int intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;

                        var firstCurQuarterDate = new DateTime(archiveDate.Year, 3 * intCurQuarter - 2, 1);
                        DateTime lastCurQuarterDate = new DateTime(firstCurQuarterDate.Year, firstCurQuarterDate.Month + 2, 1).AddMonths(1).AddDays(-1);

                        DateTime nextQuarterDate = lastCurQuarterDate.AddMonths(periodValue * 3);
                        int quarter = ((nextQuarterDate.Month - 1) / 3) + 1;

                        firstDay = new DateTime(nextQuarterDate.Year, 3 * quarter - 2, 1);
                        lastDay = new DateTime(firstDay.Year, firstDay.Month + 2, 1).AddMonths(1).AddDays(-1);

                        ////////////////////////////////////
                        int quarterDesc = Convert.ToInt32(reportDescValue.Trim(' ').Substring(1, 1));
                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || quarter != quarterDesc)
                        {
                            archiveDate = archiveDate.AddMonths(1);

                            intCurQuarter = ((archiveDate.Month - 1) / 3) + 1;

                            firstCurQuarterDate = new DateTime(archiveDate.Year, 3 * intCurQuarter - 2, 1);
                            lastCurQuarterDate = new DateTime(firstCurQuarterDate.Year, firstCurQuarterDate.Month + 2, 1).AddMonths(1).AddDays(-1);

                            nextQuarterDate = lastCurQuarterDate.AddMonths(periodValue * 3);
                            quarter = ((nextQuarterDate.Month - 1) / 3) + 1;

                            firstDay = new DateTime(nextQuarterDate.Year, 3 * quarter - 2, 1);
                            lastDay = new DateTime(firstDay.Year, firstDay.Month + 2, 1).AddMonths(1).AddDays(-1);
                        }
                        /////////////////////////////////

                        string indexCode;
                        if (routeIdValue.Contains("IV_"))
                        {
                            indexCode = routeIdValue.Replace("IV_", "");
                            plusIndex = indexCode.IndexOf('+');
                            indexCode = indexCode.Substring(0, plusIndex);
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = routeIdValue.Substring(0, plusIndex);
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Quarter,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }
                    #endregion

                    #region + X CAL

                    else if (routeIdValue.Contains("CAL") && routeIdValue.Contains("+"))
                    {
                        int plusIndex = routeIdValue.IndexOf('+');
                        int periodValue = Convert.ToInt16(routeIdValue.Substring(plusIndex + 1, 1));

                        firstDay = new DateTime(archiveDate.AddYears(periodValue).Year, 1, 1);
                        lastDay = new DateTime(archiveDate.AddYears(periodValue).Year, 12, 31);

                        ////////////////////////////////////
                        string yearDesc = reportDescValue.Trim(' ').Substring(4, 2);

                        if (archiveDate.Date == GetLastBusinessDay(archiveDate.Year, archiveDate.Date.Month) || firstDay.Year.ToString().Substring(2, 2) != yearDesc)
                        {
                            archiveDate = archiveDate.AddMonths(1);
                            firstDay = new DateTime(archiveDate.AddYears(periodValue).Year, 1, 1);
                            lastDay = new DateTime(archiveDate.AddYears(periodValue).Year, 12, 31);
                        }
                        /////////////////////////////////

                        string indexCode;
                        if (routeIdValue.Contains("IV_"))
                        {
                            indexCode = routeIdValue.Replace("IV_", "");
                            plusIndex = indexCode.IndexOf('+');
                            indexCode = indexCode.Substring(0, plusIndex);
                            if (indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                            }
                            index = indexes.Where(a => a.BOAMappingName == indexCode).SingleOrDefault();
                            if (index != null)
                            {
                                indexBoaValue = new IndexBOAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Calendar,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexBoaValues.Add(indexBoaValue);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            indexCode = routeIdValue.Substring(0, plusIndex);
                            index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                            //If Index Code contains underscore character '_', then index code is the string before underscore.
                            if (index == null && indexCode.Contains("_"))
                            {
                                indexCode = indexCode.Split('_').First();
                                Index _index = indexes.Where(a => a.FFAMappingName == indexCode).SingleOrDefault();
                                if (_index != null)
                                    index = _index;
                            }
                            if (index != null)
                            {
                                indexFfaValue = new IndexFFAValue()
                                {
                                    Date = originalArchiveDate.Date,
                                    IndexId = index.Id,
                                    PeriodFrom = firstDay.Date,
                                    PeriodTo = lastDay.Date,
                                    PeriodType = PeriodTypeEnum.Calendar,
                                    PeriodValue = periodValue,
                                    Rate =
                                        Convert.ToDecimal(routeAverageValue,
                                                          new CultureInfo("en-US"))
                                };

                                indexFfaValues.Add(indexFfaValue);
                            }
                            else
                                continue;
                        }
                    }

                    #endregion
                }
            }

            if (indexFfaValues.Count == 0 && indexSpotValues.Count == 0 && indexBoaValues.Count == 0)
            {
                var myexc = new ApplicationException { Source = "There were no data to import for file " + fileName };
                objValidationException.Exceptions.Add(myexc);
            }
            else
            {
                DateTime lastDownLoadDate2;
                DateTime lastImportDate2;
                //Insert Data
                var insertResults = InsertBalticValues(indexFfaValues, indexSpotValues, indexBoaValues, isFtpDownload, true, out lastDownLoadDate2, out lastImportDate2);

                lastDownLoadDate = lastDownLoadDate2;
                lastImportDate = lastImportDate2;

                //Insertion Results
                if (insertResults == null) ////Ffa/Spot data have already been inserted in database
                {
                    var myexc = new ApplicationException { Source = "Error occurred during saving '" + fileName + " in database." };
                    objValidationException.Exceptions.Add(myexc);
                }
                else if (insertResults == 1 || insertResults == 2) //file exists
                {
                    var myexc = new ApplicationException { Source = "Data of file '" + fileName + " already exist in database." };
                    objValidationException.Exceptions.Add(myexc);
                }
                else if (insertResults == 3)//Failure in average index values calculation. The values were inserted successfully.
                {
                    var myexc = new ApplicationException { Source = "File '" + fileName + " was imported successfully. However, the calculation of average index values failed." };
                    objValidationException.Exceptions.Add(myexc);
                }
                else if (insertResults == 0)//everything is OK
                {
                    var myexc = new ApplicationException { Source = "File '" + fileName + " was imported successfully." };
                    objValidationException.Exceptions.Add(myexc);
                }
            }


            if (objValidationException.Exceptions.Count > 0) throw objValidationException;

        }

        public int? BalticExchangeInputDataInitializationData(out List<Index> indexes, out List<AppParameter> appParameters)
        {
            indexes = null;
            appParameters = null;
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "BeginBalticExchangeInputDataInitializationData: " };
                }

                const string ftpArchiveUri = "BALTIC_EXCHANGE_FTP_ARCHIVE";
                const string ftpUri = "BALTIC_EXCHANGE_FTP";
                const string ftpUsername = "BALTIC_EXCHANGE_FTP_USERNAME";
                const string ftpPassword = "BALTIC_EXCHANGE_FTP_PASSWORD";
                const string ftpXmlFolder = "BALTIC_EXHANGE_XML_FOLDER";
                const string ftplLastDownloadDate = "LAST_DOWNLOADED_BALTIC_XML_DATE";
                const string ftpNasdaqServer = "NASDAQ_FTP";
                const string ftpNasdaqServerPort = "NASDAQ_FTP_PORT";
                const string ftpNasdaqSpanPath = "NASDAQ_FTP_SPAN";
                const string ftpNasdaqVolCorPath = "NASDAQ_FTP_VOLCOR";
                const string lastImportIndexesValuesDate = "LAST_IMPORT_INDEXES_VALUES_DATE";


                indexes = Queries.GetAllIndexes(dataContext).ToList();

                var appParamCodes = new List<string>() { ftpArchiveUri, ftpUri, ftpUsername, ftpPassword, ftpXmlFolder, ftplLastDownloadDate, ftpNasdaqServer, ftpNasdaqServerPort, ftpNasdaqSpanPath, ftpNasdaqVolCorPath, lastImportIndexesValuesDate };
                appParameters = (from objAppParameter in dataContext.AppParameters
                                 where appParamCodes.Contains(objAppParameter.Code)
                                 select objAppParameter).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? InsertBalticValues(List<IndexFFAValue> indexFfaValues, List<IndexSpotValue> indexSpotValues, List<IndexBOAValue> indexBoaValues, bool isFtpDownload, bool canUpdateValues, out DateTime lastDownLoadDate, out DateTime lastImportDate)
        {
            DataContext dataContext = null;
            string userName = null;
            lastDownLoadDate = DateTime.MinValue;
            lastImportDate = DateTime.MinValue;
            IndexSpotValue lastAverageIndexSpotValue = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "InsertBalticValues: " };
                }

                //#region Update last download date parameter

                bool ffaExist;
                bool boaExist;
                bool spotExist;
                DateTime now = DateTime.Now;
                CultureInfo greekCultureInfo = new CultureInfo("el-GR", false);                

                #region Check values do not already exist in database

                //ONLY FOR XML : Check that index values have not already been inserted in database for the specified archive date.
                if(!canUpdateValues)
                {
                    if (indexFfaValues.Count > 0)
                    {
                        DateTime ffaDate = indexFfaValues[0].Date;

                        ffaExist = (from objFFaValue in dataContext.IndexFFAValues
                                    where objFFaValue.Date == ffaDate
                                          && indexFfaValues.Select(a => a.IndexId).Distinct().Contains(objFFaValue.IndexId)
                                    select objFFaValue).Any();

                        if (ffaExist)
                        {
                            dataContext.SubmitChanges();
                            return 1;
                        }
                    }
                    //Check that spot data have not already been inserted in database for the specified archive date.
                    if (indexSpotValues.Count > 0)
                    {
                        DateTime spotDate = indexSpotValues[0].Date;

                        spotExist = (from objSpotValue in dataContext.IndexSpotValues
                                     where objSpotValue.Date == spotDate
                                           && indexSpotValues.Select(a => a.IndexId).Distinct().Contains(objSpotValue.IndexId)
                                     select objSpotValue).Any();
                        dataContext.SubmitChanges();
                        if (spotExist)
                        {
                            dataContext.SubmitChanges();
                            return 2;
                        }
                    }
                    //Check that boa data have not already been inserted in database for the specified archive date.
                    if (indexBoaValues.Count > 0)
                    {
                        DateTime boaDate = indexBoaValues[0].Date;

                        boaExist = (from objBoaValue in dataContext.IndexBOAValues
                                    where objBoaValue.Date == boaDate
                                          && indexBoaValues.Select(a => a.IndexId).Distinct().Contains(objBoaValue.IndexId)
                                    select objBoaValue).Any();

                        if (boaExist)
                        {
                            dataContext.SubmitChanges();
                            return 1;
                        }
                    }
                }

                #endregion                

                #region Insert Values               

                //delete existing values if exist in database
                if(indexFfaValues.Count>0)
                {
                    var indexDates = indexFfaValues.Select(i => new { i.IndexId, i.Date }).Distinct();
                    foreach(var indexDate in indexDates)
                    {
                        var existingFFAValues = dataContext.IndexFFAValues.Where(a => a.Date == indexDate.Date && a.IndexId == indexDate.IndexId).ToList();
                        if(existingFFAValues.Count > 0)
                        {
                            dataContext.IndexFFAValues.DeleteAllOnSubmit(existingFFAValues);
                        }
                    }
                }

                if (indexSpotValues.Count > 0)
                {
                    var indexDates = indexSpotValues.Select(i => new { i.IndexId, i.Date }).Distinct();
                    foreach (var indexDate in indexDates)
                    {
                        var existingSpotValues = dataContext.IndexSpotValues.Where(a => a.Date == indexDate.Date && a.IndexId == indexDate.IndexId).ToList();
                        if (existingSpotValues.Count > 0)
                        {
                            dataContext.IndexSpotValues.DeleteAllOnSubmit(existingSpotValues);
                        }
                    }
                }

                if (indexBoaValues.Count > 0)
                {
                    var indexDates = indexBoaValues.Select(i => new { i.IndexId, i.Date }).Distinct();
                    foreach (var indexDate in indexDates)
                    {
                        var existingBOAValues = dataContext.IndexBOAValues.Where(a => a.Date == indexDate.Date && a.IndexId == indexDate.IndexId).ToList();
                        if (existingBOAValues.Count > 0)
                        {
                            dataContext.IndexBOAValues.DeleteAllOnSubmit(existingBOAValues);
                        }
                    }
                }

                foreach (var indexFfaValue in indexFfaValues)
                {
                    indexFfaValue.Id = dataContext.GetNextId(typeof(IndexFFAValue));
                    indexFfaValue.Crd = now;
                    indexFfaValue.Cruser = userName;
                    indexFfaValue.Rate = Convert.ToDecimal(indexFfaValue.Rate,
                                                           SessionRegistry.ServerSessionRegistry.ServerCultureInfo);
                    dataContext.IndexFFAValues.InsertOnSubmit(indexFfaValue);
                }

                foreach (var indexSpotValue in indexSpotValues)
                {
                    indexSpotValue.Id = dataContext.GetNextId(typeof(IndexSpotValue));
                    indexSpotValue.Crd = now;
                    indexSpotValue.Cruser = userName;
                    indexSpotValue.Rate = Convert.ToDecimal(indexSpotValue.Rate,
                                                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo);
                    dataContext.IndexSpotValues.InsertOnSubmit(indexSpotValue);
                }                

                foreach (var indexBoaValue in indexBoaValues)
                {
                    indexBoaValue.Id = dataContext.GetNextId(typeof(IndexBOAValue));
                    indexBoaValue.Crd = now;
                    indexBoaValue.Cruser = userName;
                    indexBoaValue.Rate = Convert.ToDecimal(indexBoaValue.Rate,
                                                           SessionRegistry.ServerSessionRegistry.ServerCultureInfo);
                    dataContext.IndexBOAValues.InsertOnSubmit(indexBoaValue);
                }

                #endregion

                #region Update last download date parameter

                DateTime archiveDate = Convert.ToDateTime(DateTime.MinValue, greekCultureInfo);
                AppParameter lastDownloadedFileDate =
                      dataContext.AppParameters.Where(a => a.Code == "LAST_DOWNLOADED_BALTIC_XML_DATE").Single();
                lastDownLoadDate = Convert.ToDateTime(lastDownloadedFileDate.Value, greekCultureInfo).Date;
                if (isFtpDownload)
                {
                    if (indexFfaValues.Count > 0)
                    {
                        archiveDate = Convert.ToDateTime(indexFfaValues[0].Date, greekCultureInfo);
                    }
                    if (indexSpotValues.Count > 0)
                    {
                        archiveDate = Convert.ToDateTime(indexSpotValues[0].Date, greekCultureInfo);
                    }
                    else if (indexBoaValues.Count > 0)
                    {
                        archiveDate = Convert.ToDateTime(indexBoaValues[0].Date, greekCultureInfo);
                    }

                    if (archiveDate.Date > Convert.ToDateTime(lastDownloadedFileDate.Value, greekCultureInfo).Date)
                    {
                        lastDownloadedFileDate.Value = Convert.ToDateTime(archiveDate.Date, greekCultureInfo).ToString(greekCultureInfo.DateTimeFormat.ShortDatePattern);
                        lastDownLoadDate = archiveDate.Date;
                    }
                    else
                    {
                        lastDownLoadDate = Convert.ToDateTime(lastDownloadedFileDate.Value, greekCultureInfo).Date;
                    }
                }

                AppParameter lastImportFileDate =
                    dataContext.AppParameters.Where(a => a.Code == "LAST_IMPORT_INDEXES_VALUES_DATE").Single();                

                if (indexFfaValues.Count > 0)
                {
                    archiveDate = Convert.ToDateTime(indexFfaValues.Select(a=>a.Date).Max(), greekCultureInfo);
                }
                else if (indexSpotValues.Count > 0)
                {
                    archiveDate = Convert.ToDateTime(indexSpotValues.Select(a => a.Date).Max(), greekCultureInfo);
                }
                else if (indexBoaValues.Count > 0)
                {
                    archiveDate = Convert.ToDateTime(indexBoaValues.Select(a => a.Date).Max(), greekCultureInfo);
                }

                if (archiveDate.Date > Convert.ToDateTime(lastImportFileDate.Value, greekCultureInfo).Date)
                {
                    lastImportFileDate.Value = Convert.ToDateTime(archiveDate.Date, greekCultureInfo).ToString(greekCultureInfo.DateTimeFormat.ShortDatePattern);
                    lastImportDate = archiveDate.Date;
                }
                else
                {
                    lastImportDate = Convert.ToDateTime(lastImportFileDate.Value, greekCultureInfo).Date;
                }
                

                #endregion

                dataContext.SubmitChanges();

                indexSpotValues = indexSpotValues.OrderBy(a => a.Date).ToList();
                if (indexSpotValues.Count > 0)
                {
                    if (canUpdateValues)
                    {
                        var indexesOfSpotValues = indexSpotValues.Select(a => a.IndexId).Distinct().ToList();
                        bool mustChangeAssociatedSpotValues = dataContext.AverageIndexesAssocs.Any(a => indexesOfSpotValues.Contains(a.IndexId)); 
                        if(mustChangeAssociatedSpotValues)
                        {
                            var allDates = new List<DateTime>();
                            var minimumImportDate = indexSpotValues.Select(a => a.Date).Min();
                            var maximumImportDate = indexSpotValues.Select(a => a.Date).Max();
                            allDates = dataContext.IndexSpotValues.Where(a => a.Date >= minimumImportDate && a.Date <= maximumImportDate).Select(b => b.Date).Distinct().ToList();

                            foreach (DateTime date in allDates)
                            {
                                List<AverageIndexesAssoc> indexesAssocs =
                                    (from objAverageIndexAssoc in dataContext.AverageIndexesAssocs
                                     from objIndex in dataContext.Indexes
                                     where objAverageIndexAssoc.AvgIndexId == objIndex.Id && objIndex.IsAssocIndex
                                     && objIndex.Status == ActivationStatusEnum.Active
                                     select objAverageIndexAssoc).ToList();

                                var avgSpotValuesToDelete = new List<IndexSpotValue>();
                                var avgSpotValues = new List<IndexSpotValue>();
                                foreach (AverageIndexesAssoc assoc in indexesAssocs)
                                {
                                    var index = dataContext.Indexes.Single(n => n.Id == assoc.AvgIndexId);

                                    if (avgSpotValues.Select(a => a.IndexId).Contains(assoc.AvgIndexId)) continue;

                                    List<AverageIndexesAssoc> routesWeights =
                                        indexesAssocs.Where(a => a.AvgIndexId == assoc.AvgIndexId).ToList();

                                    List<IndexSpotValue> routesValues =
                                        dataContext.IndexSpotValues.Where(
                                            a =>
                                            routesWeights.Select(b => b.IndexId).Contains(a.IndexId) && a.Date == date.Date)
                                            .
                                            ToList();

                                    //Check that there are values for all routes
                                    if (routesValues.Count != routesWeights.Count)
                                    {
                                        SiAuto.Si.GetSession(userName).LogError("Average Index Id: " + assoc.AvgIndexId +
                                                                                ". The number of route values is not the same as the number of associated route indexes.");
                                        return 3;
                                    }

                                    decimal rate = routesWeights.Aggregate<AverageIndexesAssoc, decimal>(0,
                                                                                                       (current,
                                                                                                        routeWeight) =>
                                                                                                       current +
                                                                                                       routesValues.Single(
                                                                                                           a => a.IndexId ==
                                                                                                                routeWeight.
                                                                                                                    IndexId).
                                                                                                           Rate *
                                                                                                       routeWeight.Weight /
                                                                                                       100);

                                    if (index.AssocOffset != null || index.AssocOffset != 0)
                                        rate = rate + (decimal)index.AssocOffset;

                                    //check if spot value for Is Assoc index has already been calculated

                                    var existingIsAssocSpotValue = dataContext.IndexSpotValues.SingleOrDefault(a => a.Date == date.Date && a.IndexId == assoc.AvgIndexId);
                                    if (existingIsAssocSpotValue != null)
                                    {
                                        avgSpotValuesToDelete.Add(existingIsAssocSpotValue);
                                    }

                                    var newIndexSpotValue = new IndexSpotValue
                                    {
                                        Id = dataContext.GetNextId(typeof(IndexSpotValue)),
                                        IndexId = assoc.AvgIndexId,
                                        Date = date.Date,
                                        Crd = now,
                                        Cruser = userName,
                                        Rate = rate
                                    };
                                    avgSpotValues.Add(newIndexSpotValue);

                                    var derivedIndexes = dataContext.Indexes.Where(a => a.DerivedFromIndexId == assoc.AvgIndexId).ToList();
                                    if (derivedIndexes.Count > 0)
                                    {
                                        foreach (var derivedIndex in derivedIndexes)
                                        {
                                            var existingDerivedIndexSpotValue = dataContext.IndexSpotValues.SingleOrDefault(a => a.Date == date.Date && a.IndexId == derivedIndex.Id);
                                            if (existingDerivedIndexSpotValue != null)
                                            {
                                                avgSpotValuesToDelete.Add(existingDerivedIndexSpotValue);
                                            }

                                            newIndexSpotValue = new IndexSpotValue
                                            {
                                                Id = dataContext.GetNextId(typeof(IndexSpotValue)),
                                                IndexId = derivedIndex.Id,
                                                Date = date.Date,
                                                Crd = now,
                                                Cruser = userName,
                                                Rate = rate + (derivedIndex.DerivedOffset ?? 0)
                                            };
                                            avgSpotValues.Add(newIndexSpotValue);
                                        }
                                    }
                                }
                                dataContext.IndexSpotValues.DeleteAllOnSubmit(avgSpotValuesToDelete);
                                dataContext.IndexSpotValues.InsertAllOnSubmit(avgSpotValues);

                            }
                            dataContext.SubmitChanges();
                        }
                    }
                    else
                    {
                        //Find the most recent average spot value                   
                        lastAverageIndexSpotValue =
                           (from objIndexSpotValue in dataContext.IndexSpotValues
                            from objIndex in dataContext.Indexes
                            where objIndex.IsAssocIndex &&
                                  objIndex.Id == objIndexSpotValue.IndexId && objIndex.Status == ActivationStatusEnum.Active
                            select objIndexSpotValue).OrderByDescending(
                                b => b.Date).FirstOrDefault();

                        var allDates = new List<DateTime>();

                        allDates = dataContext.IndexSpotValues.Where(a => a.Date > lastAverageIndexSpotValue.Date && a.Date <= indexSpotValues[0].Date).Select(b => b.Date).Distinct().ToList();

                        foreach (DateTime date in allDates)
                        {
                            List<AverageIndexesAssoc> indexesAssocs =
                                (from objAverageIndexAssoc in dataContext.AverageIndexesAssocs
                                 from objIndex in dataContext.Indexes
                                 where objAverageIndexAssoc.AvgIndexId == objIndex.Id && objIndex.IsAssocIndex
                                 && objIndex.Status == ActivationStatusEnum.Active
                                 select objAverageIndexAssoc).ToList();

                            var avgSpotValuesToDelete = new List<IndexSpotValue>();
                            var avgSpotValues = new List<IndexSpotValue>();
                            foreach (AverageIndexesAssoc assoc in indexesAssocs)
                            {
                                var index = dataContext.Indexes.Single(n => n.Id == assoc.AvgIndexId);

                                if (avgSpotValues.Select(a => a.IndexId).Contains(assoc.AvgIndexId)) continue;

                                List<AverageIndexesAssoc> routesWeights =
                                    indexesAssocs.Where(a => a.AvgIndexId == assoc.AvgIndexId).ToList();

                                List<IndexSpotValue> routesValues =
                                    dataContext.IndexSpotValues.Where(
                                        a =>
                                        routesWeights.Select(b => b.IndexId).Contains(a.IndexId) && a.Date == date.Date)
                                        .
                                        ToList();

                                //Check that there are values for all routes
                                if (routesValues.Count != routesWeights.Count)
                                {
                                    SiAuto.Si.GetSession(userName).LogError("Average Index Id: " + assoc.AvgIndexId +
                                                                            ". The number of route values is not the same as the number of associated route indexes.");
                                    return 3;
                                }

                                decimal rate = routesWeights.Aggregate<AverageIndexesAssoc, decimal>(0,
                                                                                                   (current,
                                                                                                    routeWeight) =>
                                                                                                   current +
                                                                                                   routesValues.Single(
                                                                                                       a => a.IndexId ==
                                                                                                            routeWeight.
                                                                                                                IndexId).
                                                                                                       Rate *
                                                                                                   routeWeight.Weight /
                                                                                                   100);

                                if (index.AssocOffset != null || index.AssocOffset != 0)
                                    rate = rate + (decimal)index.AssocOffset;

                                //check if spot value for Is Assoc index has already been calculated
                                var existingIsAssocSpotValue = dataContext.IndexSpotValues.SingleOrDefault(a => a.Date == date.Date && a.IndexId == assoc.AvgIndexId);
                                if (existingIsAssocSpotValue != null)
                                {
                                    avgSpotValuesToDelete.Add(existingIsAssocSpotValue);
                                }

                                var newIndexSpotValue = new IndexSpotValue
                                {
                                    Id = dataContext.GetNextId(typeof(IndexSpotValue)),
                                    IndexId = assoc.AvgIndexId,
                                    Date = date.Date,
                                    Crd = now,
                                    Cruser = userName,
                                    Rate = rate
                                };
                                avgSpotValues.Add(newIndexSpotValue);

                                var derivedIndexes = dataContext.Indexes.Where(a => a.DerivedFromIndexId == assoc.AvgIndexId).ToList();
                                if (derivedIndexes.Count > 0)
                                {
                                    foreach (var derivedIndex in derivedIndexes)
                                    {
                                        var existingDerivedIndexSpotValue = dataContext.IndexSpotValues.SingleOrDefault(a => a.Date == date.Date && a.IndexId == derivedIndex.Id);
                                        if (existingDerivedIndexSpotValue != null)
                                        {
                                            avgSpotValuesToDelete.Add(existingDerivedIndexSpotValue);
                                        }

                                        newIndexSpotValue = new IndexSpotValue
                                        {
                                            Id = dataContext.GetNextId(typeof(IndexSpotValue)),
                                            IndexId = derivedIndex.Id,
                                            Date = date.Date,
                                            Crd = now,
                                            Cruser = userName,
                                            Rate = rate + (derivedIndex.DerivedOffset ?? 0)
                                        };
                                        avgSpotValues.Add(newIndexSpotValue);
                                    }
                                }
                            }
                            dataContext.IndexSpotValues.DeleteAllOnSubmit(avgSpotValuesToDelete);
                            dataContext.IndexSpotValues.InsertAllOnSubmit(avgSpotValues);
                        }
                        dataContext.SubmitChanges();
                    }
                
                }
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEInstallments(long loanId, List<LoanPrincipalInstallment> loanPrincipalInstallments, List<LoanInterestInstallment> loanInterestInstallments)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AddEditInstallments: " };
                }

                DateTime now = DateTime.Now.Date;

                List<LoanPrincipalInstallment> existingPrincipalInstallments =
                    (from objPrincipalInstallment in dataContext.LoanPrincipalInstallments
                     from objTranche in dataContext.LoanTranches
                     where objTranche.LoanId == loanId
                           && objTranche.Id == objPrincipalInstallment.LoanTrancheId
                     select objPrincipalInstallment).ToList();

                List<LoanInterestInstallment> exisitingInterestInstallments =
                    (from objInterestInstallment in dataContext.LoanInterestInstallments
                     from objTranche in dataContext.LoanTranches
                     where objTranche.LoanId == loanId
                           && objTranche.Id == objInterestInstallment.LoanTrancheId
                     select objInterestInstallment).ToList();

                dataContext.LoanPrincipalInstallments.DeleteAllOnSubmit(existingPrincipalInstallments);
                dataContext.LoanInterestInstallments.DeleteAllOnSubmit(exisitingInterestInstallments);

                foreach (LoanPrincipalInstallment loanPrincipalInstallment in loanPrincipalInstallments)
                {
                    loanPrincipalInstallment.Id = dataContext.GetNextId(typeof(LoanPrincipalInstallment));
                    dataContext.LoanPrincipalInstallments.InsertOnSubmit(loanPrincipalInstallment);
                }

                foreach (LoanInterestInstallment loanInterestInstallment in loanInterestInstallments)
                {
                    loanInterestInstallment.Id = dataContext.GetNextId(typeof(LoanInterestInstallment));
                    dataContext.LoanInterestInstallments.InsertOnSubmit(loanInterestInstallment);
                }

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AERiskParametersInitializationData(out List<Index> indexes, out List<RiskVolatility> riskVolatilities, out List<RiskCorrelation> riskCorrelations)
        {
            indexes = null;
            riskCorrelations = null;
            riskVolatilities = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AERiskParametersInitializationData: " };
                }

                indexes = Queries.GetAllIndexes(dataContext).ToList();
                riskVolatilities = Queries.GetAllRiskVolatilities(dataContext).ToList();
                riskCorrelations = Queries.GetAllRiskCorrelations(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AERiskParameteres(List<RiskVolatility> riskVolatilities, List<RiskCorrelation> riskCorrelations)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AERiskParameteres: " };
                }

                DateTime now = DateTime.Now.Date;

                List<RiskVolatility> existingRiskVolatilities = dataContext.RiskVolatilities.ToList();
                List<RiskCorrelation> existingRiskCorrelations = dataContext.RiskCorrelations.ToList();

                dataContext.RiskVolatilities.DeleteAllOnSubmit(existingRiskVolatilities);
                dataContext.RiskCorrelations.DeleteAllOnSubmit(existingRiskCorrelations);

                foreach (RiskVolatility riskVolatility in riskVolatilities)
                {
                    riskVolatility.Id = dataContext.GetNextId(typeof(RiskVolatility));
                    dataContext.RiskVolatilities.InsertOnSubmit(riskVolatility);
                }
                foreach (RiskCorrelation riskCorrelation in riskCorrelations)
                {
                    riskCorrelation.Id = dataContext.GetNextId(typeof(RiskCorrelation));
                    dataContext.RiskCorrelations.InsertOnSubmit(riskCorrelation);
                }

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVHiearchyInitializationData(out List<HierarchyNode> hierarchyNodes, out List<HierarchyNodeType> hierarchyNodeTypes)
        {
            hierarchyNodes = null;
            hierarchyNodeTypes = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVHiearchyInitializationData: " };
                }

                hierarchyNodes = Queries.GetHierarchyNodes(dataContext).ToList();
                hierarchyNodeTypes = Queries.GetHierarchyNodeTypes(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVHierarchyNodes(List<HierarchyNode> hierarchyNodes)
        {
            DataContext dataContext = null;
            string userName = null;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVHierarchyNodes: " };
                }

                List<HierarchyNode> existingHierarchyNodes = dataContext.HierarchyNodes.ToList();
                dataContext.HierarchyNodes.DeleteAllOnSubmit(existingHierarchyNodes);

                foreach (var hierarchyNode in hierarchyNodes)
                {
                    List<HierarchyNode> childNodes = hierarchyNodes.Where(a => a.ParentId == hierarchyNode.Id).ToList();
                    long newId = dataContext.GetNextId(typeof(HierarchyNode));
                    foreach (var childNode in childNodes)
                    {
                        childNode.ParentId = newId;
                    }
                    hierarchyNode.Id = newId;
                    dataContext.HierarchyNodes.InsertOnSubmit(hierarchyNode);
                }

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVCashFlowInitializationData(out List<CashFlowModel> cashFlowModels, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowGroupItem> cashFlowGroupsItems)
        {
            cashFlowModels = null;
            cashFlowGroups = null;
            cashFlowItems = null;
            cashFlowModelGroups = null;
            cashFlowGroupsItems = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "AEVCashFlowInitializationData: " };
                }

                List<CashFlowModel> models = Queries.GetCashFlowModels(dataContext).ToList();
                List<CashFlowGroup> groups = Queries.GetCashFlowGroups(dataContext).ToList();
                List<CashFlowItem> items = Queries.GetCashFlowItems(dataContext).ToList();

                List<CashFlowModelGroup> modelsGroups = Queries.GetCashFlowModelGroups(dataContext).ToList();
                List<CashFlowGroupItem> groupsItems = Queries.GetCashFlowGroupItems(dataContext).ToList();

                cashFlowModels = models;
                cashFlowGroups = groups;
                cashFlowItems = items;
                cashFlowModelGroups = modelsGroups;
                cashFlowGroupsItems = groupsItems;

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVCashFlowItemInitializationData(out List<Currency> currencies)
        {
            currencies = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVCashFlowItemInitializationData: " };
                }

                currencies = Queries.GetCurrenciesAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetCashFlowItems(out List<CashFlowItem> cashFlowItems)
        {
            cashFlowItems = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetCashFlowItems: " };
                }

                List<CashFlowItem> items = Queries.GetCashFlowItems(dataContext).ToList();

                cashFlowItems = items;

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AECashFlowEntities(List<CashFlowItem> cashFlowItems, List<CashFlowGroup> cashFlowGroups, List<CashFlowModel> cashFlowModels, List<CashFlowGroupItem> cashFlowGroupItems, List<CashFlowModelGroup> cashFlowModelsGroups)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AECashFlowEntities: " };
                }

                DateTime now = DateTime.Now.Date;

                List<long> existingCashFlowItemIds = dataContext.CashFlowItems.Select(a => a.Id).ToList();
                foreach (long cashFlowItemId in existingCashFlowItemIds)
                {
                    if (!cashFlowItems.Select(a => a.Id).Contains(cashFlowItemId))
                    {
                        var cashFlowItemToDelete = dataContext.CashFlowItems.Where(a => a.Id == cashFlowItemId).Single();
                        dataContext.CashFlowItems.DeleteOnSubmit(cashFlowItemToDelete);

                        List<CashFlowGroupItem> groupItemsToDelete =
                            dataContext.CashFlowGroupsItems.Where(a => a.ItemId == cashFlowItemToDelete.Id).ToList();
                        dataContext.CashFlowGroupsItems.DeleteAllOnSubmit(groupItemsToDelete);
                    }
                }

                List<long> existingCashFlowGroupIds = dataContext.CashFlowGroups.Select(a => a.Id).ToList();
                foreach (long cashFlowGroupId in existingCashFlowGroupIds)
                {
                    if (!cashFlowGroups.Select(a => a.Id).Contains(cashFlowGroupId))
                    {
                        var cashFlowGroupToDelete = dataContext.CashFlowGroups.Where(a => a.Id == cashFlowGroupId).Single();
                        dataContext.CashFlowGroups.DeleteOnSubmit(cashFlowGroupToDelete);

                        List<CashFlowGroupItem> groupItemsToDelete =
                            dataContext.CashFlowGroupsItems.Where(a => a.GroupId == cashFlowGroupToDelete.Id).ToList();
                        dataContext.CashFlowGroupsItems.DeleteAllOnSubmit(groupItemsToDelete);

                        List<CashFlowModelGroup> modelGroupsToDelete =
                            dataContext.CashFlowModelsGroups.Where(a => a.GroupId == cashFlowGroupToDelete.Id).ToList();
                        dataContext.CashFlowModelsGroups.DeleteAllOnSubmit(modelGroupsToDelete);
                    }
                }

                List<long> existingCashFlowModelIds = dataContext.CashFlowModels.Select(a => a.Id).ToList();
                foreach (long cashFlowModelId in existingCashFlowModelIds)
                {
                    if (!cashFlowModels.Select(a => a.Id).Contains(cashFlowModelId))
                    {
                        var cashFlowModelToDelete = dataContext.CashFlowModels.Where(a => a.Id == cashFlowModelId).Single();
                        dataContext.CashFlowModels.DeleteOnSubmit(cashFlowModelToDelete);

                        List<CashFlowModelGroup> modelGroupsToDelete =
                            dataContext.CashFlowModelsGroups.Where(a => a.ModelId == cashFlowModelToDelete.Id).ToList();
                        dataContext.CashFlowModelsGroups.DeleteAllOnSubmit(modelGroupsToDelete);
                    }
                }

                foreach (CashFlowGroup cashFlowGroup in cashFlowGroups)
                {
                    if (cashFlowGroup.Crd == DateTime.MinValue)
                    {
                        long newGroupId = dataContext.GetNextId(typeof(CashFlowGroup));

                        //Set the right Id
                        List<CashFlowModelGroup> modelGroups =
                            cashFlowModelsGroups.Where(a => a.GroupId == cashFlowGroup.Id).ToList();
                        foreach (CashFlowModelGroup cashFlowModelGroup in modelGroups)
                        {
                            cashFlowModelGroup.GroupId = newGroupId;
                        }

                        List<CashFlowModelGroup> modelGroupsWithParentId =
                            cashFlowModelsGroups.Where(a => a.ParentGroupId == cashFlowGroup.Id).ToList();
                        foreach (CashFlowModelGroup cashFlowModelGroup in modelGroupsWithParentId)
                        {
                            cashFlowModelGroup.ParentGroupId = newGroupId;
                        }

                        List<CashFlowGroupItem> groupItems = cashFlowGroupItems.Where(a => a.GroupId == cashFlowGroup.Id).ToList();
                        foreach (CashFlowGroupItem cashFlowGroupItem in groupItems)
                        {
                            cashFlowGroupItem.GroupId = newGroupId;
                        }

                        cashFlowGroup.Id = newGroupId;
                        cashFlowGroup.Cruser = userName;
                        cashFlowGroup.Chuser = userName;
                        cashFlowGroup.Crd = now;
                        cashFlowGroup.Chd = now;

                        dataContext.CashFlowGroups.InsertOnSubmit(cashFlowGroup);
                    }
                    else
                    {
                        cashFlowGroup.Chuser = userName;
                        cashFlowGroup.Chd = now;

                        dataContext.CashFlowGroups.Attach(cashFlowGroup, true);

                        List<CashFlowGroupItem> existingGroupItems =
                        dataContext.CashFlowGroupsItems.Where(a => a.GroupId == cashFlowGroup.Id).ToList();

                        dataContext.CashFlowGroupsItems.DeleteAllOnSubmit(existingGroupItems);
                    }
                }

                foreach (CashFlowGroupItem cashFlowGroupItem in cashFlowGroupItems)
                {
                    cashFlowGroupItem.Id = dataContext.GetNextId(typeof(CashFlowGroupItem));
                    dataContext.CashFlowGroupsItems.InsertOnSubmit(cashFlowGroupItem);
                }

                foreach (CashFlowModel cashFlowModel in cashFlowModels)
                {
                    if (cashFlowModel.Crd == DateTime.MinValue)
                    {
                        long newModelId = dataContext.GetNextId(typeof(CashFlowModel));

                        //Set the right Id
                        List<CashFlowModelGroup> modelGroups =
                            cashFlowModelsGroups.Where(a => a.ModelId == cashFlowModel.Id).ToList();
                        foreach (CashFlowModelGroup cashFlowModelGroup in modelGroups)
                        {
                            cashFlowModelGroup.ModelId = newModelId;
                        }

                        cashFlowModel.Id = newModelId;
                        cashFlowModel.Cruser = userName;
                        cashFlowModel.Chuser = userName;
                        cashFlowModel.Crd = now;
                        cashFlowModel.Chd = now;

                        dataContext.CashFlowModels.InsertOnSubmit(cashFlowModel);
                    }
                    else
                    {
                        cashFlowModel.Chuser = userName;
                        cashFlowModel.Chd = now;

                        dataContext.CashFlowModels.Attach(cashFlowModel, true);

                        List<CashFlowModelGroup> existingModelGroups =
                        dataContext.CashFlowModelsGroups.Where(a => a.ModelId == cashFlowModel.Id).ToList();

                        dataContext.CashFlowModelsGroups.DeleteAllOnSubmit(existingModelGroups);
                    }
                }

                foreach (CashFlowModelGroup cashFlowModelGroup in cashFlowModelsGroups)
                {
                    cashFlowModelGroup.Id = dataContext.GetNextId(typeof(CashFlowModelGroup));
                    dataContext.CashFlowModelsGroups.InsertOnSubmit(cashFlowModelGroup);
                }

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVInterestReferenceRateInitializationData(out List<Currency> currencies)
        {
            currencies = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVInterestReferenceRateInitializationData: " };
                }

                currencies = Queries.GetCurrenciesAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEInterestReferenceRate(bool isEdit, InterestReferenceRate interestReferenceRate)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEInterestReferenceRate" };
                }

                InterestReferenceRate existingRate = dataContext.InterestReferenceRates.Where(
                    a =>
                    a.CurrencyId == interestReferenceRate.CurrencyId && a.Date == interestReferenceRate.Date &&
                    a.Type == interestReferenceRate.Type && a.PeriodType == interestReferenceRate.PeriodType && a.Id != interestReferenceRate.Id).
                    SingleOrDefault();

                if (existingRate != null)
                    return 1;

                if (!isEdit)
                {
                    interestReferenceRate.Id = dataContext.GetNextId(typeof(InterestReferenceRate));
                    dataContext.InterestReferenceRates.InsertOnSubmit(interestReferenceRate);
                }
                else
                {
                    dataContext.InterestReferenceRates.Attach(interestReferenceRate, true);
                }

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? DeleteInterestReferenceRate(long interestReferenceRateId)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "DeleteInterestReferenceRate: " };
                }

                InterestReferenceRate interestReferenceRate =
                    dataContext.InterestReferenceRates.Where(a => a.Id == interestReferenceRateId).Single();

                dataContext.InterestReferenceRates.DeleteOnSubmit(interestReferenceRate);

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? AEVExchangeRateInitializationData(out List<Currency> currencies)
        {
            currencies = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "AEVExchangeRateInitializationData: " };
                }

                currencies = Queries.GetCurrenciesAll(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? DeleteExchangeRate(long exchangeRateId)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "DeleteExchangeRate: " };
                }

                ExchangeRate exchangeRate =
                    dataContext.ExchangeRates.Where(a => a.Id == exchangeRateId).Single();

                ExchangeRate reverseExchangeRate =
                            dataContext.ExchangeRates.Where(
                                a =>
                                a.SourceCurrencyId == exchangeRate.TargetCurrencyId &&
                                a.TargetCurrencyId == exchangeRate.SourceCurrencyId).Single();

                dataContext.ExchangeRates.DeleteOnSubmit(exchangeRate);
                dataContext.ExchangeRates.DeleteOnSubmit(reverseExchangeRate);

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? ApplicationParametersInitializationData(out List<AppParameter> appParameters)
        {
            appParameters = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "ApplicationParametersInitializationData: " };
                }

                appParameters = AppParameter.GetAllAppParameters(dataContext).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        #endregion

        #region Special

        public int? PollService()
        {
            return 0;
        }

        public int? ValidateCredentials(string userName, string password, out User user)
        {
            user = null;
            DataContext dataContext = null;

            try
            {
                dataContext = new DataContext(DBConnectionString);
                dataContext.ObjectTrackingEnabled = false;

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "ValidateCredentials: " };
                }

                User objUser = dataContext.Users.SingleOrDefault(a => a.Login.ToUpper() == userName.ToUpper() && a.Password == Convert.ToBase64String(Encoding.ASCII.GetBytes(password)) && a.Status == ActivationStatusEnum.Active);

                if (objUser == null) return 1;

                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);

                user = objUser;

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        #endregion

        #region Trades

        public int? GetViewTradesInitializationData(out List<Book> books, out List<Book> userAssocBooks, out List<CashFlowModel> models, out List<Index> indexes)
        {
            books = null;
            userAssocBooks = null;
            models = null;
            indexes = null;

            //DataContext dataContext = null;
            string userName = null;

            using (var dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false })
            {
                try
                {
                    userName = GenericContext<ProgramInfo>.Current.Value.UserName;

                    if (SiAuto.Si.Level == Level.Debug)
                    {
                        if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                        dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                        {
                            TitleLimit = 0,
                            TitlePrefix = "GetViewTradesInitializationDate: "
                        };
                    }

                    long userId =
                        dataContext.Users.Single(a => a.Login == GenericContext<ProgramInfo>.Current.Value.UserName).Id;

                    books = Queries.GetBooksByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                    userAssocBooks = Queries.GetBooksByUserId(dataContext, userId).ToList();

                    models = Queries.GetCashFlowModelsByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                    indexes = Queries.GetAllIndexes(dataContext).ToList();

                    return 0;
                }
                catch (Exception exc)
                {
                    HandleException(userName, exc);
                    return null;
                }
            }
            
        }

        public int? AEVTradeInitializationData(long? tradeInfoId, FormActionTypeEnum action, bool getExtraData, out List<Company> companies,
                                               out List<Company> counterparties,
                                               out List<Trader> traders, out List<Company> brokers,
                                               out List<Market> marketSegments, out List<Region> marketRegions,
                                               out List<Route> marketRoutes, out List<Vessel> vessels,
                                               out List<Index> indexes,
                                               out List<Company> banks,
                                               out List<Account> accounts, out List<ClearingHouse> clearingHouses,
                                               out List<VesselPool> vesselPools, out List<VesselBenchmarking> vesselBenchmarkings,
                                               out List<TradeVersionInfo> tradeVersionInfos, out Trade trade, out List<TradeVersionInfo> tcTradeVersionInfos)
        {
            companies = null;
            counterparties = null;
            traders = null;
            brokers = null;
            marketSegments = null;
            marketRegions = null;
            marketRoutes = null;
            vessels = null;
            indexes = null;
            banks = null;
            accounts = null;
            clearingHouses = null;
            vesselPools = null;
            vesselBenchmarkings = null;
            tradeVersionInfos = null;
            trade = null;
            tcTradeVersionInfos = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "AEVTradeInitializationData: " };
                }

                long userId =
                    dataContext.Users.Single(a => a.Login == GenericContext<ProgramInfo>.Current.Value.UserName).Id;

                if (action == FormActionTypeEnum.Add || action == FormActionTypeEnum.Edit)
                {
                    companies =
                        Queries.GetCompaniesByUserTraderAssocsByStatus(dataContext, CompanyTypeEnum.Internal, userId).
                            Distinct().ToList();
                    traders = Queries.GetTradersByStatusByUser(dataContext, ActivationStatusEnum.Active, userId).ToList();
                }
                else
                {
                    companies =
                        Queries.GetCompaniesAll(dataContext).Distinct().ToList();
                    traders = Queries.GetTradersAll(dataContext).ToList();
                }
                counterparties =
                    Queries.GetCompaniesByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                brokers = Queries.GetCompaniesBySubtypeStatus(dataContext, CompanySubtypeEnum.Broker,
                                                              ActivationStatusEnum.Active).ToList();
                marketSegments = Queries.GetMarketSegments(dataContext).ToList();
                marketRegions = Queries.GetMarketRegionsByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                marketRoutes = Queries.GetMarketRoutesByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                vessels = Queries.GetVesselsByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                indexes = Queries.GetIndexesByStatus(dataContext, ActivationStatusEnum.Active).ToList();
                vesselBenchmarkings = Queries.GetVesselBenchmarkings(dataContext).ToList();

                List<TraderCompanyAssoc> companyTraderAssocs =
                    Queries.GetTraderCompanyAssocsByStatus(dataContext, ActivationStatusEnum.Active).
                        ToList();

                List<TraderMarketAssoc> traderMarketAssocs =
                    Queries.GetTraderMarketAssocsByStatus(dataContext, ActivationStatusEnum.Active).ToList
                        ();

                foreach (Trader trader in traders)
                {
                    trader.Companies =
                        companies.Where(
                            b =>
                            companyTraderAssocs.Where(a => a.TraderId == trader.Id).Select(a => a.CompanyId).Contains(
                                b.Id)).Select(b => b).ToList();

                    trader.Markets =
                        marketSegments.Where(
                            b =>
                            traderMarketAssocs.Where(a => a.TraderId == trader.Id).Select(a => a.MarketId).Contains(b.Id))
                            .Select(b => b).ToList();

                    List<Book> traderBooks = (from objBook in dataContext.Books
                                              from objTraderBook in dataContext.TraderBooks
                                              where objTraderBook.BookId == objBook.Id
                                                    && objTraderBook.TraderId == trader.Id
                                              select objBook).ToList();

                    trader.Books = traderBooks.Select(a => new CustomBook()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        ParentBookId = a.ParentBookId,
                        Status = a.Status,
                        PositionCalculatedByDays = a.PositionCalculatedByDays
                    }).ToList();

                    foreach (CustomBook book in trader.Books)
                    {
                        if (book.ParentBookId != null)
                            book.ParentBook =
                                traderBooks.Where(a => a.Id == book.ParentBookId).Select(
                                    a =>
                                    new CustomBook()
                                    { Id = a.Id, Name = a.Name, ParentBookId = a.ParentBookId, Status = a.Status , PositionCalculatedByDays = a.PositionCalculatedByDays }).
                                    Single();
                        book.ChildrenBooks = traderBooks.Where(b => b.ParentBookId == book.Id).Select(
                            a =>
                            new CustomBook()
                            { Id = a.Id, Name = a.Name, ParentBookId = a.ParentBookId, Status = a.Status, PositionCalculatedByDays = a.PositionCalculatedByDays }).ToList();
                    }
                }

                if (getExtraData)
                {
                    banks =
                        Queries.GetCompaniesBySubtypeStatus(dataContext, CompanySubtypeEnum.ClearingBank,
                                                            ActivationStatusEnum.Active).ToList();
                    accounts =
                        Queries.GetAccountsByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                    clearingHouses =
                        Queries.GetClearingHousesByStatus(dataContext, ActivationStatusEnum.Active).ToList
                            ();
                    vesselPools =
                        Queries.GetVesselPoolsByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                    var poolInfoVesselsAnon = Queries.GetVesselPerPoolAndBenchMarking(dataContext).ToList();

                    foreach (var pool in vesselPools)
                    {
                        List<Vessel> vesselsInfo =
                            poolInfoVesselsAnon.Where(a => a.VesselPoolInfo.VesselPoolId == pool.Id).Select(a => a.Vessel).ToList();

                        foreach (Vessel vessel in vesselsInfo)
                        {
                            vessel.CurrentVesselBenchMarking =
                                poolInfoVesselsAnon.Where(a => a.Vessel.Id == vessel.Id).Select(
                                    a => a.VesselBenchmarking).Single();
                        }

                        pool.CurrentVessels = vesselsInfo;
                    }

                    tcTradeVersionInfos = (from objTradeInfo in dataContext.TradeInfos
                                           from objTrade in dataContext.Trades
                                           where objTrade.Type == TradeTypeEnum.TC
                                                 && objTrade.Id == objTradeInfo.TradeId
                                                 && objTradeInfo.DateTo == null
                                           select new TradeVersionInfo()
                                           {
                                               TradeId = objTrade.Id,
                                               TradeInfoId = objTradeInfo.Id,
                                               Code = objTradeInfo.Code,
                                               ExternalCode = objTradeInfo.ExternalCode,
                                               DateFrom = objTradeInfo.DateFrom,
                                               DateTo = objTradeInfo.DateTo
                                           }).ToList();
                }

                if (action != FormActionTypeEnum.Add)
                {
                    long tradeId =
                        dataContext.TradeInfos.Where(a => a.Id == tradeInfoId).Select(a => a.TradeId).Single();

                    tradeVersionInfos = dataContext.TradeInfos.Where(a => a.TradeId == tradeId).Select(
                        a =>
                        new TradeVersionInfo()
                        {
                            TradeId = tradeId,
                            TradeInfoId = a.Id,
                            Code = a.Code,
                            ExternalCode = a.ExternalCode,
                            DateFrom = a.DateFrom,
                            DateTo = a.DateTo
                        }).ToList();

                    IQueryable<TradeInfo> tradeInfoQuery = from objTradeInfo in dataContext.TradeInfos
                                                           from objTrade in dataContext.Trades
                                                           from objCompany in dataContext.Companies
                                                           from objCounterparty in dataContext.Companies
                                                           from objTrader in dataContext.Traders
                                                           from objMarket in dataContext.Markets
                                                           from objRegion in dataContext.Regions
                                                           from objRoute in dataContext.Routes
                                                           from objMTMFwdIndex in dataContext.Indexes
                                                           where objTradeInfo.TradeId == objTrade.Id
                                                                 && objTradeInfo.CompanyId == objCompany.Id
                                                                 && objTradeInfo.CounterpartyId == objCounterparty.Id
                                                                 && objTradeInfo.TraderId == objTrader.Id
                                                                 && objTradeInfo.MarketId == objMarket.Id
                                                                 && objTradeInfo.RegionId == objRegion.Id
                                                                 && objTradeInfo.RouteId == objRoute.Id
                                                                 && objTradeInfo.MTMFwdIndexId == objMTMFwdIndex.Id
                                                                 && objTradeInfo.Id == tradeInfoId
                                                           select objTradeInfo;

                    var anonTradeInfoQuery = (from objTradeInfo in tradeInfoQuery
                                              from objTrade in dataContext.Trades
                                              from objCompany in dataContext.Companies
                                              from objCounterparty in dataContext.Companies
                                              from objTrader in dataContext.Traders
                                              from objMarket in dataContext.Markets
                                              from objRegion in dataContext.Regions
                                              from objRoute in dataContext.Routes
                                              from objMTMFwdIndex in dataContext.Indexes
                                              where objTradeInfo.TradeId == objTrade.Id
                                                    && objTradeInfo.CompanyId == objCompany.Id
                                                    && objTradeInfo.CounterpartyId == objCounterparty.Id
                                                    && objTradeInfo.TraderId == objTrader.Id
                                                    && objTradeInfo.MarketId == objMarket.Id
                                                    && objTradeInfo.RegionId == objRegion.Id
                                                    && objTradeInfo.RouteId == objRoute.Id
                                                    && objTradeInfo.MTMFwdIndexId == objMTMFwdIndex.Id
                                              select
                                                  new
                                                  {
                                                      Trade = objTrade,
                                                      TradeInfo = objTradeInfo,
                                                      CompanyName = objCompany.Name,
                                                      CounterpartyName = objCounterparty.Name,
                                                      TraderName = objTrader.Name,
                                                      MarketName = objMarket.Name,
                                                      MarketTonnes = objMarket.Tonnes,
                                                      RegionName = objRegion.Name,
                                                      RouteName = objRoute.Name,
                                                      MTMFwdIndexName = objMTMFwdIndex.Name
                                                  });

                    var anonTradeInfoList = anonTradeInfoQuery.ToList();

                    var tradeBrokerInfos = (from objTradeInfo in tradeInfoQuery
                                            from objTradeBrokerInfo in dataContext.TradeBrokerInfos
                                            from objBroker in dataContext.Companies
                                            where objTradeInfo.Id == objTradeBrokerInfo.TradeInfoId
                                                  && objBroker.Id == objTradeBrokerInfo.BrokerId
                                            select new
                                            {
                                                objBroker,
                                                objTradeBrokerInfo,
                                                objTradeInfo.Id
                                            }).ToList();

                    var tradeInfoBooks = (from objTradeInfo in tradeInfoQuery
                                          from objTradeInfoBook in dataContext.TradeInfoBooks
                                          where objTradeInfo.Id == objTradeInfoBook.TradeInfoId
                                          select new
                                          {
                                              objTradeInfoBook,
                                              objTradeInfo.Id
                                          }).ToList();

                    var tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                     from objTradeTCInfo in dataContext.TradeTcInfos
                                                     from objTrade in dataContext.Trades
                                                     from objTradeTCInfoLeg in dataContext.TradeTcInfoLegs
                                                     from objVessel in dataContext.Vessels
                                                     where objTrade.Type == TradeTypeEnum.TC
                                                           && objTrade.Id == objTradeInfo.TradeId
                                                           && objTradeInfo.Id == objTradeTCInfo.TradeInfoId
                                                           && objTradeTCInfo.Id == objTradeTCInfoLeg.TradeTcInfoId
                                                           && objTradeTCInfo.VesselId == objVessel.Id
                                                     orderby objTradeTCInfoLeg.PeriodFrom
                                                     select
                                                         new
                                                         {
                                                             TradeTCInfoLeg = objTradeTCInfoLeg,
                                                             TradeTCInfo = objTradeTCInfo,
                                                             TradeInfoId = objTradeInfo.Id,
                                                             VesselName = objVessel.Name
                                                         });

                    var tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                        from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                        from objTrade in dataContext.Trades
                                                        from objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs
                                                        join objVesselL in dataContext.Vessels
                                                            on objTradeCargoInfo.VesselId equals objVesselL.Id into
                                                            objVesselLefts
                                                        from objVessel in objVesselLefts.DefaultIfEmpty()
                                                        where objTrade.Type == TradeTypeEnum.Cargo
                                                              && objTrade.Id == objTradeInfo.TradeId
                                                              && objTradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                              &&
                                                              objTradeCargoInfo.Id ==
                                                              objTradeCargoInfoLeg.TradeCargoInfoId
                                                        orderby objTradeCargoInfoLeg.PeriodFrom
                                                        select
                                                            new
                                                            {
                                                                TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                                TradeCargoInfo = objTradeCargoInfo,
                                                                TradeInfoId = objTradeInfo.Id,
                                                                VesselName = objVessel.Name
                                                            });

                    var tradeTCInfosWithLegsList = tradeTCInfosWithLegsQuery.ToList();
                    var tradeCargoInfosWithLegsList = tradeCargoInfosWithLegsQuery.ToList();

                    var tradeFFAInfos = (from objTradeInfo in tradeInfoQuery
                                         from objTradeFFAInfo in dataContext.TradeFfaInfos
                                         from objTrade in dataContext.Trades
                                         where objTrade.Type == TradeTypeEnum.FFA
                                               && objTrade.Id == objTradeInfo.TradeId
                                               && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                         select new { TradeFFAInfo = objTradeFFAInfo, TradeInfoId = objTradeInfo.Id }).
                        ToList();

                    var tradeOptionInfos = (from objTradeInfo in tradeInfoQuery
                                            from objTradeOptionInfo in dataContext.TradeOptionInfos
                                            from objTrade in dataContext.Trades
                                            where objTrade.Type == TradeTypeEnum.Option
                                                  && objTrade.Id == objTradeInfo.TradeId
                                                  && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                            select
                                                new
                                                {
                                                    TradeOptionInfo = objTradeOptionInfo,
                                                    TradeInfoId = objTradeInfo.Id
                                                })
                        .ToList();

                    foreach (var anon in anonTradeInfoList)
                    {
                        anon.TradeInfo.TradeBrokerInfos =
                            tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeBrokerInfo).
                                ToList();
                        anon.TradeInfo.TradeInfoBooks =
                            tradeInfoBooks.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeInfoBook).ToList();

                        anon.TradeInfo.BrokersName =
                            tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Any()
                                ? tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(
                                    a => a.objBroker.Name + "(" + a.objTradeBrokerInfo.Commission + "%)").Aggregate(
                                        (a1, a2) => a1 + ", " + a2)
                                : null;
                        anon.TradeInfo.MarketName = anon.MarketName;
                        anon.TradeInfo.MarketTonnes = anon.MarketTonnes;
                        anon.TradeInfo.TraderName = anon.TraderName;
                        anon.TradeInfo.CounterpartyName = anon.CounterpartyName;
                        anon.TradeInfo.CompanyName = anon.CompanyName;
                        anon.TradeInfo.MTMFwdIndexName = anon.MTMFwdIndexName;
                        anon.Trade.Info = anon.TradeInfo;
                        if (anon.Trade.Type == TradeTypeEnum.TC)
                        {
                            if (tradeTCInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                            {
                                anon.Trade.Info.TCInfo =
                                    tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeTCInfo;
                                anon.Trade.Info.TCInfo.VesselName =
                                    tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                                anon.Trade.Info.TCInfo.Legs =
                                    tradeTCInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                        a => a.TradeTCInfoLeg).ToList();
                            }
                            else
                            {
                                anon.Trade.Info.TCInfo = new TradeTcInfo();
                                anon.Trade.Info.TCInfo.Legs = new List<TradeTcInfoLeg>();
                            }
                        }
                        else if (anon.Trade.Type == TradeTypeEnum.FFA)
                        {
                            anon.Trade.Info.FFAInfo =
                                tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeFFAInfo;

                            var poolInfoVesselsAnon =
                                Queries.GetVesselPerPoolAndBenchMarkingPerSignDate(dataContext,
                                                                                   anon.Trade.Info.SignDate.Date).ToList
                                    ();

                            var pools = new List<VesselPool>();
                            foreach (var poolInfo in poolInfoVesselsAnon)
                            {
                                if (pools.Select(a => a.Id).Contains(poolInfo.VesselPool.Id)) continue;

                                List<Vessel> vesselInfo =
                                    poolInfoVesselsAnon.Where(
                                        a => a.VesselPoolInfo.VesselPoolId == poolInfo.VesselPool.Id).Select(
                                            a => a.Vessel).ToList();
                                foreach (Vessel vessel in vesselInfo)
                                {
                                    vessel.CurrentVesselBenchMarking =
                                        poolInfoVesselsAnon.Where(a => a.Vessel.Id == vessel.Id).Select(
                                            a => a.VesselBenchmarking).SingleOrDefault();
                                }
                                poolInfo.VesselPool.CurrentVessels = vesselInfo;
                                pools.Add(poolInfo.VesselPool);
                            }

                            vesselPools = pools;
                        }
                        else if (anon.Trade.Type == TradeTypeEnum.Cargo)
                        {
                            if (tradeCargoInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) !=
                                null)
                            {
                                anon.Trade.Info.CargoInfo =
                                    tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                                        TradeCargoInfo;
                                anon.Trade.Info.CargoInfo.VesselName =
                                    tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                                        VesselName;
                                anon.Trade.Info.CargoInfo.Legs =
                                    tradeCargoInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                        a => a.TradeCargoInfoLeg).ToList();
                            }
                            else
                            {
                                anon.Trade.Info.CargoInfo = new TradeCargoInfo();
                                anon.Trade.Info.CargoInfo.Legs = new List<TradeCargoInfoLeg>();
                            }
                        }
                        else if (anon.Trade.Type == TradeTypeEnum.Option)
                        {
                            anon.Trade.Info.OptionInfo =
                                tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeOptionInfo;

                            var poolInfoVesselsAnon =
                                Queries.GetVesselPerPoolAndBenchMarkingPerSignDate(dataContext,
                                                                                   anon.Trade.Info.SignDate.Date).ToList
                                    ();

                            var pools = new List<VesselPool>();
                            foreach (var poolInfo in poolInfoVesselsAnon)
                            {
                                if (pools.Select(a => a.Id).Contains(poolInfo.VesselPool.Id)) continue;

                                List<Vessel> vesselInfo =
                                    poolInfoVesselsAnon.Where(
                                        a => a.VesselPoolInfo.VesselPoolId == poolInfo.VesselPool.Id).Select(
                                            a => a.Vessel).ToList();
                                foreach (Vessel vessel in vesselInfo)
                                {
                                    vessel.CurrentVesselBenchMarking =
                                        poolInfoVesselsAnon.Where(a => a.Vessel.Id == vessel.Id).Select(
                                            a => a.VesselBenchmarking).SingleOrDefault();
                                }
                                poolInfo.VesselPool.CurrentVessels = vesselInfo;
                                pools.Add(poolInfo.VesselPool);
                            }

                            vesselPools = pools;
                        }
                    }

                    trade = anonTradeInfoList.Select(a => a.Trade).Single();
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? InsertNewTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                                    List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                                    TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                                    TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "InsertNewTrade: " };
                }

                int? result = SaveTrade(dataContext, userName, trade, tradeInfo, tradeTcInfo, tradeTcInfoLegs,
                                        tradeFfaInfo,
                                        tradeCargoInfo, tradeCargoInfoLegs, tradeOptionInfo, tradeBrokerInfos,
                                        tradeInfoBooks);

                if (result == 0 && poolTrades != null)
                {
                    foreach (Trade poolTrade in poolTrades)
                    {
                        SaveTrade(dataContext, userName, poolTrade, poolTrade.Info, tradeTcInfo, tradeTcInfoLegs,
                                  poolTrade.Info.FFAInfo,
                                  tradeCargoInfo, tradeCargoInfoLegs, poolTrade.Info.OptionInfo,
                                  poolTrade.Info.TradeBrokerInfos,
                                  poolTrade.Info.TradeInfoBooks);
                    }
                }
                if (result == 0)
                    dataContext.SubmitChanges();

                return result;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? UpdateTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                                   List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                                   TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                                   TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "UpdateTrade: " };
                }

                int? result = UpdateTrade(dataContext, userName, trade, tradeInfo, tradeTcInfo, tradeTcInfoLegs,
                                        tradeFfaInfo,
                                        tradeCargoInfo, tradeCargoInfoLegs, tradeOptionInfo, tradeBrokerInfos,
                                        tradeInfoBooks);

                if (result == 0 && poolTrades != null)
                {
                    foreach (Trade poolTrade in poolTrades)
                    {
                        SaveTrade(dataContext, userName, poolTrade, poolTrade.Info, tradeTcInfo, tradeTcInfoLegs,
                                  poolTrade.Info.FFAInfo,
                                  tradeCargoInfo, tradeCargoInfoLegs, poolTrade.Info.OptionInfo,
                                  poolTrade.Info.TradeBrokerInfos,
                                  poolTrade.Info.TradeInfoBooks);
                    }
                }

                if (result == 0)
                    dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetEntitiesThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, DomainObject entityTypeInstanceToReturn, List<DomainObject> entityTypeFilters, out List<DomainObject> entities)
        {
            entities = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetEntitiesThatHaveTradesByFilters: " };
                }

                IQueryable<TradeInfo> internalQueryTemp =
                    from objTrade in dataContext.Trades
                    from objTradeInfo in dataContext.TradeInfos
                    where objTradeInfo.DateTo == null
                          && objTrade.Id == objTradeInfo.TradeId
                          && objTrade.Status == ActivationStatusEnum.Active
                          && objTradeInfo.SignDate <= positionDate
                          && objTradeInfo.PeriodFrom <= periodTo
                          && objTradeInfo.PeriodTo >= periodFrom
                    select objTradeInfo;

                if (!acceptDrafts)
                    internalQueryTemp = from objTradeInfo in internalQueryTemp
                                        from objTrade in dataContext.Trades
                                        where objTrade.State != TradeStateEnum.Draft
                                        select objTradeInfo;
                //Add parent companies that do not have trades



                foreach (DomainObject entityTypeFilter in entityTypeFilters)
                {
                    if (entityTypeFilter.GetType() == typeof(Company))
                    {
                        long companyId = ((Company)entityTypeFilter).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.CompanyId == companyId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.GetType() == typeof(Market))
                    {
                        long marketId = ((Market)entityTypeFilter).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.MarketId == marketId
                                            select objTradeInfo;
                    }
                }

                if (entityTypeInstanceToReturn.GetType() == typeof(Company) &&
                    ((Company)entityTypeInstanceToReturn).IsCounterParty == false)
                {
                    IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                     select objTradeInfo.CompanyId;

                    entities = new List<DomainObject>((from objCompany in dataContext.Companies
                                                       where internalQuery.Contains(objCompany.Id)
                                                       select objCompany).ToList());
                }
                else if (entityTypeInstanceToReturn.GetType() == typeof(Market))
                {
                    IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                     select objTradeInfo.MarketId;

                    entities = new List<DomainObject>((from objMarket in dataContext.Markets
                                                       where internalQuery.Contains(objMarket.Id)
                                                       select objMarket).ToList());
                }
                else if (entityTypeInstanceToReturn.GetType() == typeof(Trader))
                {
                    IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                     select objTradeInfo.TraderId;

                    entities = new List<DomainObject>((from objTrader in dataContext.Traders
                                                       where internalQuery.Contains(objTrader.Id)
                                                       select objTrader).ToList());
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, List<DomainObject> entityTypeFilters, out List<Trade> trades)
        {
            trades = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "GetTradesByFilters: " };
                }

                IQueryable<TradeInfo> tradeInfoQuery;

                if (entityTypeFilters.Count > 0 && entityTypeFilters[0].GetType() == typeof(Book))
                {
                    long bookId = ((Book)entityTypeFilters[0]).Id;
                    tradeInfoQuery = from objTrade in dataContext.Trades
                                     from objTradeInfo in dataContext.TradeInfos
                                     from objTradeInfoBook in dataContext.TradeInfoBooks
                                     where objTradeInfo.Id == objTradeInfoBook.TradeInfoId
                                           && objTradeInfoBook.BookId == bookId
                                           && objTrade.Id == objTradeInfo.TradeId
                                           && objTradeInfo.DateTo == null
                                     select objTradeInfo;
                }
                else
                {
                    tradeInfoQuery =
                        from objTrade in dataContext.Trades
                        from objTradeInfo in dataContext.TradeInfos
                        where objTradeInfo.DateTo == null
                              && objTrade.Id == objTradeInfo.TradeId
                              && objTrade.Status == ActivationStatusEnum.Active
                              && objTradeInfo.SignDate <= positionDate
                              && objTradeInfo.PeriodFrom <= periodTo
                              && objTradeInfo.PeriodTo >= periodFrom
                        select objTradeInfo;
                }

                if (!acceptDrafts)
                    tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                     from objTrade in dataContext.Trades
                                     where objTradeInfo.TradeId == objTrade.Id
                                           && objTrade.State != TradeStateEnum.Draft
                                     select objTradeInfo;

                foreach (DomainObject entityTypeFilter in entityTypeFilters)
                {
                    if (entityTypeFilter.GetType() == typeof(Company))
                    {
                        long companyId = ((Company)entityTypeFilter).Id;
                        tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                         where objTradeInfo.CompanyId == companyId
                                         select objTradeInfo;
                    }
                    else if (entityTypeFilter.GetType() == typeof(Market))
                    {
                        long marketId = ((Market)entityTypeFilter).Id;
                        tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                         where objTradeInfo.MarketId == marketId
                                         select objTradeInfo;
                    }
                    else if (entityTypeFilter.GetType() == typeof(Trader))
                    {
                        long traderId = ((Trader)entityTypeFilter).Id;
                        tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                         where objTradeInfo.TraderId == traderId
                                         select objTradeInfo;
                    }
                }

                tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                 from objTrade in dataContext.Trades
                                 from objCompany in dataContext.Companies
                                 from objCounterparty in dataContext.Companies
                                 from objTrader in dataContext.Traders
                                 from objMarket in dataContext.Markets
                                 from objRegion in dataContext.Regions
                                 from objRoute in dataContext.Routes
                                 from objMTMFwdIndex in dataContext.Indexes
                                 where objTradeInfo.TradeId == objTrade.Id
                                       && objTradeInfo.CompanyId == objCompany.Id
                                       && objTradeInfo.CounterpartyId == objCounterparty.Id
                                       && objTradeInfo.TraderId == objTrader.Id
                                       && objTradeInfo.MarketId == objMarket.Id
                                       && objTradeInfo.RegionId == objRegion.Id
                                       && objTradeInfo.RouteId == objRoute.Id
                                       && objTradeInfo.MTMFwdIndexId == objMTMFwdIndex.Id
                                 select objTradeInfo;

                var anonTradeInfoQuery = (from objTradeInfo in tradeInfoQuery
                                          from objTrade in dataContext.Trades
                                          from objCompany in dataContext.Companies
                                          from objCounterparty in dataContext.Companies
                                          from objTrader in dataContext.Traders
                                          from objMarket in dataContext.Markets
                                          from objRegion in dataContext.Regions
                                          from objRoute in dataContext.Routes
                                          from objMTMFwdIndex in dataContext.Indexes
                                          where objTradeInfo.TradeId == objTrade.Id
                                                && objTradeInfo.CompanyId == objCompany.Id
                                                && objTradeInfo.CounterpartyId == objCounterparty.Id
                                                && objTradeInfo.TraderId == objTrader.Id
                                                && objTradeInfo.MarketId == objMarket.Id
                                                && objTradeInfo.RegionId == objRegion.Id
                                                && objTradeInfo.RouteId == objRoute.Id
                                                && objTradeInfo.MTMFwdIndexId == objMTMFwdIndex.Id
                                          select
                                              new
                                              {
                                                  Trade = objTrade,
                                                  TradeInfo = objTradeInfo,
                                                  CompanyName = objCompany.Name,
                                                  CounterpartyName = objCounterparty.Name,
                                                  TraderName = objTrader.Name,
                                                  MarketName = objMarket.Name,
                                                  MarketTonnes = objMarket.Tonnes,
                                                  RegionName = objRegion.Name,
                                                  RouteName = objRoute.Name,
                                                  MTMFwdIndexName = objMTMFwdIndex.Name
                                              });

                var anonTradeInfoList = anonTradeInfoQuery.ToList();

                var tradeBrokerInfos = (from objTradeInfo in tradeInfoQuery
                                        from objTradeBrokerInfo in dataContext.TradeBrokerInfos
                                        from objBroker in dataContext.Companies
                                        where objTradeInfo.Id == objTradeBrokerInfo.TradeInfoId
                                              && objBroker.Id == objTradeBrokerInfo.BrokerId
                                        select new
                                        {
                                            objBroker,
                                            objTradeBrokerInfo,
                                            objTradeInfo.Id
                                        }).ToList();

                var tradeInfoBooks = (from objTradeInfo in tradeInfoQuery
                                      from objTradeInfoBook in dataContext.TradeInfoBooks
                                      where objTradeInfo.Id == objTradeInfoBook.TradeInfoId
                                      select new
                                      {
                                          objTradeInfoBook,
                                          objTradeInfo.Id
                                      }).ToList();

                var tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                 from objTradeTCInfo in dataContext.TradeTcInfos
                                                 from objTrade in dataContext.Trades
                                                 from objTradeTCInfoLeg in dataContext.TradeTcInfoLegs
                                                 from objVessel in dataContext.Vessels
                                                 where objTrade.Type == TradeTypeEnum.TC
                                                       && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeTCInfo.TradeInfoId
                                                       && objTradeTCInfo.Id == objTradeTCInfoLeg.TradeTcInfoId
                                                       && objTradeTCInfo.VesselId == objVessel.Id
                                                       && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                       && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                 orderby objTradeTCInfoLeg.PeriodFrom
                                                 select
                                                     new
                                                     {
                                                         TradeTCInfoLeg = objTradeTCInfoLeg,
                                                         TradeTCInfo = objTradeTCInfo,
                                                         TradeInfoId = objTradeInfo.Id,
                                                         VesselName = objVessel.Name
                                                     });

                var tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                    from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                    from objTrade in dataContext.Trades
                                                    from objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs
                                                    from objVessel in dataContext.Vessels
                                                    where objTrade.Type == TradeTypeEnum.Cargo
                                                          && objTrade.Id == objTradeInfo.TradeId
                                                          && objTradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                          &&
                                                          objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                                          && objTradeCargoInfo.VesselId == objVessel.Id
                                                          && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                          && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                    orderby objTradeCargoInfoLeg.PeriodFrom
                                                    select
                                                        new
                                                        {
                                                            TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                            TradeCargoInfo = objTradeCargoInfo,
                                                            TradeInfoId = objTradeInfo.Id,
                                                            VesselName = objVessel.Name
                                                        });

                if (!acceptMinimum)
                {
                    tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                where objAnon.TradeTCInfoLeg.IsOptional
                                                select objAnon;

                    tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                   where objAnon.TradeCargoInfoLeg.IsOptional
                                                   select objAnon;
                }


                if (!acceptOptional)
                {
                    tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                where objAnon.TradeTCInfoLeg.IsOptional == false
                                                select objAnon;

                    tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                   where objAnon.TradeCargoInfoLeg.IsOptional == false
                                                   select objAnon;
                }

                var tradeTCInfosWithLegsList = tradeTCInfosWithLegsQuery.ToList();
                var tradeCargoInfosWithLegsList = tradeCargoInfosWithLegsQuery.ToList();

                var tradeFFAInfos = (from objTradeInfo in tradeInfoQuery
                                     from objTradeFFAInfo in dataContext.TradeFfaInfos
                                     from objTrade in dataContext.Trades
                                     where objTrade.Type == TradeTypeEnum.FFA
                                           && objTrade.Id == objTradeInfo.TradeId
                                           && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                     select new { TradeFFAInfo = objTradeFFAInfo, TradeInfoId = objTradeInfo.Id }).ToList();

                var tradeOptionInfos = (from objTradeInfo in tradeInfoQuery
                                        from objTradeOptionInfo in dataContext.TradeOptionInfos
                                        from objTrade in dataContext.Trades
                                        where objTrade.Type == TradeTypeEnum.Option
                                              && objTrade.Id == objTradeInfo.TradeId
                                              && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                        select new { TradeOptionInfo = objTradeOptionInfo, TradeInfoId = objTradeInfo.Id })
                    .ToList();

                foreach (var anon in anonTradeInfoList)
                {
                    anon.TradeInfo.TradeBrokerInfos =
                        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeBrokerInfo).ToList();
                    anon.TradeInfo.TradeInfoBooks =
                        tradeInfoBooks.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeInfoBook).ToList();

                    anon.TradeInfo.BrokersName =
                        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(
                            a => a.objBroker.Name + "(" + a.objTradeBrokerInfo.Commission + "%)").Aggregate(
                                (a1, a2) => a1 + ", " + a2);
                    anon.TradeInfo.MarketName = anon.MarketName;
                    anon.TradeInfo.MarketTonnes = anon.MarketTonnes;
                    anon.TradeInfo.TraderName = anon.TraderName;
                    anon.TradeInfo.CounterpartyName = anon.CounterpartyName;
                    anon.TradeInfo.CompanyName = anon.CompanyName;
                    anon.TradeInfo.MTMFwdIndexName = anon.MTMFwdIndexName;
                    anon.Trade.Info = anon.TradeInfo;
                    if (anon.Trade.Type == TradeTypeEnum.TC)
                    {
                        if (tradeTCInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.Trade.Info.TCInfo =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeTCInfo;
                            anon.Trade.Info.TCInfo.VesselName =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.Trade.Info.TCInfo.Legs =
                                tradeTCInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeTCInfoLeg).ToList();
                        }
                        else
                        {
                            anon.Trade.Info.TCInfo = new TradeTcInfo();
                            anon.Trade.Info.TCInfo.Legs = new List<TradeTcInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.FFA)
                    {
                        anon.Trade.Info.FFAInfo =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeFFAInfo;
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Cargo)
                    {
                        if (tradeCargoInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.Trade.Info.CargoInfo =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                                    TradeCargoInfo;
                            anon.Trade.Info.CargoInfo.VesselName =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.Trade.Info.CargoInfo.Legs =
                                tradeCargoInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeCargoInfoLeg).ToList();
                        }
                        else
                        {
                            anon.Trade.Info.CargoInfo = new TradeCargoInfo();
                            anon.Trade.Info.CargoInfo.Legs = new List<TradeCargoInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Option)
                    {
                        anon.Trade.Info.OptionInfo =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeOptionInfo;
                    }
                }

                trades = anonTradeInfoList.Select(a => a.Trade).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetUserAssociatedBooks(out List<Book> books)
        {
            books = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                    { TitleLimit = 0, TitlePrefix = "GetUserAssociatedBooks: " };
                }

                long userId =
                    dataContext.Users.Single(a => a.Login == GenericContext<ProgramInfo>.Current.Value.UserName).Id;


                books = Queries.GetBooksByUserId(dataContext, userId).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetVesselPoolsBySignDate(DateTime signDate, out List<VesselPool> vesselPools)
        {
            vesselPools = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetVesselPoolsBySignDate: " };
                }

                var poolInfoVesselsAnon = Queries.GetVesselPerPoolAndBenchMarkingPerSignDate(dataContext, signDate).ToList();

                var pools = new List<VesselPool>();
                foreach (var poolInfo in poolInfoVesselsAnon)
                {
                    if (pools.Select(a => a.Id).Contains(poolInfo.VesselPool.Id)) continue;

                    List<Vessel> vessels =
                        poolInfoVesselsAnon.Where(a => a.VesselPoolInfo.VesselPoolId == poolInfo.VesselPool.Id).Select(
                            a => a.Vessel).ToList();
                    foreach (Vessel vessel in vessels)
                    {
                        vessel.CurrentVesselBenchMarking =
                            poolInfoVesselsAnon.Where(a => a.Vessel.Id == vessel.Id).Select(
                                a => a.VesselBenchmarking).SingleOrDefault();
                    }
                    poolInfo.VesselPool.CurrentVessels = vessels;
                    pools.Add(poolInfo.VesselPool);
                }

                vesselPools = pools;

                return 0;

            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        private int? SaveTrade(DataContext dataContext, string userName, Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                                    List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                                    TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                                    TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks)
        {
            try
            {
                DateTime now = DateTime.Now;

                trade.Id = dataContext.GetNextId(typeof(Trade));
                trade.Cruser = userName;
                trade.Chuser = userName;
                trade.Crd = now;
                trade.Chd = now;

                tradeInfo.Id = dataContext.GetNextId(typeof(TradeInfo));
                tradeInfo.TradeId = trade.Id;
                tradeInfo.Code = trade.Type.ToString("g") + "_" + now.ToString("u") + "_" + trade.Id;
                tradeInfo.DateFrom = Convert.ToDateTime(tradeInfo.SignDate,
                                                        SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date;
                //tradeInfo.DateFrom = now;
                tradeInfo.DateTo = null;
                tradeInfo.Cruser = userName;
                tradeInfo.Chuser = userName;
                tradeInfo.Crd = now;
                tradeInfo.Chd = now;

                foreach (TradeBrokerInfo tradeBrokerInfo in tradeBrokerInfos)
                {
                    tradeBrokerInfo.Id = dataContext.GetNextId(typeof(TradeBrokerInfo));
                    tradeBrokerInfo.TradeInfoId = tradeInfo.Id;
                    dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                }

                foreach (TradeInfoBook tradeInfoBook in tradeInfoBooks)
                {
                    tradeInfoBook.Id = dataContext.GetNextId(typeof(TradeInfoBook));
                    tradeInfoBook.TradeInfoId = tradeInfo.Id;
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);
                }

                switch (trade.Type)
                {
                    case TradeTypeEnum.TC:
                        tradeTcInfo.Id = dataContext.GetNextId(typeof(TradeTcInfo));
                        tradeTcInfo.TradeInfoId = tradeInfo.Id;

                        foreach (TradeTcInfoLeg leg in tradeTcInfoLegs)
                        {
                            leg.Id = dataContext.GetNextId(typeof(TradeTcInfoLeg));
                            leg.TradeTcInfoId = tradeTcInfo.Id;
                            dataContext.TradeTcInfoLegs.InsertOnSubmit(leg);
                        }

                        dataContext.TradeTcInfos.InsertOnSubmit(tradeTcInfo);

                        break;
                    case TradeTypeEnum.FFA:
                        tradeFfaInfo.Id = dataContext.GetNextId(typeof(TradeFfaInfo));
                        tradeFfaInfo.TradeInfoId = tradeInfo.Id;

                        dataContext.TradeFfaInfos.InsertOnSubmit(tradeFfaInfo);
                        break;

                    case TradeTypeEnum.Cargo:
                        tradeCargoInfo.Id = dataContext.GetNextId(typeof(TradeCargoInfo));
                        tradeCargoInfo.TradeInfoId = tradeInfo.Id;

                        foreach (TradeCargoInfoLeg leg in tradeCargoInfoLegs)
                        {
                            leg.Id = dataContext.GetNextId(typeof(TradeCargoInfoLeg));
                            leg.TradeCargoInfoId = tradeCargoInfo.Id;
                            dataContext.TradeCargoInfoLegs.InsertOnSubmit(leg);
                        }

                        dataContext.TradeCargoInfos.InsertOnSubmit(tradeCargoInfo);
                        break;
                    case TradeTypeEnum.Option:
                        tradeOptionInfo.Id = dataContext.GetNextId(typeof(TradeOptionInfo));
                        tradeOptionInfo.TradeInfoId = tradeInfo.Id;

                        dataContext.TradeOptionInfos.InsertOnSubmit(tradeOptionInfo);
                        break;
                }

                dataContext.Trades.InsertOnSubmit(trade);
                dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
        }

        private int? UpdateTrade(DataContext dataContext, string userName, Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                                   List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                                   TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                                   TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks)
        {
            try
            {

                DateTime now = DateTime.Now;

                trade.Chuser = userName;
                trade.Chd = now;
                dataContext.Trades.Attach(trade, true);

                TradeInfo oldTradeInfo =
                    dataContext.TradeInfos.Where(a => a.TradeId == trade.Id && a.DateTo == null).Single();
                oldTradeInfo.DateTo = now.Date;
                oldTradeInfo.Chd = now;
                oldTradeInfo.Chuser = userName;

                TradeInfo newTradeInfo = tradeInfo;
                newTradeInfo.Id = dataContext.GetNextId(typeof(TradeInfo));
                newTradeInfo.TradeId = trade.Id;
                newTradeInfo.Code = trade.Type.ToString("g") + "_" + now.ToString("u") + "_" + trade.Id;
                newTradeInfo.DateFrom = now.Date;
                newTradeInfo.DateTo = null;
                newTradeInfo.Cruser = userName;
                newTradeInfo.Chuser = userName;
                newTradeInfo.Crd = now;
                newTradeInfo.Chd = now;

                foreach (TradeBrokerInfo tradeBrokerInfo in tradeBrokerInfos)
                {
                    tradeBrokerInfo.Id = dataContext.GetNextId(typeof(TradeBrokerInfo));
                    tradeBrokerInfo.TradeInfoId = newTradeInfo.Id;
                    dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                }

                foreach (TradeInfoBook tradeInfoBook in tradeInfoBooks)
                {
                    tradeInfoBook.Id = dataContext.GetNextId(typeof(TradeInfoBook));
                    tradeInfoBook.TradeInfoId = newTradeInfo.Id;
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);
                }

                switch (trade.Type)
                {
                    case TradeTypeEnum.TC:
                        tradeTcInfo.Id = dataContext.GetNextId(typeof(TradeTcInfo));
                        tradeTcInfo.TradeInfoId = newTradeInfo.Id;

                        foreach (TradeTcInfoLeg leg in tradeTcInfoLegs)
                        {
                            leg.Id = dataContext.GetNextId(typeof(TradeTcInfoLeg));
                            leg.TradeTcInfoId = tradeTcInfo.Id;
                            dataContext.TradeTcInfoLegs.InsertOnSubmit(leg);
                        }

                        dataContext.TradeTcInfos.InsertOnSubmit(tradeTcInfo);

                        break;
                    case TradeTypeEnum.FFA:
                        tradeFfaInfo.Id = dataContext.GetNextId(typeof(TradeFfaInfo));
                        tradeFfaInfo.TradeInfoId = newTradeInfo.Id;

                        dataContext.TradeFfaInfos.InsertOnSubmit(tradeFfaInfo);
                        break;

                    case TradeTypeEnum.Cargo:
                        tradeCargoInfo.Id = dataContext.GetNextId(typeof(TradeCargoInfo));
                        tradeCargoInfo.TradeInfoId = newTradeInfo.Id;

                        foreach (TradeCargoInfoLeg leg in tradeCargoInfoLegs)
                        {
                            leg.Id = dataContext.GetNextId(typeof(TradeCargoInfoLeg));
                            leg.TradeCargoInfoId = tradeCargoInfo.Id;
                            dataContext.TradeCargoInfoLegs.InsertOnSubmit(leg);
                        }

                        dataContext.TradeCargoInfos.InsertOnSubmit(tradeCargoInfo);
                        break;
                    case TradeTypeEnum.Option:
                        tradeOptionInfo.Id = dataContext.GetNextId(typeof(TradeOptionInfo));
                        tradeOptionInfo.TradeInfoId = newTradeInfo.Id;

                        dataContext.TradeOptionInfos.InsertOnSubmit(tradeOptionInfo);
                        break;
                }

                dataContext.TradeInfos.InsertOnSubmit(newTradeInfo);

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
        }
      
        public int? GetEntitiesThatHaveTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
    long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters, out List<HierarchyNodeInfo> hierarchyNodes)
        {
            hierarchyNodes = new List<HierarchyNodeInfo>();

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetEntitiesThatHaveTradesByFilters2: " };
                }

                IQueryable<BasicTrade> tradeTCInfosQuery = null;
                IQueryable<BasicTrade> tradeCargoInfosQuery = null;
                IQueryable<BasicTrade> tradeFFAInfosQuery = null;
                IQueryable<BasicTrade> tradeOptionInfosQuery = null;
                IQueryable<BasicTrade> tradeTcInfoLegsQuery;
                IQueryable<BasicTrade> tradeCargoInfoLegsQuery;

                //Get company sublevel if exists
                var selectedTypeNode = entityTypeFilters.FirstOrDefault();

                if (selectedTypeNode != null && selectedTypeNode.DomainObject != null && selectedTypeNode.HierarchyNodeType != null)
                {
                    if (selectedTypeNode.DomainObject.GetType() == typeof(Company) &&
                        selectedTypeNode.HierarchyNodeType.Code == "CMP")
                    {
                        //get next level (subcompanies)
                        var company = (Company)selectedTypeNode.DomainObject;

                        hierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                 where objCompany.ParentId == company.Id
                                                 select new HierarchyNodeInfo()
                                                 {
                                                     DomainObject = objCompany,
                                                     HierarchyNodeType = selectedTypeNode.HierarchyNodeType

                                                 }).ToList());
                    }
                }

                IQueryable<BasicTrade> internalQueryTemp = from objTrade in dataContext.Trades
                                                           from objTradeInfo in dataContext.TradeInfos
                                                           join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                                                           join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                                           where objTradeInfo.DateTo == null
                                                                 && objTradeBook.Status == ActivationStatusEnum.Active
                                                                 && objTrade.Id == objTradeInfo.TradeId
                                                                 && objTrade.Status == ActivationStatusEnum.Active
                                                                 && objTradeInfo.SignDate <= positionDate
                                                                 && objTradeInfo.PeriodFrom <= periodTo
                                                                 && objTradeInfo.PeriodTo >= periodFrom
                                                           select new BasicTrade { Trade = objTrade, TradeInfo = objTradeInfo };


                if (!acceptDrafts)
                    internalQueryTemp = from objTradeInfo in internalQueryTemp
                                        where objTradeInfo.Trade.State != TradeStateEnum.Draft
                                        select objTradeInfo;


                foreach (HierarchyNodeInfo entityTypeFilter in entityTypeFilters)
                {
                    #region Filter is Domain Object
                    if (entityTypeFilter.DomainObject != null && entityTypeFilter.HierarchyNodeType != null)
                    {
                        if (entityTypeFilter.DomainObject.GetType() == typeof(Company) && entityTypeFilter.HierarchyNodeType.Code == "CMP"
                            && selectedTypeNode == entityTypeFilter)
                        {
                            long companyId = ((Company)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.TradeInfo.CompanyId == companyId
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Market))
                        {
                            long marketId = ((Market)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.TradeInfo.MarketId == marketId
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Trader))
                        {
                            long traderId = ((Trader)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.TradeInfo.TraderId == traderId
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Company) && entityTypeFilter.HierarchyNodeType.Code == "BRK")//Broker
                        {
                            long brokerId = ((Company)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                from objTradeInfoBroker in dataContext.TradeBrokerInfos
                                                where objTradeInfo.TradeInfo.Id == objTradeInfoBroker.TradeInfoId &&
                                                      objTradeInfoBroker.BrokerId == brokerId
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "IDX")
                        {
                            long indexId = ((Index)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.TradeInfo.MTMFwdIndexId == indexId
                                                select objTradeInfo;
                        }

                        #region Vessel
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Vessel))
                        {
                            long vesselId = ((Vessel)entityTypeFilter.DomainObject).Id;

                            tradeTCInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeTcInfo in dataContext.TradeTcInfos
                                                 from objVessel in dataContext.Vessels
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                       && objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                       && objTradeTcInfo.VesselId == vesselId
                                                 select objTradeInfo);

                            tradeCargoInfosQuery = (from objTradeInfo in internalQueryTemp
                                                    from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                          && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                          && objTradeCargoInfo.VesselId != null
                                                          && objTradeCargoInfo.VesselId == vesselId
                                                    select objTradeInfo);

                            tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                        && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                        && objTradeFFAInfo.VesselId != null
                                                        && objTradeFFAInfo.VesselId == vesselId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                     from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                           && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                           && objTradeOptionInfo.VesselId != null
                                                           && objTradeOptionInfo.VesselId == vesselId
                                                     select objTradeInfo);

                            internalQueryTemp = tradeTCInfosQuery.Union(tradeCargoInfosQuery).Union(tradeFFAInfosQuery).Union(tradeOptionInfosQuery);
                        }
                        #endregion

                        #region Vessel Pool
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(VesselPool))
                        {
                            long vesselPoolId = ((VesselPool)entityTypeFilter.DomainObject).Id;

                            tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                        && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                        && objTradeFFAInfo.VesselPoolId != null
                                                        && objTradeFFAInfo.VesselPoolId == vesselPoolId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                     from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                           && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                           && objTradeOptionInfo.VesselPoolId != null
                                                           && objTradeOptionInfo.VesselPoolId == vesselPoolId
                                                     select objTradeInfo);

                            internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        #endregion

                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Desk) && entityTypeFilter.HierarchyNodeType.Code == "DESK")//Desk
                        {
                            long deskId = ((Desk)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                from objTrader in dataContext.Traders
                                                where objTradeInfo.TradeInfo.TraderId == objTrader.Id &&
                                                      objTrader.DeskId == deskId
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(ProfitCentre) && entityTypeFilter.HierarchyNodeType.Code == "PRFCENTRE")//ProfitCenter
                        {
                            long profitCentreId = ((ProfitCentre)entityTypeFilter.DomainObject).Id;
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                from objTrader in dataContext.Traders
                                                where objTradeInfo.TradeInfo.TraderId == objTrader.Id &&
                                                      objTrader.ProfitCentreId == profitCentreId
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(ClearingHouse) && entityTypeFilter.HierarchyNodeType.Code == "OTC")
                        {
                            long clearingHouseId = ((ClearingHouse)entityTypeFilter.DomainObject).Id;

                            tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                        && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                        && objTradeFFAInfo.ClearingHouseId != null
                                                        && objTradeFFAInfo.ClearingHouseId == clearingHouseId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                     from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                           && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                           && objTradeOptionInfo.ClearingHouseId != null
                                                           && objTradeOptionInfo.ClearingHouseId == clearingHouseId
                                                     select objTradeInfo);

                            internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "CRV")
                        {
                            long indexId = ((Index)entityTypeFilter.DomainObject).Id;

                            tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                        && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                        && objTradeFFAInfo.IndexId == indexId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                     from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                           && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                           && objTradeOptionInfo.IndexId == indexId
                                                     select objTradeInfo);

                            internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                    }
                    #endregion

                    #region Filter is Object

                    else if (entityTypeFilter.StrObject != null)
                    {
                        if (entityTypeFilter.HierarchyNodeType.Code == "DLT")
                        {
                            var tradeType = (TradeTypeEnum)Enum.Parse(typeof(TradeTypeEnum), entityTypeFilter.StrObject);
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.Trade.Type == tradeType
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "PFT")// Phsyical/Financial Type
                        {
                            string type = entityTypeFilter.StrObject;
                            List<TradeTypeEnum> tradeTypes = type == "Physical"
                                                                ? new List<TradeTypeEnum>() { TradeTypeEnum.TC, TradeTypeEnum.Cargo }
                                                                : new List<TradeTypeEnum>() { TradeTypeEnum.Option, TradeTypeEnum.FFA };

                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where tradeTypes.Contains(objTradeInfo.Trade.Type)
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "DIR")// Call/Put
                        {
                            var tradeType = (TradeInfoDirectionEnum)Enum.Parse(typeof(TradeInfoDirectionEnum), entityTypeFilter.StrObject);

                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.TradeInfo.Direction == tradeType
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "STA")// Status
                        {
                            var tradeStatus = (TradeStateEnum)Enum.Parse(typeof(TradeStateEnum), entityTypeFilter.StrObject);

                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                where objTradeInfo.Trade.State == tradeStatus
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "CMPTYPE")// Company Type
                        {
                            var companyType = (CompanyTypeEnum)Enum.Parse(typeof(CompanyTypeEnum), entityTypeFilter.StrObject);

                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                from objCompany in dataContext.Companies
                                                where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                      && objCompany.Type == companyType
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "CMPSUBTYPE")//Company SubType
                        {
                            var companySubType = (CompanySubtypeEnum)Enum.Parse(typeof(CompanySubtypeEnum), entityTypeFilter.StrObject);
                            internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                from objCompany in dataContext.Companies
                                                from objCompanySubType in dataContext.CompanySubtypes
                                                where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                      && objCompany.SubtypeId == objCompanySubType.Id
                                                      && objCompanySubType.Subtype == companySubType
                                                select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "OTC")
                        {
                            tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                        && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                        && objTradeFFAInfo.ClearingHouseId == null
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                     from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                           && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                           && objTradeOptionInfo.ClearingHouseId == null
                                                     select objTradeInfo);

                            internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        if (entityTypeFilter.HierarchyNodeType.Code == "LRT")
                        {
                            var tradeRateType =
                                (TradeInfoLegRateTypeEnum)
                                Enum.Parse(typeof(TradeInfoLegRateTypeEnum), entityTypeFilter.StrObject);

                            tradeTcInfoLegsQuery = (from objTradeInfo in internalQueryTemp
                                                    from objTradeTCInfo in dataContext.TradeTcInfos
                                                    from objTradeTCInfoLeg in dataContext.TradeTcInfoLegs
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                          && objTradeInfo.TradeInfo.Id == objTradeTCInfo.TradeInfoId
                                                          && objTradeTCInfo.Id == objTradeTCInfoLeg.TradeTcInfoId
                                                          && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                          && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                          && objTradeTCInfoLeg.RateType == tradeRateType
                                                    select objTradeInfo);

                            tradeCargoInfoLegsQuery = (from objTradeInfo in internalQueryTemp
                                                       from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                       from objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs
                                                       where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                             && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                             && objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                                             && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                             && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                             && objTradeCargoInfoLeg.RateType == tradeRateType
                                                       select objTradeInfo);

                            internalQueryTemp = tradeTcInfoLegsQuery.Union(tradeCargoInfoLegsQuery);
                        }
                    }
                    #endregion
                }

                HierarchyNodeType childNodeType;
                if (hierarchyNodeTypeId == null)//It means that this is the first time the form is loaded so get the parent node of the hierarchy
                {

                    childNodeType = (from objHierarchyNode in dataContext.HierarchyNodes
                                     from objHierarchyNodeType in dataContext.HierarchyNodeTypes
                                     where objHierarchyNode.ParentId == null &&
                                           objHierarchyNode.NodeTypeId == objHierarchyNodeType.Id
                                     select
                                         objHierarchyNodeType
                                    ).SingleOrDefault();

                }
                else //Get next node type
                {
                    childNodeType = (from objParentHierarchyNode in dataContext.HierarchyNodes
                                     from objChildHierarchyNode in dataContext.HierarchyNodes
                                     from objChildHierarchyNodeType in dataContext.HierarchyNodeTypes
                                     where objParentHierarchyNode.NodeTypeId == hierarchyNodeTypeId
                                           && objParentHierarchyNode.Id == objChildHierarchyNode.ParentId
                                           && objChildHierarchyNode.NodeTypeId == objChildHierarchyNodeType.Id
                                     select objChildHierarchyNodeType).SingleOrDefault();
                }

                if (childNodeType == null)
                {
                    return 0;
                }

                #region Get Entities by node type
                long testtemp;
                if (childNodeType.Code == "CMP")
                {
                    IQueryable<long> internalQuery = internalQueryTemp.Select(x => x.TradeInfo.CompanyId);

                    //from objTradeInfo in internalQueryTemp
                    //                             where objTradeInfo.CompanyId == 2039
                    //                             select objTradeInfo.CompanyId;

                    List<long> internalQueryComp = internalQuery.Distinct().ToList();
                    var allcomps = new List<long?>();
                    foreach (long comp in internalQueryComp)
                    {
                        testtemp = comp;
                        AddParentComps(allcomps, testtemp);
                    }

                    if (hierarchyNodeTypeId == null)
                    {
                        hierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                 where allcomps.Contains(objCompany.Id) &&
                                                 objCompany.ParentId == null
                                                 select new HierarchyNodeInfo()
                                                 {
                                                     DomainObject = objCompany,
                                                     HierarchyNodeType = childNodeType
                                                 }).ToList());
                    }
                    else
                    {
                        hierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                 where allcomps.Contains(objCompany.Id)
                                                 select new HierarchyNodeInfo()
                                                 {
                                                     DomainObject = objCompany,
                                                     HierarchyNodeType = childNodeType
                                                 }).ToList());
                    }

                }
                else if (childNodeType.Code == "MRK")
                {
                    IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                     select objTradeInfo.TradeInfo.MarketId;

                    hierarchyNodes.AddRange((from objMarket in dataContext.Markets
                                             where internalQuery.Contains(objMarket.Id)
                                             select new HierarchyNodeInfo()
                                             {
                                                 DomainObject = objMarket,
                                                 HierarchyNodeType = childNodeType
                                             }).ToList());
                }
                else if (childNodeType.Code == "TRD")
                {
                    IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                     select objTradeInfo.TradeInfo.TraderId;

                    hierarchyNodes.AddRange((from objTrader in dataContext.Traders
                                             where internalQuery.Contains(objTrader.Id)
                                             select new HierarchyNodeInfo()
                                             {
                                                 DomainObject = objTrader,
                                                 HierarchyNodeType = childNodeType
                                             }).ToList());
                }
                else if (childNodeType.Code == "DLT")//Deal Type
                {
                    List<TradeTypeEnum> internalQuery = (from objTradeInfo in internalQueryTemp
                                                         select objTradeInfo.Trade.Type).Distinct().ToList();

                    hierarchyNodes.AddRange(internalQuery.Select(tradeTypeEnum => new HierarchyNodeInfo()
                    {
                        StrObject = tradeTypeEnum.ToString("g"),
                        HierarchyNodeType = childNodeType
                    }));
                }
                else if (childNodeType.Code == "BRK")//Broker
                {
                    IQueryable<long> internalQuery = (from objTradeInfo in internalQueryTemp
                                                      from objTradeInfoBroker in dataContext.TradeBrokerInfos
                                                      where objTradeInfo.TradeInfo.Id == objTradeInfoBroker.TradeInfoId
                                                      select objTradeInfoBroker.BrokerId);

                    hierarchyNodes.AddRange((from objBroker in dataContext.Companies
                                             where internalQuery.Contains(objBroker.Id)
                                             select new HierarchyNodeInfo()
                                             {
                                                 DomainObject = objBroker,
                                                 HierarchyNodeType = childNodeType
                                             }).ToList());
                }

                #region Vessel

                else if (childNodeType.Code == "VSL")//Vessel
                {
                    IQueryable<long> vessels;

                    IQueryable<long> TcVesselIds = (from objTradeInfo in internalQueryTemp
                                                    from objTradeTcInfo in dataContext.TradeTcInfos
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                          && objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                    select objTradeTcInfo.VesselId);

                    IQueryable<long> CargoVesselIds = (from objTradeInfo in internalQueryTemp
                                                       from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                       where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                             && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                             && objTradeCargoInfo.VesselId != null
                                                       select (long)objTradeCargoInfo.VesselId);

                    IQueryable<long> FFAVesselIds = (from objTradeInfo in internalQueryTemp
                                                     from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                           && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                           && objTradeFFAInfo.VesselId != null
                                                     select (long)objTradeFFAInfo.VesselId);

                    IQueryable<long> OptionVesselIds = (from objTradeInfo in internalQueryTemp
                                                        from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                        where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                              && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                              && objTradeOptionInfo.VesselId != null
                                                        select (long)objTradeOptionInfo.VesselId);

                    vessels = TcVesselIds.Union(CargoVesselIds).Union(FFAVesselIds).Union(OptionVesselIds).Distinct();

                    hierarchyNodes.AddRange((from objVessel in dataContext.Vessels
                                             where vessels.Contains(objVessel.Id)
                                             select new HierarchyNodeInfo()
                                             {
                                                 DomainObject = objVessel,
                                                 HierarchyNodeType = childNodeType
                                             }));
                }
                #endregion

                else if (childNodeType.Code == "PFT")//Physical/Financial Type
                {
                    List<TradeTypeEnum> tradeTypes = (from objTradeInfo in internalQueryTemp
                                                      select objTradeInfo.Trade.Type).ToList();

                    if (tradeTypes.Contains(TradeTypeEnum.Cargo) || tradeTypes.Contains(TradeTypeEnum.TC))
                        hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Physical", HierarchyNodeType = childNodeType });
                    if (tradeTypes.Contains(TradeTypeEnum.Option) || tradeTypes.Contains(TradeTypeEnum.FFA))
                        hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Financial", HierarchyNodeType = childNodeType });
                }
                else if (childNodeType.Code == "LRT")//Leg Rate Type
                {
                    List<TradeInfoLegRateTypeEnum> rateTypes;

                    List<TradeInfoLegRateTypeEnum> TcRateTypes = (from objTradeInfo in internalQueryTemp
                                                                  from objTradeTcInfo in dataContext.TradeTcInfos
                                                                  from objTradeTcInfoLeg in dataContext.TradeTcInfoLegs
                                                                  where objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                                        &&
                                                                        objTradeTcInfo.Id ==
                                                                        objTradeTcInfoLeg.TradeTcInfoId
                                                                  select objTradeTcInfoLeg.RateType).Distinct().ToList();

                    List<TradeInfoLegRateTypeEnum> CargoRateTypes = (from objTradeInfo in internalQueryTemp
                                                                     from objTradeCargoInfo in
                                                                         dataContext.TradeCargoInfos
                                                                     from objTradeCargoInfoLeg in
                                                                         dataContext.TradeCargoInfoLegs
                                                                     where objTradeInfo.TradeInfo.Id ==
                                                                           objTradeCargoInfo.TradeInfoId
                                                                           &&
                                                                           objTradeCargoInfo.Id ==
                                                                           objTradeCargoInfoLeg.TradeCargoInfoId
                                                                     select objTradeCargoInfoLeg.RateType).Distinct().ToList();

                    rateTypes = TcRateTypes.Union(CargoRateTypes).Distinct().ToList();

                    hierarchyNodes.AddRange(rateTypes.Select(legRateType => new HierarchyNodeInfo()
                    {
                        StrObject = legRateType.ToString("g"),
                        HierarchyNodeType = childNodeType
                    }));
                }
                else if (childNodeType.Code == "OPT")//Optiona/Minimum Leg
                {
                    List<bool> TcIsOptional = (from objTradeInfo in internalQueryTemp
                                               from objTradeTcInfo in dataContext.TradeTcInfos
                                               from objTradeTcInfoLeg in dataContext.TradeTcInfoLegs
                                               where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                     && objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                     && objTradeTcInfo.Id ==
                                                     objTradeTcInfoLeg.TradeTcInfoId
                                               select objTradeTcInfoLeg.IsOptional).ToList();

                    List<bool> CargoIsOptional = (from objTradeInfo in internalQueryTemp
                                                  from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                  from objTradeCargoInfoLeg in
                                                      dataContext.TradeCargoInfoLegs
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                        && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                        && objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                                  select objTradeCargoInfoLeg.IsOptional).Distinct().ToList();

                    List<bool> isOptional = TcIsOptional.Union(CargoIsOptional).Distinct().ToList();

                    if (isOptional.Contains(true))
                        hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Optional", HierarchyNodeType = childNodeType });
                    if (isOptional.Contains(false))
                        hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Minimum", HierarchyNodeType = childNodeType });
                }
                else if (childNodeType.Code == "SETPERIOD")//Settlement Period
                {
                    List<string> ffaPeriods = (from objTradeInfo in internalQueryTemp
                                               from objTradeFFAInfo in dataContext.TradeFfaInfos
                                               where objTradeFFAInfo.TradeInfoId == objTradeInfo.TradeInfo.Id
                                               select objTradeFFAInfo.Period).Distinct().ToList();

                    List<string> optionPeriods = (from objTradeInfo in internalQueryTemp
                                                  from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                  where objTradeOptionInfo.TradeInfoId == objTradeInfo.TradeInfo.Id
                                                  select objTradeOptionInfo.Period).Distinct().ToList();

                    List<string> periods = ffaPeriods.Union(optionPeriods).Distinct().ToList();

                    foreach (var period in periods)
                    {
                        hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = period, HierarchyNodeType = childNodeType });
                    }
                }
                else if (childNodeType.Code == "DIR")//Call/Put
                {
                    hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeInfoDirectionEnum.InOrBuy.ToString("g"), HierarchyNodeType = childNodeType });
                    hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeInfoDirectionEnum.OutOrSell.ToString("g"), HierarchyNodeType = childNodeType });
                }
                else if (childNodeType.Code == "STA")//Status
                {
                    hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Confirmed.ToString("g"), HierarchyNodeType = childNodeType });
                    hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Draft.ToString("g"), HierarchyNodeType = childNodeType });
                    hierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Locked.ToString("g"), HierarchyNodeType = childNodeType });
                }

                #region Vessel Pool
                else if (childNodeType.Code == "VSLP")//Vessel pool
                {
                    IQueryable<long> vesselPools;


                    IQueryable<long> FFAVesselPoolIds = (from objTradeInfo in internalQueryTemp
                                                         from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                         where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                               && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                               && objTradeFFAInfo.VesselPoolId != null
                                                         select (long)objTradeFFAInfo.VesselPoolId);

                    IQueryable<long> OptionVesselPoolIds = (from objTradeInfo in internalQueryTemp
                                                            from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                            where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                                  && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                                  && objTradeOptionInfo.VesselPoolId != null
                                                            select (long)objTradeOptionInfo.VesselPoolId);

                    vesselPools = FFAVesselPoolIds.Union(OptionVesselPoolIds).Distinct();

                    hierarchyNodes = new List<HierarchyNodeInfo>((from objVesselPool in dataContext.VesselPools
                                                                  where vesselPools.Contains(objVesselPool.Id)
                                                                  select new HierarchyNodeInfo()
                                                                  {
                                                                      DomainObject = objVesselPool,
                                                                      HierarchyNodeType = childNodeType
                                                                  }));
                }
                #endregion

                else if (childNodeType.Code == "CMPTYPE")//Company Type
                {
                    List<CompanyTypeEnum> companyTypes = (from objTradeInfo in internalQueryTemp
                                                          from objCompany in dataContext.Companies
                                                          where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                          select objCompany.Type).Distinct().ToList();

                    hierarchyNodes.AddRange(companyTypes.Select(companyType => new HierarchyNodeInfo()
                    {
                        StrObject = companyType.ToString("g"),
                        HierarchyNodeType = childNodeType
                    }));
                }
                else if (childNodeType.Code == "DESK")
                {
                    List<Desk> desks = (from objTradeInfo in internalQueryTemp
                                        from objTrader in dataContext.Traders
                                        from objDesk in dataContext.Desks
                                        where objTradeInfo.TradeInfo.TraderId == objTrader.Id
                                              && objTrader.DeskId == objDesk.Id
                                        select objDesk).Distinct().ToList();

                    hierarchyNodes.AddRange(desks.Select(desk => new HierarchyNodeInfo() { DomainObject = desk, HierarchyNodeType = childNodeType }));
                }
                else if (childNodeType.Code == "PRFCENTRE")
                {
                    List<ProfitCentre> profitCentres = (from objTradeInfo in internalQueryTemp
                                                        from objTrader in dataContext.Traders
                                                        from objProfitCentre in dataContext.ProfitCentres
                                                        where objTradeInfo.TradeInfo.TraderId == objTrader.Id
                                                              && objTrader.ProfitCentreId == objProfitCentre.Id
                                                        select objProfitCentre).Distinct().ToList();

                    hierarchyNodes.AddRange(profitCentres.Select(profiCentre => new HierarchyNodeInfo() { DomainObject = profiCentre, HierarchyNodeType = childNodeType }));
                }
                else if (childNodeType.Code == "CMPSUBTYPE")//Company Sub Type
                {
                    List<CompanySubtypeEnum> internalQuery = (from objTradeInfo in internalQueryTemp
                                                              from objCompany in dataContext.Companies
                                                              from objCompanySubType in dataContext.CompanySubtypes
                                                              where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                                    && objCompany.SubtypeId == objCompanySubType.Id
                                                              select objCompanySubType.Subtype).Distinct().ToList();

                    hierarchyNodes.AddRange(internalQuery.Select(tradeTypeEnum => new HierarchyNodeInfo()
                    {
                        StrObject =
                            tradeTypeEnum.ToString("g"),
                        HierarchyNodeType =
                            childNodeType
                    }));
                }
                else if (childNodeType.Code == "IDX")
                {
                    IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                     select objTradeInfo.TradeInfo.MTMFwdIndexId;

                    hierarchyNodes = new List<HierarchyNodeInfo>((from objIndex in dataContext.Indexes
                                                                  where internalQuery.Contains(objIndex.Id)
                                                                  select new HierarchyNodeInfo()
                                                                  {
                                                                      DomainObject = objIndex,
                                                                      HierarchyNodeType = childNodeType
                                                                  }).ToList());
                }
                else if (childNodeType.Code == "OTC")
                {
                    IQueryable<long> clearingHousesId;

                    IQueryable<long> FFAclearingHousesIds = (from objTradeInfo in internalQueryTemp
                                                             from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                             where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                                   && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                                   && objTradeFFAInfo.ClearingHouseId != null
                                                             select (long)objTradeFFAInfo.ClearingHouseId);

                    IQueryable<long> OptionclearingHousesIds = (from objTradeInfo in internalQueryTemp
                                                                from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                                where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                                      &&
                                                                      objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                                      && objTradeOptionInfo.ClearingHouseId != null
                                                                select (long)objTradeOptionInfo.ClearingHouseId);

                    clearingHousesId = FFAclearingHousesIds.Union(OptionclearingHousesIds).Distinct();

                    hierarchyNodes = new List<HierarchyNodeInfo>((from objClearingHouse in dataContext.Clearinghouses
                                                                  where clearingHousesId.Contains(objClearingHouse.Id)
                                                                  select new HierarchyNodeInfo()
                                                                  {
                                                                      DomainObject = objClearingHouse,
                                                                      HierarchyNodeType = childNodeType
                                                                  }));
                    hierarchyNodes.Add(new HierarchyNodeInfo()
                    {
                        HierarchyNodeType = childNodeType,
                        StrObject = "OTC"
                    });
                }
                else if (childNodeType.Code == "CRV")
                {
                    IQueryable<long> indexIds;

                    IQueryable<long> FFAIndexIds = (from objTradeInfo in internalQueryTemp
                                                    from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                          && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    select (long)objTradeFFAInfo.IndexId);

                    IQueryable<long> OptionIndexIds = (from objTradeInfo in internalQueryTemp
                                                       from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                       where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                             && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       select (long)objTradeOptionInfo.IndexId);

                    indexIds = FFAIndexIds.Union(OptionIndexIds).Distinct();

                    hierarchyNodes = new List<HierarchyNodeInfo>((from objIndex in dataContext.Indexes
                                                                  where indexIds.Contains(objIndex.Id)
                                                                  select new HierarchyNodeInfo()
                                                                  {
                                                                      DomainObject = objIndex,
                                                                      HierarchyNodeType = childNodeType
                                                                  }));
                }

                #endregion

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }
        
        public int? GetTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays, List<HierarchyNodeInfo> entityTypeFilters, out List<Trade> trades, out List<Company> companies)
        {
            trades = new List<Trade>();

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetTradesByFilters2: " };
                }

                List<long> tradeInfoIds;

                IQueryable<BasicTrade> tradeInfoQuery;
                IQueryable<BasicTrade> tradeTCInfosQuery;
                IQueryable<BasicTrade> tradeCargoInfosQuery;
                IQueryable<BasicTrade> tradeFFAInfosQuery;
                IQueryable<BasicTrade> tradeOptionInfosQuery;
                IQueryable<BasicTrade> tradeTcInfoLegsQuery;
                IQueryable<BasicTrade> tradeCargoInfoLegsQuery;



                if (entityTypeFilters.Count > 0 && entityTypeFilters[0].DomainObject != null &&
                    entityTypeFilters[0].DomainObject.GetType() == typeof(Book))
                {
                    long bookId = ((Book)entityTypeFilters[0].DomainObject).Id;
                    var q = (from objTrade in dataContext.Trades
                             join objTradeInfo in dataContext.TradeInfos on objTrade.Id equals objTradeInfo.TradeId
                             join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                             join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                             where objTradeInfoBook.BookId == bookId
                                   && objTradeBook.Status == ActivationStatusEnum.Active
                                   && ((objTradeInfo.DateFrom <= positionDate & objTradeInfo.DateTo > positionDate) || objTradeInfo.DateTo == null)                                   
                                   && objTradeInfo.SignDate <= positionDate
                                   && objTradeInfo.PeriodFrom <= periodTo
                                   && objTradeInfo.PeriodTo >= periodFrom
                                   && objTrade.Status == ActivationStatusEnum.Active
                             group objTradeInfo by objTradeInfo.TradeId
                                 into g
                             select new { MaxTradeInfoId = (from objTradeInfo2 in g select objTradeInfo2.Id).Max() });

                    tradeInfoQuery = from objTradeInfo in dataContext.TradeInfos
                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                     join objMaxTradeInfoId in q on objTradeInfo.Id equals objMaxTradeInfoId.MaxTradeInfoId

                                     select new BasicTrade { Trade = objTrade, TradeInfo = objTradeInfo };

                    companies = null;
                }
                else
                {

                    companies = null;
                    var q = (from objTradeInfo in dataContext.TradeInfos
                             join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                             join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                             join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                             where objTradeBook.Status == ActivationStatusEnum.Active
                                   && ((objTradeInfo.DateFrom <= positionDate & objTradeInfo.DateTo > positionDate) || objTradeInfo.DateTo == null)
                                   && objTrade.Status == ActivationStatusEnum.Active
                                   && objTradeInfo.SignDate <= positionDate
                                   && objTradeInfo.PeriodFrom <= periodTo
                                   && objTradeInfo.PeriodTo >= periodFrom
                             group objTradeInfo by objTradeInfo.TradeId
                                 into g
                             select new { MaxTradeInfoId = (from objTradeInfo2 in g select objTradeInfo2.Id).Max() });

                    tradeInfoIds = (from objTradeInfo in dataContext.TradeInfos
                                    join objMaxTradeInfoId in q
                                    on objTradeInfo.Id equals objMaxTradeInfoId.MaxTradeInfoId
                                    select objTradeInfo.Id).ToList();

                    //if (tradeInfoIds.Count < 1000)
                    //    tradeInfoQuery = dataContext.TradeInfos.Where(a => tradeInfoIds.Contains(a.Id));
                    //else
                    tradeInfoQuery = from objTradeInfo in dataContext.TradeInfos
                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                     join objMaxTradeInfoId in q
                                     on objTradeInfo.Id equals objMaxTradeInfoId.MaxTradeInfoId
                                     select new BasicTrade { Trade = objTrade, TradeInfo = objTradeInfo }; 


                }

                if (!acceptDrafts)
                    tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                     where objTradeInfo.Trade.State != TradeStateEnum.Draft

                                     select objTradeInfo;

                if(onlyNonZeroTotalDays)
                {
                    tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                     join objTradeFFAInfoL in dataContext.TradeFfaInfos
                                     on objTradeInfo.TradeInfo.Id equals objTradeFFAInfoL.TradeInfoId into objTradeFFAInfoLefts
                                     from objTradeFFAInfo in objTradeFFAInfoLefts.DefaultIfEmpty()

                                     where objTradeInfo.Trade.Type != TradeTypeEnum.FFA ||
                                     (objTradeInfo.Trade.Type == TradeTypeEnum.FFA && objTradeFFAInfo.QuantityDays - objTradeFFAInfo.ClosedOutQuantityTotal > 0)
                                     


                                     select objTradeInfo;
                }
                
                #region Filter Query by company
                List<long> comps = new List<long>();
                List<long> results = new List<long>();
                foreach (HierarchyNodeInfo entityTypeFilter in entityTypeFilters)
                {
                    if (entityTypeFilter.DomainObject != null && entityTypeFilter.HierarchyNodeType != null)
                    {
                        if (entityTypeFilter.DomainObject.GetType() == typeof(Company) &&
                          entityTypeFilter.HierarchyNodeType.Code == "CMP")
                        {

                            long companyId = ((Company)entityTypeFilter.DomainObject).Id;
                            comps.Add(companyId);
                            //companies.Add(((Company)entityTypeFilter.DomainObject));
                            var test = entityTypeFilter.HierarchyNodeType.Code;
                        }

                    }
                }



                //Filter with companies and child companies
                tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                 where comps.Contains(objTradeInfo.TradeInfo.CompanyId)
                                 select objTradeInfo;

                var cmp = (from objComp in dataContext.Companies
                           where comps.Contains(objComp.Id)
                           select objComp
                            );

                #endregion

                foreach (HierarchyNodeInfo entityTypeFilter in entityTypeFilters)
                {
                    #region Filter Query

                    if (entityTypeFilter.DomainObject != null && entityTypeFilter.HierarchyNodeType != null)
                    {
                        if (entityTypeFilter.DomainObject.GetType() == typeof(Market))
                        {
                            long marketId = ((Market)entityTypeFilter.DomainObject).Id;

                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where objTradeInfo.TradeInfo.MarketId == marketId
                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Trader))
                        {
                            long traderId = ((Trader)entityTypeFilter.DomainObject).Id;
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where objTradeInfo.TradeInfo.TraderId == traderId
                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Company) &&
                                 entityTypeFilter.HierarchyNodeType.Code == "BRK") //Broker
                        {
                            long brokerId = ((Company)entityTypeFilter.DomainObject).Id;
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             join objTradeInfoBroker in dataContext.TradeBrokerInfos on objTradeInfo.TradeInfo.Id equals objTradeInfoBroker.TradeInfoId
                                             where objTradeInfoBroker.BrokerId == brokerId

                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Vessel))
                        {
                            long vesselId = ((Vessel)entityTypeFilter.DomainObject).Id;

                            tradeTCInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                 join objTradeTcInfo in dataContext.TradeTcInfos on objTradeInfo.TradeInfo.Id equals objTradeTcInfo.TradeInfoId                                                 
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.TC


                                                       && objTradeTcInfo.VesselId == vesselId
                                                 select objTradeInfo);

                            tradeCargoInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                    join objTradeCargoInfo in dataContext.TradeCargoInfos on objTradeInfo.TradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo


                                                          && objTradeCargoInfo.VesselId == vesselId
                                                    select objTradeInfo);

                            tradeFFAInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                  join objTradeFfaInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals objTradeFfaInfo.TradeInfoId
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA


                                                        && objTradeFfaInfo.VesselId == vesselId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id equals objTradeOptionInfo.TradeInfoId
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option


                                                           && objTradeOptionInfo.VesselId == vesselId
                                                     select objTradeInfo);

                            tradeInfoQuery =
                                tradeTCInfosQuery.Union(tradeCargoInfosQuery).Union(tradeFFAInfosQuery).Union(
                                    tradeOptionInfosQuery);
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Desk) &&
                                 entityTypeFilter.HierarchyNodeType.Code == "DESK") //Desk
                        {
                            long deskId = ((Desk)entityTypeFilter.DomainObject).Id;
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             join objTrader in dataContext.Traders on objTradeInfo.TradeInfo.TraderId equals objTrader.Id
                                             where objTrader.DeskId == deskId

                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(ProfitCentre) &&
                                 entityTypeFilter.HierarchyNodeType.Code == "PRFCENTRE") //ProfitCenter
                        {
                            long profitCentreId = ((ProfitCentre)entityTypeFilter.DomainObject).Id;
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             join objTrader in dataContext.Traders on objTradeInfo.TradeInfo.TraderId equals objTrader.Id
                                             where objTrader.ProfitCentreId == profitCentreId

                                             select objTradeInfo;
                        }

                        #region Vessel Pool

                        else if (entityTypeFilter.DomainObject.GetType() == typeof(VesselPool))
                        {
                            long vesselPoolId = ((VesselPool)entityTypeFilter.DomainObject).Id;

                            tradeFFAInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                  join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals objTradeFFAInfo.TradeInfoId
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA


                                                        && objTradeFFAInfo.VesselPoolId != null
                                                        && objTradeFFAInfo.VesselPoolId == vesselPoolId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id equals objTradeOptionInfo.TradeInfoId
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option



                                                           && objTradeOptionInfo.VesselPoolId != null
                                                           && objTradeOptionInfo.VesselPoolId == vesselPoolId

                                                     select objTradeInfo);

                            tradeInfoQuery = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        #endregion

                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "IDX")
                        {
                            long indexId = ((Index)entityTypeFilter.DomainObject).Id;
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where objTradeInfo.TradeInfo.MTMFwdIndexId == indexId
                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(ClearingHouse) &&
                                 entityTypeFilter.HierarchyNodeType.Code == "OTC")
                        {
                            long clearingHouseId = ((ClearingHouse)entityTypeFilter.DomainObject).Id;

                            tradeFFAInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                  join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals objTradeFFAInfo.TradeInfoId
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA



                                                        && objTradeFFAInfo.ClearingHouseId != null
                                                        && objTradeFFAInfo.ClearingHouseId == clearingHouseId


                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id equals objTradeOptionInfo.TradeInfoId

                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option




                                                           && objTradeOptionInfo.ClearingHouseId != null
                                                           && objTradeOptionInfo.ClearingHouseId == clearingHouseId


                                                     select objTradeInfo);

                            tradeInfoQuery = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "CRV")
                        {
                            long indexId = ((Index)entityTypeFilter.DomainObject).Id;

                            tradeFFAInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                  join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals objTradeFFAInfo.TradeInfoId
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA


                                                        && objTradeFFAInfo.IndexId == indexId
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id equals objTradeOptionInfo.TradeInfoId
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option


                                                           && objTradeOptionInfo.IndexId == indexId
                                                     select objTradeInfo);

                            tradeInfoQuery = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                    }
                    else if (entityTypeFilter.StrObject != null)
                    {
                        if (entityTypeFilter.HierarchyNodeType.Code == "DLT") //Deal Type
                        {
                            var tradeType =
                                (TradeTypeEnum)Enum.Parse(typeof(TradeTypeEnum), entityTypeFilter.StrObject);
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where objTradeInfo.Trade.Type == tradeType

                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "PFT") // Phsyical/Financial Type
                        {
                            string type = entityTypeFilter.StrObject;
                            List<TradeTypeEnum> tradeTypes = type == "Physical"
                                                                 ? new List<TradeTypeEnum>() { TradeTypeEnum.TC, TradeTypeEnum.Cargo }
                                                                 : new List<TradeTypeEnum>() { TradeTypeEnum.Option, TradeTypeEnum.FFA };

                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where tradeTypes.Contains(objTradeInfo.Trade.Type)

                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "SETPERIOD")
                        {
                            string period = entityTypeFilter.StrObject;

                            tradeFFAInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                  join objTradeFfaInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals objTradeFfaInfo.TradeInfoId
                                                  where objTradeFfaInfo.Period == period

                                                  select objTradeInfo
                                                 );

                            tradeOptionInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id equals objTradeOptionInfo.TradeInfoId
                                                     where objTradeOptionInfo.Period == period

                                                     select objTradeInfo
                                                    );

                            tradeInfoQuery = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "DIR") // Call/Put
                        {
                            var tradeType =
                                (TradeInfoDirectionEnum)
                                Enum.Parse(typeof(TradeInfoDirectionEnum), entityTypeFilter.StrObject);

                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where objTradeInfo.TradeInfo.Direction == tradeType

                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "STA") // Status
                        {
                            var tradeStatus =
                                (TradeStateEnum)Enum.Parse(typeof(TradeStateEnum), entityTypeFilter.StrObject);

                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             where objTradeInfo.Trade.State == tradeStatus

                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "CMPTYPE") // Company Type
                        {
                            var companyType =
                                (CompanyTypeEnum)Enum.Parse(typeof(CompanyTypeEnum), entityTypeFilter.StrObject);

                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             join objCompany in dataContext.Companies on objTradeInfo.TradeInfo.CompanyId equals objCompany.Id
                                             where objCompany.Type == companyType


                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "CMPSUBTYPE") //Company SubType
                        {
                            var companySubType =
                                (CompanySubtypeEnum)Enum.Parse(typeof(CompanySubtypeEnum), entityTypeFilter.StrObject);
                            tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                             join objCompany in dataContext.Companies on objTradeInfo.TradeInfo.CompanyId equals objCompany.Id
                                             join objCompanySubType in dataContext.CompanySubtypes on objCompany.SubtypeId equals objCompanySubType.Id
                                             where objCompanySubType.Subtype == companySubType



                                             select objTradeInfo;
                        }
                        else if (entityTypeFilter.HierarchyNodeType.Code == "OTC")
                        {
                            tradeFFAInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                  join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals objTradeFFAInfo.TradeInfoId
                                                  where objTradeInfo.Trade.Type == TradeTypeEnum.FFA


                                                        && objTradeFFAInfo.ClearingHouseId == null
                                                  select objTradeInfo);

                            tradeOptionInfosQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id equals objTradeOptionInfo.TradeInfoId
                                                     where objTradeInfo.Trade.Type == TradeTypeEnum.Option


                                                           && objTradeOptionInfo.ClearingHouseId == null
                                                     select objTradeInfo);

                            tradeInfoQuery = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                        }
                        if (entityTypeFilter.HierarchyNodeType.Code == "LRT")
                        {
                            var tradeRateType =
                                (TradeInfoLegRateTypeEnum)
                                Enum.Parse(typeof(TradeInfoLegRateTypeEnum), entityTypeFilter.StrObject);

                            tradeTcInfoLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                    join objTradeTCInfo in dataContext.TradeTcInfos on objTradeInfo.TradeInfo.Id equals objTradeTCInfo.TradeInfoId
                                                    join objTradeTCInfoLeg in dataContext.TradeTcInfoLegs on objTradeTCInfo.Id equals objTradeTCInfoLeg.TradeTcInfoId
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.TC



                                                          && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                          && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                          && objTradeTCInfoLeg.RateType == tradeRateType
                                                    select objTradeInfo);

                            tradeCargoInfoLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                       join objTradeCargoInfo in dataContext.TradeCargoInfos on objTradeInfo.TradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                       join objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs on objTradeCargoInfo.Id equals objTradeCargoInfoLeg.TradeCargoInfoId
                                                       where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo



                                                             && objTradeCargoInfoLeg.RateType == tradeRateType
                                                             && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                             && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                       select objTradeInfo);

                            tradeInfoQuery = tradeTcInfoLegsQuery.Union(tradeCargoInfoLegsQuery);
                        }
                    }


                    #endregion
                }

                var anonTradeInfoList = (from objTradeInfo in tradeInfoQuery
                                         join objCompany in dataContext.Companies on objTradeInfo.TradeInfo.CompanyId equals objCompany.Id
                                         join objCounterparty in dataContext.Companies on objTradeInfo.TradeInfo.CounterpartyId equals objCounterparty.Id
                                         join objTrader in dataContext.Traders on objTradeInfo.TradeInfo.TraderId equals objTrader.Id
                                         join objMarket in dataContext.Markets on objTradeInfo.TradeInfo.MarketId equals objMarket.Id
                                         join objRegion in dataContext.Regions on objTradeInfo.TradeInfo.RegionId equals objRegion.Id
                                         join objRoute in dataContext.Routes on objTradeInfo.TradeInfo.RouteId equals objRoute.Id
                                         join objMTMFwdIndex in dataContext.Indexes on
                                             objTradeInfo.TradeInfo.MTMFwdIndexId equals objMTMFwdIndex.Id
                                         select
                                             new
                                             {
                                                 Trade = objTradeInfo.Trade,
                                                 TradeInfo = objTradeInfo.TradeInfo,
                                                 CompanyName = objCompany.Name,
                                                 Ownership = objCompany.Ownership,
                                                 Subtype = objCompany.SubtypeId, //
                                                 CounterpartyName = objCounterparty.Name,
                                                 TraderName = objTrader.Name,
                                                 MarketName = objMarket.Name,
                                                 MarketTonnes = objMarket.Tonnes,
                                                 RegionName = objRegion.Name,
                                                 RouteName = objRoute.Name,
                                                 MTMFwdIndexName = objMTMFwdIndex.Name
                                             }).ToList();

                var tradeBrokerInfos = (from objTradeInfo in tradeInfoQuery
                                        join objTradeBrokerInfo in dataContext.TradeBrokerInfos on objTradeInfo.TradeInfo.Id
                                            equals objTradeBrokerInfo.TradeInfoId
                                        join objBroker in dataContext.Companies on objTradeBrokerInfo.BrokerId equals
                                            objBroker.Id
                                        select new
                                        {
                                            objBroker,
                                            objTradeBrokerInfo,
                                            objTradeInfo.TradeInfo.Id
                                        }).ToList();

                var tradeInfoBooks = (from objTradeInfo in tradeInfoQuery
                                      join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.TradeInfo.Id equals objTradeInfoBook.TradeInfoId
                                      join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                      where objTradeBook.Status == ActivationStatusEnum.Active
                                      select new
                                      {
                                          objTradeInfoBook,
                                          objTradeInfo.TradeInfo.Id
                                      }).ToList();


                IQueryable<TcTradeLegInfo> tradeTCInfosWithLegsQuery;
                IQueryable<CargoTradeLegInfo> tradeCargoInfosWithLegsQuery;

                if (entityTypeFilters.Count > 0 && entityTypeFilters[0].DomainObject != null &&
                    entityTypeFilters[0].DomainObject.GetType() == typeof(Book))
                {
                    tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                 join objTradeTCInfo in dataContext.TradeTcInfos on objTradeInfo.TradeInfo.Id
                                                     equals objTradeTCInfo.TradeInfoId
                                                 join objTradeTCInfoLeg in dataContext.TradeTcInfoLegs on
                                                     objTradeTCInfo.Id equals objTradeTCInfoLeg.TradeTcInfoId
                                                 join objVessel in dataContext.Vessels on objTradeTCInfo.VesselId equals
                                                     objVessel.Id
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                       && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                       && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                 orderby objTradeTCInfoLeg.PeriodFrom
                                                 select
                                                     new TcTradeLegInfo
                                                     {
                                                         TradeTCInfoLeg = objTradeTCInfoLeg,
                                                         TradeTCInfo = objTradeTCInfo,
                                                         TradeInfoId = objTradeInfo.TradeInfo.Id,
                                                         VesselName = objVessel.Name
                                                     });

                    tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                    join objTradeCargoInfo in dataContext.TradeCargoInfos on
                                                        objTradeInfo.TradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                    join objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs on
                                                        objTradeCargoInfo.Id equals
                                                        objTradeCargoInfoLeg.TradeCargoInfoId
                                                    join objVesselL in dataContext.Vessels
                                                        on objTradeCargoInfo.VesselId equals objVesselL.Id into
                                                        objVesselLefts
                                                    from objVessel in objVesselLefts.DefaultIfEmpty()
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                    && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                    && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                    orderby objTradeCargoInfoLeg.PeriodFrom
                                                    select
                                                        new CargoTradeLegInfo
                                                        {
                                                            TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                            TradeCargoInfo = objTradeCargoInfo,
                                                            TradeInfoId = objTradeInfo.TradeInfo.Id,
                                                            VesselName = objVessel.Name
                                                        });

                    if (!acceptMinimum)
                    {
                        tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                    where objAnon.TradeTCInfoLeg.IsOptional
                                                    select objAnon;

                        tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                       where objAnon.TradeCargoInfoLeg.IsOptional
                                                       select objAnon;
                    }

                    if (!acceptOptional)
                    {
                        tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                    where objAnon.TradeTCInfoLeg.IsOptional == false
                                                    select objAnon;

                        tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                       where objAnon.TradeCargoInfoLeg.IsOptional == false
                                                       select objAnon;
                    }
                }
                else
                {
                    tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                 join objTradeTCInfo in dataContext.TradeTcInfos on objTradeInfo.TradeInfo.Id
                                                     equals objTradeTCInfo.TradeInfoId
                                                 join objTradeTCInfoLeg in dataContext.TradeTcInfoLegs on
                                                     objTradeTCInfo.Id equals objTradeTCInfoLeg.TradeTcInfoId
                                                 join objVessel in dataContext.Vessels on objTradeTCInfo.VesselId equals
                                                     objVessel.Id
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                       && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                       && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                 orderby objTradeTCInfoLeg.PeriodFrom
                                                 select
                                                     new TcTradeLegInfo
                                                     {
                                                         TradeTCInfoLeg = objTradeTCInfoLeg,
                                                         TradeTCInfo = objTradeTCInfo,
                                                         TradeInfoId = objTradeInfo.TradeInfo.Id,
                                                         VesselName = objVessel.Name
                                                     });
                    

                    tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                    join objTradeCargoInfo in dataContext.TradeCargoInfos on
                                                        objTradeInfo.TradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                    join objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs on
                                                        objTradeCargoInfo.Id equals
                                                        objTradeCargoInfoLeg.TradeCargoInfoId
                                                    join objVesselL in dataContext.Vessels
                                                        on objTradeCargoInfo.VesselId equals objVesselL.Id into
                                                        objVesselLefts
                                                    from objVessel in objVesselLefts.DefaultIfEmpty()
                                                    where objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                          && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                          && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                    orderby objTradeCargoInfoLeg.PeriodFrom
                                                    select
                                                        new CargoTradeLegInfo
                                                        {
                                                            TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                            TradeCargoInfo = objTradeCargoInfo,
                                                            TradeInfoId = objTradeInfo.TradeInfo.Id,
                                                            VesselName = objVessel.Name
                                                        });

                    if (!acceptMinimum)
                    {
                        tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                    where objAnon.TradeTCInfoLeg.IsOptional
                                                    select objAnon;

                        tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                       where objAnon.TradeCargoInfoLeg.IsOptional
                                                       select objAnon;
                    }

                    if (!acceptOptional)
                    {
                        tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                    where objAnon.TradeTCInfoLeg.IsOptional == false
                                                    select objAnon;

                        tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                       where objAnon.TradeCargoInfoLeg.IsOptional == false
                                                       select objAnon;
                    }
                }


                if (entityTypeFilters.Where(a => a.HierarchyNodeType != null).Select(a => a.HierarchyNodeType.Code).Contains("LRT"))
                {
                    HierarchyNodeInfo node = entityTypeFilters.Where(a => a.HierarchyNodeType.Code == "LRT").Single();
                    var tradeRateType = (TradeInfoLegRateTypeEnum)Enum.Parse(typeof(TradeInfoLegRateTypeEnum), node.StrObject);

                    tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                where objAnon.TradeTCInfoLeg.RateType == tradeRateType
                                                select objAnon;

                    tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                   where objAnon.TradeCargoInfoLeg.RateType == tradeRateType
                                                   select objAnon;
                }
                if (entityTypeFilters.Where(a => a.HierarchyNodeType != null).Select(a => a.HierarchyNodeType.Code).Contains("OPT"))
                {
                    HierarchyNodeInfo node = entityTypeFilters.Where(a => a.HierarchyNodeType.Code == "OPT").Single();
                    var optional = node.StrObject == "Minimum" ? false : true;

                    tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                where objAnon.TradeTCInfoLeg.IsOptional == optional
                                                select objAnon;

                    tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                   where objAnon.TradeCargoInfoLeg.IsOptional == optional
                                                   select objAnon;
                }

                var tradeTCInfosWithLegsList = tradeTCInfosWithLegsQuery.ToList();
                var tradeCargoInfosWithLegsList = tradeCargoInfosWithLegsQuery.ToList();

                var tradeFFAInfos = (from objTradeInfo in tradeInfoQuery
                                     join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.TradeInfo.Id equals
                                         objTradeFFAInfo.TradeInfoId
                                     join objVesselL in dataContext.Vessels
                                         on objTradeFFAInfo.VesselId equals objVesselL.Id into objVesselLefts
                                     from objVessel in objVesselLefts.DefaultIfEmpty()
                                     where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                     select
                                         new
                                         {
                                             TradeFFAInfo = objTradeFFAInfo,
                                             TradeInfoId = objTradeInfo.TradeInfo.Id,
                                             VesselName = objVessel.Name
                                         }).ToList();

                var tradeOptionInfos = (from objTradeInfo in tradeInfoQuery
                                        join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.TradeInfo.Id
                                            equals objTradeOptionInfo.TradeInfoId
                                        join objVesselL in dataContext.Vessels
                                            on objTradeOptionInfo.VesselId equals objVesselL.Id into objVesselLefts
                                        from objVessel in objVesselLefts.DefaultIfEmpty()
                                        where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                        select
                                            new
                                            {
                                                TradeOptionInfo = objTradeOptionInfo,
                                                TradeInfoId = objTradeInfo.TradeInfo.Id,
                                                VesselName = objVessel.Name
                                            }).ToList();

                Dictionary<long, decimal> cmpOwnerShips = new Dictionary<long, decimal>();
                foreach (long companyId in comps)
                {
                    cmpOwnerShips[companyId] = ownPerc(cmp, companyId);
                }

                foreach (var anon in anonTradeInfoList)
                {
                    anon.TradeInfo.TradeBrokerInfos =
                        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeBrokerInfo).ToList();
                    anon.TradeInfo.TradeInfoBooks =
                        tradeInfoBooks.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeInfoBook).ToList();
                    anon.TradeInfo.BrokersName =
                        tradeBrokerInfos.Any(a => a.Id == anon.TradeInfo.Id)
                            ? tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(
                                a => a.objBroker.Name + "(" + a.objTradeBrokerInfo.Commission + "%)").Aggregate(
                                    (a1, a2) => a1 + ", " + a2)
                            : null;
                    anon.TradeInfo.MarketName = anon.MarketName;
                    anon.TradeInfo.MarketTonnes = anon.MarketTonnes;
                    anon.TradeInfo.TraderName = anon.TraderName;
                    anon.TradeInfo.CounterpartyName = anon.CounterpartyName;
                    anon.TradeInfo.CompanyName = anon.CompanyName;
                    anon.TradeInfo.Ownership = cmpOwnerShips[anon.TradeInfo.CompanyId];
                    anon.TradeInfo.SubType = anon.Subtype;

                    anon.TradeInfo.MTMFwdIndexName = anon.MTMFwdIndexName;

                    if (anon.Trade.Type == TradeTypeEnum.TC)
                    {
                        if (tradeTCInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.TradeInfo.TCInfo =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeTCInfo;
                            anon.TradeInfo.TCInfo.VesselName =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.TradeInfo.TCInfo.Legs =
                                tradeTCInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeTCInfoLeg).ToList();
                        }
                        else
                        {
                            anon.TradeInfo.TCInfo = new TradeTcInfo();
                            anon.TradeInfo.TCInfo.Legs = new List<TradeTcInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.FFA)
                    {
                        anon.TradeInfo.FFAInfo =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeFFAInfo;
                        anon.TradeInfo.FFAInfo.VesselName =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Cargo)
                    {
                        if (tradeCargoInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.TradeInfo.CargoInfo =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                                    TradeCargoInfo;
                            anon.TradeInfo.CargoInfo.VesselName =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.TradeInfo.CargoInfo.Legs =
                                tradeCargoInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeCargoInfoLeg).ToList();
                        }
                        else
                        {
                            anon.TradeInfo.CargoInfo = new TradeCargoInfo();
                            anon.TradeInfo.CargoInfo.Legs = new List<TradeCargoInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Option)
                    {
                        anon.TradeInfo.OptionInfo =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeOptionInfo;
                        anon.TradeInfo.OptionInfo.VesselName =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                    }

                    anon.Trade.Info = anon.TradeInfo;
                }
                trades = anonTradeInfoList.Select(a => a.Trade).ToList();
                
                companies = null;
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                companies = null;
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetCompanyHierarchyThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
           long? hierarchyNodeTypeId, HierarchyNodeInfo entityCompany, out Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentHierarchyNodes)
        {

            var hierarchyNodes = new List<HierarchyNodeInfo>();
            parentHierarchyNodes = new Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>>();

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetCompanyHierarchyThatHaveTradesByFilters: " };
                }




                //Get company sublevel if exists

                List<HierarchyNodeInfo> entityTypeFilters = new List<HierarchyNodeInfo>();
                entityTypeFilters.Add(entityCompany);


                hierarchyNodes = ProcessHierarchy2(dataContext, positionDate, periodFrom, periodTo, acceptDrafts, hierarchyNodeTypeId, entityTypeFilters, hierarchyNodes, parentHierarchyNodes);


                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }


        private List<HierarchyNodeInfo> ProcessHierarchy(DataContext dataContext, DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
            long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters, List<HierarchyNodeInfo> hierarchyNodes, Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentHierarchyNodes)
        {

            List<HierarchyNodeInfo> newHierarchyNodes = new List<HierarchyNodeInfo>();


            IQueryable<TradeInfo> tradeTCInfosQuery = null;
            IQueryable<TradeInfo> tradeCargoInfosQuery = null;
            IQueryable<TradeInfo> tradeFFAInfosQuery = null;
            IQueryable<TradeInfo> tradeOptionInfosQuery = null;
            IQueryable<TradeInfo> tradeTcInfoLegsQuery;
            IQueryable<TradeInfo> tradeCargoInfoLegsQuery;

            var selectedTypeNode = entityTypeFilters.FirstOrDefault();
            if (selectedTypeNode != null && selectedTypeNode.DomainObject != null && selectedTypeNode.HierarchyNodeType != null)
            {
                if (selectedTypeNode.DomainObject.GetType() == typeof(Company) &&
                    selectedTypeNode.HierarchyNodeType.Code == "CMP")
                {
                    //get next level (subcompanies)
                    var company = (Company)entityTypeFilters[0].DomainObject;

                    newHierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                where objCompany.ParentId == company.Id
                                                select new HierarchyNodeInfo()
                                                {
                                                    DomainObject = objCompany,
                                                    HierarchyNodeType = selectedTypeNode.HierarchyNodeType

                                                }).ToList());
                }
            }



            IQueryable<TradeInfo> internalQueryTemp;

            if (!acceptDrafts)
                internalQueryTemp = from objTrade in dataContext.Trades
                                    from objTradeInfo in dataContext.TradeInfos
                                    join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                                    join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                    where objTradeInfo.DateTo == null
                                          && objTradeBook.Status == ActivationStatusEnum.Active
                                          && objTrade.Id == objTradeInfo.TradeId
                                          && objTrade.Status == ActivationStatusEnum.Active
                                          && objTradeInfo.SignDate <= positionDate
                                          && objTradeInfo.PeriodFrom <= periodTo
                                          && objTradeInfo.PeriodTo >= periodFrom
                                          && objTrade.State != TradeStateEnum.Draft
                                    select objTradeInfo;
            else
            {
                internalQueryTemp = from objTrade in dataContext.Trades
                                    from objTradeInfo in dataContext.TradeInfos
                                    join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                                    join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                    where objTradeInfo.DateTo == null
                                          && objTradeBook.Status == ActivationStatusEnum.Active
                                          && objTrade.Id == objTradeInfo.TradeId
                                          && objTrade.Status == ActivationStatusEnum.Active
                                          && objTradeInfo.SignDate <= positionDate
                                          && objTradeInfo.PeriodFrom <= periodTo
                                          && objTradeInfo.PeriodTo >= periodFrom
                                    select objTradeInfo;
            }


            foreach (HierarchyNodeInfo entityTypeFilter in entityTypeFilters)
            {
                #region Filter is Domain Object
                if (entityTypeFilter.DomainObject != null && entityTypeFilter.HierarchyNodeType != null)
                {
                    if (entityTypeFilter.DomainObject.GetType() == typeof(Company) && entityTypeFilter.HierarchyNodeType.Code == "CMP"
                        && selectedTypeNode == entityTypeFilter)
                    {
                        long companyId = ((Company)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.CompanyId == companyId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Market))
                    {
                        long marketId = ((Market)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.MarketId == marketId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Trader))
                    {
                        long traderId = ((Trader)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.TraderId == traderId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Company) && entityTypeFilter.HierarchyNodeType.Code == "BRK")//Broker
                    {
                        long brokerId = ((Company)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTradeInfoBroker in dataContext.TradeBrokerInfos
                                            where objTradeInfo.Id == objTradeInfoBroker.TradeInfoId &&
                                                  objTradeInfoBroker.BrokerId == brokerId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "IDX")
                    {
                        long indexId = ((Index)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.MTMFwdIndexId == indexId
                                            select objTradeInfo;
                    }

                    #region Vessel
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Vessel))
                    {
                        long vesselId = ((Vessel)entityTypeFilter.DomainObject).Id;

                        tradeTCInfosQuery = (from objTradeInfo in internalQueryTemp
                                             from objTradeTcInfo in dataContext.TradeTcInfos
                                             from objTrade in dataContext.Trades
                                             from objVessel in dataContext.Vessels
                                             where
                                             objTrade.Type == TradeTypeEnum.TC
                                                && objTrade.Id == objTradeInfo.TradeId
                                                   && objTradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                   && objTradeTcInfo.VesselId == vesselId
                                             select objTradeInfo);

                        tradeCargoInfosQuery = (from objTradeInfo in internalQueryTemp
                                                from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                from objTrade in dataContext.Trades
                                                where
                                                 objTrade.Type == TradeTypeEnum.Cargo
                                                      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                      && objTradeCargoInfo.VesselId != null
                                                      && objTradeCargoInfo.VesselId == vesselId
                                                select objTradeInfo);

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                              from objTrade in dataContext.Trades
                                              where
                                              objTrade.Type == TradeTypeEnum.FFA
                                                    && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.VesselId != null
                                                    && objTradeFFAInfo.VesselId == vesselId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                 from objTrade in dataContext.Trades
                                                 where
                                                 objTrade.Type == TradeTypeEnum.Option
                                                       && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.VesselId != null
                                                       && objTradeOptionInfo.VesselId == vesselId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeTCInfosQuery.Union(tradeCargoInfosQuery).Union(tradeFFAInfosQuery).Union(tradeOptionInfosQuery);
                    }
                    #endregion

                    #region Vessel Pool
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(VesselPool))
                    {
                        long vesselPoolId = ((VesselPool)entityTypeFilter.DomainObject).Id;

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                              from objTrade in dataContext.Trades
                                              where
                                              objTrade.Type == TradeTypeEnum.FFA
                                                    && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.VesselPoolId != null
                                                    && objTradeFFAInfo.VesselPoolId == vesselPoolId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                 from objTrade in dataContext.Trades
                                                 where

                                                 objTrade.Type == TradeTypeEnum.Option
                                                       && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.VesselPoolId != null
                                                       && objTradeOptionInfo.VesselPoolId == vesselPoolId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                    #endregion

                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Desk) && entityTypeFilter.HierarchyNodeType.Code == "DESK")//Desk
                    {
                        long deskId = ((Desk)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrader in dataContext.Traders
                                            where objTradeInfo.TraderId == objTrader.Id &&
                                                  objTrader.DeskId == deskId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(ProfitCentre) && entityTypeFilter.HierarchyNodeType.Code == "PRFCENTRE")//ProfitCenter
                    {
                        long profitCentreId = ((ProfitCentre)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrader in dataContext.Traders
                                            where objTradeInfo.TraderId == objTrader.Id &&
                                                  objTrader.ProfitCentreId == profitCentreId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(ClearingHouse) && entityTypeFilter.HierarchyNodeType.Code == "OTC")
                    {
                        long clearingHouseId = ((ClearingHouse)entityTypeFilter.DomainObject).Id;

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                              from objTrade in dataContext.Trades
                                              where objTrade.Type == TradeTypeEnum.FFA
                                                      && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.ClearingHouseId != null
                                                    && objTradeFFAInfo.ClearingHouseId == clearingHouseId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                 from objTrade in dataContext.Trades
                                                 where objTrade.Type == TradeTypeEnum.Option
                                                  && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.ClearingHouseId != null
                                                       && objTradeOptionInfo.ClearingHouseId == clearingHouseId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "CRV")
                    {
                        long indexId = ((Index)entityTypeFilter.DomainObject).Id;

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                              from objTrade in dataContext.Trades
                                              where objTrade.Type == TradeTypeEnum.FFA
                                                    && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.IndexId == indexId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                 from objTrade in dataContext.Trades
                                                 where objTrade.Type == TradeTypeEnum.Option
                                                       && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.IndexId == indexId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                }
                #endregion

                #region Filter is Object

                else if (entityTypeFilter.StrObject != null)
                {
                    if (entityTypeFilter.HierarchyNodeType.Code == "DLT")
                    {
                        var tradeType = (TradeTypeEnum)Enum.Parse(typeof(TradeTypeEnum), entityTypeFilter.StrObject);
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrade in dataContext.Trades
                                            where
                                                  objTrade.Id == objTradeInfo.TradeId
                                                      &&

                                                  objTrade.Type == tradeType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "PFT")// Phsyical/Financial Type
                    {
                        string type = entityTypeFilter.StrObject;
                        List<TradeTypeEnum> tradeTypes = type == "Physical"
                                                            ? new List<TradeTypeEnum>() { TradeTypeEnum.TC, TradeTypeEnum.Cargo }
                                                            : new List<TradeTypeEnum>() { TradeTypeEnum.Option, TradeTypeEnum.FFA };

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrade in dataContext.Trades
                                            where objTrade.Id == objTradeInfo.TradeId
                                                  &&
                                                  tradeTypes.Contains(objTrade.Type)
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "DIR")// Call/Put
                    {
                        var tradeType = (TradeInfoDirectionEnum)Enum.Parse(typeof(TradeInfoDirectionEnum), entityTypeFilter.StrObject);

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrade in dataContext.Trades
                                            where objTrade.Id == objTradeInfo.TradeId
                                                  &&
                                                  objTradeInfo.Direction == tradeType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "STA")// Status
                    {
                        var tradeStatus = (TradeStateEnum)Enum.Parse(typeof(TradeStateEnum), entityTypeFilter.StrObject);

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrade in dataContext.Trades
                                            where objTrade.Id == objTradeInfo.TradeId
                                                  &&
                                                  objTrade.State == tradeStatus
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "CMPTYPE")// Company Type
                    {
                        var companyType = (CompanyTypeEnum)Enum.Parse(typeof(CompanyTypeEnum), entityTypeFilter.StrObject);

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrade in dataContext.Trades
                                            from objCompany in dataContext.Companies
                                            where objTrade.Id == objTradeInfo.TradeId
                                            && objTradeInfo.CompanyId == objCompany.Id
                                                  && objCompany.Type == companyType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "CMPSUBTYPE")//Company SubType
                    {
                        var companySubType = (CompanySubtypeEnum)Enum.Parse(typeof(CompanySubtypeEnum), entityTypeFilter.StrObject);
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrade in dataContext.Trades
                                            from objCompany in dataContext.Companies
                                            from objCompanySubType in dataContext.CompanySubtypes
                                            where objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.CompanyId == objCompany.Id
                                                  && objCompany.SubtypeId == objCompanySubType.Id
                                                  && objCompanySubType.Subtype == companySubType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "OTC")
                    {
                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                              from objTrade in dataContext.Trades
                                              where
                                              objTrade.Type == TradeTypeEnum.FFA
                                                    && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.ClearingHouseId == null
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                 from objTrade in dataContext.Trades
                                                 where
                                                objTrade.Type == TradeTypeEnum.Option
                                                       && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.ClearingHouseId == null
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                    if (entityTypeFilter.HierarchyNodeType.Code == "LRT")
                    {
                        var tradeRateType =
                            (TradeInfoLegRateTypeEnum)
                            Enum.Parse(typeof(TradeInfoLegRateTypeEnum), entityTypeFilter.StrObject);

                        tradeTcInfoLegsQuery = (from objTradeInfo in internalQueryTemp
                                                from objTradeTCInfo in dataContext.TradeTcInfos
                                                from objTrade in dataContext.Trades
                                                from objTradeTCInfoLeg in dataContext.TradeTcInfoLegs
                                                where
                                                objTrade.Type == TradeTypeEnum.TC
                                                      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.Id == objTradeTCInfo.TradeInfoId
                                                      && objTradeTCInfo.Id == objTradeTCInfoLeg.TradeTcInfoId
                                                      && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                      && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                      && objTradeTCInfoLeg.RateType == tradeRateType
                                                select objTradeInfo);

                        tradeCargoInfoLegsQuery = (from objTradeInfo in internalQueryTemp
                                                   from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                   from objTrade in dataContext.Trades
                                                   from objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs
                                                   where
                                                   objTrade.Type == TradeTypeEnum.Cargo
                                                         && objTrade.Id == objTradeInfo.TradeId
                                                         && objTradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                         && objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                                         && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                         && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                         && objTradeCargoInfoLeg.RateType == tradeRateType
                                                   select objTradeInfo);

                        internalQueryTemp = tradeTcInfoLegsQuery.Union(tradeCargoInfoLegsQuery);
                    }
                }
                #endregion
            }

            HierarchyNodeType childNodeType;
            if (hierarchyNodeTypeId == null)//It means that this is the first time the form is loaded so get the parent node of the hierarchy
            {

                childNodeType = (from objHierarchyNode in dataContext.HierarchyNodes
                                 from objHierarchyNodeType in dataContext.HierarchyNodeTypes
                                 where objHierarchyNode.ParentId == null &&
                                       objHierarchyNode.NodeTypeId == objHierarchyNodeType.Id
                                 select
                                     objHierarchyNodeType
                                ).SingleOrDefault();

            }
            else //Get next node type
            {
                childNodeType = (from objParentHierarchyNode in dataContext.HierarchyNodes
                                 from objChildHierarchyNode in dataContext.HierarchyNodes
                                 from objChildHierarchyNodeType in dataContext.HierarchyNodeTypes
                                 where objParentHierarchyNode.NodeTypeId == hierarchyNodeTypeId
                                       && objParentHierarchyNode.Id == objChildHierarchyNode.ParentId
                                       && objChildHierarchyNode.NodeTypeId == objChildHierarchyNodeType.Id
                                 select objChildHierarchyNodeType).SingleOrDefault();
            }

            if (childNodeType == null)
            {
                return hierarchyNodes;
            }


            #region Get Entities by node type
            long testtemp;
            if (childNodeType.Code == "CMP")
            {
                IQueryable<long> internalQuery = internalQueryTemp.Select(x => x.CompanyId);

                List<long> internalQueryComp = internalQuery.Distinct().ToList();
                var allcomps = new List<long?>();
                foreach (long comp in internalQueryComp)
                {
                    testtemp = comp;
                    AddParentComps(allcomps, testtemp);
                }

                if (hierarchyNodeTypeId == null)
                {
                    newHierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                where allcomps.Contains(objCompany.Id) &&
                                                //where internalQuery.Contains(objCompany.Id) &&
                                                objCompany.ParentId == null
                                                select new HierarchyNodeInfo()
                                                {
                                                    DomainObject = objCompany,
                                                    HierarchyNodeType = childNodeType
                                                }).ToList());
                }
                else
                {
                    newHierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                where allcomps.Contains(objCompany.Id)
                                                //                                                 where internalQuery.Contains(objCompany.Id)
                                                select new HierarchyNodeInfo()
                                                {
                                                    DomainObject = objCompany,
                                                    HierarchyNodeType = childNodeType
                                                }).ToList());
                }

            }
            else if (childNodeType.Code == "MRK")
            {
                IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                 select objTradeInfo.MarketId;

                newHierarchyNodes.AddRange((from objMarket in dataContext.Markets
                                            where internalQuery.Contains(objMarket.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objMarket,
                                                HierarchyNodeType = childNodeType
                                            }).ToList());
            }
            else if (childNodeType.Code == "TRD")
            {
                IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                 select objTradeInfo.TraderId;

                newHierarchyNodes.AddRange((from objTrader in dataContext.Traders
                                            where internalQuery.Contains(objTrader.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objTrader,
                                                HierarchyNodeType = childNodeType
                                            }).ToList());
            }
            else if (childNodeType.Code == "DLT")//Deal Type
            {
                List<TradeTypeEnum> internalQuery = (from objTradeInfo in internalQueryTemp
                                                     from objTrade in dataContext.Trades
                                                     where objTrade.Id == objTradeInfo.TradeId
                                                     select objTrade.Type).Distinct().ToList();

                newHierarchyNodes.AddRange(internalQuery.Select(tradeTypeEnum => new HierarchyNodeInfo()
                {
                    StrObject = tradeTypeEnum.ToString("g"),
                    HierarchyNodeType = childNodeType
                }));
            }
            else if (childNodeType.Code == "BRK")//Broker
            {
                IQueryable<long> internalQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeInfoBroker in dataContext.TradeBrokerInfos
                                                  where objTradeInfo.Id == objTradeInfoBroker.TradeInfoId
                                                  select objTradeInfoBroker.BrokerId);

                newHierarchyNodes.AddRange((from objBroker in dataContext.Companies
                                            where internalQuery.Contains(objBroker.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objBroker,
                                                HierarchyNodeType = childNodeType
                                            }).ToList());
            }

            #region Vessel

            else if (childNodeType.Code == "VSL")//Vessel
            {
                IQueryable<long> vessels;

                IQueryable<long> TcVesselIds = (from objTradeInfo in internalQueryTemp
                                                from objTradeTcInfo in dataContext.TradeTcInfos
                                                from objTrade in dataContext.Trades
                                                where
                                                objTrade.Type == TradeTypeEnum.TC
                                                      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                select objTradeTcInfo.VesselId);

                IQueryable<long> CargoVesselIds = (from objTradeInfo in internalQueryTemp
                                                   from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                   from objTrade in dataContext.Trades
                                                   where
                                                   objTrade.Type == TradeTypeEnum.Cargo
                                                         && objTrade.Id == objTradeInfo.TradeId
                                                         && objTradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                         && objTradeCargoInfo.VesselId != null
                                                   select (long)objTradeCargoInfo.VesselId);

                IQueryable<long> FFAVesselIds = (from objTradeInfo in internalQueryTemp
                                                 from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                 from objTrade in dataContext.Trades
                                                 where
                                                 objTrade.Type == TradeTypeEnum.FFA
                                                       && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                       && objTradeFFAInfo.VesselId != null
                                                 select (long)objTradeFFAInfo.VesselId);

                IQueryable<long> OptionVesselIds = (from objTradeInfo in internalQueryTemp
                                                    from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                    from objTrade in dataContext.Trades
                                                    where
                                                   objTrade.Type == TradeTypeEnum.Option
                                                          && objTrade.Id == objTradeInfo.TradeId
                                                          && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                          && objTradeOptionInfo.VesselId != null
                                                    select (long)objTradeOptionInfo.VesselId);

                vessels = TcVesselIds.Union(CargoVesselIds).Union(FFAVesselIds).Union(OptionVesselIds).Distinct();

                newHierarchyNodes.AddRange((from objVessel in dataContext.Vessels
                                            where vessels.Contains(objVessel.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objVessel,
                                                HierarchyNodeType = childNodeType
                                            }));
            }
            #endregion

            else if (childNodeType.Code == "PFT")//Physical/Financial Type
            {
                List<TradeTypeEnum> tradeTypes = (from objTradeInfo in internalQueryTemp
                                                  from objTrade in dataContext.Trades
                                                  where objTradeInfo.TradeId == objTrade.Id
                                                  select objTrade.Type).ToList();

                if (tradeTypes.Contains(TradeTypeEnum.Cargo) || tradeTypes.Contains(TradeTypeEnum.TC))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Physical", HierarchyNodeType = childNodeType });
                if (tradeTypes.Contains(TradeTypeEnum.Option) || tradeTypes.Contains(TradeTypeEnum.FFA))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Financial", HierarchyNodeType = childNodeType });
            }
            else if (childNodeType.Code == "LRT")//Leg Rate Type
            {
                List<TradeInfoLegRateTypeEnum> rateTypes;

                List<TradeInfoLegRateTypeEnum> TcRateTypes = (from objTradeInfo in internalQueryTemp
                                                              from objTrade in dataContext.Trades
                                                              from objTradeTcInfo in dataContext.TradeTcInfos
                                                              from objTradeTcInfoLeg in dataContext.TradeTcInfoLegs
                                                              where objTrade.Id == objTradeInfo.TradeId
                                                                        && objTradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                                    && objTradeTcInfo.Id ==
                                                                    objTradeTcInfoLeg.TradeTcInfoId
                                                              select objTradeTcInfoLeg.RateType).Distinct().ToList();

                List<TradeInfoLegRateTypeEnum> CargoRateTypes = (from objTradeInfo in internalQueryTemp
                                                                 from objTrade in dataContext.Trades
                                                                 from objTradeCargoInfo in
                                                                     dataContext.TradeCargoInfos
                                                                 from objTradeCargoInfoLeg in
                                                                     dataContext.TradeCargoInfoLegs
                                                                 where objTrade.Id == objTradeInfo.TradeId
                                                                           && objTradeInfo.Id ==
                                                                       objTradeCargoInfo.TradeInfoId
                                                                       &&
                                                                       objTradeCargoInfo.Id ==
                                                                       objTradeCargoInfoLeg.TradeCargoInfoId
                                                                 select objTradeCargoInfoLeg.RateType).Distinct().ToList();

                rateTypes = TcRateTypes.Union(CargoRateTypes).Distinct().ToList();

                newHierarchyNodes.AddRange(rateTypes.Select(legRateType => new HierarchyNodeInfo()
                {
                    StrObject = legRateType.ToString("g"),
                    HierarchyNodeType = childNodeType
                }));
            }
            else if (childNodeType.Code == "OPT")//Optiona/Minimum Leg
            {
                List<bool> TcIsOptional = (from objTradeInfo in internalQueryTemp
                                           from objTrade in dataContext.Trades
                                           from objTradeTcInfo in dataContext.TradeTcInfos
                                           from objTradeTcInfoLeg in dataContext.TradeTcInfoLegs
                                           where
                                           objTrade.Id == objTradeInfo.TradeId
                                                 && objTrade.Type == TradeTypeEnum.TC
                                                 && objTradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                 && objTradeTcInfo.Id ==
                                                 objTradeTcInfoLeg.TradeTcInfoId
                                           select objTradeTcInfoLeg.IsOptional).ToList();

                List<bool> CargoIsOptional = (from objTradeInfo in internalQueryTemp
                                              from objTrade in dataContext.Trades
                                              from objTradeCargoInfo in dataContext.TradeCargoInfos
                                              from objTradeCargoInfoLeg in
                                                  dataContext.TradeCargoInfoLegs
                                              where

                                              objTrade.Id == objTradeInfo.TradeId
                                                    && objTrade.Type == TradeTypeEnum.Cargo
                                                    && objTradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                    && objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                              select objTradeCargoInfoLeg.IsOptional).Distinct().ToList();

                List<bool> isOptional = TcIsOptional.Union(CargoIsOptional).Distinct().ToList();

                if (isOptional.Contains(true))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Optional", HierarchyNodeType = childNodeType });
                if (isOptional.Contains(false))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Minimum", HierarchyNodeType = childNodeType });
            }
            else if (childNodeType.Code == "SETPERIOD")//Settlement Period
            {
                List<string> ffaPeriods = (from objTradeInfo in internalQueryTemp
                                           from objTradeFFAInfo in dataContext.TradeFfaInfos
                                           where objTradeFFAInfo.TradeInfoId == objTradeInfo.Id
                                           select objTradeFFAInfo.Period).Distinct().ToList();

                List<string> optionPeriods = (from objTradeInfo in internalQueryTemp
                                              from objTradeOptionInfo in dataContext.TradeOptionInfos
                                              where objTradeOptionInfo.TradeInfoId == objTradeInfo.Id
                                              select objTradeOptionInfo.Period).Distinct().ToList();

                List<string> periods = ffaPeriods.Union(optionPeriods).Distinct().ToList();

                foreach (var period in periods)
                {
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = period, HierarchyNodeType = childNodeType });
                }
            }
            else if (childNodeType.Code == "DIR")//Call/Put
            {
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeInfoDirectionEnum.InOrBuy.ToString("g"), HierarchyNodeType = childNodeType });
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeInfoDirectionEnum.OutOrSell.ToString("g"), HierarchyNodeType = childNodeType });
            }
            else if (childNodeType.Code == "STA")//Status
            {
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Confirmed.ToString("g"), HierarchyNodeType = childNodeType });
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Draft.ToString("g"), HierarchyNodeType = childNodeType });
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Locked.ToString("g"), HierarchyNodeType = childNodeType });
            }

            #region Vessel Pool
            else if (childNodeType.Code == "VSLP")//Vessel pool
            {
                IQueryable<long> vesselPools;


                IQueryable<long> FFAVesselPoolIds = (from objTradeInfo in internalQueryTemp
                                                     from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                     from objTrade in dataContext.Trades
                                                     where
                                                     objTrade.Type == TradeTypeEnum.FFA
                                                           && objTrade.Id == objTradeInfo.TradeId
                                                           && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                           && objTradeFFAInfo.VesselPoolId != null
                                                     select (long)objTradeFFAInfo.VesselPoolId);

                IQueryable<long> OptionVesselPoolIds = (from objTradeInfo in internalQueryTemp
                                                        from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                        from objTrade in dataContext.Trades
                                                        where
                                                        objTrade.Type == TradeTypeEnum.Option
                                                              && objTrade.Id == objTradeInfo.TradeId
                                                              && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                              && objTradeOptionInfo.VesselPoolId != null
                                                        select (long)objTradeOptionInfo.VesselPoolId);

                vesselPools = FFAVesselPoolIds.Union(OptionVesselPoolIds).Distinct();

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objVesselPool in dataContext.VesselPools
                                                                 where vesselPools.Contains(objVesselPool.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objVesselPool,
                                                                     HierarchyNodeType = childNodeType
                                                                 }));
            }
            #endregion

            else if (childNodeType.Code == "CMPTYPE")//Company Type
            {
                List<CompanyTypeEnum> companyTypes = (from objTradeInfo in internalQueryTemp
                                                      from objCompany in dataContext.Companies
                                                      where objTradeInfo.CompanyId == objCompany.Id
                                                      select objCompany.Type).Distinct().ToList();

                newHierarchyNodes.AddRange(companyTypes.Select(companyType => new HierarchyNodeInfo()
                {
                    StrObject = companyType.ToString("g"),
                    HierarchyNodeType = childNodeType
                }));
            }
            else if (childNodeType.Code == "DESK")
            {
                List<Desk> desks = (from objTradeInfo in internalQueryTemp
                                    from objTrader in dataContext.Traders
                                    from objDesk in dataContext.Desks
                                    where objTradeInfo.TraderId == objTrader.Id
                                          && objTrader.DeskId == objDesk.Id
                                    select objDesk).Distinct().ToList();

                newHierarchyNodes.AddRange(desks.Select(desk => new HierarchyNodeInfo() { DomainObject = desk, HierarchyNodeType = childNodeType }));
            }
            else if (childNodeType.Code == "PRFCENTRE")
            {
                List<ProfitCentre> profitCentres = (from objTradeInfo in internalQueryTemp
                                                    from objTrader in dataContext.Traders
                                                    from objProfitCentre in dataContext.ProfitCentres
                                                    where objTradeInfo.TraderId == objTrader.Id
                                                          && objTrader.ProfitCentreId == objProfitCentre.Id
                                                    select objProfitCentre).Distinct().ToList();

                newHierarchyNodes.AddRange(profitCentres.Select(profiCentre => new HierarchyNodeInfo() { DomainObject = profiCentre, HierarchyNodeType = childNodeType }));
            }
            else if (childNodeType.Code == "CMPSUBTYPE")//Company Sub Type
            {
                List<CompanySubtypeEnum> internalQuery = (from objTradeInfo in internalQueryTemp
                                                          from objCompany in dataContext.Companies
                                                          from objCompanySubType in dataContext.CompanySubtypes
                                                          where objTradeInfo.CompanyId == objCompany.Id
                                                                && objCompany.SubtypeId == objCompanySubType.Id
                                                          select objCompanySubType.Subtype).Distinct().ToList();

                newHierarchyNodes.AddRange(internalQuery.Select(tradeTypeEnum => new HierarchyNodeInfo()
                {
                    StrObject =
                        tradeTypeEnum.ToString("g"),
                    HierarchyNodeType =
                        childNodeType
                }));
            }
            else if (childNodeType.Code == "IDX")
            {
                IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                 select objTradeInfo.MTMFwdIndexId;

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objIndex in dataContext.Indexes
                                                                 where internalQuery.Contains(objIndex.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objIndex,
                                                                     HierarchyNodeType = childNodeType
                                                                 }).ToList());
            }
            else if (childNodeType.Code == "OTC")
            {
                IQueryable<long> clearingHousesId;

                IQueryable<long> FFAclearingHousesIds = (from objTradeInfo in internalQueryTemp
                                                         from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                         from objTrade in dataContext.Trades
                                                         where
                                                         objTrade.Type == TradeTypeEnum.FFA
                                                               && objTrade.Id == objTradeInfo.TradeId
                                                               && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                               && objTradeFFAInfo.ClearingHouseId != null
                                                         select (long)objTradeFFAInfo.ClearingHouseId);

                IQueryable<long> OptionclearingHousesIds = (from objTradeInfo in internalQueryTemp
                                                            from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                            from objTrade in dataContext.Trades
                                                            where
                                                            objTrade.Type == TradeTypeEnum.Option
                                                                  && objTrade.Id == objTradeInfo.TradeId
                                                                  &&
                                                                  objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                                  && objTradeOptionInfo.ClearingHouseId != null
                                                            select (long)objTradeOptionInfo.ClearingHouseId);

                clearingHousesId = FFAclearingHousesIds.Union(OptionclearingHousesIds).Distinct();

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objClearingHouse in dataContext.Clearinghouses
                                                                 where clearingHousesId.Contains(objClearingHouse.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objClearingHouse,
                                                                     HierarchyNodeType = childNodeType
                                                                 }));
                newHierarchyNodes.Add(new HierarchyNodeInfo()
                {
                    HierarchyNodeType = childNodeType,
                    StrObject = "OTC"
                });
            }
            else if (childNodeType.Code == "CRV")
            {
                IQueryable<long> indexIds;

                IQueryable<long> FFAIndexIds = (from objTradeInfo in internalQueryTemp
                                                from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                from objTrade in dataContext.Trades
                                                where
                                                objTrade.Type == TradeTypeEnum.FFA
                                                      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                select (long)objTradeFFAInfo.IndexId);

                IQueryable<long> OptionIndexIds = (from objTradeInfo in internalQueryTemp
                                                   from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                   from objTrade in dataContext.Trades
                                                   where objTrade.Type == TradeTypeEnum.Option
                                                         && objTrade.Id == objTradeInfo.TradeId
                                                         && objTradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                   select (long)objTradeOptionInfo.IndexId);

                indexIds = FFAIndexIds.Union(OptionIndexIds).Distinct();

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objIndex in dataContext.Indexes
                                                                 where indexIds.Contains(objIndex.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objIndex,
                                                                     HierarchyNodeType = childNodeType
                                                                 }));
            }
            hierarchyNodes.AddRange(newHierarchyNodes);
            foreach (HierarchyNodeInfo node in newHierarchyNodes)
            {
                List<HierarchyNodeInfo> parents = new List<HierarchyNodeInfo>();
                parents.Add(selectedTypeNode);
                if (node.DomainObject == null || (node.DomainObject.GetType() != typeof(Company) &&
                    node.HierarchyNodeType.Code != "CMP"))
                {
                    //for(int i=entityTypeFilters.Count -1; i>=0; i--)
                    for (int i = 0; i <= entityTypeFilters.Count - 1; i++)
                    {
                        if (entityTypeFilters[i].DomainObject.GetType() == typeof(Company) &&
                    entityTypeFilters[i].HierarchyNodeType.Code == "CMP")
                        {
                            parents.Add(entityTypeFilters[i]);
                            break;
                        }
                    }
                }


                parentHierarchyNodes.Add(node, parents);
                var listOfHierarchy = new List<HierarchyNodeInfo>();

                if (!entityTypeFilters.Contains(node))
                    listOfHierarchy.Add(node);
                listOfHierarchy.AddRange(entityTypeFilters);

                ProcessHierarchy(dataContext, positionDate, periodFrom, periodTo, acceptDrafts, node.HierarchyNodeType.Id, listOfHierarchy, hierarchyNodes, parentHierarchyNodes);
            }


            #endregion
            return hierarchyNodes;
        }

        private List<HierarchyNodeInfo> ProcessHierarchy2(DataContext dataContext, DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
           long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters, List<HierarchyNodeInfo> hierarchyNodes, Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentHierarchyNodes)
        {

            List<HierarchyNodeInfo> newHierarchyNodes = new List<HierarchyNodeInfo>();


            IQueryable<BasicTrade> tradeTCInfosQuery = null;
            IQueryable<BasicTrade> tradeCargoInfosQuery = null;
            IQueryable<BasicTrade> tradeFFAInfosQuery = null;
            IQueryable<BasicTrade> tradeOptionInfosQuery = null;
            IQueryable<BasicTrade> tradeTcInfoLegsQuery;
            IQueryable<BasicTrade> tradeCargoInfoLegsQuery;

            var selectedTypeNode = entityTypeFilters.FirstOrDefault();
            if (selectedTypeNode != null && selectedTypeNode.DomainObject != null && selectedTypeNode.HierarchyNodeType != null)
            {
                if (selectedTypeNode.DomainObject.GetType() == typeof(Company) &&
                    selectedTypeNode.HierarchyNodeType.Code == "CMP")
                {
                    //get next level (subcompanies)
                    var company = (Company)entityTypeFilters[0].DomainObject;

                    newHierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                where objCompany.ParentId == company.Id
                                                select new HierarchyNodeInfo()
                                                {
                                                    DomainObject = objCompany,
                                                    HierarchyNodeType = selectedTypeNode.HierarchyNodeType

                                                }).ToList());
                }
            }



            IQueryable<BasicTrade> internalQueryTemp;

            if (!acceptDrafts)
                internalQueryTemp = from objTrade in dataContext.Trades
                                    from objTradeInfo in dataContext.TradeInfos
                                    join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                                    join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                    where objTradeInfo.DateTo == null
                                          && objTradeBook.Status == ActivationStatusEnum.Active
                                          && objTrade.Id == objTradeInfo.TradeId
                                          && objTrade.Status == ActivationStatusEnum.Active
                                          && objTradeInfo.SignDate <= positionDate
                                          && objTradeInfo.PeriodFrom <= periodTo
                                          && objTradeInfo.PeriodTo >= periodFrom
                                          && objTrade.State != TradeStateEnum.Draft
                                    select new BasicTrade { TradeInfo = objTradeInfo, Trade = objTrade };
            else
            {
                internalQueryTemp = from objTrade in dataContext.Trades
                                    from objTradeInfo in dataContext.TradeInfos
                                    join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                                    join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                    where objTradeInfo.DateTo == null
                                          && objTradeBook.Status == ActivationStatusEnum.Active
                                          && objTrade.Id == objTradeInfo.TradeId
                                          && objTrade.Status == ActivationStatusEnum.Active
                                          && objTradeInfo.SignDate <= positionDate
                                          && objTradeInfo.PeriodFrom <= periodTo
                                          && objTradeInfo.PeriodTo >= periodFrom
                                    select new BasicTrade { TradeInfo = objTradeInfo, Trade = objTrade };
            }


            foreach (HierarchyNodeInfo entityTypeFilter in entityTypeFilters)
            {
                #region Filter is Domain Object
                if (entityTypeFilter.DomainObject != null && entityTypeFilter.HierarchyNodeType != null)
                {
                    if (entityTypeFilter.DomainObject.GetType() == typeof(Company) && entityTypeFilter.HierarchyNodeType.Code == "CMP"
                        && selectedTypeNode == entityTypeFilter)
                    {
                        long companyId = ((Company)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.TradeInfo.CompanyId == companyId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Market))
                    {
                        long marketId = ((Market)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.TradeInfo.MarketId == marketId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Trader))
                    {
                        long traderId = ((Trader)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.TradeInfo.TraderId == traderId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Company) && entityTypeFilter.HierarchyNodeType.Code == "BRK")//Broker
                    {
                        long brokerId = ((Company)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTradeInfoBroker in dataContext.TradeBrokerInfos
                                            where objTradeInfo.TradeInfo.Id == objTradeInfoBroker.TradeInfoId &&
                                                  objTradeInfoBroker.BrokerId == brokerId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "IDX")
                    {
                        long indexId = ((Index)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            where objTradeInfo.TradeInfo.MTMFwdIndexId == indexId
                                            select objTradeInfo;
                    }

                    #region Vessel
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Vessel))
                    {
                        long vesselId = ((Vessel)entityTypeFilter.DomainObject).Id;

                        tradeTCInfosQuery = (from objTradeInfo in internalQueryTemp
                                             from objTradeTcInfo in dataContext.TradeTcInfos
                                                 //   from objTrade in dataContext.Trades
                                             from objVessel in dataContext.Vessels
                                             where objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                             //objTrade.Type == TradeTypeEnum.TC
                                             //   && objTrade.Id == objTradeInfo.Trade.TradeId
                                                   && objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                   && objTradeTcInfo.VesselId == vesselId
                                             select objTradeInfo);

                        tradeCargoInfosQuery = (from objTradeInfo in internalQueryTemp
                                                from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                    //   from objTrade in dataContext.Trades
                                                where
                                                 objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                //objTrade.Type == TradeTypeEnum.Cargo
                                                //      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                      && objTradeCargoInfo.VesselId != null
                                                      && objTradeCargoInfo.VesselId == vesselId
                                                select objTradeInfo);

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  // from objTrade in dataContext.Trades
                                              where
                                               objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                              //objTrade.Type == TradeTypeEnum.FFA
                                              //      && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.VesselId != null
                                                    && objTradeFFAInfo.VesselId == vesselId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     //  from objTrade in dataContext.Trades
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                       // objTrade.Type == TradeTypeEnum.Option
                                                       //   && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.VesselId != null
                                                       && objTradeOptionInfo.VesselId == vesselId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeTCInfosQuery.Union(tradeCargoInfosQuery).Union(tradeFFAInfosQuery).Union(tradeOptionInfosQuery);
                    }
                    #endregion

                    #region Vessel Pool
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(VesselPool))
                    {
                        long vesselPoolId = ((VesselPool)entityTypeFilter.DomainObject).Id;

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  //    from objTrade in dataContext.Trades
                                              where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                              //objTrade.Type == TradeTypeEnum.FFA
                                              //      && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.VesselPoolId != null
                                                    && objTradeFFAInfo.VesselPoolId == vesselPoolId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     //      from objTrade in dataContext.Trades
                                                 where
                                                  objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                 //objTrade.Type == TradeTypeEnum.Option
                                                 //      && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.VesselPoolId != null
                                                       && objTradeOptionInfo.VesselPoolId == vesselPoolId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                    #endregion

                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Desk) && entityTypeFilter.HierarchyNodeType.Code == "DESK")//Desk
                    {
                        long deskId = ((Desk)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrader in dataContext.Traders
                                            where objTradeInfo.TradeInfo.TraderId == objTrader.Id &&
                                                  objTrader.DeskId == deskId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(ProfitCentre) && entityTypeFilter.HierarchyNodeType.Code == "PRFCENTRE")//ProfitCenter
                    {
                        long profitCentreId = ((ProfitCentre)entityTypeFilter.DomainObject).Id;
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objTrader in dataContext.Traders
                                            where objTradeInfo.TradeInfo.TraderId == objTrader.Id &&
                                                  objTrader.ProfitCentreId == profitCentreId
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(ClearingHouse) && entityTypeFilter.HierarchyNodeType.Code == "OTC")
                    {
                        long clearingHouseId = ((ClearingHouse)entityTypeFilter.DomainObject).Id;

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  //  from objTrade in dataContext.Trades
                                              where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                    // objTrade.Type == TradeTypeEnum.FFA
                                                    //  && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.ClearingHouseId != null
                                                    && objTradeFFAInfo.ClearingHouseId == clearingHouseId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     // from objTrade in dataContext.Trades
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                 //objTrade.Type == TradeTypeEnum.Option
                                                 // && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.ClearingHouseId != null
                                                       && objTradeOptionInfo.ClearingHouseId == clearingHouseId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                    else if (entityTypeFilter.DomainObject.GetType() == typeof(Index) && entityTypeFilter.HierarchyNodeType.Code == "CRV")
                    {
                        long indexId = ((Index)entityTypeFilter.DomainObject).Id;

                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  //   from objTrade in dataContext.Trades
                                              where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                              //objTrade.Type == TradeTypeEnum.FFA
                                              //      && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.IndexId == indexId
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     //      from objTrade in dataContext.Trades
                                                 where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                 //objTrade.Type == TradeTypeEnum.Option
                                                 //      && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.IndexId == indexId
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                }
                #endregion

                #region Filter is Object

                else if (entityTypeFilter.StrObject != null)
                {
                    if (entityTypeFilter.HierarchyNodeType.Code == "DLT")
                    {
                        var tradeType = (TradeTypeEnum)Enum.Parse(typeof(TradeTypeEnum), entityTypeFilter.StrObject);
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                //     from objTrade in dataContext.Trades
                                            where
                                                  //objTrade.Id == objTradeInfo.TradeId
                                                  //    && 

                                                  objTradeInfo.Trade.Type == tradeType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "PFT")// Phsyical/Financial Type
                    {
                        string type = entityTypeFilter.StrObject;
                        List<TradeTypeEnum> tradeTypes = type == "Physical"
                                                            ? new List<TradeTypeEnum>() { TradeTypeEnum.TC, TradeTypeEnum.Cargo }
                                                            : new List<TradeTypeEnum>() { TradeTypeEnum.Option, TradeTypeEnum.FFA };

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                //    from objTrade in dataContext.Trades
                                            where //objTrade.Id == objTradeInfo.TradeId
                                                  //&& 
                                                  tradeTypes.Contains(objTradeInfo.Trade.Type)
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "DIR")// Call/Put
                    {
                        var tradeType = (TradeInfoDirectionEnum)Enum.Parse(typeof(TradeInfoDirectionEnum), entityTypeFilter.StrObject);

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                //    from objTrade in dataContext.Trades
                                            where //objTrade.Id == objTradeInfo.TradeId
                                                  // && 
                                                  objTradeInfo.TradeInfo.Direction == tradeType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "STA")// Status
                    {
                        var tradeStatus = (TradeStateEnum)Enum.Parse(typeof(TradeStateEnum), entityTypeFilter.StrObject);

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                                //     from objTrade in dataContext.Trades
                                            where //objTrade.Id == objTradeInfo.TradeId
                                                  //&& 
                                                  objTradeInfo.Trade.State == tradeStatus
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "CMPTYPE")// Company Type
                    {
                        var companyType = (CompanyTypeEnum)Enum.Parse(typeof(CompanyTypeEnum), entityTypeFilter.StrObject);

                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objCompany in dataContext.Companies
                                            where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                  && objCompany.Type == companyType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "CMPSUBTYPE")//Company SubType
                    {
                        var companySubType = (CompanySubtypeEnum)Enum.Parse(typeof(CompanySubtypeEnum), entityTypeFilter.StrObject);
                        internalQueryTemp = from objTradeInfo in internalQueryTemp
                                            from objCompany in dataContext.Companies
                                            from objCompanySubType in dataContext.CompanySubtypes
                                            where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                  && objCompany.SubtypeId == objCompanySubType.Id
                                                  && objCompanySubType.Subtype == companySubType
                                            select objTradeInfo;
                    }
                    else if (entityTypeFilter.HierarchyNodeType.Code == "OTC")
                    {
                        tradeFFAInfosQuery = (from objTradeInfo in internalQueryTemp
                                              from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                  //       from objTrade in dataContext.Trades
                                              where
                                              objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                    // objTrade.Type == TradeTypeEnum.FFA
                                                    //  && objTrade.Id == objTradeInfo.TradeId
                                                    && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                    && objTradeFFAInfo.ClearingHouseId == null
                                              select objTradeInfo);

                        tradeOptionInfosQuery = (from objTradeInfo in internalQueryTemp
                                                 from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                     //       from objTrade in dataContext.Trades
                                                 where
                                                 objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                 //objTrade.Type == TradeTypeEnum.Option
                                                 //      && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                       && objTradeOptionInfo.ClearingHouseId == null
                                                 select objTradeInfo);

                        internalQueryTemp = tradeFFAInfosQuery.Union(tradeOptionInfosQuery);
                    }
                    if (entityTypeFilter.HierarchyNodeType.Code == "LRT")
                    {
                        var tradeRateType =
                            (TradeInfoLegRateTypeEnum)
                            Enum.Parse(typeof(TradeInfoLegRateTypeEnum), entityTypeFilter.StrObject);

                        tradeTcInfoLegsQuery = (from objTradeInfo in internalQueryTemp
                                                from objTradeTCInfo in dataContext.TradeTcInfos
                                                    //       from objTrade in dataContext.Trades
                                                from objTradeTCInfoLeg in dataContext.TradeTcInfoLegs
                                                where
                                                objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                //objTrade.Type == TradeTypeEnum.TC
                                                //      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.TradeInfo.Id == objTradeTCInfo.TradeInfoId
                                                      && objTradeTCInfo.Id == objTradeTCInfoLeg.TradeTcInfoId
                                                      && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                      && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                      && objTradeTCInfoLeg.RateType == tradeRateType
                                                select objTradeInfo);

                        tradeCargoInfoLegsQuery = (from objTradeInfo in internalQueryTemp
                                                   from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                       //          from objTrade in dataContext.Trades
                                                   from objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs
                                                   where
                                                   objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                   //objTrade.Type == TradeTypeEnum.Cargo
                                                   //      && objTrade.Id == objTradeInfo.TradeId
                                                         && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                         && objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                                         && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                         && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                         && objTradeCargoInfoLeg.RateType == tradeRateType
                                                   select objTradeInfo);

                        internalQueryTemp = tradeTcInfoLegsQuery.Union(tradeCargoInfoLegsQuery);
                    }
                }
                #endregion
            }

            HierarchyNodeType childNodeType;
            if (hierarchyNodeTypeId == null)//It means that this is the first time the form is loaded so get the parent node of the hierarchy
            {

                childNodeType = (from objHierarchyNode in dataContext.HierarchyNodes
                                 from objHierarchyNodeType in dataContext.HierarchyNodeTypes
                                 where objHierarchyNode.ParentId == null &&
                                       objHierarchyNode.NodeTypeId == objHierarchyNodeType.Id
                                 select
                                     objHierarchyNodeType
                                ).SingleOrDefault();

            }
            else //Get next node type
            {
                childNodeType = (from objParentHierarchyNode in dataContext.HierarchyNodes
                                 from objChildHierarchyNode in dataContext.HierarchyNodes
                                 from objChildHierarchyNodeType in dataContext.HierarchyNodeTypes
                                 where objParentHierarchyNode.NodeTypeId == hierarchyNodeTypeId
                                       && objParentHierarchyNode.Id == objChildHierarchyNode.ParentId
                                       && objChildHierarchyNode.NodeTypeId == objChildHierarchyNodeType.Id
                                 select objChildHierarchyNodeType).SingleOrDefault();
            }

            if (childNodeType == null)
            {
                return hierarchyNodes;
            }


            #region Get Entities by node type
            long testtemp;
            if (childNodeType.Code == "CMP")
            {
                IQueryable<long> internalQuery = internalQueryTemp.Select(x => x.TradeInfo.CompanyId);

                List<long> internalQueryComp = internalQuery.Distinct().ToList();
                var allcomps = new List<long?>();
                foreach (long comp in internalQueryComp)
                {
                    testtemp = comp;
                    AddParentComps(allcomps, testtemp);
                }

                if (hierarchyNodeTypeId == null)
                {
                    newHierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                where allcomps.Contains(objCompany.Id) &&
                                                //where internalQuery.Contains(objCompany.Id) &&
                                                objCompany.ParentId == null
                                                select new HierarchyNodeInfo()
                                                {
                                                    DomainObject = objCompany,
                                                    HierarchyNodeType = childNodeType
                                                }).ToList());
                }
                else
                {
                    newHierarchyNodes.AddRange((from objCompany in dataContext.Companies
                                                where allcomps.Contains(objCompany.Id)
                                                //                                                 where internalQuery.Contains(objCompany.Id)
                                                select new HierarchyNodeInfo()
                                                {
                                                    DomainObject = objCompany,
                                                    HierarchyNodeType = childNodeType
                                                }).ToList());
                }

            }
            else if (childNodeType.Code == "MRK")
            {
                IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                 select objTradeInfo.TradeInfo.MarketId;

                newHierarchyNodes.AddRange((from objMarket in dataContext.Markets
                                            where internalQuery.Contains(objMarket.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objMarket,
                                                HierarchyNodeType = childNodeType
                                            }).ToList());
            }
            else if (childNodeType.Code == "TRD")
            {
                IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                 select objTradeInfo.TradeInfo.TraderId;

                newHierarchyNodes.AddRange((from objTrader in dataContext.Traders
                                            where internalQuery.Contains(objTrader.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objTrader,
                                                HierarchyNodeType = childNodeType
                                            }).ToList());
            }
            else if (childNodeType.Code == "DLT")//Deal Type
            {
                List<TradeTypeEnum> internalQuery = (from objTradeInfo in internalQueryTemp
                                                         //  from objTrade in dataContext.Trades
                                                         //  where objTrade.Id == objTradeInfo.TradeId
                                                     select objTradeInfo.Trade.Type).Distinct().ToList();

                newHierarchyNodes.AddRange(internalQuery.Select(tradeTypeEnum => new HierarchyNodeInfo()
                {
                    StrObject = tradeTypeEnum.ToString("g"),
                    HierarchyNodeType = childNodeType
                }));
            }
            else if (childNodeType.Code == "BRK")//Broker
            {
                IQueryable<long> internalQuery = (from objTradeInfo in internalQueryTemp
                                                  from objTradeInfoBroker in dataContext.TradeBrokerInfos
                                                  where objTradeInfo.TradeInfo.Id == objTradeInfoBroker.TradeInfoId
                                                  select objTradeInfoBroker.BrokerId);

                newHierarchyNodes.AddRange((from objBroker in dataContext.Companies
                                            where internalQuery.Contains(objBroker.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objBroker,
                                                HierarchyNodeType = childNodeType
                                            }).ToList());
            }

            #region Vessel

            else if (childNodeType.Code == "VSL")//Vessel
            {
                IQueryable<long> vessels;

                IQueryable<long> TcVesselIds = (from objTradeInfo in internalQueryTemp
                                                from objTradeTcInfo in dataContext.TradeTcInfos
                                                    //     from objTrade in dataContext.Trades
                                                where
                                                objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                                //objTrade.Type == TradeTypeEnum.TC
                                                //      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                select objTradeTcInfo.VesselId);

                IQueryable<long> CargoVesselIds = (from objTradeInfo in internalQueryTemp
                                                   from objTradeCargoInfo in dataContext.TradeCargoInfos
                                                       //           from objTrade in dataContext.Trades
                                                   where
                                                   objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                                   //objTrade.Type == TradeTypeEnum.Cargo
                                                   //      && objTrade.Id == objTradeInfo.TradeId
                                                         && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                         && objTradeCargoInfo.VesselId != null
                                                   select (long)objTradeCargoInfo.VesselId);

                IQueryable<long> FFAVesselIds = (from objTradeInfo in internalQueryTemp
                                                 from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                     //        from objTrade in dataContext.Trades
                                                 where
                                                 objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                 //objTrade.Type == TradeTypeEnum.FFA
                                                 //      && objTrade.Id == objTradeInfo.TradeId
                                                       && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                       && objTradeFFAInfo.VesselId != null
                                                 select (long)objTradeFFAInfo.VesselId);

                IQueryable<long> OptionVesselIds = (from objTradeInfo in internalQueryTemp
                                                    from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                        //          from objTrade in dataContext.Trades
                                                    where
                                                    objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                    //objTrade.Type == TradeTypeEnum.Option
                                                    //      && objTrade.Id == objTradeInfo.TradeId
                                                          && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                          && objTradeOptionInfo.VesselId != null
                                                    select (long)objTradeOptionInfo.VesselId);

                vessels = TcVesselIds.Union(CargoVesselIds).Union(FFAVesselIds).Union(OptionVesselIds).Distinct();

                newHierarchyNodes.AddRange((from objVessel in dataContext.Vessels
                                            where vessels.Contains(objVessel.Id)
                                            select new HierarchyNodeInfo()
                                            {
                                                DomainObject = objVessel,
                                                HierarchyNodeType = childNodeType
                                            }));
            }
            #endregion

            else if (childNodeType.Code == "PFT")//Physical/Financial Type
            {
                List<TradeTypeEnum> tradeTypes = (from objTradeInfo in internalQueryTemp
                                                      //    from objTrade in dataContext.Trades
                                                      //  where objTradeInfo.TradeId == objTrade.Id
                                                  select objTradeInfo.Trade.Type).ToList();

                if (tradeTypes.Contains(TradeTypeEnum.Cargo) || tradeTypes.Contains(TradeTypeEnum.TC))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Physical", HierarchyNodeType = childNodeType });
                if (tradeTypes.Contains(TradeTypeEnum.Option) || tradeTypes.Contains(TradeTypeEnum.FFA))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Financial", HierarchyNodeType = childNodeType });
            }
            else if (childNodeType.Code == "LRT")//Leg Rate Type
            {
                List<TradeInfoLegRateTypeEnum> rateTypes;

                List<TradeInfoLegRateTypeEnum> TcRateTypes = (from objTradeInfo in internalQueryTemp
                                                              from objTradeTcInfo in dataContext.TradeTcInfos
                                                              from objTradeTcInfoLeg in dataContext.TradeTcInfoLegs
                                                              where objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                                    &&
                                                                    objTradeTcInfo.Id ==
                                                                    objTradeTcInfoLeg.TradeTcInfoId
                                                              select objTradeTcInfoLeg.RateType).Distinct().ToList();

                List<TradeInfoLegRateTypeEnum> CargoRateTypes = (from objTradeInfo in internalQueryTemp
                                                                 from objTradeCargoInfo in
                                                                     dataContext.TradeCargoInfos
                                                                 from objTradeCargoInfoLeg in
                                                                     dataContext.TradeCargoInfoLegs
                                                                 where objTradeInfo.TradeInfo.Id ==
                                                                       objTradeCargoInfo.TradeInfoId
                                                                       &&
                                                                       objTradeCargoInfo.Id ==
                                                                       objTradeCargoInfoLeg.TradeCargoInfoId
                                                                 select objTradeCargoInfoLeg.RateType).Distinct().ToList();

                rateTypes = TcRateTypes.Union(CargoRateTypes).Distinct().ToList();

                newHierarchyNodes.AddRange(rateTypes.Select(legRateType => new HierarchyNodeInfo()
                {
                    StrObject = legRateType.ToString("g"),
                    HierarchyNodeType = childNodeType
                }));
            }
            else if (childNodeType.Code == "OPT")//Optiona/Minimum Leg
            {
                List<bool> TcIsOptional = (from objTradeInfo in internalQueryTemp
                                               //       from objTrade in dataContext.Trades
                                           from objTradeTcInfo in dataContext.TradeTcInfos
                                           from objTradeTcInfoLeg in dataContext.TradeTcInfoLegs
                                           where
                                           objTradeInfo.Trade.Type == TradeTypeEnum.TC
                                           //objTrade.Id == objTradeInfo.TradeId
                                           //      && objTrade.Type == TradeTypeEnum.TC
                                                 && objTradeInfo.TradeInfo.Id == objTradeTcInfo.TradeInfoId
                                                 && objTradeTcInfo.Id ==
                                                 objTradeTcInfoLeg.TradeTcInfoId
                                           select objTradeTcInfoLeg.IsOptional).ToList();

                List<bool> CargoIsOptional = (from objTradeInfo in internalQueryTemp
                                                  //         from objTrade in dataContext.Trades
                                              from objTradeCargoInfo in dataContext.TradeCargoInfos
                                              from objTradeCargoInfoLeg in
                                                  dataContext.TradeCargoInfoLegs
                                              where
                                              objTradeInfo.Trade.Type == TradeTypeEnum.Cargo
                                              //objTrade.Id == objTradeInfo.TradeId
                                              //      && objTrade.Type == TradeTypeEnum.Cargo
                                                    && objTradeInfo.TradeInfo.Id == objTradeCargoInfo.TradeInfoId
                                                    && objTradeCargoInfo.Id == objTradeCargoInfoLeg.TradeCargoInfoId
                                              select objTradeCargoInfoLeg.IsOptional).Distinct().ToList();

                List<bool> isOptional = TcIsOptional.Union(CargoIsOptional).Distinct().ToList();

                if (isOptional.Contains(true))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Optional", HierarchyNodeType = childNodeType });
                if (isOptional.Contains(false))
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = "Minimum", HierarchyNodeType = childNodeType });
            }
            else if (childNodeType.Code == "SETPERIOD")//Settlement Period
            {
                List<string> ffaPeriods = (from objTradeInfo in internalQueryTemp
                                           from objTradeFFAInfo in dataContext.TradeFfaInfos
                                           where objTradeFFAInfo.TradeInfoId == objTradeInfo.TradeInfo.Id
                                           select objTradeFFAInfo.Period).Distinct().ToList();

                List<string> optionPeriods = (from objTradeInfo in internalQueryTemp
                                              from objTradeOptionInfo in dataContext.TradeOptionInfos
                                              where objTradeOptionInfo.TradeInfoId == objTradeInfo.TradeInfo.Id
                                              select objTradeOptionInfo.Period).Distinct().ToList();

                List<string> periods = ffaPeriods.Union(optionPeriods).Distinct().ToList();

                foreach (var period in periods)
                {
                    newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = period, HierarchyNodeType = childNodeType });
                }
            }
            else if (childNodeType.Code == "DIR")//Call/Put
            {
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeInfoDirectionEnum.InOrBuy.ToString("g"), HierarchyNodeType = childNodeType });
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeInfoDirectionEnum.OutOrSell.ToString("g"), HierarchyNodeType = childNodeType });
            }
            else if (childNodeType.Code == "STA")//Status
            {
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Confirmed.ToString("g"), HierarchyNodeType = childNodeType });
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Draft.ToString("g"), HierarchyNodeType = childNodeType });
                newHierarchyNodes.Add(new HierarchyNodeInfo() { StrObject = TradeStateEnum.Locked.ToString("g"), HierarchyNodeType = childNodeType });
            }

            #region Vessel Pool
            else if (childNodeType.Code == "VSLP")//Vessel pool
            {
                IQueryable<long> vesselPools;


                IQueryable<long> FFAVesselPoolIds = (from objTradeInfo in internalQueryTemp
                                                     from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                         //            from objTrade in dataContext.Trades
                                                     where
                                                     objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                     //objTrade.Type == TradeTypeEnum.FFA
                                                     //      && objTrade.Id == objTradeInfo.TradeId
                                                           && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                           && objTradeFFAInfo.VesselPoolId != null
                                                     select (long)objTradeFFAInfo.VesselPoolId);

                IQueryable<long> OptionVesselPoolIds = (from objTradeInfo in internalQueryTemp
                                                        from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                            //            from objTrade in dataContext.Trades
                                                        where
                                                        objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                        //objTrade.Type == TradeTypeEnum.Option
                                                        //      && objTrade.Id == objTradeInfo.TradeId
                                                              && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                              && objTradeOptionInfo.VesselPoolId != null
                                                        select (long)objTradeOptionInfo.VesselPoolId);

                vesselPools = FFAVesselPoolIds.Union(OptionVesselPoolIds).Distinct();

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objVesselPool in dataContext.VesselPools
                                                                 where vesselPools.Contains(objVesselPool.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objVesselPool,
                                                                     HierarchyNodeType = childNodeType
                                                                 }));
            }
            #endregion

            else if (childNodeType.Code == "CMPTYPE")//Company Type
            {
                List<CompanyTypeEnum> companyTypes = (from objTradeInfo in internalQueryTemp
                                                      from objCompany in dataContext.Companies
                                                      where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                      select objCompany.Type).Distinct().ToList();

                newHierarchyNodes.AddRange(companyTypes.Select(companyType => new HierarchyNodeInfo()
                {
                    StrObject = companyType.ToString("g"),
                    HierarchyNodeType = childNodeType
                }));
            }
            else if (childNodeType.Code == "DESK")
            {
                List<Desk> desks = (from objTradeInfo in internalQueryTemp
                                    from objTrader in dataContext.Traders
                                    from objDesk in dataContext.Desks
                                    where objTradeInfo.TradeInfo.TraderId == objTrader.Id
                                          && objTrader.DeskId == objDesk.Id
                                    select objDesk).Distinct().ToList();

                newHierarchyNodes.AddRange(desks.Select(desk => new HierarchyNodeInfo() { DomainObject = desk, HierarchyNodeType = childNodeType }));
            }
            else if (childNodeType.Code == "PRFCENTRE")
            {
                List<ProfitCentre> profitCentres = (from objTradeInfo in internalQueryTemp
                                                    from objTrader in dataContext.Traders
                                                    from objProfitCentre in dataContext.ProfitCentres
                                                    where objTradeInfo.TradeInfo.TraderId == objTrader.Id
                                                          && objTrader.ProfitCentreId == objProfitCentre.Id
                                                    select objProfitCentre).Distinct().ToList();

                newHierarchyNodes.AddRange(profitCentres.Select(profiCentre => new HierarchyNodeInfo() { DomainObject = profiCentre, HierarchyNodeType = childNodeType }));
            }
            else if (childNodeType.Code == "CMPSUBTYPE")//Company Sub Type
            {
                List<CompanySubtypeEnum> internalQuery = (from objTradeInfo in internalQueryTemp
                                                          from objCompany in dataContext.Companies
                                                          from objCompanySubType in dataContext.CompanySubtypes
                                                          where objTradeInfo.TradeInfo.CompanyId == objCompany.Id
                                                                && objCompany.SubtypeId == objCompanySubType.Id
                                                          select objCompanySubType.Subtype).Distinct().ToList();

                newHierarchyNodes.AddRange(internalQuery.Select(tradeTypeEnum => new HierarchyNodeInfo()
                {
                    StrObject =
                        tradeTypeEnum.ToString("g"),
                    HierarchyNodeType =
                        childNodeType
                }));
            }
            else if (childNodeType.Code == "IDX")
            {
                IQueryable<long> internalQuery = from objTradeInfo in internalQueryTemp
                                                 select objTradeInfo.TradeInfo.MTMFwdIndexId;

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objIndex in dataContext.Indexes
                                                                 where internalQuery.Contains(objIndex.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objIndex,
                                                                     HierarchyNodeType = childNodeType
                                                                 }).ToList());
            }
            else if (childNodeType.Code == "OTC")
            {
                IQueryable<long> clearingHousesId;

                IQueryable<long> FFAclearingHousesIds = (from objTradeInfo in internalQueryTemp
                                                         from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                             //        from objTrade in dataContext.Trades
                                                         where
                                                          objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                         //objTrade.Type == TradeTypeEnum.FFA
                                                         //      && objTrade.Id == objTradeInfo.TradeId
                                                               && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                               && objTradeFFAInfo.ClearingHouseId != null
                                                         select (long)objTradeFFAInfo.ClearingHouseId);

                IQueryable<long> OptionclearingHousesIds = (from objTradeInfo in internalQueryTemp
                                                            from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                                //          from objTrade in dataContext.Trades
                                                            where
                                                            objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                            //objTrade.Type == TradeTypeEnum.Option
                                                            //      && objTrade.Id == objTradeInfo.TradeId
                                                                  &&
                                                                  objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                                  && objTradeOptionInfo.ClearingHouseId != null
                                                            select (long)objTradeOptionInfo.ClearingHouseId);

                clearingHousesId = FFAclearingHousesIds.Union(OptionclearingHousesIds).Distinct();

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objClearingHouse in dataContext.Clearinghouses
                                                                 where clearingHousesId.Contains(objClearingHouse.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objClearingHouse,
                                                                     HierarchyNodeType = childNodeType
                                                                 }));
                newHierarchyNodes.Add(new HierarchyNodeInfo()
                {
                    HierarchyNodeType = childNodeType,
                    StrObject = "OTC"
                });
            }
            else if (childNodeType.Code == "CRV")
            {
                IQueryable<long> indexIds;

                IQueryable<long> FFAIndexIds = (from objTradeInfo in internalQueryTemp
                                                from objTradeFFAInfo in dataContext.TradeFfaInfos
                                                    //      from objTrade in dataContext.Trades
                                                where objTradeInfo.Trade.Type == TradeTypeEnum.FFA
                                                //objTrade.Type == TradeTypeEnum.FFA
                                                //      && objTrade.Id == objTradeInfo.TradeId
                                                      && objTradeInfo.TradeInfo.Id == objTradeFFAInfo.TradeInfoId
                                                select (long)objTradeFFAInfo.IndexId);

                IQueryable<long> OptionIndexIds = (from objTradeInfo in internalQueryTemp
                                                   from objTradeOptionInfo in dataContext.TradeOptionInfos
                                                       //         from objTrade in dataContext.Trades
                                                   where objTradeInfo.Trade.Type == TradeTypeEnum.Option
                                                   //objTrade.Type == TradeTypeEnum.Option
                                                   //      && objTrade.Id == objTradeInfo.TradeId
                                                         && objTradeInfo.TradeInfo.Id == objTradeOptionInfo.TradeInfoId
                                                   select (long)objTradeOptionInfo.IndexId);

                indexIds = FFAIndexIds.Union(OptionIndexIds).Distinct();

                newHierarchyNodes = new List<HierarchyNodeInfo>((from objIndex in dataContext.Indexes
                                                                 where indexIds.Contains(objIndex.Id)
                                                                 select new HierarchyNodeInfo()
                                                                 {
                                                                     DomainObject = objIndex,
                                                                     HierarchyNodeType = childNodeType
                                                                 }));
            }
            hierarchyNodes.AddRange(newHierarchyNodes);
            foreach (HierarchyNodeInfo node in newHierarchyNodes)
            {
                List<HierarchyNodeInfo> parents = new List<HierarchyNodeInfo>();
                parents.Add(selectedTypeNode);
                if (node.DomainObject == null || (node.DomainObject.GetType() != typeof(Company) &&
                    node.HierarchyNodeType.Code != "CMP"))
                {
                    for (int i = 0; i <= entityTypeFilters.Count - 1; i++)
                    {
                        if (entityTypeFilters[i].DomainObject.GetType() == typeof(Company) &&
                    entityTypeFilters[i].HierarchyNodeType.Code == "CMP")
                        {
                            parents.Add(entityTypeFilters[i]);
                            break;
                        }
                    }
                }


                parentHierarchyNodes.Add(node, parents);
                var listOfHierarchy = new List<HierarchyNodeInfo>();

                if (!entityTypeFilters.Contains(node))
                    listOfHierarchy.Add(node);
                listOfHierarchy.AddRange(entityTypeFilters);

                ProcessHierarchy2(dataContext, positionDate, periodFrom, periodTo, acceptDrafts, node.HierarchyNodeType.Id, listOfHierarchy, hierarchyNodes, parentHierarchyNodes);
            }


            #endregion
            return hierarchyNodes;
        }


        private void AddParentComps(List<long?> comps, long? comp_id)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetTradesByFilters2: " };
                }
                var companies = dataContext.Companies;
                var parent_id = companies.Where(x => x.Id == comp_id).Select(x => x.ParentId).FirstOrDefault();

                //from cmp in dataContext.Companies
                //where cmp.Id == comp_id
                //select cmp.ParentId;

                if (parent_id == null)
                {
                    comps.Add(comp_id);
                }
                else
                {
                    comps.Add(comp_id);
                    AddParentComps(comps, parent_id);
                }

            }
            catch (Exception exc)
            {
                HandleException(userName, exc);

            }

        }


        //TODO: call recursive function to calculate ownership percentage
        public static decimal ownPerc(IQueryable<Company> complist, long? cmpId)
        {

            var parenid = (from c in complist //complist
                           where c.Id == cmpId
                           select c.ParentId).Single();
            var query = (from c in complist
                         where c.Id == cmpId
                         select new { c.Ownership }).Single();

            decimal perc = query.Ownership.GetValueOrDefault(1);
            //ÏƒÏ…Î½Î¸Î®ÎºÎ· Ï„ÎµÏÎ¼Î±Ï„Î¹ÏƒÎ¼Î¿Ï
            if (!(complist.Any(c => c.Id == parenid)))
                return 1;
            decimal x = perc / 100 * ownPerc(complist, parenid);

            return x;//Recursive call    

        }

        public int? GetTradesBySelectionBooks(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays, List<HierarchyNodeInfo> entityTypeFilters, out List<Trade> trades)
        {
            trades = new List<Trade>();

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetTradesBySelectionBooks: " };
                }

                IQueryable<TradeInfo> tradeInfoQuery;

                List<long> parentBooksIds = dataContext.Books.Where(a => a.ParentBookId != null).Select(a => a.ParentBookId.Value).Distinct().ToList();
                List<long> selectionBookIds = entityTypeFilters.Select(a => ((Book)a.DomainObject).Id).ToList();
                List<Book> allBooks = dataContext.Books.ToList();
                List<long> notBookIds = new List<long>();

                List<Book> selectionBooks = (from objBook in dataContext.Books
                                             where selectionBookIds.Contains(objBook.Id)
                                             select objBook
                                            ).ToList();

                foreach (var selectionBook in selectionBooks)
                {
                    if (parentBooksIds.Contains(selectionBook.Id))//If is parent
                    {
                        List<long> selectedBookChildren =
                            allBooks.Where(a => a.ParentBookId == selectionBook.Id).Select(a => a.Id).ToList();

                        foreach (var selectedBookChild in selectedBookChildren)
                        {
                            if (!selectionBookIds.Contains(selectedBookChild))
                                notBookIds.Add(selectedBookChild);
                        }
                    }
                }

                List<long> notTradeInfoIds = (from objTrade in dataContext.Trades
                                              join objTradeInfo in dataContext.TradeInfos on objTrade.Id equals
                                                  objTradeInfo.TradeId
                                              join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id
                                                  equals
                                                  objTradeInfoBook.TradeInfoId
                                              where notBookIds.Contains(objTradeInfoBook.BookId)
                                                    && ((objTradeInfo.DateFrom <= positionDate && objTradeInfo.DateTo > positionDate) || objTradeInfo.DateTo == null)
                                                    && objTradeInfo.SignDate <= positionDate
                                                    && objTradeInfo.PeriodFrom <= periodTo
                                                    && objTradeInfo.PeriodTo >= periodFrom
                                                    && objTrade.Status == ActivationStatusEnum.Active
                                              select objTradeInfo.Id).ToList();

                var q = (from objTrade in dataContext.Trades
                         join objTradeInfo in dataContext.TradeInfos on objTrade.Id equals objTradeInfo.TradeId
                         join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                         join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                         where objTradeBook.Status == ActivationStatusEnum.Active
                               && selectionBookIds.Contains(objTradeInfoBook.BookId)
                               && !notTradeInfoIds.Contains(objTradeInfoBook.TradeInfoId)
                               && ((objTradeInfo.DateFrom <= positionDate && objTradeInfo.DateTo > positionDate) || objTradeInfo.DateTo == null)
                               && objTradeInfo.SignDate <= positionDate
                               && objTradeInfo.PeriodFrom <= periodTo
                               && objTradeInfo.PeriodTo >= periodFrom
                               && objTrade.Status == ActivationStatusEnum.Active
                         group objTradeInfo by objTradeInfo.TradeId
                             into g
                         select new { MaxTradeInfoId = (from objTradeInfo2 in g select objTradeInfo2.Id).Max() });

                tradeInfoQuery = from objTradeInfo in dataContext.TradeInfos
                                 join objMaxTradeInfoId in q on objTradeInfo.Id equals objMaxTradeInfoId.MaxTradeInfoId
                                 select objTradeInfo;


                if (!acceptDrafts)
                    tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                     where objTrade.State != TradeStateEnum.Draft

                                     select objTradeInfo;

                if (onlyNonZeroTotalDays)
                {
                    tradeInfoQuery = from objTradeInfo in tradeInfoQuery
                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                     join objTradeFFAInfoL in dataContext.TradeFfaInfos
                                     on objTradeInfo.Id equals objTradeFFAInfoL.TradeInfoId into objTradeFFAInfoLefts
                                     from objTradeFFAInfo in objTradeFFAInfoLefts.DefaultIfEmpty()

                                     where objTrade.Type != TradeTypeEnum.FFA ||
                                     (objTrade.Type == TradeTypeEnum.FFA && objTradeFFAInfo.QuantityDays - objTradeFFAInfo.ClosedOutQuantityTotal > 0)



                                     select objTradeInfo;
                }


                var anonTradeInfoList = (from objTradeInfo in tradeInfoQuery
                                         join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                         join objCompany in dataContext.Companies on objTradeInfo.CompanyId equals objCompany.Id
                                         join objCounterparty in dataContext.Companies on objTradeInfo.CounterpartyId equals objCounterparty.Id
                                         join objTrader in dataContext.Traders on objTradeInfo.TraderId equals objTrader.Id
                                         join objMarket in dataContext.Markets on objTradeInfo.MarketId equals objMarket.Id
                                         join objRegion in dataContext.Regions on objTradeInfo.RegionId equals objRegion.Id
                                         join objRoute in dataContext.Routes on objTradeInfo.RouteId equals objRoute.Id
                                         join objMTMFwdIndex in dataContext.Indexes on
                                             objTradeInfo.MTMFwdIndexId equals objMTMFwdIndex.Id
                                         select
                                             new
                                             {
                                                 Trade = objTrade,
                                                 TradeInfo = objTradeInfo,
                                                 CompanyName = objCompany.Name,
                                                 Ownership = objCompany.Ownership,
                                                 Subtype = objCompany.SubtypeId, //
                                                 CounterpartyName = objCounterparty.Name,
                                                 TraderName = objTrader.Name,
                                                 MarketName = objMarket.Name,
                                                 MarketTonnes = objMarket.Tonnes,
                                                 RegionName = objRegion.Name,
                                                 RouteName = objRoute.Name,
                                                 MTMFwdIndexName = objMTMFwdIndex.Name
                                             }).ToList();


                List<long> tradeInfoIds = anonTradeInfoList.Select(n => n.TradeInfo.Id).ToList();
                bool hasTcs = anonTradeInfoList.Any(n => n.Trade.Type == TradeTypeEnum.TC);
                bool hasCargos = anonTradeInfoList.Any(n => n.Trade.Type == TradeTypeEnum.Cargo);

                //var tradeBrokerInfos = (from objTradeInfo in tradeInfoQuery
                //                        join objTradeBrokerInfo in dataContext.TradeBrokerInfos on objTradeInfo.Id
                //                            equals objTradeBrokerInfo.TradeInfoId
                //                        join objBroker in dataContext.Companies on objTradeBrokerInfo.BrokerId equals
                //                            objBroker.Id
                //                        select new
                //                        {
                //                            objBroker,
                //                            objTradeBrokerInfo,
                //                            objTradeInfo.Id
                //                        }).ToList();

                var tradeBrokerInfos = (from objTradeBrokerInfo in dataContext.TradeBrokerInfos
                                        join objBroker in dataContext.Companies on objTradeBrokerInfo.BrokerId equals objBroker.Id
                                        where tradeInfoIds.Contains(objTradeBrokerInfo.TradeInfoId)
                                        select new
                                        {
                                            objBroker,
                                            objTradeBrokerInfo,
                                        }).ToList();

                //var tradeInfoBooks = (from objTradeInfo in tradeInfoQuery
                //                      join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals objTradeInfoBook.TradeInfoId
                //                      join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                //                      where objTradeBook.Status == ActivationStatusEnum.Active
                //                      select new
                //                      {
                //                          objTradeInfoBook,
                //                          objTradeInfo.Id
                //                      }).ToList();

                var tradeInfoBooks = (from objTradeInfoBook in dataContext.TradeInfoBooks
                                      join objTradeBook in dataContext.Books on objTradeInfoBook.BookId equals objTradeBook.Id
                                      where objTradeBook.Status == ActivationStatusEnum.Active
                                      && tradeInfoIds.Contains(objTradeInfoBook.TradeInfoId)
                                      select new
                                      {
                                          objTradeInfoBook
                                      }).ToList();


                IQueryable<TcTradeLegInfo> tradeTCInfosWithLegsQuery = null;
                IQueryable<CargoTradeLegInfo> tradeCargoInfosWithLegsQuery = null;

                if (entityTypeFilters.Count > 0 && entityTypeFilters[0].DomainObject != null &&
                    entityTypeFilters[0].DomainObject.GetType() == typeof(Book))
                {
                    if (hasTcs)
                        tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeTCInfo in dataContext.TradeTcInfos on objTradeInfo.Id
                                                         equals objTradeTCInfo.TradeInfoId
                                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals
                                                         objTrade.Id
                                                     join objTradeTCInfoLeg in dataContext.TradeTcInfoLegs on
                                                         objTradeTCInfo.Id equals objTradeTCInfoLeg.TradeTcInfoId
                                                     join objVessel in dataContext.Vessels on objTradeTCInfo.VesselId equals
                                                         objVessel.Id
                                                     where objTrade.Type == TradeTypeEnum.TC
                                                           && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                           && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                     orderby objTradeTCInfoLeg.PeriodFrom
                                                     select
                                                         new TcTradeLegInfo
                                                         {
                                                             TradeTCInfoLeg = objTradeTCInfoLeg,
                                                             TradeTCInfo = objTradeTCInfo,
                                                             TradeInfoId = objTradeInfo.Id,
                                                             VesselName = objVessel.Name
                                                         });

                    if (hasCargos)
                        tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                        join objTradeCargoInfo in dataContext.TradeCargoInfos on
                                                            objTradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                        join objTrade in dataContext.Trades on objTradeInfo.TradeId equals
                                                            objTrade.Id
                                                        join objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs on
                                                            objTradeCargoInfo.Id equals
                                                            objTradeCargoInfoLeg.TradeCargoInfoId
                                                        join objVesselL in dataContext.Vessels
                                                            on objTradeCargoInfo.VesselId equals objVesselL.Id into
                                                            objVesselLefts
                                                        from objVessel in objVesselLefts.DefaultIfEmpty()
                                                        where objTrade.Type == TradeTypeEnum.Cargo
                                                        && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                        && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                        orderby objTradeCargoInfoLeg.PeriodFrom
                                                        select
                                                            new CargoTradeLegInfo
                                                            {
                                                                TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                                TradeCargoInfo = objTradeCargoInfo,
                                                                TradeInfoId = objTradeInfo.Id,
                                                                VesselName = objVessel.Name
                                                            });



                    if (!acceptMinimum)
                    {
                        if (hasTcs)
                            tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                        where objAnon.TradeTCInfoLeg.IsOptional
                                                        select objAnon;

                        if (hasCargos)
                            tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                           where objAnon.TradeCargoInfoLeg.IsOptional
                                                           select objAnon;
                    }

                    if (!acceptOptional)
                    {
                        if (hasTcs)
                            tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                        where !objAnon.TradeTCInfoLeg.IsOptional
                                                        select objAnon;

                        if (hasCargos)
                            tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                           where !objAnon.TradeCargoInfoLeg.IsOptional
                                                           select objAnon;
                    }                   
                }
                else
                {
                    if (hasTcs)
                        tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                     join objTradeTCInfo in dataContext.TradeTcInfos on objTradeInfo.Id
                                                         equals objTradeTCInfo.TradeInfoId
                                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals
                                                         objTrade.Id
                                                     join objTradeTCInfoLeg in dataContext.TradeTcInfoLegs on
                                                         objTradeTCInfo.Id equals objTradeTCInfoLeg.TradeTcInfoId
                                                     join objVessel in dataContext.Vessels on objTradeTCInfo.VesselId equals
                                                         objVessel.Id
                                                     where objTrade.Type == TradeTypeEnum.TC
                                                           && objTradeTCInfoLeg.PeriodFrom <= periodTo
                                                           && objTradeTCInfoLeg.PeriodTo >= periodFrom
                                                     orderby objTradeTCInfoLeg.PeriodFrom
                                                     select
                                                         new TcTradeLegInfo
                                                         {
                                                             TradeTCInfoLeg = objTradeTCInfoLeg,
                                                             TradeTCInfo = objTradeTCInfo,
                                                             TradeInfoId = objTradeInfo.Id,
                                                             VesselName = objVessel.Name
                                                         });

                    if (hasCargos)
                        tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                        join objTradeCargoInfo in dataContext.TradeCargoInfos on
                                                            objTradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                        join objTrade in dataContext.Trades on objTradeInfo.TradeId equals
                                                            objTrade.Id
                                                        join objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs on
                                                            objTradeCargoInfo.Id equals
                                                            objTradeCargoInfoLeg.TradeCargoInfoId
                                                        join objVesselL in dataContext.Vessels
                                                            on objTradeCargoInfo.VesselId equals objVesselL.Id into
                                                            objVesselLefts
                                                        from objVessel in objVesselLefts.DefaultIfEmpty()
                                                        where objTrade.Type == TradeTypeEnum.Cargo
                                                              && objTradeCargoInfoLeg.PeriodFrom <= periodTo
                                                              && objTradeCargoInfoLeg.PeriodTo >= periodFrom
                                                        orderby objTradeCargoInfoLeg.PeriodFrom
                                                        select
                                                            new CargoTradeLegInfo
                                                            {
                                                                TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                                TradeCargoInfo = objTradeCargoInfo,
                                                                TradeInfoId = objTradeInfo.Id,
                                                                VesselName = objVessel.Name
                                                            });

                    if (!acceptMinimum)
                    {
                        if (hasTcs)
                            tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                        where objAnon.TradeTCInfoLeg.IsOptional
                                                        select objAnon;

                        if (hasCargos)
                            tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                           where objAnon.TradeCargoInfoLeg.IsOptional
                                                           select objAnon;
                    }

                    if (!acceptOptional)
                    {
                        if (hasTcs)
                            tradeTCInfosWithLegsQuery = from objAnon in tradeTCInfosWithLegsQuery
                                                        where objAnon.TradeTCInfoLeg.IsOptional == false
                                                        select objAnon;

                        if (hasCargos)
                            tradeCargoInfosWithLegsQuery = from objAnon in tradeCargoInfosWithLegsQuery
                                                           where objAnon.TradeCargoInfoLeg.IsOptional == false
                                                           select objAnon;
                    }
                }

                List<TcTradeLegInfo> tradeTCInfosWithLegsList = null;
                List<CargoTradeLegInfo> tradeCargoInfosWithLegsList = null;


                if (hasTcs)
                    tradeTCInfosWithLegsList = tradeTCInfosWithLegsQuery.ToList();

                if (hasCargos)
                    tradeCargoInfosWithLegsList = tradeCargoInfosWithLegsQuery.ToList();

                var tradeFFAInfos = (from objTradeInfo in tradeInfoQuery
                                     join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.Id equals
                                         objTradeFFAInfo.TradeInfoId
                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                     join objVesselL in dataContext.Vessels
                                         on objTradeFFAInfo.VesselId equals objVesselL.Id into objVesselLefts
                                     from objVessel in objVesselLefts.DefaultIfEmpty()
                                     where objTrade.Type == TradeTypeEnum.FFA
                                     select
                                         new
                                         {
                                             TradeFFAInfo = objTradeFFAInfo,
                                             TradeInfoId = objTradeInfo.Id,
                                             VesselName = objVessel.Name
                                         }).ToList();
                

                var tradeOptionInfos = (from objTradeInfo in tradeInfoQuery
                                        join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.Id
                                            equals objTradeOptionInfo.TradeInfoId
                                        join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                        join objVesselL in dataContext.Vessels
                                            on objTradeOptionInfo.VesselId equals objVesselL.Id into objVesselLefts
                                        from objVessel in objVesselLefts.DefaultIfEmpty()
                                        where objTrade.Type == TradeTypeEnum.Option
                                        select
                                            new
                                            {
                                                TradeOptionInfo = objTradeOptionInfo,
                                                TradeInfoId = objTradeInfo.Id,
                                                VesselName = objVessel.Name
                                            }).ToList();

                //foreach (var anon in anonTradeInfoList)
                //{
                //    anon.TradeInfo.TradeBrokerInfos =
                //        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeBrokerInfo).ToList();
                //    anon.TradeInfo.TradeInfoBooks =
                //        tradeInfoBooks.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeInfoBook).ToList();
                //    anon.TradeInfo.BrokersName =
                //        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Any()
                //            ? tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(
                //                a => a.objBroker.Name + "(" + a.objTradeBrokerInfo.Commission + "%)").Aggregate(
                //                    (a1, a2) => a1 + ", " + a2)
                //            : null;
                //    anon.TradeInfo.MarketName = anon.MarketName;
                //    anon.TradeInfo.MarketTonnes = anon.MarketTonnes;
                //    anon.TradeInfo.TraderName = anon.TraderName;
                //    anon.TradeInfo.CounterpartyName = anon.CounterpartyName;
                //    anon.TradeInfo.CompanyName = anon.CompanyName;
                //    anon.TradeInfo.MTMFwdIndexName = anon.MTMFwdIndexName;

                //    if (anon.Trade.Type == TradeTypeEnum.TC && tradeTCInfosWithLegsList!=null)
                //    {
                //        if (tradeTCInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                //        {
                //            anon.TradeInfo.TCInfo =
                //                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeTCInfo;
                //            anon.TradeInfo.TCInfo.VesselName =
                //                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                //            anon.TradeInfo.TCInfo.Legs =
                //                tradeTCInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                //                    a => a.TradeTCInfoLeg).ToList();
                //        }
                //        else
                //        {
                //            anon.TradeInfo.TCInfo = new TradeTcInfo();
                //            anon.TradeInfo.TCInfo.Legs = new List<TradeTcInfoLeg>();
                //        }
                //    }
                //    else if (anon.Trade.Type == TradeTypeEnum.FFA)
                //    {
                //        anon.TradeInfo.FFAInfo =
                //            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeFFAInfo;
                //        anon.TradeInfo.FFAInfo.VesselName =
                //            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                //    }
                //    else if (anon.Trade.Type == TradeTypeEnum.Cargo && tradeCargoInfosWithLegsList!=null)
                //    {
                //        if (tradeCargoInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                //        {
                //            anon.TradeInfo.CargoInfo =
                //                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                //                    TradeCargoInfo;
                //            anon.TradeInfo.CargoInfo.VesselName =
                //                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                //            anon.TradeInfo.CargoInfo.Legs =
                //                tradeCargoInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                //                    a => a.TradeCargoInfoLeg).ToList();
                //        }
                //        else
                //        {
                //            anon.TradeInfo.CargoInfo = new TradeCargoInfo();
                //            anon.TradeInfo.CargoInfo.Legs = new List<TradeCargoInfoLeg>();
                //        }
                //    }
                //    else if (anon.Trade.Type == TradeTypeEnum.Option)
                //    {
                //        anon.TradeInfo.OptionInfo =
                //            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeOptionInfo;
                //        anon.TradeInfo.OptionInfo.VesselName =
                //            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                //    }

                //    anon.Trade.Info = anon.TradeInfo;
                //}

                Parallel.ForEach(anonTradeInfoList, (anon) =>
                {

                    anon.TradeInfo.TradeBrokerInfos =
                                tradeBrokerInfos.Where(a => a.objTradeBrokerInfo.TradeInfoId == anon.TradeInfo.Id).Select(a => a.objTradeBrokerInfo).ToList();
                    anon.TradeInfo.TradeInfoBooks =
                        tradeInfoBooks.Where(a => a.objTradeInfoBook.TradeInfoId == anon.TradeInfo.Id).Select(a => a.objTradeInfoBook).ToList();
                    anon.TradeInfo.BrokersName =
                        tradeBrokerInfos.Where(a => a.objTradeBrokerInfo.TradeInfoId == anon.TradeInfo.Id).Any()
                            ? tradeBrokerInfos.Where(a => a.objTradeBrokerInfo.TradeInfoId == anon.TradeInfo.Id).Select(
                                a => a.objBroker.Name + "(" + a.objTradeBrokerInfo.Commission + "%)").Aggregate(
                                    (a1, a2) => a1 + ", " + a2)
                            : null;

                    anon.TradeInfo.MarketName = anon.MarketName;
                    anon.TradeInfo.MarketTonnes = anon.MarketTonnes;
                    anon.TradeInfo.TraderName = anon.TraderName;
                    anon.TradeInfo.CounterpartyName = anon.CounterpartyName;
                    anon.TradeInfo.CompanyName = anon.CompanyName;
                    anon.TradeInfo.Ownership = 1;
                    anon.TradeInfo.SubType = anon.Subtype;
                    anon.TradeInfo.MTMFwdIndexName = anon.MTMFwdIndexName;

                    if (anon.Trade.Type == TradeTypeEnum.TC && tradeTCInfosWithLegsList != null)
                    {
                        if (tradeTCInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.TradeInfo.TCInfo =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeTCInfo;
                            anon.TradeInfo.TCInfo.VesselName =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.TradeInfo.TCInfo.Legs =
                                tradeTCInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeTCInfoLeg).ToList();
                        }
                        else
                        {
                            anon.TradeInfo.TCInfo = new TradeTcInfo();
                            anon.TradeInfo.TCInfo.Legs = new List<TradeTcInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.FFA)
                    {
                        anon.TradeInfo.FFAInfo =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeFFAInfo;
                        anon.TradeInfo.FFAInfo.VesselName =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Cargo && tradeCargoInfosWithLegsList != null)
                    {
                        if (tradeCargoInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.TradeInfo.CargoInfo =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                                    TradeCargoInfo;
                            anon.TradeInfo.CargoInfo.VesselName =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.TradeInfo.CargoInfo.Legs =
                                tradeCargoInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeCargoInfoLeg).ToList();
                        }
                        else
                        {
                            anon.TradeInfo.CargoInfo = new TradeCargoInfo();
                            anon.TradeInfo.CargoInfo.Legs = new List<TradeCargoInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Option)
                    {
                        anon.TradeInfo.OptionInfo =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeOptionInfo;
                        anon.TradeInfo.OptionInfo.VesselName =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                    }

                    anon.Trade.Info = anon.TradeInfo;
                });

                trades = anonTradeInfoList.Select(a => a.Trade).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }


        /// <summary>
        /// Method called to update the trade status (Active/Inactive). All versions of the trade are updated to the new status.
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="userName"></param>
        /// <param name="tradeTypeEnum">The trade type</param>
        /// <param name="identifierId">TC/Cargo -> Leg ID, FFA/OPTION -> FFA/Option trade info ID </param>
        /// <param name="status">The new status</param>
        /// <returns></returns>
        /// 
        public int? UpdateTradeStatus(TradeTypeEnum tradeTypeEnum, long identifierId, ActivationStatusEnum status)
        {
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "UpdateTradeStatus: " };
                }

                if (tradeTypeEnum == TradeTypeEnum.TC)
                {
                    Trade tcTrade = (from objTCInfoLeg in dataContext.TradeTcInfoLegs
                                     from objTradeTCInfo in dataContext.TradeTcInfos
                                     from objTradeInfo in dataContext.TradeInfos
                                     from objTrade in dataContext.Trades
                                     where objTCInfoLeg.Id == identifierId &&
                                           objTCInfoLeg.TradeTcInfoId == objTradeTCInfo.Id &&
                                           objTradeTCInfo.TradeInfoId == objTradeInfo.Id &&
                                           objTradeInfo.TradeId == objTrade.Id
                                     select objTrade).SingleOrDefault();
                    if (tcTrade == null)
                        return null;

                    tcTrade.Status = status;
                    tcTrade.Chuser = userName;
                    tcTrade.Chd = DateTime.Now;

                }
                else if (tradeTypeEnum == TradeTypeEnum.Cargo)
                {
                    Trade cargoTrade = (from objCargoInfoLeg in dataContext.TradeCargoInfoLegs
                                        from objTradeCargoInfo in dataContext.TradeCargoInfos
                                        from objTradeInfo in dataContext.TradeInfos
                                        from objTrade in dataContext.Trades
                                        where objCargoInfoLeg.Id == identifierId &&
                                           objCargoInfoLeg.TradeCargoInfoId == objTradeCargoInfo.Id &&
                                           objTradeCargoInfo.TradeInfoId == objTradeInfo.Id &&
                                           objTradeInfo.TradeId == objTrade.Id
                                        select objTrade).SingleOrDefault();
                    if (cargoTrade == null)
                        return null;

                    cargoTrade.Status = status;
                    cargoTrade.Chuser = userName;
                    cargoTrade.Chd = DateTime.Now;
                }
                else if (tradeTypeEnum == TradeTypeEnum.FFA)
                {
                    Trade ffaTrade = (from objTradeFFAInfo in dataContext.TradeFfaInfos
                                      from objTradeInfo in dataContext.TradeInfos
                                      from objTrade in dataContext.Trades
                                      where objTradeFFAInfo.Id == identifierId &&
                                            objTradeFFAInfo.TradeInfoId == objTradeInfo.Id &&
                                            objTradeInfo.TradeId == objTrade.Id
                                      select objTrade).SingleOrDefault();
                    if (ffaTrade == null)
                        return null;

                    ffaTrade.Status = status;
                    ffaTrade.Chuser = userName;
                    ffaTrade.Chd = DateTime.Now;
                }
                else if (tradeTypeEnum == TradeTypeEnum.Option)
                {
                    Trade optionTrade = (from objTradeOptionInfo in dataContext.TradeOptionInfos
                                         from objTradeInfo in dataContext.TradeInfos
                                         from objTrade in dataContext.Trades
                                         where objTradeOptionInfo.Id == identifierId &&
                                               objTradeOptionInfo.TradeInfoId == objTradeInfo.Id &&
                                               objTradeInfo.TradeId == objTrade.Id
                                         select objTrade).SingleOrDefault();
                    if (optionTrade == null)
                        return null;

                    optionTrade.Status = status;
                    optionTrade.Chuser = userName;
                    optionTrade.Chd = DateTime.Now;
                }

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
        }

        public int? CheckUserHasPermissionOnTrader(long userId, long traderId, out bool hasPermission)
        {
            DataContext dataContext = null;
            string userName = null;

            hasPermission = false;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "CheckUserHasPermissionOnTrader: " };
                }

                hasPermission = dataContext.UserTraders.Any(a => a.TraderId == traderId && a.UserId == userId);

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
        }

        public int? GetInactiveTrades(out List<Trade> trades)
        {
            trades = new List<Trade>();

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetInactiveTrades: " };
                }

                List<long> tradeInfoIds;
                IQueryable<TradeInfo> tradeInfoQuery;


                var q = (from objTradeInfo in dataContext.TradeInfos
                         join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                         where objTrade.Status == ActivationStatusEnum.Inactive
                         group objTradeInfo by objTradeInfo.TradeId
                                into g
                         select new { MaxTradeInfoId = (from objTradeInfo2 in g select objTradeInfo2.Id).Max() });

                tradeInfoIds = (from objTradeInfo in dataContext.TradeInfos
                                join objMaxTradeInfoId in q
                                on objTradeInfo.Id equals objMaxTradeInfoId.MaxTradeInfoId
                                select objTradeInfo.Id).ToList();

                if (tradeInfoIds.Count < 1000)
                    tradeInfoQuery = dataContext.TradeInfos.Where(a => tradeInfoIds.Contains(a.Id));
                else
                    tradeInfoQuery = from objTradeInfo in dataContext.TradeInfos
                                     join objMaxTradeInfoId in q
                                     on objTradeInfo.Id equals objMaxTradeInfoId.MaxTradeInfoId
                                     select objTradeInfo;


                var anonTradeInfoList = (from objTradeInfo in tradeInfoQuery
                                         join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                         join objCompany in dataContext.Companies on objTradeInfo.CompanyId equals objCompany.Id
                                         join objCounterparty in dataContext.Companies on objTradeInfo.CounterpartyId equals objCounterparty.Id
                                         join objTrader in dataContext.Traders on objTradeInfo.TraderId equals objTrader.Id
                                         join objMarket in dataContext.Markets on objTradeInfo.MarketId equals objMarket.Id
                                         join objRegion in dataContext.Regions on objTradeInfo.RegionId equals objRegion.Id
                                         join objRoute in dataContext.Routes on objTradeInfo.RouteId equals objRoute.Id
                                         join objMTMFwdIndex in dataContext.Indexes on
                                             objTradeInfo.MTMFwdIndexId equals objMTMFwdIndex.Id
                                         select
                                             new
                                             {
                                                 Trade = objTrade,
                                                 TradeInfo = objTradeInfo,
                                                 CompanyName = objCompany.Name,
                                                 Ownership = objCompany.Ownership,
                                                 Subtype = objCompany.SubtypeId, //
                                                 CounterpartyName = objCounterparty.Name,
                                                 TraderName = objTrader.Name,
                                                 MarketName = objMarket.Name,
                                                 MarketTonnes = objMarket.Tonnes,
                                                 RegionName = objRegion.Name,
                                                 RouteName = objRoute.Name,
                                                 MTMFwdIndexName = objMTMFwdIndex.Name
                                             }).ToList();

                var tradeBrokerInfos = (from objTradeInfo in tradeInfoQuery
                                        join objTradeBrokerInfo in dataContext.TradeBrokerInfos on objTradeInfo.Id
                                            equals objTradeBrokerInfo.TradeInfoId
                                        join objBroker in dataContext.Companies on objTradeBrokerInfo.BrokerId equals
                                            objBroker.Id
                                        select new
                                        {
                                            objBroker,
                                            objTradeBrokerInfo,
                                            objTradeInfo.Id
                                        }).ToList();

                var tradeInfoBooks = (from objTradeInfo in tradeInfoQuery
                                      join objTradeInfoBook in dataContext.TradeInfoBooks on objTradeInfo.Id equals
                                          objTradeInfoBook.TradeInfoId
                                      select new
                                      {
                                          objTradeInfoBook,
                                          objTradeInfo.Id
                                      }).ToList();


                IQueryable<TcTradeLegInfo> tradeTCInfosWithLegsQuery;
                IQueryable<CargoTradeLegInfo> tradeCargoInfosWithLegsQuery;

                {
                    tradeTCInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                 join objTradeTCInfo in dataContext.TradeTcInfos on objTradeInfo.Id
                                                     equals objTradeTCInfo.TradeInfoId
                                                 join objTrade in dataContext.Trades on objTradeInfo.TradeId equals
                                                     objTrade.Id
                                                 join objTradeTCInfoLeg in dataContext.TradeTcInfoLegs on
                                                     objTradeTCInfo.Id equals objTradeTCInfoLeg.TradeTcInfoId
                                                 join objVessel in dataContext.Vessels on objTradeTCInfo.VesselId equals
                                                     objVessel.Id
                                                 where objTrade.Type == TradeTypeEnum.TC
                                                 orderby objTradeTCInfoLeg.PeriodFrom
                                                 select
                                                     new TcTradeLegInfo
                                                     {
                                                         TradeTCInfoLeg = objTradeTCInfoLeg,
                                                         TradeTCInfo = objTradeTCInfo,
                                                         TradeInfoId = objTradeInfo.Id,
                                                         VesselName = objVessel.Name
                                                     });

                    tradeCargoInfosWithLegsQuery = (from objTradeInfo in tradeInfoQuery
                                                    join objTradeCargoInfo in dataContext.TradeCargoInfos on
                                                        objTradeInfo.Id equals objTradeCargoInfo.TradeInfoId
                                                    join objTrade in dataContext.Trades on objTradeInfo.TradeId equals
                                                        objTrade.Id
                                                    join objTradeCargoInfoLeg in dataContext.TradeCargoInfoLegs on
                                                        objTradeCargoInfo.Id equals
                                                        objTradeCargoInfoLeg.TradeCargoInfoId
                                                    join objVesselL in dataContext.Vessels
                                                        on objTradeCargoInfo.VesselId equals objVesselL.Id into
                                                        objVesselLefts
                                                    from objVessel in objVesselLefts.DefaultIfEmpty()
                                                    where objTrade.Type == TradeTypeEnum.Cargo
                                                    orderby objTradeCargoInfoLeg.PeriodFrom
                                                    select
                                                        new CargoTradeLegInfo
                                                        {
                                                            TradeCargoInfoLeg = objTradeCargoInfoLeg,
                                                            TradeCargoInfo = objTradeCargoInfo,
                                                            TradeInfoId = objTradeInfo.Id,
                                                            VesselName = objVessel.Name
                                                        });
                }


                var tradeTCInfosWithLegsList = tradeTCInfosWithLegsQuery.ToList();
                var tradeCargoInfosWithLegsList = tradeCargoInfosWithLegsQuery.ToList();

                var tradeFFAInfos = (from objTradeInfo in tradeInfoQuery
                                     join objTradeFFAInfo in dataContext.TradeFfaInfos on objTradeInfo.Id equals
                                         objTradeFFAInfo.TradeInfoId
                                     join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                     join objVesselL in dataContext.Vessels
                                         on objTradeFFAInfo.VesselId equals objVesselL.Id into objVesselLefts
                                     from objVessel in objVesselLefts.DefaultIfEmpty()
                                     where objTrade.Type == TradeTypeEnum.FFA
                                     select
                                         new
                                         {
                                             TradeFFAInfo = objTradeFFAInfo,
                                             TradeInfoId = objTradeInfo.Id,
                                             VesselName = objVessel.Name
                                         }).ToList();

                var tradeOptionInfos = (from objTradeInfo in tradeInfoQuery
                                        join objTradeOptionInfo in dataContext.TradeOptionInfos on objTradeInfo.Id
                                            equals objTradeOptionInfo.TradeInfoId
                                        join objTrade in dataContext.Trades on objTradeInfo.TradeId equals objTrade.Id
                                        join objVesselL in dataContext.Vessels
                                            on objTradeOptionInfo.VesselId equals objVesselL.Id into objVesselLefts
                                        from objVessel in objVesselLefts.DefaultIfEmpty()
                                        where objTrade.Type == TradeTypeEnum.Option
                                        select
                                            new
                                            {
                                                TradeOptionInfo = objTradeOptionInfo,
                                                TradeInfoId = objTradeInfo.Id,
                                                VesselName = objVessel.Name
                                            }).ToList();

                foreach (var anon in anonTradeInfoList)
                {
                    anon.TradeInfo.TradeBrokerInfos =
                        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeBrokerInfo).ToList();
                    anon.TradeInfo.TradeInfoBooks =
                        tradeInfoBooks.Where(a => a.Id == anon.TradeInfo.Id).Select(a => a.objTradeInfoBook).ToList();
                    anon.TradeInfo.BrokersName =
                        tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Any()
                            ? tradeBrokerInfos.Where(a => a.Id == anon.TradeInfo.Id).Select(
                                a => a.objBroker.Name + "(" + a.objTradeBrokerInfo.Commission + "%)").Aggregate(
                                    (a1, a2) => a1 + ", " + a2)
                            : null;
                    anon.TradeInfo.MarketName = anon.MarketName;
                    anon.TradeInfo.MarketTonnes = anon.MarketTonnes;
                    anon.TradeInfo.TraderName = anon.TraderName;
                    anon.TradeInfo.CounterpartyName = anon.CounterpartyName;
                    anon.TradeInfo.CompanyName = anon.CompanyName;
                    anon.TradeInfo.MTMFwdIndexName = anon.MTMFwdIndexName;

                    if (anon.Trade.Type == TradeTypeEnum.TC)
                    {
                        if (tradeTCInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.TradeInfo.TCInfo =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeTCInfo;
                            anon.TradeInfo.TCInfo.VesselName =
                                tradeTCInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.TradeInfo.TCInfo.Legs =
                                tradeTCInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeTCInfoLeg).ToList();
                        }
                        else
                        {
                            anon.TradeInfo.TCInfo = new TradeTcInfo();
                            anon.TradeInfo.TCInfo.Legs = new List<TradeTcInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.FFA)
                    {
                        anon.TradeInfo.FFAInfo =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeFFAInfo;
                        anon.TradeInfo.FFAInfo.VesselName =
                            tradeFFAInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Cargo)
                    {
                        if (tradeCargoInfosWithLegsList.FirstOrDefault(a => a.TradeInfoId == anon.TradeInfo.Id) != null)
                        {
                            anon.TradeInfo.CargoInfo =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).
                                    TradeCargoInfo;
                            anon.TradeInfo.CargoInfo.VesselName =
                                tradeCargoInfosWithLegsList.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                            anon.TradeInfo.CargoInfo.Legs =
                                tradeCargoInfosWithLegsList.Where(a => a.TradeInfoId == anon.TradeInfo.Id).Select(
                                    a => a.TradeCargoInfoLeg).ToList();
                        }
                        else
                        {
                            anon.TradeInfo.CargoInfo = new TradeCargoInfo();
                            anon.TradeInfo.CargoInfo.Legs = new List<TradeCargoInfoLeg>();
                        }
                    }
                    else if (anon.Trade.Type == TradeTypeEnum.Option)
                    {
                        anon.TradeInfo.OptionInfo =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).TradeOptionInfo;
                        anon.TradeInfo.OptionInfo.VesselName =
                            tradeOptionInfos.First(a => a.TradeInfoId == anon.TradeInfo.Id).VesselName;
                    }

                    anon.Trade.Info = anon.TradeInfo;
                }
                trades = anonTradeInfoList.Select(a => a.Trade).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        #endregion

        #region MCRuns

        public int? GetMCRuns(out List<MCSimulationInfo> mcRuns)
        {
            mcRuns = null;

            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetMCRuns: " };
                }

                long userId =
                    dataContext.Users.Single(a => a.Login == GenericContext<ProgramInfo>.Current.Value.UserName).Id;

                mcRuns = Queries.GetAllMCRuns(dataContext).ToList();
                //TODO query

                //books = Queries.GetBooksByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                //userAssocBooks = Queries.GetBooksByUserId(dataContext, userId).ToList();

                //models = Queries.GetCashFlowModelsByStatus(dataContext, ActivationStatusEnum.Active).ToList();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? GetMCSimulationDetails(MCSimulationInfo mcSimInfo, out MCSimulation mcSimulation)
        {
            mcSimulation = null;
            DataContext dataContext = null;
            string userName = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = false };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "GetMCmcSimulationDetails: " };
                }

                long userId =
                    dataContext.Users.Single(a => a.Login == GenericContext<ProgramInfo>.Current.Value.UserName).Id;

                //mcRuns = Queries.GetAllMCRuns(dataContext).ToList();

                //Fill MCSimulation Obj
                MCSimulation mcSim = new MCSimulation();
                mcSim.Info = mcSimInfo;

                mcSim.Cashbound = (from objCashBound in dataContext.MCCashBounds
                                   where objCashBound.MCSimId == mcSimInfo.Id
                                   select objCashBound).ToList();

                mcSim.MonthFrequency = (from objMonthFrequency in dataContext.MCMonthFrequency
                                        where objMonthFrequency.MCSimId == mcSimInfo.Id
                                        select objMonthFrequency).ToList();

                mcSim.AllValues = (from objAllValues in dataContext.MCAllValues
                                   where objAllValues.MCSimId == mcSimInfo.Id
                                   select objAllValues).ToList();

                mcSimulation = mcSim;
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        public int? DeleteMCRuns(MCSimulationInfo mcSimulationInfo)
        {
            DataContext dataContext = null;
            string userName = null;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "DeleteMCRuns: " };
                }

                dataContext.MCSimulationInfo.DeleteOnSubmit(
                    dataContext.MCSimulationInfo.Where(a => a.Id == mcSimulationInfo.Id).FirstOrDefault());

                dataContext.MCMonthFrequency.DeleteAllOnSubmit(
                    dataContext.MCMonthFrequency.Where(a => a.MCSimId == mcSimulationInfo.Id).ToList());

                dataContext.MCCashBounds.DeleteAllOnSubmit(
                    dataContext.MCCashBounds.Where(a => a.MCSimId == mcSimulationInfo.Id).ToList());

                dataContext.MCAllValues.DeleteAllOnSubmit(
                    dataContext.MCAllValues.Where(a => a.MCSimId == mcSimulationInfo.Id).ToList());

                dataContext.SubmitChanges();

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }

        }

        #endregion

        #region Error Handling

        private void HandleException(string userName, Exception exc)
        {
            try
            {
                LogException(userName, exc);
                lock (smtpClientSynchObject)
                {
                    SendException(userName, exc);
                }
            }
            catch
            {
            }
        }

        private void LogException(string userName, Exception exc)
        {
            if (userName == null)
            {
                SiAuto.Main.LogException("Unhandled Exception", exc);
            }
            else
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                SiAuto.Si.GetSession(userName).LogException("Unhandled Exception", exc);
            }
        }

        private void HandleMessageLog(string userName, string message)
        {
            try
            {
                if (userName == null)
                {
                    SiAuto.Main.LogMessage(message);
                }
                else
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    SiAuto.Si.GetSession(userName).LogMessage(message);
                }
            }
            catch
            {
            }
        }

        private void SendException(string userName, Exception exc)
        {
            if (smtpClient != null)
            {
                if (!String.IsNullOrEmpty(exisSupportEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisSupportEmailAddress);
                        var mailMessage = new MailMessage
                        {
                            IsBodyHtml = false,
                            From = fromAddress,
                            Sender = fromAddress,
                            Subject = "EXCEPTION (" + userName + "): " + exc.Message,
                            Priority = MailPriority.High,
                            Body = exc.ToString()
                        };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }

                if (!String.IsNullOrEmpty(exisIssueTrackerEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisIssueTrackerEmailAddress);
                        var mailMessage = new MailMessage
                        {
                            IsBodyHtml = false,
                            From = fromAddress,
                            Sender = fromAddress,
                            Subject = "EXCEPTION (" + userName + "): " + exc.Message,
                            Priority = MailPriority.High,
                            Body = exc.ToString()
                        };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }
            }
        }

        #region class ValidationException

        private class ValidationException : ApplicationException
        {
            private ArrayList m_ApplicationExceptions;

            public ValidationException()
                : base("")
            {
                m_ApplicationExceptions = new ArrayList();
            }

            public ArrayList Exceptions
            {
                get
                {
                    return m_ApplicationExceptions;
                }
            }

            public ArrayList GetExceptionsByType(Type type)
            {
                var ret = new ArrayList();
                foreach (Exception exc in m_ApplicationExceptions)
                {
                    if (type == exc.GetType())
                    {
                        ret.Add(exc);
                    }
                }
                return ret;
            }
        }
        #endregion

        #endregion

        #region Internal Classes

        #region Nested type: MTMReturnValue

        internal class MTMReturnValue
        {
            internal Dictionary<string, Dictionary<DateTime, decimal>> tradeResults;
            internal Dictionary<DateTime, Dictionary<string, decimal>> indexesValues;
        }

        #endregion

        #region Nested type: AccountInformation

        internal class AccountInformation
        {
            internal Account Account;
            internal Company Bank;
            internal Company Company;
            internal Currency Currency;
        }

        #endregion

        #region Nested type: CompanyInformation

        internal class CompanyInformation
        {
            internal Company Company { get; set; }
            internal CompanySubtype Subtype { get; set; }
            internal Contact Contact { get; set; }
        }

        #endregion

        #region Nested type: IndexInformation

        internal class IndexInformation
        {
            internal Index Index;
            internal Market Market;
        }

        #endregion

        /*#region Nested type: TradeInformation

        internal class TradeInformation
        {
            internal Trade Trade { get; set; }
            internal TradeInfo TradeInfo { get; set; }
            internal TradeTcInfo TradeTCInfo { get; set; }
            internal TradeFfaInfo TradeFFAInfo { get; set; }
            internal TradeTcInfoLeg TradeTCInfoLeg { get; set; }
            internal TradeCargoInfo TradeCargoInfo { get; set; }
            internal TradeCargoInfoLeg TradeCargoInfoLeg { get; set; }
            internal TradeOptionInfo TradeOptionInfo { get; set; }
            internal string Books { get; set; }
        }

        #endregion*/

        #region Nested type: TraderInformation

        internal class TraderInformation
        {
            internal Trader Trader { get; set; }
            internal ProfitCentre ProfitCentre { get; set; }
            internal Desk Desk { get; set; }
            internal Market Market { get; set; }
            internal Company Company { get; set; }
            internal Book Book { get; set; }
            internal TraderMarketAssoc TraderMarket { get; set; }
            internal TraderCompanyAssoc TraderCompany { get; set; }
            internal TraderBook TraderBook { get; set; }
            internal UserTrader UserTrader { get; set; }
        }

        #endregion

        #region Nested type: VesselInformation

        internal class VesselInformation
        {
            internal Company Company;
            internal Market Market;
            internal Vessel Vessel;
            internal VesselInfo VesselInfo;
        }

        #endregion

        #region Nested type: VesselPoolInformation

        internal class VesselPoolInformation
        {
            internal PoolInfoVessel PoolInfoVessel;
            internal Vessel Vessel;
            internal VesselPool VesselPool;
            internal VesselPoolInfo VesselPoolInfo;
            internal VesselBenchmarking VesselBenchmarking;
        }

        #endregion

        #region Nested type: BookInformation

        internal class BookInformation
        {
            internal Book Book;
            internal UserBook UserBook;
        }

        #endregion

        #region Nested type: LoanInformation

        internal class LoanInformation
        {
            internal Loan Loan { get; set; }
            internal LoanTranche LoanTranche { get; set; }
            internal LoanDrawdown LoanDrawdown { get; set; }
        }

        #endregion

        #region Nested type: CashFlowInformation

        internal class CashFlowInformation
        {
            internal long CashFlowModelId { get; set; }
            internal CashFlowGroup CashFlowGroup { get; set; }
            internal long CashFlowGroupId { get; set; }
            internal CashFlowItem CashFlowItem { get; set; }
        }

        #endregion

        #region Nested Type: TradeTCLegsInfo

        internal class TcTradeLegInfo
        {
            internal TradeTcInfoLeg TradeTCInfoLeg { get; set; }
            internal TradeTcInfo TradeTCInfo { get; set; }
            internal long TradeInfoId { get; set; }
            internal string VesselName { get; set; }
        }

        internal class CargoTradeLegInfo
        {
            internal TradeCargoInfoLeg TradeCargoInfoLeg { get; set; }
            internal TradeCargoInfo TradeCargoInfo { get; set; }
            internal long TradeInfoId { get; set; }
            internal string VesselName { get; set; }
        }

        #endregion

        #region types FileStruct & FileListStyle

        public struct FileStruct
        {
            public string Flags;
            public string Owner;
            public string Group;
            public bool IsDirectory;
            public DateTime CreateTime;
            public string Name;
        }

        public enum FileListStyle
        {
            UnixStyle,
            WindowsStyle,
            Unknown
        }

        #endregion

        #region class ParseListDirectory

        public class ParseListDirectory
        {
            public FileStruct[] GetList(string datastring)
            {
                List<FileStruct> myListArray = new List<FileStruct>();
                string[] dataRecords = datastring.Split('\n');
                FileListStyle _directoryListStyle = GuessFileListStyle(dataRecords);
                foreach (string s in dataRecords)
                {
                    if (_directoryListStyle != FileListStyle.Unknown && s != "")
                    {
                        FileStruct f = new FileStruct();
                        f.Name = "..";
                        switch (_directoryListStyle)
                        {
                            case FileListStyle.UnixStyle:
                                f = ParseFileStructFromUnixStyleRecord(s);
                                break;
                            case FileListStyle.WindowsStyle:
                                f = ParseFileStructFromWindowsStyleRecord(s);
                                break;
                        }
                        if (!(f.Name == "." || f.Name == ".."))
                        {
                            myListArray.Add(f);
                        }
                    }
                }
                return myListArray.ToArray();
            }
            public FileStruct ParseFileStructFromWindowsStyleRecord(string Record)
            {
                FileStruct f = new FileStruct();
                string processstr = Record.Trim();
                string dateStr = processstr.Substring(0, 8);
                processstr = (processstr.Substring(8, processstr.Length - 8)).Trim();
                string timeStr = processstr.Substring(0, 7);
                processstr = (processstr.Substring(7, processstr.Length - 7)).Trim();
                f.CreateTime = DateTime.Parse(dateStr + " " + timeStr);
                if (processstr.Substring(0, 5) == "<DIR>")
                {
                    f.IsDirectory = true;
                    processstr = (processstr.Substring(5, processstr.Length - 5)).Trim();
                }
                else
                {
                    string[] strs = processstr.Split(new char[] { ' ' });
                    processstr = strs[1].Trim();
                    f.IsDirectory = false;
                }
                f.Name = processstr;  //Rest is name   
                return f;
            }
            public FileListStyle GuessFileListStyle(string[] recordList)
            {
                foreach (string s in recordList)
                {
                    if (s.Length > 10
                     && Regex.IsMatch(s.Substring(0, 10), "(-|d)(-|r)(-|w)(-|x)(-|r)(-|w)(-|x)(-|r)(-|w)(-|x)"))
                    {
                        return FileListStyle.UnixStyle;
                    }
                    else if (s.Length > 8
                     && Regex.IsMatch(s.Substring(0, 8), "[0-9][0-9]-[0-9][0-9]-[0-9][0-9]"))
                    {
                        return FileListStyle.WindowsStyle;
                    }
                }
                return FileListStyle.Unknown;
            }
            private FileStruct ParseFileStructFromUnixStyleRecord(string Record)
            {
                ///Assuming record style as
                /// dr-xr-xr-x   1 owner    group               0 Nov 25  2002 bussys
                FileStruct f = new FileStruct();
                string processstr = Record.Trim();
                f.Flags = processstr.Substring(0, 9);
                f.IsDirectory = (f.Flags[0] == 'd');
                processstr = (processstr.Substring(11)).Trim();
                CutSubstringFromStringWithTrim(ref processstr, ' ', 0);   //skip one part
                f.Owner = CutSubstringFromStringWithTrim(ref processstr, ' ', 0);
                f.Group = CutSubstringFromStringWithTrim(ref processstr, ' ', 0);
                CutSubstringFromStringWithTrim(ref processstr, ' ', 0);   //skip one part

                var tokens = processstr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length > 3)
                {
                    //f.CreateTime = DateTime.ParseExact(tokens[0] + tokens[1], "MM-dd-yyHH:mmtt",
                    //                                   CultureInfo.InvariantCulture);
                    f.Name = tokens[3];
                }

                //f.CreateTime = DateTime.Parse(_cutSubstringFromStringWithTrim(ref processstr, ' ', 8));
                //f.Name = processstr;   //Rest of the part is name
                return f;
            }
            public string CutSubstringFromStringWithTrim(ref string s, char c, int startIndex)
            {
                int pos1 = s.IndexOf(c, startIndex);
                string retString = s.Substring(0, pos1);
                s = (s.Substring(pos1)).Trim();
                return retString;
            }
        }

        #endregion

        #endregion

        #region New FTP

        public static void getFileList(string sourceURI, string sourceUser, string sourcePass, List<FileName> sourceFileList)
        {
            string line = "";
            FtpWebRequest sourceRequest;
            sourceRequest = (FtpWebRequest)WebRequest.Create(sourceURI);
            //sourceRequest.Credentials = new NetworkCredential(sourceUser, sourcePass);
            sourceRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            sourceRequest.UseBinary = true;
            sourceRequest.KeepAlive = false;
            sourceRequest.Timeout = -1;
            sourceRequest.UsePassive = true;
            FtpWebResponse sourceRespone = (FtpWebResponse)sourceRequest.GetResponse();
            //Creates a list(fileList) of the file names
            using (Stream responseStream = sourceRespone.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    line = reader.ReadLine();
                    while (line != null)
                    {
                        var fileName = new FileName
                        {
                            fName = line
                        };
                        sourceFileList.Add(fileName);
                        line = reader.ReadLine();
                    }
                }
            }
        }
        #endregion

        #region Public Classes

        #region Type: TradeInformation

        public class TradeInformation
        {
            public Trade Trade { get; set; }
            public TradeInfo TradeInfo { get; set; }
            public TradeTcInfo TradeTCInfo { get; set; }
            public TradeFfaInfo TradeFFAInfo { get; set; }
            public TradeTcInfoLeg TradeTCInfoLeg { get; set; }
            public TradeCargoInfo TradeCargoInfo { get; set; }
            public TradeCargoInfoLeg TradeCargoInfoLeg { get; set; }
            public TradeOptionInfo TradeOptionInfo { get; set; }
            public string Books { get; set; }
            public BookPositionCalculatedByDaysEnum BookPositionCalcByDays { get; set; }
        }

        public class BasicTrade
        {
            public Trade Trade { get; set; }
            public TradeInfo TradeInfo { get; set; }

        }

        #endregion

        #endregion

        #region Compiled Queries

        public static class Queries
        {
            internal static Func<DataContext, IQueryable<VesselInformation>>
                GetVesselsWithCompaniesAndMarkets =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objVessel in dataContext.Vessels
                        from objVesselInfo in dataContext.VesselInfos
                        from objMarket in dataContext.Markets
                        from objCompany in dataContext.Companies
                        where objVessel.Id == objVesselInfo.VesselId
                              && objMarket.Id == objVessel.MarketId
                              && objCompany.Id == objVessel.CompanyId
                        select new VesselInformation { Company = objCompany, Market = objMarket, Vessel = objVessel, VesselInfo = objVesselInfo }
                        );

            internal static Func<DataContext, IQueryable<VesselExpense>>
                GetVesselsExpenses =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objVessel in dataContext.Vessels
                        from objVesselExpense in dataContext.VesselExpenses
                        where objVessel.Id == objVesselExpense.VesselId
                        select objVesselExpense
                        );

            internal static Func<DataContext, long, IQueryable<TraderMarketAssoc>>
                GetTraderMarketsByTrader =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long traderId) =>
                        from objTraderMarket in dataContext.TraderMarketAssocs
                        where objTraderMarket.TraderId == traderId
                        select objTraderMarket
                        );

            internal static Func<DataContext, long, IQueryable<TraderCompanyAssoc>>
                GetTraderCompaniesByTrader =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long traderId) =>
                        from objTraderCompany in dataContext.TraderCompanyAssocs
                        where objTraderCompany.TraderId == traderId
                        select objTraderCompany
                        );

            internal static Func<DataContext, long, IQueryable<TraderBook>>
                GetTraderBooksByTrader =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long traderId) =>
                        from objTraderBook in dataContext.TraderBooks
                        where objTraderBook.TraderId == traderId
                        select objTraderBook
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Desk>>
                GetDesksByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objDesk in dataContext.Desks
                        where objDesk.Status == status
                        select objDesk
                        );

            internal static Func<DataContext, ActivationStatusEnum, ContactTypeEnum, IQueryable<Contact>>
                GetContactsByTypeStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status, ContactTypeEnum type) =>
                        from objContact in dataContext.Contacts
                        where objContact.Status == status
                              && objContact.Type == type
                        select objContact
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<ProfitCentre>>
                GetProfitCentresByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objProfitCentre in dataContext.ProfitCentres
                        where objProfitCentre.Status == status
                        select objProfitCentre
                        );

            internal static Func<DataContext, IQueryable<TraderInformation>>
                GetTradersWithCompanies =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objTrader in dataContext.Traders
                        from objCompany in dataContext.Companies
                        from objTraderCompany in dataContext.TraderCompanyAssocs
                        where objTrader.Id == objTraderCompany.TraderId
                              && objCompany.Id == objTraderCompany.CompanyId
                        select new TraderInformation { Trader = objTrader, Company = objCompany, TraderCompany = objTraderCompany }
                        );

            internal static Func<DataContext, IQueryable<TraderInformation>>
                GetTradersWithBooks =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objTrader in dataContext.Traders
                        from objBook in dataContext.Books
                        from objTraderBook in dataContext.TraderBooks
                        where objTrader.Id == objTraderBook.TraderId
                              && objBook.Id == objTraderBook.BookId
                        select new TraderInformation { Trader = objTrader, Book = objBook, TraderBook = objTraderBook }
                        );

            internal static Func<DataContext, IQueryable<VesselPoolInformation>>
                GetVesselPoolsWithVessels =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objVesselPool in dataContext.VesselPools
                        from objVesselPoolInfo in dataContext.VesselPoolInfos
                        from objPoolInfoVessel in dataContext.PoolInfoVessels
                        from objVessel in dataContext.Vessels
                        where objVesselPoolInfo.PeriodTo == null
                              && objVesselPoolInfo.VesselPoolId == objVesselPool.Id
                              && objPoolInfoVessel.VesselPoolInfoId == objVesselPoolInfo.Id
                              && objPoolInfoVessel.VesselId == objVessel.Id
                        select
                            new VesselPoolInformation { VesselPool = objVesselPool, VesselPoolInfo = objVesselPoolInfo, Vessel = objVessel, PoolInfoVessel = objPoolInfoVessel }
                        );

            internal static Func<DataContext, long, IQueryable<VesselPoolInformation>>
                GetVesselPoolInfosHistoryByVessel =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long vesselPoolId) =>
                        from objVesselPoolInfo in dataContext.VesselPoolInfos
                        from objPoolInfoVessel in dataContext.PoolInfoVessels
                        from objVessel in dataContext.Vessels
                        where objVesselPoolInfo.PeriodTo != null
                              && objVesselPoolInfo.VesselPoolId == vesselPoolId
                              && objPoolInfoVessel.VesselPoolInfoId == objVesselPoolInfo.Id
                              && objPoolInfoVessel.VesselId == objVessel.Id
                        select
                            new VesselPoolInformation
                            {
                                VesselPoolInfo = objVesselPoolInfo,
                                Vessel = objVessel,
                                PoolInfoVessel = objPoolInfoVessel
                            }
                        );

            internal static Func<DataContext, long, IQueryable<VesselPoolInformation>>
                GetCurrentPoolInfoVesselsByVesselPool =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long vesselPoolId) =>
                        from objVesselPoolInfo in dataContext.VesselPoolInfos
                        from objPoolInfoVessel in dataContext.PoolInfoVessels
                        where objVesselPoolInfo.PeriodTo == null
                              && objVesselPoolInfo.VesselPoolId == vesselPoolId
                              && objPoolInfoVessel.VesselPoolInfoId == objVesselPoolInfo.Id
                        select
                            new VesselPoolInformation { VesselPoolInfo = objVesselPoolInfo, PoolInfoVessel = objPoolInfoVessel }
                        );

            internal static Func<DataContext, IQueryable<TraderInformation>>
                GetTradersWithMarkets =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objTrader in dataContext.Traders
                        from objMarket in dataContext.Markets
                        from objTraderMarket in dataContext.TraderMarketAssocs
                        where objTrader.Id == objTraderMarket.TraderId
                              && objMarket.Id == objTraderMarket.MarketId
                        select new TraderInformation { Trader = objTrader, Market = objMarket, TraderMarket = objTraderMarket }
                        );

            internal static Func<DataContext, IQueryable<TraderInformation>>
                GetTradersWithProfitCentresDesks =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objTrader in dataContext.Traders
                        from objProfitCentre in
                            dataContext.ProfitCentres.Where(a => a.Id == objTrader.ProfitCentreId).DefaultIfEmpty()
                        from objDesk in
                            dataContext.Desks.Where(a => a.Id == objTrader.DeskId).DefaultIfEmpty()
                        select new TraderInformation { Trader = objTrader, ProfitCentre = objProfitCentre, Desk = objDesk }
                        );

            internal static Func<DataContext, IQueryable<Company>>
                GetCompaniesAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objCompany in dataContext.Companies
                        select objCompany
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Company>>
                GetCompaniesByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objCompany in dataContext.Companies
                        where objCompany.Status == status
                        orderby objCompany.Name
                        select objCompany
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<CompanySubtype>>
                GetCompanySubtypesByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objCompanySubtype in dataContext.CompanySubtypes
                        where objCompanySubtype.Status == status
                        select objCompanySubtype
                        );

            internal static Func<DataContext, IQueryable<CompanyInformation>>
                GetCompaniesWithManagersSubtypes =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objCompany in dataContext.Companies
                        from objCompanySubtype in dataContext.CompanySubtypes
                        from objContact in
                            dataContext.Contacts.Where(a => a.Id == objCompany.ManagerId).DefaultIfEmpty()
                        where objCompany.SubtypeId == objCompanySubtype.Id
                        select
                            new CompanyInformation { Company = objCompany, Contact = objContact, Subtype = objCompanySubtype }
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<VesselPool>>
                GetVesselPoolsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objVesselPool in dataContext.VesselPools
                        where objVesselPool.Status == status
                        orderby objVesselPool.Name
                        select objVesselPool
                        );

            internal static Func<DataContext, DateTime, IQueryable<VesselPoolInformation>>
                GetVesselPerPoolAndBenchMarkingPerSignDate =
                    CompiledQuery.Compile((DataContext dataContext, DateTime signDate) =>

                                          from objVessel in dataContext.Vessels
                                          from objVesselPoolInfo in dataContext.VesselPoolInfos
                                          from objPoolInfoVessel in dataContext.PoolInfoVessels
                                          from objVesselPool in dataContext.VesselPools
                                          join objVesselBenchmarkingl in
                                              dataContext.VesselBenchmarkings.Where(
                                                  a => signDate.Date >= a.PeriodFrom.Date && a.PeriodTo.Date > signDate.Date)
                                              on objVessel.Id equals objVesselBenchmarkingl.VesselId into
                                              vesselBenchmarkingLefts
                                          from objVesselBenchmarking in vesselBenchmarkingLefts.DefaultIfEmpty()
                                          where objVessel.Id == objPoolInfoVessel.VesselId
                                                && objPoolInfoVessel.VesselPoolInfoId == objVesselPoolInfo.Id
                                                && objVesselPoolInfo.PeriodFrom.Date <= signDate.Date
                                                &&
                                                (objVesselPoolInfo.PeriodTo == null ||
                                                 objVesselPoolInfo.PeriodTo.Value.Date > signDate.Date)
                                                && objVesselPool.Id == objVesselPoolInfo.VesselPoolId
                                          select
                                              new VesselPoolInformation()
                                              {
                                                  Vessel = objVessel,
                                                  VesselPool = objVesselPool,
                                                  VesselPoolInfo = objVesselPoolInfo,
                                                  VesselBenchmarking = objVesselBenchmarking
                                              }
                        );

            internal static Func<DataContext, IQueryable<VesselPoolInformation>>
                GetVesselPerPoolAndBenchMarking =
                    CompiledQuery.Compile((DataContext dataContext) =>

                                          from objVessel in dataContext.Vessels
                                          from objVesselPoolInfo in dataContext.VesselPoolInfos
                                          from objVesselPool in dataContext.PoolInfoVessels
                                          from objVesselBenchmarking in dataContext.VesselBenchmarkings
                                          where objVessel.Id == objVesselPool.VesselId
                                                && objVesselPool.VesselPoolInfoId == objVesselPoolInfo.Id
                                                && objVesselBenchmarking.VesselId == objVessel.Id
                                                && DateTime.Now.Date >= objVesselBenchmarking.PeriodFrom.Date
                                                && objVesselBenchmarking.PeriodTo.Date >= DateTime.Now.Date
                                                && objVesselPool.Id == objVesselPoolInfo.VesselPoolId
                                                && objVesselPoolInfo.PeriodTo == null
                                          select
                                              new VesselPoolInformation()
                                              {
                                                  Vessel = objVessel,
                                                  VesselPoolInfo = objVesselPoolInfo,
                                                  VesselBenchmarking = objVesselBenchmarking
                                              }
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<ClearingHouse>>
                GetClearingHousesByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objClearingHouse in dataContext.Clearinghouses
                        where objClearingHouse.Status == status
                        orderby objClearingHouse.Name
                        select objClearingHouse
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Account>>
                GetAccountsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objAccount in dataContext.Accounts
                        where objAccount.Status == status
                        orderby objAccount.Name
                        select objAccount
                        );

            internal static Func<DataContext, IQueryable<ProfitCentre>>
                GetProfitCentresAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objProfitCentre in dataContext.ProfitCentres
                        select objProfitCentre
                        );

            internal static Func<DataContext, IQueryable<Desk>>
                GetDesksAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objDesk in dataContext.Desks
                        select objDesk
                        );

            internal static Func<DataContext, IQueryable<ClearingHouse>>
                GetClearingHousesAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objClearingHouse in dataContext.Clearinghouses
                        select objClearingHouse
                        );

            internal static Func<DataContext, IQueryable<CompanySubtype>>
                GetCompanySubtypesAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objCompanySubtype in dataContext.CompanySubtypes
                        select objCompanySubtype
                        );

            internal static Func<DataContext, IQueryable<IndexInformation>>
                GetIndexesAllWithMarkets =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objIndex in dataContext.Indexes
                        from objMarket in dataContext.Markets
                        where objIndex.MarketId == objMarket.Id
                        select new IndexInformation { Index = objIndex, Market = objMarket }
                        );

            internal static Func<DataContext, IQueryable<AccountInformation>>
                GetAccountsAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objAccount in dataContext.Accounts
                        from objCompany in dataContext.Companies
                        from objBank in dataContext.Companies
                        from objCurrency in dataContext.Currencies
                        where objAccount.CompanyId == objCompany.Id
                              && objAccount.BankId == objBank.Id
                              && objAccount.CurrencyId == objCurrency.Id
                        select
                            new AccountInformation { Account = objAccount, Company = objCompany, Bank = objBank, Currency = objCurrency }
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Book>>
                GetBooksByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objBook in dataContext.Books
                        where objBook.Status == status
                        select objBook
                        );

            internal static Func<DataContext, IQueryable<Book>>
                GetBooksAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objBook in dataContext.Books
                        select objBook
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Index>>
                GetIndexesByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objIndex in dataContext.Indexes
                        where objIndex.Status == status
                        orderby objIndex.Name
                        select objIndex
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<TraderMarketAssoc>>
                GetTraderMarketAssocsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objTrader in dataContext.Traders
                        from objMarket in dataContext.Markets
                        from objTraderMarketAssoc in dataContext.TraderMarketAssocs
                        where objTrader.Status == status
                              && objTraderMarketAssoc.TraderId == objTrader.Id
                              && objTraderMarketAssoc.MarketId == objMarket.Id
                        select objTraderMarketAssoc
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<TraderCompanyAssoc>>
                GetTraderCompanyAssocsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objTrader in dataContext.Traders
                        from objCompany in dataContext.Companies
                        from objCompanyTraderAssoc in dataContext.TraderCompanyAssocs
                        where objTrader.Status == status
                              && objCompany.Status == status
                              && objCompanyTraderAssoc.TraderId == objTrader.Id
                              && objCompanyTraderAssoc.CompanyId == objCompany.Id
                        select objCompanyTraderAssoc
                        );

            internal static
                Func
                    <DataContext, CompanyTypeEnum, CompanySubtypeEnum, long,
                        IQueryable<Company>>
                GetCompaniesByUserTraderAssocsByStatusBySubType =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanyTypeEnum type, CompanySubtypeEnum subType,
                         long userId) =>
                        from objTrader in dataContext.Traders
                        from objCompany in dataContext.Companies
                        from objCompanyTraderAssoc in dataContext.TraderCompanyAssocs
                        from objUserTraderAssoc in dataContext.UserTraders
                        from objCompanySubtype in dataContext.CompanySubtypes
                        where objTrader.Status == ActivationStatusEnum.Active
                              && objCompany.Status == ActivationStatusEnum.Active
                              && objCompany.SubtypeId == objCompanySubtype.Id
                              && objCompany.Type == type
                              && objCompanyTraderAssoc.TraderId == objTrader.Id
                              && objCompanyTraderAssoc.CompanyId == objCompany.Id
                              && objUserTraderAssoc.TraderId == objTrader.Id
                              && objUserTraderAssoc.UserId == userId
                              && objCompanySubtype.Subtype == subType
                        select objCompany
                        );

            internal static
                Func
                    <DataContext, CompanyTypeEnum, long, IQueryable<Company>>
                GetCompaniesByUserTraderAssocsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanyTypeEnum type,
                         long userId) =>
                        from objTrader in dataContext.Traders
                        from objCompany in dataContext.Companies
                        from objCompanyTraderAssoc in dataContext.TraderCompanyAssocs
                        from objUserTraderAssoc in dataContext.UserTraders
                        where objTrader.Status == ActivationStatusEnum.Active
                              && objCompany.Status == ActivationStatusEnum.Active
                              && objCompany.Type == type
                              && objCompanyTraderAssoc.TraderId == objTrader.Id
                              && objCompanyTraderAssoc.CompanyId == objCompany.Id
                              && objUserTraderAssoc.TraderId == objTrader.Id
                              && objUserTraderAssoc.UserId == userId
                        orderby objCompany.Name
                        select objCompany
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Vessel>>
                GetVesselsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objVessel in dataContext.Vessels
                        where objVessel.Status == status
                        orderby objVessel.Name
                        select objVessel
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Vessel>>
                GetVesselsNotInPoolByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objVessel in dataContext.Vessels
                        where objVessel.Status == status
                              && !(from objPoolInfoVessel in dataContext.PoolInfoVessels
                                   from objPoolInfo in dataContext.VesselPoolInfos
                                   where
                                       objPoolInfo.PeriodTo == null
                                       && objPoolInfo.Id == objPoolInfoVessel.VesselPoolInfoId
                                   select objPoolInfoVessel.VesselId).Contains(objVessel.Id)
                        select objVessel
                        );

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Trader>>
                GetTradersByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objTrader in dataContext.Traders
                        where objTrader.Status == status
                        select objTrader
                        );

            internal static Func<DataContext, IQueryable<Trader>>
                GetTradersAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objTrader in dataContext.Traders
                        select objTrader
                        );

            internal static Func<DataContext, ActivationStatusEnum, long, IQueryable<Trader>>
                GetTradersByStatusByUser =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status, long userId) =>
                        from objTrader in dataContext.Traders
                        from objUserTrader in dataContext.UserTraders
                        where objTrader.Status == status
                              && objUserTrader.UserId == userId
                              && objUserTrader.TraderId == objTrader.Id
                        orderby objTrader.Name
                        select objTrader
                        );

            internal static Func<DataContext, IQueryable<TraderInformation>>
                GetTradersWithUsers =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objTrader in dataContext.Traders
                        from objUserTrader in dataContext.UserTraders
                        where objUserTrader.TraderId == objTrader.Id
                        select new TraderInformation { Trader = objTrader, UserTrader = objUserTrader }
                        );

            internal static Func<DataContext, IQueryable<BookInformation>>
                GetBooksWithUsers =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objBook in dataContext.Books
                        from objUserBook in dataContext.UserBooks
                        where objUserBook.BookId == objBook.Id
                        select new BookInformation() { Book = objBook, UserBook = objUserBook }
                        );

            internal static Func<DataContext, IQueryable<Currency>>
                GetCurrenciesAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objCurrency in dataContext.Currencies
                        select objCurrency
                        );

            internal static Func<DataContext, CompanySubtypeEnum, ActivationStatusEnum, IQueryable<Company>>
                GetCompaniesBySubtypeStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanySubtypeEnum subType, ActivationStatusEnum status) =>
                        from objCompany in dataContext.Companies
                        from objCompanySubtype in dataContext.CompanySubtypes
                        where
                            objCompany.SubtypeId == objCompanySubtype.Id
                            && objCompany.Status == status
                            && objCompanySubtype.Subtype == subType
                        orderby objCompany.Name
                        select objCompany
                        );

            internal static Func<DataContext, CompanyTypeEnum, CompanySubtypeEnum, ActivationStatusEnum, IQueryable<Company>>
                GetCompaniesByTypeSubtypeStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanyTypeEnum type, CompanySubtypeEnum subType, ActivationStatusEnum status) =>
                        from objCompany in dataContext.Companies
                        from objCompanySubtype in dataContext.CompanySubtypes
                        where
                            objCompany.SubtypeId == objCompanySubtype.Id &&
                            objCompany.Type == type && objCompany.Status == status
                            && objCompanySubtype.Subtype == subType
                        select objCompany
                        );

            internal static Func<DataContext, CompanyTypeEnum, ActivationStatusEnum, IQueryable<Company>>
                GetCompaniesByTypeStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanyTypeEnum type, ActivationStatusEnum status) =>
                        from objCompany in dataContext.Companies
                        where
                            objCompany.Type == type && objCompany.Status == status
                        select objCompany
                        );

            internal static Func<DataContext, CompanyTypeEnum, CompanySubtypeEnum, ActivationStatusEnum, IQueryable<Company>>
                GetCompaniesByNotTypeSubtypeStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanyTypeEnum type, CompanySubtypeEnum subType, ActivationStatusEnum status) =>
                        from objCompany in dataContext.Companies
                        from objCompanySubtype in dataContext.CompanySubtypes
                        where
                            objCompany.SubtypeId == objCompanySubtype.Id &&
                            objCompany.Type != type && objCompany.Status == status
                            && objCompanySubtype.Subtype == subType
                        select objCompany
                        );

            internal static Func<DataContext, CompanyTypeEnum, ActivationStatusEnum, IQueryable<Company>>
                GetCompaniesByNotTypeStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, CompanyTypeEnum type, ActivationStatusEnum status) =>
                        from objCompany in dataContext.Companies
                        where
                            objCompany.Type != type && objCompany.Status == status
                        select objCompany
                        );

            internal static Func<DataContext, long, IQueryable<IndexCustomValue>>
                GetCustomIndexValuesByIndex =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long indexId) =>
                        from objCustomIndexValue in dataContext.IndexCustomValues
                        where objCustomIndexValue.IndexId == indexId
                        select objCustomIndexValue);

            internal static Func<DataContext, long, DateTime, IQueryable<IndexFFAValue>>
            GetBFAValuesByDateByIndex =
                CompiledQuery.Compile(
                    (DataContext dataContext, long indexId, DateTime date) =>
                    from objIndexValue in dataContext.IndexFFAValues
                    where objIndexValue.IndexId == indexId
                    && objIndexValue.Date == date
                    select objIndexValue
                    );

            internal static Func<DataContext, long, DateTime, IQueryable<IndexSpotValue>>
            GetSpotValuesByDateByIndex =
                CompiledQuery.Compile(
                    (DataContext dataContext, long indexId, DateTime date) =>
                    from objIndexValue in dataContext.IndexSpotValues
                    where objIndexValue.IndexId == indexId
                    && objIndexValue.Date == date
                    select objIndexValue
                    );


            internal static Func<DataContext, IQueryable<Market>>
                GetMarketSegments =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objMarketSegment in dataContext.Markets
                        orderby objMarketSegment.Name
                        select objMarketSegment);

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Region>>
                GetMarketRegionsByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objMarketRegion in dataContext.Regions
                        where objMarketRegion.Status == status
                        orderby objMarketRegion.Name
                        select objMarketRegion);

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<Route>>
                GetMarketRoutesByStatus =
                    CompiledQuery.Compile(
                        (DataContext dataContext, ActivationStatusEnum status) =>
                        from objMarketRoute in dataContext.Routes
                        where objMarketRoute.Status == status
                        orderby objMarketRoute.Name
                        select objMarketRoute);

            internal static Func<DataContext, IQueryable<Index>>
                GetAllIndexes =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objIndex in dataContext.Indexes
                        select objIndex
                        );

            internal static Func<DataContext, IQueryable<VesselBenchmarking>>
                GetVesselBenchmarkings =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objVesselBenchmarking in dataContext.VesselBenchmarkings
                        select objVesselBenchmarking
                        );

            internal static Func<DataContext, IQueryable<User>>
                GetUsers =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objUser in dataContext.Users
                        select objUser
                        );

            internal static Func<DataContext, long, IQueryable<UserTrader>>
                GetUserTraderAssocsByUserId =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long userId) =>
                        from objUserTrader in dataContext.UserTraders
                        where objUserTrader.UserId == userId
                        select objUserTrader
                        );

            internal static Func<DataContext, long, IQueryable<UserBook>>
                GetUserBookAssocsByUserId =
                    CompiledQuery.Compile(
                        (DataContext dataContext, long userId) =>
                        from objUserBook in dataContext.UserBooks
                        where objUserBook.UserId == userId
                        select objUserBook
                        );

            internal static Func<DataContext, long, IQueryable<Book>>
                GetBooksByUserId =
                    CompiledQuery.Compile(
                    (DataContext dataContext, long userId) =>
                        from objBook in dataContext.Books
                        from objUserBook in
                            dataContext.UserBooks
                        where objUserBook.UserId == userId
                                && objUserBook.BookId == objBook.Id
                                && objBook.Status == ActivationStatusEnum.Active
                        select objBook);

            internal static Func<DataContext, IQueryable<Contact>>
                GetContactsAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objContact in dataContext.Contacts
                        select objContact
                        );

            internal static Func<DataContext, IQueryable<Loan>>
                GetLoansAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoan in dataContext.Loans
                        select objLoan
                        );

            internal static Func<DataContext, IQueryable<LoanBank>>
                GetLoanBanks =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoan in dataContext.Loans
                        from objLoanBank in dataContext.LoanBanks
                        from objBank in dataContext.Companies
                        where objLoan.Id == objLoanBank.LoanId
                              && objLoanBank.BankId == objBank.Id
                        select objLoanBank
                        );

            internal static Func<DataContext, IQueryable<LoanCollateralAsset>>
                GetLoanAssets =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoan in dataContext.Loans
                        from objLoanAsset in dataContext.LoanCollateralAssets
                        where objLoan.Id == objLoanAsset.LoanId
                        select objLoanAsset
                        );

            internal static Func<DataContext, IQueryable<LoanInformation>>
                GetLoanTranches =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoan in dataContext.Loans
                        from objLoanTranche in dataContext.LoanTranches
                        from objDrawdown in dataContext.LoanDrawdowns
                        where objLoan.Id == objLoanTranche.LoanId
                              && objLoanTranche.Id == objDrawdown.LoanTrancheId
                        select new LoanInformation { LoanTranche = objLoanTranche, LoanDrawdown = objDrawdown }
                        );

            internal static Func<DataContext, IQueryable<LoanInterestInstallment>>
                GetLoanInterestInstallmentsAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoanInterestInstallment in dataContext.LoanInterestInstallments
                        select objLoanInterestInstallment
                        );

            internal static Func<DataContext, IQueryable<LoanPrincipalInstallment>>
                GetLoanPrincipalInstallmentsAll =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoanPrincipalInstallment in dataContext.LoanPrincipalInstallments
                        select objLoanPrincipalInstallment
                        );

            internal static Func<DataContext, IQueryable<LoanBorrower>>
                GetLoanBorrowers =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objLoan in dataContext.Loans
                        from objLoanBorrower in dataContext.LoanBorrowers
                        where objLoan.Id == objLoanBorrower.LoanId
                        select objLoanBorrower
                        );

            internal static Func<DataContext, IQueryable<RiskVolatility>>
                GetAllRiskVolatilities =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objVolatility in dataContext.RiskVolatilities
                        select objVolatility
                        );

            internal static Func<DataContext, IQueryable<RiskCorrelation>>
                GetAllRiskCorrelations =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objRiskCorrelation in dataContext.RiskCorrelations
                        select objRiskCorrelation
                        );

            internal static Func<DataContext, IQueryable<HierarchyNode>>
                GetHierarchyNodes =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objHierarchyNode in dataContext.HierarchyNodes
                        select objHierarchyNode
                        );

            internal static Func<DataContext, IQueryable<HierarchyNodeType>>
                GetHierarchyNodeTypes =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objHierarchyNodeType in dataContext.HierarchyNodeTypes
                        select objHierarchyNodeType
                        );

            internal static Func<DataContext, IQueryable<CashFlowModel>>
                GetCashFlowModels =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objModel in dataContext.CashFlowModels
                        select objModel
                        );

            internal static Func<DataContext, IQueryable<CashFlowGroup>>
                GetCashFlowGroups =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objGroup in dataContext.CashFlowGroups
                        select objGroup
                        );

            internal static Func<DataContext, IQueryable<CashFlowItem>>
                GetCashFlowItems =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objItem in dataContext.CashFlowItems
                        select objItem
                        );

            internal static Func<DataContext, IQueryable<CashFlowModelGroup>>
                GetCashFlowModelGroups =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objAssoc in dataContext.CashFlowModelsGroups
                        select objAssoc
                        );

            internal static Func<DataContext, IQueryable<CashFlowGroupItem>>
                GetCashFlowGroupItems =
                    CompiledQuery.Compile(
                        (DataContext dataContext) =>
                        from objAssoc in dataContext.CashFlowGroupsItems
                        select objAssoc
                        );

            internal static Func<DataContext, IQueryable<InterestReferenceRate>>
                GetInterestReferenceRates =
                    CompiledQuery.Compile((DataContext dataContext) =>
                                          from objInterestReferenceRate in dataContext.InterestReferenceRates
                                          select objInterestReferenceRate);

            internal static Func<DataContext, IQueryable<ExchangeRate>>
                GetExchangeRates =
                    CompiledQuery.Compile((DataContext dataContext) =>
                                          from objExchangeRate in dataContext.ExchangeRates
                                          select objExchangeRate);

            internal static Func<DataContext, ActivationStatusEnum, IQueryable<CashFlowModel>>
                GetCashFlowModelsByStatus =
                    CompiledQuery.Compile((DataContext dataContext, ActivationStatusEnum status) =>
                                          from objModel in dataContext.CashFlowModels
                                          where objModel.Status == status
                                          select objModel);
            internal static Func<DataContext, IQueryable<MCSimulationInfo>>
               GetAllMCRuns =
                   CompiledQuery.Compile(
                       (DataContext dataContext) =>
                       from objMCRun in dataContext.MCSimulationInfo
                       select objMCRun
                       );

        }

        #endregion

    }
}
