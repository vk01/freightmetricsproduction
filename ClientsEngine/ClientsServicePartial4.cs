﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using Exis.Domain;
using Exis.WCFExtensions;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;
using System.Data.Common;
using System.Globalization;
using System.Diagnostics;

namespace Exis.ClientsEngine
{
    public partial class ClientsService
    {
        #region IClientsService Members

        #region Import with Versioning

        public int? ImportTrades(List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, List<TmpImportCargoTrade> tmpImportCargoTrades, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades, out string errorMessage)
        {
            DataContext dataContext = null;
            string userName = null;

            errorMessage = "";

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                DateTime runDate = DateTime.Now;

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "ImportTrades: " };
                }

                if (importInProgress) return 7;//Return 7 in case an import is in progress

                lock (importInProgressLock)
                {
                    importInProgress = true;
                }

                AppParameter interfaceUserNameAppParam =
                    dataContext.AppParameters.SingleOrDefault(a => a.Code == "INTERFACE_USER_NAME");

                if (interfaceUserNameAppParam == null)
                {
                    SiAuto.Main.LogError("Application parameter 'INTERFACE_USER_NAME' was not found.");
                    importInProgress = false;
                    errorMessage = "There was an error during import. Application parameter 'INTERFACE_USER_NAME' was not found.";

                    return null;
                }

                string interfaceUserName = interfaceUserNameAppParam.Value;

                int? importRes = null;
                if (tmpImportFfaOptionTrades != null)
                {
                    if(DeleteTempSoftMarData(dataContext, TradeTypeEnum.FFA) == null)
                    {
                        importInProgress = false;
                        errorMessage = "There was an error during import. There was an error during the deletion of temporary FFA SoftMar data.";
                        return 1;
                    }
                    foreach (TmpImportFfaOptionTrade tmpImportFfaOptionTrade in tmpImportFfaOptionTrades)
                    {
                        //var trades = dataContext.TradeInfos.Where(
                        //        a => a.ExternalCode == tmpImportFfaOptionTrade.ContractNo && a.DateTo == null).ToList();

                        //if (trades.Count > 1)
                        //{
                        //   var trade1 = trades.FirstOrDefault();
                        //    Debug.WriteLine(trade1.ExternalCode);
                        //}

                        tmpImportFfaOptionTrade.Id = dataContext.GetNextId(typeof(TmpImportFfaOptionTrade));
                        tmpImportFfaOptionTrade.RunDate = runDate;
                        dataContext.TmpImportFfaOptionTrades.InsertOnSubmit(tmpImportFfaOptionTrade);
                    }

                    importRes = InsertFfaOptionTrades_(dataContext, tmpImportFfaOptionTrades, interfaceUserName, out errorMessage);
                    if (importRes == null)
                    {
                        importInProgress = false;
                        return 1;
                    }
                }
                else if (tmpImportCargoTrades != null)
                {
                    if (DeleteTempSoftMarData(dataContext, TradeTypeEnum.Cargo) == null)
                    {
                        importInProgress = false;
                        errorMessage = "There was an error during import. There was an error during the deletion of temporary Cargo SoftMar data.";
                        return 1;
                    }
                    foreach (TmpImportCargoTrade tmpImportCargoTrade in tmpImportCargoTrades)
                    {
                        tmpImportCargoTrade.Id = dataContext.GetNextId(typeof(TmpImportCargoTrade));
                        tmpImportCargoTrade.RunDate = runDate;
                        dataContext.TmpImportCargoTrades.InsertOnSubmit(tmpImportCargoTrade);
                    }

                    importRes = InsertCargoTrades_(dataContext, tmpImportCargoTrades, interfaceUserName, out errorMessage);
                    if (importRes == null)
                    {
                        importInProgress = false;
                        return 1;
                    }
                }
                else if (tmpImportTcInTrades != null && tmpImportTcOutTrades != null)
                {
                    foreach (var tmpImportTcInTrade in tmpImportTcInTrades)
                    {
                        if (tmpImportTcOutTrades.Select(a => a.Voy).Contains(tmpImportTcInTrade.HeadFixtureRef))
                        {
                            importInProgress = false;
                            SiAuto.Main.LogError("External code '" + tmpImportTcInTrade.HeadFixtureRef + "' exists in both tc files.");
                            return 1; //Case there is a tc in trade with same external code with a tc out trade.
                        }
                    }
                    if (DeleteTempSoftMarData(dataContext, TradeTypeEnum.TC) == null)
                    {
                        errorMessage = "There was an error during import. There was an error during the deletion of temporary TC SoftMar data.";
                        importInProgress = false;
                        return 1;
                    }
                    foreach (TmpImportTcInTrade tmpImportTcInTrade in tmpImportTcInTrades)
                    {
                        tmpImportTcInTrade.Id = dataContext.GetNextId(typeof(TmpImportTcInTrade));
                        tmpImportTcInTrade.RunDate = runDate;
                        dataContext.TmpImportTcInTrades.InsertOnSubmit(tmpImportTcInTrade);
                    }
                    foreach (TmpImportTcOutTrade tmpImportTcOutTrade in tmpImportTcOutTrades)
                    {
                        tmpImportTcOutTrade.Id = dataContext.GetNextId(typeof(TmpImportTcOutTrade));
                        tmpImportTcOutTrade.RunDate = runDate;
                        dataContext.TmpImportTcOutTrades.InsertOnSubmit(tmpImportTcOutTrade);
                    }

                    importRes = InsertTcTrades_(dataContext, tmpImportTcInTrades, tmpImportTcOutTrades, interfaceUserName, out errorMessage);
                    if (importRes == null)
                    {
                        importInProgress = false;
                        return 1;
                    }
                }

                dataContext.SubmitChanges();

                importInProgress = false;

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                errorMessage = "There was an exception during import. Exception Message: " + exc.Message;
                return null;
            }
            finally
            {
                if (dataContext != null)
                    dataContext.Dispose();
            }
        }

        private int? InsertFfaOptionTrades_(DataContext dataContext, List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, string interfaceUserName, out string errorMessage)
        {
            DataContext configurationContext = null;

            string userName = null;
            errorMessage = "";

            bool isUpdate;
            const string defaultTraderName = "Unknown";
            const string defaultVesselName = "TBN";
            const string defaultBookName = "TBA";

            Dictionary<Type, Dictionary<string, long>> memConfigData;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                configurationContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "InsertFFAOptionTrades: " };
                }

                DateTime runDate = tmpImportFfaOptionTrades[0].RunDate;
                memConfigData = new Dictionary<Type, Dictionary<string, long>>();

                Index index = null;
                var insertedContractCodes = new List<string>();

                CultureInfo startDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportFfaOptionTrades.Select(a => a.StartDate).ToList());
                CultureInfo endDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportFfaOptionTrades.Select(a => a.EndDate).ToList());
                CultureInfo tradeDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportFfaOptionTrades.Select(a => a.TradeDate).ToList());

                CultureInfo brokerRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.BrokerRate).ToList());
                if (brokerRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Broker Rate'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'Broker Rate'";
                    return null;
                }

                CultureInfo fixedRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.FixedRate).ToList());
                if (fixedRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Fixed Rate'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'Fixed Rate'";
                    return null;
                }

                CultureInfo premiumRateRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.PremiumRate).ToList());
                if (premiumRateRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Premium Rate'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'Premium Rate'";
                    return null;
                }

                CultureInfo quantityCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.QtyTradedPerMonth).ToList());
                if (quantityCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'QtyTradedPerMonth'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'QtyTradedPerMonth'";
                    return null;
                }

                CultureInfo closedOutQuantityTotalCultureInfo =
                   GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.ClosedOutQuantityTotal).ToList());
                if (closedOutQuantityTotalCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Closed Out Quantity Total'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'Closed Out Quantity Total'";
                    return null;
                }

                foreach (TmpImportFfaOptionTrade tmpImportFfaOptionTrade in tmpImportFfaOptionTrades)
                {
                    if (insertedContractCodes.Contains(tmpImportFfaOptionTrade.ContractNo)) continue;
                    insertedContractCodes.Add(tmpImportFfaOptionTrade.ContractNo);

                    int? configResult = SetConfigurationData(configurationContext,
                                                             tmpImportFfaOptionTrade.ContractType.Contains("FFA")
                                                                 ? TradeTypeEnum.FFA
                                                                 : TradeTypeEnum.Option,
                                                             tmpImportFfaOptionTrade.RowNumber, runDate,
                                                             tmpImportFfaOptionTrade.Company,
                                                             tmpImportFfaOptionTrade.Counterparty,
                                                             tmpImportFfaOptionTrade.Trader,
                                                             tmpImportFfaOptionTrade.Keyword,
                                                             new List<string>() { tmpImportFfaOptionTrade.Broker },
                                                             tmpImportFfaOptionTrade.HedgedAgainst,
                                                             tmpImportFfaOptionTrade.ClearingHouse,
                                                             tmpImportFfaOptionTrade.TradeCode, null,
                                                             ref memConfigData, out index, out errorMessage);

                    if (configResult == null)
                        return configResult;

                    isUpdate = dataContext.TradeInfos.Any(a => a.ExternalCode == tmpImportFfaOptionTrade.ContractNo);

                    Trade trade = null;
                    TradeInfo tradeInfo;

                    #region Insert

                    if (!isUpdate)
                    {
                        trade = new Trade()
                        {
                            Id = dataContext.GetNextId(typeof (Trade)),
                            Type =
                                tmpImportFfaOptionTrade.ContractType.Contains("FFA")
                                    ? TradeTypeEnum.FFA
                                    : TradeTypeEnum.Option,
                            State = TradeStateEnum.Confirmed,
                            Status = ActivationStatusEnum.Active,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate
                        };

                        dataContext.Trades.InsertOnSubmit(trade);

                        tradeInfo = new TradeInfo
                        {
                            Id = dataContext.GetNextId(typeof (TradeInfo)),
                            TradeId = trade.Id,
                            Code =
                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                trade.Id,
                            ExternalCode = tmpImportFfaOptionTrade.ContractNo,
                            SignDate = Convert.ToDateTime(
                                DateTime.Parse(tmpImportFfaOptionTrade.TradeDate, tradeDateCultureInfo),
                                SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            CompanyId =
                                memConfigData[typeof (Company)][tmpImportFfaOptionTrade.Company.ToUpper()],
                            CounterpartyId =
                                memConfigData[typeof (Company)][tmpImportFfaOptionTrade.Counterparty.ToUpper()],
                            TraderId =
                                string.IsNullOrEmpty(tmpImportFfaOptionTrade.Trader)
                                    ? memConfigData[typeof (Trader)][defaultTraderName.ToUpper()]
                                    : memConfigData[typeof (Trader)][tmpImportFfaOptionTrade.Trader.ToUpper()],
                            MTMFwdIndexId = index.Id,
                            MTMStressIndexId = index.Id,
                            MarketId = index.MarketId,
                            PeriodFrom =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportFfaOptionTrade.StartDate,
                                        startDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            PeriodTo =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportFfaOptionTrade.EndDate, endDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            Direction = tmpImportFfaOptionTrade.BuySell.ToUpper() == "Buy".ToUpper()
                                ? TradeInfoDirectionEnum.InOrBuy
                                : TradeInfoDirectionEnum.OutOrSell,
                            IsJointVenture = false,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate,
                            Version = 1,
                            DateFrom = runDate.Date,
                            DateTo = null,
                            RouteId = memConfigData[typeof (Route)].ElementAt(0).Value,
                            RegionId = memConfigData[typeof (Region)].ElementAt(0).Value
                        };
                        dataContext.TradeInfos.InsertOnSubmit(tradeInfo);
                    }
                        #endregion
                 
                    #region Update

                    else
                    {
                        //var trades = dataContext.TradeInfos.Where(
                        //        a => a.ExternalCode == tmpImportFfaOptionTrade.ContractNo && a.DateTo == null).ToList();

                        //if (trades.Count > 1)
                        //{
                        //    var trade1 = trades.FirstOrDefault();
                        //    Debug.WriteLine(trade1.ExternalCode);
                        //}
                        TradeInfo existingTradeInfo = null;
                        try
                        {
                            existingTradeInfo =
                            dataContext.TradeInfos.Single(
                                a => a.ExternalCode == tmpImportFfaOptionTrade.ContractNo && a.DateTo == null);
                            
                            trade = dataContext.Trades.Single(a => a.Id == existingTradeInfo.TradeId);
                        }
                        catch(Exception exc)
                        {
                            SiAuto.Main.LogError("Error during updating " + tmpImportFfaOptionTrade.ContractNo );
                            throw exc;
                        }
                        

                        trade.Chd = runDate;
                        trade.Chuser = interfaceUserName;

                        existingTradeInfo.Chd = runDate;
                        existingTradeInfo.Chuser = interfaceUserName;
                        existingTradeInfo.DateTo = runDate.Date;

                        tradeInfo = new TradeInfo
                        {
                            Id = dataContext.GetNextId(typeof (TradeInfo)),
                            TradeId = trade.Id,
                            Code =
                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                trade.Id,
                            ExternalCode = tmpImportFfaOptionTrade.ContractNo,
                            SignDate = Convert.ToDateTime(
                                DateTime.Parse(tmpImportFfaOptionTrade.TradeDate, tradeDateCultureInfo),
                                SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            CompanyId =
                                memConfigData[typeof (Company)][tmpImportFfaOptionTrade.Company.ToUpper()],
                            CounterpartyId =
                                memConfigData[typeof (Company)][tmpImportFfaOptionTrade.Counterparty.ToUpper()],
                            TraderId =
                                string.IsNullOrEmpty(tmpImportFfaOptionTrade.Trader)
                                    ? memConfigData[typeof (Trader)][defaultTraderName.ToUpper()]
                                    : memConfigData[typeof (Trader)][tmpImportFfaOptionTrade.Trader.ToUpper()],
                            MTMFwdIndexId = index.Id,
                            MTMStressIndexId = index.Id,
                            MarketId = index.MarketId,
                            PeriodFrom =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportFfaOptionTrade.StartDate,
                                        startDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            PeriodTo =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportFfaOptionTrade.EndDate, endDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            Direction = tmpImportFfaOptionTrade.BuySell.ToUpper() == "Buy".ToUpper()
                                ? TradeInfoDirectionEnum.InOrBuy
                                : TradeInfoDirectionEnum.OutOrSell,
                            IsJointVenture = false,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate,
                            Version = 1,
                            DateFrom = runDate.Date,
                            DateTo = null,
                            RouteId = memConfigData[typeof (Route)].ElementAt(0).Value,
                            RegionId = memConfigData[typeof (Region)].ElementAt(0).Value
                        };
                        dataContext.TradeInfos.InsertOnSubmit(tradeInfo);
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(tmpImportFfaOptionTrade.Broker))
                    {
                        var commission =
                            Convert.ToDecimal(Decimal.Parse(tmpImportFfaOptionTrade.BrokerRate, brokerRateCultureInfo),
                                              SessionRegistry.ServerSessionRegistry.ServerCultureInfo);

                        var tradeBrokerInfo = new TradeBrokerInfo()
                        {
                            BrokerId = memConfigData[typeof(Company)][tmpImportFfaOptionTrade.Broker.ToUpper()],
                            Commission = commission <= 10 ? commission : 0,
                            Id = dataContext.GetNextId(typeof(TradeBrokerInfo)),
                            TradeInfoId = tradeInfo.Id
                        };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }

                    string bookName = string.IsNullOrEmpty(tmpImportFfaOptionTrade.Keyword)
                                          ? defaultBookName
                                          : tmpImportFfaOptionTrade.Keyword;
                    var tradeInfoBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][bookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);

                    Book book =
                        dataContext.Books.SingleOrDefault(a => a.Id == memConfigData[typeof(Book)][bookName.ToUpper()]);

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    #region Company books

                    string companyBookName = tmpImportFfaOptionTrade.Company + "-" + bookName;
                    var tradeInfoCompanyBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][companyBookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoCompanyBook);

                    #endregion

                    if (trade.Type == TradeTypeEnum.FFA)
                    {
                        var tradeFfaInfo = new TradeFfaInfo()
                                               {
                                                   Id = dataContext.GetNextId(typeof (TradeFfaInfo)),
                                                   AllocationType =
                                                       string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                           ? TradeInfoVesselAllocationTypeEnum.None
                                                           : TradeInfoVesselAllocationTypeEnum.Single,
                                                   AccountId = null,
                                                   BankId = null,
                                                   ClearingFees = 0,
                                                   IndexId =
                                                       index.Id,
                                                   ClearingHouseId =
                                                       string.IsNullOrEmpty(tmpImportFfaOptionTrade.ClearingHouse)
                                                           ? (long?) null
                                                           : memConfigData[typeof (ClearingHouse)][
                                                               tmpImportFfaOptionTrade.ClearingHouse.ToUpper()],
                                                   PeriodType = PeriodTypeEnum.Month,
                                                   QuantityType = TradeInfoQuantityTypeEnum.PerMonth,
                                                   Purpose = TradeFFAInfoPurposeEnum.Hedge,
                                                   QuantityDays =
                                                       Convert.ToDecimal(
                                                           Decimal.Parse(tmpImportFfaOptionTrade.QtyTradedPerMonth,
                                                                         quantityCultureInfo),
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo),
                                                   PeriodDayCountType = TradeInfoPeriodDayCountTypeEnum.Thirty,
                                                   SettlementType = TradeInfoSettlementTypeEnum.AllDays,
                                                   Price =
                                                       Convert.ToDecimal(
                                                           Decimal.Parse(tmpImportFfaOptionTrade.FixedRate,
                                                                         fixedRateCultureInfo),
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo),
                                                   VesselId =
                                                       string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                           ? (long?) null
                                                           : memConfigData[typeof (Vessel)][
                                                               tmpImportFfaOptionTrade.HedgedAgainst.ToUpper()],
                                                   VesselPoolId = null,
                                                   TradeInfoId = tradeInfo.Id,
                                                   Period = tmpImportFfaOptionTrade.Period + "-" +
                                                            tradeInfo.PeriodFrom.Year.ToString().Substring(
                                                                tradeInfo.PeriodFrom.Year.ToString().Length - 2),
                                                   ClosedOutQuantityTotal =
                                                       Convert.ToDecimal(
                                                           Decimal.Parse(tmpImportFfaOptionTrade.ClosedOutQuantityTotal,
                                                                         closedOutQuantityTotalCultureInfo),
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo)
                        };
                        dataContext.TradeFfaInfos.InsertOnSubmit(tradeFfaInfo);
                    }
                    else if (trade.Type == TradeTypeEnum.Option)
                    {
                        var tradeOptionInfo = new TradeOptionInfo()
                                                  {
                                                      Id = dataContext.GetNextId(typeof (TradeOptionInfo)),
                                                      AllocationType =
                                                          string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                              ? TradeInfoVesselAllocationTypeEnum.None
                                                              : TradeInfoVesselAllocationTypeEnum.Single,
                                                      AccountId = null,
                                                      BankId = null,
                                                      ClearingFees = 0,
                                                      IndexId = index.Id,
                                                      ClearingHouseId =
                                                          string.IsNullOrEmpty(tmpImportFfaOptionTrade.ClearingHouse)
                                                              ? (long?) null
                                                              : memConfigData[typeof (ClearingHouse)][
                                                                  tmpImportFfaOptionTrade.ClearingHouse.ToUpper()],
                                                      PeriodType = PeriodTypeEnum.Month,
                                                      Period = tmpImportFfaOptionTrade.Period + "-" +
                                                               tradeInfo.PeriodFrom.Year.ToString().Substring(
                                                                   tradeInfo.PeriodFrom.Year.ToString().Length - 2),
                                                      PeriodDayCountType = TradeInfoPeriodDayCountTypeEnum.Thirty,
                                                      Purpose = TradeFFAInfoPurposeEnum.Hedge,
                                                      Premium =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.PremiumRate,
                                                                            premiumRateRateCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      PremiumDueDate =
                                                          Convert.ToDateTime(
                                                              DateTime.Parse(tmpImportFfaOptionTrade.TradeDate,
                                                                             tradeDateCultureInfo),
                                                              SessionRegistry.ServerSessionRegistry.ServerCultureInfo).
                                                          Date,
                                                      QuantityDays =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.QtyTradedPerMonth,
                                                                            quantityCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      QuantityType = TradeInfoQuantityTypeEnum.PerMonth,
                                                      SettlementType = TradeInfoSettlementTypeEnum.AllDays,
                                                      Strike =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.FixedRate,
                                                                            fixedRateCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      TradeInfoId = tradeInfo.Id,
                                                      Type =
                                                          tmpImportFfaOptionTrade.ContractType.ToUpper().Contains(
                                                              "PUT")
                                                              ? TradeOPTIONInfoTypeEnum.Put
                                                              : TradeOPTIONInfoTypeEnum.Call,
                                                      VesselId =
                                                          string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                              ? (long?) null
                                                              : memConfigData[typeof (Vessel)][
                                                                  tmpImportFfaOptionTrade.HedgedAgainst.ToUpper()],
                                                      VesselPoolId = null
                                                  };
                        dataContext.TradeOptionInfos.InsertOnSubmit(tradeOptionInfo);
                    }

                    configurationContext.SubmitChanges();
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                errorMessage = "There was an exception during import. Excpetion Message: " + exc.Message;
                return null;
            }
            finally
            {
                if (configurationContext != null)
                    configurationContext.Dispose();
            }
        }

        private int? InsertCargoTrades_(DataContext dataContext, List<TmpImportCargoTrade> tmpImportCargoTrades, string interfaceUserName, out string errorMessage)
        {
            DataContext configurationContext = null;

            errorMessage = "";
            string userName = null;
            bool isUpdate;
            Dictionary<Type, Dictionary<string, long>> memConfigData;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                configurationContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "InsertCargoTrades: " };
                }

                DateTime runDate = tmpImportCargoTrades[0].RunDate;
                memConfigData = new Dictionary<Type, Dictionary<string, long>>();

                const string defaultCompany = "AMNBC";
                const string defaultTrader = "MR";
                const string defaultBook = "AMNBulk";

                var insertedTradeCodes = new List<string>();

                CultureInfo dateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportCargoTrades.Select(a => a.Date).ToList());
                CultureInfo laydaysDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportCargoTrades.Select(a => a.LaydaysDate).ToList());

                CultureInfo baseTceRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportCargoTrades.Select(a => a.BaseTceRate).ToList());
                if (baseTceRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'BaseTceRate'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'BaseTceRate'";
                    return null;
                }

                CultureInfo sizeAdjustCultureInfo =
                    GetCultureOfNumericValues(tmpImportCargoTrades.Select(a => a.SizeAdjust).ToList());
                if (sizeAdjustCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'SizeAdjust'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'SizeAdjust'";
                    return null;
                }

                Trade trade;
                TradeInfo tradeInfo;
                foreach (TmpImportCargoTrade tmpImportCargoTrade in tmpImportCargoTrades)
                {
                    if (insertedTradeCodes.Contains(tmpImportCargoTrade.OurRef)) continue;

                    Index index = null;
                    int? configResult = SetConfigurationData(configurationContext, TradeTypeEnum.Cargo,
                                                             tmpImportCargoTrade.RowNumber, runDate, defaultCompany,
                                                             tmpImportCargoTrade.Charterer, tmpImportCargoTrade.FixedBy, defaultBook,
                                                             new List<string>(), null,
                                                             null, tmpImportCargoTrade.TradeRoute, null, ref memConfigData,
                                                             out index, out errorMessage);
                    if (configResult == null)
                        return null;

                    isUpdate = dataContext.TradeInfos.Any(a => a.ExternalCode == tmpImportCargoTrade.OurRef);

                    List<TmpImportCargoTrade> legTrades =
                            tmpImportCargoTrades.Where(a => a.OurRef == tmpImportCargoTrade.OurRef).ToList();

                    #region Insert
                    if (!isUpdate)
                    {
                        trade = new Trade()
                        {
                            Id = dataContext.GetNextId(typeof(Trade)),
                            Type = TradeTypeEnum.Cargo,
                            State = TradeStateEnum.Confirmed,
                            Status = ActivationStatusEnum.Active,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate
                        };

                        dataContext.Trades.InsertOnSubmit(trade);

                        tradeInfo = new TradeInfo()
                        {
                            Id = dataContext.GetNextId(typeof(TradeInfo)),
                            TradeId = trade.Id,
                            Code =
                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                trade.Id,
                            ExternalCode = tmpImportCargoTrade.OurRef,
                            SignDate =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportCargoTrade.Date,
                                                   dateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            CompanyId = memConfigData[typeof(Company)][defaultCompany.ToUpper()],
                            CounterpartyId =
                                memConfigData[typeof(Company)][tmpImportCargoTrade.Charterer.ToUpper()],
                            TraderId = string.IsNullOrEmpty(tmpImportCargoTrade.FixedBy)
                                           ? memConfigData[typeof(Trader)][defaultTrader.ToUpper()]
                                           : memConfigData[typeof(Trader)][tmpImportCargoTrade.FixedBy.ToUpper()],
                            MTMFwdIndexId = index.Id,
                            MTMStressIndexId = index.Id,
                            MarketId = index.MarketId,
                            PeriodFrom =
                                legTrades.Select(
                                    a =>
                                    Convert.ToDateTime(
                                    DateTime.Parse(tmpImportCargoTrade.LaydaysDate,
                                                   laydaysDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo)).Min().Date,
                            PeriodTo =
                                legTrades.Select(
                                    a =>
                                    Convert.ToDateTime(
                                    DateTime.Parse(tmpImportCargoTrade.LaydaysDate,
                                                   laydaysDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).AddDays(
                                                           Double.Parse(
                                                               a.EstDuration,
                                                               SessionRegistry.
                                                                   ServerSessionRegistry.
                                                                   ServerCultureInfo))).Max().Date,
                            Direction = TradeInfoDirectionEnum.InOrBuy,
                            IsJointVenture = false,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate,
                            DateFrom = runDate.Date,
                            DateTo = null,
                            Version = 1,
                            RouteId = memConfigData[typeof(Route)].ElementAt(0).Value,
                            RegionId = memConfigData[typeof(Region)].ElementAt(0).Value
                        };
                        dataContext.TradeInfos.InsertOnSubmit(tradeInfo);
                    }
                    #endregion

                    #region Update

                    else
                    {
                        TradeInfo existingTradeInfo =
                            dataContext.TradeInfos.Single(
                                a => a.ExternalCode == tmpImportCargoTrade.OurRef && a.DateTo == null);
                        trade = dataContext.Trades.Single(a => a.Id == existingTradeInfo.TradeId);

                        trade.Chd = runDate;
                        trade.Chuser = interfaceUserName;

                        existingTradeInfo.Chd = runDate;
                        existingTradeInfo.Chuser = interfaceUserName;
                        existingTradeInfo.DateTo = runDate.Date;

                        tradeInfo = new TradeInfo()
                        {
                            Id = dataContext.GetNextId(typeof(TradeInfo)),
                            TradeId = trade.Id,
                            Code =
                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                trade.Id,
                            ExternalCode = tmpImportCargoTrade.OurRef,
                            SignDate =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportCargoTrade.Date,
                                                   dateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            CompanyId = memConfigData[typeof(Company)][defaultCompany.ToUpper()],
                            CounterpartyId =
                                memConfigData[typeof(Company)][tmpImportCargoTrade.Charterer.ToUpper()],
                            TraderId = string.IsNullOrEmpty(tmpImportCargoTrade.FixedBy)
                                           ? memConfigData[typeof(Trader)][defaultTrader.ToUpper()]
                                           : memConfigData[typeof(Trader)][tmpImportCargoTrade.FixedBy.ToUpper()],
                            MTMFwdIndexId = index.Id,
                            MTMStressIndexId = index.Id,
                            MarketId = index.MarketId,
                            PeriodFrom =
                                legTrades.Select(
                                    a =>
                                    Convert.ToDateTime(
                                    DateTime.Parse(tmpImportCargoTrade.LaydaysDate,
                                                   laydaysDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo)).Min().Date,
                            PeriodTo =
                                legTrades.Select(
                                    a =>
                                    Convert.ToDateTime(
                                    DateTime.Parse(tmpImportCargoTrade.LaydaysDate,
                                                   laydaysDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).AddDays(
                                                           Double.Parse(
                                                               a.EstDuration,
                                                               SessionRegistry.
                                                                   ServerSessionRegistry.
                                                                   ServerCultureInfo))).Max().Date,
                            Direction = TradeInfoDirectionEnum.InOrBuy,
                            IsJointVenture = false,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate,
                            DateFrom = runDate.Date,
                            DateTo = null,
                            Version = 1,
                            RouteId = memConfigData[typeof(Route)].ElementAt(0).Value,
                            RegionId = memConfigData[typeof(Region)].ElementAt(0).Value
                        };
                        dataContext.TradeInfos.InsertOnSubmit(tradeInfo);
                    }

                    #endregion

                    var tradeInfoBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][defaultBook.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);

                    Book book =
                        dataContext.Books.SingleOrDefault(a => a.Id == memConfigData[typeof(Book)][defaultBook.ToUpper()]);

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    #region Company books

                    string companyBookName = defaultCompany + "-" + defaultBook;
                    var tradeInfoCompanyBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][companyBookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoCompanyBook);

                    #endregion

                    var tradeCargoInfo = new TradeCargoInfo()
                    {
                        Id = dataContext.GetNextId(typeof(TradeCargoInfo)),
                        TradeInfoId = tradeInfo.Id,
                        VesselIndex = Convert.ToDecimal(
                                     Decimal.Parse(tmpImportCargoTrade.SizeAdjust,
                                                   sizeAdjustCultureInfo),
                                     SessionRegistry.
                                         ServerSessionRegistry.
                                         ServerCultureInfo)
                    };
                    dataContext.TradeCargoInfos.InsertOnSubmit(tradeCargoInfo);


                    for (int i = 1; i <= legTrades.Count(); i++)
                    {
                        var legCargoTrade = legTrades[i - 1];

                        var leg = new TradeCargoInfoLeg()
                        {
                            Id = dataContext.GetNextId(typeof(TradeCargoInfoLeg)),
                            Identifier = i,
                            IndexId = null,
                            IndexPercentage = null,
                            IsOptional = false,
                            OptionalStatus = null,
                            PeriodFrom =
                                DateTime.Parse(legCargoTrade.LaydaysDate,
                                               SessionRegistry.ServerSessionRegistry.
                                                   ServerCultureInfo),
                            PeriodTo =
                                DateTime.Parse(legCargoTrade.LaydaysDate,
                                               SessionRegistry.ServerSessionRegistry.
                                                   ServerCultureInfo).AddDays(
                                                       Double.Parse(
                                                           legCargoTrade.
                                                               EstDuration,
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo)),
                            Quantity = 50000,
                            QuantityVariation = null,
                            Rate = null,
                            RateType = TradeInfoLegRateTypeEnum.Fixed,
                            Tce = Convert.ToDecimal(
                                                Decimal.Parse(tmpImportCargoTrade.BaseTceRate,
                                                              baseTceRateCultureInfo),
                                                SessionRegistry.
                                                    ServerSessionRegistry.
                                                    ServerCultureInfo),
                            TradeCargoInfoId = tradeCargoInfo.Id
                        };
                        dataContext.TradeCargoInfoLegs.InsertOnSubmit(leg);
                    }

                    insertedTradeCodes.Add(tmpImportCargoTrade.OurRef);
                    configurationContext.SubmitChanges();
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                errorMessage = "There was an exception during import. Excpetion Message: " + exc.Message;
                return null;
            }
            finally
            {
                if (configurationContext != null)
                    configurationContext.Dispose();
            }
        }

        private int? InsertTcTrades_(DataContext dataContext, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades, string interfaceUserName, out string errorMessage)
        {
            DataContext configurationContext = null;

            errorMessage = "";
            string userName = null;
            const string defaultTraderName = "Unknown";
            const string defaultVesselName = "TBN";
            const string defaultBookName = "TBA";

            Dictionary<Type, Dictionary<string, long>> memConfigData;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                configurationContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "InsertTCTrades: " };
                }

                DateTime runDate = tmpImportTcInTrades[0].RunDate;
                memConfigData = new Dictionary<Type, Dictionary<string, long>>();


                CultureInfo cpDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.CpDate).ToList());
                CultureInfo periodFromCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodFrom).ToList());
                CultureInfo periodToCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodTo).ToList());
                CultureInfo periodToMaxCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodToMax).ToList());
                CultureInfo periodToMinCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodToMin).ToList());
                CultureInfo rateCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcInTrades.Select(a => a.Rate).ToList());
                if (rateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Rate'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'Rate'";
                    return null;
                }

                CultureInfo sizeAdjustCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcInTrades.Select(a => a.SizeAdjust).ToList());
                if (sizeAdjustCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'SizeAdjust'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'SizeAdjust'";
                    return null;
                }

                CultureInfo startDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.StartDate).ToList());
                CultureInfo endDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.EndDate).ToList());
                //CultureInfo periodStartCultureInfo =
                //    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.PeriodStart).ToList());
                //CultureInfo periodEndCultureInfo =
                //    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.PeriodEnd).ToList());
                CultureInfo tceCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcOutTrades.Select(a => a.TceEstimate).ToList());
                if (tceCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'TceEstimate'");
                    errorMessage = "There was an error during import. Cannot recognize the culture of numeric values of column 'TceEstimate'";
                    return null;
                }
                CultureInfo vesselSizeCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcOutTrades.Select(a => a.VesselSize).ToList());

                var tcInHeadFixturesNotInserted = new List<string>();
                bool isUpdate;

                #region TC In Trades
                foreach (TmpImportTcInTrade tmpImportTcInTrade in tmpImportTcInTrades)
                {
                    //if (!tmpImportTcOutTrades.Select(a => a.OurRef).Contains(tmpImportTcInTrade.HeadFixtureRef))
                    //{
                    //    SiAuto.Main.LogError("There was no corresponding tc out trade for tc in trade with code :" +
                    //                         tmpImportTcInTrade.HeadFixtureRef + ", RowNumber: " +
                    //                         tmpImportTcInTrade.RowNumber);
                    //    continue;
                    //}

                    isUpdate = (from objInfo in dataContext.TradeInfos
                                where objInfo.ExternalCode == tmpImportTcInTrade.HeadFixtureRef
                                      && objInfo.PeriodFrom.Date == Convert.ToDateTime(
                                          DateTime.Parse(tmpImportTcInTrade.PeriodFrom,
                                                         periodFromCultureInfo),
                                          SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date
                                      && objInfo.DateTo == null
                                select objInfo).Any();

                    Index index = null;

                    string counterPartyStr = string.IsNullOrEmpty(tmpImportTcInTrade.DisponentOwner)
                                            ? (string.IsNullOrEmpty(tmpImportTcInTrade.Company1)
                                                   ? "TBA"
                                                   : tmpImportTcInTrade.Company1)
                                            : tmpImportTcInTrade.DisponentOwner;

                    int? configResult = SetConfigurationData(configurationContext, TradeTypeEnum.TC,
                                                             tmpImportTcInTrade.RowNumber, runDate,
                                                             tmpImportTcInTrade.Company,
                                                             counterPartyStr, tmpImportTcInTrade.FixedBy,
                                                             tmpImportTcInTrade.AnalysisCode,
                                                             new List<string>()
                                                                 {
                                                                     tmpImportTcInTrade.Broker1,
                                                                     tmpImportTcInTrade.Broker2,
                                                                     tmpImportTcInTrade.Broker3
                                                                 },
                                                             tmpImportTcInTrade.Vessel,
                                                             null, tmpImportTcInTrade.TradeRoute,
                                                             tmpImportTcInTrade.YearBuilt, ref memConfigData,
                                                             out index, out errorMessage);
                    if (configResult == null)
                        return null;
                    
                    #region Legs calculation

                    var legs = new List<TradeTcInfoLeg>();

                    if ((tmpImportTcInTrade.Status.ToUpper() == "Redelivered".ToUpper() || tmpImportTcInTrade.Status.ToUpper() == "Finalised".ToUpper()) ||
                        (tmpImportTcInTrade.PeriodToMax == tmpImportTcInTrade.PeriodToMin &&
                         !(tmpImportTcInTrade.Status.ToUpper() == "Redelivered".ToUpper() || tmpImportTcInTrade.Status.ToUpper() == "Finalised".ToUpper())) ||
                        (!(tmpImportTcInTrade.Status.ToUpper() == "Redelivered".ToUpper() || tmpImportTcInTrade.Status.ToUpper() == "Finalised".ToUpper())
                        && tmpImportTcInTrade.PeriodToMax != tmpImportTcInTrade.PeriodToMin 
                        && tmpImportTcInTrade.Status.ToUpper() == "Last Voyage Running".ToUpper()))
                    {
                        var leg = new TradeTcInfoLeg()
                        {
                            Id = dataContext.GetNextId(typeof(TradeTcInfoLeg)),
                            Identifier = 1,
                            IndexId =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? index.Id
                                    : (long?)null,
                            IndexPercentage =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? (Convert.ToDecimal(
                                        Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                      sizeAdjustCultureInfo),
                                        SessionRegistry.
                                            ServerSessionRegistry.
                                            ServerCultureInfo))
                                    : (decimal?)null,
                            IsOptional = false,
                            OptionalStatus = null,
                            PeriodFrom =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportTcInTrade.PeriodFrom,
                                                   periodFromCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            PeriodTo =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportTcInTrade.PeriodTo,
                                                   periodToCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            Rate =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? null
                                    : string.IsNullOrEmpty(tmpImportTcInTrade.Rate)
                                          ? (decimal?)null
                                          : Convert.ToDecimal(
                                              Decimal.Parse(tmpImportTcInTrade.Rate,
                                                            rateCultureInfo),
                                              SessionRegistry.
                                                  ServerSessionRegistry.
                                                  ServerCultureInfo),
                            RateType =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? TradeInfoLegRateTypeEnum.IndexLinked
                                    : TradeInfoLegRateTypeEnum.Fixed,
                            RedeliveryDays = 20
                        };
                        legs.Add(leg);
                    }
                    else
                    {
                        //Period To Min
                        DateTime periodToMin = Convert.ToDateTime(
                            DateTime.Parse(tmpImportTcInTrade.PeriodToMin,
                                           periodToMinCultureInfo),
                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date;
                        //Period To
                        DateTime periodTo = Convert.ToDateTime(
                            DateTime.Parse(tmpImportTcInTrade.PeriodTo,
                                           periodToCultureInfo),
                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo).
                            Date;

                        //Period ToMax
                        DateTime periodToMax = Convert.ToDateTime(
                            DateTime.Parse(tmpImportTcInTrade.PeriodToMax,
                                           periodToMaxCultureInfo),
                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date;

                        var leg1 = new TradeTcInfoLeg()
                        {
                            Id = dataContext.GetNextId(typeof(TradeTcInfoLeg)),
                            Identifier = 1,
                            IndexId =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? index.Id
                                    : (long?)null,
                            IndexPercentage =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? (Convert.ToDecimal(
                                        Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                      sizeAdjustCultureInfo),
                                        SessionRegistry.
                                            ServerSessionRegistry.
                                            ServerCultureInfo))
                                    : (decimal?)null,
                            IsOptional = false,
                            OptionalStatus = null,
                            PeriodFrom =
                                Convert.ToDateTime(
                                    DateTime.Parse(tmpImportTcInTrade.PeriodFrom,
                                                   periodFromCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                            PeriodTo = periodToMin,
                            Rate = tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                       ? (decimal?)null
                                       : Convert.ToDecimal(
                                           Decimal.Parse(tmpImportTcInTrade.Rate,
                                                         rateCultureInfo),
                                           SessionRegistry.
                                               ServerSessionRegistry.
                                               ServerCultureInfo),
                            RateType =
                                tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                    ? TradeInfoLegRateTypeEnum.IndexLinked
                                    : TradeInfoLegRateTypeEnum.Fixed,
                            RedeliveryDays = 20
                        };
                        legs.Add(leg1);

                        var leg2 = new TradeTcInfoLeg()
                                       {
                                           Id = dataContext.GetNextId(typeof (TradeTcInfoLeg)),
                                           Identifier = 2,
                                           IndexId =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? index.Id
                                                   : (long?) null,
                                           IndexPercentage =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? (Convert.ToDecimal(
                                                       Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                                     sizeAdjustCultureInfo),
                                                       SessionRegistry.
                                                           ServerSessionRegistry.
                                                           ServerCultureInfo))
                                                   : (decimal?) null,
                                           IsOptional = true,
                                           OptionalStatus =
                                               runDate < periodToMin
                                                   ? (periodTo == periodToMin
                                                          ? TradeInfoLegOptionalStatusEnum
                                                                .Pending
                                                          : TradeInfoLegOptionalStatusEnum
                                                                .Declared)
                                                   : TradeInfoLegOptionalStatusEnum.Declared,
                                           PeriodFrom = periodToMin,
                                           PeriodTo = periodToMax,
                                           Rate = tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                      ? (decimal?) null
                                                      : string.IsNullOrEmpty(tmpImportTcInTrade.Rate)
                                                            ? (decimal?) null
                                                            : Convert.ToDecimal(
                                                                Decimal.Parse(tmpImportTcInTrade.Rate,
                                                                              rateCultureInfo),
                                                                SessionRegistry.
                                                                    ServerSessionRegistry.
                                                                    ServerCultureInfo),
                                           RateType =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? TradeInfoLegRateTypeEnum.IndexLinked
                                                   : TradeInfoLegRateTypeEnum.Fixed,
                                           RedeliveryDays = 20
                                       };
                        
                        legs.Add(leg2);
                    }

                    #endregion

                    Trade trade;
                    if (!isUpdate)
                    {
                        trade = new Trade()
                        {
                            Id = dataContext.GetNextId(typeof(Trade)),
                            Type = TradeTypeEnum.TC,
                            State = TradeStateEnum.Confirmed,
                            Status = ActivationStatusEnum.Active,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate
                        };
                        dataContext.Trades.InsertOnSubmit(trade);
                    }
                    else
                    {
                        TradeInfo existingTradeInfo;
                        List<TradeInfo> existingTradeInfos = (from objInfo in dataContext.TradeInfos
                                                              where
                                                                  objInfo.ExternalCode ==
                                                                  tmpImportTcInTrade.HeadFixtureRef
                                                                  && objInfo.PeriodFrom.Date == Convert.ToDateTime(
                                                                      DateTime.Parse(tmpImportTcInTrade.PeriodFrom,
                                                                                     periodFromCultureInfo),
                                                                      SessionRegistry.ServerSessionRegistry.
                                                                          ServerCultureInfo)
                                                                                                    .Date
                                                                  && objInfo.DateTo == null
                                                              select objInfo).ToList();

                        if (existingTradeInfos.Count == 1)
                            existingTradeInfo = existingTradeInfos.Single();
                        else
                        {
                            tcInHeadFixturesNotInserted.Add(tmpImportTcInTrade.HeadFixtureRef);
                            SiAuto.Main.LogError("Combination of code '" + tmpImportTcInTrade.HeadFixtureRef +
                                                 "' and Period From '" + tmpImportTcInTrade.PeriodFrom +
                                                 "' exists in more than one trades." +
                                                 ", RowNumber: " +
                                                 tmpImportTcInTrade.RowNumber);

                            errorMessage = "There was an error during import. Combination of code '" + tmpImportTcInTrade.HeadFixtureRef +
                                           "' and Period From '" + tmpImportTcInTrade.PeriodFrom +
                                           "' exists in more than one trades." +
                                           ", RowNumber: " +
                                           tmpImportTcInTrade.RowNumber;
                            continue;
                        }

                        trade = dataContext.Trades.Single(a => a.Id == existingTradeInfo.TradeId);

                        trade.Chd = runDate;
                        trade.Chuser = interfaceUserName;

                        existingTradeInfo.Chd = runDate;
                        existingTradeInfo.Chuser = interfaceUserName;
                        existingTradeInfo.DateTo = runDate.Date;
                    }

                    var tradeInfo = new TradeInfo()
                                              {
                                                  Id = dataContext.GetNextId(typeof(TradeInfo)),
                                                  TradeId = trade.Id,
                                                  Code =
                                                      trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                                      trade.Id,
                                                  ExternalCode = tmpImportTcInTrade.HeadFixtureRef,
                                                  SignDate =
                                                      Convert.ToDateTime(
                                                          DateTime.Parse(tmpImportTcInTrade.CpDate,
                                                                         cpDateCultureInfo),
                                                          SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                                  CompanyId = memConfigData[typeof(Company)][tmpImportTcInTrade.Company.ToUpper()],
                                                  CounterpartyId = memConfigData[typeof(Company)][counterPartyStr.ToUpper()],
                                                  TraderId = string.IsNullOrEmpty(tmpImportTcInTrade.FixedBy)
                                                                 ? memConfigData[typeof(Trader)][defaultTraderName.ToUpper()]
                                                                 : memConfigData[typeof(Trader)][tmpImportTcInTrade.FixedBy.ToUpper()],
                                                  MTMFwdIndexId = index.Id,
                                                  MTMStressIndexId = index.Id,
                                                  MarketId = index.MarketId,
                                                  PeriodFrom =
                                                      legs.Select(a => a.PeriodFrom).Min().Date,
                                                  PeriodTo =
                                                      legs.Select(a => a.PeriodTo).Max().Date,
                                                  Direction = TradeInfoDirectionEnum.InOrBuy,
                                                  IsJointVenture = false,
                                                  DateFrom = runDate.Date,
                                                  DateTo = null,
                                                  Version = 1,
                                                  RouteId = memConfigData[typeof(Route)].ElementAt(0).Value,
                                                  RegionId = memConfigData[typeof(Region)].ElementAt(0).Value,
                                                  Cruser = interfaceUserName,
                                                  Chuser = interfaceUserName,
                                                  Crd = runDate,
                                                  Chd = runDate,
                                              };
                    dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                    var tradeTcInfo = new TradeTcInfo()
                    {
                        Address = null,
                        BallastBonus = null,
                        Delivery = null,
                        Id = dataContext.GetNextId(typeof(TradeTcInfo)),
                        IsBareboat = null,
                        Redelivery = null,
                        TradeInfoId = tradeInfo.Id,
                        VesselId = string.IsNullOrEmpty(tmpImportTcInTrade.Vessel)
                                       ? memConfigData[typeof(Vessel)][defaultVesselName.ToUpper()]
                                       : memConfigData[typeof(Vessel)][tmpImportTcInTrade.Vessel.ToUpper()],
                        VesselIndex =
                            Convert.ToDecimal(
                                        Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                      sizeAdjustCultureInfo),
                                        SessionRegistry.
                                            ServerSessionRegistry.
                                            ServerCultureInfo)
                    };
                    dataContext.TradeTcInfos.InsertOnSubmit(tradeTcInfo);
                    foreach (TradeTcInfoLeg tradeTcInfoLeg in legs)
                    {
                        tradeTcInfoLeg.TradeTcInfoId = tradeTcInfo.Id;
                        dataContext.TradeTcInfoLegs.InsertOnSubmit(tradeTcInfoLeg);
                    }

                    #region Brokers

                    decimal defaultCommission = 1.25m;

                    if (!string.IsNullOrEmpty(tmpImportTcInTrade.Broker1))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                        {
                            Id = dataContext.GetNextId(typeof(TradeBrokerInfo)),
                            BrokerId =
                                memConfigData[typeof(Company)][tmpImportTcInTrade.Broker1.ToUpper()],
                            Commission = defaultCommission,
                            TradeInfoId = tradeInfo.Id
                        };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }
                    if (!string.IsNullOrEmpty(tmpImportTcInTrade.Broker2))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                        {
                            Id = dataContext.GetNextId(typeof(TradeBrokerInfo)),
                            BrokerId =
                                memConfigData[typeof(Company)][tmpImportTcInTrade.Broker2.ToUpper()],
                            Commission = defaultCommission,
                            TradeInfoId = tradeInfo.Id
                        };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }
                    if (!string.IsNullOrEmpty(tmpImportTcInTrade.Broker3))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                        {
                            Id = dataContext.GetNextId(typeof(TradeBrokerInfo)),
                            BrokerId =
                                memConfigData[typeof(Company)][tmpImportTcInTrade.Broker3.ToUpper()],
                            Commission = defaultCommission,
                            TradeInfoId = tradeInfo.Id
                        };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }

                    #endregion

                    string bookName = string.IsNullOrEmpty(tmpImportTcInTrade.AnalysisCode)
                                          ? defaultBookName
                                          : tmpImportTcInTrade.AnalysisCode;
                    var tradeInfoBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][bookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);
                    Book book =
                        dataContext.Books.SingleOrDefault(a => a.Id == memConfigData[typeof(Book)][bookName.ToUpper()]);

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }


                    #region Company books

                    string companyBookName = tmpImportTcInTrade.Company + "-" + bookName;
                    var tradeInfoCompanyBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][companyBookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoCompanyBook);

                    #endregion

                    //insertedTcInTradeCodes.Add(tmpImportTcInTrade.HeadFixtureRef);
                    configurationContext.SubmitChanges();
                }
                #endregion

                #region TC Out Trades

                var insertedTcOutTradeCodes = new List<string>();
                foreach (TmpImportTcOutTrade tmpImportTcOutTrade in tmpImportTcOutTrades)
                {
                    if (!tmpImportTcInTrades.Select(a => a.HeadFixtureRef).Contains(tmpImportTcOutTrade.OurRef))
                    {
                        SiAuto.Main.LogError("There was no corrsponding TC IN trade (TC In External Code:" + tmpImportTcOutTrade.OurRef + " ) for tc out trade with code :" +
                                             tmpImportTcOutTrade.Voy + ", RowNumber: " +
                                             tmpImportTcOutTrade.RowNumber);
                        errorMessage = "There was an error during import. There was no corrsponding TC IN trade (TC In External Code:" +
                                       tmpImportTcOutTrade.OurRef + " ) for tc out trade with code :" +
                                       tmpImportTcOutTrade.Voy + ", RowNumber: " +
                                       tmpImportTcOutTrade.RowNumber;
                        continue;
                    }
                    if (tcInHeadFixturesNotInserted.Contains(tmpImportTcOutTrade.OurRef))
                    {
                        SiAuto.Main.LogError("TC OUT Trade with code " + tmpImportTcOutTrade.Voy +
                                             " was not inserted because the corresponding TC IN trade with code "
                                             + tmpImportTcOutTrade.OurRef + " was not inserted either. RowNumber: " +
                                             tmpImportTcOutTrade.RowNumber);
                        errorMessage = "There was an error during import. TC OUT Trade with code " + tmpImportTcOutTrade.Voy +
                                       " was not inserted because the corresponding TC IN trade with code "
                                       + tmpImportTcOutTrade.OurRef + " was not inserted either. RowNumber: " +
                                       tmpImportTcOutTrade.RowNumber;
                        continue;
                    }

                    TmpImportTcInTrade importTcInTrade =
                        tmpImportTcInTrades.First(a => a.HeadFixtureRef == tmpImportTcOutTrade.OurRef);

                    
                    Index index = null;
                    int? configResult = SetConfigurationData(configurationContext, TradeTypeEnum.Cargo,
                                                             tmpImportTcOutTrade.RowNumber, runDate,
                                                             importTcInTrade.Company,
                                                             tmpImportTcOutTrade.Charterer, tmpImportTcOutTrade.ChartId,
                                                             importTcInTrade.AnalysisCode,
                                                             new List<string>(),
                                                             tmpImportTcOutTrade.Vessel, null,
                                                             importTcInTrade.TradeRoute, null, ref memConfigData,
                                                             out index, out errorMessage);
                    if (configResult == null)
                        return null;

                    isUpdate = dataContext.TradeInfos.Any(a => a.ExternalCode == tmpImportTcOutTrade.Voy);

                    #region Legs Calculation

                    decimal rate;
                    //Rate Calculation
                    rate = Convert.ToDecimal(Decimal.Parse(tmpImportTcOutTrade.VesselSize, vesselSizeCultureInfo),
                                             SessionRegistry.ServerSessionRegistry.ServerCultureInfo) > 59999
                               ? Convert.ToDecimal(Decimal.Parse(tmpImportTcOutTrade.TceEstimate,
                                                                 tceCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo) / (1 - 0.0375m)
                               : Convert.ToDecimal(Decimal.Parse(tmpImportTcOutTrade.TceEstimate,
                                                                 tceCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo) / (1 - 0.05m);
                    var leg = new TradeTcInfoLeg()
                    {
                        Id = dataContext.GetNextId(typeof(TradeTcInfoLeg)),
                        Identifier = 1,
                        IndexId = null,
                        IndexPercentage = null,
                        IsOptional = false,
                        OptionalStatus = null,
                        PeriodFrom =
                            Convert.ToDateTime(
                                DateTime.Parse(tmpImportTcOutTrade.StartDate,
                                               startDateCultureInfo),
                                SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                        PeriodTo =
                            Convert.ToDateTime(
                                DateTime.Parse(tmpImportTcOutTrade.EndDate,
                                               endDateCultureInfo),
                                SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                        Rate = rate,
                        RateType = TradeInfoLegRateTypeEnum.Fixed,
                        RedeliveryDays = 0 //TODO
                    };

                    #endregion

                    Trade trade;
                    if(!isUpdate)
                    {
                        trade = new Trade()
                        {
                            Id = dataContext.GetNextId(typeof(Trade)),
                            Type = TradeTypeEnum.TC,
                            State = TradeStateEnum.Confirmed,
                            Status = ActivationStatusEnum.Active,
                            Cruser = interfaceUserName,
                            Chuser = interfaceUserName,
                            Crd = runDate,
                            Chd = runDate
                        };
                        dataContext.Trades.InsertOnSubmit(trade);
                    }
                    else
                    {
                        TradeInfo existingTradeInfo = dataContext.TradeInfos.Single(a => a.ExternalCode == tmpImportTcOutTrade.Voy && a.DateTo == null);
                        trade = dataContext.Trades.Single(a => a.Id == existingTradeInfo.TradeId);

                        trade.Chd = runDate;
                        trade.Chuser = interfaceUserName;

                        existingTradeInfo.Chd = runDate;
                        existingTradeInfo.Chuser = interfaceUserName;
                        existingTradeInfo.DateTo = runDate.Date;
                    }

                    var tradeInfo = new TradeInfo()
                    {
                        Id = dataContext.GetNextId(typeof(TradeInfo)),
                        TradeId = trade.Id,
                        Code =
                            trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                            trade.Id,
                        ExternalCode = tmpImportTcOutTrade.Voy,
                        SignDate =
                            Convert.ToDateTime(
                               DateTime.Parse(tmpImportTcOutTrade.StartDate,
                                              startDateCultureInfo),
                               SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date > runDate
                                ? runDate
                                : Convert.ToDateTime(
                               DateTime.Parse(tmpImportTcOutTrade.StartDate,
                                              startDateCultureInfo),
                               SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                        CompanyId = memConfigData[typeof(Company)][importTcInTrade.Company.ToUpper()],
                        CounterpartyId =
                            memConfigData[typeof(Company)][tmpImportTcOutTrade.Charterer.ToUpper()],
                        TraderId = string.IsNullOrEmpty(tmpImportTcOutTrade.ChartId)
                                       ? memConfigData[typeof(Trader)][defaultTraderName.ToUpper()]
                                       : memConfigData[typeof(Trader)][
                                           tmpImportTcOutTrade.ChartId.ToUpper()],
                        MTMFwdIndexId = index.Id,
                        MTMStressIndexId = index.Id,
                        MarketId = index.MarketId,
                        PeriodFrom =
                            leg.PeriodFrom,
                        PeriodTo =
                            leg.PeriodTo,
                        Direction = TradeInfoDirectionEnum.OutOrSell,
                        IsJointVenture = false,
                        DateFrom = runDate.Date,
                        DateTo = null,
                        Version = 1,
                        RouteId = memConfigData[typeof(Route)].ElementAt(0).Value,
                        RegionId = memConfigData[typeof(Region)].ElementAt(0).Value,
                        Cruser = interfaceUserName,
                        Chuser = interfaceUserName,
                        Crd = runDate,
                        Chd = runDate,
                    };
                    dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                    var tradeTcInfo = new TradeTcInfo()
                    {
                        Address = null,
                        BallastBonus = null,
                        Delivery = null,
                        Id = dataContext.GetNextId(typeof(TradeTcInfo)),
                        IsBareboat = null,
                        Redelivery = null,
                        TradeInfoId = tradeInfo.Id,
                        VesselId = string.IsNullOrEmpty(tmpImportTcOutTrade.Vessel)
                                       ? memConfigData[typeof(Vessel)][defaultVesselName.ToUpper()]
                                       : memConfigData[typeof(Vessel)][tmpImportTcOutTrade.Vessel.ToUpper()],
                        VesselIndex =
                            Convert.ToDecimal(Decimal.Parse(importTcInTrade.SizeAdjust,
                                                            sizeAdjustCultureInfo),
                                              SessionRegistry.ServerSessionRegistry.
                                                  ServerCultureInfo)
                    };
                    dataContext.TradeTcInfos.InsertOnSubmit(tradeTcInfo);

                    leg.TradeTcInfoId = tradeTcInfo.Id;
                    dataContext.TradeTcInfoLegs.InsertOnSubmit(leg);

                    string bookName = string.IsNullOrEmpty(importTcInTrade.AnalysisCode)
                                          ? defaultBookName
                                          : importTcInTrade.AnalysisCode;
                    var tradeInfoBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][bookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);
                    Book book = dataContext.Books.SingleOrDefault(a => a.Id == memConfigData[typeof(Book)][bookName.ToUpper()]);

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    #region Company books

                    string companyBookName = importTcInTrade.Company + "-" + bookName;
                    var tradeInfoCompanyBook = new TradeInfoBook()
                    {
                        BookId = memConfigData[typeof(Book)][companyBookName.ToUpper()],
                        Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                        TradeInfoId = tradeInfo.Id
                    };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoCompanyBook);

                    #endregion

                    insertedTcOutTradeCodes.Add(tmpImportTcOutTrade.OurRef);

                    configurationContext.SubmitChanges();
                }

                #endregion

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                errorMessage = "There was an exception during import1711. Excpetion Message: " + exc.Message;
                return null;
            }
            finally
            {
                if (configurationContext != null)
                    configurationContext.Dispose();
            }
        }

        private int? DeleteTempSoftMarData(DataContext dataContext, TradeTypeEnum tradeTypeEnum)
        {
            try
            {
                if (tradeTypeEnum == TradeTypeEnum.FFA || tradeTypeEnum == TradeTypeEnum.Option)
                {
                    //List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades = dataContext.TmpImportFfaOptionTrades.ToList();
                    //dataContext.TmpImportFfaOptionTrades.DeleteAllOnSubmit(tmpImportFfaOptionTrades);
                    dataContext.ExecuteCommand("DELETE FROM TMP_IMPORT_FFA_OPTION_TRADES");
                }
                else if (tradeTypeEnum == TradeTypeEnum.Cargo)
                {
                    //List<TmpImportCargoTrade> tmpImportCargoTrades = dataContext.TmpImportCargoTrades.ToList();
                    //dataContext.TmpImportCargoTrades.DeleteAllOnSubmit(tmpImportCargoTrades);
                    dataContext.ExecuteCommand("DELETE FROM TMP_IMPORT_CARGO_TRADES");
                }
                else if (tradeTypeEnum == TradeTypeEnum.TC)
                {
                    //List<TmpImportTcInTrade> tmpImportTcInTrades = dataContext.TmpImportTcInTrades.ToList();
                    //dataContext.TmpImportTcInTrades.DeleteAllOnSubmit(tmpImportTcInTrades);

                    //List<TmpImportTcOutTrade> tmpImportTcOutTrades = dataContext.TmpImportTcOutTrades.ToList();
                    //dataContext.TmpImportTcOutTrades.DeleteAllOnSubmit(tmpImportTcOutTrades);

                    dataContext.ExecuteCommand("DELETE FROM TMP_IMPORT_TC_IN_TRADES");
                    dataContext.ExecuteCommand("DELETE FROM TMP_IMPORT_TC_OUT_TRADES");
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(GenericContext<ProgramInfo>.Current.Value.UserName, exc);
                return null;
            }
        }

        #endregion

        #region Import without Versioning

        //public int? ImportTrades(List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, List<TmpImportCargoTrade> tmpImportCargoTrades, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades)
        //{
        //    DataContext dataContext = null;
        //    string userName = null;
        //    try
        //    {
        //        userName = GenericContext<ProgramInfo>.Current.Value.UserName;
        //        dataContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

        //        DateTime runDate = DateTime.Now;

        //        if (SiAuto.Si.Level == Level.Debug)
        //        {
        //            if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
        //            dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "ImportTrades: " };
        //        }

        //        AppParameter interfaceUserNameAppParam =
        //            dataContext.AppParameters.Where(a => a.Code == "INTERFACE_USER_NAME").SingleOrDefault();

        //        if (interfaceUserNameAppParam == null)
        //        {
        //            SiAuto.Main.LogError("Application parameter 'INTERFACE_USER_NAME' was not found.");
        //            return null;
        //        }

        //        string interfaceUserName = interfaceUserNameAppParam.Value;

        //        int? importRes = null;
        //        if (tmpImportFfaOptionTrades != null)
        //        {
        //            if (DeletePreviousSoftMarData(dataContext, interfaceUserName, TradeTypeEnum.FFA) == null)
        //                return 1;
        //            foreach (TmpImportFfaOptionTrade tmpImportFfaOptionTrade in tmpImportFfaOptionTrades)
        //            {
        //                tmpImportFfaOptionTrade.Id = dataContext.GetNextId(typeof(TmpImportFfaOptionTrade));
        //                tmpImportFfaOptionTrade.RunDate = runDate;
        //                dataContext.TmpImportFfaOptionTrades.InsertOnSubmit(tmpImportFfaOptionTrade);
        //            }

        //            importRes = InsertFfaOptionTrades(dataContext, tmpImportFfaOptionTrades, interfaceUserName);
        //            if (importRes == null)
        //                return 1;
        //        }
        //        else if (tmpImportCargoTrades != null)
        //        {
        //            if (DeletePreviousSoftMarData(dataContext, interfaceUserName, TradeTypeEnum.Cargo) == null)
        //                return 1;
        //            foreach (TmpImportCargoTrade tmpImportCargoTrade in tmpImportCargoTrades)
        //            {
        //                tmpImportCargoTrade.Id = dataContext.GetNextId(typeof(TmpImportCargoTrade));
        //                tmpImportCargoTrade.RunDate = runDate;
        //                dataContext.TmpImportCargoTrades.InsertOnSubmit(tmpImportCargoTrade);
        //            }

        //            importRes = InsertCargoTrades(dataContext, tmpImportCargoTrades, interfaceUserName);
        //            if (importRes == null)
        //                return 1;
        //        }
        //        else if (tmpImportTcInTrades != null && tmpImportTcOutTrades != null)
        //        {
        //            if (DeletePreviousSoftMarData(dataContext, interfaceUserName, TradeTypeEnum.TC) == null)
        //                return 1;
        //            foreach (TmpImportTcInTrade tmpImportTcInTrade in tmpImportTcInTrades)
        //            {
        //                tmpImportTcInTrade.Id = dataContext.GetNextId(typeof(TmpImportTcInTrade));
        //                tmpImportTcInTrade.RunDate = runDate;
        //                dataContext.TmpImportTcInTrades.InsertOnSubmit(tmpImportTcInTrade);
        //            }
        //            foreach (TmpImportTcOutTrade tmpImportTcOutTrade in tmpImportTcOutTrades)
        //            {
        //                tmpImportTcOutTrade.Id = dataContext.GetNextId(typeof(TmpImportTcOutTrade));
        //                tmpImportTcOutTrade.RunDate = runDate;
        //                dataContext.TmpImportTcOutTrades.InsertOnSubmit(tmpImportTcOutTrade);
        //            }

        //            importRes = InsertTcTrades(dataContext, tmpImportTcInTrades, tmpImportTcOutTrades, interfaceUserName);
        //            if (importRes == null)
        //                return 1;
        //        }

        //        dataContext.SubmitChanges();

        //        return 0;
        //    }
        //    catch (Exception exc)
        //    {
        //        HandleException(userName, exc);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (dataContext != null)
        //            dataContext.Dispose();
        //    }
        //}

        private int? InsertFfaOptionTrades(DataContext dataContext, List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, string interfaceUserName, out string errorMessage)
        {
            DataContext configurationContext = null;

            errorMessage = "";
            string userName = null;
            const string defaultTraderName = "Unknown";
            const string defaultVesselName = "TBN";
            const string defaultBookName = "TBA";

            Dictionary<Type, Dictionary<string, long>> memConfigData;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                configurationContext = new DataContext(DBConnectionString) {ObjectTrackingEnabled = true};

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName))
                                                   {TitleLimit = 0, TitlePrefix = "InsertFFAOptionTrades: "};
                }

                DateTime runDate = tmpImportFfaOptionTrades[0].RunDate;
                memConfigData = new Dictionary<Type, Dictionary<string, long>>();

                Index index = null;
                var insertedContractCodes = new List<string>();

                CultureInfo startDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportFfaOptionTrades.Select(a => a.StartDate).ToList());
                CultureInfo endDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportFfaOptionTrades.Select(a => a.EndDate).ToList());
                CultureInfo tradeDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportFfaOptionTrades.Select(a => a.TradeDate).ToList());
                
                CultureInfo brokerRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.BrokerRate).ToList());
                if (brokerRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Broker Rate'");
                    return null;
                }
                
                CultureInfo fixedRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.FixedRate).ToList());
                if (fixedRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Fixed Rate'");
                    return null;
                }

                CultureInfo premiumRateRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.PremiumRate).ToList());
                if (premiumRateRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Premium Rate'");
                    return null;
                }

                CultureInfo quantityCultureInfo =
                    GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.QtyTradedPerMonth).ToList());
                if (quantityCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'QtyTradedPerMonth'");
                    return null;
                }

                CultureInfo closedOutQuantityTotalCultureInfo =
                  GetCultureOfNumericValues(tmpImportFfaOptionTrades.Select(a => a.ClosedOutQuantityTotal).ToList());
                if (closedOutQuantityTotalCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Closed Out Quantity Total'");
                    return null;
                }

                foreach (TmpImportFfaOptionTrade tmpImportFfaOptionTrade in tmpImportFfaOptionTrades)
                {
                    if (insertedContractCodes.Contains(tmpImportFfaOptionTrade.ContractNo)) continue;
                    insertedContractCodes.Add(tmpImportFfaOptionTrade.ContractNo);

                    int? configResult = SetConfigurationData(configurationContext,
                                                             tmpImportFfaOptionTrade.ContractType.Contains("FFA")
                                                                 ? TradeTypeEnum.FFA
                                                                 : TradeTypeEnum.Option,
                                                             tmpImportFfaOptionTrade.RowNumber, runDate,
                                                             tmpImportFfaOptionTrade.Company,
                                                             tmpImportFfaOptionTrade.Counterparty,
                                                             tmpImportFfaOptionTrade.Trader,
                                                             tmpImportFfaOptionTrade.Keyword,
                                                             new List<string>() {tmpImportFfaOptionTrade.Broker},
                                                             tmpImportFfaOptionTrade.HedgedAgainst,
                                                             tmpImportFfaOptionTrade.ClearingHouse,
                                                             tmpImportFfaOptionTrade.TradeCode, null,
                                                             ref memConfigData, out index, out errorMessage);

                    if (configResult == null)
                        return configResult;

                    #region Insert

                    var trade = new Trade()
                                    {
                                        Id = dataContext.GetNextId(typeof (Trade)),
                                        Type =
                                            tmpImportFfaOptionTrade.ContractType.Contains("FFA")
                                                ? TradeTypeEnum.FFA
                                                : TradeTypeEnum.Option,
                                        State = TradeStateEnum.Confirmed,
                                        Status = ActivationStatusEnum.Active,
                                        Cruser = interfaceUserName,
                                        Chuser = interfaceUserName,
                                        Crd = runDate,
                                        Chd = runDate
                                    };

                    dataContext.Trades.InsertOnSubmit(trade);

                    var tradeInfo = new TradeInfo
                                        {
                                            Id = dataContext.GetNextId(typeof (TradeInfo)),
                                            TradeId = trade.Id,
                                            Code =
                                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                                trade.Id,
                                            ExternalCode = tmpImportFfaOptionTrade.ContractNo,
                                            SignDate = Convert.ToDateTime(
                                                DateTime.Parse(tmpImportFfaOptionTrade.TradeDate, tradeDateCultureInfo),
                                                SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                            CompanyId =
                                                memConfigData[typeof(Company)][tmpImportFfaOptionTrade.Company.ToUpper()],
                                            CounterpartyId =
                                                memConfigData[typeof(Company)][tmpImportFfaOptionTrade.Counterparty.ToUpper()],
                                            TraderId =
                                                string.IsNullOrEmpty(tmpImportFfaOptionTrade.Trader)
                                                    ? memConfigData[typeof(Trader)][defaultTraderName.ToUpper()]
                                                    : memConfigData[typeof(Trader)][tmpImportFfaOptionTrade.Trader.ToUpper()],
                                            MTMFwdIndexId = index.Id,
                                            MTMStressIndexId = index.Id,
                                            MarketId = index.MarketId,
                                            PeriodFrom =
                                                Convert.ToDateTime(
                                                    DateTime.Parse(tmpImportFfaOptionTrade.StartDate,
                                                                   startDateCultureInfo),
                                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                            PeriodTo =
                                                Convert.ToDateTime(
                                                    DateTime.Parse(tmpImportFfaOptionTrade.EndDate, endDateCultureInfo),
                                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                            Direction = tmpImportFfaOptionTrade.BuySell.ToUpper() == "Buy".ToUpper()
                                                            ? TradeInfoDirectionEnum.InOrBuy
                                                            : TradeInfoDirectionEnum.OutOrSell,
                                            IsJointVenture = false,
                                            Cruser = interfaceUserName,
                                            Chuser = interfaceUserName,
                                            Crd = runDate,
                                            Chd = runDate,
                                            Version = 1,
                                            DateFrom = runDate.Date,
                                            DateTo = null,
                                            RouteId = memConfigData[typeof (Route)].ElementAt(0).Value,
                                            RegionId = memConfigData[typeof (Region)].ElementAt(0).Value
                                        };
                    dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                    if (!string.IsNullOrEmpty(tmpImportFfaOptionTrade.Broker))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                                                  {
                                                      BrokerId =
                                                          memConfigData[typeof(Company)][tmpImportFfaOptionTrade.Broker.ToUpper()],
                                                      Commission =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.BrokerRate,
                                                                            brokerRateCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      Id = dataContext.GetNextId(typeof (TradeBrokerInfo)),
                                                      TradeInfoId = tradeInfo.Id
                                                  };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }

                    string bookName = string.IsNullOrEmpty(tmpImportFfaOptionTrade.Keyword)
                                          ? defaultBookName
                                          : tmpImportFfaOptionTrade.Keyword;
                    var tradeInfoBook = new TradeInfoBook()
                                            {
                                                BookId = memConfigData[typeof(Book)][bookName.ToUpper()],
                                                Id = dataContext.GetNextId(typeof (TradeInfoBook)),
                                                TradeInfoId = tradeInfo.Id
                                            };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);

                    Book book =
                        dataContext.Books.Where(a => a.Id == memConfigData[typeof(Book)][bookName.ToUpper()]).
                            SingleOrDefault();
                               
                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    if (trade.Type == TradeTypeEnum.FFA)
                    {
                        var tradeFfaInfo = new TradeFfaInfo()
                                               {
                                                   Id = dataContext.GetNextId(typeof (TradeFfaInfo)),
                                                   AllocationType =
                                                       string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                           ? TradeInfoVesselAllocationTypeEnum.None
                                                           : TradeInfoVesselAllocationTypeEnum.Single,
                                                   AccountId = null,
                                                   BankId = null,
                                                   ClearingFees = 0,
                                                   IndexId =
                                                       index.Id,
                                                   ClearingHouseId =
                                                       string.IsNullOrEmpty(tmpImportFfaOptionTrade.ClearingHouse)
                                                           ? (long?) null
                                                           : memConfigData[typeof (ClearingHouse)][
                                                               tmpImportFfaOptionTrade.ClearingHouse.ToUpper()],
                                                   PeriodType = PeriodTypeEnum.Month,
                                                   QuantityType = TradeInfoQuantityTypeEnum.PerMonth,
                                                   Purpose = TradeFFAInfoPurposeEnum.Hedge,
                                                   QuantityDays =
                                                       Convert.ToDecimal(
                                                           Decimal.Parse(tmpImportFfaOptionTrade.QtyTradedPerMonth,
                                                                         quantityCultureInfo),
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo),
                                                   PeriodDayCountType = TradeInfoPeriodDayCountTypeEnum.Thirty,
                                                   SettlementType = TradeInfoSettlementTypeEnum.AllDays,
                                                   Price =
                                                       Convert.ToDecimal(
                                                           Decimal.Parse(tmpImportFfaOptionTrade.FixedRate,
                                                                         fixedRateCultureInfo),
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo),
                                                   VesselId =
                                                       string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                           ? (long?) null
                                                           : memConfigData[typeof (Vessel)][
                                                               tmpImportFfaOptionTrade.HedgedAgainst.ToUpper()],
                                                   VesselPoolId = null,
                                                   TradeInfoId = tradeInfo.Id,
                                                   Period = tmpImportFfaOptionTrade.Period + "-" +
                                                            tradeInfo.PeriodFrom.Year.ToString().Substring(
                                                                tradeInfo.PeriodFrom.Year.ToString().Length - 2),
                                                   ClosedOutQuantityTotal =
                                                       Convert.ToDecimal(
                                                           Decimal.Parse(tmpImportFfaOptionTrade.ClosedOutQuantityTotal,
                                                                         closedOutQuantityTotalCultureInfo),
                                                           SessionRegistry.
                                                               ServerSessionRegistry.
                                                               ServerCultureInfo)

                        };
                        dataContext.TradeFfaInfos.InsertOnSubmit(tradeFfaInfo);
                    }
                    else if (trade.Type == TradeTypeEnum.Option)
                    {
                        var tradeOptionInfo = new TradeOptionInfo()
                                                  {
                                                      Id = dataContext.GetNextId(typeof (TradeOptionInfo)),
                                                      AllocationType =
                                                          string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                              ? TradeInfoVesselAllocationTypeEnum.None
                                                              : TradeInfoVesselAllocationTypeEnum.Single,
                                                      AccountId = null,
                                                      BankId = null,
                                                      ClearingFees = 0,
                                                      IndexId =
                                                          index.Id,
                                                      ClearingHouseId =
                                                          string.IsNullOrEmpty(tmpImportFfaOptionTrade.ClearingHouse)
                                                              ? (long?) null
                                                              : memConfigData[typeof (ClearingHouse)][
                                                                  tmpImportFfaOptionTrade.ClearingHouse.ToUpper()],
                                                      PeriodType = PeriodTypeEnum.Month,
                                                      Period = tmpImportFfaOptionTrade.Period + "-" +
                                                               tradeInfo.PeriodFrom.Year.ToString().Substring(
                                                                   tradeInfo.PeriodFrom.Year.ToString().Length - 2),
                                                      PeriodDayCountType = TradeInfoPeriodDayCountTypeEnum.Thirty,
                                                      Purpose = TradeFFAInfoPurposeEnum.Hedge,
                                                      Premium =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.PremiumRate,
                                                                            premiumRateRateCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      PremiumDueDate =
                                                          Convert.ToDateTime(
                                                              DateTime.Parse(tmpImportFfaOptionTrade.TradeDate,
                                                                             tradeDateCultureInfo),
                                                              SessionRegistry.ServerSessionRegistry.ServerCultureInfo).
                                                          Date,
                                                      QuantityDays =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.QtyTradedPerMonth,
                                                                            quantityCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      QuantityType = TradeInfoQuantityTypeEnum.PerMonth,
                                                      SettlementType = TradeInfoSettlementTypeEnum.AllDays,
                                                      Strike =
                                                          Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportFfaOptionTrade.FixedRate,
                                                                            fixedRateCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                                      TradeInfoId = tradeInfo.Id,
                                                      Type =
                                                          tmpImportFfaOptionTrade.ContractType.ToUpper().Contains(
                                                              "PUT")
                                                              ? TradeOPTIONInfoTypeEnum.Put
                                                              : TradeOPTIONInfoTypeEnum.Call,
                                                      VesselId =
                                                          string.IsNullOrEmpty(tmpImportFfaOptionTrade.HedgedAgainst)
                                                              ? (long?) null
                                                              : memConfigData[typeof (Vessel)][
                                                                  tmpImportFfaOptionTrade.HedgedAgainst.ToUpper()],
                                                      VesselPoolId = null
                        };
                        dataContext.TradeOptionInfos.InsertOnSubmit(tradeOptionInfo);
                    }

                    #endregion

                    configurationContext.SubmitChanges();
                }
                
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (configurationContext != null)
                    configurationContext.Dispose();
            }
        }

        private int? InsertCargoTrades(DataContext dataContext, List<TmpImportCargoTrade> tmpImportCargoTrades, string interfaceUserName, out string errorMessage)
        {
            DataContext configurationContext = null;
            errorMessage = "";
            string userName = null;
            Dictionary<Type, Dictionary<string, long>> memConfigData;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                configurationContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "InsertCargoTrades: " };
                }

                DateTime runDate = tmpImportCargoTrades[0].RunDate;
                memConfigData = new Dictionary<Type, Dictionary<string, long>>();

                const string defaultCompany = "AMNBC";
                const string defaultTrader = "MR";
                const string defaultBook = "AMNBulk";

                var insertedTradeCodes = new List<string>();

                CultureInfo dateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportCargoTrades.Select(a => a.Date).ToList());
                CultureInfo laydaysDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportCargoTrades.Select(a => a.LaydaysDate).ToList());
                
                CultureInfo baseTceRateCultureInfo =
                    GetCultureOfNumericValues(tmpImportCargoTrades.Select(a => a.BaseTceRate).ToList());
                if (baseTceRateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'BaseTceRate'");
                    return null;
                }

                CultureInfo sizeAdjustCultureInfo =
                    GetCultureOfNumericValues(tmpImportCargoTrades.Select(a => a.SizeAdjust).ToList());
                if (sizeAdjustCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'SizeAdjust'");
                    return null;
                }

                foreach (TmpImportCargoTrade tmpImportCargoTrade in tmpImportCargoTrades)
                {
                    if (insertedTradeCodes.Contains(tmpImportCargoTrade.OurRef)) continue;

                    Index index = null;
                    int? configResult = SetConfigurationData(configurationContext, TradeTypeEnum.Cargo,
                                                             tmpImportCargoTrade.RowNumber, runDate, defaultCompany,
                                                             tmpImportCargoTrade.Charterer, tmpImportCargoTrade.FixedBy, defaultBook,
                                                             new List<string>(), null,
                                                             null, tmpImportCargoTrade.TradeRoute, null, ref memConfigData,
                                                             out index, out errorMessage);
                    if (configResult == null)
                        return null;

                    #region Insert

                    var trade = new Trade()
                                    {
                                        Id = dataContext.GetNextId(typeof (Trade)),
                                        Type = TradeTypeEnum.Cargo,
                                        State = TradeStateEnum.Confirmed,
                                        Status = ActivationStatusEnum.Active,
                                        Cruser = interfaceUserName,
                                        Chuser = interfaceUserName,
                                        Crd = runDate,
                                        Chd = runDate
                                    };

                    dataContext.Trades.InsertOnSubmit(trade);

                    List<TmpImportCargoTrade> legTrades =
                        tmpImportCargoTrades.Where(a => a.OurRef == tmpImportCargoTrade.OurRef).ToList();

                    var tradeInfo = new TradeInfo()
                                        {
                                            Id = dataContext.GetNextId(typeof (TradeInfo)),
                                            TradeId = trade.Id,
                                            Code =
                                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                                trade.Id,
                                            ExternalCode = tmpImportCargoTrade.OurRef,
                                            SignDate =
                                                Convert.ToDateTime(
                                                    DateTime.Parse(tmpImportCargoTrade.Date,
                                                                   dateCultureInfo),
                                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                            CompanyId = memConfigData[typeof(Company)][defaultCompany.ToUpper()],
                                            CounterpartyId =
                                                memConfigData[typeof (Company)][tmpImportCargoTrade.Charterer.ToUpper()],
                                            TraderId = string.IsNullOrEmpty(tmpImportCargoTrade.FixedBy)
                                                           ? memConfigData[typeof(Trader)][defaultTrader.ToUpper()]
                                                           : memConfigData[typeof(Trader)][tmpImportCargoTrade.FixedBy.ToUpper()],
                                            MTMFwdIndexId = index.Id,
                                            MTMStressIndexId = index.Id,
                                            MarketId = index.MarketId,
                                            PeriodFrom =
                                                legTrades.Select(
                                                    a =>
                                                    Convert.ToDateTime(
                                                    DateTime.Parse(tmpImportCargoTrade.LaydaysDate,
                                                                   laydaysDateCultureInfo),
                                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo)).Min().Date,
                                            PeriodTo =
                                                legTrades.Select(
                                                    a =>
                                                    Convert.ToDateTime(
                                                    DateTime.Parse(tmpImportCargoTrade.LaydaysDate,
                                                                   laydaysDateCultureInfo),
                                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).AddDays(
                                                                           Double.Parse(
                                                                               a.EstDuration,
                                                                               SessionRegistry.
                                                                                   ServerSessionRegistry.
                                                                                   ServerCultureInfo))).Max().Date,
                                            Direction = TradeInfoDirectionEnum.InOrBuy,
                                            IsJointVenture = false,
                                            Cruser = interfaceUserName,
                                            Chuser = interfaceUserName,
                                            Crd = runDate,
                                            Chd = runDate,
                                            DateFrom = runDate.Date,
                                            DateTo = null,
                                            Version = 1,
                                            RouteId = memConfigData[typeof (Route)].ElementAt(0).Value,
                                            RegionId = memConfigData[typeof (Region)].ElementAt(0).Value
                                        };
                    dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                    var tradeInfoBook = new TradeInfoBook()
                                            {
                                                BookId = memConfigData[typeof (Book)][defaultBook.ToUpper()],
                                                Id = dataContext.GetNextId(typeof (TradeInfoBook)),
                                                TradeInfoId = tradeInfo.Id
                                            };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);

                    Book book =
                        dataContext.Books.Where(a => a.Id == memConfigData[typeof(Book)][defaultBook.ToUpper()]).
                            SingleOrDefault();

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    var tradeCargoInfo = new TradeCargoInfo()
                                             {
                                                 Id = dataContext.GetNextId(typeof (TradeCargoInfo)),
                                                 TradeInfoId = tradeInfo.Id,
                                                 VesselIndex = Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportCargoTrade.SizeAdjust,
                                                                            sizeAdjustCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo)
                                             };
                    dataContext.TradeCargoInfos.InsertOnSubmit(tradeCargoInfo);

                    for (int i = 1; i <= legTrades.Count(); i++)
                    {
                        var legCargoTrade = legTrades[i - 1];

                        var leg = new TradeCargoInfoLeg()
                                      {
                                          Id = dataContext.GetNextId(typeof (TradeCargoInfoLeg)),
                                          Identifier = i,
                                          IndexId = null,
                                          IndexPercentage = null,
                                          IsOptional = false,
                                          OptionalStatus = null,
                                          PeriodFrom =
                                              DateTime.Parse(legCargoTrade.LaydaysDate,
                                                             SessionRegistry.ServerSessionRegistry.
                                                                 ServerCultureInfo),
                                          PeriodTo =
                                              DateTime.Parse(legCargoTrade.LaydaysDate,
                                                             SessionRegistry.ServerSessionRegistry.
                                                                 ServerCultureInfo).AddDays(
                                                                     Double.Parse(
                                                                         legCargoTrade.
                                                                             EstDuration,
                                                                         SessionRegistry.
                                                                             ServerSessionRegistry.
                                                                             ServerCultureInfo)),
                                          Quantity = 50000,
                                          QuantityVariation = null,
                                          Rate = null,
                                          RateType = TradeInfoLegRateTypeEnum.Fixed,
                                          Tce = Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportCargoTrade.BaseTceRate,
                                                                            baseTceRateCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo),
                                          TradeCargoInfoId = tradeCargoInfo.Id
                                      };
                        dataContext.TradeCargoInfoLegs.InsertOnSubmit(leg);
                    }

                    #endregion

                    insertedTradeCodes.Add(tmpImportCargoTrade.OurRef);
                    configurationContext.SubmitChanges();
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
            finally
            {
                if (configurationContext != null)
                    configurationContext.Dispose();
            }
        }

        private int? InsertTcTrades(DataContext dataContext, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades, string interfaceUserName, out string errorMessage)
        {
            DataContext configurationContext = null;
            errorMessage = "";
            string userName = null;
            const string defaultTraderName = "Unknown";
            const string defaultVesselName = "TBN";
            const string defaultBookName = "TBA";

            Dictionary<Type, Dictionary<string, long>> memConfigData;

            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                configurationContext = new DataContext(DBConnectionString) { ObjectTrackingEnabled = true };

                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "InsertTCTrades: " };
                }

                DateTime runDate = tmpImportTcInTrades[0].RunDate;
                memConfigData = new Dictionary<Type, Dictionary<string, long>>();


                CultureInfo cpDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.CpDate).ToList());
                CultureInfo periodFromCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodFrom).ToList());
                CultureInfo periodToCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodTo).ToList());
                CultureInfo periodToMaxCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodToMax).ToList());
                CultureInfo periodToMinCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcInTrades.Select(a => a.PeriodToMin).ToList());
                CultureInfo rateCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcInTrades.Select(a => a.Rate).ToList());
                if (rateCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'Rate'");
                    return null;
                }

                CultureInfo sizeAdjustCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcInTrades.Select(a => a.SizeAdjust).ToList());
                if (sizeAdjustCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'SizeAdjust'");
                    return null;
                }

                CultureInfo startDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.StartDate).ToList());
                CultureInfo endDateCultureInfo =
                    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.EndDate).ToList());
                //CultureInfo periodStartCultureInfo =
                //    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.PeriodStart).ToList());
                //CultureInfo periodEndCultureInfo =
                //    GetCultureOfDateTimeValues(tmpImportTcOutTrades.Select(a => a.PeriodEnd).ToList());
                CultureInfo tceCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcOutTrades.Select(a => a.TceEstimate).ToList());
                if (tceCultureInfo == null)
                {
                    SiAuto.Main.LogError("Cannot recognize the culture of numeric values of column 'TceEstimate'");
                    return null;
                }
                CultureInfo vesselSizeCultureInfo =
                    GetCultureOfNumericValues(tmpImportTcOutTrades.Select(a => a.VesselSize).ToList());

                bool isUpdate;
                //var insertedTcInTradeCodes = new List<string>();
                foreach (TmpImportTcInTrade tmpImportTcInTrade in tmpImportTcInTrades)
                {
                    //if (!tmpImportTcOutTrades.Select(a => a.OurRef).Contains(tmpImportTcInTrade.HeadFixtureRef))
                    //{
                    //    SiAuto.Main.LogError("There was no corresponding tc out trade for tc in trade with code :" +
                    //                         tmpImportTcInTrade.HeadFixtureRef + ", RowNumber: " +
                    //                         tmpImportTcInTrade.RowNumber);
                    //    continue;
                    //}

                    Index index = null;
                    
                    string counterPartyStr = string.IsNullOrEmpty(tmpImportTcInTrade.DisponentOwner)
                                            ? (string.IsNullOrEmpty(tmpImportTcInTrade.Company1)
                                                   ? "TBA"
                                                   : tmpImportTcInTrade.Company1)
                                            : tmpImportTcInTrade.DisponentOwner;

                    int? configResult = SetConfigurationData(configurationContext, TradeTypeEnum.TC,
                                                             tmpImportTcInTrade.RowNumber, runDate,
                                                             tmpImportTcInTrade.Company,
                                                             counterPartyStr, tmpImportTcInTrade.FixedBy,
                                                             tmpImportTcInTrade.AnalysisCode,
                                                             new List<string>()
                                                                 {
                                                                     tmpImportTcInTrade.Broker1,
                                                                     tmpImportTcInTrade.Broker2,
                                                                     tmpImportTcInTrade.Broker3
                                                                 },
                                                             tmpImportTcInTrade.Vessel,
                                                             null, tmpImportTcInTrade.TradeRoute,
                                                             tmpImportTcInTrade.YearBuilt, ref memConfigData,
                                                             out index, out errorMessage);
                    if (configResult == null)
                        return null;

                    #region Legs calculation

                    var legs = new List<TradeTcInfoLeg>();

                    if ((tmpImportTcInTrade.Status.ToUpper() == "Redelivered".ToUpper() || tmpImportTcInTrade.Status.ToUpper() == "Finalised".ToUpper()) ||
                        (tmpImportTcInTrade.PeriodToMax == tmpImportTcInTrade.PeriodToMin &&
                         !(tmpImportTcInTrade.Status.ToUpper() == "Redelivered".ToUpper() || tmpImportTcInTrade.Status.ToUpper() == "Finalised".ToUpper())))
                    {
                        var leg = new TradeTcInfoLeg()
                                      {
                                          Id = dataContext.GetNextId(typeof (TradeTcInfoLeg)),
                                          Identifier = 1,
                                          IndexId =
                                              tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                  ? index.Id
                                                  : (long?) null,
                                          IndexPercentage =
                                              tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                  ? (Convert.ToDecimal(
                                                      Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                                    sizeAdjustCultureInfo),
                                                      SessionRegistry.
                                                          ServerSessionRegistry.
                                                          ServerCultureInfo))
                                                  : (decimal?) null,
                                          IsOptional = false,
                                          OptionalStatus = null,
                                          PeriodFrom =
                                              Convert.ToDateTime(
                                                  DateTime.Parse(tmpImportTcInTrade.PeriodFrom,
                                                                 periodFromCultureInfo),
                                                  SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                          PeriodTo =
                                              Convert.ToDateTime(
                                                  DateTime.Parse(tmpImportTcInTrade.PeriodTo,
                                                                 periodToCultureInfo),
                                                  SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                          Rate =
                                              tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                  ? null
                                                  : string.IsNullOrEmpty(tmpImportTcInTrade.Rate)
                                                        ? (decimal?) null
                                                        : Convert.ToDecimal(
                                                            Decimal.Parse(tmpImportTcInTrade.Rate,
                                                                          rateCultureInfo),
                                                            SessionRegistry.
                                                                ServerSessionRegistry.
                                                                ServerCultureInfo),
                                          RateType =
                                              tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                  ? TradeInfoLegRateTypeEnum.IndexLinked
                                                  : TradeInfoLegRateTypeEnum.Fixed,
                                          RedeliveryDays = 20
                                      };
                        legs.Add(leg);
                    }
                    else
                    {
                        //Find corresponding tc out trade legs
                        List<TmpImportTcOutTrade> outTradeLegs =
                            tmpImportTcOutTrades.Where(a => a.OurRef == tmpImportTcInTrade.HeadFixtureRef).ToList();

                        //Find the last End Date of tc out trade legs
                        DateTime? lastEndDate =
                            outTradeLegs.Select(
                                a =>
                                Convert.ToDateTime(
                                    DateTime.Parse(a.EndDate,
                                                   endDateCultureInfo),
                                    SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date).Any()
                                ? outTradeLegs.Select(
                                    a =>
                                    Convert.ToDateTime(
                                        DateTime.Parse(a.EndDate,
                                                       endDateCultureInfo),
                                        SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date).Max()
                                : (DateTime?)null;

                        //Period To Min
                        DateTime periodToMin = Convert.ToDateTime(
                            DateTime.Parse(tmpImportTcInTrade.PeriodToMin,
                                           periodToMinCultureInfo),
                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date;
                        //Period To
                        DateTime periodTo = Convert.ToDateTime(
                            DateTime.Parse(tmpImportTcInTrade.PeriodTo,
                                           periodToCultureInfo),
                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo).
                            Date;

                        //Period ToMax
                        DateTime periodToMax = Convert.ToDateTime(
                            DateTime.Parse(tmpImportTcInTrade.PeriodToMax,
                                           periodToMaxCultureInfo),
                            SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date;

                        var leg1 = new TradeTcInfoLeg()
                                       {
                                           Id = dataContext.GetNextId(typeof (TradeTcInfoLeg)),
                                           Identifier = 1,
                                           IndexId =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? index.Id
                                                   : (long?) null,
                                           IndexPercentage =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? (Convert.ToDecimal(
                                                       Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                                     sizeAdjustCultureInfo),
                                                       SessionRegistry.
                                                           ServerSessionRegistry.
                                                           ServerCultureInfo))
                                                   : (decimal?) null,
                                           IsOptional = false,
                                           OptionalStatus = null,
                                           PeriodFrom =
                                               Convert.ToDateTime(
                                                   DateTime.Parse(tmpImportTcInTrade.PeriodFrom,
                                                                  periodFromCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                           PeriodTo = periodToMin,
                                           Rate = tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                      ? (decimal?) null
                                                      : Convert.ToDecimal(
                                                          Decimal.Parse(tmpImportTcInTrade.Rate,
                                                                        rateCultureInfo),
                                                          SessionRegistry.
                                                              ServerSessionRegistry.
                                                              ServerCultureInfo),
                                           RateType =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? TradeInfoLegRateTypeEnum.IndexLinked
                                                   : TradeInfoLegRateTypeEnum.Fixed,
                                           RedeliveryDays = 20
                                       };
                        legs.Add(leg1);

                        var leg2 = new TradeTcInfoLeg()
                                       {
                                           Id = dataContext.GetNextId(typeof (TradeTcInfoLeg)),
                                           Identifier = 1,
                                           IndexId =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? index.Id
                                                   : (long?) null,
                                           IndexPercentage =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? (Convert.ToDecimal(
                                                       Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                                     sizeAdjustCultureInfo),
                                                       SessionRegistry.
                                                           ServerSessionRegistry.
                                                           ServerCultureInfo))
                                                   : (decimal?) null,
                                           IsOptional = true,
                                           OptionalStatus =
                                               runDate < periodToMin
                                                   ? (tmpImportTcInTrade.Status.ToUpper() != "Running".ToUpper()
                                                          ? TradeInfoLegOptionalStatusEnum.
                                                                NonDeclared
                                                          : (periodTo == periodToMin
                                                                 ? TradeInfoLegOptionalStatusEnum
                                                                       .Pending
                                                                 : TradeInfoLegOptionalStatusEnum
                                                                       .Declared))
                                                   : TradeInfoLegOptionalStatusEnum.Declared,
                                           PeriodFrom = periodToMin,
                                           PeriodTo = periodToMax,
                                           Rate = tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                      ? (decimal?) null
                                                      : string.IsNullOrEmpty(tmpImportTcInTrade.Rate)
                                                            ? (decimal?) null
                                                            : Convert.ToDecimal(
                                                                Decimal.Parse(tmpImportTcInTrade.Rate,
                                                                              rateCultureInfo),
                                                                SessionRegistry.
                                                                    ServerSessionRegistry.
                                                                    ServerCultureInfo),
                                           RateType =
                                               tmpImportTcInTrade.RateType.ToUpper() == "Flexible Hire".ToUpper()
                                                   ? TradeInfoLegRateTypeEnum.IndexLinked
                                                   : TradeInfoLegRateTypeEnum.Fixed,
                                           RedeliveryDays = 20
                                       };

                        if (runDate >= periodToMin && tmpImportTcInTrade.Status.ToUpper() != "Running".ToUpper() && lastEndDate == (DateTime?)null)
                        {
                            SiAuto.Main.LogError("Last End Date of tc out leg does not exist, RowNumber: " + tmpImportTcInTrade.RowNumber);
                            return null;
                        }
                        if (runDate >= periodToMin && tmpImportTcInTrade.Status.ToUpper() != "Running".ToUpper())
                            leg2.PeriodTo = lastEndDate.Value;

                        legs.Add(leg2);
                    }

                    #endregion

                    #region Insert

                    var trade = new Trade()
                                    {
                                        Id = dataContext.GetNextId(typeof (Trade)),
                                        Type = TradeTypeEnum.TC,
                                        State = TradeStateEnum.Confirmed,
                                        Status = ActivationStatusEnum.Active,
                                        Cruser = interfaceUserName,
                                        Chuser = interfaceUserName,
                                        Crd = runDate,
                                        Chd = runDate
                                    };
                    dataContext.Trades.InsertOnSubmit(trade);

                    var tradeInfo = new TradeInfo()
                                        {
                                            Id = dataContext.GetNextId(typeof (TradeInfo)),
                                            TradeId = trade.Id,
                                            Code =
                                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                                trade.Id,
                                            ExternalCode = tmpImportTcInTrade.HeadFixtureRef,
                                            SignDate =
                                                Convert.ToDateTime(
                                                   DateTime.Parse(tmpImportTcInTrade.CpDate,
                                                                  cpDateCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                            CompanyId = memConfigData[typeof(Company)][tmpImportTcInTrade.Company.ToUpper()],
                                            CounterpartyId = memConfigData[typeof (Company)][counterPartyStr.ToUpper()],
                                            TraderId = string.IsNullOrEmpty(tmpImportTcInTrade.FixedBy)
                                                           ? memConfigData[typeof(Trader)][defaultTraderName.ToUpper()]
                                                           : memConfigData[typeof(Trader)][tmpImportTcInTrade.FixedBy.ToUpper()],
                                            MTMFwdIndexId = index.Id,
                                            MTMStressIndexId = index.Id,
                                            MarketId = index.MarketId,
                                            PeriodFrom =
                                                legs.Select(a => a.PeriodFrom).Min().Date,
                                            PeriodTo =
                                                legs.Select(a => a.PeriodTo).Max().Date,
                                            Direction = TradeInfoDirectionEnum.InOrBuy,
                                            IsJointVenture = false,
                                            DateFrom = runDate.Date,
                                            DateTo = null,
                                            Version = 1,
                                            RouteId = memConfigData[typeof (Route)].ElementAt(0).Value,
                                            RegionId = memConfigData[typeof (Region)].ElementAt(0).Value,
                                            Cruser = interfaceUserName,
                                            Chuser = interfaceUserName,
                                            Crd = runDate,
                                            Chd = runDate,
                                        };
                    dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                    var tradeTcInfo = new TradeTcInfo()
                                          {
                                              Address = null,
                                              BallastBonus = null,
                                              Delivery = null,
                                              Id = dataContext.GetNextId(typeof (TradeTcInfo)),
                                              IsBareboat = null,
                                              Redelivery = null,
                                              TradeInfoId = tradeInfo.Id,
                                              VesselId = string.IsNullOrEmpty(tmpImportTcInTrade.Vessel)
                                                             ? memConfigData[typeof(Vessel)][defaultVesselName.ToUpper()]
                                                             : memConfigData[typeof (Vessel)][tmpImportTcInTrade.Vessel.ToUpper()],
                                              VesselIndex =
                                                  Convert.ToDecimal(
                                                              Decimal.Parse(tmpImportTcInTrade.SizeAdjust,
                                                                            sizeAdjustCultureInfo),
                                                              SessionRegistry.
                                                                  ServerSessionRegistry.
                                                                  ServerCultureInfo)
                                          };
                    dataContext.TradeTcInfos.InsertOnSubmit(tradeTcInfo);
                    foreach (TradeTcInfoLeg tradeTcInfoLeg in legs)
                    {
                        tradeTcInfoLeg.TradeTcInfoId = tradeTcInfo.Id;
                        dataContext.TradeTcInfoLegs.InsertOnSubmit(tradeTcInfoLeg);
                    }

                    #region Brokers

                    decimal defaultCommission = 1.25m;

                    if (!string.IsNullOrEmpty(tmpImportTcInTrade.Broker1))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                                                  {
                                                      Id = dataContext.GetNextId(typeof (TradeBrokerInfo)),
                                                      BrokerId =
                                                          memConfigData[typeof(Company)][tmpImportTcInTrade.Broker1.ToUpper()],
                                                      Commission = defaultCommission,
                                                      TradeInfoId = tradeInfo.Id
                                                  };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }
                    if (!string.IsNullOrEmpty(tmpImportTcInTrade.Broker2))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                                                  {
                                                      Id = dataContext.GetNextId(typeof (TradeBrokerInfo)),
                                                      BrokerId =
                                                          memConfigData[typeof(Company)][tmpImportTcInTrade.Broker2.ToUpper()],
                                                      Commission = defaultCommission,
                                                      TradeInfoId = tradeInfo.Id
                                                  };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }
                    if (!string.IsNullOrEmpty(tmpImportTcInTrade.Broker3))
                    {
                        var tradeBrokerInfo = new TradeBrokerInfo()
                                                  {
                                                      Id = dataContext.GetNextId(typeof (TradeBrokerInfo)),
                                                      BrokerId =
                                                          memConfigData[typeof(Company)][tmpImportTcInTrade.Broker3.ToUpper()],
                                                      Commission = defaultCommission,
                                                      TradeInfoId = tradeInfo.Id
                                                  };
                        dataContext.TradeBrokerInfos.InsertOnSubmit(tradeBrokerInfo);
                    }

                    #endregion

                    string bookName = string.IsNullOrEmpty(tmpImportTcInTrade.AnalysisCode)
                                          ? defaultBookName
                                          : tmpImportTcInTrade.AnalysisCode;
                    var tradeInfoBook = new TradeInfoBook()
                                            {
                                                BookId = memConfigData[typeof(Book)][bookName.ToUpper()],
                                                Id = dataContext.GetNextId(typeof (TradeInfoBook)),
                                                TradeInfoId = tradeInfo.Id
                                            };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);
                    Book book =
                        dataContext.Books.Where(a => a.Id == memConfigData[typeof(Book)][bookName.ToUpper()]).
                            SingleOrDefault();

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    #endregion

                    //insertedTcInTradeCodes.Add(tmpImportTcInTrade.HeadFixtureRef);
                    configurationContext.SubmitChanges();
                }

                var insertedTcOutTradeCodes = new List<string>();
                foreach (TmpImportTcOutTrade tmpImportTcOutTrade in tmpImportTcOutTrades)
                {
                    if (!tmpImportTcInTrades.Select(a => a.HeadFixtureRef).Contains(tmpImportTcOutTrade.OurRef))
                    {
                        SiAuto.Main.LogError("There was no corrsponding tc in trade for tc out trade with code :" +
                                             tmpImportTcOutTrade.OurRef + ", RowNumber: " +
                                             tmpImportTcOutTrade.RowNumber);
                        continue;
                    }

                    TmpImportTcInTrade importTcInTrade =
                        tmpImportTcInTrades.Where(a => a.HeadFixtureRef == tmpImportTcOutTrade.OurRef).First();

                    string counterPartyStr = string.IsNullOrEmpty(importTcInTrade.DisponentOwner)
                                            ? (string.IsNullOrEmpty(importTcInTrade.Company1)
                                                   ? "TBA"
                                                   : importTcInTrade.Company1)
                                            : importTcInTrade.DisponentOwner;

                    Index index = null;
                    int? configResult = SetConfigurationData(configurationContext, TradeTypeEnum.Cargo,
                                                             tmpImportTcOutTrade.RowNumber, runDate,
                                                             importTcInTrade.Company,
                                                             counterPartyStr, tmpImportTcOutTrade.ChartId,
                                                             importTcInTrade.AnalysisCode,
                                                             new List<string>(),
                                                             tmpImportTcOutTrade.Vessel, null,
                                                             importTcInTrade.TradeRoute, null, ref memConfigData,
                                                             out index, out errorMessage);
                    if (configResult == null)
                        return null;

                    #region Legs Calculation

                    decimal rate;
                    //Rate Calculation
                    rate = Convert.ToDecimal(Decimal.Parse(tmpImportTcOutTrade.VesselSize, vesselSizeCultureInfo),
                                             SessionRegistry.ServerSessionRegistry.ServerCultureInfo) > 59999
                               ? Convert.ToDecimal(Decimal.Parse(tmpImportTcOutTrade.TceEstimate,
                                                                 tceCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo)/(1 - 0.0375m)
                               : Convert.ToDecimal(Decimal.Parse(tmpImportTcOutTrade.TceEstimate,
                                                                 tceCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo)/(1 - 0.05m);
                    var leg = new TradeTcInfoLeg()
                                  {
                                      Id = dataContext.GetNextId(typeof (TradeTcInfoLeg)),
                                      Identifier = 1,
                                      IndexId = null,
                                      IndexPercentage = null,
                                      IsOptional = false,
                                      OptionalStatus = null,
                                      PeriodFrom =
                                          Convert.ToDateTime(
                                              DateTime.Parse(tmpImportTcOutTrade.StartDate,
                                                             startDateCultureInfo),
                                              SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                      PeriodTo =
                                          Convert.ToDateTime(
                                              DateTime.Parse(tmpImportTcOutTrade.EndDate,
                                                             endDateCultureInfo),
                                              SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                      Rate = rate,
                                      RateType = TradeInfoLegRateTypeEnum.Fixed,
                                      RedeliveryDays = 0 //TODO
                                  };

                    #endregion

                    #region Insert

                    var trade = new Trade()
                                    {
                                        Id = dataContext.GetNextId(typeof (Trade)),
                                        Type = TradeTypeEnum.TC,
                                        State = TradeStateEnum.Confirmed,
                                        Status = ActivationStatusEnum.Active,
                                        Cruser = interfaceUserName,
                                        Chuser = interfaceUserName,
                                        Crd = runDate,
                                        Chd = runDate
                                    };
                    dataContext.Trades.InsertOnSubmit(trade);

                    var tradeInfo = new TradeInfo()
                                        {
                                            Id = dataContext.GetNextId(typeof (TradeInfo)),
                                            TradeId = trade.Id,
                                            Code =
                                                trade.Type.ToString("g") + "_" + runDate.ToString("u") + "_" +
                                                trade.Id,
                                            ExternalCode = tmpImportTcOutTrade.Voy,
                                            SignDate =
                                                Convert.ToDateTime(
                                                   DateTime.Parse(tmpImportTcOutTrade.StartDate,
                                                                  startDateCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date > runDate
                                                    ? runDate
                                                    : Convert.ToDateTime(
                                                   DateTime.Parse(tmpImportTcOutTrade.StartDate,
                                                                  startDateCultureInfo),
                                                   SessionRegistry.ServerSessionRegistry.ServerCultureInfo).Date,
                                            CompanyId = memConfigData[typeof(Company)][importTcInTrade.Company.ToUpper()],
                                            CounterpartyId =
                                                memConfigData[typeof (Company)][counterPartyStr.ToUpper()],
                                            TraderId = string.IsNullOrEmpty(tmpImportTcOutTrade.ChartId)
                                                           ? memConfigData[typeof(Trader)][defaultTraderName.ToUpper()]
                                                           : memConfigData[typeof (Trader)][
                                                               tmpImportTcOutTrade.ChartId.ToUpper()],
                                            MTMFwdIndexId = index.Id,
                                            MTMStressIndexId = index.Id,
                                            MarketId = index.MarketId,
                                            PeriodFrom =
                                                leg.PeriodFrom,
                                            PeriodTo =
                                                leg.PeriodTo,
                                            Direction = TradeInfoDirectionEnum.OutOrSell,
                                            IsJointVenture = false,
                                            DateFrom = runDate.Date,
                                            DateTo = null,
                                            Version = 1,
                                            RouteId = memConfigData[typeof (Route)].ElementAt(0).Value,
                                            RegionId = memConfigData[typeof (Region)].ElementAt(0).Value,
                                            Cruser = interfaceUserName,
                                            Chuser = interfaceUserName,
                                            Crd = runDate,
                                            Chd = runDate,
                                        };
                    dataContext.TradeInfos.InsertOnSubmit(tradeInfo);

                    var tradeTcInfo = new TradeTcInfo()
                                          {
                                              Address = null,
                                              BallastBonus = null,
                                              Delivery = null,
                                              Id = dataContext.GetNextId(typeof (TradeTcInfo)),
                                              IsBareboat = null,
                                              Redelivery = null,
                                              TradeInfoId = tradeInfo.Id,
                                              VesselId = string.IsNullOrEmpty(tmpImportTcOutTrade.Vessel)
                                                             ? memConfigData[typeof(Vessel)][defaultVesselName.ToUpper()]
                                                             : memConfigData[typeof (Vessel)][tmpImportTcOutTrade.Vessel.ToUpper()],
                                              VesselIndex =
                                                  Convert.ToDecimal(Decimal.Parse(importTcInTrade.SizeAdjust,
                                                                                  sizeAdjustCultureInfo),
                                                                    SessionRegistry.ServerSessionRegistry.
                                                                        ServerCultureInfo)
                                          };
                    dataContext.TradeTcInfos.InsertOnSubmit(tradeTcInfo);

                    leg.TradeTcInfoId = tradeTcInfo.Id;
                    dataContext.TradeTcInfoLegs.InsertOnSubmit(leg);

                    string bookName = string.IsNullOrEmpty(importTcInTrade.AnalysisCode)
                                          ? defaultBookName
                                          : importTcInTrade.AnalysisCode;
                    var tradeInfoBook = new TradeInfoBook()
                                            {
                                                BookId = memConfigData[typeof(Book)][bookName.ToUpper()],
                                                Id = dataContext.GetNextId(typeof (TradeInfoBook)),
                                                TradeInfoId = tradeInfo.Id
                                            };
                    dataContext.TradeInfoBooks.InsertOnSubmit(tradeInfoBook);
                    Book book =
                        dataContext.Books.Where(a => a.Id == memConfigData[typeof(Book)][bookName.ToUpper()]).
                            SingleOrDefault();

                    if (book != null && book.ParentBookId != null)
                    {
                        string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                     book.ParentBookId;

                        List<long> parentBookIds = dataContext.Query<long>(sql).ToList();
                        foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                        {
                            var newTradeBook = new TradeInfoBook()
                            {
                                BookId = parentBookId,
                                Id = dataContext.GetNextId(typeof(TradeInfoBook)),
                                TradeInfoId = tradeInfo.Id
                            };
                            dataContext.TradeInfoBooks.InsertOnSubmit(newTradeBook);
                        }
                    }

                    #endregion

                    insertedTcOutTradeCodes.Add(tmpImportTcOutTrade.OurRef);

                    configurationContext.SubmitChanges();
                }
                
                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                errorMessage = exc.Message;
                return null;
            }
            finally
            {
                if (configurationContext != null)
                    configurationContext.Dispose();
            }
        }

        private int? DeletePreviousSoftMarData(DataContext dataContext, string interfaceUserName, TradeTypeEnum tradeTypeEnum)
        {
            try
            {
                List<Trade> softMarTrades;
                if (tradeTypeEnum == TradeTypeEnum.FFA || tradeTypeEnum == TradeTypeEnum.Option)
                    softMarTrades =
                        dataContext.Trades.Where(
                            a =>
                            a.Cruser == interfaceUserName &&
                            (a.Type == TradeTypeEnum.FFA || a.Type == TradeTypeEnum.Option)).ToList();
                else
                    softMarTrades = dataContext.Trades.Where(a => a.Cruser == interfaceUserName && a.Type == tradeTypeEnum).ToList();
                List<long> softMarTradesIds = softMarTrades.Select(a => a.Id).ToList();
                dataContext.Trades.DeleteAllOnSubmit(softMarTrades);

                List<TradeInfo> softMarTradeInfos =
                        dataContext.TradeInfos.Where(a => softMarTradesIds.Contains(a.TradeId)).ToList();
                List<long> softMarTradeInfosIds = softMarTradeInfos.Select(a => a.Id).ToList();
                dataContext.TradeInfos.DeleteAllOnSubmit(softMarTradeInfos);

                List<TradeBrokerInfo> softMarTradeBrokerInfos =
                    dataContext.TradeBrokerInfos.Where(a => softMarTradeInfosIds.Contains(a.TradeInfoId)).ToList();
                dataContext.TradeBrokerInfos.DeleteAllOnSubmit(softMarTradeBrokerInfos);

                List<TradeInfoBook> softMarTradeInfoBooks =
                    dataContext.TradeInfoBooks.Where(a => softMarTradeInfosIds.Contains(a.TradeInfoId)).ToList();
                dataContext.TradeInfoBooks.DeleteAllOnSubmit(softMarTradeInfoBooks);

                if (tradeTypeEnum == TradeTypeEnum.FFA || tradeTypeEnum == TradeTypeEnum.Option)
                {
                    List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades = dataContext.TmpImportFfaOptionTrades.ToList();
                    dataContext.TmpImportFfaOptionTrades.DeleteAllOnSubmit(tmpImportFfaOptionTrades);

                    List<TradeFfaInfo> softMarFfaInfos =
                        dataContext.TradeFfaInfos.Where(a => softMarTradeInfosIds.Contains(a.TradeInfoId)).ToList();
                    dataContext.TradeFfaInfos.DeleteAllOnSubmit(softMarFfaInfos);

                    List<TradeOptionInfo> softMarOptionInfos =
                        dataContext.TradeOptionInfos.Where(a => softMarTradeInfosIds.Contains(a.TradeInfoId)).ToList();
                    dataContext.TradeOptionInfos.DeleteAllOnSubmit(softMarOptionInfos);
                }
                else if (tradeTypeEnum == TradeTypeEnum.Cargo)
                {
                    List<TmpImportCargoTrade> tmpImportCargoTrades = dataContext.TmpImportCargoTrades.ToList();
                    dataContext.TmpImportCargoTrades.DeleteAllOnSubmit(tmpImportCargoTrades);

                    List<TradeCargoInfo> softMarCargoInfos =
                        dataContext.TradeCargoInfos.Where(a => softMarTradeInfosIds.Contains(a.TradeInfoId)).ToList();
                    dataContext.TradeCargoInfos.DeleteAllOnSubmit(softMarCargoInfos);

                    List<long> softMarCargoInfosIds = softMarCargoInfos.Select(a => a.Id).ToList();

                    List<TradeCargoInfoLeg> softMarTradeCargoLegs =
                        dataContext.TradeCargoInfoLegs.Where(a => softMarCargoInfosIds.Contains(a.TradeCargoInfoId))
                            .ToList();
                    dataContext.TradeCargoInfoLegs.DeleteAllOnSubmit(softMarTradeCargoLegs);
                }
                else if (tradeTypeEnum == TradeTypeEnum.TC)
                {
                    List<TmpImportTcInTrade> tmpImportTcInTrades = dataContext.TmpImportTcInTrades.ToList();
                    dataContext.TmpImportTcInTrades.DeleteAllOnSubmit(tmpImportTcInTrades);

                    List<TmpImportTcOutTrade> tmpImportTcOutTrades = dataContext.TmpImportTcOutTrades.ToList();
                    dataContext.TmpImportTcOutTrades.DeleteAllOnSubmit(tmpImportTcOutTrades);

                    List<TradeTcInfo> softMarTcInfos =
                        dataContext.TradeTcInfos.Where(a => softMarTradeInfosIds.Contains(a.TradeInfoId)).ToList();
                    dataContext.TradeTcInfos.DeleteAllOnSubmit(softMarTcInfos);

                    List<long> softMarTcInfosIds = softMarTcInfos.Select(a => a.Id).ToList();

                    List<TradeTcInfoLeg> softMarTradeTcLegs =
                        dataContext.TradeTcInfoLegs.Where(a => softMarTcInfosIds.Contains(a.TradeTcInfoId))
                            .ToList();
                    dataContext.TradeTcInfoLegs.DeleteAllOnSubmit(softMarTradeTcLegs);
                }

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(GenericContext<ProgramInfo>.Current.Value.UserName, exc);
                return null;
            }
        }

        #endregion

        private int? SetConfigurationData(DataContext configurationContext, TradeTypeEnum tradeTypeEnum, long? rowNumber, DateTime runDate, string companyStr, string counterpartyStr, string traderStr, string bookStr, List<string> brokers, string vesselStr, string clearingHouseStr, string indexCodeStr, string yearBuilt, ref Dictionary<Type, Dictionary<string, long>> memConfigData, out Index index, out string errorMessage)
        {
            string userName = null;
            errorMessage = "";
            const string importUserName = "SoftMar";
            index = null;
            try
            {
                userName = GenericContext<ProgramInfo>.Current.Value.UserName;
                if (SiAuto.Si.Level == Level.Debug)
                {
                    if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                    configurationContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(userName)) { TitleLimit = 0, TitlePrefix = "SetConfigurationData: " };
                }

                #region Default route/region

                if (!memConfigData.ContainsKey(typeof(Route)))//Mandatory for TradeInfo
                {
                    Route defaultRoute = configurationContext.Routes.SingleOrDefault(a => a.IsDefault);
                    Region defaultRegion = configurationContext.Regions.SingleOrDefault(a => a.IsDefault);

                    if (defaultRoute == null)
                    {
                        SiAuto.Main.LogError("A default Route is not defined");
                        errorMessage = "There was an error during import. A default Route is not defined.";
                        return null;
                    }
                    if (defaultRegion == null)
                    {
                        SiAuto.Main.LogError("A default Region is not defined");
                        errorMessage = "There was an error during import. A default Region is not defined.";
                        return null;
                    }
                    if (!memConfigData.ContainsKey(typeof(Route)))
                        memConfigData.Add(typeof(Route), new Dictionary<string, long>());
                    memConfigData[typeof(Route)][defaultRoute.Name.ToUpper()] = defaultRoute.Id;
                    if (!memConfigData.ContainsKey(typeof(Region)))
                        memConfigData.Add(typeof(Region), new Dictionary<string, long>());
                    memConfigData[typeof(Region)][defaultRegion.Name.ToUpper()] = defaultRegion.Id;
                }

                #endregion

                #region Trade Info Company

                Company company;
                if (string.IsNullOrEmpty(companyStr))//Mandatory for TradeInfo
                {
                    SiAuto.Main.LogError("Field 'Company' does not have a value, RowNumber: " + rowNumber);
                    errorMessage = "There was an error during import. Field 'Company' does not have a value, RowNumber: " + rowNumber;
                    return null;
                }
                CompanySubtype subType =
                    configurationContext.CompanySubtypes.FirstOrDefault(a => a.Subtype == CompanySubtypeEnum.Counterparty);
                if (subType == null)
                {
                    SiAuto.Main.LogError(
                        "There is no CompanySubType with type Counterparty in the system. RowNumber: " + rowNumber);
                    errorMessage = "There was an error during import. There is no CompanySubType with type Counterparty in the system. RowNumber: " +
                                   rowNumber;
                    return null;
                }
                if (!memConfigData.ContainsKey(typeof(Company)) || !memConfigData[typeof(Company)].ContainsKey(companyStr.ToUpper()))
                {
                    company =
                        configurationContext.Companies.FirstOrDefault(a => a.Name.ToUpper() == companyStr.ToUpper());
                    if (company == null)
                    {
                        company = new Company()
                        {
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName,
                            Id = configurationContext.GetNextId(typeof(Company)),
                            IsCounterParty = true,
                            ManagerId = null,
                            Name = companyStr,
                            Ownership = null,
                            Type = CompanyTypeEnum.Internal,
                            ParentId = null,
                            RiskRating = 1,
                            Status = ActivationStatusEnum.Active,
                            SubtypeId = subType.Id
                        };
                        configurationContext.Companies.InsertOnSubmit(company);

                        if (!memConfigData.ContainsKey(typeof(Company)))
                            memConfigData.Add(typeof(Company), new Dictionary<string, long>());

                        memConfigData[typeof(Company)][companyStr.ToUpper()] = company.Id;
                    }
                    else
                    {
                        if (!memConfigData.ContainsKey(typeof(Company)))
                            memConfigData.Add(typeof(Company), new Dictionary<string, long>());
                        memConfigData[typeof(Company)][companyStr.ToUpper()] = company.Id;
                    }
                }

                #endregion

                #region Trade Info Counterparty

                if (string.IsNullOrEmpty(counterpartyStr))//Mandatory for TradeInfo
                {
                    SiAuto.Main.LogError("Field 'Counterparty' does not have a value, RowNumber: " + rowNumber);
                    errorMessage = "There was an error during import. Field 'Counterparty' does not have a value, RowNumber: " + rowNumber;
                    return null;
                }
                if (!memConfigData.ContainsKey(typeof(Company)) ||
                    !memConfigData[typeof(Company)].ContainsKey(counterpartyStr.ToUpper()))
                {
                    var counterparty =
                        configurationContext.Companies.FirstOrDefault(a => a.Name.ToUpper() == counterpartyStr.ToUpper());
                    if (counterparty == null)
                    {
                        counterparty = new Company()
                        {
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName,
                            Id = configurationContext.GetNextId(typeof(Company)),
                            IsCounterParty = true,
                            ManagerId = null,
                            Name = counterpartyStr,
                            Ownership = null,
                            Type = CompanyTypeEnum.External,
                            ParentId = null,
                            RiskRating = 1,
                            Status = ActivationStatusEnum.Active,
                            SubtypeId = subType.Id
                        };
                        configurationContext.Companies.InsertOnSubmit(counterparty);

                        if (!memConfigData.ContainsKey(typeof(Company)))
                            memConfigData.Add(typeof(Company), new Dictionary<string, long>());
                        memConfigData[typeof(Company)][counterpartyStr.ToUpper()] = counterparty.Id;
                    }
                    else
                    {
                        if (!memConfigData.ContainsKey(typeof(Company)))
                            memConfigData.Add(typeof(Company), new Dictionary<string, long>());
                        memConfigData[typeof(Company)][counterpartyStr.ToUpper()] = counterparty.Id;
                    }
                }

                #endregion

                #region SoftMar Index Code Mapping

                InterfaceIndexesMapping mapping =
                    configurationContext.InterfaceIndexesMappings.SingleOrDefault(a => a.InterfaceIndexCode.ToUpper() == indexCodeStr.ToUpper());

                if (mapping == null || string.IsNullOrEmpty(indexCodeStr)) //Mandatory for TradeInfo
                {
                    SiAuto.Main.LogError("There is no mapping between SoftMar and Baltic Index. SoftMar Code:" +
                                         indexCodeStr + ", RowNumber: " +
                                         rowNumber);
                    errorMessage = "There was an error during import. There is no mapping between SoftMar and Baltic Index. SoftMar Code:" +
                                   indexCodeStr + ", RowNumber: " +
                                   rowNumber;
                    return null;
                }

                Index fmIndex = configurationContext.Indexes.SingleOrDefault(a => a.Id == mapping.FmIndexId);
                if (fmIndex == null)
                {
                    SiAuto.Main.LogError("SoftMar code "+ mapping.InterfaceIndexCode +" maps to FmIndexId: " + mapping.FmIndexId +
                                         " but this index id does not exist in the database. RowNumber: " +
                                         rowNumber);
                    errorMessage = "There was an error during import. SoftMar code " + mapping.InterfaceIndexCode + " maps to FmIndexId: " + mapping.FmIndexId +
                                         " but this index id does not exist in the database. RowNumber: " +
                                         rowNumber;
                    return null;
                }

                index = fmIndex;

                #endregion

                #region TradeInfo Books

                Book book = null;
                string defaultBookName = "TBA";
                if ((string.IsNullOrEmpty(bookStr) && (!memConfigData.ContainsKey(typeof(Book)) || !memConfigData[typeof(Book)].ContainsKey(defaultBookName.ToUpper())))
                    || !memConfigData.ContainsKey(typeof(Book)) || (!string.IsNullOrEmpty(bookStr) && !memConfigData[typeof(Book)].ContainsKey(bookStr.ToUpper())))
                {
                    book = string.IsNullOrEmpty(bookStr)
                               ? configurationContext.Books.SingleOrDefault(a => a.Name.ToUpper() == defaultBookName.ToUpper())
                               : configurationContext.Books.SingleOrDefault(a => a.Name.ToUpper() == bookStr.ToUpper());

                    if (book == null)
                    {
                        book = new Book()
                        {
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName,
                            Id = configurationContext.GetNextId(typeof(Book)),
                            Name = string.IsNullOrEmpty(bookStr) ? defaultBookName : bookStr,
                            Status = ActivationStatusEnum.Active,
                            PositionCalculatedByDays = BookPositionCalculatedByDaysEnum.Quantity
                        };
                        configurationContext.Books.InsertOnSubmit(book);

                        if (!memConfigData.ContainsKey(typeof(Book)))
                            memConfigData.Add(typeof(Book), new Dictionary<string, long>());
                        memConfigData[typeof(Book)][book.Name.ToUpper()] = book.Id;
                    }
                    else
                    {
                        if (!memConfigData.ContainsKey(typeof(Book)))
                            memConfigData.Add(typeof(Book), new Dictionary<string, long>());
                        memConfigData[typeof(Book)][book.Name.ToUpper()] = book.Id;
                    }
                }
                else
                {
                    book = string.IsNullOrEmpty(bookStr)
                               ? configurationContext.Books.SingleOrDefault(a => a.Name.ToUpper() == defaultBookName.ToUpper())
                               : configurationContext.Books.SingleOrDefault(a => a.Name.ToUpper() == bookStr.ToUpper());
                }

                #region Make sub-book from company

                Book companyBook = null;

                //string companyBookName = companyStr + "-" + bookStr;
                string companyBookName = companyStr + "-" + book.Name.ToUpper();

                if ((!string.IsNullOrEmpty(companyStr) && !memConfigData[typeof(Book)].ContainsKey(companyBookName.ToUpper())))
                {
                    companyBook =
                        configurationContext.Books.SingleOrDefault(a => a.Name.ToUpper() == companyBookName.ToUpper());

                    if (companyBook == null)
                    {
                        companyBook = new Book()
                        {
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName,
                            Id = configurationContext.GetNextId(typeof (Book)),
                            Name = companyBookName,
                            ParentBookId = memConfigData[typeof (Book)][book.Name.ToUpper()],
                            Status = ActivationStatusEnum.Active,
                            PositionCalculatedByDays = BookPositionCalculatedByDaysEnum.Quantity
                        };
                        configurationContext.Books.InsertOnSubmit(companyBook);

                        if (!memConfigData.ContainsKey(typeof (Book)))
                            memConfigData.Add(typeof (Book), new Dictionary<string, long>());
                        memConfigData[typeof (Book)][companyBook.Name.ToUpper()] = companyBook.Id;
                    }
                    else
                    {
                        if (!memConfigData.ContainsKey(typeof (Book)))
                            memConfigData.Add(typeof (Book), new Dictionary<string, long>());
                        memConfigData[typeof (Book)][companyBook.Name.ToUpper()] = companyBook.Id;
                    }

                }

                #endregion

                #endregion

                #region Trade Info Trader

                Trader trader = null;
                string defaultTraderName = "Unknown";
                if ((string.IsNullOrEmpty(traderStr) && (!memConfigData.ContainsKey(typeof(Trader)) || !memConfigData[typeof(Trader)].ContainsKey(defaultTraderName.ToUpper())))
                    || !memConfigData.ContainsKey(typeof(Trader)) || (!string.IsNullOrEmpty(traderStr) && !memConfigData[typeof(Trader)].ContainsKey(traderStr.ToUpper())))
                {
                    trader = string.IsNullOrEmpty(traderStr)
                                 ? configurationContext.Traders.FirstOrDefault(
                                     a => a.Name.ToUpper() == defaultTraderName.ToUpper())
                                 : configurationContext.Traders.FirstOrDefault(
                                     a => a.Name.ToUpper() == traderStr.ToUpper());
                    if (trader == null)
                    {
                        trader = new Trader()
                        {
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName,
                            Id = configurationContext.GetNextId(typeof(Trader)),
                            Name = string.IsNullOrEmpty(traderStr) ? defaultTraderName : traderStr,
                            Status = ActivationStatusEnum.Active,
                            TradeTypes = tradeTypeEnum.ToString("d")
                        };
                        configurationContext.Traders.InsertOnSubmit(trader);

                        var traderCompany = new TraderCompanyAssoc()
                        {
                            CompanyId = memConfigData[typeof(Company)][companyStr.ToUpper()],
                            Id = configurationContext.GetNextId(typeof(TraderCompanyAssoc)),
                            TraderId = trader.Id
                        };
                        configurationContext.TraderCompanyAssocs.InsertOnSubmit(traderCompany);

                        var traderMarket = new TraderMarketAssoc()
                        {
                            Id = configurationContext.GetNextId(typeof(TraderMarketAssoc)),
                            MarketId = index.MarketId,
                            TraderId = trader.Id
                        };
                        configurationContext.TraderMarketAssocs.InsertOnSubmit(traderMarket);

                        #region Trader Book
                        long bookId = string.IsNullOrEmpty(bookStr)
                                          ? memConfigData[typeof(Book)][defaultBookName.ToUpper()]
                                          : memConfigData[typeof(Book)][bookStr.ToUpper()];
                        var traderBook = new TraderBook()
                        {
                            BookId = bookId,
                            Id = configurationContext.GetNextId(typeof(TraderBook)),
                            TraderId = trader.Id
                        };
                        configurationContext.TraderBooks.InsertOnSubmit(traderBook);
                        if (book != null && book.ParentBookId != null)
                        {
                            string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                         book.ParentBookId;

                            List<long> parentBookIds = configurationContext.Query<long>(sql).ToList();
                            foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                            {
                                var newTraderBook = new TraderBook()
                                {
                                    BookId = parentBookId,
                                    Id = configurationContext.GetNextId(typeof(TraderBook)),
                                    TraderId = trader.Id
                                };
                                configurationContext.TraderBooks.InsertOnSubmit(newTraderBook);
                            }
                        }

                        #region sub-book

                        var subBookId = memConfigData[typeof(Book)][companyBookName.ToUpper()];

                        long subBook = memConfigData[typeof(Book)][companyBookName.ToUpper()];

                        var traderSubBook = new TraderBook()
                        {
                            BookId = subBookId,
                            Id = configurationContext.GetNextId(typeof(TraderBook)),
                            TraderId = trader.Id
                        };
                        configurationContext.TraderBooks.InsertOnSubmit(traderSubBook);

                        #endregion

                        #endregion

                        if (!memConfigData.ContainsKey(typeof(Trader)))
                            memConfigData.Add(typeof(Trader), new Dictionary<string, long>());
                        memConfigData[typeof(Trader)][trader.Name.ToUpper()] = trader.Id;
                    }
                    else//Trader exists
                    {
                        #region TraderBook
                        long bookId = string.IsNullOrEmpty(bookStr)
                                          ? memConfigData[typeof(Book)][defaultBookName.ToUpper()]
                                          : memConfigData[typeof(Book)][bookStr.ToUpper()];

                        bool hasPermissionOnBook =
                            configurationContext.TraderBooks.Any(a => a.TraderId == trader.Id && a.BookId == bookId);
                        if (!hasPermissionOnBook)
                        {
                            var traderBook = new TraderBook()
                            {
                                BookId = bookId,
                                Id = configurationContext.GetNextId(typeof(TraderBook)),
                                TraderId = trader.Id
                            };
                            configurationContext.TraderBooks.InsertOnSubmit(traderBook);
                        }
                        if (book != null && book.ParentBookId != null)
                        {
                            string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                         book.ParentBookId;

                            List<long> parentBookIds = configurationContext.Query<long>(sql).ToList();
                            foreach (long parentBookId in parentBookIds.Where(a => a != book.Id))
                            {
                                hasPermissionOnBook =
                                    configurationContext.TraderBooks.Any(
                                        a => a.TraderId == trader.Id && a.BookId == parentBookId);
                                if (!hasPermissionOnBook)
                                {
                                    var newTraderBook = new TraderBook()
                                    {
                                        BookId = parentBookId,
                                        Id = configurationContext.GetNextId(typeof(TraderBook)),
                                        TraderId = trader.Id
                                    };
                                    configurationContext.TraderBooks.InsertOnSubmit(newTraderBook);
                                }

                            }
                        }

                        #region sub-book

                        var subBookId = memConfigData[typeof(Book)][companyBookName.ToUpper()];
                        
                        hasPermissionOnBook =
                            configurationContext.TraderBooks.Any(a => a.TraderId == trader.Id && a.BookId == subBookId);
                        if (!hasPermissionOnBook)
                        {
                            long subBook = memConfigData[typeof (Book)][companyBookName.ToUpper()];

                            var traderSubBook = new TraderBook()
                            {
                                BookId = subBookId,
                                Id = configurationContext.GetNextId(typeof (TraderBook)),
                                TraderId = trader.Id
                            };
                            configurationContext.TraderBooks.InsertOnSubmit(traderSubBook);
                        }

                        #endregion

                        #endregion

                        #region TraderCompany

                        long companyId = memConfigData[typeof(Company)][companyStr.ToUpper()];

                        bool hasAssocToCompany =
                            configurationContext.TraderCompanyAssocs.Any(a => a.TraderId == trader.Id && a.CompanyId == companyId);
                        if (!hasAssocToCompany)
                        {
                            var traderCompanyAssoc = new TraderCompanyAssoc()
                            {
                                CompanyId = companyId,
                                Id = configurationContext.GetNextId(typeof(TraderCompanyAssoc)),
                                TraderId = trader.Id
                            };
                            configurationContext.TraderCompanyAssocs.InsertOnSubmit(traderCompanyAssoc);
                        }
                        #endregion

                        #region TraderMarket

                        long marketId = index.MarketId;

                        bool hasAssocToMarket =
                            configurationContext.TraderMarketAssocs.Any(a => a.TraderId == trader.Id && a.MarketId == marketId);
                        if (!hasAssocToMarket)
                        {
                            var traderMarketAssoc = new TraderMarketAssoc()
                            {
                                MarketId = marketId,
                                Id = configurationContext.GetNextId(typeof(TraderCompanyAssoc)),
                                TraderId = trader.Id
                            };
                            configurationContext.TraderMarketAssocs.InsertOnSubmit(traderMarketAssoc);
                        }
                        #endregion

                        if (!memConfigData.ContainsKey(typeof(Trader)))
                            memConfigData.Add(typeof(Trader), new Dictionary<string, long>());
                        memConfigData[typeof(Trader)][trader.Name.ToUpper()] = trader.Id;
                    }
                }
                else
                {
                    long traderId = string.IsNullOrEmpty(traderStr)
                                      ? memConfigData[typeof(Trader)][defaultTraderName.ToUpper()]
                                      : memConfigData[typeof(Trader)][traderStr.ToUpper()];
                    #region TraderBook
                    long bookId = string.IsNullOrEmpty(bookStr)
                                      ? memConfigData[typeof(Book)][defaultBookName.ToUpper()]
                                      : memConfigData[typeof(Book)][bookStr.ToUpper()];

                    bool hasPermissionOnBook =
                        configurationContext.TraderBooks.Any(a => a.TraderId == traderId && a.BookId == bookId);
                    if (!hasPermissionOnBook)
                    {
                        var traderBook = new TraderBook()
                        {
                            BookId = bookId,
                            Id = configurationContext.GetNextId(typeof(TraderBook)),
                            TraderId = traderId
                        };
                        configurationContext.TraderBooks.InsertOnSubmit(traderBook);

                        Book currentBook = configurationContext.Books.SingleOrDefault(a => a.Id == bookId);
                        if (currentBook != null && currentBook.ParentBookId != null)
                        {
                            string sql = @"select t.id from books t connect by t.id =  prior t.parent_book_id start with t.id = " +
                                         currentBook.ParentBookId;

                            List<long> parentBookIds = configurationContext.Query<long>(sql).ToList();
                            foreach (long parentBookId in parentBookIds.Where(a => a != currentBook.Id))
                            {
                                hasPermissionOnBook =
                                    configurationContext.TraderBooks.Any(
                                        a => a.TraderId == traderId && a.BookId == parentBookId);
                                if (!hasPermissionOnBook)
                                {
                                    var newTraderBook = new TraderBook()
                                    {
                                        BookId = parentBookId,
                                        Id = configurationContext.GetNextId(typeof(TraderBook)),
                                        TraderId = traderId
                                    };
                                    configurationContext.TraderBooks.InsertOnSubmit(newTraderBook);
                                }

                            }
                        }
                    }
                    #endregion

                    #region TraderCompany

                    long companyId = memConfigData[typeof(Company)][companyStr.ToUpper()];

                    bool hasAssocToCompany =
                        configurationContext.TraderCompanyAssocs.Any(a => a.TraderId == traderId && a.CompanyId == companyId);
                    if (!hasAssocToCompany)
                    {
                        var traderCompanyAssoc = new TraderCompanyAssoc()
                        {
                            CompanyId = companyId,
                            Id = configurationContext.GetNextId(typeof(TraderCompanyAssoc)),
                            TraderId = traderId
                        };
                        configurationContext.TraderCompanyAssocs.InsertOnSubmit(traderCompanyAssoc);
                    }
                    #endregion

                    #region TraderMarket

                    long marketId = index.MarketId;

                    bool hasAssocToMarket =
                        configurationContext.TraderMarketAssocs.Any(a => a.TraderId == traderId && a.MarketId == marketId);
                    if (!hasAssocToMarket)
                    {
                        var traderMarketAssoc = new TraderMarketAssoc()
                        {
                            MarketId = marketId,
                            Id = configurationContext.GetNextId(typeof(TraderCompanyAssoc)),
                            TraderId = traderId
                        };
                        configurationContext.TraderMarketAssocs.InsertOnSubmit(traderMarketAssoc);
                    }
                    #endregion
                }

                #endregion

                #region TradeInfo Brokers

                Company broker = null;
                if (brokers.Count == 0 && tradeTypeEnum != TradeTypeEnum.Cargo)
                {
                    SiAuto.Main.LogError("Field 'Broker' does not have a value, RowNumber: " + rowNumber);
                    errorMessage = "There was an error during import. Field 'Broker' does not have a value, RowNumber: " + rowNumber;
                    return null;
                }
                foreach (string brokerStr in brokers)
                {
                    if (!string.IsNullOrEmpty(brokerStr) && tradeTypeEnum != TradeTypeEnum.Cargo && (!memConfigData.ContainsKey(typeof(Company)) || !memConfigData[typeof(Company)].ContainsKey(brokerStr.ToUpper())))
                    {
                        broker = (from objCompany in configurationContext.Companies
                                  from objCompanySubtype in configurationContext.CompanySubtypes
                                  where
                                      objCompany.SubtypeId == objCompanySubtype.Id
                                      && objCompanySubtype.Subtype == CompanySubtypeEnum.Broker
                                      && objCompany.Name.ToUpper() == brokerStr.ToUpper()
                                  select objCompany).SingleOrDefault();

                        CompanySubtype brokerSubType =
                            configurationContext.CompanySubtypes.FirstOrDefault(a => a.Subtype == CompanySubtypeEnum.Broker);
                        if (brokerSubType == null)
                        {
                            SiAuto.Main.LogError(
                                "There is no CompanySubType with subType Broker in the system. RowNumber: " + rowNumber);
                            errorMessage = "There was an error during import. There is no CompanySubType with subType Broker in the system. RowNumber: " +
                                           rowNumber;
                            return null;
                        }

                        if (broker == null)
                        {
                            broker = new Company()
                            {
                                Chd = runDate,
                                Chuser = importUserName,
                                Crd = runDate,
                                Cruser = importUserName,
                                Id = configurationContext.GetNextId(typeof(Company)),
                                IsCounterParty = false,//TODO
                                ManagerId = null,
                                Name = brokerStr,
                                Ownership = null,
                                Type = CompanyTypeEnum.Internal,//TODO
                                ParentId = null,
                                RiskRating = 1,
                                Status = ActivationStatusEnum.Active,
                                SubtypeId = brokerSubType.Id
                            };
                            configurationContext.Companies.InsertOnSubmit(broker);

                            if (!memConfigData.ContainsKey(typeof(Company)))
                                memConfigData.Add(typeof(Company), new Dictionary<string, long>());
                            memConfigData[typeof(Company)][brokerStr.ToUpper()] = broker.Id;
                        }
                        else
                        {
                            if (!memConfigData.ContainsKey(typeof(Company)))
                                memConfigData.Add(typeof(Company), new Dictionary<string, long>());
                            memConfigData[typeof(Company)][brokerStr.ToUpper()] = broker.Id;
                        }
                    }
                }

                #endregion

                #region Vessel

                Vessel vessel = null;
                string defaultVesselName = "TBN";
                if ((string.IsNullOrEmpty(vesselStr) && (!memConfigData.ContainsKey(typeof(Vessel)) || !memConfigData[typeof(Vessel)].ContainsKey(defaultVesselName.ToUpper())))
                    || !memConfigData.ContainsKey(typeof(Vessel)) || (!string.IsNullOrEmpty(vesselStr) && !memConfigData[typeof(Vessel)].ContainsKey(vesselStr.ToUpper())))
                {
                    vessel = string.IsNullOrEmpty(vesselStr)
                                 ? configurationContext.Vessels.SingleOrDefault(a => a.Name.ToUpper() == defaultVesselName.ToUpper())
                                 : configurationContext.Vessels.SingleOrDefault(a => a.Name.ToUpper() == vesselStr.ToUpper());

                    var year = 1900;
                    if (!string.IsNullOrEmpty(yearBuilt))
                        year = Convert.ToInt32(yearBuilt);

                    if (vessel == null)
                    {
                        vessel = new Vessel()
                        {
                            Id = configurationContext.GetNextId(typeof(Vessel)),
                            Name = string.IsNullOrEmpty(vesselStr) ? defaultVesselName : vesselStr,
                            AcquisitionPrice = 1,
                            BenchAgeFrequency = 1,
                            BenchAgePoints = 0,
                            BenchInitial = 100,
                            Benchmark = 0,
                            CompanyId = tradeTypeEnum == TradeTypeEnum.TC ? memConfigData[typeof(Company)][counterpartyStr.ToUpper()] : memConfigData[typeof(Company)][companyStr.ToUpper()],
                            DateBuilt = new DateTime(year, 1, 1),
                            ImoNumber = "0000000",
                            MarketId = index.MarketId,
                            MarketPrice = 1,
                            Status = ActivationStatusEnum.Active,
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName
                        };
                        configurationContext.Vessels.InsertOnSubmit(vessel);

                        var vesselInfo = new VesselInfo()
                        {
                            Beam = null,
                            ChiefOfficerId = null,
                            Classification = null,
                            CommercialFee = null,//TODO
                            ContactId = null,
                            DeadWeight = 0,
                            DeadWeightSummer = 0,
                            DeadWeightTropic = 0,
                            DeadWeightWinter = 0,
                            DeliveryDate = null,
                            DieselOilBallastSea = null,
                            DieselOilLadenSea = null,
                            DieselOilPort = null,
                            DraftSummer = 0,
                            DraftTropic = 0,
                            DraftWinter = 0,
                            Equity = null,
                            Flag = null,
                            FuelOilBallastSea = null,
                            FuelOilLadenSea = null,
                            FuelOilPort = null,
                            GrainCapacity = 0,
                            Id = configurationContext.GetNextId(typeof(VesselInfo)),
                            LightWeight = 0,
                            LOA = null,
                            ManagementFee = null,//TODO
                            MasterOfficerId = null,
                            MoaDate = null,
                            PiClub = null,
                            SaleAmount = 0,
                            SaleDate = vessel.DateBuilt.AddYears(25),
                            TechnicalManagerId = null,
                            Tpc = null,
                            UsefulLife = 25,
                            VesselId = vessel.Id,
                            Yard = null
                        };
                        configurationContext.VesselInfos.InsertOnSubmit(vesselInfo);

                        #region Vessel Benchmarking

                        decimal benchInitial = vessel.BenchInitial;
                        int benchFrequency = vessel.BenchAgeFrequency;
                        decimal benchPoints = vessel.BenchAgePoints;
                        decimal currentBenchmark = benchInitial;
                        DateTime dateFrom = vessel.DateBuilt.Date;
                        DateTime dateTo = dateFrom.AddYears(benchFrequency).Date;

                        VesselBenchmarking newVesselBenchmarking;
                        var vesselBenchmarkings = new List<VesselBenchmarking>();

                        for (int i = 0; i < 40; i++)
                        {
                            if (dateFrom.Year > vessel.DateBuilt.AddYears(40).Year)
                                break;

                            currentBenchmark = currentBenchmark - benchPoints;
                            newVesselBenchmarking = new VesselBenchmarking()
                            {
                                Id = configurationContext.GetNextId(typeof(VesselBenchmarking)),
                                PeriodFrom = dateFrom,
                                PeriodTo = dateTo,
                                Benchmark = currentBenchmark,
                                VesselId = vessel.Id
                            };
                            vesselBenchmarkings.Add(newVesselBenchmarking);

                            dateFrom = dateTo.Date;
                            dateTo = dateTo.AddYears(benchFrequency).Date;

                            configurationContext.VesselBenchmarkings.InsertOnSubmit(newVesselBenchmarking);
                        }

                        #endregion

                        #region Vessel Expenses

                        var expenses = new List<VesselExpense>();
                        for (int j = 1; j < 31; j++)
                        {
                            expenses.Add(new VesselExpense()
                            {
                                Id = configurationContext.GetNextId(typeof(VesselExpense)),
                                Age = j,
                                DepreciationProfile = 0,
                                DryDockExpenses = 0,
                                DryDockOffHire = 0,
                                OperationalExpenses = 0,
                                VesselId = vessel.Id
                            });
                        }
                        configurationContext.VesselExpenses.InsertAllOnSubmit(expenses);

                        #endregion

                        if (!memConfigData.ContainsKey(typeof(Vessel)))
                            memConfigData.Add(typeof(Vessel), new Dictionary<string, long>());
                        memConfigData[typeof(Vessel)][vessel.Name.ToUpper()] = vessel.Id;
                    }
                    else
                    {
                        if (!memConfigData.ContainsKey(typeof(Vessel)))
                            memConfigData.Add(typeof(Vessel), new Dictionary<string, long>());
                        memConfigData[typeof(Vessel)][vessel.Name.ToUpper()] = vessel.Id;
                    }
                }

                #endregion

                #region TradeFFAOptionInfo Clearing House

                if (!string.IsNullOrEmpty(clearingHouseStr) && (!memConfigData.ContainsKey(typeof(ClearingHouse)) || !memConfigData[typeof(ClearingHouse)].ContainsKey(clearingHouseStr.ToUpper())))
                {
                    var clearingHouse =
                        configurationContext.Clearinghouses.FirstOrDefault(a => a.Name.ToUpper() == clearingHouseStr.ToUpper());
                    if (clearingHouse == null)
                    {
                        clearingHouse = new ClearingHouse()
                        {
                            Chd = runDate,
                            Chuser = importUserName,
                            Crd = runDate,
                            Cruser = importUserName,
                            Id = configurationContext.GetNextId(typeof(ClearingHouse)),
                            Name = clearingHouseStr,
                            Status = ActivationStatusEnum.Active
                        };

                        if (!memConfigData.ContainsKey(typeof(ClearingHouse)))
                            memConfigData.Add(typeof(ClearingHouse), new Dictionary<string, long>());
                        memConfigData[typeof(ClearingHouse)][clearingHouseStr.ToUpper()] = clearingHouse.Id;
                    }
                    else
                    {
                        if (!memConfigData.ContainsKey(typeof(ClearingHouse)))
                            memConfigData.Add(typeof(ClearingHouse), new Dictionary<string, long>());
                        memConfigData[typeof(ClearingHouse)][clearingHouseStr.ToUpper()] = clearingHouse.Id;
                    }
                }

                #endregion

                return 0;
            }
            catch (Exception exc)
            {
                HandleException(userName, exc);
                return null;
            }
        }

        private CultureInfo GetCultureOfDateTimeValues(List<string> datesStr)
        {
            var greekCultureInfo = new CultureInfo("el-GR");
            var americanCultureInfo = new CultureInfo("en-US");

            foreach (string dateStr in datesStr)
            {
                try
                {
                    DateTime dateFormat = DateTime.Parse(dateStr, greekCultureInfo);
                }
                catch (FormatException exc)
                {
                    return americanCultureInfo;
                }
            }
            return greekCultureInfo;
        }

        private CultureInfo GetCultureOfNumericValues(List<string> numbersStr)
        {
            CultureInfo cultureInfo = null;
            var greekCultureInfo = new CultureInfo("el-GR");
            var americanCultureInfo = new CultureInfo("en-US");

            var nonDigitChars = new List<string>();

            foreach (string numberStr in numbersStr)
            {
                nonDigitChars.Clear();
                nonDigitChars.AddRange(from c in numberStr where !Char.IsNumber(c) select c.ToString());
                if (nonDigitChars.Count == 2)//If there are two non-digit characters, check which culture corresponds to
                {
                    string firstChar = nonDigitChars[0];
                    string secondChar = nonDigitChars[1];
                    if (firstChar == americanCultureInfo.NumberFormat.NumberGroupSeparator &&
                        secondChar == americanCultureInfo.NumberFormat.NumberDecimalSeparator)
                    {
                        cultureInfo = americanCultureInfo;
                        break;
                    }
                    cultureInfo = greekCultureInfo;
                    break;
                }
                if (nonDigitChars.Count == 1)
                {
                    int indexOfOperator = numberStr.IndexOf(nonDigitChars[0]);
                    if (indexOfOperator >= 4)//It means it is a decimal separator
                    {
                        if (nonDigitChars[0] == americanCultureInfo.NumberFormat.NumberDecimalSeparator)
                        {
                            cultureInfo = americanCultureInfo;
                            break;
                        }
                        cultureInfo = greekCultureInfo;
                        break;
                    }
                }
                if (nonDigitChars.Count == 0)
                    continue;
            }
            if (cultureInfo != null)
                return cultureInfo;

            foreach (string numberStr in numbersStr)
            {
                nonDigitChars.Clear();
                nonDigitChars.AddRange(from c in numberStr where !Char.IsNumber(c) select c.ToString());
                if (nonDigitChars.Count == 1)
                {
                    int indexOfOperator = numberStr.IndexOf(nonDigitChars[0]);
                    if (indexOfOperator < 4) //Decided that in this case the non-digit character is a decimal separator
                    {
                        if (nonDigitChars[0] == americanCultureInfo.NumberFormat.NumberDecimalSeparator)
                        {
                            cultureInfo = americanCultureInfo;
                            break;
                        }
                        cultureInfo = greekCultureInfo;
                        break;
                    }
                }
            }
            if (cultureInfo != null)
                return cultureInfo;

            return greekCultureInfo;//Return by default the greek culture if no match found
        }

        #endregion
    }
}
