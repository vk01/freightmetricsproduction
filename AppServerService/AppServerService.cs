﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using System.Threading;
using AppServer;

namespace Exis.AppServer
{
    public partial class AppServerService : ServiceBase
    {
        private AppService appService;
        private Thread workerThread;

        public AppServerService()
        {
            AppDomain.CurrentDomain.SetShadowCopyFiles();
            AppDomain.CurrentDomain.SetCachePath("Cache");
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            appService = new AppService();
            appService.Initialize();
            workerThread = new Thread(appService.Run);
            workerThread.Start();
        }

        protected override void OnStop()
        {
            appService.Stop();
            workerThread.Interrupt();
            workerThread.Join();
        }
    }

    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            ServiceProcessInstaller spi = new ServiceProcessInstaller();
            spi.Account = ServiceAccount.LocalSystem;
            ServiceInstaller si = new ServiceInstaller();
            si.ServiceName = "FreightMetricsApplicationServer";
            si.StartType = ServiceStartMode.Automatic;
            si.DisplayName = "FreightMetrics Application Server";
            Installers.AddRange(new Installer[] { spi, si });
        }
    }
}
