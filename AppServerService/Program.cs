﻿using System;
using System.IO;
using System.ServiceProcess;
using Exis.Domain;

namespace Exis.AppServer
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			// DomainObject.InitializeDataContext();
			AppDomain.CurrentDomain.SetShadowCopyFiles();
			AppDomain.CurrentDomain.SetCachePath("Cache");
			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[] 
			{ 
				new AppServerService() 
			};
			Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
			ServiceBase.Run(ServicesToRun);
		}
	}
}
