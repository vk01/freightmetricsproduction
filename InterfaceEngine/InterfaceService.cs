﻿using System;
using System.Net.Mail;
using System.ServiceModel;
using Exis.ServiceInterfaces;
using Exis.WCFExtensions;
using Gurock.SmartInspect;

namespace Exis.InterfaceEngine
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, MaxItemsInObjectGraph = Int32.MaxValue,
        ConcurrencyMode = ConcurrencyMode.Multiple,
        IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    [ErrorHandlerBehavior]
    public class InterfaceService : IInterfaceService
    {
        private readonly object smtpClientSynchObject = new object();
        public string DBConnectionString;
        public string exisIssueTrackerEmailAddress;
        public string exisSupportEmailAddress;
        public string installationEmailAddress;

        public SmtpClient smtpClient;

        #region Exception Helpers

        private void HandleException(string userName, Exception exc)
        {
            try
            {
                LogException(userName, exc);
                lock (smtpClientSynchObject)
                {
                    SendException(userName, exc);
                }
            }
            catch
            {
            }
        }

        private void LogException(string userName, Exception exc)
        {
            if (userName == null)
            {
                SiAuto.Main.LogException("Unhandled Exception", exc);
            }
            else
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                SiAuto.Si.GetSession(userName).LogException("Unhandled Exception", exc);
            }
        }

        private void SendException(string userName, Exception exc)
        {
            if (smtpClient != null)
            {
                if (!String.IsNullOrEmpty(exisSupportEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisSupportEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + (userName ?? "Main") + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }

                if (!String.IsNullOrEmpty(exisIssueTrackerEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisIssueTrackerEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + (userName ?? "Main") + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }
            }
        }

        #endregion

        #region Compiled Queries

        public static class Queries
        {
        }

        #endregion

        public void Test()
        {
            throw new NotImplementedException();
        }
    }
}