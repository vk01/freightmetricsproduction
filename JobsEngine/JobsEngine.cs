﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using Exis.Domain;
using Exis.RuleEngine;
using Exis.SessionRegistry;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;

namespace Exis.JobsEngine
{
    public static class JobsEngine
    {
        public static bool BoolReadyStopEngine;
        public static bool BoolRequestStopEngine;
        [ThreadStatic] private static DataContext dataContext;
        public static string DBConnectionString;
        public static string exisIssueTrackerEmailAddress;
        public static string exisSupportEmailAddress;
        public static string installationEmailAddress;
        public static SmtpClient smtpClient;
        private static object smtpClientSynchObject = new object();

        public static void Run()
        {
            BoolRequestStopEngine = false;

            while (!BoolRequestStopEngine)
            {
                HandleJobs();

                BoolReadyStopEngine = true;

                if (!BoolRequestStopEngine)
                {
                    Thread.Sleep(1000*60);
                }
            }
        }

        public static void HandleJobs()
        {
            try
            {
                dataContext = new DataContext(DBConnectionString);
                if (SiAuto.Si.Level == Level.Debug) dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Main) { TitleLimit = 0, TitlePrefix = "" };

                List<Job> jobs =
                    dataContext.Jobs.Where(a => a.Status_Str == ActivationStatusEnum.Active.ToString("d")).ToList();
                SystemData.context = dataContext;
                SiAuto.Main.LogDebug("Checking Jobs...");
                foreach (Job job in jobs)
                {
                    job.context = dataContext;
                    SystemData.context = dataContext;
                    SiAuto.Main.LogDebug("Checking Job with Id:" + job.Id);
                    Condition condition =
                        dataContext.Conditions.Where(a => a.Id == job.ConditionId).Single();
                    condition.context = dataContext;
                    bool blResult = ConditionEvaluator.ExecuteCondition(condition, job);
                    if (!blResult)
                    {
                        SiAuto.Main.LogDebug("Condition with Id:" + job.ConditionId + " for Job with Id:" + job.Id +
                                             " returned FALSE");
                        continue;
                    }
                    SiAuto.Main.LogDebug("Condition with Id:" + job.ConditionId + " for Job with Id:" + job.Id +
                                             " returned TRUE");
                    var currentDate = DateTime.Now;

                    WorkflowDiagrVersion wdv = (from objWorkflowVersion in dataContext.WorkflowDiagrVersions
                                                join objDiagram in dataContext.WorkflowDiagrams on
                                                    objWorkflowVersion.WorkflowDiagramId
                                                    equals objDiagram.Id
                                                where
                                                    (objWorkflowVersion.DateTo == null ||
                                                     objWorkflowVersion.DateTo > currentDate) &&
                                                    objWorkflowVersion.DateFrom <= currentDate &&
                                                    objWorkflowVersion.Status_Str ==
                                                    WorkflowDiagramVersionTypeEnum.Production.ToString("d") &&
                                                    objDiagram.CaseTypeId == job.CaseTypeId
                                                select objWorkflowVersion).SingleOrDefault();

                    CaseType caseType = dataContext.CaseTypes.Where(a => a.Id == job.CaseTypeId).Single();

                    if (wdv == null)
                    {
                        var myexc =
                            new ApplicationException(
                                "Failed to find a valid Workflow Diagram Version for the associated Case Type:" +
                                job.CaseTypeId);
                        HandleException(null, myexc);
                        return;
                    }

                    SiAuto.Main.LogDebug("Found a valid Workflow Diagram Version with Id:" + wdv.Id);

                    var objCase = new Case
                                      {
                                          Id = dataContext.GetNextId(typeof (Case)),
                                          CustomerId = job.CustomerId,
                                          CaseTypeId = job.CaseTypeId,
                                          ActStatus = CaseActionStatusEnum.Opening,
                                          WorkflowDiagramVersionId = wdv.Id,
                                          Status = CaseEventStatusEnum.Opened,
                                          Priority = caseType.Priority,
                                          Cruser = "JobsEngine",
                                          Chuser = "JobsEngine",
                                          Chappuser = null,
                                          Crappuser = null,
                                          Chd = currentDate,
                                          Crd = currentDate,
                                          Chappd = currentDate,
                                          Crappd = currentDate,
                                          DateFrom = currentDate
                                      };
                    objCase.context = dataContext;
                    dataContext.Cases.InsertOnSubmit(objCase);

                    if (!String.IsNullOrEmpty(job.CaseUdfValues))
                    {
                        string[] udfArray1 = job.CaseUdfValues.Split(new[] {"&sep;"},
                                                                     StringSplitOptions.RemoveEmptyEntries);

                        foreach (string str1 in udfArray1)
                        {
                            string strUdfId = str1.Substring(0, str1.IndexOf('='));
                            string strUdfValue = str1.Substring(str1.IndexOf('=') + 1);

                            Udf udf = objCase.Udfs.Where(a => a.Id.ToString() == strUdfId).SingleOrDefault();
                            if (udf == null)
                            {
                                var myexc =
                                    new ApplicationException(
                                        "Failed to find Udf with Id:" + strUdfId);
                                HandleException(null, myexc);
                                return;
                            }

                            UdfsValue newUdfValue = new UdfsValue
                                                        {
                                                            Id = dataContext.GetNextId(typeof (UdfsValue)),
                                                            UdfId = udf.Id,
                                                            RefId = objCase.Id,
                                                            Value = strUdfValue,
                                                            Chappuser = ServerSessionRegistry.User,
                                                            Chappd = currentDate
                                                        };
                            newUdfValue.context = dataContext;
                            dataContext.UdfsValues.InsertOnSubmit(newUdfValue);
                        }
                    }
                    dataContext.SubmitChanges();
                    SiAuto.Main.LogDebug("Case with Id:" + objCase.Id + " created for Customer with Id:" + objCase.CustomerId);
                }
            }
            catch (Exception exc)
            {
                HandleException(null, exc);
            }
            finally
            {
                if (dataContext != null) dataContext.Dispose();
            }
        }

        #region Handle Exceptions

        private static void HandleException(string userName, Exception exc)
        {
            try
            {
                LogException(userName, exc);
                lock (smtpClientSynchObject)
                {
                    SendException(userName, exc);
                }
            }
            catch
            {

            }
        }

        private static void LogException(string userName, Exception exc)
        {
            if (userName == null)
            {
                SiAuto.Main.LogException("Unhandled Exception", exc);
            }
            else
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                SiAuto.Si.GetSession(userName).LogException("Unhandled Exception", exc);
            }
        }

        private static void SendException(string userName, Exception exc)
        {
            if (smtpClient != null)
            {
                if (!String.IsNullOrEmpty(exisSupportEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisSupportEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + (userName ?? "Main") + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }

                if (!String.IsNullOrEmpty(exisIssueTrackerEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisIssueTrackerEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + (userName ?? "Main") + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }
            }
        }

        #endregion
    }
}