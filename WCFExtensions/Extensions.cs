﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Security;
using Gurock.SmartInspect;

namespace Exis.WCFExtensions
{
    public abstract class HeaderClientBase<T, H> : InterceptorClientBase<T> where T : class
    {
        public H Header
        { get; protected set; }

        public HeaderClientBase()
            : this(default(H))
        { }

        public HeaderClientBase(string endpointName)
            : this(default(H), endpointName)
        { }

        public HeaderClientBase(string endpointName, string remoteAddress)
            : this(default(H), endpointName, remoteAddress)
        { }

        public HeaderClientBase(string endpointName, EndpointAddress remoteAddress)
            : this(default(H), endpointName, remoteAddress)
        { }

        public HeaderClientBase(Binding binding, EndpointAddress remoteAddress)
            : this(default(H), binding, remoteAddress)
        { }

        public HeaderClientBase(H header)
        {
            Header = header;
        }

        public HeaderClientBase(H header, string endpointName)
            : base(endpointName)
        {
            Header = header;
        }

        public HeaderClientBase(H header, string endpointName, string remoteAddress)
            : base(endpointName, remoteAddress)
        {
            Header = header;
        }

        public HeaderClientBase(H header, string endpointName, EndpointAddress remoteAddress)
            : base(endpointName, remoteAddress)
        {
            Header = header;
        }

        public HeaderClientBase(H header, Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
            Header = header;
        }
        protected override void PreInvoke(ref Message request)
        {
            GenericContext<H> context = new GenericContext<H>(Header);
            MessageHeader<GenericContext<H>> genericHeader = new MessageHeader<GenericContext<H>>(context);
            request.Headers.Add(genericHeader.GetUntypedHeader(GenericContext<H>.TypeName, GenericContext<H>.TypeNamespace));
        }
    }

    public abstract class InterceptorClientBase<T> : ClientBase<T> where T : class
    {
        public InterceptorClientBase()
        {
            Endpoint.Behaviors.Add(new ClientInterceptor(this));
        }
        public InterceptorClientBase(string endpointName)
            : base(endpointName)
        {
            Endpoint.Behaviors.Add(new ClientInterceptor(this));
        }

        public InterceptorClientBase(string endpointName, string remoteAddress)
            : base(endpointName, remoteAddress)
        {
            Endpoint.Behaviors.Add(new ClientInterceptor(this));
        }
        public InterceptorClientBase(string endpointName, EndpointAddress remoteAddress)
            : base(endpointName, remoteAddress)
        {
            Endpoint.Behaviors.Add(new ClientInterceptor(this));
        }
        public InterceptorClientBase(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
            Endpoint.Behaviors.Add(new ClientInterceptor(this));
        }

        protected virtual void PreInvoke(ref Message request)
        { }

        protected virtual void PostInvoke(ref Message reply)
        { }

        class ClientInterceptor : IEndpointBehavior, IClientMessageInspector
        {
            InterceptorClientBase<T> Proxy
            { get; set; }

            internal ClientInterceptor(InterceptorClientBase<T> proxy)
            {
                Proxy = proxy;
            }

            object IClientMessageInspector.BeforeSendRequest(ref Message request, IClientChannel channel)
            {
                Proxy.PreInvoke(ref request);
                return null;
            }
            void IClientMessageInspector.AfterReceiveReply(ref Message reply, object correlationState)
            {
                Proxy.PostInvoke(ref reply);
            }

            void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
            {
                clientRuntime.MessageInspectors.Add(this);
            }

            void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
            { }
            void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
            { }
            void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
            { }
        }
    }

    [DataContract]
    public class ProgramInfo
    {
        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string HostName { get; set; }

        [DataMember]
        public string AppName { get; set; }

        [DataMember]
        public string UserName { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class ErrorHandlerBehaviorAttribute : Attribute, IErrorHandler, IServiceBehavior
    {
        protected Type ServiceType { get; set; }

        #region IErrorHandler Members

        bool IErrorHandler.HandleError(Exception exc)
        {
            string userName = GenericContext<ProgramInfo>.Current == null ||
                              GenericContext<ProgramInfo>.Current.Value == null
                                  ? null
                                  : GenericContext<ProgramInfo>.Current.Value.UserName;
            if (String.IsNullOrEmpty(userName))
            {
                SiAuto.Main.LogException("WCF Exception", exc);
            }
            else
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                SiAuto.Si.GetSession(userName).LogException("Unhandled Exception", exc);
            }
            return false;
        }

        void IErrorHandler.ProvideFault(Exception exc, MessageVersion version, ref Message fault)
        {
        }

        #endregion

        #region IServiceBehavior Members

        void IServiceBehavior.Validate(ServiceDescription description, ServiceHostBase host)
        {
        }

        void IServiceBehavior.AddBindingParameters(ServiceDescription description, ServiceHostBase host,
                                                   Collection<ServiceEndpoint> endpoints,
                                                   BindingParameterCollection parameters)
        {
        }

        void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription description, ServiceHostBase host)
        {
            ServiceType = description.ServiceType;
            foreach (ChannelDispatcher dispatcher in host.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(this);
            }
        }

        #endregion
    }

    [DataContract]
    public class GenericContext<T>
    {
        public static string TypeName;
        public static string TypeNamespace;

        [DataMember] public readonly T Value;

        static GenericContext()
        {
            TypeNamespace = "net.clr:" + typeof (T).FullName;
            TypeName = "GenericContext";
        }

        public GenericContext(T value)
        {
            Value = value;
        }

        public GenericContext()
            : this(default(T))
        {
        }

        public static GenericContext<T> Current
        {
            get
            {
                OperationContext context = OperationContext.Current;
                if (context == null)
                {
                    return null;
                }
                try
                {
                    return context.IncomingMessageHeaders.GetHeader<GenericContext<T>>(TypeName, TypeNamespace);
                }
                catch (Exception exception)
                {
                    return null;
                }
            }
            set
            {
                OperationContext context = OperationContext.Current;

                //Having multiple GenericContext<T> headers is an error
                bool headerExists = false;
                try
                {
                    context.OutgoingMessageHeaders.GetHeader<GenericContext<T>>(TypeName, TypeNamespace);
                    headerExists = true;
                }
                catch (MessageHeaderException exception)
                {
                }
                if (headerExists)
                {
                    throw new InvalidOperationException("A header with name " + TypeName + " and namespace " +
                                                        TypeNamespace + " already exists in the message.");
                }
                var genericHeader = new MessageHeader<GenericContext<T>>(value);
                context.OutgoingMessageHeaders.Add(genericHeader.GetUntypedHeader(TypeName, TypeNamespace));
            }
        }

        private static bool IsDataContract(Type type)
        {
            object[] attributes = type.GetCustomAttributes(typeof (DataContractAttribute), false);
            return attributes.Length == 1;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public abstract class ServiceInterceptorBehaviorAttribute : Attribute, IServiceBehavior
    {
        #region IServiceBehavior Members

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase host)
        {
            foreach (ServiceEndpoint endpoint in serviceDescription.Endpoints)
            {
                foreach (OperationDescription operation in endpoint.Contract.Operations)
                {
                    if (operation.Behaviors.Find<OperationInterceptorBehaviorAttribute>() != null)
                    {
                        continue;
                    }
                    operation.Behaviors.Add(CreateOperationInterceptor());
                }
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase,
                                         Collection<ServiceEndpoint> endpoints,
                                         BindingParameterCollection bindingParameters)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion

        protected abstract OperationInterceptorBehaviorAttribute CreateOperationInterceptor();
    }

    [AttributeUsage(AttributeTargets.Method)]
    public abstract class OperationInterceptorBehaviorAttribute : Attribute, IOperationBehavior
    {
        #region IOperationBehavior Members

        public void AddBindingParameters(OperationDescription operationDescription,
                                         BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            IOperationInvoker oldInvoker = dispatchOperation.Invoker;
            dispatchOperation.Invoker = CreateInvoker(oldInvoker);
        }

        public void Validate(OperationDescription operationDescription)
        {
        }

        #endregion

        protected abstract GenericInvoker CreateInvoker(IOperationInvoker oldInvoker);
    }

    public abstract class GenericInvoker : IOperationInvoker
    {
        private readonly IOperationInvoker m_OldInvoker;

        public GenericInvoker(IOperationInvoker oldInvoker)
        {
            m_OldInvoker = oldInvoker;
        }

        #region IOperationInvoker Members

        public virtual object[] AllocateInputs()
        {
            return m_OldInvoker.AllocateInputs();
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            PreInvoke(instance, inputs);
            object returnedValue = null;
            var outputParams = new object[] {};
            Exception exception = null;
            try
            {
                returnedValue = m_OldInvoker.Invoke(instance, inputs, out outputParams);
                outputs = outputParams;
                return returnedValue;
            }
            catch (Exception operationException)
            {
                exception = operationException;
                throw;
            }
            finally
            {
                PostInvoke(instance, returnedValue, outputParams, exception);
            }
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            PreInvoke(instance, inputs);
            return m_OldInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            object returnedValue = null;
            object[] outputParams = {};
            Exception exception = null;

            try
            {
                returnedValue = m_OldInvoker.InvokeEnd(instance, out outputs, result);
                outputs = outputParams;
                return returnedValue;
            }
            catch (Exception operationException)
            {
                exception = operationException;
                throw;
            }
            finally
            {
                PostInvoke(instance, returnedValue, outputParams, exception);
            }
        }

        public bool IsSynchronous
        {
            get { return m_OldInvoker.IsSynchronous; }
        }

        #endregion

        /// <summary>
        /// Exceptions here will abort the call
        /// </summary>
        /// <returns></returns>
        protected virtual void PreInvoke(object instance, object[] inputs)
        {
        }

        /// <summary>
        /// Always called, even if operation had an exception
        /// </summary>
        /// <returns></returns>
        protected virtual void PostInvoke(object instance, object returnedValue, object[] outputs, Exception exception)
        {
        }
    }

    public class ServiceParameterTracerAttribute : ServiceInterceptorBehaviorAttribute
    {
        protected override OperationInterceptorBehaviorAttribute
            CreateOperationInterceptor()
        {
            return new OperationParameterTracerAttribute();
        }
    }

    public class OperationParameterTracerAttribute :
        OperationInterceptorBehaviorAttribute
    {
        protected override GenericInvoker CreateInvoker(IOperationInvoker oldInvoker)
        {
            return new ParameterTracerInvoker(oldInvoker);
        }
    }

    internal class ParameterTracerInvoker : GenericInvoker
    {
        public ParameterTracerInvoker(IOperationInvoker oldInvoker)
            : base(oldInvoker)
        {
        }

        protected override void PreInvoke(object instance, object[] inputs)
        {
        }

        protected override void PostInvoke(object instance, object returnedValue,
                                           object[] outputs, Exception exception)
        {
        }
    }

    public class MessageInspector : IDispatchMessageInspector
    {
        #region IDispatchMessageInspector Members

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            return DateTime.Now;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            /*TimeSpan duration;
            try
            {
                DateTime start = (DateTime)correlationState;
                DateTime end = DateTime.Now;
                duration = end - start;
            }
            catch (Exception exc)
            {
                SiAuto.Main.LogException(new ApplicationException("Failed to calculate duration.", exc));
                return;
            }
            

            if (duration > TimeSpan.FromSeconds(5))
                SiAuto.Main.LogWarning(reply.Headers.Action.Substring(reply.Headers.Action.LastIndexOf("/") + 1) + " :" +
                                       duration.TotalSeconds + " seconds.");

            string strMessage = reply.ToString();
            if (String.IsNullOrEmpty(strMessage))
            {
                SiAuto.Main.LogException(new ApplicationException("Failed to get Message string."));
                return;
            }
            if (strMessage.Length > 100000)
                SiAuto.Main.LogWarning(reply.Headers.Action.Substring(reply.Headers.Action.LastIndexOf("/") + 1) + " :" +
                                       strMessage.Length + " chars.");
            return;*/
            TimeSpan duration;
            try
            {
                DateTime start = (DateTime)correlationState;
                DateTime end = DateTime.Now;
                duration = end - start;
            }
            catch (Exception exc)
            {
                SiAuto.Main.LogException(new ApplicationException("Failed to calculate duration.", exc));
                return;
            }


            if (duration > TimeSpan.FromSeconds(5))
            {
                if (reply != null)
                {
                    SiAuto.Main.LogWarning(reply.Headers.Action.Substring(reply.Headers.Action.LastIndexOf("/") + 1) +
                                           " :" +
                                           duration.TotalSeconds + " seconds.");
                }
                else
                {
                    SiAuto.Main.LogWarning("Action (one-way) duration:" +
                                           duration.TotalSeconds + " seconds.");
                }
            }

            if (reply != null)
            {
                string strMessage = reply.ToString();
                if (String.IsNullOrEmpty(strMessage))
                {
                    SiAuto.Main.LogException(new ApplicationException("Failed to get Message string."));
                    return;
                }
                if (strMessage.Length > 100000)

                    SiAuto.Main.LogWarning(reply.Headers.Action.Substring(reply.Headers.Action.LastIndexOf("/") + 1) +
                                           " :" +
                                           strMessage.Length + " chars.");
            }
            return;
        }

        #endregion
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MessageInspectorBehavior : Attribute, IServiceBehavior
    {
        #region IServiceBehavior Members

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase,
                                         Collection<ServiceEndpoint> endpoints,
                                         BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher chDisp in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher epDisp in chDisp.Endpoints)
                {
                    epDisp.DispatchRuntime.MessageInspectors.Add(new MessageInspector());
                }
            }
        }

        #endregion
    }

    public class MyServiceAuthorizationManager : ServiceAuthorizationManager
    {
        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            try
            {
                // ProgramInfo programInfo = GenericContext<ProgramInfo>.Current.Value;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

}