﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exis.Domain;
using Exis.Domain.Attributes;
using Gurock.SmartInspect;

namespace Exis.WorkflowSystemFunctions
{
    public static class SystemFunctions
    {
        #region GENERAL

        [FunctionDescription(
            @"Copies a value from one field to another. It can also be used to copy a constant value to a field.")]
        public static SystemFunctionReturnValueEnum CopyValue(ref Event objEvent,
                                                              [FunctionDescription(
                                                                  "The source field or constant value."
                                                                  )] Object inValue,
                                                              [FunctionDescription(
                                                                  "The destination field")] out Object outValue
            )
        {
            outValue = inValue;
            return SystemFunctionReturnValueEnum.Success;
        }

        [FunctionDescription(
            @"Waits until all open subcases are closed.")]
        public static SystemFunctionReturnValueEnum WaitForSubCasesCompletion(ref Event objEvent)
        {
            DataContext dataContext = objEvent.context;
            long CurrentCaseId = objEvent.CaseId;
            if (
                dataContext.Cases.Where(
                    a => a.ParentCaseId == CurrentCaseId && a.Status_Str == CaseEventStatusEnum.Opened.ToString("d")).ToList().
                    Count == 0)
                return SystemFunctionReturnValueEnum.Success;
            return SystemFunctionReturnValueEnum.NoResult;
        }

        [FunctionDescription(
            @"Appends or prepends a text on another text using a separator and returns the resulting text.")]
        public static SystemFunctionReturnValueEnum AppendText(ref Event objEvent,
                                                               [FunctionDescription("The original text.")] string
                                                                   originalValue,
                                                               [FunctionDescription(
                                                                   "The text that should be appended or prepended.")] string appendedValue,
                                                               [FunctionDescription(
                                                                   "The append or prepend switch. True if append, false if prepend"
                                                                   )] bool appendOrPrepend,
                                                               [FunctionDescription("The separator to be used")] TextAppendSeparator separator,
                                                               [FunctionDescription("The resulting text.")] out string
                                                                   returnedValue)
        {
            string strSeparator = "";
            switch (separator)
            {
                case TextAppendSeparator.Comma:
                    strSeparator = ",";
                    break;
                case TextAppendSeparator.Pipe:
                    strSeparator = "|";
                    break;
                case TextAppendSeparator.CRLF:
                    strSeparator = Environment.NewLine;
                    break;
            }

            if (appendOrPrepend)
            {
                returnedValue = originalValue + strSeparator + appendedValue;
            }
            else
            {
                returnedValue = appendedValue + strSeparator + originalValue;
            }
            return SystemFunctionReturnValueEnum.Success;
        }

        [FunctionDescription(@"Closes all the open user events of this case's subcases with the given result")]
        public static SystemFunctionReturnValueEnum CloseSubcaseEvents(ref Event objEvent,
                                                                       [FunctionDescription("The original text.")] CaseEventResultEnum result)
        {
            DataContext dataContext = objEvent.context;
            long CurrentCaseId = objEvent.CaseId;
            DateTime currentDate = DateTime.Now;

            try
            {
                List<Event> subcaseEvents = (from objCase in dataContext.Cases
                                             join curEvent in dataContext.Events on objCase.Id equals curEvent.CaseId
                                             join objEventType in dataContext.EventTypes on curEvent.EventTypeId equals
                                                 objEventType.Id
                                             where
                                                 objCase.ParentCaseId == CurrentCaseId &&
                                                 objCase.Status_Str == CaseEventStatusEnum.Opened.ToString("d") &&
                                                 curEvent.Status_Str == CaseEventStatusEnum.Opened.ToString("d") &&
                                                 curEvent.ActionStatus_Str ==
                                                 EventActionStatusEnum.OpenActionCompleted.ToString("d")
                                                 &&
                                                 objEventType.ProcessType_Str == EventTypeProcessEnum.User.ToString("d")
                                             select curEvent).ToList();

                foreach (Event subcaseEvent in subcaseEvents)
                {
                    subcaseEvent.Result_Str = result.ToString("d");
                    subcaseEvent.ActionStatus_Str = EventActionStatusEnum.MainActionCompleted.ToString("d");
                    subcaseEvent.Chuser = "WorkflowEngine";
                    subcaseEvent.Chd = currentDate;
                }
                dataContext.SubmitChanges();
                return SystemFunctionReturnValueEnum.Success;
            }
            catch (Exception e)
            {
                SiAuto.Si.GetSession(CurrentCaseId.ToString()).LogException("Exception in CloseSubcaseEvents.", e);
                return SystemFunctionReturnValueEnum.NoResult;
            }
        }

        #endregion

        #region Appointments

        [FunctionDescription(@"Closes the current active appointment of the case. Always return success")]
        public static SystemFunctionReturnValueEnum CloseAppointment(ref Event objEvent)
        {
            DataContext dataContext = objEvent.context;
            Case objCase = objEvent.Case;
            objCase.context = dataContext;
            Appointment apt = objCase.CurrentAppointment;

            if (apt != null)
            {
                apt.Datt = DateTime.Now;
                apt.Status = 1;
                apt.Label = Convert.ToInt32(AppointmentLabelEnum.Completed.ToString("d"));
            }
            return SystemFunctionReturnValueEnum.Success;
        }

        [FunctionDescription(@"Closes the current active appointment of the case. Always return success")]
        public static SystemFunctionReturnValueEnum CancelAppointment(ref Event objEvent)
        {
            DataContext dataContext = objEvent.context;
            Case objCase = objEvent.Case;
            objCase.context = dataContext;
            Appointment apt = objCase.CurrentAppointment;

            if (apt != null)
            {
                apt.Datt = DateTime.Now;
                apt.Status = 1;
                apt.Label = Convert.ToInt32(AppointmentLabelEnum.Cancelled.ToString("d"));
            }
            return SystemFunctionReturnValueEnum.Success;
        }

        #endregion

        #region Maintainance

        #endregion

        #region Jobs

        #endregion
    }
}