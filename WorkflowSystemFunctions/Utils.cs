﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Net.Mime;
using System.Text;

namespace Exis.WorkflowSystemFunctions
{
    internal static class Utils
    {
        /// <summary>
        /// Securely copies data from one stream to another using a 4K byte buffer
        /// </summary>
        /// <param name="source">The origination stream</param>
        /// <param name="target">The destination stream</param>
        internal static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }

        /// <summary>
        /// Adds a new file to a new or existing zip. If the stream does not contain any data then a new archive will be create.
        /// </summary>
        /// <param name="packageStream">The stream that contains the archive data</param>
        /// <param name="fileStream">The stream that contains the data to be added to the archive</param>
        /// <param name="fileName">The filename relative to the root of the archive (e.g. Test\test.xml)</param>
        /// <param name="contentType">The <see cref="MediaTypeNames"/> that best describes data in the new archive part.</param>
        internal static void AddStreamToZip(Stream packageStream, Stream fileStream, string fileName, string contentType)
        {
            var package = Package.Open(packageStream, FileMode.OpenOrCreate);
            var partUri = PackUriHelper.CreatePartUri(new Uri(fileName, UriKind.Relative));
            var part = package.CreatePart(partUri, contentType, CompressionOption.Normal);
            if (part != null) CopyStream(fileStream, part.GetStream());
            package.Close();
        }

        /// <summary>
        /// Produces a Luhn algorithm check digit for the given string
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        internal static string GetCheckDigit(string Code)
        {
            int sum = 0;
            for (int i = Code.Length - 1; i >= 0; i--)
            {
                int d = Code[i] - '0';
                if (i % 2 == 0)
                {
                    d = d * 2;
                    if (d > 9)
                    {
                        d = d - 9;
                    }
                }
                sum += d;
            }
            int checkdigit = ((sum / 10 + 1) * 10 - sum) % 10;
            return checkdigit.ToString();
        }
    }
}
