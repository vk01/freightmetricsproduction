﻿using System;
using System.Configuration;
using System.Threading;
using Exis.SessionRegistry;
using Gurock.SmartInspect;

namespace Exis.JobsEngineConsole
{
    public class Program
    {
        private static int Main(string[] args)
        {
            if (!Thread.GetDomain().IsDefaultAppDomain())
            {
                ServerSessionRegistry.ApplicationDirectory =
                    Thread.GetDomain().SetupInformation.ApplicationBase;
                ServerSessionRegistry.User = "JobsEngine";
                HandleArguments(args);
            }
            else
            {
                SiAuto.Si.LoadConfiguration(ServerSessionRegistry.ApplicationDirectory +
                                            @"\Logs\WorkflowEngineLogConfig.sic");
                ConfigurationManager.GetSection("DBConn");
            }

            try
            {
                Thread engineThread = new Thread(JobsEngine.JobsEngine.Run);
                engineThread.Start();
            }
            catch
            {
                return 1;
            }

            return 0;
        }

        public static void ChangeDebugLevel()
        {
            var level = (string) AppDomain.CurrentDomain.GetData("LogLevel");
            SiAuto.Main.LogMessage("Changing debug level from " + SiAuto.Si.Level + " to " + level);
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), level, true);
        }

        public static void Stop()
        {
            JobsEngine.JobsEngine.BoolRequestStopEngine = true;
            while (true)
            {
                Thread.Sleep(1000);
                if (JobsEngine.JobsEngine.BoolReadyStopEngine) break;
            }
        }

        private static void HandleArguments(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Usage: CaseGenerationEngine <Installation Name> <Connection String> <LogLevel>");
                throw new ApplicationException("Invalid count of arguments.");
            }

            string InstallationName = args[0];
            string ConnectionString = args[1];
            string LogLevel = args[2];

            Console.WriteLine("Jobs Engine Starting");
            Console.WriteLine("Arguments:");
            foreach (string s in args)
            {
                Console.WriteLine("\t- " + s);
            }
            SiAuto.Si.AppName = "Jobs Engine - " + InstallationName;

            SiAuto.Si.Connections = "file(append=\"true\", filename=\"" +
                                    ServerSessionRegistry.ApplicationDirectory + "Logs\\JobsEngineLog_" +
                                    InstallationName +
                                    ".sil\", maxsize=\"65536\", rotate=\"daily\", backlog=\"0\", caption=\"file\", flushon=\"error\", keepopen=\"true\", reconnect=\"false\")";
            SiAuto.Si.DefaultLevel = Level.Message;
            SiAuto.Si.Enabled = true;
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), LogLevel, true);

            JobsEngine.JobsEngine.DBConnectionString = ConnectionString;
        }
    }
}