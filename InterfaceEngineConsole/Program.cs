﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using System.Xml;
using Exis.InterfaceEngine;
using Exis.ServiceInterfaces;
using Exis.SessionRegistry;
using Gurock.SmartInspect;

namespace Exis.InterfaceEngineConsole
{
    public class Program
    {
        private static InterfaceService objService;
        private static ServiceHost objServiceHost;

        private static int Main(string[] args)
        {
            if (!Thread.GetDomain().IsDefaultAppDomain())
            {
                try
                {
                    ServerSessionRegistry.initialize();
                    ServerSessionRegistry.ApplicationDirectory =
                        Thread.GetDomain().SetupInformation.ApplicationBase;
                    ServerSessionRegistry.User = "InterfaceEngine";
                    HandleArguments(args);
                    objServiceHost.Open();
                }
                catch
                {
                    return 1;
                }
            }
            else
            {
                try
                {
                    objServiceHost = CreateServiceHost("9000");
                    objServiceHost.Open();
                }
                catch
                {
                    return 1;
                }
                Console.ReadLine();
                objServiceHost.Close();
            }
            return 0;
        }

        private static ServiceHost CreateServiceHost(string port)
        {
            objService = new InterfaceService();

            objService.DBConnectionString = "User Id=crm3;Password=crm3;Data Source=artemis;Pooling=true";
            
            var serviceHost = new ServiceHost(objService);


            var netTcpBinding = new NetTcpBinding(SecurityMode.None)
                                    {
                                        MaxReceivedMessageSize = Int32.MaxValue,
                                        OpenTimeout = TimeSpan.MaxValue,
                                        CloseTimeout = TimeSpan.MaxValue,
                                        ReceiveTimeout = TimeSpan.MaxValue,
                                        SendTimeout = TimeSpan.MaxValue,
                                        ListenBacklog = 1000,
                                        ReaderQuotas =
                                            new XmlDictionaryReaderQuotas { MaxStringContentLength = Int32.MaxValue, MaxArrayLength = Int32.MaxValue },
                                        PortSharingEnabled = true
                                    };
            netTcpBinding.ReliableSession.Enabled = true;
            netTcpBinding.ReliableSession.Ordered = true;
            netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromHours(11);
            serviceHost.AddServiceEndpoint(typeof (IInterfaceService), netTcpBinding,
                                           new Uri("net.tcp://localhost:" + port + "/Exis/FreightMetrics/InterfaceEngine")).Behaviors.Add(new InlineXsdInWsdlBehavior());

            var basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None)
                                       {
                                           MaxReceivedMessageSize = Int32.MaxValue,
                                           OpenTimeout = TimeSpan.MaxValue,
                                           CloseTimeout = TimeSpan.MaxValue,
                                           ReceiveTimeout = TimeSpan.MaxValue,
                                           SendTimeout = TimeSpan.MaxValue,
                                           ReaderQuotas =
                                               new XmlDictionaryReaderQuotas { MaxStringContentLength = Int32.MaxValue, MaxArrayLength = Int32.MaxValue }
                                       };

            serviceHost.AddServiceEndpoint(typeof (IInterfaceService), basicHttpBinding,
                                           new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) +
                                                   "/Exis/FreightMetrics/InterfaceEngine"));

            var ipcBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                                 {
                                     MaxReceivedMessageSize = Int32.MaxValue,
                                     OpenTimeout = TimeSpan.MaxValue,
                                     CloseTimeout = TimeSpan.MaxValue,
                                     ReceiveTimeout = TimeSpan.MaxValue,
                                     SendTimeout = TimeSpan.MaxValue,
                                     ReaderQuotas =
                                         new XmlDictionaryReaderQuotas { MaxStringContentLength = Int32.MaxValue, MaxArrayLength = Int32.MaxValue }
                                 };
            serviceHost.AddServiceEndpoint(typeof (IInterfaceService), ipcBinding,
                                           "net.pipe://localhost/Exis/FreightMetrics/InterfaceEngine/" + port);

            serviceHost.Description.Behaviors.Add(new ServiceThrottlingBehavior
            {
                MaxConcurrentCalls = 100, // For Singletons with Multiple Threads, the number of concurrent threads
                // MaxConcurrentInstances = 100,// Ignored for Singletons
                MaxConcurrentSessions = 100 // No of Clients with a Transport session aka connected to this Service
            });

//#if DEBUG
            var behavior = new ServiceMetadataBehavior { HttpGetEnabled = true, HttpGetUrl = new Uri("http://localhost:" + (Convert.ToInt32(port) + 1) + "/Exis/FreightMetrics/InterfaceEngine/" + "mex") };
            serviceHost.Description.Behaviors.Add(behavior);
//#endif
            return serviceHost;
        }

      public static void Stop()
        {
            objServiceHost.Close();
        }

        public static void Abort()
        {
            objServiceHost.Abort();
        }

        public static void ChangeDebugLevel()
        {
            var level = (string) AppDomain.CurrentDomain.GetData("LogLevel");
            SiAuto.Main.LogMessage("Changing debug level from " + SiAuto.Si.Level + " to " + level);
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), level, true);
        }

        private static void HandleArguments(string[] args)
        {
            if (args.Length < 5)
            {
                Console.WriteLine(
                    "Usage: InterfaceEngine <Installation Name> <Connection String> <LogLevel> <Base Folder> <Port> (<SmtpHost> <SmtpPort> <SupportAddress> <IssueTrackerAddress> <InstallationAddress>)");
                throw new ApplicationException("Invalid count of arguments.");
            }

            string InstallationName = args[0];
            string ConnectionString = args[1];
            string LogLevel = args[2];
            string BaseFolder = args[3];
            string Port = args[4];

            Console.WriteLine("Interface Engine Starting");
            Console.WriteLine("Arguments:");
            foreach (string s in args)
            {
                Console.WriteLine("\t- " + s);
            }
            SiAuto.Si.AppName = "Interface Engine - " + InstallationName;
            SiAuto.Si.Connections = "file(append=\"true\", filename=\"" +
                                    ServerSessionRegistry.ApplicationDirectory + "\\Logs\\InterfaceEngineLog_" +
                                    InstallationName +
                                    ".sil\", maxsize=\"65536\", rotate=\"daily\", backlog=\"0\", caption=\"file\", flushon=\"error\", keepopen=\"true\", reconnect=\"false\")";
            SiAuto.Si.DefaultLevel = Level.Message;
            SiAuto.Si.Enabled = true;
            SiAuto.Si.Level = (Level) Enum.Parse(typeof (Level), LogLevel, true);
            objServiceHost = CreateServiceHost(Port);
            objService.DBConnectionString = ConnectionString;
            if (args.Length > 5)
            {
                string smtpHost = args[5];
                string smtpPort = args[6];
                string supportAddress = args[7];
                string issueTrackerAddress = args[8];
                string installationAddress = args[9];

                if (smtpHost != "0")
                {
                    objService.smtpClient = new SmtpClient(smtpHost, Int32.Parse(smtpPort));
                    objService.exisSupportEmailAddress = supportAddress;
                    objService.exisIssueTrackerEmailAddress = issueTrackerAddress;
                    objService.installationEmailAddress = installationAddress;
                }
            }
        }
    }
}