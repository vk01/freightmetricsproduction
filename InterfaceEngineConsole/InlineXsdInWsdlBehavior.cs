﻿using System.Collections;
using System.Collections.Generic;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml.Schema;
using ServiceDescription = System.Web.Services.Description.ServiceDescription;

namespace Exis.InterfaceEngineConsole
{
    /// <summary>
    /// IEndpointBehavior implementation that will
    /// force WCF to generate all schemas inline into the
    /// generated WSDL files, instead of as individual files.
    /// </summary>
    public class InlineXsdInWsdlBehavior
       : IWsdlExportExtension, IEndpointBehavior
    {

        #region IWsdlExportExtension Implementation
        //
        // IWsdlExportExtension Implementation
        //
        public void ExportContract(
              WsdlExporter exporter,
              WsdlContractConversionContext context
           )
        {
            // never called
        }

        public void ExportEndpoint(
              WsdlExporter exporter,
              WsdlEndpointConversionContext context
           )
        {
            XmlSchemaSet schemaSet = exporter.GeneratedXmlSchemas;

            foreach (ServiceDescription wsdl in exporter.GeneratedWsdlDocuments)
            {
                //
                // Recursively find all schemas imported by this wsdl
                // and then add them. In the process, remove any
                // <xsd:imports/>
                //
                List<XmlSchema> importsList = new List<XmlSchema>();
                foreach (XmlSchema schema in wsdl.Types.Schemas)
                {
                    AddImportedSchemas(schema, schemaSet, importsList);
                }
                wsdl.Types.Schemas.Clear();
                foreach (XmlSchema schema in importsList)
                {
                    RemoveXsdImports(schema);
                    wsdl.Types.Schemas.Add(schema);
                }
            }
        }

        #endregion // IWsdlExportExtension Implementation


        #region Private Methods
        //
        // Private Methods
        //

        /// <summary>
        /// Recursively extract all the list of imported
        /// schemas
        /// </summary>
        /// <param name="schema">Schema to examine</param>
        /// <param name="schemaSet">SchemaSet with all referenced schemas</param>
        /// <param name="importsList">List to add imports to</param>
        private void AddImportedSchemas(
           XmlSchema schema,
           XmlSchemaSet schemaSet,
           List<XmlSchema> importsList
           )
        {
            foreach (XmlSchemaImport import in schema.Includes)
            {
                ICollection realSchemas =
                   schemaSet.Schemas(import.Namespace);
                foreach (XmlSchema ixsd in realSchemas)
                {
                    if (!importsList.Contains(ixsd))
                    {
                        importsList.Add(ixsd);
                        AddImportedSchemas(ixsd, schemaSet, importsList);
                    }
                }
            }
        }

        /// <summary>
        /// Remove any &lt;xsd:imports/&gt; in the schema
        /// </summary>
        /// <param name="schema">Schema to process</param>
        private void RemoveXsdImports(XmlSchema schema)
        {
            for (int i = 0; i < schema.Includes.Count; i++)
            {
                if (schema.Includes[i] is XmlSchemaImport)
                    schema.Includes.RemoveAt(i--);
            }
        }

        #endregion // Private Methods


        #region IEndpointBehavior Implementation
        //
        // IContractBehavior Implementation
        //
        public void AddBindingParameters(
           ServiceEndpoint endpoint,
           BindingParameterCollection bindingParameters
           )
        {
            // not needed
        }

        public void ApplyClientBehavior(
           ServiceEndpoint endpoint,
           ClientRuntime clientRuntime
           )
        {
            // not needed
        }

        public void ApplyDispatchBehavior(
           ServiceEndpoint endpoint,
           EndpointDispatcher dispatcher
           )
        {
            // not needed
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            // not needed
        }

        #endregion // IContractBehavior Implementation

    } // class InlineXsdInWsdlBehavior
}