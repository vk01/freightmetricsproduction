﻿using System.ServiceModel;

namespace Exis.Server.ServiceInterfaces
{
    [ServiceContract]
    public interface IWorkflowServiceReqRep
    {
        [OperationContract]
        int? ProcessCase(long CaseId);
    }

    [ServiceContract]
    public interface IWorkflowServiceOneWay
    {
        [OperationContract(IsOneWay = true)]
        void ProcessCaseOneWay(long CaseId);
    }
}

namespace Exis.WinClient.ServiceInterfaces
{
    [ServiceContract]
    public interface IWorkflowServiceReqRep
    {
        [OperationContract]
        int? ProcessCase(long CaseId);

        [OperationContract(AsyncPattern = true)]
        System.IAsyncResult BeginProcessCase(long CaseId, System.AsyncCallback callback, object asyncState);

        System.Nullable<int> EndProcessCase(System.IAsyncResult result);
    }

    [ServiceContract]
    public interface IWorkflowServiceOneWay
    {
        [OperationContract(IsOneWay = true)]
        void ProcessCaseOneWay(long CaseId);

        [OperationContract(IsOneWay = true, AsyncPattern = true)]
        System.IAsyncResult BeginProcessCaseOneWay(long CaseId, System.AsyncCallback callback, object asyncState);

        void EndProcessCaseOneWay(System.IAsyncResult result);
    }
}

namespace Exis.SilverlightClient.ServiceInterfaces
{
    [ServiceContract]
    public interface IWorkflowServiceReqRep
    {
        [OperationContract(AsyncPattern = true)]
        System.IAsyncResult BeginProcessCase(long CaseId, System.AsyncCallback callback, object asyncState);

        System.Nullable<int> EndProcessCase(System.IAsyncResult result);
    }
}

