﻿using System;
using System.ServiceModel;

namespace Exis.ServiceInterfaces
{
    [ServiceContract]
    public interface IInterfaceService
    {
        [OperationContract]
        void Test();
    }
}