using System;
using System.Collections.Generic;
using System.ServiceModel;
using Exis.Domain;
using System.Xml;

namespace Exis.ServiceInterfaces
{
    [ServiceContract]
    public interface IClientsService
    {
        #region Interface Members

        #region Administration

        [OperationContract]
        int? AEVVesselInitializationData(out List<Company> companies, out List<Market> markets, out List<Contact> contacts);

        [OperationContract]
        int? AEVIndexInitializationData(long? indexId, out List<Market> markets, out List<Index> indexes, out List<IndexCustomValue> customIndexValues, out List<AverageIndexesAssoc> averageIndexesAssocs);

        [OperationContract]
        int? AEVCompanyInitializationData(out List<Company> companies, out List<CompanySubtype> companySubtypes, out List<Contact> managers);

        [OperationContract]
        int? AEVTraderInitializationData(out List<Company> companies, out List<Market> markets,
                                         out List<ProfitCentre> profitCentres, out List<Desk> desks, out List<Book> books);
        

        [OperationContract]
        int? AEVBookInitializationData(out List<Book> books);

        [OperationContract]
        int? AEVBankAccountInitializationData(out List<Company> companies, out List<Company> banks,
                                              out List<Currency> currencies);

        [OperationContract]
        int? AEVVesselPoolInitializationData(out List<Vessel> vessels);

        [OperationContract]
        int? AEVVesselPoolHistory(long vesselPoolId, out List<VesselPoolInfo> vesselPoolInfos);

        [OperationContract]
        int? AEEntity(bool isEdit, DomainObject entity);

        [OperationContract]
        int? AEIndex(bool isEdit, Index index, List<AverageIndexesAssoc> averageIndexesAssocs);

        [OperationContract]
        int? GetEntities(DomainObject entity, out List<DomainObject> entities);

        [OperationContract]
        int? BalticExchangeInputDataInitializationData(out List<Index> indexes, out List<AppParameter> appParameters);

        [OperationContract]
        int? FtpBatchProcessingFiles(string ftpsource, string ftpArchiveUri, string ftpUri, string ftpXmlFolder, string ftpUsername, string ftpPassword,
            bool isFtpDownload, List<Index> indexes,
            Dictionary<string, int> monthStr, out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages);

        [OperationContract]
        int? LocalProcessingFileXML(XmlElement objRequestElement, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages);

        [OperationContract]
        int? LocalProcessingFileExcel(List<ParseExcelIndexValue> indexValues, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages);

        [OperationContract]
        int? InsertBalticValues(List<IndexFFAValue> indexFfaValues, List<IndexSpotValue> indexSpotValues,
                                List<IndexBOAValue> indexBoaValues, bool isFtpDownload, bool canUpdateValues, out DateTime lastDownLoadDate, out DateTime lastImportDate);

        [OperationContract]
        int? AEVUserInitializationData(out List<Trader> traders, out List<Book> books);

        [OperationContract]
        int? GetIndexValues(long indexId, DateTime date, int absoluteOffset, int percentageOffset, out List<IndexSpotValue> spotValues, out List<IndexFFAValue> originalBFAValues, out Dictionary<DateTime, decimal> normalizedBFAValues, out Dictionary<DateTime, decimal> curveValues);
        
        [OperationContract]
        int? GetUserAssociatedBooks(out List<Book> books);

        [OperationContract]
        int? GetVesselPoolsBySignDate(DateTime signDate, out List<VesselPool> vesselPools);

        [OperationContract]
        int? AEVLoanInitializationData(out List<Company> banks, out List<Vessel> vessels, out List<Company> obligors,
                                       out List<Company> guarantors,
                                       out List<Currency> currencies, out List<Company> borrowers);

        [OperationContract]
        int? AEInstallments(long loanId, List<LoanPrincipalInstallment> loanPrincipalInstallments,
                                 List<LoanInterestInstallment> loanInterestInstallments);

        [OperationContract]
        int? AERiskParametersInitializationData(out List<Index> indexes, out List<RiskVolatility> riskVolatilities, out List<RiskCorrelation> riskCorrelations);

        [OperationContract]
        int? AERiskParameteres(List<RiskVolatility> riskVolatilities, List<RiskCorrelation> riskCorrelations);

        [OperationContract]
        int? AEVHiearchyInitializationData(out List<HierarchyNode> hierarchyNodes,
                                                  out List<HierarchyNodeType> hierarchyNodeTypes);

        [OperationContract]
        int? AEVCashFlowInitializationData(out List<CashFlowModel> cashFlowModels,
                                           out List<CashFlowGroup> cashFlowGroups, out List<CashFlowItem> cashFlowItems,
                                           out List<CashFlowModelGroup> cashFlowModelGroups,
                                           out List<CashFlowGroupItem> cashFlowGroupsItems);

        [OperationContract]
        int? GetCashFlowItems(out List<CashFlowItem> cashFlowItems);

        [OperationContract]
        int? AECashFlowEntities(List<CashFlowItem> cashFlowItems, List<CashFlowGroup> cashFlowGroups, List<CashFlowModel> cashFlowModels, List<CashFlowGroupItem> cashFlowGroupItems, List<CashFlowModelGroup> cashFlowModelsGroups);

        [OperationContract]
        int? AEVHierarchyNodes(List<HierarchyNode> hierarchyNodes);

        [OperationContract]
        int? AEVCashFlowItemInitializationData(out List<Currency> currencies);

        [OperationContract]
        int? AEVInterestReferenceRateInitializationData(out List<Currency> currencies);

        [OperationContract]
        int? AEInterestReferenceRate(bool isEdit, InterestReferenceRate interestReferenceRate);

        [OperationContract]
        int? DeleteInterestReferenceRate(long interestReferenceRateId);

        [OperationContract]
        int? AEVExchangeRateInitializationData(out List<Currency> currencies);

        [OperationContract]
        int? DeleteExchangeRate(long exchangeRateId);

        [OperationContract]
        int? ApplicationParametersInitializationData(out List<AppParameter> appParameters);

        [OperationContract]
        int? CheckUserHasPermissionOnTrader(long userId, long traderId, out bool hasPermission);

        #endregion

        [OperationContract]
        int? PollService();

        [OperationContract]
        int? ValidateCredentials(string userName, string password, out User user);

        [OperationContract]
        int? AEVTradeInitializationData(long? tradeInfoId, FormActionTypeEnum action, bool getExtraData, out List<Company> companies,
                                        out List<Company> counterparties,
                                        out List<Trader> traders, out List<Company> brokers,
                                        out List<Market> marketSegments, out List<Region> marketRegions,
                                        out List<Route> marketRoutes, out List<Vessel> vessels, out List<Index> indexes,
                                        out List<Company> banks,
                                        out List<Account> accounts, out List<ClearingHouse> clearingHouses,
                                        out List<VesselPool> vesselPools,
                                        out List<VesselBenchmarking> vesselBenchmarkings,
                                        out List<TradeVersionInfo> tradeVersionInfos, out Trade trade, out List<TradeVersionInfo> tcTradeVersionInfos);

        [OperationContract]
        int? InsertNewTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                            List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                            TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                            TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos,
                            List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades);

        [OperationContract]
        int? UpdateTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                         List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                         TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                        TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades);

        [OperationContract]
        int? GetEntitiesThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, DomainObject entityTypeInstanceToReturn, List<DomainObject> entityTypeFilters, out List<DomainObject> entities);

        [OperationContract]
        int? GetTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, List<DomainObject> entityTypeFilters, out List<Trade> trades);

        [OperationContract(Name = "GeneratePositionExtraTradeTypeOptions")]
        int? GeneratePosition(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot,string tradeTypeItems, List<string> tradeIdentifiers, out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage);
        [OperationContract]
        int? GeneratePosition(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, List<string> tradeIdentifiers, out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage);

        [OperationContract(Name = "GenerateMarkToMarketExtraTradeTypeOptions")]
        int? GenerateMarkToMarket(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, string tradeTypeItems, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage);
        [OperationContract]
        int? GenerateMarkToMarket(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage);

        /* [OperationContract]
         int? GenerateMonteCarloSimulation(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns, out Dictionary<DateTime, List<double>> monthFrequency, out List<double> cashBounds, out List<Dictionary<DateTime, decimal>> allValues);*/
        [OperationContract(IsOneWay = true)]
        void GenerateMonteCarloSimulation(string mcName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns);

        [OperationContract]
        int? GetCFModels(out List<CashFlowModel> models);

        [OperationContract]
        int? GenerateCashFlow(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, List<string> tradeIdentifiers, long cashFlowModelId,
                              bool calculateBreakEven, bool calculateValuation, bool calculateIRR, bool calculateBEBalloon, bool calculateBEEquity, bool calculateBEScrapValue, decimal valuationDiscountRate,
                              out Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowGroupItem> cashFlowGroupItems,
                              out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsAll, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsSpot, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsBalloon, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsEquity,
                              out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsScrappings, out Dictionary<string, Dictionary<DateTime, decimal>> valuationResults, out Dictionary<string, Dictionary<DateTime, decimal>> valuationDiscountFactor, out Dictionary<string, Dictionary<DateTime, decimal>> valuationPresentValue,
                              out Dictionary<string, Dictionary<DateTime, decimal>> irrResults);

        [OperationContract]
        int? GetEntitiesThatHaveTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo,
                                                 bool acceptDrafts,
                                                 long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters,
                                                 out List<HierarchyNodeInfo> hierarchyNodes);


        [OperationContract]
        int? GetCompanyHierarchyThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo,
                                                 bool acceptDrafts,
                                                 long? hierarchyNodeTypeId, HierarchyNodeInfo entityCompany,
                                                 out Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentHierarchyNodes);

        [OperationContract]
        int? GetTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
                                 bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays, List<HierarchyNodeInfo> entityTypeFilters,
                                 out List<Trade> trades, out List<Company> companies);

        [OperationContract]
        int? GetViewTradesInitializationData(out List<Book> books, out List<Book> userAssocBooks, out List<CashFlowModel> models, out List<Index> indexes);

        [OperationContract]
        int? ImportTrades(List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, List<TmpImportCargoTrade> tmpImportCargoTrades, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades, out string errorMessage);

        [OperationContract]
        int? UpdateTradeStatus(TradeTypeEnum tradeTypeEnum, long identifierId, ActivationStatusEnum status);

        [OperationContract]
        int? GetAverageIndexesValues(List<long> indexes, DateTime curveDate, DateTime periodFrom, DateTime periodTo,
                                     out Dictionary<long, Dictionary<DateTime, decimal>> forwardIndexesValues,
                                     out Dictionary<long, Dictionary<DateTime, decimal>> spotIndexesValues);

        [OperationContract]
        int? GetTradesBySelectionBooks(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
                                       bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays,
                                       List<HierarchyNodeInfo> entityTypeFilters, out List<Trade> trades);

        [OperationContract]
        int? EnergyGenerateMarkToMarket(DateTime curveDate, DateTime periodFrom, DateTime periodTo,
                                        out Dictionary<DateTime, decimal> results,
                                        out string errorMessage);

        [OperationContract]
        int? GetInactiveTrades(out List<Trade> trades);

        [OperationContract]
        int? GetMCRuns(out List<MCSimulationInfo> mcRuns);

        [OperationContract]
        int? GetMCSimulationDetails(MCSimulationInfo mcSimInfo, out MCSimulation mcSimulation);

        [OperationContract]
        int? DeleteMCRuns(MCSimulationInfo mcSimulationInfo);        

        #endregion
    }

    /*
    [ServiceContract]
    [DeliveryRequirements(RequireOrderedDelivery = true, QueuedDeliveryRequirements = QueuedDeliveryRequirementsMode.Allowed)]
    public interface IPolicyRetriever
    {
        [OperationContract, WebGet(UriTemplate = "/clientaccesspolicy.xml")]
        Stream GetSilverlightPolicy();
        [OperationContract, WebGet(UriTemplate = "/crossdomain.xml")]
        Stream GetFlashPolicy();
    }
     */
}