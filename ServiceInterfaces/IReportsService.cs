﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using Exis.Domain;

namespace Exis.ServiceInterfaces
{
    [ServiceContract]
    public interface IReportsService
    {
        [OperationContract]
        void Test();
    }
}