﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exis.Domain;

namespace Exis.WinClient.General
{

    public class CorVal
    {
        public int x;
        public int y;
        public decimal val;
    }



    public class Correlation
    {
        public string key;
        public string tablename;

        public List<CorVal> CorrTable = new List<CorVal>();

        public void AddVal(int a, int b, decimal val)
        {
            CorVal newval = new CorVal();
            newval.x = a;
            newval.y = b;
            newval.val = val;
            CorrTable.Add(newval);
        }
    }
}
