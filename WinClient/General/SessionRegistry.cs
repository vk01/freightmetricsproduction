﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using Exis.Domain;
using Exis.WCFExtensions;
using Exis.WinClient.ClientsEngineService;
using Exis.WinClient.ReportsEngineService;
using Exis.WinClient.WorkflowEngineService;

namespace Exis.WinClient.General
{
    public static class SessionRegistry
    {
        public static string GeneralErrorMessage = "An error occurred. Please try again.";
        private static List<string> m_AllPermissions;
        private static CultureInfo m_ClientCultureInfo;
        private static CultureInfo m_ServerCultureInfo;
        private static SessionTypeEnum m_SessionType = SessionTypeEnum.Win;
        private static List<string> m_UserPermissions;

        private static List<string> m_HasPermissionList;
        private static List<string> m_HasNotPermissionList;

        public static SessionTypeEnum SessionType
        {
            get { return m_SessionType; }
            set { m_SessionType = value; }
        }

        public static ClientsServiceClient Client { get; set; }

        public static ReportsServiceClient Reports { get; set; }

        public static WorkflowServiceClientReqRep WorkflowReqRep { get; set; }

        public static WorkflowServiceClientOneWay WorkflowOneWay { get; set; }

        public static InstanceContext CtiInstanceContext { get; set; }

        public static Binding ClientBinding { get; set; }

        public static Binding ReportsBinding { get; set; }

        public static Binding CtiBinding { get; set; }

        public static Binding WorkflowBinding { get; set; }

        public static EndpointAddress ClientEndpointAddress { get; set; }

        public static EndpointAddress ReportsEndpointAddress { get; set; }

        public static EndpointAddress CtiEndpointAddress { get; set; }

        public static EndpointAddress WorkflowReqRepEndPointAddress { get; set; }

        public static EndpointAddress WorkflowOneWayEndPointAddress { get; set; }

        public static ProgramInfo ProgramInfo { get; set; }

        public static CultureInfo ClientCultureInfo
        {
            get { return m_ClientCultureInfo; }

            set { m_ClientCultureInfo = value; }
        }

        public static List<string> UserPermissions
        {
            get { return m_UserPermissions; }
        }

        public static List<string> AllPermissions
        {
            get { return m_AllPermissions; }
        }

        public static List<string> UserHasPermissions
        {
            get { return m_HasPermissionList; }
            set { m_HasPermissionList = value; }
        }

        public static List<string> UserHasNotPermissions
        {
            get { return m_HasNotPermissionList; }
            set { m_HasNotPermissionList = value; }
        }

        public static User AppUser { get; set; }

        public static string ApplicationDirectory { get; set; }

        public static void ResetClientService()
        {
            try
            {
                Client.Close();
            }
            catch (Exception)
            {
            }
            try
            {
                Client.Abort();
            }
            catch (Exception)
            {
            }

            Client = new ClientsServiceClient(ClientBinding, ClientEndpointAddress);
            foreach (OperationDescription op in Client.Endpoint.Contract.Operations)
            {
                var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dataContractBehavior != null)
                {
                    dataContractBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                }
            }
        }

        public static void ResetReportsService()
        {
            try
            {
                Reports.Close();
            }
            catch (Exception)
            {
            }
            try
            {
                Reports.Abort();
            }
            catch (Exception)
            {
            }
            Reports = new ReportsServiceClient(ReportsBinding, ReportsEndpointAddress);
        }

        public static void ResetWorkflowEngineService()
        {
            try
            {
                WorkflowReqRep.Close();
                WorkflowOneWay.Close();
            }
            catch (Exception)
            {
            }
            try
            {
                WorkflowReqRep.Abort();
                WorkflowOneWay.Abort();
            }
            catch (Exception)
            {
            }
            WorkflowReqRep = new WorkflowServiceClientReqRep(WorkflowBinding, WorkflowReqRepEndPointAddress);
            WorkflowOneWay = new WorkflowServiceClientOneWay(WorkflowBinding, WorkflowOneWayEndPointAddress);
        }

        public static void initialize()
        {
            m_ClientCultureInfo = CultureInfo.CurrentCulture;

            m_UserPermissions = new List<string>();
            m_AllPermissions = new List<string>();

            m_HasPermissionList = new List<string>();
            m_HasNotPermissionList = new List<string>();
        }

        public static bool HasPermission(string code)
        {
            return true;
        }
    }
}