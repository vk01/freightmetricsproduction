﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Exis.WinClient.General
{
    class FormValidationException : ApplicationException
    {
        private ArrayList m_ApplicationExceptions;

        public FormValidationException()
            : base("")
        {
            m_ApplicationExceptions = new ArrayList();
        }

        public ArrayList Exceptions
        {
            get
            {
                return m_ApplicationExceptions;
            }
        }

        public ArrayList GetExceptionsByType(Type type)
        {
            var ret = new ArrayList();
            foreach (Exception exc in m_ApplicationExceptions)
            {
                if (type == exc.GetType())
                {
                    ret.Add(exc);
                }
            }
            return ret;
        }
    }
}
