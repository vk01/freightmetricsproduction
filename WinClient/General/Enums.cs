namespace Exis.WinClient.General
{
    public enum ProductModeEnum
    {
        RetailProduct,
        ServicebasedProduct,
        None
    } ;

    internal enum SearchCustomerTypeEnum
    {
        FindCustomer,
        FindDealer,
        FindParentCustomer,
        FindChildCustomer,
        FindAppointmentCustomer
    } ;

    internal enum SearchCustomerResultEnum
    {
        Add,
        Edit,
        Select
    } ;

    internal enum OrderActionTypeEnum
    {
        NewContract,
        EditContract,
        SuspendContract,
        EditOrder,
        ReplaceContract
    } ;

    internal enum ViewJobsTypeEnum
    {
        All,
        Locked
    } ;

    internal enum CustomerManagementResultEnum
    {
        None,
        Reload
    }

    public enum SessionTypeEnum
    {
        Win,
        Web
    }

    public enum GenderEnum
    {
        Male = 1,
        Female = 2,
        Null
    } ;

    public enum TitleEnum
    {
        Mr = 1,
        Mrs = 2,
        Dr = 3,
        Null
    } ;

    public enum CustomerStatusEnum
    {
        Closed = 0,
        Active = 1,
        Suspended = 2,
        Potential = 3,
        Prospect = 4,
        Null
    } ;
}