﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList;
using Exis.Domain;

namespace Exis.WinClient
{
    public class BookClient : TreeList.IVirtualTreeListData
    {
        private CustomBook _book;
        private List<BookClient> _childrenBooks;
        private List<CustomBook> _allBooks;

        public BookClient(CustomBook book)
        {
            _book = book;
        }

        public BookClient(CustomBook book, List<CustomBook> allBooks)
        {
            _book = book;
            _allBooks = allBooks;
        }

        public CustomBook Book
        {
            get { return _book; }
        }

        public List<BookClient> ChildrenBooks
        {
            set { _childrenBooks = value; }
        }

        public List<CustomBook> AllBooks
        {
            set { _allBooks = value; }
            get { return _allBooks; }
        }

        public void VirtualTreeGetChildNodes(VirtualTreeGetChildNodesInfo info)
        {
            if (((BookClient)info.Node).Book == null)
            {
                info.Children = _childrenBooks;
            }
            else
            {
                info.Children = _allBooks.Where(a => a.ParentBookId == ((BookClient) info.Node).Book.Id).Select(b=> new BookClient(b, _allBooks)).ToList();
            }
        }

        public void VirtualTreeGetCellValue(VirtualTreeGetCellValueInfo info)
        {
            info.CellData = ((BookClient) info.Node).Book.Name;
        }

        public void VirtualTreeSetCellValue(VirtualTreeSetCellValueInfo info)
        {
            info.Cancel = true;
        }
    }
}
