﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using DevExpress.XtraTabbedMdi;
using Exis.Domain;
using Exis.WinClient.Controls;
using Exis.WinClient.Controls.Core;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using System.Windows.Threading;
using System.Threading;

namespace Exis.WinClient
{
    public partial class MainForm : XtraForm
    {
        #region Private Variables 

        private DispatcherTimer timer;
        private XtraForm activeForm;
        private bool isFirstTime;

        #endregion

        #region Enumeration

        public enum NotificationType
        {
            Report,
            Event,
            NewCall, //CRM-1935
            Reminder,
            Other
        }

        public enum LoginStatus
        {
            LoggedIn,
            LoggedOut
        }

        #endregion

        #region Constructor/Destructor

        public MainForm()
        {
            InitializeComponent();
            Initialize();

        }

        #endregion

        #region Private Methods

        private void Logout()
        {
            
        }
        private void Initialize()
        {
            isFirstTime = true;
            SetUIControls(LoginStatus.LoggedOut);

            bmnuHelp.Enabled = true;
            
            Text = Strings.Freight_Metrics;
            //Text = Strings.Freight_Metrics_Test;

            //timer = new DispatcherTimer();
            //timer.Tick += new EventHandler(TimerTick);
            //timer.Interval = new TimeSpan(0, 0, 120);
            //timer.Start();
        }

        private void SetUIControls(LoginStatus status)
        {
            rpApplicationSettings.Visible = true;

            if (status == LoginStatus.LoggedOut)
            {
                bmnuLogIn.Visibility = BarItemVisibility.Always;
                bmnuLogIn.Caption = "Login";

                if (isFirstTime)
                {
                    bmnuLogIn.ItemClick += BmnuLoginItemClick;
                    isFirstTime = false;
                }
                else
                {
                    bmnuLogIn.ItemClick -= BmnuLogoutItemClick;
                    bmnuLogIn.ItemClick += BmnuLoginItemClick;
                }

                bmnuApplicationParametersManagement.Enabled = false;

                rpMainMenu.Visible = false;
                rpEntitiesManagement.Visible = false;
            }

            if (status == LoginStatus.LoggedIn)
            {
                bmnuLogIn.Caption = "Logout";
                bmnuLogIn.ItemClick -= BmnuLoginItemClick;
                bmnuLogIn.ItemClick += BmnuLogoutItemClick;

                bmnuApplicationParametersManagement.Enabled = true;

                rpMainMenu.Visible = true;
                ribbon.SelectedPage = rpMainMenu;
                rpEntitiesManagement.Visible = true;
            }
        }

        #endregion

        #region Menu Items Handlers

        private void BmnuLoginItemClick(object sender, ItemClickEventArgs e)
        {
            var objLoginForm = new LoginForm();

            if (objLoginForm.ShowDialog(this) == DialogResult.OK)
            {
                SetUIControls(LoginStatus.LoggedIn);

                timer = new DispatcherTimer();
                timer.Tick += new EventHandler(TimerTick);
                timer.Interval = new TimeSpan(0, 0, 120);
                timer.Start();
            }
        }

        void TimerTick(object sender, EventArgs e)
        {
            BeginPollService();
        }

        private void BmnuLogoutItemClick(object sender, ItemClickEventArgs e)
        {
            SessionRegistry.AppUser = null;

            SetUIControls(LoginStatus.LoggedOut);

            if(timer != null)
                timer.Stop();

            var pages = mdiManager.Pages.Cast<XtraMdiTabPage>().ToList();

            foreach (XtraMdiTabPage page in pages)
            {
                page.MdiChild.Close();
            }

            if (OnLogout != null)
            {
                OnLogout();
            }

            //var objLoginForm = new LoginForm3();

            //if (objLoginForm.ShowDialog(this) == DialogResult.OK)
            //{

            //    //InitializeComponent();
            //    //Initialize();
            //}
            //else
            //{
            //    XtraMessageBox.Show("failed to load");
            //}
        }

        private void BmnuChangeUiLanguageItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show(this,
                            Program.GlobalResourceManager.GetString(
                                "A language selector will appear the next time you attempt to run the application."),
                            Program.GlobalResourceManager.GetString("Exis Application"), MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            GeneralSettings.Default.CnGUILanguage = Convert.ToString("");
            GeneralSettings.Default.Save();
        }

        private void BmnuChangePasswordItemClick(object sender, ItemClickEventArgs e)
        {
        }

        private void BmnuSettingsItemClick(object sender, ItemClickEventArgs e)
        {
            var objServConnSettForm = new ServConnSettingsForm();
            objServConnSettForm.ShowDialog(this);
        }

        private void BmnuAboutItemClick(object sender, ItemClickEventArgs e)
        {
            var aboutForm = new AboutForm();
            aboutForm.ShowInTaskbar = false;
            aboutForm.StartPosition = FormStartPosition.CenterParent;
            aboutForm.ShowDialog(this);
        }

        private void BmnuViewTradesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuViewTrades.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new ViewTradesForm { MdiParent = this, Text = Strings.View_Trades };

                form.OnDataSaved += AEVFormDataSaved;
                form.ViewChartEvent += OnViewChart;
                form.RatiosReportEvent += OnRatiosReportEvent;
                form.OnEditTrade += OnEditTrade;
                form.OnViewTrade += OnViewTrade;
                form.OnOpenActivateTradesForm += OnOpenActivateTradesForm;
                form.OnTradeDeactivated += OnTradeDeactivated;
                form.OnGenerateMTM += OnGenerateMTM;
                form.OnGeneratePOS += OnGeneratePOS;
                form.OnGenerateCF += OnGenerateCF;
                bmnuViewTrades.Tag = form;
                form.Tag = bmnuViewTrades;
                form.Show();
            }
        }

        private void bmnuCompaniesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuCompanies.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new CompaniesManagementForm { MdiParent = this, Text = Strings.Companies };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                form.OnSubtypesEvent += OnCompanySubtypesEvent;
                bmnuCompanies.Tag = form;
                form.Tag = bmnuCompanies;
                form.Show();
            }
        }

        private void bmnuTradersClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuTraders.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new TradersManagementForm { MdiParent = this, Text = Strings.Traders };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuTraders.Tag = form;
                form.Tag = bmnuTraders;
                form.Show();
            }
        }

        //private void bmnuVesselsClick(object sender, ItemClickEventArgs e)
        //{
        //    if (bmnuVessels.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new VesselsManagementForm { MdiParent = this, Text = Strings.Vessels };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        bmnuVessels.Tag = form;
        //        form.Tag = bmnuVessels;
        //        form.Show();
        //    }
        //}

        private void bmnuVesselPoolsClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuVesselPools.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new VesselPoolsManagementForm() { MdiParent = this, Text = Strings.Vessel_Pools };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                form.OnVesselEvent += OnVesselManagementEvent;
                bmnuVesselPools.Tag = form;
                form.Tag = bmnuVesselPools;
                form.Show();
            }
        }

        private void bmnuBooksClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuBooks.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new BooksManagementForm { MdiParent = this, Text = Strings.Books };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuBooks.Tag = form;
                form.Tag = bmnuBooks;
                form.Show();
            }
        }

        private void bmnuBankAccountsClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuBankAccounts.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new BankAccountsManagementForm { MdiParent = this, Text = Strings.Bank_Accounts };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuBankAccounts.Tag = form;
                form.Tag = bmnuBankAccounts;
                form.Show();
            }
        }

        private void bmnuProfitCentersClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuProfitCenters.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new ProfitCentresManagementForm { MdiParent = this, Text = Strings.Profit_Centres };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuProfitCenters.Tag = form;
                form.Tag = bmnuProfitCenters;
                form.Show();
            }
        }

        private void bmnuDesksClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuDesks.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new DesksManagementForm { MdiParent = this, Text = Strings.Desks };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuDesks.Tag = form;
                form.Tag = bmnuDesks;
                form.Show();
            }
        }

        private void bmnuClearingHousesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuClearingHouses.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new ClearingHousesManagementForm { MdiParent = this, Text = Strings.Clearing_Houses };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuClearingHouses.Tag = form;
                form.Tag = bmnuClearingHouses;
                form.Show();
            }
        }

        //private void bmnuCompanySubtypesClick(object sender, ItemClickEventArgs e)
        //{
        //    if (bmnuCompanySubtypes.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new CompanySubtypesManagementForm() { MdiParent = this, Text = Strings.Company_Subtypes };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        bmnuCompanySubtypes.Tag = form;
        //        form.Tag = bmnuCompanySubtypes;
        //        form.Show();
        //    }
        //}

        private void bmnuIndexesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuIndexes.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new IndexesManagementForm() { MdiParent = this, Text = Strings.Indexes };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuIndexes.Tag = form;
                form.Tag = bmnuIndexes;
                form.Show();
            }
        }

        private void bmnuContactsClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuContacts.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new ContactsManagementForm { MdiParent = this, Text = Strings.Contacts };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuContacts.Tag = form;
                form.Tag = bmnuContacts;
                form.Show();
            }
        }

        private void bmnuLoansManagementClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuLoansManagement.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new LoansManagementForm() { MdiParent = this, Text = Strings.Loans };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                form.ManageInstallmentsEvent += ManageInstallmentsEvent;
                bmnuLoansManagement.Tag = form;
                form.Tag = bmnuLoansManagement;
                form.Show();
            }
        }

        private void bmnuHierarchyManagementClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuHierarchyManagement.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new HierarchyNodesManagement() { MdiParent = this, Text = Strings.Entities_Hierarchy };
                form.OnDataSaved += AEVFormDataSaved;
                bmnuHierarchyManagement.Tag = form;
                form.Tag = bmnuHierarchyManagement;
                form.Show();
            }
        }

        private void bmnuInterestRefRatesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuInterestRefRates.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new InterestReferenceRatesManagementForm() { MdiParent = this, Text = Strings.Interest_Reference_Rates };
                form.OnDataSaved += AEVFormDataSaved;
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;

                bmnuInterestRefRates.Tag = form;
                form.Tag = bmnuInterestRefRates;
                form.Show();
            }
        }
   
        private void bmnuExchangeRatesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuExchangeRates.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new Controls.ExchangeRatesManagementForm() { MdiParent = this, Text = Strings.Exchange_Rates };
                form.OnDataSaved += AEVFormDataSaved;
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;

                bmnuExchangeRates.Tag = form;
                form.Tag = bmnuExchangeRates;
                form.Show();
            }
        }

        private void bmnuAddTCTradeClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuAddTCTrade.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new AddEditViewTradeForm(TradeTypeEnum.TC) { MdiParent = this, Text = Strings.Add_TC_Trade };
                form.OnDataSaved += AEVFormDataSaved;
                bmnuAddTCTrade.Tag = form;
                form.Tag = bmnuAddTCTrade;
                form.Show();
            }
        }

        private void bmnuAddFFATradeClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuAddFFATrade.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new AddEditViewTradeForm(TradeTypeEnum.FFA) { MdiParent = this, Text = Strings.Add_FFA_Trade };
                form.OnDataSaved += AEVFormDataSaved;
                bmnuAddFFATrade.Tag = form;
                form.Tag = bmnuAddFFATrade;
                form.Show();
            }
        }

        private void bmnuAddCargoTradeClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuAddCargoTrade.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new AddEditViewTradeForm(TradeTypeEnum.Cargo) { MdiParent = this, Text = Strings.Add_Cargo_Trade };
                form.OnDataSaved += AEVFormDataSaved;
                bmnuAddCargoTrade.Tag = form;
                form.Tag = bmnuAddCargoTrade;
                form.Show();
            }
        }
        
        private void bmnuAddOptionTradeClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuAddOptionTrade.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new AddEditViewTradeForm(TradeTypeEnum.Option) { MdiParent = this, Text = Strings.Add_Option_Trade };
                form.OnDataSaved += AEVFormDataSaved;
                form.OnViewTrade += OnViewTrade;
                bmnuAddOptionTrade.Tag = form;
                form.Tag = bmnuAddOptionTrade;
                form.Show();
            }
        }

        private void bmnuBalticInputDataClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuBalticInputData.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new BalticExchangeDataInputForm { MdiParent = this, Text = Strings.Baltic_Exchange_Data_Input_Data };
                form.FormClosed += SimpleFormClosed;
                bmnuBalticInputData.Tag = form;
                form.Tag = bmnuBalticInputData;
                form.Show();
            }
        }

        private void bmnuImportTradesClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuImportTrades.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new ImportTradesForm() { MdiParent = this };
                bmnuImportTrades.Tag = form;
                form.Tag = bmnuImportTrades;
                form.Show();
            }
        }

        private void bmnuRiskManagementClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuRiskManagement.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new RiskParametersManagementForm() { MdiParent = this, Text = Strings.Risk_Parameters };
                form.OnDataSaved += AEVFormDataSaved;
                bmnuRiskManagement.Tag = form;
                form.Tag = bmnuRiskManagement;
                form.Show();
            }
        }

        private void bmnuCashFlowClick(object sender, ItemClickEventArgs e)
        {
            if (de.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new CashFlowManagementForm() { MdiParent = this, Text = Strings.Cash_Flows };
                form.OnDataSaved += AEVFormDataSaved;
                form.AddEditViewCashFlowItemEvent += OnAddEditViewCashFlowItemEvent;
                de.Tag = form;
                form.Tag = de;
                form.Show();
            }
        }

        private void bmnuUsersManagementClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuUsersManagement.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new UsersManagementForm() { MdiParent = this, Text = Strings.Users };
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.ViewEntityEvent += ViewEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                bmnuUsersManagement.Tag = form;
                form.Tag = bmnuUsersManagement;
                form.Show();
            }
        }

        private void bmnuApplicationParametersManagementClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuApplicationParametersManagement.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new ApplicationParametersManagement() { MdiParent = this };
                bmnuApplicationParametersManagement.Tag = form;
                form.InsertEntityEvent += InsertEntityEvent;
                form.EditEntityEvent += EditEntityEvent;
                form.OnDataSaved += AEVFormDataSaved;
                form.Tag = bmnuApplicationParametersManagement;
                form.Show();
            }
        }

        private void bmnuMCSimulationsClick(object sender, ItemClickEventArgs e)
        {
            if (bmnuMCSimulations.Tag != null)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((BarManager)sender).PressedLink.Item.Tag)
                    )
                {
                    mdiManager.SelectedPage = page;
                    break;
                }
            }
            else
            {
                var form = new MCRunsManagementForm() { MdiParent = this, Text = "Monte Carlo Simulations" };
                bmnuMCSimulations.Tag = form;
                form.MonteCarloSimulationEvent += OnMonteCarloSimulation;
                form.OnDataSaved += AEVFormDataSaved;
                form.Tag = bmnuMCSimulations;
                form.Show();
            }
        }
        
        #endregion

        #region Event Handlers

        private void MainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (SessionRegistry.Client != null) SessionRegistry.Client.Close();
            }
            catch (Exception)
            {

            }

            try
            {
                if (SessionRegistry.Reports != null)
                    SessionRegistry.Reports.Close();
            }
            catch (Exception)
            {

            }

            
        }

        //private void navAddTCTrade_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navAddTCTrade.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new AddEditViewTradeForm(TradeTypeEnum.TC) {MdiParent = this, Text = Strings.Add_TC_Trade};
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navAddTCTrade.Tag = form;
        //        form.Tag = navAddTCTrade;
        //        form.Show();
        //    }
        //}

        //private void navAddFFATrade_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navAddFFATrade.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new AddEditViewTradeForm(TradeTypeEnum.FFA) {MdiParent = this, Text = Strings.Add_FFA_Trade};
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navAddFFATrade.Tag = form;
        //        form.Tag = navAddFFATrade;
        //        form.Show();
        //    }
        //}

        //private void navAddCargo_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navAddCargoTrade.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new AddEditViewTradeForm(TradeTypeEnum.Cargo) { MdiParent = this, Text = Strings.Add_Cargo_Trade };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navAddCargoTrade.Tag = form;
        //        form.Tag = navAddCargoTrade;
        //        form.Show();
        //    }
        //}

        //private void navAddOptionTrade_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navAddOptionTrade.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new AddEditViewTradeForm(TradeTypeEnum.Option) { MdiParent = this, Text = Strings.Add_Option_Trade };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.OnViewTrade += OnViewTrade;
        //        navAddOptionTrade.Tag = form;
        //        form.Tag = navAddOptionTrade;
        //        form.Show();
        //    }
        //}

        //private void navCompanies_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navCompanies.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new CompaniesManagementForm {MdiParent = this, Text = Strings.Companies};
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navCompanies.Tag = form;
        //        form.Tag = navCompanies;
        //        form.Show();
        //    }
        //}

        //private void navTraders_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navTraders.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new TradersManagementForm {MdiParent = this, Text = Strings.Traders};
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navTraders.Tag = form;
        //        form.Tag = navTraders;
        //        form.Show();
        //    }
        //}

        //private void navVessels_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navVessels.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new VesselsManagementForm {MdiParent = this, Text = Strings.Vessels};
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navVessels.Tag = form;
        //        form.Tag = navVessels;
        //        form.Show();
        //    }
        //}

        //private void navBooks_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navBooks.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new BooksManagementForm { MdiParent = this, Text = Strings.Books };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navBooks.Tag = form;
        //        form.Tag = navBooks;
        //        form.Show();
        //    }
        //}

        //private void navBankAccounts_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navBankAccounts.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new BankAccountsManagementForm { MdiParent = this, Text = Strings.Bank_Accounts };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navBankAccounts.Tag = form;
        //        form.Tag = navBankAccounts;
        //        form.Show();
        //    }
        //}

        //private void navProfitCentres_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navProfitCentres.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new ProfitCentresManagementForm { MdiParent = this, Text = Strings.Profit_Centres };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navProfitCentres.Tag = form;
        //        form.Tag = navProfitCentres;
        //        form.Show();
        //    }
        //}

        //private void navDesks_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navDesks.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new DesksManagementForm { MdiParent = this, Text = Strings.Desks };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navDesks.Tag = form;
        //        form.Tag = navDesks;
        //        form.Show();
        //    }
        //}

        //private void navCompanySubtypes_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navCompanySubtypes.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new CompanySubtypesManagementForm() { MdiParent = this, Text = Strings.Company_Subtypes };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navCompanySubtypes.Tag = form;
        //        form.Tag = navCompanySubtypes;
        //        form.Show();
        //    }
        //}

        //private void navClearingHouses_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navClearingHouses.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new ClearingHousesManagementForm { MdiParent = this, Text = Strings.Clearing_Houses };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navClearingHouses.Tag = form;
        //        form.Tag = navClearingHouses;
        //        form.Show();
        //    }
        //}

        //private void navVesselPools_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navVesselPools.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new VesselPoolsManagementForm() { MdiParent = this, Text = Strings.Vessel_Pools };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navVesselPools.Tag = form;
        //        form.Tag = navVesselPools;
        //        form.Show();
        //    }
        //}

        //private void navIndexes_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navIndexes.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new IndexesManagementForm() { MdiParent = this, Text = Strings.Indexes };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navIndexes.Tag = form;
        //        form.Tag = navIndexes;
        //        form.Show();
        //    }
        //}

        //private void navViewTrades_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navViewTrades.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new ViewTradesForm3 {MdiParent = this, Text = Strings.View_Trades};
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.ViewChartEvent += OnViewChart;
        //        form.MonteCarloSimulationEvent += OnMonteCarloSimulation;
        //        form.RatiosReportEvent += OnRatiosReportEvent;
        //        form.OnEditTrade += OnEditTrade;
        //        form.OnViewTrade += OnViewTrade;
        //        form.OnOpenActivateTradesForm += OnOpenActivateTradesForm;
        //        form.OnTradeDeactivated += OnTradeDeactivated;
        //        navViewTrades.Tag = form;
        //        form.Tag = navViewTrades;
        //        form.Show();
        //    }
        //}

        //private void navBalticExchangeInputData_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navBalticExchangeInputData.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new BalticExchangeDataInputForm {MdiParent = this, Text = Strings.Baltic_Exchange_Data_Input_Data};
        //        form.FormClosed += SimpleFormClosed;
        //        navBalticExchangeInputData.Tag = form;
        //        form.Tag = navBalticExchangeInputData;
        //        form.Show();
        //    }
        //}

        //private void navUsersManagement_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navUsersManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new UsersManagementForm() { MdiParent = this, Text = Strings.Users };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navUsersManagement.Tag = form;
        //        form.Tag = navUsersManagement;
        //        form.Show();
        //    }
        //}

        //private void navContacts_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navContacts.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new ContactsManagementForm { MdiParent = this, Text = Strings.Contacts };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navContacts.Tag = form;
        //        form.Tag = navContacts;
        //        form.Show();
        //    }
        //}

        //private void navLoansManagement_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navLoansManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new LoansManagementForm() { MdiParent = this, Text = Strings.Loans };
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.ViewEntityEvent += ViewEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.ManageInstallmentsEvent += ManageInstallmentsEvent;
        //        navLoansManagement.Tag = form;
        //        form.Tag = navLoansManagement;
        //        form.Show();
        //    }
        //}

        //private void navRiskParametersManagement_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navRiskParametersManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new RiskParametersManagementForm() { MdiParent = this, Text = Strings.Risk_Parameters };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navRiskParametersManagement.Tag = form;
        //        form.Tag = navRiskParametersManagement;
        //        form.Show();
        //    }
        //}

        //private void navHierarchyManagement_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navHierarchyManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new HierarchyNodesManagement() { MdiParent = this, Text = Strings.Entities_Hierarchy };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        navHierarchyManagement.Tag = form;
        //        form.Tag = navHierarchyManagement;
        //        form.Show();
        //    }
        //}

        //private void navCashFlowManagement_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navCashFlowManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new CashFlowManagementForm() { MdiParent = this, Text = Strings.Cash_Flows };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.AddEditViewCashFlowItemEvent += OnAddEditViewCashFlowItemEvent;
        //        navCashFlowManagement.Tag = form;
        //        form.Tag = navCashFlowManagement;
        //        form.Show();
        //    }
        //}

        //private void NavInterestReferenceRatesManagementLinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navInterestReferenceRatesManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new InterestReferenceRatesManagementForm() { MdiParent = this, Text = Strings.Interest_Reference_Rates };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
                
        //        navInterestReferenceRatesManagement.Tag = form;
        //        form.Tag = navInterestReferenceRatesManagement;
        //        form.Show();
        //    }
        //}

        //private void navExchangeRates_LinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navExchangeRatesManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new Controls.ExchangeRatesManagementForm() { MdiParent = this, Text = Strings.Exchange_Rates };
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;

        //        navExchangeRatesManagement.Tag = form;
        //        form.Tag = navExchangeRatesManagement;
        //        form.Show();
        //    }
        //}

        //private void NavImportTradesLinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navImportTrades.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new ImportTradesForm() { MdiParent = this };
        //        navImportTrades.Tag = form;
        //        form.Tag = navImportTrades;
        //        form.Show();
        //    }
        //}

        //private void NavApplicationParametersManagementLinkClicked(object sender, NavBarLinkEventArgs e)
        //{
        //    if (navApplicationParametersManagement.Tag != null)
        //    {
        //        foreach (
        //            XtraMdiTabPage page in
        //                mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem)sender).Tag)
        //            )
        //        {
        //            mdiManager.SelectedPage = page;
        //            break;
        //        }
        //    }
        //    else
        //    {
        //        var form = new ApplicationParametersManagement() { MdiParent = this };
        //        navApplicationParametersManagement.Tag = form;
        //        form.InsertEntityEvent += InsertEntityEvent;
        //        form.EditEntityEvent += EditEntityEvent;
        //        form.OnDataSaved += AEVFormDataSaved;
        //        form.Tag = navApplicationParametersManagement;
        //        form.Show();
        //    }
        //}

        private void MdiManagerPageRemoved(object sender, MdiTabPageEventArgs e)
        {
            var bmnuItem = (BarButtonItem)((XtraForm)e.Page.MdiChild).Tag;
            if (bmnuItem != null) bmnuItem.Tag = null;
        }

        private void AEVFormDataSaved(object sender, bool closeForm)
        {
            //var bmnuItem = (BarButtonItem)((XtraForm)sender).Tag;
            //if (closeForm) bmnuItem.Tag = null;

           /* var navItem = (BarButtonItem) ((XtraForm) sender).Tag;
            if(closeForm) navItem.Tag = null;*/
      
            if (sender.GetType() == typeof (CompanyAEVForm) && ((CompanyAEVForm) sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof (CompaniesManagementForm)))
                {
                    ((CompaniesManagementForm) page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof (TraderAEVForm) &&
                     ((TraderAEVForm) sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof (TradersManagementForm)))
                {
                    ((TradersManagementForm) page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(VesselAEVForm) &&
                     ((VesselAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(VesselsManagementForm)))
                {
                    ((VesselsManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(BookAEVForm) &&
                     ((BookAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(BooksManagementForm)))
                {
                    ((BooksManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(BankAccountAEVForm) &&
                     ((BankAccountAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(BankAccountsManagementForm)))
                {
                    ((BankAccountsManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(ProfitCentreAEVForm) &&
                     ((ProfitCentreAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ProfitCentresManagementForm)))
                {
                    ((ProfitCentresManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(DeskAEVForm) &&
                     ((DeskAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(DesksManagementForm)))
                {
                    ((DesksManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(ClearingHouseAEVForm) &&
                     ((ClearingHouseAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ClearingHousesManagementForm)))
                {
                    ((ClearingHousesManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(CompanySubtypeAEVForm) &&
                     ((CompanySubtypeAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(CompanySubtypesManagementForm)))
                {
                    ((CompanySubtypesManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(VesselPoolAEVForm) &&
                     ((VesselPoolAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(VesselPoolsManagementForm)))
                {
                    ((VesselPoolsManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(UserAEVForm) &&
                     ((UserAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(UsersManagementForm)))
                {
                    ((UsersManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(ContactAEVForm) &&
                     ((ContactAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ContactsManagementForm)))
                {
                    ((ContactsManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(IndexAEVForm) &&
                     ((IndexAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(IndexesManagementForm)))
                {
                    ((IndexesManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(LoanAEVForm) &&
                     ((LoanAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(LoansManagementForm)))
                {
                    ((LoansManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(InstallmentAEVForm) &&
                     ((InstallmentAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(LoansManagementForm)))
                {
                    ((LoansManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(AddEditViewTradeForm) &&
                     ((AddEditViewTradeForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ViewTradesForm)))
                {
                    ((ViewTradesForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(InterestReferenceRateAEVForm) &&
                     ((InterestReferenceRateAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(InterestReferenceRatesManagementForm)))
                {
                    ((InterestReferenceRatesManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(ExchangeRateAEVForm) &&
                     ((ExchangeRateAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ExchangeRatesManagementForm)))
                {
                    ((ExchangeRatesManagementForm)page.MdiChild).RefreshData();
                    break;
                }
            }
            else if (sender.GetType() == typeof(AppParameterAEVForm) &&
                     ((AppParameterAEVForm)sender).DialogResult == DialogResult.OK)
            {
                foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ApplicationParametersManagement)))
                {
                    ((ApplicationParametersManagement)page.MdiChild).RefreshData();
                    break;
                }
            }
            if (closeForm)
                ((XtraForm)sender).Close();
        }

        private void SimpleFormClosed(object sender, FormClosedEventArgs e)
        {
            var navItem = (BarButtonItem)((XtraForm)sender).Tag;
            //navItem.Tag = null;
        }

        public void NavLinkClicked(object sender, NavBarLinkEventArgs e)
        {
            foreach (
                XtraMdiTabPage page in
                    mdiManager.Pages.Cast<XtraMdiTabPage>().Where(page => page.MdiChild == ((NavBarItem) sender).Tag))
            {
                mdiManager.SelectedPage = page;
                break;
            }
        }

        private void InsertEntityEvent(Type type)
        {
            if (InvokeRequired)
            {
                Action<Type> action = InsertEntityEvent;
                Invoke(action, type);
                return;
            }

            if (type == typeof (Company))
            {
               /* foreach (
                    NavBarItemLink link in
                        nbgAdministrationOpenActions.ItemLinks.Cast<NavBarItemLink>().Where(
                            link =>
                            link.Item.Tag.GetType() == typeof (CompanyAEVForm) &&
                            ((CompanyAEVForm) link.Item.Tag).FormActionType == FormActionTypeEnum.Add))
                {
                    link.PerformClick();
                    return;
                }*/

                /*NavBarItemLink newLink = nbgAdministrationOpenActions.AddItem();
                newLink.Item.Caption = Strings.Add_Company;
                newLink.Item.Hint = newLink.Item.Caption;
                newLink.Item.LinkClicked += NavLinkClicked;*/

                var form = new CompanyAEVForm {MdiParent = this, Text = Strings.Add_Company};
                form.OnDataSaved += AEVFormDataSaved;
                /*newLink.Item.Tag = form;
                form.Tag = newLink.Item;*/
                form.Show();

               /* nbgAdministrationOpenActions.Expanded = true;*/
               /* newLink.PerformClick();*/
            }
            else if (type == typeof (Trader))
            {
                var form = new TraderAEVForm {MdiParent = this, Text = Strings.Add_Trader};
                form.OnDataSaved += AEVFormDataSaved;
                form.Show();
            }
            else if (type == typeof (Vessel))
            {
                var form = new VesselAEVForm {MdiParent = this, Text = Strings.Add_Vessel};
                form.OnDataSaved += AEVFormDataSaved;
                form.Show();
            }
            else if (type == typeof(Book))
            {
                
                var form = new BookAEVForm { MdiParent = this, Text = Strings.Add_Book };
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

             
            }
            else if (type == typeof(Account))
            {
                var form = new BankAccountAEVForm { MdiParent = this, Text = Strings.Add_Bank_Account };
                form.OnDataSaved += AEVFormDataSaved;
                form.Show();
            }
            else if (type == typeof(ProfitCentre))
            {
                var form = new ProfitCentreAEVForm { MdiParent = this, Text = Strings.Add_Profit_Centre };
                form.OnDataSaved += AEVFormDataSaved;
                form.Show();
            }
            else if (type == typeof(Desk))
            {
                var form = new DeskAEVForm { MdiParent = this, Text = Strings.Add_Desk };
                form.OnDataSaved += AEVFormDataSaved;
                form.Show();
            }
            else if (type == typeof(ClearingHouse))
            {
                var form = new ClearingHouseAEVForm { MdiParent = this, Text = Strings.Add_Clearing_House };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

           
            }
            else if (type == typeof(CompanySubtype))
            {
               

                var form = new CompanySubtypeAEVForm { MdiParent = this, Text = Strings.Add_Company_Subtype };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();

            }
            else if (type == typeof(VesselPool))
            {
                
                var form = new VesselPoolAEVForm { MdiParent = this, Text = Strings.Add_Vessel_Pool };
                form.OnDataSaved += AEVFormDataSaved;
            
                form.Show();

           
            }
            else if (type == typeof(Index))
            {
                var form = new IndexAEVForm { MdiParent = this, Text = Strings.Add_Index };
                form.OnDataSaved += AEVFormDataSaved;
             
                form.Show();
            }
            else if (type == typeof(User))
            {
                
                var form = new UserAEVForm { MdiParent = this, Text = Strings.Add_User };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();
            }
            else if (type == typeof(Contact))
            {
                
                var form = new ContactAEVForm { MdiParent = this, Text = Strings.Add_Contact };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();

            }
            else if (type == typeof(Loan))
            {
              
                var form = new LoanAEVForm { MdiParent = this, Text = Strings.Add_Loan };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (type == typeof(InterestReferenceRate))
            {
                
                var form = new InterestReferenceRateAEVForm() { MdiParent = this, Text = Strings.Add_Interest_Reference_Rate };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();
            }
            else if (type == typeof(ExchangeRate))
            {
               
                var form = new ExchangeRateAEVForm() { MdiParent = this, Text = Strings.Add_Exchange_Rate };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();
            }
            else if (type == typeof(AppParameter))
            {
                
                var form = new AppParameterAEVForm() { MdiParent = this, Text = Strings.Add_Application_Parameter };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();

            }
        }

        private void EditEntityEvent(DomainObject entity)
        {
            if (InvokeRequired)
            {
                Action<DomainObject> action = EditEntityEvent;
                Invoke(action, entity);
                return;
            }

            if (entity.GetType() == typeof (Company))
            {
                /*foreach (
                    NavBarItemLink link in
                        nbgAdministrationOpenActions.ItemLinks.Cast<NavBarItemLink>().Where(
                            link =>
                            link.Item.Tag.GetType() == typeof (CompanyAEVForm) &&
                            ((CompanyAEVForm) link.Item.Tag).FormActionType == FormActionTypeEnum.Edit &&
                            ((CompanyAEVForm) link.Item.Tag).Company.Id == ((Company) entity).Id))
                {
                    link.PerformClick();
                    return;
                }
                NavBarItemLink newLink = nbgAdministrationOpenActions.AddItem();
                newLink.Item.Caption = Strings.Edit_Company + " " + ((Company) entity).Name;
                newLink.Item.Hint = newLink.Item.Caption;
                newLink.Item.LinkClicked += NavLinkClicked;
                */
                var form = new CompanyAEVForm((Company) entity, FormActionTypeEnum.Edit)
                               {MdiParent = this, Text = Strings.Edit_Company + " " + ((Company) entity).Name};
                form.OnDataSaved += AEVFormDataSaved;
                
                //newLink.Item.Tag = form;
                //form.Tag = newLink.Item;
                form.Show();

                /*nbgAdministrationOpenActions.Expanded = true;
                newLink.PerformClick();*/
            }
            else if (entity.GetType() == typeof (Trader))
            {
                
                var form = new TraderAEVForm((Trader) entity, FormActionTypeEnum.Edit)
                               {MdiParent = this, Text = Strings.Edit_Trader + " " + ((Trader) entity).Name};
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

              
            }
            else if (entity.GetType() == typeof (Vessel))
            {
                
                var form = new VesselAEVForm((Vessel) entity, FormActionTypeEnum.Edit)
                               {MdiParent = this, Text = Strings.Edit_Vessel + " " + ((Vessel) entity).Name};
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();
            }
            else if (entity.GetType() == typeof(Book))
            {
                

                var form = new BookAEVForm((Book)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Book + " " + ((Book)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

                
            }
            else if (entity.GetType() == typeof(Account))
            {
                
                var form = new BankAccountAEVForm((Account)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Bank_Account + " " + ((Account)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

              
            }
            else if (entity.GetType() == typeof(ProfitCentre))
            {
                
                var form = new ProfitCentreAEVForm((ProfitCentre)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Profit_Centre + " " + ((ProfitCentre)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

                
            }
            else if (entity.GetType() == typeof(Desk))
            {
               

                var form = new DeskAEVForm((Desk)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Desk + " " + ((Desk)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

               
            }
            else if (entity.GetType() == typeof(ClearingHouse))
            {
               

                var form = new ClearingHouseAEVForm((ClearingHouse)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Clearing_House + " " + ((ClearingHouse)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();
            }
            else if (entity.GetType() == typeof(CompanySubtype))
            {
                var form = new CompanySubtypeAEVForm((CompanySubtype)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Company_Subtype + " " + ((CompanySubtype)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();
            }
            else if (entity.GetType() == typeof(VesselPool))
            {
                
                var form = new VesselPoolAEVForm((VesselPool)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Vessel_Pool + " " + ((VesselPool)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (entity.GetType() == typeof(Index))
            {
                var form = new IndexAEVForm((Index)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Index + " " + ((Index)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
             
                form.Show();

            }
            else if (entity.GetType() == typeof(User))
            {
               
                var form = new UserAEVForm((User)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_User + " " + ((User)entity).Login };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

               
            }
            else if (entity.GetType() == typeof(Contact))
            {
                
                var form = new ContactAEVForm((Contact) entity, FormActionTypeEnum.Edit)
                               {MdiParent = this, Text = Strings.Edit_Contact + " " + ((Contact) entity).Name};
                form.OnDataSaved += AEVFormDataSaved;

                form.Show();

            }
            else if (entity.GetType() == typeof(Loan))
            {
                var form = new LoanAEVForm((Loan) entity, FormActionTypeEnum.Edit)
                               {MdiParent = this, Text = Strings.Edit_Loan + " " + ((Loan) entity).ExternalCode};
                form.OnDataSaved += AEVFormDataSaved;

                form.Show();

            }
            else if (entity.GetType() == typeof(InterestReferenceRate))
            {
               
                var form = new InterestReferenceRateAEVForm((InterestReferenceRate) entity, FormActionTypeEnum.Edit)
                               {MdiParent = this, Text = Strings.Edit_Interest_Reference_Rate};
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (entity.GetType() == typeof(ExchangeRate))
            {
                
                var form = new ExchangeRateAEVForm((ExchangeRate)entity, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Exchange_Rate };
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

            }
            else if (entity.GetType() == typeof(AppParameter))
            {
                var form = new AppParameterAEVForm((AppParameter)entity) { MdiParent = this, Text = Strings.Edit_Application_Parameter };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
        }

        private void ViewEntityEvent(DomainObject entity)
        {
            if (InvokeRequired)
            {
                Action<DomainObject> action = ViewEntityEvent;
                Invoke(action, entity);
                return;
            }

            if (entity.GetType() == typeof (Company))
            {
               
                var form = new CompanyAEVForm((Company) entity, FormActionTypeEnum.View)
                               {MdiParent = this, Text = Strings.View_Company};
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

            }
            else if (entity.GetType() == typeof (Trader))
            {
                
                var form = new TraderAEVForm((Trader) entity, FormActionTypeEnum.View)
                               {MdiParent = this, Text = Strings.View_Trader + " " + ((Trader) entity).Name};
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (entity.GetType() == typeof (Vessel))
            {
                
                var form = new VesselAEVForm((Vessel) entity, FormActionTypeEnum.View)
                               {MdiParent = this, Text = Strings.View_Vessel + " " + ((Vessel) entity).Name};
                form.OnDataSaved += AEVFormDataSaved;

                form.Show();

            }
            else if (entity.GetType() == typeof(Book))
            {
                var form = new BookAEVForm((Book)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Book + " " + ((Book)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (entity.GetType() == typeof(Account))
            {
                
                var form = new BankAccountAEVForm((Account)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Bank_Account + " " + ((Account)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
             
                form.Show();

            }
            else if (entity.GetType() == typeof(ProfitCentre))
            {
                
                var form = new ProfitCentreAEVForm((ProfitCentre)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Profit_Centre + " " + ((ProfitCentre)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

            }
            else if (entity.GetType() == typeof(Desk))
            {
                var form = new DeskAEVForm((Desk)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Desk + " " + ((Desk)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();

            }
            else if (entity.GetType() == typeof(ClearingHouse))
            {
                
                var form = new ClearingHouseAEVForm((ClearingHouse)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Clearing_House + " " + ((ClearingHouse)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();

            }
            else if (entity.GetType() == typeof(CompanySubtype))
            {
               

                var form = new CompanySubtypeAEVForm((CompanySubtype)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Company_Subtype + " " + ((CompanySubtype)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (entity.GetType() == typeof(VesselPool))
            {
                var form = new VesselPoolAEVForm((VesselPool)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Vessel_Pool + " " + ((VesselPool)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();

            }
            else if (entity.GetType() == typeof(Index))
            {
               
                var form = new IndexAEVForm((Index)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Index + " " + ((Index)entity).Name };
                form.OnDataSaved += AEVFormDataSaved;
               
                form.Show();

            }
            else if (entity.GetType() == typeof(User))
            {
                
                var form = new UserAEVForm((User)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_User + " " + ((User)entity).Login };
                form.OnDataSaved += AEVFormDataSaved;
              
                form.Show();

            }
            if (entity.GetType() == typeof(Contact))
            {
               

                var form = new ContactAEVForm((Contact)entity, FormActionTypeEnum.View) { MdiParent = this, Text = Strings.View_Contact };
                form.OnDataSaved += AEVFormDataSaved;
                
                form.Show();
                
            }
            else if (entity.GetType() == typeof(Loan))
            {

                var form = new LoanAEVForm((Loan) entity, FormActionTypeEnum.View)
                               {MdiParent = this, Text = Strings.View_Loan + " " + ((Loan) entity).ExternalCode};
                form.OnDataSaved += AEVFormDataSaved;

                form.Show();
            }
        }

        private void ManageInstallmentsEvent(Loan loan)
        {
            

            var form = new InstallmentAEVForm(loan, FormActionTypeEnum.Edit) { MdiParent = this, Text = Strings.Edit_Loan_Installments + " " + ((Loan)loan).ExternalCode };
            form.OnDataSaved += AEVFormDataSaved;
         
            form.Show();

        }

        private void OnViewChart(MemoryStream chartData, string title)
        {
            if (InvokeRequired)
            {
                Action<MemoryStream, string> action = OnViewChart;
                Invoke(action, chartData, title);
                return;
            }

           
            var form = new ChartForm(chartData) { MdiParent = this, Text = title};
            form.OnDataSaved += AEVFormDataSaved;
    
            form.Show();
        }

        private void OnMonteCarloSimulation(MCSimulation mcSimulation, Dictionary<DateTime, List<double>> monthFrequency, List<double> cashBounds, List<Dictionary<DateTime, decimal>> allValues)
        {
            if (InvokeRequired)
            {
                Action<MCSimulation, Dictionary<DateTime, List<double>>, List<double>, List<Dictionary<DateTime, decimal>>> action = OnMonteCarloSimulation;
                Invoke(action, mcSimulation, monthFrequency, cashBounds, allValues);
                return;
            }

            var form = new MonteCarloSimulationForm(mcSimulation, monthFrequency, cashBounds, allValues) { MdiParent = this, Text = Strings.Monte_Carlo_Simulation + ": " + mcSimulation.Info.Name };
            form.OnDataSaved += AEVFormDataSaved;
            form.Show();
        }

        private void OnViewTrade(long tradeId, long tradeInfoId, string externalCode, TradeTypeEnum tradeType)
        {
           
            var form = new AddEditViewTradeForm(FormActionTypeEnum.View, tradeInfoId, tradeType)
                           {MdiParent = this, Text = Strings.View_Trade + " " + externalCode};
            form.OnDataSaved += AEVFormDataSaved;
            form.OnViewTrade += OnViewTrade;
          
            form.Show();
        }

        private void OnEditTrade(long tradeId, long tradeInfoId, string externalCode, TradeTypeEnum tradeType)
        {
           

            var form = new AddEditViewTradeForm(FormActionTypeEnum.Edit, tradeInfoId, tradeType) { MdiParent = this, Text = Strings.Edit_Trade + " " + externalCode };
            form.OnDataSaved += AEVFormDataSaved;
            form.OnViewTrade += OnViewTrade;
           
            form.Show();

           
        }

        private void OnAddEditViewCashFlowItemEvent(FormActionTypeEnum formActionTypeEnum, CashFlowItem cashFlowItem)
        {
            if(formActionTypeEnum == FormActionTypeEnum.Add)
            {
                
                var form = new CashFlowItemAEVForm() { MdiParent = this, Text = Strings.Add_Cashflow_Item };
                form.OnDataSaved += AEVFormDataSaved;
                form.AddEditViewCashFlowItemClosedEvent += OnAddEditViewCashFlowItemClosedEvent;
               
                form.Show();

            }
            else
            {
                

                string text = formActionTypeEnum == FormActionTypeEnum.Edit
                    ? "Edit Cash Flow Item " + cashFlowItem.Name : "View Cash Flow Item " + cashFlowItem.Name;

                
                var form = new CashFlowItemAEVForm(cashFlowItem, formActionTypeEnum) { MdiParent = this, Text = text };
                form.OnDataSaved += AEVFormDataSaved;
                form.AddEditViewCashFlowItemClosedEvent += OnAddEditViewCashFlowItemClosedEvent;
             
                form.Show();

            }
        }

        private void OnAddEditViewCashFlowItemClosedEvent(long? value)
        {
            /*foreach (
                    NavBarItemLink link in
                        nbgAdministrationActions.ItemLinks.Cast<NavBarItemLink>().Where(
                            link => link.Item.Tag != null &&
                            link.Item.Tag.GetType() == typeof(CashFlowManagementForm)))
            {
                ((CashFlowManagementForm)link.Item.Tag).RefreshCashFlowItems();
                link.PerformClick();
                return;
            }*/
        }

        private void OnRatiosReportEvent(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            if (InvokeRequired)
            {
                Action<ViewTradeGeneralInfo> action = OnRatiosReportEvent;
                Invoke(action, viewTradesGeneralInfo);
                return;
            }

            var form = new PositionMTMRatiosReportForm(viewTradesGeneralInfo) { MdiParent = this, Text = "Ratios Report" };
            form.Show();
        }

        private void OnOpenActivateTradesForm()
        {
            var form = new InactiveTradesForm() { MdiParent = this, Text = "Activate Trades" };
            form.FormClosed += SimpleFormClosed;
            form.OnTradeActivated += OnTradeActivated;

            form.Show();
        }

        private void OnTradeDeactivated()
        {
            foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(InactiveTradesForm)))
            {
                ((InactiveTradesForm)page.MdiChild).RefreshData();
                break;
            }
        }

        private void OnTradeActivated()
        {
            foreach (
                    XtraMdiTabPage page in
                        mdiManager.Pages.Cast<XtraMdiTabPage>().Where(
                            page => page.MdiChild.GetType() == typeof(ViewTradesForm)))
            {
                ((ViewTradesForm)page.MdiChild).RefreshData();
                break;
            }
        }

        private void OnGenerateMTM(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            if (InvokeRequired)
            {
                Action<ViewTradeGeneralInfo> action = OnGenerateMTM;
                Invoke(action, viewTradesGeneralInfo);
                return;
            }

            var form = new GenerateMTMForm(viewTradesGeneralInfo) { MdiParent = this, Text = "Mark To Market" };
            form.Show();

        }

        private void OnGeneratePOS(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            if (InvokeRequired)
            {
                Action<ViewTradeGeneralInfo> action = OnGeneratePOS;
                Invoke(action, viewTradesGeneralInfo);
                return;
            }

            var form = new GeneratePOSForm(viewTradesGeneralInfo) { MdiParent = this, Text = "Position" };
            form.Show();
        }

        private void OnGenerateCF(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            if (InvokeRequired)
            {
                Action<ViewTradeGeneralInfo> action = OnGenerateMTM;
                Invoke(action, viewTradesGeneralInfo);
                return;
            }

            var form = new GenerateCFForm(viewTradesGeneralInfo) { MdiParent = this, Text = "Cash Flow" };
            form.Show();

        }

        private void OnCompanySubtypesEvent()
        {
            if (InvokeRequired)
            {
                Action action = OnCompanySubtypesEvent;
                Invoke(action);
                return;
            }

            var form = new CompanySubtypesManagementForm() { MdiParent = this, Text = Strings.Company_Subtypes };
            form.InsertEntityEvent += InsertEntityEvent;
            form.EditEntityEvent += EditEntityEvent;
            form.ViewEntityEvent += ViewEntityEvent;
            form.OnDataSaved += AEVFormDataSaved;
            //bmnuCompanySubtypes.Tag = form;
            //form.Tag = bmnuCompanySubtypes;
            form.Show();

        }

        private void OnVesselManagementEvent()
        {
            if (InvokeRequired)
            {
                Action action = OnVesselManagementEvent;
                Invoke(action);
                return;
            }

            var form = new VesselsManagementForm { MdiParent = this, Text = Strings.Vessels };
            form.InsertEntityEvent += InsertEntityEvent;
            form.EditEntityEvent += EditEntityEvent;
            form.ViewEntityEvent += ViewEntityEvent;
            form.OnDataSaved += AEVFormDataSaved;
            //bmnuVessels.Tag = form;
            //form.Tag = bmnuVessels;
            form.Show();
        }

        #endregion

        #region Proxy Calls

        private void BeginPollService()
        {
            try
            {
                SessionRegistry.Client.BeginPollService(EndPollService, null);
            }
            catch (Exception)
            {
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndPollService(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndPollService;
                Invoke(action, ar);
                return;
            }

            int? result = null;
            try
            {
                result = SessionRegistry.Client.EndPollService(ar);
            }
            catch (Exception)
            {
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this, Strings.
                                              Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region
        public event Action OnLogout;
        #endregion
    }
}