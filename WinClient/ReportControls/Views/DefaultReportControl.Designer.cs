﻿namespace Exis.WinClient.ReportControls.Views
{
    partial class DefaultReportControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkReportPersist = new System.Windows.Forms.CheckBox();
            this.txtReportTitle = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblReportPersist = new System.Windows.Forms.Label();
            this.lblReportFormat = new System.Windows.Forms.Label();
            this.lblReportTitle = new System.Windows.Forms.Label();
            this.btnCreateReport = new System.Windows.Forms.Button();
            this.gbReportProperties = new System.Windows.Forms.GroupBox();
            this.gbReportProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkReportPersist
            // 
            this.chkReportPersist.AutoSize = true;
            this.chkReportPersist.Location = new System.Drawing.Point(135, 76);
            this.chkReportPersist.Name = "chkReportPersist";
            this.chkReportPersist.Size = new System.Drawing.Size(15, 14);
            this.chkReportPersist.TabIndex = 41;
            this.chkReportPersist.UseVisualStyleBackColor = true;
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Location = new System.Drawing.Point(135, 19);
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Size = new System.Drawing.Size(260, 21);
            this.txtReportTitle.TabIndex = 11;
            // 
            // btnClose
            // 
            this.btnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClose.Location = new System.Drawing.Point(8, 196);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 32);
            this.btnClose.TabIndex = 129;
            // 
            // lblReportPersist
            // 
            this.lblReportPersist.Location = new System.Drawing.Point(6, 72);
            this.lblReportPersist.Name = "lblReportPersist";
            this.lblReportPersist.Size = new System.Drawing.Size(123, 20);
            this.lblReportPersist.TabIndex = 39;
            this.lblReportPersist.Text = "Persist in DB:";
            this.lblReportPersist.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReportFormat
            // 
            this.lblReportFormat.Location = new System.Drawing.Point(6, 45);
            this.lblReportFormat.Name = "lblReportFormat";
            this.lblReportFormat.Size = new System.Drawing.Size(123, 20);
            this.lblReportFormat.TabIndex = 36;
            this.lblReportFormat.Text = "Format:";
            this.lblReportFormat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReportTitle
            // 
            this.lblReportTitle.Location = new System.Drawing.Point(6, 18);
            this.lblReportTitle.Name = "lblReportTitle";
            this.lblReportTitle.Size = new System.Drawing.Size(123, 20);
            this.lblReportTitle.TabIndex = 2;
            this.lblReportTitle.Text = "Title:";
            this.lblReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCreateReport
            // 
            this.btnCreateReport.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCreateReport.Location = new System.Drawing.Point(312, 196);
            this.btnCreateReport.Name = "btnCreateReport";
            this.btnCreateReport.Size = new System.Drawing.Size(123, 32);
            this.btnCreateReport.TabIndex = 128;
            this.btnCreateReport.Text = "Create";
            // 
            // gbReportProperties
            // 
            this.gbReportProperties.Controls.Add(this.chkReportPersist);
            this.gbReportProperties.Controls.Add(this.txtReportTitle);
            this.gbReportProperties.Controls.Add(this.lblReportPersist);
            this.gbReportProperties.Controls.Add(this.lblReportFormat);
            this.gbReportProperties.Controls.Add(this.lblReportTitle);
            this.gbReportProperties.Location = new System.Drawing.Point(8, 88);
            this.gbReportProperties.Name = "gbReportProperties";
            this.gbReportProperties.Size = new System.Drawing.Size(427, 102);
            this.gbReportProperties.TabIndex = 127;
            this.gbReportProperties.TabStop = false;
            this.gbReportProperties.Text = "Report Properties";
            // 
            // DefaultReportControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCreateReport);
            this.Controls.Add(this.gbReportProperties);
            this.Name = "DefaultReportControl";
            this.Size = new System.Drawing.Size(442, 317);
            this.gbReportProperties.ResumeLayout(false);
            this.gbReportProperties.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkReportPersist;
        private System.Windows.Forms.TextBox txtReportTitle;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblReportPersist;
        private System.Windows.Forms.Label lblReportFormat;
        private System.Windows.Forms.Label lblReportTitle;
        private System.Windows.Forms.Button btnCreateReport;
        private System.Windows.Forms.GroupBox gbReportProperties;
    }
}
