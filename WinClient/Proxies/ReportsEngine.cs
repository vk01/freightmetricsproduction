﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using Exis.ServiceInterfaces;
using Exis.WCFExtensions;
using Exis.WinClient.General;

namespace Exis.WinClient.ReportsEngineService
{
    public partial class ReportsServiceClient : HeaderClientBase<IReportsService, ProgramInfo>, IReportsService
    {
        public ReportsServiceClient(Binding binding, EndpointAddress remoteAddress) :
            base(SessionRegistry.ProgramInfo, binding, remoteAddress)
        {
        }

        public void Test()
        {
            throw new NotImplementedException();
        }
    }
}