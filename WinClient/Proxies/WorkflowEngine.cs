﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Exis.Domain;
using Exis.WinClient.ServiceInterfaces;
using Exis.ServiceInterfaces;
using Exis.WinClient.General;
using Exis.WCFExtensions;

namespace Exis.WinClient.WorkflowEngineService
{
    public partial class WorkflowServiceClientReqRep : HeaderClientBase<IWorkflowServiceReqRep, ProgramInfo>, IWorkflowServiceReqRep
    {
        public WorkflowServiceClientReqRep(Binding binding, EndpointAddress remoteAddress) :
            base(SessionRegistry.ProgramInfo, binding, remoteAddress)
        {
        }

        public int? ProcessCase(long CaseId)
        {
            return base.Channel.ProcessCase(CaseId);
        }

        public IAsyncResult BeginProcessCase(long CaseId, AsyncCallback callback, object asyncState)
        {
            return base.Channel.BeginProcessCase(CaseId, callback, asyncState);
        }

        public int? EndProcessCase(IAsyncResult result)
        {
            return base.Channel.EndProcessCase(result);
        }
    }

    public partial class WorkflowServiceClientOneWay : HeaderClientBase<IWorkflowServiceOneWay, ProgramInfo>, IWorkflowServiceOneWay
    {
        public WorkflowServiceClientOneWay(Binding binding, EndpointAddress remoteAddress) :
            base(SessionRegistry.ProgramInfo, binding, remoteAddress)
        {
        }

        public void ProcessCaseOneWay(long CaseId)
        {
            base.Channel.ProcessCaseOneWay(CaseId);
        }

        public IAsyncResult BeginProcessCaseOneWay(long CaseId, AsyncCallback callback, object asyncState)
        {
            return base.Channel.BeginProcessCaseOneWay(CaseId, callback, asyncState);
        }

        public void EndProcessCaseOneWay(IAsyncResult result)
        {
            base.Channel.EndProcessCaseOneWay(result);
        }
    }
}