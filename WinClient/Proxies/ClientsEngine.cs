using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Exis.Domain;
using Exis.WCFExtensions;
using Exis.WinClient.General;
using System.Xml;

namespace Exis.WinClient.ClientsEngineService
{
    public class ClientsServiceClient : HeaderClientBase<IClientsServiceClient, ProgramInfo>, IClientsServiceClient
    {
        public ClientsServiceClient(Binding binding, EndpointAddress remoteAddress)
            : base(SessionRegistry.ProgramInfo, binding, remoteAddress)
        {
        }

        #region Administration

        public IAsyncResult BeginAEVCompanyInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVCompanyInitializationData(callback, state);
        }

        public int? EndAEVCompanyInitializationData(out List<Company> companies, out List<CompanySubtype> companySubtypes, out List<Contact> managers, IAsyncResult ar)
        {
            return Channel.EndAEVCompanyInitializationData(out companies, out companySubtypes, out managers, ar);
        }

        public IAsyncResult BeginAEVTraderInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVTraderInitializationData(callback, state);
        }

        public int? EndAEVTraderInitializationData(out List<Company> companies, out List<Market> markets,
                                                   out List<ProfitCentre> profitCentres, out List<Desk> desks, out List<Book> books, IAsyncResult ar)
        {
            return Channel.EndAEVTraderInitializationData(out companies, out markets,
                                                          out profitCentres, out desks, out books, ar);
        }

        public IAsyncResult BeginAEVVesselTypeInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVVesselTypeInitializationData(callback, state);
        }

        public int? EndAEVVesselTypeInitializationData(out List<Market> markets, IAsyncResult ar)
        {
            return Channel.EndAEVVesselTypeInitializationData(out markets, ar);
        }

        public IAsyncResult BeginAEVVesselInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVVesselInitializationData(callback, state);
        }

        public int? EndAEVVesselInitializationData(out List<Company> companies, out List<Market> markets, out List<Contact> contacts, IAsyncResult ar)
        {
            return Channel.EndAEVVesselInitializationData(out companies, out markets, out contacts, ar);
        }

        public IAsyncResult BeginAEVBookInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVBookInitializationData(callback, state);
        }

        public int? EndAEVBookInitializationData(out List<Book> books, IAsyncResult ar)
        {
            return Channel.EndAEVBookInitializationData(out books, ar);
        }

        public IAsyncResult BeginAEVBankAccountInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVBankAccountInitializationData(callback, state);
        }

        public int? EndAEVBankAccountInitializationData(out List<Company> companies, out List<Company> banks, out List<Currency> currencies, IAsyncResult ar)
        {
            return Channel.EndAEVBankAccountInitializationData(out companies, out banks, out currencies, ar);
        }

        public IAsyncResult BeginAEVVesselPoolInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVVesselPoolInitializationData(callback, state);
        }

        public int? EndAEVVesselPoolInitializationData(out List<Vessel> vessels, IAsyncResult ar)
        {
            return Channel.EndAEVVesselPoolInitializationData(out vessels, ar);
        }

        public IAsyncResult BeginAEVIndexInitializationData(long? indexId, AsyncCallback callback, object state)
        {
            return Channel.BeginAEVIndexInitializationData(indexId, callback, state);
        }

        public int? EndAEVIndexInitializationData(out List<Market> markets, out List<Index> indexes, out List<IndexCustomValue> customIndexValues, out List<AverageIndexesAssoc> averageIndexesAssocs, IAsyncResult ar)
        {
            return Channel.EndAEVIndexInitializationData(out markets, out indexes, out customIndexValues, out averageIndexesAssocs, ar);
        }

        public IAsyncResult BeginAEVVesselPoolHistory(long vesselPoolId, AsyncCallback callback, object state)
        {
            return Channel.BeginAEVVesselPoolHistory(vesselPoolId, callback, state);
        }

        public int? EndAEVVesselPoolHistory(out List<VesselPoolInfo> vesselPoolInfos, IAsyncResult ar)
        {
            return Channel.EndAEVVesselPoolHistory(out vesselPoolInfos, ar);
        }

        public IAsyncResult BeginAEEntity(bool isEdit, DomainObject entity, AsyncCallback callback, object state)
        {
            return Channel.BeginAEEntity(isEdit, entity, callback, state);
        }

        public int? EndAEEntity(IAsyncResult ar)
        {
            return Channel.EndAEEntity(ar);
        }

        public IAsyncResult BeginGetEntities(DomainObject entity, AsyncCallback callback, object state)
        {
            return Channel.BeginGetEntities(entity, callback, state);
        }

        public int? EndGetEntities(out List<DomainObject> entities, IAsyncResult ar)
        {
            return Channel.EndGetEntities(out entities, ar);
        }

        public int? InsertBalticValues(List<IndexFFAValue> indexFfaValues, List<IndexSpotValue> indexSpotValues, List<IndexBOAValue> indexBoaValues, bool isFtpDownload, out DateTime lastDownLoadDate, out DateTime lastImportDate)
        {
            return Channel.InsertBalticValues(indexFfaValues, indexSpotValues, indexBoaValues, isFtpDownload, out lastDownLoadDate, out lastImportDate);
        }

        public IAsyncResult BeginInsertBalticValues(List<IndexFFAValue> indexFfaValues, List<IndexSpotValue> indexSpotValues, List<IndexBOAValue> indexBoaValues, bool isFtpDownload, AsyncCallback callback, object state)
        {
            return Channel.BeginInsertBalticValues(indexFfaValues, indexSpotValues, indexBoaValues, isFtpDownload, callback, state);
        }

        public int? EndInsertBalticValues(out DateTime lastDownLoadDate, out DateTime lastImportDate, IAsyncResult ar)
        {
            return Channel.EndInsertBalticValues(out lastDownLoadDate, out lastImportDate,  ar);
        }

        public IAsyncResult BeginBalticExchangeInputDataInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginBalticExchangeInputDataInitializationData(callback, state);
        }

        public int? EndBalticExchangeInputDataInitializationData(out List<Index> indexes, out List<AppParameter> appParameters, IAsyncResult ar)
        {
            return Channel.EndBalticExchangeInputDataInitializationData(out indexes, out appParameters, ar);
        }

        public IAsyncResult BeginFtpBatchProcessingFiles(string ftpsource, string ftpArchiveUri, string ftpUri, string ftpXmlFolder, string ftpUsername, string ftpPassword,
            bool isFtpDownload, List<Index> indexes,
            Dictionary<string, int> monthStr, AsyncCallback callback, object state)
        {
            return Channel.BeginFtpBatchProcessingFiles(ftpsource, ftpArchiveUri, ftpUri, ftpXmlFolder, ftpUsername, ftpPassword,
             isFtpDownload, indexes, monthStr, callback, state);
        }

        public int? EndFtpBatchProcessingFiles(out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages, IAsyncResult ar)
        {
            return Channel.EndFtpBatchProcessingFiles(out lastDownLoadDate, out lastImportDate, out errorMessages, ar);
        }

        public IAsyncResult BeginLocalProcessingFileXML(XmlElement objRequestElement, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, AsyncCallback callback, object state)
        {
            return Channel.BeginLocalProcessingFileXML(objRequestElement, fileName, indexes, monthStr, callback, state);
        }

        public int? EndLocalProcessingFileXML(out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages, IAsyncResult ar)
        {
            return Channel.EndLocalProcessingFileXML(out lastDownLoadDate, out lastImportDate, out errorMessages, ar);
        }

        public IAsyncResult BeginLocalProcessingFileExcel(List<ParseExcelIndexValue> indexValues, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, AsyncCallback callback, object state)
        {
            return Channel.BeginLocalProcessingFileExcel(indexValues, fileName, indexes, monthStr, callback, state);
        }

        public int? EndLocalProcessingFileExcel(out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages, IAsyncResult ar)
        {
            return Channel.EndLocalProcessingFileExcel(out lastDownLoadDate, out lastImportDate, out errorMessages, ar);
        }

        public IAsyncResult BeginAEVUserInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVUserInitializationData(callback, state);
        }

        public int? EndAEVUserInitializationData(out List<Trader> traders, out List<Book> books, IAsyncResult ar)
        {
            return Channel.EndAEVUserInitializationData(out traders, out books, ar);
        }

        public IAsyncResult BeginGetUserAssociatedBooks(AsyncCallback callback, object state)
        {
            return Channel.BeginGetUserAssociatedBooks(callback, state);
        }

        public int? EndGetUserAssociatedBooks(out List<Book> books, IAsyncResult ar)
        {
            return Channel.EndGetUserAssociatedBooks(out books, ar);
        }

        public IAsyncResult BeginAEVLoanInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVLoanInitializationData(callback, state);
        }

        public int? EndAEVLoanInitializationData(out List<Company> banks, out List<Vessel> vessels, out List<Company> obligors, out List<Company> guarantors,
            out List<Currency> currencies, out List<Company> borrowers, IAsyncResult ar)
        {
            return Channel.EndAEVLoanInitializationData(out banks, out vessels, out obligors, out guarantors,
                                                        out currencies, out borrowers, ar);
        }

        public IAsyncResult BeginAEInstallments(long loanId, List<LoanPrincipalInstallment> loanPrincipalInstallments,
                                 List<LoanInterestInstallment> loanInterestInstallments, AsyncCallback callback, object state)
        {
            return Channel.BeginAEInstallments(loanId, loanPrincipalInstallments, loanInterestInstallments, callback, state);
        }

        public int? EndAEInstallments(IAsyncResult ar)
        {
            return Channel.EndAEInstallments(ar);
        }

        public IAsyncResult BeginAERiskParametersInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAERiskParametersInitializationData(callback, state);
        }

        public int? EndAERiskParametersInitializationData(out List<Index> indexes, out List<RiskVolatility> riskVolatilities, out List<RiskCorrelation> riskCorrelations, IAsyncResult ar)
        {
            return Channel.EndAERiskParametersInitializationData(out indexes, out riskVolatilities, out riskCorrelations, ar);
        }

        public IAsyncResult BeginAERiskParameteres(List<RiskVolatility> riskVolatilities, List<RiskCorrelation> riskCorrelations, AsyncCallback callback, object state)
        {
            return Channel.BeginAERiskParameteres(riskVolatilities, riskCorrelations, callback, state);
        }

        public int? EndAERiskParameteres(IAsyncResult ar)
        {
            return Channel.EndAERiskParameteres(ar);
        }

        public IAsyncResult BeginAEVHiearchyInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVHiearchyInitializationData(callback, state);
        }

        public int? EndAEVHiearchyInitializationData(out List<HierarchyNode> hierarchyNodes, out List<HierarchyNodeType> hierarchyNodeTypes, IAsyncResult ar)
        {
            return Channel.EndAEVHiearchyInitializationData(out hierarchyNodes, out hierarchyNodeTypes, ar);
        }

        public IAsyncResult BeginAEVHierarchyNodes(List<HierarchyNode> hierarchyNodes, AsyncCallback callback, object state)
        {
            return Channel.BeginAEVHierarchyNodes(hierarchyNodes, callback, state);
        }

        public int? EndAEVHierarchyNodes(IAsyncResult ar)
        {
            return Channel.EndAEVHierarchyNodes(ar);
        }

        public IAsyncResult BeginAEVCashFlowInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVCashFlowInitializationData(callback, state);
        }

        public int? EndAEVCashFlowInitializationData(out List<CashFlowModel> cashFlowModels, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowGroupItem> cashFlowGroupsItems, IAsyncResult ar)
        {
            return Channel.EndAEVCashFlowInitializationData(out cashFlowModels, out cashFlowGroups, out cashFlowItems, out cashFlowModelGroups, out cashFlowGroupsItems, ar);
        }

        public IAsyncResult BeginAEVCashFlowItemInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVCashFlowItemInitializationData(callback, state);
        }

        public int? EndAEVCashFlowItemInitializationData(out List<Currency> currencies, IAsyncResult ar)
        {
            return Channel.EndAEVCashFlowItemInitializationData(out currencies, ar);
        }

        public IAsyncResult BeginGetCashFlowItems(AsyncCallback callback, object state)
        {
            return Channel.BeginGetCashFlowItems(callback, state);
        }

        public int? EndGetCashFlowItems(out List<CashFlowItem> cashFlowItems, IAsyncResult ar)
        {
            return Channel.EndGetCashFlowItems(out cashFlowItems, ar);
        }

        public IAsyncResult BeginAECashFlowEntities(List<CashFlowItem> cashFlowItems, List<CashFlowGroup> cashFlowGroups, List<CashFlowModel> cashFlowModels, List<CashFlowGroupItem> cashFlowGroupItems, List<CashFlowModelGroup> cashFlowModelsGroups, AsyncCallback callback, object state)
        {
            return Channel.BeginAECashFlowEntities(cashFlowItems, cashFlowGroups, cashFlowModels, cashFlowGroupItems, cashFlowModelsGroups, callback, state);
        }

        public int? EndAECashFlowEntities(IAsyncResult ar)
        {
            return Channel.EndAECashFlowEntities(ar);
        }

        public IAsyncResult BeginAEVInterestReferenceRateInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVInterestReferenceRateInitializationData(callback, state);
        }

        public int? EndAEVInterestReferenceRateInitializationData(out List<Currency> currencies, IAsyncResult ar)
        {
            return Channel.EndAEVInterestReferenceRateInitializationData(out currencies, ar);
        }

        public IAsyncResult BeginDeleteInterestReferenceRate(long interestReferenceRateId, AsyncCallback callback, object state)
        {
            return Channel.BeginDeleteInterestReferenceRate(interestReferenceRateId, callback, state);
        }

        public int? EndDeleteInterestReferenceRate(IAsyncResult ar)
        {
            return Channel.EndDeleteInterestReferenceRate(ar);
        }

        public IAsyncResult BeginAEInterestReferenceRate(bool isEdit, InterestReferenceRate interestReferenceRate, AsyncCallback callback, object state)
        {
            return Channel.BeginAEInterestReferenceRate(isEdit, interestReferenceRate, callback, state);
        }

        public int? EndAEInterestReferenceRate(IAsyncResult ar)
        {
            return Channel.EndAEInterestReferenceRate(ar);
        }

        public IAsyncResult BeginAEVExchangeRateInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginAEVExchangeRateInitializationData(callback, state);
        }

        public int? EndAEVExchangeRateInitializationData(out List<Currency> currencies, IAsyncResult ar)
        {
            return Channel.EndAEVExchangeRateInitializationData(out currencies, ar);
        }

        public IAsyncResult BeginDeleteExchangeRate(long exchangeRateId, AsyncCallback callback, object state)
        {
            return Channel.BeginDeleteExchangeRate(exchangeRateId, callback, state);
        }

        public int? EndDeleteExchangeRate(IAsyncResult ar)
        {
            return Channel.EndDeleteExchangeRate(ar);
        }

        public IAsyncResult BeginApplicationParametersInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginApplicationParametersInitializationData(callback, state);
        }

        public int? EndApplicationParametersInitializationData(out List<AppParameter> appParameters, IAsyncResult ar)
        {
            return Channel.EndApplicationParametersInitializationData(out appParameters, ar);
        }

        public IAsyncResult BeginAEIndex(bool isEdit, Index index, List<AverageIndexesAssoc> averageIndexesAssocs, AsyncCallback callback, object state)
        {
            return Channel.BeginAEIndex(isEdit, index, averageIndexesAssocs, callback, state);
        }

        public int? EndAEIndex(IAsyncResult ar)
        {
            return Channel.EndAEIndex(ar);
        }

        public IAsyncResult BeginCheckUserHasPermissionOnTrader(long userId, long traderId, AsyncCallback callback, object state)
        {
            return Channel.BeginCheckUserHasPermissionOnTrader(userId, traderId, callback, state);
        }

        public int? EndCheckUserHasPermissionOnTrader(out bool hasPermission, IAsyncResult ar)
        {
            return Channel.EndCheckUserHasPermissionOnTrader(out hasPermission, ar);
        }

        #endregion

        #region Other

        public IAsyncResult BeginPollService(AsyncCallback callback, object state)
        {
            return Channel.BeginPollService(callback, state);
        }

        public int? EndPollService(IAsyncResult ar)
        {
            return Channel.EndPollService(ar);
        }

        public IAsyncResult BeginValidateCredentials(string userName, string password,
                                                     AsyncCallback callback, object state)
        {
            return Channel.BeginValidateCredentials(userName, password, callback, state);
        }

        public int? EndValidateCredentials(out User user, IAsyncResult ar)
        {
            return Channel.EndValidateCredentials(out user, ar);
        }


        public IAsyncResult BeginAEVTradeInitializationData(long? tradeInfoId, FormActionTypeEnum action, bool getExtraData,
                                                            AsyncCallback callback, object state)
        {
            return Channel.BeginAEVTradeInitializationData(tradeInfoId, action, getExtraData, callback, state);
        }

        public int? EndAEVTradeInitializationData(out List<Company> companies,
                                                  out List<Company> counterparties, out List<Trader> traders,
                                                  out List<Company> brokers, out List<Market> marketSegments,
                                                  out List<Region> marketRegions,
                                                  out List<Route> marketRoutes, out List<Vessel> vessels,
                                                  out List<Index> indexes, out List<Company> banks,
                                                  out List<Account> accounts, out List<ClearingHouse> clearingHouses,
                                                  out List<VesselPool> vesselPools, out List<VesselBenchmarking> vesselBenchmarkings,
                                                  out List<TradeVersionInfo> tradeVersionInfos, out Trade trade, out List<TradeVersionInfo> tcTradeVersionInfos,
                                                  IAsyncResult ar)
        {
            return Channel.EndAEVTradeInitializationData(out companies, out counterparties, out traders, out brokers,
                                                         out marketSegments, out marketRegions, out marketRoutes,
                                                         out vessels, out indexes, out banks,
                                                         out accounts, out clearingHouses, out vesselPools, out vesselBenchmarkings, out tradeVersionInfos, out trade, out tcTradeVersionInfos, ar);
        }

        public IAsyncResult BeginInsertNewTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo, List<TradeTcInfoLeg> tradeTcInfoLegs,
            TradeFfaInfo tradeFfaInfo, TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs, TradeOptionInfo tradeOptionInfo,
            List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades, AsyncCallback callback, object state)
        {
            return Channel.BeginInsertNewTrade(trade, tradeInfo, tradeTcInfo,
                                               tradeTcInfoLegs, tradeFfaInfo, tradeCargoInfo, tradeCargoInfoLegs, tradeOptionInfo,
                                               tradeBrokerInfos, tradeInfoBooks, poolTrades,
                                               callback, state);
        }

        public int? EndInsertNewTrade(IAsyncResult ar)
        {
            return Channel.EndInsertNewTrade(ar);
        }

        public IAsyncResult BeginUpdateTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                         List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                         TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs, TradeOptionInfo tradeOptionInfo,
                         List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades, AsyncCallback callback, object state)
        {
            return Channel.BeginUpdateTrade(trade, tradeInfo, tradeTcInfo, tradeTcInfoLegs, tradeFfaInfo, tradeCargoInfo,
                                            tradeCargoInfoLegs, tradeOptionInfo,
                                            tradeBrokerInfos, tradeInfoBooks, poolTrades, callback, state);
        }

        public int? EndUpdateTrade(IAsyncResult ar)
        {
            return Channel.EndUpdateTrade(ar);
        }

        public IAsyncResult BeginGetEntitiesThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, DomainObject entityTypeInstanceToReturn, List<DomainObject> entityTypeFilters, AsyncCallback callback, object state)
        {
            return Channel.BeginGetEntitiesThatHaveTradesByFilters(positionDate, periodFrom, periodTo, acceptDrafts, entityTypeInstanceToReturn, entityTypeFilters,
                                                                   callback, state);
        }

        public int? EndGetEntitiesThatHaveTradesByFilters(out List<DomainObject> entities, IAsyncResult ar)
        {
            return Channel.EndGetEntitiesThatHaveTradesByFilters(out entities, ar);
        }

        public IAsyncResult BeginGetTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, List<DomainObject> entityTypeFilters, AsyncCallback callback, object state)
        {
            return Channel.BeginGetTradesByFilters(positionDate, periodFrom, periodTo, acceptDrafts, acceptMinimum, acceptOptional, entityTypeFilters, callback, state);
        }

        public int? EndGetTradesByFilters(out List<Trade> trades, IAsyncResult ar)
        {
            return Channel.EndGetTradesByFilters(out trades, ar);
        }

        public IAsyncResult BeginGeneratePositionExtraTradeTypeOptions(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, string tradeTypeItems, List<string> tradeIdentifiers, AsyncCallback callback, object state)
        {
            return Channel.BeginGeneratePositionExtraTradeTypeOptions(curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, tradeTypeItems, tradeIdentifiers, callback, state);
        }
        public IAsyncResult BeginGeneratePosition(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, List<string> tradeIdentifiers, AsyncCallback callback, object state)
        {
            return Channel.BeginGeneratePosition(curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, tradeIdentifiers, callback, state);
        }

        public int? EndGeneratePosition(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage, IAsyncResult ar)
        {
            return Channel.EndGeneratePosition(out results, out resultsWithoutWeight, out errorMessage, ar);
        }
        public int? EndGeneratePositionExtraTradeTypeOptions(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage, IAsyncResult ar)
        {
            return Channel.EndGeneratePositionExtraTradeTypeOptions(out results, out resultsWithoutWeight, out errorMessage, ar);
        }
             
        public IAsyncResult BeginGenerateMarkToMarketExtraTradeTypeOptions(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, string tradeTypeItems, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, AsyncCallback callback, object state)
        {
            return Channel.BeginGenerateMarkToMarketExtraTradeTypeOptions(positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, optionEmbeddedValue, optionNullify, MTMType, tradeTypeItems, tradeIdentifiers, tradeIdsByPhysicalFinancialType, callback, state);
        }
        public IAsyncResult BeginGenerateMarkToMarket(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, AsyncCallback callback, object state)
        {
            return Channel.BeginGenerateMarkToMarket(positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, optionEmbeddedValue, optionNullify, MTMType, tradeIdentifiers, tradeIdsByPhysicalFinancialType, callback, state);
        }

        public int? EndGenerateMarkToMarket(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage, IAsyncResult ar)
        {
            return Channel.EndGenerateMarkToMarket(out results, out embeddedValues, out indexesValues, out errorMessage, ar);
        }
        public int? EndGenerateMarkToMarketExtraTradeTypeOptions(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage, IAsyncResult ar)
        {
            return Channel.EndGenerateMarkToMarketExtraTradeTypeOptions(out results, out embeddedValues, out indexesValues, out errorMessage, ar);
        }

        //public IAsyncResult BeginGenerateMonteCarloSimulation(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns, AsyncCallback callback, object state)
        //{
        //    return Channel.BeginGenerateMonteCarloSimulation(positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, MTMType, tradeIdentifiers, noOfSimulationRuns, callback, state);
        //}

        //public int? EndGenerateMonteCarloSimulation(out Dictionary<DateTime, List<double>> monthFrequency, out List<double> cashBounds, out List<Dictionary<DateTime, decimal>> allValues, IAsyncResult ar)
        //{
        //    return Channel.EndGenerateMonteCarloSimulation(out monthFrequency, out cashBounds, out allValues, ar);
        //}

        public void GenerateMonteCarloSimulation(string mcName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns)
        {
            Channel.GenerateMonteCarloSimulation(mcName, positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, MTMType, tradeIdentifiers, noOfSimulationRuns);
        }

        public IAsyncResult BeginCalculateMTMValues(long indexId, DateTime date, AsyncCallback callback, object state)
        {
            return Channel.BeginCalculateMTMValues(indexId, date, callback, state);
        }

        public int? EndCalculateMTMValues(out Dictionary<DateTime, decimal> finalIndexValues, out Dictionary<DateTime, decimal> finalCurveValues, IAsyncResult ar)
        {
            return Channel.EndCalculateMTMValues(out finalIndexValues, out finalCurveValues, ar);
        }

        public IAsyncResult BeginGetCFModels(AsyncCallback callback, object state)
        {
            return Channel.BeginGetCFModels(callback, state);
        }

        public int? EndGetCFModels(out List<CashFlowModel> models, IAsyncResult ar)
        {
            return Channel.EndGetCFModels(out models, ar);
        }

        public IAsyncResult BeginGenerateCashFlow(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, List<string> tradeIdentifiers, long cashFlowModelId, bool calculateBreakEven, bool calculateValuation, bool calculateIRR, bool calculateBEBalloon, bool calculateBEEquity, bool calculateBEScrapValue, decimal valuationDiscountRate, AsyncCallback callback, object state)
        {
            return Channel.BeginGenerateCashFlow(positionDate, curveDate, periodFrom, periodTo, positionMethod, marketSensitivityType, marketSensitivityValue, useSpot, optionPremium, tradeIdentifiers, cashFlowModelId, calculateBreakEven, calculateValuation, calculateIRR, calculateBEBalloon, calculateBEEquity, calculateBEScrapValue, valuationDiscountRate, callback, state);
        }

        public int? EndGenerateCashFlow(out Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowGroupItem> cashFlowGroupItems, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsAll, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsSpot, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsBalloon, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsEquity, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsScrappings, out Dictionary<string, Dictionary<DateTime, decimal>> valuationResults, out Dictionary<string, Dictionary<DateTime, decimal>> valuationDiscountFactor, out Dictionary<string, Dictionary<DateTime, decimal>> valuationPresentValue, out Dictionary<string, Dictionary<DateTime, decimal>> irrResults, IAsyncResult ar)
        {
            return Channel.EndGenerateCashFlow(out cashFlowItemResults, out cashFlowGroups, out cashFlowModelGroups, out cashFlowItems, out cashFlowGroupItems, out breakEvenResultsAll, out breakEvenResultsSpot, out breakEvenResultsBalloon, out breakEvenResultsEquity, out breakEvenResultsScrappings, out valuationResults, out valuationDiscountFactor, out valuationPresentValue, out irrResults, ar);
        }

        public IAsyncResult BeginGetIndexValues(long indexId, DateTime date, int absoluteOffset, int percentageOffset, AsyncCallback callback, object state)
        {
            return Channel.BeginGetIndexValues(indexId, date, absoluteOffset, percentageOffset, callback, state);
        }

        public int? EndGetIndexValues(out List<IndexSpotValue> spotValues, out List<IndexFFAValue> originalBFAValues, out Dictionary<DateTime, decimal> normalizedBFAValues, out Dictionary<DateTime, decimal> curveValues, IAsyncResult ar)
        {
            return Channel.EndGetIndexValues(out spotValues, out originalBFAValues, out normalizedBFAValues, out curveValues, ar);
        }

        public IAsyncResult BeginGetTradeInitializationData(long tradeId, bool fetchAllData, AsyncCallback callback, object state)
        {
            return Channel.BeginGetTradeInitializationData(tradeId, fetchAllData, callback, state);
        }

        public int? EndGetTradeInitializationData(out List<TradeVersionInfo> tradeVersionInfos, out Trade trade, out List<VesselPool> vesselPools, IAsyncResult ar)
        {
            return Channel.EndGetTradeInitializationData(out tradeVersionInfos, out trade, out vesselPools, ar);
        }

        public IAsyncResult BeginGetVesselPoolsBySignDate(DateTime signDate, AsyncCallback callback, object state)
        {
            return Channel.BeginGetVesselPoolsBySignDate(signDate, callback, state);
        }

        public int? EndGetVesselPoolsBySignDate(out List<VesselPool> vesselPools, IAsyncResult ar)
        {
            return Channel.EndGetVesselPoolsBySignDate(out vesselPools, ar);
        }

        public IAsyncResult BeginGetCompanyHierarchyThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, long? hierarchyNodeTypeId, HierarchyNodeInfo entityCompany, AsyncCallback callback, object state)
        {
            return Channel.BeginGetCompanyHierarchyThatHaveTradesByFilters(positionDate, periodFrom, periodTo, acceptDrafts,
            hierarchyNodeTypeId, entityCompany, callback, state);
        }

        public int? EndGetCompanyHierarchyThatHaveTradesByFilters(out Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentHierarchyNodes, IAsyncResult ar)
        {
            return Channel.EndGetCompanyHierarchyThatHaveTradesByFilters(out parentHierarchyNodes, ar);
        }


        public IAsyncResult BeginGetEntitiesThatHaveTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters, AsyncCallback callback, object state)
        {
            return Channel.BeginGetEntitiesThatHaveTradesByFilters2(positionDate, periodFrom, periodTo, acceptDrafts,
                                                                    hierarchyNodeTypeId, entityTypeFilters,
                                                                    callback, state);
        }

        public int? EndGetEntitiesThatHaveTradesByFilters2(out List<HierarchyNodeInfo> hierarchyNodes, IAsyncResult ar)
        {
            return Channel.EndGetEntitiesThatHaveTradesByFilters2(out hierarchyNodes, ar);
        }

        public IAsyncResult BeginGetTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays, List<HierarchyNodeInfo> entityTypeFilters, AsyncCallback callback, object state)
        {
            return Channel.BeginGetTradesByFilters2(positionDate, periodFrom, periodTo, acceptDrafts, acceptMinimum, acceptOptional, onlyNonZeroTotalDays, entityTypeFilters, callback, state);
        }

        public int? EndGetTradesByFilters2(out List<Trade> trades, out List<Company> companies, IAsyncResult ar)
        {
            return Channel.EndGetTradesByFilters2(out trades, out companies, ar);
        }

        public IAsyncResult BeginGetViewTradesInitializationData(AsyncCallback callback, object state)
        {
            return Channel.BeginGetViewTradesInitializationData(callback, state);
        }

        public int? EndGetViewTradesInitializationData(out List<Book> books, out List<Book> userAssocBooks, out List<CashFlowModel> models, out List<Index> indexes, IAsyncResult ar)
        {
            return Channel.EndGetViewTradesInitializationData(out books, out userAssocBooks, out models, out indexes, ar);
        }

        public IAsyncResult BeginImportTrades(List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, List<TmpImportCargoTrade> tmpImportCargoTrades, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades, AsyncCallback callback, object state)
        {
            return Channel.BeginImportTrades(tmpImportFfaOptionTrades, tmpImportCargoTrades, tmpImportTcInTrades, tmpImportTcOutTrades, callback, state);
        }

        public int? EndImportTrades(out string errorMessage, IAsyncResult ar)
        {
            return Channel.EndImportTrades(out errorMessage, ar);
        }

        public IAsyncResult BeginUpdateTradeStatus(TradeTypeEnum tradeTypeEnum, long identifierId, ActivationStatusEnum status, AsyncCallback callback, object state)
        {
            return Channel.BeginUpdateTradeStatus(tradeTypeEnum, identifierId, status, callback, state);
        }

        public int? EndUpdateTradeStatus(IAsyncResult ar)
        {
            return Channel.EndUpdateTradeStatus(ar);
        }

        public IAsyncResult BeginGetAverageIndexesValues(List<long> indexes, DateTime curveDate, DateTime periodFrom, DateTime periodTo, AsyncCallback callback, object state)
        {
            return Channel.BeginGetAverageIndexesValues(indexes, curveDate, periodFrom, periodTo, callback, state);
        }

        public int? EndGetAverageIndexesValues(out Dictionary<long, Dictionary<DateTime, decimal>> forwardIndexesValues, out Dictionary<long, Dictionary<DateTime, decimal>> spotIndexesValues, IAsyncResult ar)
        {
            return Channel.EndGetAverageIndexesValues(out forwardIndexesValues, out spotIndexesValues, ar);
        }

        public IAsyncResult BeginGetTradesBySelectionBooks(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts,
                                       bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays,
                                       List<HierarchyNodeInfo> entityTypeFilters, AsyncCallback callback, object state)
        {
            return Channel.BeginGetTradesBySelectionBooks(positionDate, periodFrom, periodTo, acceptDrafts,
                                                          acceptMinimum, acceptOptional, onlyNonZeroTotalDays,
                                                          entityTypeFilters, callback,
                                                          state);
        }

        public int? EndGetTradesBySelectionBooks(out List<Trade> trades, IAsyncResult ar)
        {
            return Channel.EndGetTradesBySelectionBooks(out trades, ar);
        }

        public IAsyncResult BeginEnergyGenerateMarkToMarket(DateTime curveDate, DateTime periodFrom, DateTime periodTo, AsyncCallback callback, object state)
        {
            return Channel.BeginEnergyGenerateMarkToMarket(curveDate, periodFrom, periodTo, callback, state);
        }

        public int? EndEnergyGenerateMarkToMarket(out Dictionary<DateTime, decimal> results, out string errorMessage, IAsyncResult ar)
        {
            return Channel.EndEnergyGenerateMarkToMarket(out results, out errorMessage, ar);
        }

        public IAsyncResult BeginGetInactiveTrades(AsyncCallback callback, object state)
        {
            return Channel.BeginGetInactiveTrades(callback, state);
        }

        public int? EndGetInactiveTrades(out List<Trade> trades, IAsyncResult ar)
        {
            return Channel.EndGetInactiveTrades(out trades, ar);
        }

        public IAsyncResult BeginGetMCRuns(AsyncCallback callback, object state)
        {
            return Channel.BeginGetMCRuns(callback, state);
        }

        public int? EndGetMCRuns(out List<MCSimulationInfo> mcRuns, IAsyncResult ar)
        {
            return Channel.EndGetMCRuns(out mcRuns, ar);
        }

        public IAsyncResult BeginGetMCSimulationDetails(MCSimulationInfo mcSimInfo, AsyncCallback callback, object state)
        {
            return Channel.BeginGetMCSimulationDetails(mcSimInfo, callback, state);
        }

        public int? EndGetMCSimulationDetails(out MCSimulation mcSimulation, IAsyncResult ar)
        {
            return Channel.EndGetMCSimulationDetails(out mcSimulation, ar);
        }

        public IAsyncResult BeginDeleteMCRuns(MCSimulationInfo mcSimulationInfo, AsyncCallback callback, object state)
        {
            return Channel.BeginDeleteMCRuns(mcSimulationInfo, callback, state);
        }

        public int? EndDeleteMCRuns(IAsyncResult ar)
        {
            return Channel.EndDeleteMCRuns(ar);
        }

        public IAsyncResult BeginInsertMCRuns(AsyncCallback callback, object state)
        {
            return Channel.BeginInsertMCRuns(callback, state);
        }

        public int? EndInsertMCRuns(out string message, IAsyncResult ar)
        {
            return Channel.EndInsertMCRuns(out message, ar);
        }

        #endregion
    }

    [ServiceContract(Name = "IClientsService")]
    public interface IClientsServiceClient
    {
        #region Administration

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVCompanyInitializationData(AsyncCallback callback, object state);

        int? EndAEVCompanyInitializationData(out List<Company> companies, out List<CompanySubtype> companySubtypes, out List<Contact> managers, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVTraderInitializationData(AsyncCallback callback, object state);

        int? EndAEVTraderInitializationData(out List<Company> companies, out List<Market> markets,
                                            out List<ProfitCentre> profitCentres, out List<Desk> desks, out List<Book> books, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVVesselTypeInitializationData(AsyncCallback callback, object state);

        int? EndAEVVesselTypeInitializationData(out List<Market> markets, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVVesselInitializationData(AsyncCallback callback, object state);

        int? EndAEVVesselInitializationData(out List<Company> companies, out List<Market> markets, out List<Contact> contacts, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVBookInitializationData(AsyncCallback callback, object state);

        int? EndAEVBookInitializationData(out List<Book> books, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVBankAccountInitializationData(AsyncCallback callback, object state);

        int? EndAEVBankAccountInitializationData(out List<Company> companies, out List<Company> banks, out List<Currency> currencies, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVVesselPoolInitializationData(AsyncCallback callback, object state);

        int? EndAEVVesselPoolInitializationData(out List<Vessel> vessels, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVIndexInitializationData(long? indexId, AsyncCallback callback, object state);

        int? EndAEVIndexInitializationData(out List<Market> markets, out List<Index> indexes, out List<IndexCustomValue> customIndexValues, out List<AverageIndexesAssoc> averageIndexesAssocs, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVVesselPoolHistory(long vesselPoolId, AsyncCallback callback, object state);

        int? EndAEVVesselPoolHistory(out List<VesselPoolInfo> vesselPoolInfos, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEEntity(bool isEdit, DomainObject entity, AsyncCallback callback, object state);

        int? EndAEEntity(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetEntities(DomainObject entity, AsyncCallback callback, object state);

        int? EndGetEntities(out List<DomainObject> entities, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginBalticExchangeInputDataInitializationData(AsyncCallback callback, object state);

        int? EndBalticExchangeInputDataInitializationData(out List<Index> indexes, out List<AppParameter> appParameters, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginFtpBatchProcessingFiles(string ftpsource, string ftpArchiveUri, string ftpUri, string ftpXmlFolder, string ftpUsername, string ftpPassword,
            bool isFtpDownload, List<Index> indexes,
            Dictionary<string, int> monthStr, AsyncCallback callback, object state);

        int? EndFtpBatchProcessingFiles(out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLocalProcessingFileXML(XmlElement objRequestElement, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, AsyncCallback callback, object state);

        int? EndLocalProcessingFileXML(out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLocalProcessingFileExcel(List<ParseExcelIndexValue> indexValues, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, AsyncCallback callback, object state);

        int? EndLocalProcessingFileExcel(out DateTime lastDownLoadDate, out DateTime lastImportDate, out List<string> errorMessages, IAsyncResult ar);

        [OperationContract]
        int? InsertBalticValues(List<IndexFFAValue> indexFfaValues, List<IndexSpotValue> indexSpotValues, List<IndexBOAValue> indexBoaValues, bool isFtpDownload, out DateTime lastDownLoadDate, out DateTime lastImportDate);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInsertBalticValues(List<IndexFFAValue> indexFfaValues,
                                                        List<IndexSpotValue> indexSpotValues, List<IndexBOAValue> indexBoaValues, bool isFtpDownload, AsyncCallback callback,
                                                        object state);

        int? EndInsertBalticValues(out DateTime lastDownLoadDate, out DateTime lastImportDate,  IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVUserInitializationData(AsyncCallback callback, object state);

        int? EndAEVUserInitializationData(out List<Trader> traders, out List<Book> books, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetUserAssociatedBooks(AsyncCallback callback, object state);

        int? EndGetUserAssociatedBooks(out List<Book> books, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVLoanInitializationData(AsyncCallback callback, object state);

        int? EndAEVLoanInitializationData(out List<Company> banks, out List<Vessel> vessels, out List<Company> obligors,
                                          out List<Company> guarantors,
                                          out List<Currency> currencies, out List<Company> borrowers,
                                          IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEInstallments(long loanId, List<LoanPrincipalInstallment> loanPrincipalInstallments,
                                 List<LoanInterestInstallment> loanInterestInstallments, AsyncCallback callback, object state);

        int? EndAEInstallments(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAERiskParametersInitializationData(AsyncCallback callback, object state);

        int? EndAERiskParametersInitializationData(out List<Index> indexes, out List<RiskVolatility> riskVolatilities, out List<RiskCorrelation> riskCorrelations, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAERiskParameteres(List<RiskVolatility> riskVolatilities, List<RiskCorrelation> riskCorrelations, AsyncCallback callback, object state);

        int? EndAERiskParameteres(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVHiearchyInitializationData(AsyncCallback callback, object state);

        int? EndAEVHiearchyInitializationData(out List<HierarchyNode> hierarchyNodes,
                                              out List<HierarchyNodeType> hierarchyNodeTypes, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVHierarchyNodes(List<HierarchyNode> hierarchyNodes, AsyncCallback callback, object state);

        int? EndAEVHierarchyNodes(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVCashFlowInitializationData(AsyncCallback callback, object state);

        int? EndAEVCashFlowInitializationData(out List<CashFlowModel> cashFlowModels, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowGroupItem> cashFlowGroupsItems, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVCashFlowItemInitializationData(AsyncCallback callback, object state);

        int? EndAEVCashFlowItemInitializationData(out List<Currency> currencies, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetCashFlowItems(AsyncCallback callback, object state);

        int? EndGetCashFlowItems(out List<CashFlowItem> cashFlowItems, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAECashFlowEntities(List<CashFlowItem> cashFlowItems, List<CashFlowGroup> cashFlowGroups, List<CashFlowModel> cashFlowModels, List<CashFlowGroupItem> cashFlowGroupItems, List<CashFlowModelGroup> cashFlowModelsGroups, AsyncCallback callback, object state);

        int? EndAECashFlowEntities(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVInterestReferenceRateInitializationData(AsyncCallback callback, object state);

        int? EndAEVInterestReferenceRateInitializationData(out List<Currency> currencies, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteInterestReferenceRate(long interestReferenceRateId, AsyncCallback callback, object state);

        int? EndDeleteInterestReferenceRate(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEInterestReferenceRate(bool isEdit, InterestReferenceRate interestReferenceRate, AsyncCallback callback, object state);

        int? EndAEInterestReferenceRate(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVExchangeRateInitializationData(AsyncCallback callback, object state);

        int? EndAEVExchangeRateInitializationData(out List<Currency> currencies, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteExchangeRate(long exchangeRateId, AsyncCallback callback, object state);

        int? EndDeleteExchangeRate(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginApplicationParametersInitializationData(AsyncCallback callback, object state);

        int? EndApplicationParametersInitializationData(out List<AppParameter> appParameters, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEIndex(bool isEdit, Index index, List<AverageIndexesAssoc> averageIndexesAssocs, AsyncCallback callback, object state);

        int? EndAEIndex(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCheckUserHasPermissionOnTrader(long userId, long traderId, AsyncCallback callback, object state);

        int? EndCheckUserHasPermissionOnTrader(out bool hasPermission, IAsyncResult ar);

        #endregion

        #region Other

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPollService(AsyncCallback callback, object state);

        int? EndPollService(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginValidateCredentials(string userName, string password,
                                              AsyncCallback callback, object state);

        int? EndValidateCredentials(out User user, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAEVTradeInitializationData(long? tradeInfoId, FormActionTypeEnum action, bool getExtraData, AsyncCallback callback, object state);

        int? EndAEVTradeInitializationData(out List<Company> companies, out List<Company> counterparties,
                                           out List<Trader> traders, out List<Company> brokers,
                                           out List<Market> marketSegments, out List<Region> marketRegions,
                                           out List<Route> marketRoutes, out List<Vessel> vessels,
                                           out List<Index> indexes, out List<Company> banks,
                                           out List<Account> accounts, out List<ClearingHouse> clearingHouses,
                                           out List<VesselPool> vesselPools, out List<VesselBenchmarking> vesselBenchmarkings,
                                           out List<TradeVersionInfo> tradeVersionInfos, out Trade trade, out List<TradeVersionInfo> tcTradeVersionInfos,
                                           IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInsertNewTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                                         List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                                         TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                                         TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos,
                                         List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades,
                                         AsyncCallback callback, object state);

        int? EndInsertNewTrade(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo,
                                      List<TradeTcInfoLeg> tradeTcInfoLegs, TradeFfaInfo tradeFfaInfo,
                                      TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs,
                                      TradeOptionInfo tradeOptionInfo,
                                      List<TradeBrokerInfo> tradeBrokerInfos, List<TradeInfoBook> tradeInfoBooks, List<Trade> poolTrades,
                                      AsyncCallback callback, object state);

        int? EndUpdateTrade(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetEntitiesThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, DomainObject entityTypeInstanceToReturn, List<DomainObject> entityTypeFilters, AsyncCallback callback, object state);

        int? EndGetEntitiesThatHaveTradesByFilters(out List<DomainObject> entities, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, List<DomainObject> entityTypeFilters, AsyncCallback callback, object state);

        int? EndGetTradesByFilters(out List<Trade> trades, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGeneratePositionExtraTradeTypeOptions(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot,string tradeTypeItems, List<string> tradeIdentifiers, AsyncCallback callback, object state);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGeneratePosition(DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, List<string> tradeIdentifiers, AsyncCallback callback, object state);

        int? EndGeneratePosition(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight, out string errorMessage, IAsyncResult ar);
        int? EndGeneratePositionExtraTradeTypeOptions(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, Dictionary<DateTime, decimal>> resultsWithoutWeight , out string errorMessage, IAsyncResult ar);       

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGenerateMarkToMarketExtraTradeTypeOptions(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, string tradeTypeItems, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, AsyncCallback callback, object state);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGenerateMarkToMarket(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, bool optionEmbeddedValue, bool optionNullify, string MTMType, List<string> tradeIdentifiers, Dictionary<long, string> tradeIdsByPhysicalFinancialType, AsyncCallback callback, object state);

        int? EndGenerateMarkToMarket(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage, IAsyncResult ar);
        int? EndGenerateMarkToMarketExtraTradeTypeOptions(out Dictionary<string, Dictionary<DateTime, decimal>> results, out Dictionary<string, decimal> embeddedValues, out Dictionary<DateTime, Dictionary<string, decimal>> indexesValues, out string errorMessage, IAsyncResult ar);

        //[OperationContract(AsyncPattern = true)]
        //IAsyncResult BeginGenerateMonteCarloSimulation(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns, AsyncCallback callback, object state);

        //int? EndGenerateMonteCarloSimulation(out Dictionary<DateTime, List<double>> monthFrequency, out List<double> cashBounds, out List<Dictionary<DateTime, decimal>> allValues, IAsyncResult ar);
        [OperationContract(IsOneWay = true)]
        void GenerateMonteCarloSimulation(string mcName, DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, string MTMType, List<string> tradeIdentifiers, int noOfSimulationRuns);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCalculateMTMValues(long indexId, DateTime date, AsyncCallback callback, object state);

        int? EndCalculateMTMValues(out Dictionary<DateTime, decimal> finalIndexValues, out Dictionary<DateTime, decimal> finalCurveValues, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetCFModels(AsyncCallback callback, object state);

        int? EndGetCFModels(out List<CashFlowModel> models, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGenerateCashFlow(DateTime positionDate, DateTime curveDate, DateTime periodFrom, DateTime periodTo, string positionMethod, string marketSensitivityType, decimal marketSensitivityValue, bool useSpot, bool optionPremium, List<string> tradeIdentifiers, long cashFlowModelId, bool calculateBreakEven, bool calculateValuation, bool calculateIRR, bool calculateBEBalloon, bool calculateBEEquity, bool calculateBEScrapValue, decimal valuationDiscountRate, AsyncCallback callback, object state);

        int? EndGenerateCashFlow(out Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults, out List<CashFlowGroup> cashFlowGroups, out List<CashFlowModelGroup> cashFlowModelGroups, out List<CashFlowItem> cashFlowItems, out List<CashFlowGroupItem> cashFlowGroupItems, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsAll,
                                 out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsSpot, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsBalloon, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsEquity, out Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsScrappings,
                                 out Dictionary<string, Dictionary<DateTime, decimal>> valuationResults, out Dictionary<string, Dictionary<DateTime, decimal>> valuationDiscountFactor, out Dictionary<string, Dictionary<DateTime, decimal>> valuationPresentValue, out Dictionary<string, Dictionary<DateTime, decimal>> irrResults, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetIndexValues(long indexId, DateTime date, int absoluteOffset, int percentageOffset, AsyncCallback callback, object state);

        int? EndGetIndexValues(out List<IndexSpotValue> spotValues, out List<IndexFFAValue> originalBFAValues, out Dictionary<DateTime, decimal> normalizedBFAValues, out Dictionary<DateTime, decimal> curveValues, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetTradeInitializationData(long tradeId, bool fetchAllData, AsyncCallback callback, object state);

        int? EndGetTradeInitializationData(out List<TradeVersionInfo> tradeVersionInfos, out Trade trade, out List<VesselPool> vesselPools, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetVesselPoolsBySignDate(DateTime signDate, AsyncCallback callback, object state);

        int? EndGetVesselPoolsBySignDate(out List<VesselPool> vesselPools, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetEntitiesThatHaveTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters, AsyncCallback callback, object state);

        int? EndGetEntitiesThatHaveTradesByFilters2(out List<HierarchyNodeInfo> hierarchyNodes, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetCompanyHierarchyThatHaveTradesByFilters(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, long? hierarchyNodeTypeId, HierarchyNodeInfo entityCompany, AsyncCallback callback, object state);

        int? EndGetCompanyHierarchyThatHaveTradesByFilters(out Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentHierarchyNodes, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetTradesByFilters2(DateTime positionDate, DateTime periodFrom, DateTime periodTo, bool acceptDrafts, bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays, List<HierarchyNodeInfo> entityTypeFilters, AsyncCallback callback, object state);

        int? EndGetTradesByFilters2(out List<Trade> trades, out List<Company> companies, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetViewTradesInitializationData(AsyncCallback callback, object state);

        int? EndGetViewTradesInitializationData(out List<Book> books, out List<Book> userAssocBooks, out List<CashFlowModel> models, out List<Index> indexes, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginImportTrades(List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, List<TmpImportCargoTrade> tmpImportCargoTrades, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades, AsyncCallback callback, object state);

        int? EndImportTrades(out string errorMessage, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTradeStatus(TradeTypeEnum tradeTypeEnum, long identifierId, ActivationStatusEnum status, AsyncCallback callback, object state);

        int? EndUpdateTradeStatus(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetAverageIndexesValues(List<long> indexes, DateTime curveDate, DateTime periodFrom, DateTime periodTo, AsyncCallback callback, object state);

        int? EndGetAverageIndexesValues(out Dictionary<long, Dictionary<DateTime, decimal>> forwardIndexesValues, out Dictionary<long, Dictionary<DateTime, decimal>> spotIndexesValues, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetTradesBySelectionBooks(DateTime positionDate, DateTime periodFrom, DateTime periodTo,
                                                    bool acceptDrafts,
                                                    bool acceptMinimum, bool acceptOptional, bool onlyNonZeroTotalDays,
                                                    List<HierarchyNodeInfo> entityTypeFilters, AsyncCallback callback,
                                                    object state);

        int? EndGetTradesBySelectionBooks(out List<Trade> trades, IAsyncResult ar);


        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginEnergyGenerateMarkToMarket(DateTime curveDate, DateTime periodFrom,
                                               DateTime periodTo, AsyncCallback callback,
                                                    object state);

        int? EndEnergyGenerateMarkToMarket(out Dictionary<DateTime, decimal> results, out string errorMessage, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetInactiveTrades(AsyncCallback callback, object state);

        int? EndGetInactiveTrades(out List<Trade> trades, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetMCRuns(AsyncCallback callback, object state);

        int? EndGetMCRuns(out List<MCSimulationInfo> mcRuns, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetMCSimulationDetails(MCSimulationInfo mcSimInfo, AsyncCallback callback, object state);

        int? EndGetMCSimulationDetails(out MCSimulation mcSimulation, IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMCRuns(MCSimulationInfo mcSimulationInfo, AsyncCallback callback, object state);

        int? EndDeleteMCRuns(IAsyncResult ar);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInsertMCRuns(AsyncCallback callback, object state);

        int? EndInsertMCRuns(out string message, IAsyncResult ar);

        #endregion

    }
}