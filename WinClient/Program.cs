﻿using System;
using System.Deployment.Application;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Runtime.Remoting;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using Exis.WCFExtensions;

namespace Exis.WinClient
{
    internal class Program
    {
        #region Main Entry Point

        public static ResourceManager GlobalResourceManager = new ResourceManager("Exis.WinClient.Strings", Assembly.GetExecutingAssembly());

        [STAThread]
        private static void Main()
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += LastChanceHandler;
                Application.ThreadException += OnThreadException;
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                SessionRegistry.ApplicationDirectory = Thread.GetDomain().SetupInformation.ApplicationBase;
                CultureInfo ci;
                
                SessionRegistry.initialize();
                ProgramInfo programInfo = new ProgramInfo();

                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    programInfo.Version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                }
                else
                {
                    Assembly asm = Assembly.GetExecutingAssembly();
                    string[] parts = asm.FullName.Split(',');
                    string version = parts[1].Substring(parts[1].IndexOf('=') +1);
                    programInfo.Version = version;
                }
                programInfo.HostName = Dns.GetHostName();
                programInfo.AppName = "WinClient";
                SessionRegistry.ProgramInfo = programInfo;
                ci = new CultureInfo("en-US", false);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CurrentCulture;
                Thread.CurrentThread.CurrentUICulture = ci;

                Application.Run(new MainForm());
            }
            catch (Exception exc)
            {
                MessageBox.Show("A fatal error occurred.","Exis Application", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Other functions

        public static void LastChanceHandler(object sender, UnhandledExceptionEventArgs args)
        {
            MessageBox.Show(args.ExceptionObject.ToString(), Program.GlobalResourceManager.GetString("Error Occurred"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void OnThreadException(object sender, ThreadExceptionEventArgs args)
        {
            MessageBox.Show(args.Exception.ToString(), Program.GlobalResourceManager.GetString("Error Occurred"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion
    }
}