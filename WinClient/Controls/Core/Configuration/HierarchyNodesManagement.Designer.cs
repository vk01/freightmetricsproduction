﻿namespace Exis.WinClient.Controls
{
    partial class HierarchyNodesManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HierarchyNodesManagement));
            this.rpsiCmbNodeType = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.treeListHierarchyNodes = new DevExpress.XtraTreeList.TreeList();
            this.tlcNodeTypeName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.lookupNodeType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutTreeListHierarchyNodes = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnMoveUp = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnMoveDown = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnAdd = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnDelete = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiCmbNodeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListHierarchyNodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupNodeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTreeListHierarchyNodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // rpsiCmbNodeType
            // 
            this.rpsiCmbNodeType.AutoHeight = false;
            this.rpsiCmbNodeType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpsiCmbNodeType.Name = "rpsiCmbNodeType";
            this.rpsiCmbNodeType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.treeListHierarchyNodes);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(596, 350);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            this.imageList24.Images.SetKeyName(6, "moveDown24x24.ico");
            this.imageList24.Images.SetKeyName(7, "moveUp24x24.ico");
            // 
            // treeListHierarchyNodes
            // 
            this.treeListHierarchyNodes.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlcNodeTypeName});
            this.treeListHierarchyNodes.DragNodesMode = DevExpress.XtraTreeList.TreeListDragNodesMode.Advanced;
            this.treeListHierarchyNodes.Location = new System.Drawing.Point(12, 12);
            this.treeListHierarchyNodes.Name = "treeListHierarchyNodes";
            this.treeListHierarchyNodes.OptionsBehavior.ImmediateEditor = false;
            this.treeListHierarchyNodes.OptionsView.EnableAppearanceEvenRow = true;
            this.treeListHierarchyNodes.OptionsView.ShowColumns = false;
            this.treeListHierarchyNodes.OptionsView.ShowHorzLines = false;
            this.treeListHierarchyNodes.OptionsView.ShowIndicator = false;
            this.treeListHierarchyNodes.OptionsView.ShowVertLines = false;
            this.treeListHierarchyNodes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lookupNodeType});
            this.treeListHierarchyNodes.Size = new System.Drawing.Size(572, 326);
            this.treeListHierarchyNodes.TabIndex = 5;
            // 
            // tlcNodeTypeName
            // 
            this.tlcNodeTypeName.Caption = "Node Type";
            this.tlcNodeTypeName.ColumnEdit = this.lookupNodeType;
            this.tlcNodeTypeName.FieldName = "NodeType";
            this.tlcNodeTypeName.Name = "tlcNodeTypeName";
            this.tlcNodeTypeName.Visible = true;
            this.tlcNodeTypeName.VisibleIndex = 0;
            // 
            // lookupNodeType
            // 
            this.lookupNodeType.AutoHeight = false;
            this.lookupNodeType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupNodeType.DisplayMember = "Name";
            this.lookupNodeType.Name = "lookupNodeType";
            this.lookupNodeType.ValueMember = "Id";
            this.lookupNodeType.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.LookupNodeTypeQueryPopUp);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutTreeListHierarchyNodes});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(596, 350);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutTreeListHierarchyNodes
            // 
            this.layoutTreeListHierarchyNodes.Control = this.treeListHierarchyNodes;
            this.layoutTreeListHierarchyNodes.CustomizationFormText = "layoutTreeListHierarchyNodes";
            this.layoutTreeListHierarchyNodes.Location = new System.Drawing.Point(0, 0);
            this.layoutTreeListHierarchyNodes.Name = "layoutTreeListHierarchyNodes";
            this.layoutTreeListHierarchyNodes.Size = new System.Drawing.Size(576, 330);
            this.layoutTreeListHierarchyNodes.Text = "layoutTreeListHierarchyNodes";
            this.layoutTreeListHierarchyNodes.TextSize = new System.Drawing.Size(0, 0);
            this.layoutTreeListHierarchyNodes.TextToControlDistance = 0;
            this.layoutTreeListHierarchyNodes.TextVisible = false;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnMoveUp,
            this.btnMoveDown,
            this.btnAdd,
            this.btnDelete,
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(596, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 350);
            this.barDockControlBottom.Size = new System.Drawing.Size(596, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 350);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(596, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 350);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMoveUp),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMoveDown),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDelete),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnMoveUp.Caption = "Move Up";
            this.btnMoveUp.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnMoveUp.Id = 0;
            this.btnMoveUp.LargeImageIndex = 7;
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMoveUpClick);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnMoveDown.Caption = "Move Down";
            this.btnMoveDown.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnMoveDown.Id = 1;
            this.btnMoveDown.LargeImageIndex = 6;
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMoveDownClick);
            // 
            // btnAdd
            // 
            this.btnAdd.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnAdd.Caption = "Add";
            this.btnAdd.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnAdd.Id = 2;
            this.btnAdd.LargeImageIndex = 0;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddNodeClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnDelete.Caption = "Delete";
            this.btnDelete.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnDelete.Id = 3;
            this.btnDelete.LargeImageIndex = 3;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteNodeClick);
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 4;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 5;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCloseClick);
            // 
            // HierarchyNodesManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 382);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "HierarchyNodesManagement";
            this.Text = "Entities Hierarchy";
            this.Load += new System.EventHandler(this.HierarchyNodesManagementLoad);
            ((System.ComponentModel.ISupportInitialize)(this.rpsiCmbNodeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListHierarchyNodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupNodeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTreeListHierarchyNodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraTreeList.TreeList treeListHierarchyNodes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutTreeListHierarchyNodes;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcNodeTypeName;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookupNodeType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rpsiCmbNodeType;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnMoveUp;
        private DevExpress.XtraBars.BarLargeButtonItem btnMoveDown;
        private DevExpress.XtraBars.BarLargeButtonItem btnAdd;
        private DevExpress.XtraBars.BarLargeButtonItem btnDelete;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}