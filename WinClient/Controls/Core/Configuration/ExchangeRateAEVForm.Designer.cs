﻿namespace Exis.WinClient.Controls
{
    partial class ExchangeRateAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExchangeRateAEVForm));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.txtRate2 = new DevExpress.XtraEditors.SpinEdit();
            this.lookUpTargetCurrency2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpSourceCurrency2 = new DevExpress.XtraEditors.LookUpEdit();
            this.txtRate1 = new DevExpress.XtraEditors.SpinEdit();
            this.dtpDate = new DevExpress.XtraEditors.DateEdit();
            this.lookUpTargetCurrency1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpSourceCurrency1 = new DevExpress.XtraEditors.LookUpEdit();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgExchangeRate = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgExchange2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutSourceCurrency2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTargetCurrency2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRate2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgExchange1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutSourceCurrency1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTargetCurrency1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRate1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTargetCurrency2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpSourceCurrency2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTargetCurrency1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpSourceCurrency1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExchangeRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExchange2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSourceCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTargetCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExchange1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSourceCurrency1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTargetCurrency1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.txtRate2);
            this.layoutControl.Controls.Add(this.lookUpTargetCurrency2);
            this.layoutControl.Controls.Add(this.lookUpSourceCurrency2);
            this.layoutControl.Controls.Add(this.txtRate1);
            this.layoutControl.Controls.Add(this.dtpDate);
            this.layoutControl.Controls.Add(this.lookUpTargetCurrency1);
            this.layoutControl.Controls.Add(this.lookUpSourceCurrency1);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(821, 279);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // txtRate2
            // 
            this.txtRate2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRate2.Location = new System.Drawing.Point(531, 146);
            this.txtRate2.Name = "txtRate2";
            this.txtRate2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRate2.Properties.Mask.EditMask = "f6";
            this.txtRate2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtRate2.Properties.MaxLength = 13;
            this.txtRate2.Size = new System.Drawing.Size(254, 20);
            this.txtRate2.StyleController = this.layoutControl;
            this.txtRate2.TabIndex = 13;
            this.txtRate2.EditValueChanged += new System.EventHandler(this.TxtRate2EditValueChanged);
            // 
            // lookUpTargetCurrency2
            // 
            this.lookUpTargetCurrency2.Location = new System.Drawing.Point(531, 122);
            this.lookUpTargetCurrency2.Name = "lookUpTargetCurrency2";
            this.lookUpTargetCurrency2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpTargetCurrency2.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpTargetCurrency2.Size = new System.Drawing.Size(254, 20);
            this.lookUpTargetCurrency2.StyleController = this.layoutControl;
            this.lookUpTargetCurrency2.TabIndex = 11;
            this.lookUpTargetCurrency2.EditValueChanged += new System.EventHandler(this.LookUpTargetCurrency2EditValueChanged);
            // 
            // lookUpSourceCurrency2
            // 
            this.lookUpSourceCurrency2.Location = new System.Drawing.Point(531, 98);
            this.lookUpSourceCurrency2.Name = "lookUpSourceCurrency2";
            this.lookUpSourceCurrency2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpSourceCurrency2.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpSourceCurrency2.Size = new System.Drawing.Size(254, 20);
            this.lookUpSourceCurrency2.StyleController = this.layoutControl;
            this.lookUpSourceCurrency2.TabIndex = 10;
            this.lookUpSourceCurrency2.EditValueChanged += new System.EventHandler(this.LookUpSourceCurrency2EditValueChanged);
            // 
            // txtRate1
            // 
            this.txtRate1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRate1.Location = new System.Drawing.Point(123, 146);
            this.txtRate1.Name = "txtRate1";
            this.txtRate1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRate1.Properties.Mask.EditMask = "f6";
            this.txtRate1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtRate1.Properties.MaxLength = 13;
            this.txtRate1.Size = new System.Drawing.Size(293, 20);
            this.txtRate1.StyleController = this.layoutControl;
            this.txtRate1.TabIndex = 12;
            this.txtRate1.EditValueChanged += new System.EventHandler(this.TxtRate1EditValueChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.EditValue = null;
            this.dtpDate.Location = new System.Drawing.Point(111, 43);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDate.Size = new System.Drawing.Size(317, 20);
            this.dtpDate.StyleController = this.layoutControl;
            this.dtpDate.TabIndex = 11;
            // 
            // lookUpTargetCurrency1
            // 
            this.lookUpTargetCurrency1.Location = new System.Drawing.Point(123, 122);
            this.lookUpTargetCurrency1.Name = "lookUpTargetCurrency1";
            this.lookUpTargetCurrency1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpTargetCurrency1.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpTargetCurrency1.Size = new System.Drawing.Size(293, 20);
            this.lookUpTargetCurrency1.StyleController = this.layoutControl;
            this.lookUpTargetCurrency1.TabIndex = 10;
            this.lookUpTargetCurrency1.EditValueChanged += new System.EventHandler(this.LookUpTargetCurrency1EditValueChanged);
            // 
            // lookUpSourceCurrency1
            // 
            this.lookUpSourceCurrency1.Location = new System.Drawing.Point(123, 98);
            this.lookUpSourceCurrency1.Name = "lookUpSourceCurrency1";
            this.lookUpSourceCurrency1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpSourceCurrency1.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpSourceCurrency1.Size = new System.Drawing.Size(293, 20);
            this.lookUpSourceCurrency1.StyleController = this.layoutControl;
            this.lookUpSourceCurrency1.TabIndex = 9;
            this.lookUpSourceCurrency1.EditValueChanged += new System.EventHandler(this.LookUpSourceCurrency1EditValueChanged);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgExchangeRate,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(821, 279);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lcgExchangeRate
            // 
            this.lcgExchangeRate.CustomizationFormText = "Exchange Rate";
            this.lcgExchangeRate.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgExchange2,
            this.lcgExchange1,
            this.layoutDate,
            this.emptySpaceItem2});
            this.lcgExchangeRate.Location = new System.Drawing.Point(0, 0);
            this.lcgExchangeRate.Name = "lcgExchangeRate";
            this.lcgExchangeRate.Size = new System.Drawing.Size(801, 182);
            this.lcgExchangeRate.Text = "Exchange Rate";
            // 
            // lcgExchange2
            // 
            this.lcgExchange2.CustomizationFormText = "lcgExhange2";
            this.lcgExchange2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutSourceCurrency2,
            this.layoutTargetCurrency2,
            this.layoutRate2});
            this.lcgExchange2.Location = new System.Drawing.Point(408, 24);
            this.lcgExchange2.Name = "lcgExchange2";
            this.lcgExchange2.Size = new System.Drawing.Size(369, 115);
            this.lcgExchange2.Text = "Currency -> Currency";
            // 
            // layoutSourceCurrency2
            // 
            this.layoutSourceCurrency2.Control = this.lookUpSourceCurrency2;
            this.layoutSourceCurrency2.CustomizationFormText = "Source Currency:";
            this.layoutSourceCurrency2.Location = new System.Drawing.Point(0, 0);
            this.layoutSourceCurrency2.Name = "layoutSourceCurrency2";
            this.layoutSourceCurrency2.Size = new System.Drawing.Size(345, 24);
            this.layoutSourceCurrency2.Text = "Source Currency:";
            this.layoutSourceCurrency2.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutTargetCurrency2
            // 
            this.layoutTargetCurrency2.Control = this.lookUpTargetCurrency2;
            this.layoutTargetCurrency2.CustomizationFormText = "Target Currency:";
            this.layoutTargetCurrency2.Location = new System.Drawing.Point(0, 24);
            this.layoutTargetCurrency2.Name = "layoutTargetCurrency2";
            this.layoutTargetCurrency2.Size = new System.Drawing.Size(345, 24);
            this.layoutTargetCurrency2.Text = "Target Currency:";
            this.layoutTargetCurrency2.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutRate2
            // 
            this.layoutRate2.Control = this.txtRate2;
            this.layoutRate2.CustomizationFormText = "Rate:";
            this.layoutRate2.Location = new System.Drawing.Point(0, 48);
            this.layoutRate2.Name = "layoutRate2";
            this.layoutRate2.Size = new System.Drawing.Size(345, 24);
            this.layoutRate2.Text = "Rate:";
            this.layoutRate2.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lcgExchange1
            // 
            this.lcgExchange1.CustomizationFormText = "Currency -> Currency";
            this.lcgExchange1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutSourceCurrency1,
            this.layoutTargetCurrency1,
            this.layoutRate1});
            this.lcgExchange1.Location = new System.Drawing.Point(0, 24);
            this.lcgExchange1.Name = "lcgExchange1";
            this.lcgExchange1.Size = new System.Drawing.Size(408, 115);
            this.lcgExchange1.Text = "Currency -> Currency";
            // 
            // layoutSourceCurrency1
            // 
            this.layoutSourceCurrency1.Control = this.lookUpSourceCurrency1;
            this.layoutSourceCurrency1.CustomizationFormText = "layoutSourceCurrency";
            this.layoutSourceCurrency1.Location = new System.Drawing.Point(0, 0);
            this.layoutSourceCurrency1.Name = "layoutSourceCurrency1";
            this.layoutSourceCurrency1.Size = new System.Drawing.Size(384, 24);
            this.layoutSourceCurrency1.Text = "Source Currency:";
            this.layoutSourceCurrency1.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutTargetCurrency1
            // 
            this.layoutTargetCurrency1.Control = this.lookUpTargetCurrency1;
            this.layoutTargetCurrency1.CustomizationFormText = "Target Currency:";
            this.layoutTargetCurrency1.Location = new System.Drawing.Point(0, 24);
            this.layoutTargetCurrency1.Name = "layoutTargetCurrency1";
            this.layoutTargetCurrency1.Size = new System.Drawing.Size(384, 24);
            this.layoutTargetCurrency1.Text = "Target Currency:";
            this.layoutTargetCurrency1.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutRate1
            // 
            this.layoutRate1.Control = this.txtRate1;
            this.layoutRate1.CustomizationFormText = "Rate:";
            this.layoutRate1.Location = new System.Drawing.Point(0, 48);
            this.layoutRate1.Name = "layoutRate1";
            this.layoutRate1.Size = new System.Drawing.Size(384, 24);
            this.layoutRate1.Text = "Rate:";
            this.layoutRate1.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutDate
            // 
            this.layoutDate.Control = this.dtpDate;
            this.layoutDate.CustomizationFormText = "Date:";
            this.layoutDate.Location = new System.Drawing.Point(0, 0);
            this.layoutDate.Name = "layoutDate";
            this.layoutDate.Size = new System.Drawing.Size(408, 24);
            this.layoutDate.Text = "Date:";
            this.layoutDate.TextSize = new System.Drawing.Size(84, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(408, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(369, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 182);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(801, 77);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(821, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 279);
            this.barDockControlBottom.Size = new System.Drawing.Size(821, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 279);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(821, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 279);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // ExchangeRateAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 311);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ExchangeRateAEVForm";
            this.Activated += new System.EventHandler(this.ExchangeRateAevFormActivated);
            this.Deactivate += new System.EventHandler(this.ExchangeRateAevFormDeactivate);
            this.Load += new System.EventHandler(this.ExchangeRateAevFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRate2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTargetCurrency2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpSourceCurrency2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTargetCurrency1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpSourceCurrency1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExchangeRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExchange2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSourceCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTargetCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExchange1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSourceCurrency1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTargetCurrency1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.LookUpEdit lookUpSourceCurrency1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgExchangeRate;
        private DevExpress.XtraLayout.LayoutControlItem layoutSourceCurrency1;
        private DevExpress.XtraEditors.LookUpEdit lookUpTargetCurrency1;
        private DevExpress.XtraLayout.LayoutControlItem layoutTargetCurrency1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SpinEdit txtRate1;
        private DevExpress.XtraEditors.DateEdit dtpDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutRate1;
        private DevExpress.XtraEditors.SpinEdit txtRate2;
        private DevExpress.XtraEditors.LookUpEdit lookUpTargetCurrency2;
        private DevExpress.XtraEditors.LookUpEdit lookUpSourceCurrency2;
        private DevExpress.XtraLayout.LayoutControlGroup lcgExchange2;
        private DevExpress.XtraLayout.LayoutControlItem layoutSourceCurrency2;
        private DevExpress.XtraLayout.LayoutControlItem layoutTargetCurrency2;
        private DevExpress.XtraLayout.LayoutControlItem layoutRate2;
        private DevExpress.XtraLayout.LayoutControlGroup lcgExchange1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}