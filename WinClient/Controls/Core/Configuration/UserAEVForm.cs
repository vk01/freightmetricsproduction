﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using Exis.Domain;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Controls;

namespace Exis.WinClient.Controls
{
    public partial class UserAEVForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private User _user;

        #endregion

        #region Constructors

        public UserAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public UserAEVForm(User user, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _user = user;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            tlBooks.BeginUpdate();
            tlBooks.Columns.Add();
            tlBooks.Columns[0].Caption = Strings.Book_Name;
            tlBooks.Columns[0].VisibleIndex = 0;
            tlBooks.EndUpdate();

            if (_formActionType == FormActionTypeEnum.View)
            {
                txtFirstName.Properties.ReadOnly = true;
                txtLastName.Properties.ReadOnly = true;
                txtLogin.Properties.ReadOnly = true;
                txtPassword.Properties.ReadOnly = true;
                txtPassword2.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                clbTraders.Enabled = false;
                
                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void InitializeData()
        {
            BeginAEVTUserInitializationData();
        }

        private void PopulateTree(TreeListNode parentNode, List<Book> childrenBooks,
                                  ref List<Book> allBooks)
        {
            foreach (Book book in childrenBooks)
            {
                TreeListNode childNode =
                    tlBooks.AppendNode(
                        new object[] { book.Name }, parentNode, book);
                PopulateTree(childNode, allBooks.Where(a => a.ParentBookId == book.Id).ToList(), ref allBooks);
            }
        }

        private void LoadData()
        {
            lblId.Text = _user.Id.ToString();
            txtLogin.Text = _user.Login;
            txtPassword.Text = Encoding.Default.GetString(Convert.FromBase64String(_user.Password));
            txtPassword2.Text = Encoding.Default.GetString(Convert.FromBase64String(_user.Password));
            txtFirstName.Text = _user.FirstName;
            txtLastName.Text = _user.LastName;
            cmbStatus.SelectedItem = _user.Status;

            foreach (Book book in _user.Books)
            {
                tlBooks.NodesIterator.DoOperation(a =>
                {
                    if (a.Tag != null &&
                        ((Book)a.Tag).
                            Id ==
                        book.Id)
                    {
                        a.Checked = true;
                        a.RootNode.ExpandAll();
                    }
                });
            }

            foreach (Trader trader in _user.Traders)
            {
                clbTraders.SetItemChecked((clbTraders.FindItem(0, true, a =>
                                                                            {
                                                                                a.IsFound =
                                                                                    (((Trader) a.ItemValue).Id ==
                                                                                     trader.Id);
                                                                            })), true);
            }

            txtCreationUser.Text = _user.Cruser;
            dtpCreationDate.DateTime = _user.Crd.Value;
            txtUpdateUser.Text = _user.Chuser;
            dtpUpdateDate.DateTime = _user.Chd.Value;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtFirstName.Text.Trim()))
                errorProvider.SetError(txtFirstName, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                errorProvider.SetError(txtLastName, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtLogin.Text.Trim()))
                errorProvider.SetError(txtLogin, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtPassword.Text.Trim()))
                errorProvider.SetError(txtPassword, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtPassword2.Text.Trim()))
                errorProvider.SetError(txtPassword2, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if(txtPassword.Text != txtPassword2.Text)
                errorProvider.SetError(txtPassword, Strings.Password_mismatch);
            return !errorProvider.HasErrors;
        }

        private void PropagateCheckStateToChildren(TreeListNode node)
        {
            foreach (TreeListNode treeListNode in node.Nodes)
            {
                treeListNode.Checked = node.Checked;
                PropagateCheckStateToChildren(treeListNode);
            }
        }

        private void PropagateCheckState(TreeListNode node)
        {
            PropagateCheckStateToChildren(node);

            bool checkState = node.Checked;
            node = node.ParentNode;
            while (node != null)
            {
                if (checkState)
                {
                    node.Checked = true;
                }
                else
                {
                    bool allChildNodesUnchecked = true;
                    foreach (TreeListNode treeListNode in node.Nodes)
                    {
                        if (!treeListNode.Checked) continue;
                        allChildNodesUnchecked = false;
                        break;
                    }
                    if (allChildNodesUnchecked)
                    {
                        node.Checked = false;
                    }
                }
                node = node.ParentNode;
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVTUserInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVUserInitializationData(
                    EndAEVTUserInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVTUserInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVTUserInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Book> books;
            List<Trader> traders;

            try
            {
                result = SessionRegistry.Client.EndAEVUserInitializationData(out traders, out books, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                tlBooks.Tag = books;
                PopulateTree(null, books.Where(a => a.ParentBookId == null).ToList(), ref books);

                foreach (Trader trader in traders)
                {
                    clbTraders.Items.Add(new CheckedListBoxItem(trader, trader.Name));
                }

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVUser(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _user, EndAEVUser, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVUser(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVUser;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Login already exists
                errorProvider.SetError(txtLogin, Strings.Login_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public User User
        {
            get { return _user; }
        }

        public FormActionTypeEnum FormActionType { get { return _formActionType; } }

        #endregion

        #region GUI Events

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _user = new User();
            }

            _user.FirstName = txtFirstName.Text.Trim();
            _user.LastName = txtLastName.Text.Trim();
            _user.Login = txtLogin.Text.Trim();
            _user.Password = txtPassword.Text.Trim();
            
            _user.Status = (ActivationStatusEnum)cmbStatus.SelectedItem;
            
            var books = new List<Book>();
            tlBooks.NodesIterator.DoOperation(a => { if (a.Checked) books.Add((Book)a.Tag); });

            var traders = (from int checkedIndex in clbTraders.CheckedIndices
                           select (Trader) clbTraders.GetItemValue(checkedIndex)).ToList();

            _user.Books = books;
            _user.Traders = traders;

            BeginAEVUser(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void UserAevFormLoad(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void UserAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void UserAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void TlBooksAfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            PropagateCheckState(e.Node);
        }

        private void TlBooksBeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            if (_formActionType == FormActionTypeEnum.View)
                e.CanCheck = false;
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}