﻿namespace Exis.WinClient.Controls
{
    partial class VesselAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VesselAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.txtDeadWeight = new DevExpress.XtraEditors.SpinEdit();
            this.dtpMOADate = new DevExpress.XtraEditors.DateEdit();
            this.dtpDeliveryDate = new DevExpress.XtraEditors.DateEdit();
            this.txtEquity = new DevExpress.XtraEditors.SpinEdit();
            this.grdExpenses = new DevExpress.XtraGrid.GridControl();
            this.grvExpenses = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtAge = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gcOperationalExpenses = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtOperationExpenses = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gcDryDockExpenses = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtDryDockExpenses = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gcDryDockOffHire = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtDryDockOffHire = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gcDepreciationProfile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtDepreciationProfile = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.lookUpTechnicalManager = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpChiefOfficer = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpMasterOfficer = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupContact = new DevExpress.XtraEditors.LookUpEdit();
            this.txtBenchmark = new DevExpress.XtraEditors.SpinEdit();
            this.txtMarketPrice = new DevExpress.XtraEditors.SpinEdit();
            this.txtAcquisitionPrice = new DevExpress.XtraEditors.SpinEdit();
            this.txtBenchAgeFrequency = new DevExpress.XtraEditors.SpinEdit();
            this.txtBenchAgePoints = new DevExpress.XtraEditors.SpinEdit();
            this.txtBenchInitial = new DevExpress.XtraEditors.SpinEdit();
            this.dtpDateBuilt = new DevExpress.XtraEditors.DateEdit();
            this.txtImoNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtSaleAmount = new DevExpress.XtraEditors.SpinEdit();
            this.dtpSaleDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUsefulLife = new DevExpress.XtraEditors.SpinEdit();
            this.txtCommercialFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtManagementFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtPiClub = new DevExpress.XtraEditors.TextEdit();
            this.txtYard = new DevExpress.XtraEditors.TextEdit();
            this.txtFlag = new DevExpress.XtraEditors.TextEdit();
            this.txtClassification = new DevExpress.XtraEditors.TextEdit();
            this.txtBeam = new DevExpress.XtraEditors.SpinEdit();
            this.txtDieselLadenPort = new DevExpress.XtraEditors.SpinEdit();
            this.txtDieselBallastSea = new DevExpress.XtraEditors.SpinEdit();
            this.txtFuelOilPort = new DevExpress.XtraEditors.SpinEdit();
            this.txtLoa = new DevExpress.XtraEditors.SpinEdit();
            this.txtFuelLadenSea = new DevExpress.XtraEditors.SpinEdit();
            this.txtOilLadenSea = new DevExpress.XtraEditors.SpinEdit();
            this.txtFuelBallastSea = new DevExpress.XtraEditors.SpinEdit();
            this.txtTPC = new DevExpress.XtraEditors.SpinEdit();
            this.txtGrainCapacity = new DevExpress.XtraEditors.SpinEdit();
            this.txtDraftTropic = new DevExpress.XtraEditors.SpinEdit();
            this.txtDraftSummer = new DevExpress.XtraEditors.SpinEdit();
            this.txtDraftWinter = new DevExpress.XtraEditors.SpinEdit();
            this.txtLightweight = new DevExpress.XtraEditors.SpinEdit();
            this.txtDeadweightSummer = new DevExpress.XtraEditors.SpinEdit();
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtDeadweightTropic = new DevExpress.XtraEditors.SpinEdit();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.txtDeadWeightWinter = new DevExpress.XtraEditors.SpinEdit();
            this.lookupCompany = new DevExpress.XtraEditors.LookUpEdit();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.lookupMarket = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupVessel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCompany = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarket = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemIMONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemBenchInitial = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemBenchAgePoints = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemBenchAgeFrequency = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgVesselInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemDeadWeightWinter = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDeadWeightSummer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDeadWeightTropic = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutLightweight = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDraftWinter = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDraftSummer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDraftTropic = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemGrainCapacity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemTpc = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemLoa = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemBeam = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemClassification = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemFlag = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemYard = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPiClub = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemFuelOilBallastSea = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemFuleOilLadenAtSea = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemFuelOilAtPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDieselOilBallastAtSea = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDieselOilLadenAtPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDieselOilLadenAtSea = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemManagementFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCommercialFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemContact = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemMasterOfficer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemChiefOfficer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemTechnicalManager = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutEquity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDeliveryDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemSaleDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemSaleAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemUsefulLife = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutMOADate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDeadWeight = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDateBuilt = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAcquisitionPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemMarketPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemBenchmark = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgExpenses = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemExpenses = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpMOADate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpMOADate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDeliveryDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDeliveryDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOperationExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDryDockExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDryDockOffHire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepreciationProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTechnicalManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpChiefOfficer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpMasterOfficer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchmark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcquisitionPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchAgeFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchAgePoints.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchInitial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateBuilt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateBuilt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImoNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSaleDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSaleDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsefulLife.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercialFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManagementFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPiClub.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClassification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDieselLadenPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDieselBallastSea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelOilPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelLadenSea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOilLadenSea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelBallastSea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrainCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDraftTropic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDraftSummer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDraftWinter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLightweight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadweightSummer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadweightTropic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadWeightWinter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupVessel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemIMONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchInitial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchAgePoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchAgeFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgVesselInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeightWinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeightSummer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeightTropic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLightweight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDraftWinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDraftSummer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDraftTropic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGrainCapacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTpc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemLoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemClassification)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemYard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPiClub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFuelOilBallastSea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFuleOilLadenAtSea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFuelOilAtPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDieselOilBallastAtSea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDieselOilLadenAtPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDieselOilLadenAtSea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemManagementFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCommercialFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemMasterOfficer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemChiefOfficer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTechnicalManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEquity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSaleDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSaleAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemUsefulLife)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMOADate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDateBuilt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAcquisitionPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemMarketPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchmark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.txtDeadWeight);
            this.layoutRootControl.Controls.Add(this.dtpMOADate);
            this.layoutRootControl.Controls.Add(this.dtpDeliveryDate);
            this.layoutRootControl.Controls.Add(this.txtEquity);
            this.layoutRootControl.Controls.Add(this.grdExpenses);
            this.layoutRootControl.Controls.Add(this.lookUpTechnicalManager);
            this.layoutRootControl.Controls.Add(this.lookUpChiefOfficer);
            this.layoutRootControl.Controls.Add(this.lookUpMasterOfficer);
            this.layoutRootControl.Controls.Add(this.lookupContact);
            this.layoutRootControl.Controls.Add(this.txtBenchmark);
            this.layoutRootControl.Controls.Add(this.txtMarketPrice);
            this.layoutRootControl.Controls.Add(this.txtAcquisitionPrice);
            this.layoutRootControl.Controls.Add(this.txtBenchAgeFrequency);
            this.layoutRootControl.Controls.Add(this.txtBenchAgePoints);
            this.layoutRootControl.Controls.Add(this.txtBenchInitial);
            this.layoutRootControl.Controls.Add(this.dtpDateBuilt);
            this.layoutRootControl.Controls.Add(this.txtImoNumber);
            this.layoutRootControl.Controls.Add(this.txtSaleAmount);
            this.layoutRootControl.Controls.Add(this.dtpSaleDate);
            this.layoutRootControl.Controls.Add(this.txtUsefulLife);
            this.layoutRootControl.Controls.Add(this.txtCommercialFee);
            this.layoutRootControl.Controls.Add(this.txtManagementFee);
            this.layoutRootControl.Controls.Add(this.txtPiClub);
            this.layoutRootControl.Controls.Add(this.txtYard);
            this.layoutRootControl.Controls.Add(this.txtFlag);
            this.layoutRootControl.Controls.Add(this.txtClassification);
            this.layoutRootControl.Controls.Add(this.txtBeam);
            this.layoutRootControl.Controls.Add(this.txtDieselLadenPort);
            this.layoutRootControl.Controls.Add(this.txtDieselBallastSea);
            this.layoutRootControl.Controls.Add(this.txtFuelOilPort);
            this.layoutRootControl.Controls.Add(this.txtLoa);
            this.layoutRootControl.Controls.Add(this.txtFuelLadenSea);
            this.layoutRootControl.Controls.Add(this.txtOilLadenSea);
            this.layoutRootControl.Controls.Add(this.txtFuelBallastSea);
            this.layoutRootControl.Controls.Add(this.txtTPC);
            this.layoutRootControl.Controls.Add(this.txtGrainCapacity);
            this.layoutRootControl.Controls.Add(this.txtDraftTropic);
            this.layoutRootControl.Controls.Add(this.txtDraftSummer);
            this.layoutRootControl.Controls.Add(this.txtDraftWinter);
            this.layoutRootControl.Controls.Add(this.txtLightweight);
            this.layoutRootControl.Controls.Add(this.txtDeadweightSummer);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtDeadweightTropic);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.txtDeadWeightWinter);
            this.layoutRootControl.Controls.Add(this.lookupCompany);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.lookupMarket);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtName);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 426, 250, 350);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(953, 556);
            this.layoutRootControl.TabIndex = 2;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // txtDeadWeight
            // 
            this.txtDeadWeight.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDeadWeight.Location = new System.Drawing.Point(81, 151);
            this.txtDeadWeight.Name = "txtDeadWeight";
            this.txtDeadWeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDeadWeight.Properties.Mask.EditMask = "D";
            this.txtDeadWeight.Properties.MaxLength = 6;
            this.txtDeadWeight.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtDeadWeight.Size = new System.Drawing.Size(96, 20);
            this.txtDeadWeight.StyleController = this.layoutRootControl;
            this.txtDeadWeight.TabIndex = 10;
            // 
            // dtpMOADate
            // 
            this.dtpMOADate.EditValue = null;
            this.dtpMOADate.Location = new System.Drawing.Point(278, 127);
            this.dtpMOADate.Name = "dtpMOADate";
            this.dtpMOADate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpMOADate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpMOADate.Size = new System.Drawing.Size(163, 20);
            this.dtpMOADate.StyleController = this.layoutRootControl;
            this.dtpMOADate.TabIndex = 46;
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.EditValue = null;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(89, 127);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDeliveryDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDeliveryDate.Size = new System.Drawing.Size(129, 20);
            this.dtpDeliveryDate.StyleController = this.layoutRootControl;
            this.dtpDeliveryDate.TabIndex = 45;
            // 
            // txtEquity
            // 
            this.txtEquity.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtEquity.Location = new System.Drawing.Point(475, 295);
            this.txtEquity.Name = "txtEquity";
            this.txtEquity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtEquity.Properties.Mask.EditMask = "f2";
            this.txtEquity.Properties.MaxLength = 7;
            this.txtEquity.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtEquity.Size = new System.Drawing.Size(116, 20);
            this.txtEquity.StyleController = this.layoutRootControl;
            this.txtEquity.TabIndex = 35;
            // 
            // grdExpenses
            // 
            this.grdExpenses.Location = new System.Drawing.Point(17, 368);
            this.grdExpenses.MainView = this.grvExpenses;
            this.grdExpenses.MinimumSize = new System.Drawing.Size(741, 220);
            this.grdExpenses.Name = "grdExpenses";
            this.grdExpenses.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtAge,
            this.txtOperationExpenses,
            this.txtDryDockExpenses,
            this.txtDryDockOffHire,
            this.txtDepreciationProfile});
            this.grdExpenses.Size = new System.Drawing.Size(902, 220);
            this.grdExpenses.TabIndex = 48;
            this.grdExpenses.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvExpenses});
            // 
            // grvExpenses
            // 
            this.grvExpenses.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcAge,
            this.gcOperationalExpenses,
            this.gcDryDockExpenses,
            this.gcDryDockOffHire,
            this.gcDepreciationProfile});
            this.grvExpenses.GridControl = this.grdExpenses;
            this.grvExpenses.Name = "grvExpenses";
            this.grvExpenses.OptionsView.ShowGroupPanel = false;
            // 
            // gcAge
            // 
            this.gcAge.Caption = "Age";
            this.gcAge.ColumnEdit = this.txtAge;
            this.gcAge.FieldName = "Age";
            this.gcAge.Name = "gcAge";
            this.gcAge.OptionsColumn.AllowEdit = false;
            this.gcAge.OptionsColumn.ReadOnly = true;
            this.gcAge.Visible = true;
            this.gcAge.VisibleIndex = 0;
            // 
            // txtAge
            // 
            this.txtAge.AutoHeight = false;
            this.txtAge.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAge.Mask.EditMask = "D";
            this.txtAge.MaxLength = 2;
            this.txtAge.MaxValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.txtAge.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtAge.Name = "txtAge";
            // 
            // gcOperationalExpenses
            // 
            this.gcOperationalExpenses.Caption = "Operational Expenses";
            this.gcOperationalExpenses.ColumnEdit = this.txtOperationExpenses;
            this.gcOperationalExpenses.FieldName = "OperationalExpenses";
            this.gcOperationalExpenses.Name = "gcOperationalExpenses";
            this.gcOperationalExpenses.Visible = true;
            this.gcOperationalExpenses.VisibleIndex = 1;
            // 
            // txtOperationExpenses
            // 
            this.txtOperationExpenses.AutoHeight = false;
            this.txtOperationExpenses.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtOperationExpenses.Mask.EditMask = "f4";
            this.txtOperationExpenses.MaxLength = 11;
            this.txtOperationExpenses.Name = "txtOperationExpenses";
            // 
            // gcDryDockExpenses
            // 
            this.gcDryDockExpenses.Caption = "Dry Dock Expenses";
            this.gcDryDockExpenses.ColumnEdit = this.txtDryDockExpenses;
            this.gcDryDockExpenses.FieldName = "DryDockExpenses";
            this.gcDryDockExpenses.Name = "gcDryDockExpenses";
            this.gcDryDockExpenses.Visible = true;
            this.gcDryDockExpenses.VisibleIndex = 2;
            // 
            // txtDryDockExpenses
            // 
            this.txtDryDockExpenses.AutoHeight = false;
            this.txtDryDockExpenses.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDryDockExpenses.Mask.EditMask = "f4";
            this.txtDryDockExpenses.MaxLength = 11;
            this.txtDryDockExpenses.Name = "txtDryDockExpenses";
            // 
            // gcDryDockOffHire
            // 
            this.gcDryDockOffHire.Caption = "Dry Dock Off Hire";
            this.gcDryDockOffHire.ColumnEdit = this.txtDryDockOffHire;
            this.gcDryDockOffHire.FieldName = "DryDockOffHire";
            this.gcDryDockOffHire.Name = "gcDryDockOffHire";
            this.gcDryDockOffHire.Visible = true;
            this.gcDryDockOffHire.VisibleIndex = 3;
            // 
            // txtDryDockOffHire
            // 
            this.txtDryDockOffHire.AutoHeight = false;
            this.txtDryDockOffHire.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDryDockOffHire.Mask.EditMask = "D";
            this.txtDryDockOffHire.MaxLength = 3;
            this.txtDryDockOffHire.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtDryDockOffHire.Name = "txtDryDockOffHire";
            // 
            // gcDepreciationProfile
            // 
            this.gcDepreciationProfile.Caption = "Depreciation Profile";
            this.gcDepreciationProfile.ColumnEdit = this.txtDepreciationProfile;
            this.gcDepreciationProfile.FieldName = "DepreciationProfile";
            this.gcDepreciationProfile.Name = "gcDepreciationProfile";
            this.gcDepreciationProfile.Visible = true;
            this.gcDepreciationProfile.VisibleIndex = 4;
            // 
            // txtDepreciationProfile
            // 
            this.txtDepreciationProfile.AutoHeight = false;
            this.txtDepreciationProfile.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDepreciationProfile.Mask.EditMask = "P0";
            this.txtDepreciationProfile.Mask.UseMaskAsDisplayFormat = true;
            this.txtDepreciationProfile.MaxLength = 2;
            this.txtDepreciationProfile.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtDepreciationProfile.Name = "txtDepreciationProfile";
            // 
            // lookUpTechnicalManager
            // 
            this.lookUpTechnicalManager.Location = new System.Drawing.Point(793, 319);
            this.lookUpTechnicalManager.Name = "lookUpTechnicalManager";
            this.lookUpTechnicalManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpTechnicalManager.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpTechnicalManager.Size = new System.Drawing.Size(126, 20);
            this.lookUpTechnicalManager.StyleController = this.layoutRootControl;
            this.lookUpTechnicalManager.TabIndex = 24;
            // 
            // lookUpChiefOfficer
            // 
            this.lookUpChiefOfficer.Location = new System.Drawing.Point(538, 319);
            this.lookUpChiefOfficer.Name = "lookUpChiefOfficer";
            this.lookUpChiefOfficer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpChiefOfficer.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpChiefOfficer.Size = new System.Drawing.Size(155, 20);
            this.lookUpChiefOfficer.StyleController = this.layoutRootControl;
            this.lookUpChiefOfficer.TabIndex = 23;
            // 
            // lookUpMasterOfficer
            // 
            this.lookUpMasterOfficer.Location = new System.Drawing.Point(317, 319);
            this.lookUpMasterOfficer.Name = "lookUpMasterOfficer";
            this.lookUpMasterOfficer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpMasterOfficer.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookUpMasterOfficer.Size = new System.Drawing.Size(149, 20);
            this.lookUpMasterOfficer.StyleController = this.layoutRootControl;
            this.lookUpMasterOfficer.TabIndex = 23;
            // 
            // lookupContact
            // 
            this.lookupContact.Location = new System.Drawing.Point(62, 319);
            this.lookupContact.Name = "lookupContact";
            this.lookupContact.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupContact.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupContact.Size = new System.Drawing.Size(175, 20);
            this.lookupContact.StyleController = this.layoutRootControl;
            this.lookupContact.TabIndex = 22;
            // 
            // txtBenchmark
            // 
            this.txtBenchmark.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtBenchmark.Location = new System.Drawing.Point(861, 81);
            this.txtBenchmark.Name = "txtBenchmark";
            this.txtBenchmark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBenchmark.Properties.Mask.EditMask = "D";
            this.txtBenchmark.Properties.MaxLength = 3;
            this.txtBenchmark.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtBenchmark.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtBenchmark.Size = new System.Drawing.Size(61, 20);
            this.txtBenchmark.StyleController = this.layoutRootControl;
            this.txtBenchmark.TabIndex = 18;
            // 
            // txtMarketPrice
            // 
            this.txtMarketPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtMarketPrice.Location = new System.Drawing.Point(736, 81);
            this.txtMarketPrice.Name = "txtMarketPrice";
            this.txtMarketPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMarketPrice.Properties.Mask.EditMask = "D";
            this.txtMarketPrice.Properties.MaxLength = 9;
            this.txtMarketPrice.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtMarketPrice.Size = new System.Drawing.Size(62, 20);
            this.txtMarketPrice.StyleController = this.layoutRootControl;
            this.txtMarketPrice.TabIndex = 17;
            // 
            // txtAcquisitionPrice
            // 
            this.txtAcquisitionPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAcquisitionPrice.Location = new System.Drawing.Point(602, 81);
            this.txtAcquisitionPrice.Name = "txtAcquisitionPrice";
            this.txtAcquisitionPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAcquisitionPrice.Properties.Mask.EditMask = "D";
            this.txtAcquisitionPrice.Properties.MaxLength = 9;
            this.txtAcquisitionPrice.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtAcquisitionPrice.Size = new System.Drawing.Size(64, 20);
            this.txtAcquisitionPrice.StyleController = this.layoutRootControl;
            this.txtAcquisitionPrice.TabIndex = 17;
            // 
            // txtBenchAgeFrequency
            // 
            this.txtBenchAgeFrequency.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtBenchAgeFrequency.Location = new System.Drawing.Point(444, 81);
            this.txtBenchAgeFrequency.Name = "txtBenchAgeFrequency";
            this.txtBenchAgeFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBenchAgeFrequency.Properties.Mask.EditMask = "D";
            this.txtBenchAgeFrequency.Properties.MaxLength = 2;
            this.txtBenchAgeFrequency.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtBenchAgeFrequency.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtBenchAgeFrequency.Size = new System.Drawing.Size(70, 20);
            this.txtBenchAgeFrequency.StyleController = this.layoutRootControl;
            this.txtBenchAgeFrequency.TabIndex = 16;
            // 
            // txtBenchAgePoints
            // 
            this.txtBenchAgePoints.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBenchAgePoints.Location = new System.Drawing.Point(250, 81);
            this.txtBenchAgePoints.Name = "txtBenchAgePoints";
            this.txtBenchAgePoints.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBenchAgePoints.Properties.Mask.EditMask = "f2";
            this.txtBenchAgePoints.Properties.MaxLength = 5;
            this.txtBenchAgePoints.Size = new System.Drawing.Size(78, 20);
            this.txtBenchAgePoints.StyleController = this.layoutRootControl;
            this.txtBenchAgePoints.TabIndex = 47;
            // 
            // txtBenchInitial
            // 
            this.txtBenchInitial.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBenchInitial.Location = new System.Drawing.Point(79, 81);
            this.txtBenchInitial.Name = "txtBenchInitial";
            this.txtBenchInitial.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBenchInitial.Properties.Mask.EditMask = "f2";
            this.txtBenchInitial.Properties.MaxLength = 6;
            this.txtBenchInitial.Size = new System.Drawing.Size(77, 20);
            this.txtBenchInitial.StyleController = this.layoutRootControl;
            this.txtBenchInitial.TabIndex = 15;
            // 
            // dtpDateBuilt
            // 
            this.dtpDateBuilt.EditValue = null;
            this.dtpDateBuilt.Location = new System.Drawing.Point(705, 57);
            this.dtpDateBuilt.Name = "dtpDateBuilt";
            this.dtpDateBuilt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDateBuilt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDateBuilt.Size = new System.Drawing.Size(217, 20);
            this.dtpDateBuilt.StyleController = this.layoutRootControl;
            this.dtpDateBuilt.TabIndex = 46;
            // 
            // txtImoNumber
            // 
            this.txtImoNumber.Location = new System.Drawing.Point(525, 57);
            this.txtImoNumber.Name = "txtImoNumber";
            this.txtImoNumber.Properties.MaxLength = 7;
            this.txtImoNumber.Size = new System.Drawing.Size(123, 20);
            this.txtImoNumber.StyleController = this.layoutRootControl;
            this.txtImoNumber.TabIndex = 45;
            // 
            // txtSaleAmount
            // 
            this.txtSaleAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSaleAmount.Location = new System.Drawing.Point(684, 127);
            this.txtSaleAmount.Name = "txtSaleAmount";
            this.txtSaleAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtSaleAmount.Properties.Mask.EditMask = "D";
            this.txtSaleAmount.Properties.MaxLength = 9;
            this.txtSaleAmount.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtSaleAmount.Size = new System.Drawing.Size(95, 20);
            this.txtSaleAmount.StyleController = this.layoutRootControl;
            this.txtSaleAmount.TabIndex = 14;
            // 
            // dtpSaleDate
            // 
            this.dtpSaleDate.EditValue = null;
            this.dtpSaleDate.Location = new System.Drawing.Point(498, 127);
            this.dtpSaleDate.Name = "dtpSaleDate";
            this.dtpSaleDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpSaleDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpSaleDate.Size = new System.Drawing.Size(115, 20);
            this.dtpSaleDate.StyleController = this.layoutRootControl;
            this.dtpSaleDate.TabIndex = 44;
            // 
            // txtUsefulLife
            // 
            this.txtUsefulLife.EditValue = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.txtUsefulLife.Location = new System.Drawing.Point(840, 127);
            this.txtUsefulLife.Name = "txtUsefulLife";
            this.txtUsefulLife.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtUsefulLife.Properties.Mask.EditMask = "D";
            this.txtUsefulLife.Properties.MaxLength = 2;
            this.txtUsefulLife.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtUsefulLife.Size = new System.Drawing.Size(79, 20);
            this.txtUsefulLife.StyleController = this.layoutRootControl;
            this.txtUsefulLife.TabIndex = 15;
            // 
            // txtCommercialFee
            // 
            this.txtCommercialFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCommercialFee.Location = new System.Drawing.Point(318, 295);
            this.txtCommercialFee.Name = "txtCommercialFee";
            this.txtCommercialFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtCommercialFee.Properties.Mask.EditMask = "D";
            this.txtCommercialFee.Properties.MaxLength = 6;
            this.txtCommercialFee.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtCommercialFee.Size = new System.Drawing.Size(116, 20);
            this.txtCommercialFee.StyleController = this.layoutRootControl;
            this.txtCommercialFee.TabIndex = 15;
            // 
            // txtManagementFee
            // 
            this.txtManagementFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtManagementFee.Location = new System.Drawing.Point(107, 295);
            this.txtManagementFee.Name = "txtManagementFee";
            this.txtManagementFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtManagementFee.Properties.Mask.EditMask = "D";
            this.txtManagementFee.Properties.MaxLength = 6;
            this.txtManagementFee.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtManagementFee.Size = new System.Drawing.Size(125, 20);
            this.txtManagementFee.StyleController = this.layoutRootControl;
            this.txtManagementFee.TabIndex = 14;
            // 
            // txtPiClub
            // 
            this.txtPiClub.Location = new System.Drawing.Point(513, 223);
            this.txtPiClub.Name = "txtPiClub";
            this.txtPiClub.Size = new System.Drawing.Size(406, 20);
            this.txtPiClub.StyleController = this.layoutRootControl;
            this.txtPiClub.TabIndex = 43;
            // 
            // txtYard
            // 
            this.txtYard.Location = new System.Drawing.Point(266, 223);
            this.txtYard.Name = "txtYard";
            this.txtYard.Size = new System.Drawing.Size(195, 20);
            this.txtYard.StyleController = this.layoutRootControl;
            this.txtYard.TabIndex = 42;
            // 
            // txtFlag
            // 
            this.txtFlag.Location = new System.Drawing.Point(44, 223);
            this.txtFlag.Name = "txtFlag";
            this.txtFlag.Size = new System.Drawing.Size(189, 20);
            this.txtFlag.StyleController = this.layoutRootControl;
            this.txtFlag.TabIndex = 41;
            // 
            // txtClassification
            // 
            this.txtClassification.Location = new System.Drawing.Point(577, 199);
            this.txtClassification.Name = "txtClassification";
            this.txtClassification.Size = new System.Drawing.Size(342, 20);
            this.txtClassification.StyleController = this.layoutRootControl;
            this.txtClassification.TabIndex = 40;
            // 
            // txtBeam
            // 
            this.txtBeam.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBeam.Location = new System.Drawing.Point(438, 199);
            this.txtBeam.Name = "txtBeam";
            this.txtBeam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBeam.Properties.Mask.EditMask = "D";
            this.txtBeam.Properties.MaxLength = 3;
            this.txtBeam.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtBeam.Size = new System.Drawing.Size(66, 20);
            this.txtBeam.StyleController = this.layoutRootControl;
            this.txtBeam.TabIndex = 15;
            // 
            // txtDieselLadenPort
            // 
            this.txtDieselLadenPort.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDieselLadenPort.Location = new System.Drawing.Point(374, 271);
            this.txtDieselLadenPort.Name = "txtDieselLadenPort";
            this.txtDieselLadenPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDieselLadenPort.Properties.Mask.EditMask = "f2";
            this.txtDieselLadenPort.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDieselLadenPort.Properties.MaxLength = 6;
            this.txtDieselLadenPort.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtDieselLadenPort.Size = new System.Drawing.Size(119, 20);
            this.txtDieselLadenPort.StyleController = this.layoutRootControl;
            this.txtDieselLadenPort.TabIndex = 39;
            // 
            // txtDieselBallastSea
            // 
            this.txtDieselBallastSea.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDieselBallastSea.Location = new System.Drawing.Point(134, 271);
            this.txtDieselBallastSea.Name = "txtDieselBallastSea";
            this.txtDieselBallastSea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDieselBallastSea.Properties.Mask.EditMask = "f2";
            this.txtDieselBallastSea.Properties.MaxLength = 6;
            this.txtDieselBallastSea.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtDieselBallastSea.Size = new System.Drawing.Size(118, 20);
            this.txtDieselBallastSea.StyleController = this.layoutRootControl;
            this.txtDieselBallastSea.TabIndex = 37;
            // 
            // txtFuelOilPort
            // 
            this.txtFuelOilPort.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFuelOilPort.Location = new System.Drawing.Point(511, 247);
            this.txtFuelOilPort.Name = "txtFuelOilPort";
            this.txtFuelOilPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtFuelOilPort.Properties.Mask.EditMask = "f2";
            this.txtFuelOilPort.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtFuelOilPort.Properties.MaxLength = 6;
            this.txtFuelOilPort.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtFuelOilPort.Size = new System.Drawing.Size(93, 20);
            this.txtFuelOilPort.StyleController = this.layoutRootControl;
            this.txtFuelOilPort.TabIndex = 36;
            // 
            // txtLoa
            // 
            this.txtLoa.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLoa.Location = new System.Drawing.Point(334, 199);
            this.txtLoa.Name = "txtLoa";
            this.txtLoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtLoa.Properties.Mask.EditMask = "D";
            this.txtLoa.Properties.MaxLength = 3;
            this.txtLoa.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtLoa.Size = new System.Drawing.Size(67, 20);
            this.txtLoa.StyleController = this.layoutRootControl;
            this.txtLoa.TabIndex = 14;
            // 
            // txtFuelLadenSea
            // 
            this.txtFuelLadenSea.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFuelLadenSea.Location = new System.Drawing.Point(332, 247);
            this.txtFuelLadenSea.Name = "txtFuelLadenSea";
            this.txtFuelLadenSea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtFuelLadenSea.Properties.Mask.EditMask = "f2";
            this.txtFuelLadenSea.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtFuelLadenSea.Properties.MaxLength = 6;
            this.txtFuelLadenSea.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtFuelLadenSea.Size = new System.Drawing.Size(97, 20);
            this.txtFuelLadenSea.StyleController = this.layoutRootControl;
            this.txtFuelLadenSea.TabIndex = 35;
            // 
            // txtOilLadenSea
            // 
            this.txtOilLadenSea.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtOilLadenSea.Location = new System.Drawing.Point(612, 271);
            this.txtOilLadenSea.Name = "txtOilLadenSea";
            this.txtOilLadenSea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtOilLadenSea.Properties.Mask.EditMask = "f2";
            this.txtOilLadenSea.Properties.MaxLength = 6;
            this.txtOilLadenSea.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtOilLadenSea.Size = new System.Drawing.Size(307, 20);
            this.txtOilLadenSea.StyleController = this.layoutRootControl;
            this.txtOilLadenSea.TabIndex = 38;
            // 
            // txtFuelBallastSea
            // 
            this.txtFuelBallastSea.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFuelBallastSea.Location = new System.Drawing.Point(126, 247);
            this.txtFuelBallastSea.Name = "txtFuelBallastSea";
            this.txtFuelBallastSea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtFuelBallastSea.Properties.Mask.EditMask = "f2";
            this.txtFuelBallastSea.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtFuelBallastSea.Properties.MaxLength = 6;
            this.txtFuelBallastSea.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtFuelBallastSea.Size = new System.Drawing.Size(95, 20);
            this.txtFuelBallastSea.StyleController = this.layoutRootControl;
            this.txtFuelBallastSea.TabIndex = 34;
            // 
            // txtTPC
            // 
            this.txtTPC.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTPC.Location = new System.Drawing.Point(237, 199);
            this.txtTPC.Name = "txtTPC";
            this.txtTPC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtTPC.Properties.Mask.EditMask = "f2";
            this.txtTPC.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTPC.Properties.MaxLength = 7;
            this.txtTPC.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtTPC.Size = new System.Drawing.Size(69, 20);
            this.txtTPC.StyleController = this.layoutRootControl;
            this.txtTPC.TabIndex = 33;
            // 
            // txtGrainCapacity
            // 
            this.txtGrainCapacity.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtGrainCapacity.Location = new System.Drawing.Point(94, 199);
            this.txtGrainCapacity.Name = "txtGrainCapacity";
            this.txtGrainCapacity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtGrainCapacity.Properties.Mask.EditMask = "D";
            this.txtGrainCapacity.Properties.MaxLength = 12;
            this.txtGrainCapacity.Properties.MaxValue = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.txtGrainCapacity.Size = new System.Drawing.Size(113, 20);
            this.txtGrainCapacity.StyleController = this.layoutRootControl;
            this.txtGrainCapacity.TabIndex = 13;
            // 
            // txtDraftTropic
            // 
            this.txtDraftTropic.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDraftTropic.Location = new System.Drawing.Point(469, 175);
            this.txtDraftTropic.Name = "txtDraftTropic";
            this.txtDraftTropic.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDraftTropic.Properties.Mask.EditMask = "f2";
            this.txtDraftTropic.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDraftTropic.Properties.MaxLength = 5;
            this.txtDraftTropic.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtDraftTropic.Size = new System.Drawing.Size(450, 20);
            this.txtDraftTropic.StyleController = this.layoutRootControl;
            this.txtDraftTropic.TabIndex = 34;
            // 
            // txtDraftSummer
            // 
            this.txtDraftSummer.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDraftSummer.Location = new System.Drawing.Point(283, 175);
            this.txtDraftSummer.Name = "txtDraftSummer";
            this.txtDraftSummer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDraftSummer.Properties.Mask.EditMask = "f2";
            this.txtDraftSummer.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDraftSummer.Properties.MaxLength = 5;
            this.txtDraftSummer.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtDraftSummer.Size = new System.Drawing.Size(118, 20);
            this.txtDraftSummer.StyleController = this.layoutRootControl;
            this.txtDraftSummer.TabIndex = 33;
            // 
            // txtDraftWinter
            // 
            this.txtDraftWinter.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDraftWinter.Location = new System.Drawing.Point(84, 175);
            this.txtDraftWinter.Name = "txtDraftWinter";
            this.txtDraftWinter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDraftWinter.Properties.Mask.EditMask = "f2";
            this.txtDraftWinter.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDraftWinter.Properties.MaxLength = 5;
            this.txtDraftWinter.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtDraftWinter.Size = new System.Drawing.Size(122, 20);
            this.txtDraftWinter.StyleController = this.layoutRootControl;
            this.txtDraftWinter.TabIndex = 32;
            // 
            // txtLightweight
            // 
            this.txtLightweight.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLightweight.Location = new System.Drawing.Point(838, 151);
            this.txtLightweight.Name = "txtLightweight";
            this.txtLightweight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtLightweight.Properties.Mask.EditMask = "D";
            this.txtLightweight.Properties.MaxLength = 6;
            this.txtLightweight.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtLightweight.Size = new System.Drawing.Size(81, 20);
            this.txtLightweight.StyleController = this.layoutRootControl;
            this.txtLightweight.TabIndex = 12;
            // 
            // txtDeadweightSummer
            // 
            this.txtDeadweightSummer.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDeadweightSummer.Location = new System.Drawing.Point(485, 151);
            this.txtDeadweightSummer.Name = "txtDeadweightSummer";
            this.txtDeadweightSummer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDeadweightSummer.Properties.Mask.EditMask = "D";
            this.txtDeadweightSummer.Properties.MaxLength = 6;
            this.txtDeadweightSummer.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtDeadweightSummer.Size = new System.Drawing.Size(97, 20);
            this.txtDeadweightSummer.StyleController = this.layoutRootControl;
            this.txtDeadweightSummer.TabIndex = 10;
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(821, 638);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Size = new System.Drawing.Size(101, 20);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 8;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(560, 638);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(189, 20);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 11;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(388, 638);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Size = new System.Drawing.Size(101, 20);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 7;
            // 
            // txtDeadweightTropic
            // 
            this.txtDeadweightTropic.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDeadweightTropic.Location = new System.Drawing.Point(682, 151);
            this.txtDeadweightTropic.Name = "txtDeadweightTropic";
            this.txtDeadweightTropic.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDeadweightTropic.Properties.Mask.EditMask = "D";
            this.txtDeadweightTropic.Properties.MaxLength = 6;
            this.txtDeadweightTropic.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtDeadweightTropic.Size = new System.Drawing.Size(90, 20);
            this.txtDeadweightTropic.StyleController = this.layoutRootControl;
            this.txtDeadweightTropic.TabIndex = 11;
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(87, 638);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(223, 20);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 10;
            // 
            // txtDeadWeightWinter
            // 
            this.txtDeadWeightWinter.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDeadWeightWinter.Location = new System.Drawing.Point(280, 151);
            this.txtDeadWeightWinter.Name = "txtDeadWeightWinter";
            this.txtDeadWeightWinter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDeadWeightWinter.Properties.Mask.EditMask = "D";
            this.txtDeadWeightWinter.Properties.MaxLength = 6;
            this.txtDeadWeightWinter.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtDeadWeightWinter.Size = new System.Drawing.Size(96, 20);
            this.txtDeadWeightWinter.StyleController = this.layoutRootControl;
            this.txtDeadWeightWinter.TabIndex = 9;
            // 
            // lookupCompany
            // 
            this.lookupCompany.Location = new System.Drawing.Point(66, 57);
            this.lookupCompany.Name = "lookupCompany";
            this.lookupCompany.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupCompany.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupCompany.Size = new System.Drawing.Size(174, 20);
            this.lookupCompany.StyleController = this.layoutRootControl;
            this.lookupCompany.TabIndex = 21;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(31, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(81, 20);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // lookupMarket
            // 
            this.lookupMarket.Location = new System.Drawing.Point(284, 57);
            this.lookupMarket.Name = "lookupMarket";
            this.lookupMarket.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMarket.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupMarket.Size = new System.Drawing.Size(170, 20);
            this.lookupMarket.StyleController = this.layoutRootControl;
            this.lookupMarket.TabIndex = 20;
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(821, 33);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(101, 20);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(150, 33);
            this.txtName.Name = "txtName";
            this.txtName.Properties.MaxLength = 1000;
            this.txtName.Size = new System.Drawing.Size(629, 20);
            this.txtName.StyleController = this.layoutRootControl;
            this.txtName.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupVessel,
            this.layoutGroupUserInfo});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(936, 672);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupVessel
            // 
            this.layoutGroupVessel.CustomizationFormText = "Company";
            this.layoutGroupVessel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutId,
            this.layoutStatus,
            this.layoutCompany,
            this.layoutMarket,
            this.layoutItemIMONumber,
            this.layoutItemBenchInitial,
            this.layoutItemBenchAgePoints,
            this.layoutItemBenchAgeFrequency,
            this.lcgVesselInfo,
            this.layoutItemDateBuilt,
            this.layoutItemAcquisitionPrice,
            this.layoutItemMarketPrice,
            this.layoutItemBenchmark,
            this.lcgExpenses});
            this.layoutGroupVessel.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupVessel.Name = "layoutGroupVessel";
            this.layoutGroupVessel.Size = new System.Drawing.Size(936, 605);
            this.layoutGroupVessel.Text = "Vessel";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "Name:";
            this.layoutName.Location = new System.Drawing.Point(102, 0);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(667, 24);
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(31, 13);
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MaxSize = new System.Drawing.Size(102, 24);
            this.layoutId.MinSize = new System.Drawing.Size(102, 24);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(102, 24);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(14, 13);
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(769, 0);
            this.layoutStatus.MaxSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.MinSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(143, 24);
            this.layoutStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutCompany
            // 
            this.layoutCompany.Control = this.lookupCompany;
            this.layoutCompany.CustomizationFormText = "Company:";
            this.layoutCompany.Location = new System.Drawing.Point(0, 24);
            this.layoutCompany.Name = "layoutCompany";
            this.layoutCompany.Size = new System.Drawing.Size(230, 24);
            this.layoutCompany.Text = "Company:";
            this.layoutCompany.TextSize = new System.Drawing.Size(49, 13);
            // 
            // layoutMarket
            // 
            this.layoutMarket.Control = this.lookupMarket;
            this.layoutMarket.CustomizationFormText = "Profit Centre:";
            this.layoutMarket.Location = new System.Drawing.Point(230, 24);
            this.layoutMarket.Name = "layoutMarket";
            this.layoutMarket.Size = new System.Drawing.Size(214, 24);
            this.layoutMarket.Text = "Market:";
            this.layoutMarket.TextSize = new System.Drawing.Size(37, 13);
            // 
            // layoutItemIMONumber
            // 
            this.layoutItemIMONumber.Control = this.txtImoNumber;
            this.layoutItemIMONumber.CustomizationFormText = "IMO Number:";
            this.layoutItemIMONumber.Location = new System.Drawing.Point(444, 24);
            this.layoutItemIMONumber.Name = "layoutItemIMONumber";
            this.layoutItemIMONumber.Size = new System.Drawing.Size(194, 24);
            this.layoutItemIMONumber.Text = "IMO Number:";
            this.layoutItemIMONumber.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutItemBenchInitial
            // 
            this.layoutItemBenchInitial.Control = this.txtBenchInitial;
            this.layoutItemBenchInitial.CustomizationFormText = "Bench Initial:";
            this.layoutItemBenchInitial.Location = new System.Drawing.Point(0, 48);
            this.layoutItemBenchInitial.Name = "layoutItemBenchInitial";
            this.layoutItemBenchInitial.Size = new System.Drawing.Size(146, 24);
            this.layoutItemBenchInitial.Text = "Bench Initial:";
            this.layoutItemBenchInitial.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutItemBenchAgePoints
            // 
            this.layoutItemBenchAgePoints.Control = this.txtBenchAgePoints;
            this.layoutItemBenchAgePoints.CustomizationFormText = "Bench Age Points:";
            this.layoutItemBenchAgePoints.Location = new System.Drawing.Point(146, 48);
            this.layoutItemBenchAgePoints.Name = "layoutItemBenchAgePoints";
            this.layoutItemBenchAgePoints.Size = new System.Drawing.Size(172, 24);
            this.layoutItemBenchAgePoints.Text = "Bench Age Points:";
            this.layoutItemBenchAgePoints.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutItemBenchAgeFrequency
            // 
            this.layoutItemBenchAgeFrequency.Control = this.txtBenchAgeFrequency;
            this.layoutItemBenchAgeFrequency.CustomizationFormText = "layoutItemBenchAgeFrequency";
            this.layoutItemBenchAgeFrequency.Location = new System.Drawing.Point(318, 48);
            this.layoutItemBenchAgeFrequency.Name = "layoutItemBenchAgeFrequency";
            this.layoutItemBenchAgeFrequency.Size = new System.Drawing.Size(186, 24);
            this.layoutItemBenchAgeFrequency.Text = "Bench Age Frequency:";
            this.layoutItemBenchAgeFrequency.TextSize = new System.Drawing.Size(109, 13);
            // 
            // lcgVesselInfo
            // 
            this.lcgVesselInfo.CustomizationFormText = "Vessel Details";
            this.lcgVesselInfo.ExpandButtonVisible = true;
            this.lcgVesselInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemDeadWeightWinter,
            this.layoutItemDeadWeightSummer,
            this.layoutItemDeadWeightTropic,
            this.layoutLightweight,
            this.layoutItemDraftWinter,
            this.layoutDraftSummer,
            this.layoutItemDraftTropic,
            this.layoutItemGrainCapacity,
            this.layoutItemTpc,
            this.layoutItemLoa,
            this.layoutItemBeam,
            this.layoutItemClassification,
            this.layoutItemFlag,
            this.layoutItemYard,
            this.layoutItemPiClub,
            this.layoutItemFuelOilBallastSea,
            this.layoutItemFuleOilLadenAtSea,
            this.layoutItemFuelOilAtPort,
            this.layoutItemDieselOilBallastAtSea,
            this.layoutItemDieselOilLadenAtPort,
            this.layoutItemDieselOilLadenAtSea,
            this.layoutItemManagementFee,
            this.layoutItemCommercialFee,
            this.layoutItemContact,
            this.layoutItemMasterOfficer,
            this.layoutItemChiefOfficer,
            this.layoutItemTechnicalManager,
            this.emptySpaceItem1,
            this.layoutEquity,
            this.layoutDeliveryDate,
            this.layoutItemSaleDate,
            this.layoutItemSaleAmount,
            this.layoutItemUsefulLife,
            this.emptySpaceItem2,
            this.layoutMOADate,
            this.layoutItemDeadWeight});
            this.lcgVesselInfo.Location = new System.Drawing.Point(0, 72);
            this.lcgVesselInfo.Name = "lcgVesselInfo";
            this.lcgVesselInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgVesselInfo.Size = new System.Drawing.Size(912, 241);
            this.lcgVesselInfo.Text = "Vessel Details";
            // 
            // layoutItemDeadWeightWinter
            // 
            this.layoutItemDeadWeightWinter.Control = this.txtDeadWeightWinter;
            this.layoutItemDeadWeightWinter.CustomizationFormText = "Deadweight Winter:";
            this.layoutItemDeadWeightWinter.Location = new System.Drawing.Point(164, 24);
            this.layoutItemDeadWeightWinter.Name = "layoutItemDeadWeightWinter";
            this.layoutItemDeadWeightWinter.Size = new System.Drawing.Size(199, 24);
            this.layoutItemDeadWeightWinter.Text = "Deadweight Winter:";
            this.layoutItemDeadWeightWinter.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutItemDeadWeightSummer
            // 
            this.layoutItemDeadWeightSummer.Control = this.txtDeadweightSummer;
            this.layoutItemDeadWeightSummer.CustomizationFormText = "Deadweight Summer:";
            this.layoutItemDeadWeightSummer.Location = new System.Drawing.Point(363, 24);
            this.layoutItemDeadWeightSummer.Name = "layoutItemDeadWeightSummer";
            this.layoutItemDeadWeightSummer.Size = new System.Drawing.Size(206, 24);
            this.layoutItemDeadWeightSummer.Text = "Deadweight Summer:";
            this.layoutItemDeadWeightSummer.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutItemDeadWeightTropic
            // 
            this.layoutItemDeadWeightTropic.Control = this.txtDeadweightTropic;
            this.layoutItemDeadWeightTropic.CustomizationFormText = "layoutControlItem1";
            this.layoutItemDeadWeightTropic.Location = new System.Drawing.Point(569, 24);
            this.layoutItemDeadWeightTropic.Name = "layoutItemDeadWeightTropic";
            this.layoutItemDeadWeightTropic.Size = new System.Drawing.Size(190, 24);
            this.layoutItemDeadWeightTropic.Text = "Deadweight Tropic:";
            this.layoutItemDeadWeightTropic.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutLightweight
            // 
            this.layoutLightweight.Control = this.txtLightweight;
            this.layoutLightweight.CustomizationFormText = "Lightweight:";
            this.layoutLightweight.Location = new System.Drawing.Point(759, 24);
            this.layoutLightweight.Name = "layoutLightweight";
            this.layoutLightweight.Size = new System.Drawing.Size(147, 24);
            this.layoutLightweight.Text = "Lightweight:";
            this.layoutLightweight.TextSize = new System.Drawing.Size(59, 13);
            // 
            // layoutItemDraftWinter
            // 
            this.layoutItemDraftWinter.Control = this.txtDraftWinter;
            this.layoutItemDraftWinter.CustomizationFormText = "layoutControlItem1";
            this.layoutItemDraftWinter.Location = new System.Drawing.Point(0, 48);
            this.layoutItemDraftWinter.Name = "layoutItemDraftWinter";
            this.layoutItemDraftWinter.Size = new System.Drawing.Size(193, 24);
            this.layoutItemDraftWinter.Text = "Draft Winter:";
            this.layoutItemDraftWinter.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutDraftSummer
            // 
            this.layoutDraftSummer.Control = this.txtDraftSummer;
            this.layoutDraftSummer.CustomizationFormText = "Draft Summer:";
            this.layoutDraftSummer.Location = new System.Drawing.Point(193, 48);
            this.layoutDraftSummer.Name = "layoutDraftSummer";
            this.layoutDraftSummer.Size = new System.Drawing.Size(195, 24);
            this.layoutDraftSummer.Text = "Draft Summer:";
            this.layoutDraftSummer.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutItemDraftTropic
            // 
            this.layoutItemDraftTropic.Control = this.txtDraftTropic;
            this.layoutItemDraftTropic.CustomizationFormText = "Draft Tropic:";
            this.layoutItemDraftTropic.Location = new System.Drawing.Point(388, 48);
            this.layoutItemDraftTropic.Name = "layoutItemDraftTropic";
            this.layoutItemDraftTropic.Size = new System.Drawing.Size(518, 24);
            this.layoutItemDraftTropic.Text = "Draft Tropic:";
            this.layoutItemDraftTropic.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutItemGrainCapacity
            // 
            this.layoutItemGrainCapacity.Control = this.txtGrainCapacity;
            this.layoutItemGrainCapacity.CustomizationFormText = "Grain Capacity:";
            this.layoutItemGrainCapacity.Location = new System.Drawing.Point(0, 72);
            this.layoutItemGrainCapacity.Name = "layoutItemGrainCapacity";
            this.layoutItemGrainCapacity.Size = new System.Drawing.Size(194, 24);
            this.layoutItemGrainCapacity.Text = "Grain Capacity:";
            this.layoutItemGrainCapacity.TextSize = new System.Drawing.Size(74, 13);
            // 
            // layoutItemTpc
            // 
            this.layoutItemTpc.Control = this.txtTPC;
            this.layoutItemTpc.CustomizationFormText = "TPC:";
            this.layoutItemTpc.Location = new System.Drawing.Point(194, 72);
            this.layoutItemTpc.Name = "layoutItemTpc";
            this.layoutItemTpc.Size = new System.Drawing.Size(99, 24);
            this.layoutItemTpc.Text = "TPC:";
            this.layoutItemTpc.TextSize = new System.Drawing.Size(23, 13);
            // 
            // layoutItemLoa
            // 
            this.layoutItemLoa.Control = this.txtLoa;
            this.layoutItemLoa.CustomizationFormText = "Loa:";
            this.layoutItemLoa.Location = new System.Drawing.Point(293, 72);
            this.layoutItemLoa.Name = "layoutItemLoa";
            this.layoutItemLoa.Size = new System.Drawing.Size(95, 24);
            this.layoutItemLoa.Text = "Loa:";
            this.layoutItemLoa.TextSize = new System.Drawing.Size(21, 13);
            // 
            // layoutItemBeam
            // 
            this.layoutItemBeam.Control = this.txtBeam;
            this.layoutItemBeam.CustomizationFormText = "Beam:";
            this.layoutItemBeam.Location = new System.Drawing.Point(388, 72);
            this.layoutItemBeam.Name = "layoutItemBeam";
            this.layoutItemBeam.Size = new System.Drawing.Size(103, 24);
            this.layoutItemBeam.Text = "Beam:";
            this.layoutItemBeam.TextSize = new System.Drawing.Size(30, 13);
            // 
            // layoutItemClassification
            // 
            this.layoutItemClassification.Control = this.txtClassification;
            this.layoutItemClassification.CustomizationFormText = "Classification:";
            this.layoutItemClassification.Location = new System.Drawing.Point(491, 72);
            this.layoutItemClassification.Name = "layoutItemClassification";
            this.layoutItemClassification.Size = new System.Drawing.Size(415, 24);
            this.layoutItemClassification.Text = "Classification:";
            this.layoutItemClassification.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutItemFlag
            // 
            this.layoutItemFlag.Control = this.txtFlag;
            this.layoutItemFlag.CustomizationFormText = "Flag:";
            this.layoutItemFlag.Location = new System.Drawing.Point(0, 96);
            this.layoutItemFlag.Name = "layoutItemFlag";
            this.layoutItemFlag.Size = new System.Drawing.Size(220, 24);
            this.layoutItemFlag.Text = "Flag:";
            this.layoutItemFlag.TextSize = new System.Drawing.Size(24, 13);
            // 
            // layoutItemYard
            // 
            this.layoutItemYard.Control = this.txtYard;
            this.layoutItemYard.CustomizationFormText = "Yard:";
            this.layoutItemYard.Location = new System.Drawing.Point(220, 96);
            this.layoutItemYard.Name = "layoutItemYard";
            this.layoutItemYard.Size = new System.Drawing.Size(228, 24);
            this.layoutItemYard.Text = "Yard:";
            this.layoutItemYard.TextSize = new System.Drawing.Size(26, 13);
            // 
            // layoutItemPiClub
            // 
            this.layoutItemPiClub.Control = this.txtPiClub;
            this.layoutItemPiClub.CustomizationFormText = "P&&I Club:";
            this.layoutItemPiClub.Location = new System.Drawing.Point(448, 96);
            this.layoutItemPiClub.Name = "layoutItemPiClub";
            this.layoutItemPiClub.Size = new System.Drawing.Size(458, 24);
            this.layoutItemPiClub.Text = "P&&I Club:";
            this.layoutItemPiClub.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutItemFuelOilBallastSea
            // 
            this.layoutItemFuelOilBallastSea.Control = this.txtFuelBallastSea;
            this.layoutItemFuelOilBallastSea.CustomizationFormText = "Fuel Oil Ballast at sea:";
            this.layoutItemFuelOilBallastSea.Location = new System.Drawing.Point(0, 120);
            this.layoutItemFuelOilBallastSea.Name = "layoutItemFuelOilBallastSea";
            this.layoutItemFuelOilBallastSea.Size = new System.Drawing.Size(208, 24);
            this.layoutItemFuelOilBallastSea.Text = "Fuel Oil Ballast at sea:";
            this.layoutItemFuelOilBallastSea.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutItemFuleOilLadenAtSea
            // 
            this.layoutItemFuleOilLadenAtSea.Control = this.txtFuelLadenSea;
            this.layoutItemFuleOilLadenAtSea.CustomizationFormText = "Fuel Oil Laden at sea:";
            this.layoutItemFuleOilLadenAtSea.Location = new System.Drawing.Point(208, 120);
            this.layoutItemFuleOilLadenAtSea.Name = "layoutItemFuleOilLadenAtSea";
            this.layoutItemFuleOilLadenAtSea.Size = new System.Drawing.Size(208, 24);
            this.layoutItemFuleOilLadenAtSea.Text = "Fuel Oil Laden at sea:";
            this.layoutItemFuleOilLadenAtSea.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutItemFuelOilAtPort
            // 
            this.layoutItemFuelOilAtPort.Control = this.txtFuelOilPort;
            this.layoutItemFuelOilAtPort.CustomizationFormText = "Fuel Oil at port:";
            this.layoutItemFuelOilAtPort.Location = new System.Drawing.Point(416, 120);
            this.layoutItemFuelOilAtPort.Name = "layoutItemFuelOilAtPort";
            this.layoutItemFuelOilAtPort.Size = new System.Drawing.Size(175, 24);
            this.layoutItemFuelOilAtPort.Text = "Fuel Oil at port:";
            this.layoutItemFuelOilAtPort.TextSize = new System.Drawing.Size(75, 13);
            // 
            // layoutItemDieselOilBallastAtSea
            // 
            this.layoutItemDieselOilBallastAtSea.Control = this.txtDieselBallastSea;
            this.layoutItemDieselOilBallastAtSea.CustomizationFormText = "Diesel Oil Ballast at sea:";
            this.layoutItemDieselOilBallastAtSea.Location = new System.Drawing.Point(0, 144);
            this.layoutItemDieselOilBallastAtSea.Name = "layoutItemDieselOilBallastAtSea";
            this.layoutItemDieselOilBallastAtSea.Size = new System.Drawing.Size(239, 24);
            this.layoutItemDieselOilBallastAtSea.Text = "Diesel Oil Ballast at sea:";
            this.layoutItemDieselOilBallastAtSea.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutItemDieselOilLadenAtPort
            // 
            this.layoutItemDieselOilLadenAtPort.Control = this.txtDieselLadenPort;
            this.layoutItemDieselOilLadenAtPort.CustomizationFormText = "layoutControlItem1";
            this.layoutItemDieselOilLadenAtPort.Location = new System.Drawing.Point(239, 144);
            this.layoutItemDieselOilLadenAtPort.Name = "layoutItemDieselOilLadenAtPort";
            this.layoutItemDieselOilLadenAtPort.Size = new System.Drawing.Size(241, 24);
            this.layoutItemDieselOilLadenAtPort.Text = "Diesel Oil Laden at port:";
            this.layoutItemDieselOilLadenAtPort.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutItemDieselOilLadenAtSea
            // 
            this.layoutItemDieselOilLadenAtSea.Control = this.txtOilLadenSea;
            this.layoutItemDieselOilLadenAtSea.CustomizationFormText = "Diesel Oil Laden at sea:";
            this.layoutItemDieselOilLadenAtSea.Location = new System.Drawing.Point(480, 144);
            this.layoutItemDieselOilLadenAtSea.Name = "layoutItemDieselOilLadenAtSea";
            this.layoutItemDieselOilLadenAtSea.Size = new System.Drawing.Size(426, 24);
            this.layoutItemDieselOilLadenAtSea.Text = "Diesel Oil Laden at sea:";
            this.layoutItemDieselOilLadenAtSea.TextSize = new System.Drawing.Size(112, 13);
            // 
            // layoutItemManagementFee
            // 
            this.layoutItemManagementFee.Control = this.txtManagementFee;
            this.layoutItemManagementFee.CustomizationFormText = "Management Fee:";
            this.layoutItemManagementFee.Location = new System.Drawing.Point(0, 168);
            this.layoutItemManagementFee.Name = "layoutItemManagementFee";
            this.layoutItemManagementFee.Size = new System.Drawing.Size(219, 24);
            this.layoutItemManagementFee.Text = "Management Fee:";
            this.layoutItemManagementFee.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutItemCommercialFee
            // 
            this.layoutItemCommercialFee.Control = this.txtCommercialFee;
            this.layoutItemCommercialFee.CustomizationFormText = "Commercial Fee:";
            this.layoutItemCommercialFee.Location = new System.Drawing.Point(219, 168);
            this.layoutItemCommercialFee.Name = "layoutItemCommercialFee";
            this.layoutItemCommercialFee.Size = new System.Drawing.Size(202, 24);
            this.layoutItemCommercialFee.Text = "Commercial Fee:";
            this.layoutItemCommercialFee.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutItemContact
            // 
            this.layoutItemContact.Control = this.lookupContact;
            this.layoutItemContact.CustomizationFormText = "Contact:";
            this.layoutItemContact.Location = new System.Drawing.Point(0, 192);
            this.layoutItemContact.Name = "layoutItemContact";
            this.layoutItemContact.Size = new System.Drawing.Size(224, 24);
            this.layoutItemContact.Text = "Contact:";
            this.layoutItemContact.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutItemMasterOfficer
            // 
            this.layoutItemMasterOfficer.Control = this.lookUpMasterOfficer;
            this.layoutItemMasterOfficer.CustomizationFormText = "Master Officer:";
            this.layoutItemMasterOfficer.Location = new System.Drawing.Point(224, 192);
            this.layoutItemMasterOfficer.Name = "layoutItemMasterOfficer";
            this.layoutItemMasterOfficer.Size = new System.Drawing.Size(229, 24);
            this.layoutItemMasterOfficer.Text = "Master Officer:";
            this.layoutItemMasterOfficer.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutItemChiefOfficer
            // 
            this.layoutItemChiefOfficer.Control = this.lookUpChiefOfficer;
            this.layoutItemChiefOfficer.CustomizationFormText = "Chief Officer:";
            this.layoutItemChiefOfficer.Location = new System.Drawing.Point(453, 192);
            this.layoutItemChiefOfficer.Name = "layoutItemChiefOfficer";
            this.layoutItemChiefOfficer.Size = new System.Drawing.Size(227, 24);
            this.layoutItemChiefOfficer.Text = "Chief Officer:";
            this.layoutItemChiefOfficer.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutItemTechnicalManager
            // 
            this.layoutItemTechnicalManager.Control = this.lookUpTechnicalManager;
            this.layoutItemTechnicalManager.CustomizationFormText = "Technical Manager:";
            this.layoutItemTechnicalManager.Location = new System.Drawing.Point(680, 192);
            this.layoutItemTechnicalManager.Name = "layoutItemTechnicalManager";
            this.layoutItemTechnicalManager.Size = new System.Drawing.Size(226, 24);
            this.layoutItemTechnicalManager.Text = "Technical Manager:";
            this.layoutItemTechnicalManager.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(591, 120);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(315, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutEquity
            // 
            this.layoutEquity.Control = this.txtEquity;
            this.layoutEquity.CustomizationFormText = "Equity:";
            this.layoutEquity.Location = new System.Drawing.Point(421, 168);
            this.layoutEquity.Name = "layoutEquity";
            this.layoutEquity.Size = new System.Drawing.Size(157, 24);
            this.layoutEquity.Text = "Equity:";
            this.layoutEquity.TextSize = new System.Drawing.Size(34, 13);
            // 
            // layoutDeliveryDate
            // 
            this.layoutDeliveryDate.Control = this.dtpDeliveryDate;
            this.layoutDeliveryDate.CustomizationFormText = "Delivery Date:";
            this.layoutDeliveryDate.Location = new System.Drawing.Point(0, 0);
            this.layoutDeliveryDate.Name = "layoutDeliveryDate";
            this.layoutDeliveryDate.Size = new System.Drawing.Size(205, 24);
            this.layoutDeliveryDate.Text = "Delivery Date:";
            this.layoutDeliveryDate.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutItemSaleDate
            // 
            this.layoutItemSaleDate.Control = this.dtpSaleDate;
            this.layoutItemSaleDate.CustomizationFormText = "Sale Date:";
            this.layoutItemSaleDate.Location = new System.Drawing.Point(428, 0);
            this.layoutItemSaleDate.Name = "layoutItemSaleDate";
            this.layoutItemSaleDate.Size = new System.Drawing.Size(172, 24);
            this.layoutItemSaleDate.Text = "Sale Date:";
            this.layoutItemSaleDate.TextSize = new System.Drawing.Size(50, 13);
            // 
            // layoutItemSaleAmount
            // 
            this.layoutItemSaleAmount.Control = this.txtSaleAmount;
            this.layoutItemSaleAmount.CustomizationFormText = "Sale Amount:";
            this.layoutItemSaleAmount.Location = new System.Drawing.Point(600, 0);
            this.layoutItemSaleAmount.Name = "layoutItemSaleAmount";
            this.layoutItemSaleAmount.Size = new System.Drawing.Size(166, 24);
            this.layoutItemSaleAmount.Text = "Sale Amount:";
            this.layoutItemSaleAmount.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutItemUsefulLife
            // 
            this.layoutItemUsefulLife.Control = this.txtUsefulLife;
            this.layoutItemUsefulLife.CustomizationFormText = "Useful Life:";
            this.layoutItemUsefulLife.Location = new System.Drawing.Point(766, 0);
            this.layoutItemUsefulLife.Name = "layoutItemUsefulLife";
            this.layoutItemUsefulLife.Size = new System.Drawing.Size(140, 24);
            this.layoutItemUsefulLife.Text = "Useful Life:";
            this.layoutItemUsefulLife.TextSize = new System.Drawing.Size(54, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(578, 168);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(328, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutMOADate
            // 
            this.layoutMOADate.Control = this.dtpMOADate;
            this.layoutMOADate.CustomizationFormText = "MOA Date:";
            this.layoutMOADate.Location = new System.Drawing.Point(205, 0);
            this.layoutMOADate.Name = "layoutMOADate";
            this.layoutMOADate.Size = new System.Drawing.Size(223, 24);
            this.layoutMOADate.Text = "MOA Date:";
            this.layoutMOADate.TextSize = new System.Drawing.Size(53, 13);
            // 
            // layoutItemDeadWeight
            // 
            this.layoutItemDeadWeight.Control = this.txtDeadWeight;
            this.layoutItemDeadWeight.CustomizationFormText = "Deadweight:";
            this.layoutItemDeadWeight.Location = new System.Drawing.Point(0, 24);
            this.layoutItemDeadWeight.Name = "layoutItemDeadWeight";
            this.layoutItemDeadWeight.Size = new System.Drawing.Size(164, 24);
            this.layoutItemDeadWeight.Text = "Deadweight:";
            this.layoutItemDeadWeight.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutItemDateBuilt
            // 
            this.layoutItemDateBuilt.Control = this.dtpDateBuilt;
            this.layoutItemDateBuilt.CustomizationFormText = "Date Built:";
            this.layoutItemDateBuilt.Location = new System.Drawing.Point(638, 24);
            this.layoutItemDateBuilt.Name = "layoutItemDateBuilt";
            this.layoutItemDateBuilt.Size = new System.Drawing.Size(274, 24);
            this.layoutItemDateBuilt.Text = "Date Built:";
            this.layoutItemDateBuilt.TextSize = new System.Drawing.Size(50, 13);
            // 
            // layoutItemAcquisitionPrice
            // 
            this.layoutItemAcquisitionPrice.Control = this.txtAcquisitionPrice;
            this.layoutItemAcquisitionPrice.CustomizationFormText = "Acquisition Price:";
            this.layoutItemAcquisitionPrice.Location = new System.Drawing.Point(504, 48);
            this.layoutItemAcquisitionPrice.Name = "layoutItemAcquisitionPrice";
            this.layoutItemAcquisitionPrice.Size = new System.Drawing.Size(152, 24);
            this.layoutItemAcquisitionPrice.Text = "Acquisition Price:";
            this.layoutItemAcquisitionPrice.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutItemMarketPrice
            // 
            this.layoutItemMarketPrice.Control = this.txtMarketPrice;
            this.layoutItemMarketPrice.CustomizationFormText = "Market Price:";
            this.layoutItemMarketPrice.Location = new System.Drawing.Point(656, 48);
            this.layoutItemMarketPrice.Name = "layoutItemMarketPrice";
            this.layoutItemMarketPrice.Size = new System.Drawing.Size(132, 24);
            this.layoutItemMarketPrice.Text = "Market Price:";
            this.layoutItemMarketPrice.TextSize = new System.Drawing.Size(63, 13);
            // 
            // layoutItemBenchmark
            // 
            this.layoutItemBenchmark.Control = this.txtBenchmark;
            this.layoutItemBenchmark.CustomizationFormText = "Benchmark:";
            this.layoutItemBenchmark.Location = new System.Drawing.Point(788, 48);
            this.layoutItemBenchmark.Name = "layoutItemBenchmark";
            this.layoutItemBenchmark.Size = new System.Drawing.Size(124, 24);
            this.layoutItemBenchmark.Text = "Benchmark:";
            this.layoutItemBenchmark.TextSize = new System.Drawing.Size(56, 13);
            // 
            // lcgExpenses
            // 
            this.lcgExpenses.CustomizationFormText = "Expenses";
            this.lcgExpenses.ExpandButtonVisible = true;
            this.lcgExpenses.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemExpenses});
            this.lcgExpenses.Location = new System.Drawing.Point(0, 313);
            this.lcgExpenses.Name = "lcgExpenses";
            this.lcgExpenses.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgExpenses.Size = new System.Drawing.Size(912, 249);
            this.lcgExpenses.Text = "Expenses";
            // 
            // layoutItemExpenses
            // 
            this.layoutItemExpenses.Control = this.grdExpenses;
            this.layoutItemExpenses.CustomizationFormText = "layoutItemExpenses";
            this.layoutItemExpenses.Location = new System.Drawing.Point(0, 0);
            this.layoutItemExpenses.Name = "layoutItemExpenses";
            this.layoutItemExpenses.Size = new System.Drawing.Size(906, 224);
            this.layoutItemExpenses.Text = "layoutItemExpenses";
            this.layoutItemExpenses.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemExpenses.TextToControlDistance = 0;
            this.layoutItemExpenses.TextVisible = false;
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "User Info";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 605);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(936, 67);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "Creation User:";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(300, 24);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(300, 0);
            this.layoutCreationDate.MaxSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.MinSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(479, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(260, 24);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(739, 0);
            this.layoutUpdateDate.MaxSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.MinSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(366, 67);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(953, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 556);
            this.barDockControlBottom.Size = new System.Drawing.Size(953, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 556);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(953, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 556);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // VesselAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 588);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "VesselAEVForm";
            this.Activated += new System.EventHandler(this.VesselAEVForm_Activated);
            this.Deactivate += new System.EventHandler(this.VesselAEVForm_Deactivate);
            this.Load += new System.EventHandler(this.VesselAEVForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpMOADate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpMOADate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDeliveryDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDeliveryDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOperationExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDryDockExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDryDockOffHire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepreciationProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTechnicalManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpChiefOfficer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpMasterOfficer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchmark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcquisitionPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchAgeFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchAgePoints.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenchInitial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateBuilt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateBuilt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImoNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSaleDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSaleDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsefulLife.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercialFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManagementFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPiClub.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClassification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDieselLadenPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDieselBallastSea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelOilPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelLadenSea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOilLadenSea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelBallastSea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrainCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDraftTropic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDraftSummer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDraftWinter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLightweight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadweightSummer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadweightTropic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadWeightWinter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupVessel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemIMONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchInitial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchAgePoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchAgeFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgVesselInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeightWinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeightSummer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeightTropic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLightweight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDraftWinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDraftSummer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDraftTropic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGrainCapacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTpc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemLoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemClassification)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemYard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPiClub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFuelOilBallastSea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFuleOilLadenAtSea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFuelOilAtPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDieselOilBallastAtSea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDieselOilLadenAtPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDieselOilLadenAtSea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemManagementFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCommercialFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemMasterOfficer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemChiefOfficer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTechnicalManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEquity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSaleDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSaleAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemUsefulLife)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMOADate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDeadWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDateBuilt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAcquisitionPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemMarketPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBenchmark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraEditors.LookUpEdit lookupMarket;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupVessel;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarket;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.LookUpEdit lookupCompany;
        private DevExpress.XtraLayout.LayoutControlItem layoutCompany;
        private DevExpress.XtraEditors.SpinEdit txtLightweight;
        private DevExpress.XtraEditors.SpinEdit txtDeadweightTropic;
        private DevExpress.XtraEditors.SpinEdit txtDeadweightSummer;
        private DevExpress.XtraEditors.SpinEdit txtDeadWeightWinter;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDeadWeightTropic;
        private DevExpress.XtraLayout.LayoutControlItem layoutLightweight;
        private DevExpress.XtraEditors.SpinEdit txtGrainCapacity;
        private DevExpress.XtraEditors.SpinEdit txtDraftTropic;
        private DevExpress.XtraEditors.SpinEdit txtDraftSummer;
        private DevExpress.XtraEditors.SpinEdit txtDraftWinter;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDraftWinter;
        private DevExpress.XtraLayout.LayoutControlItem layoutDraftSummer;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDraftTropic;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemGrainCapacity;
        private DevExpress.XtraEditors.TextEdit txtClassification;
        private DevExpress.XtraEditors.SpinEdit txtBeam;
        private DevExpress.XtraEditors.SpinEdit txtDieselLadenPort;
        private DevExpress.XtraEditors.SpinEdit txtOilLadenSea;
        private DevExpress.XtraEditors.SpinEdit txtDieselBallastSea;
        private DevExpress.XtraEditors.SpinEdit txtFuelOilPort;
        private DevExpress.XtraEditors.SpinEdit txtLoa;
        private DevExpress.XtraEditors.SpinEdit txtFuelLadenSea;
        private DevExpress.XtraEditors.SpinEdit txtFuelBallastSea;
        private DevExpress.XtraEditors.SpinEdit txtTPC;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTpc;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemFuelOilBallastSea;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemFuleOilLadenAtSea;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemFuelOilAtPort;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDieselOilLadenAtSea;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDieselOilBallastAtSea;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemLoa;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBeam;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDieselOilLadenAtPort;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemClassification;
        private DevExpress.XtraEditors.TextEdit txtPiClub;
        private DevExpress.XtraEditors.TextEdit txtYard;
        private DevExpress.XtraEditors.TextEdit txtFlag;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemFlag;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemYard;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPiClub;
        private DevExpress.XtraEditors.DateEdit dtpSaleDate;
        private DevExpress.XtraEditors.SpinEdit txtUsefulLife;
        private DevExpress.XtraEditors.SpinEdit txtCommercialFee;
        private DevExpress.XtraEditors.SpinEdit txtManagementFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemManagementFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCommercialFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemUsefulLife;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemSaleDate;
        private DevExpress.XtraEditors.SpinEdit txtSaleAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemSaleAmount;
        private DevExpress.XtraEditors.TextEdit txtImoNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemIMONumber;
        private DevExpress.XtraEditors.SpinEdit txtBenchAgeFrequency;
        private DevExpress.XtraEditors.SpinEdit txtBenchAgePoints;
        private DevExpress.XtraEditors.SpinEdit txtBenchInitial;
        private DevExpress.XtraEditors.DateEdit dtpDateBuilt;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDateBuilt;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBenchInitial;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBenchAgePoints;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBenchAgeFrequency;
        private DevExpress.XtraLayout.LayoutControlGroup lcgVesselInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDeadWeightWinter;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDeadWeightSummer;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit txtMarketPrice;
        private DevExpress.XtraEditors.SpinEdit txtAcquisitionPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAcquisitionPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemMarketPrice;
        private DevExpress.XtraEditors.SpinEdit txtBenchmark;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBenchmark;
        private DevExpress.XtraEditors.LookUpEdit lookUpMasterOfficer;
        private DevExpress.XtraEditors.LookUpEdit lookupContact;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemContact;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemMasterOfficer;
        private DevExpress.XtraEditors.LookUpEdit lookUpChiefOfficer;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemChiefOfficer;
        private DevExpress.XtraEditors.LookUpEdit lookUpTechnicalManager;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTechnicalManager;
        private DevExpress.XtraGrid.GridControl grdExpenses;
        private DevExpress.XtraGrid.Views.Grid.GridView grvExpenses;
        private DevExpress.XtraGrid.Columns.GridColumn gcAge;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit txtAge;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit txtOperationExpenses;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit txtDryDockExpenses;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit txtDryDockOffHire;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit txtDepreciationProfile;
        private DevExpress.XtraLayout.LayoutControlGroup lcgExpenses;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemExpenses;
        private DevExpress.XtraGrid.Columns.GridColumn gcOperationalExpenses;
        private DevExpress.XtraGrid.Columns.GridColumn gcDryDockExpenses;
        private DevExpress.XtraGrid.Columns.GridColumn gcDryDockOffHire;
        private DevExpress.XtraGrid.Columns.GridColumn gcDepreciationProfile;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SpinEdit txtEquity;
        private DevExpress.XtraLayout.LayoutControlItem layoutEquity;
        private DevExpress.XtraEditors.DateEdit dtpDeliveryDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutDeliveryDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SpinEdit txtDeadWeight;
        private DevExpress.XtraEditors.DateEdit dtpMOADate;
        private DevExpress.XtraLayout.LayoutControlItem layoutMOADate;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDeadWeight;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}