﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class VesselPoolAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private VesselPool _vesselPool;

        #endregion

        #region Constructors

        public VesselPoolAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public VesselPoolAEVForm(VesselPool vesselPool, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _vesselPool = vesselPool;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            layoutGroupHistory.Visibility = LayoutVisibility.Never;
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            if(_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
            {
                foreach (var vessel in _vesselPool.CurrentVessels)
                {
                    int found = lstVessels.FindItem(0, true, a =>
                                                     {
                                                         a.IsFound =
                                                             (((Vessel) a.ItemValue).Id ==
                                                              vessel.Id);
                                                     });
                    if(found == -1)
                        lstVessels.Items.Add(new CheckedListBoxItem(vessel, vessel.Name));
                }
            }

            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;

                gridHistory.BeginUpdate();

                int gridViewColumnindex = 0;

                gridTradersMainView.Columns.Clear();

                gridTradersMainView.Columns.Add(new GridColumn
                {
                    Caption = Strings.Period_From,
                    FieldName = "PeriodFrom",
                    VisibleIndex = gridViewColumnindex++
                });
                gridTradersMainView.Columns.Add(new GridColumn
                {
                    Caption = Strings.Period_To,
                    FieldName = "PeriodTo",
                    VisibleIndex = gridViewColumnindex++
                });
                gridTradersMainView.Columns.Add(new GridColumn
                {
                    Caption = Strings.Vessels,
                    FieldName = "VesselsString",
                    VisibleIndex = gridViewColumnindex
                });

                gridHistory.EndUpdate();

                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                foreach (CheckedListBoxItem item in lstVessels.Items)
                {
                    item.Enabled = false;
                }
                gridTradersMainView.OptionsBehavior.Editable = false;
            }
            else
            {
                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void InitializeData()
        {
            BeginAEVVesselPoolInitializationData();
        }

        private void LoadData()
        {
            lblId.Text = _vesselPool.Id.ToString();
            txtName.Text = _vesselPool.Name;
            cmbStatus.SelectedItem = _vesselPool.Status;

            foreach (Vessel vessel in _vesselPool.CurrentVessels)
            {
                lstVessels.SetItemChecked((lstVessels.FindItem(0, true, a =>
                                                                            {
                                                                                a.IsFound =
                                                                                    (((Vessel) a.ItemValue).Id ==
                                                                                     vessel.Id);
                                                                            })), true);
            }

            txtCreationUser.Text = _vesselPool.Cruser;
            dtpCreationDate.DateTime = _vesselPool.Crd;
            txtUpdateUser.Text = _vesselPool.Chuser;
            dtpUpdateDate.DateTime = _vesselPool.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void VesselPoolAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (lstVessels.CheckedItems.Count == 0)
            {
                XtraMessageBox.Show(this, Strings.At_least_1_Vessel_should_be_selected_for_the_Pool_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _vesselPool = new VesselPool();
            }

            _vesselPool.Name = txtName.Text.Trim();
            _vesselPool.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            var vessels = new List<Vessel>();
            foreach (int checkedIndex in lstVessels.CheckedIndices)
            {
                vessels.Add((Vessel) lstVessels.GetItemValue(checkedIndex));
            }
            
            _vesselPool.CurrentVessels = vessels;

            BeginAEVVesselPool(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnHistory_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnHistory.Enabled = false;
            layoutGroupHistory.Visibility = LayoutVisibility.Always;

            BeginAEVVesselPoolHistory();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void VesselPoolAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void VesselPoolAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVVesselPoolInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVVesselPoolInitializationData(
                    EndAEVVesselPoolInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVVesselPoolInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVVesselPoolInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Vessel> vessels;

            try
            {
                result = SessionRegistry.Client.EndAEVVesselPoolInitializationData(out vessels, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                foreach (Vessel vessel in vessels)
                {
                    lstVessels.Items.Add(new CheckedListBoxItem(vessel, vessel.Name));
                }

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                if (_formActionType == FormActionTypeEnum.View)
                {
                    foreach (CheckedListBoxItem item in lstVessels.Items)
                    {
                        item.Enabled = false;
                    }
                }

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVVesselPool(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _vesselPool, EndAEVVesselPool, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVVesselPool(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVVesselPool;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        private void BeginAEVVesselPoolHistory()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVVesselPoolHistory(_vesselPool.Id, EndAEVVesselPoolHistory, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.WaitCursor;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVVesselPoolHistory(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVVesselPoolHistory;
                Invoke(action, ar);
                return;
            }
            List<VesselPoolInfo> vesselPoolInfos;
            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEVVesselPoolHistory(out vesselPoolInfos, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridHistory.DataSource = vesselPoolInfos.OrderByDescending(a=> a.PeriodFrom).ToList();
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public VesselPool VesselPool
        {
            get { return _vesselPool; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}