﻿namespace Exis.WinClient.Controls
{
    partial class BalticExchangeDataInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BalticExchangeDataInputForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageImportData = new DevExpress.XtraTab.XtraTabPage();
            this.layCtlImportData = new DevExpress.XtraLayout.LayoutControl();
            this.lblLastDownloadDate = new DevExpress.XtraEditors.LabelControl();
            this.lblOr = new DevExpress.XtraEditors.LabelControl();
            this.btnSelectBalticFile = new DevExpress.XtraEditors.ButtonEdit();
            this.btnImporBalticValues = new DevExpress.XtraEditors.SimpleButton();
            this.imageList16x16 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnDownloadFromFTPServer = new DevExpress.XtraEditors.SimpleButton();
            this.lblSelectFromFTP = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgIndexBalticValues = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemLabelOr = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBtnSelectBalticFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layVtlItemLblSelectFromFTP = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemLblLastDownLoadDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemBtnImportFromFTP = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.xtraTabPageImportNasdaq = new DevExpress.XtraTab.XtraTabPage();
            this.grControlNasdaq2 = new DevExpress.XtraEditors.GroupControl();
            this.btnImportNasdaqFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnNasdaqChooseFile = new DevExpress.XtraEditors.ButtonEdit();
            this.grControlNasdaq1 = new DevExpress.XtraEditors.GroupControl();
            this.grpImportNasdaqLblDate = new DevExpress.XtraEditors.LabelControl();
            this.grpImportNasdaqLblDatelabel = new DevExpress.XtraEditors.LabelControl();
            this.btnImportNasdaqFtp = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageImportEEX = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageValidationErrors = new DevExpress.XtraTab.XtraTabPage();
            this.layCtlErrors = new DevExpress.XtraLayout.LayoutControl();
            this.grdErrors = new DevExpress.XtraGrid.GridControl();
            this.grvErrors = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcErrorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpsiErrorType = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gcError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemGridErrors = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemTabControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblLastImportDate = new DevExpress.XtraEditors.LabelControl();
            this.layCtlItemLblLastImportDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnSelectFFAExcelFile = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnSelectSpotExcelFile = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnImportFFAExcel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnImportSpotExcel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageImportData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlImportData)).BeginInit();
            this.layCtlImportData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectBalticFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgIndexBalticValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemLabelOr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnSelectBalticFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layVtlItemLblSelectFromFTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemLblLastDownLoadDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnImportFromFTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.xtraTabPageImportNasdaq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grControlNasdaq2)).BeginInit();
            this.grControlNasdaq2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNasdaqChooseFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grControlNasdaq1)).BeginInit();
            this.grControlNasdaq1.SuspendLayout();
            this.xtraTabPageValidationErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlErrors)).BeginInit();
            this.layCtlErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiErrorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemGridErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemTabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemLblLastImportDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectFFAExcelFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectSpotExcelFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.Controls.Add(this.xtraTabControl);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1690, 25, 250, 350);
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(817, 363);
            this.layoutRootControl.TabIndex = 0;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageImportData;
            this.xtraTabControl.Size = new System.Drawing.Size(813, 359);
            this.xtraTabControl.TabIndex = 16;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageImportData,
            this.xtraTabPageImportNasdaq,
            this.xtraTabPageImportEEX,
            this.xtraTabPageValidationErrors});
            // 
            // xtraTabPageImportData
            // 
            this.xtraTabPageImportData.Controls.Add(this.layCtlImportData);
            this.xtraTabPageImportData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPageImportData.Name = "xtraTabPageImportData";
            this.xtraTabPageImportData.Size = new System.Drawing.Size(807, 328);
            this.xtraTabPageImportData.Text = "Import Baltic Exchange Data";
            // 
            // layCtlImportData
            // 
            this.layCtlImportData.Controls.Add(this.btnImportSpotExcel);
            this.layCtlImportData.Controls.Add(this.btnImportFFAExcel);
            this.layCtlImportData.Controls.Add(this.btnSelectSpotExcelFile);
            this.layCtlImportData.Controls.Add(this.btnSelectFFAExcelFile);
            this.layCtlImportData.Controls.Add(this.labelControl1);
            this.layCtlImportData.Controls.Add(this.lblLastImportDate);
            this.layCtlImportData.Controls.Add(this.lblLastDownloadDate);
            this.layCtlImportData.Controls.Add(this.lblOr);
            this.layCtlImportData.Controls.Add(this.btnSelectBalticFile);
            this.layCtlImportData.Controls.Add(this.btnImporBalticValues);
            this.layCtlImportData.Controls.Add(this.btnDownloadFromFTPServer);
            this.layCtlImportData.Controls.Add(this.lblSelectFromFTP);
            this.layCtlImportData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layCtlImportData.Location = new System.Drawing.Point(0, 0);
            this.layCtlImportData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layCtlImportData.Name = "layCtlImportData";
            this.layCtlImportData.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(666, 214, 250, 350);
            this.layCtlImportData.Root = this.layoutControlGroup3;
            this.layCtlImportData.Size = new System.Drawing.Size(807, 328);
            this.layCtlImportData.TabIndex = 0;
            this.layCtlImportData.Text = "layoutControl1";
            // 
            // lblLastDownloadDate
            // 
            this.lblLastDownloadDate.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblLastDownloadDate.Location = new System.Drawing.Point(214, 12);
            this.lblLastDownloadDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblLastDownloadDate.Name = "lblLastDownloadDate";
            this.lblLastDownloadDate.Size = new System.Drawing.Size(173, 16);
            this.lblLastDownloadDate.StyleController = this.layCtlImportData;
            this.lblLastDownloadDate.TabIndex = 16;
            this.lblLastDownloadDate.Text = "1/1/2000";
            // 
            // lblOr
            // 
            this.lblOr.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblOr.Location = new System.Drawing.Point(12, 58);
            this.lblOr.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblOr.Name = "lblOr";
            this.lblOr.Size = new System.Drawing.Size(590, 43);
            this.lblOr.StyleController = this.layCtlImportData;
            this.lblOr.TabIndex = 14;
            this.lblOr.Text = "Or";
            // 
            // btnSelectBalticFile
            // 
            this.btnSelectBalticFile.Location = new System.Drawing.Point(352, 105);
            this.btnSelectBalticFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelectBalticFile.Name = "btnSelectBalticFile";
            this.btnSelectBalticFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnSelectBalticFile.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.btnSelectBalticFile.Properties.ReadOnly = true;
            this.btnSelectBalticFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSelectBalticFile.Size = new System.Drawing.Size(168, 22);
            this.btnSelectBalticFile.StyleController = this.layCtlImportData;
            this.btnSelectBalticFile.TabIndex = 11;
            this.btnSelectBalticFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnSelectFfaSpotFileButtonClick);
            this.btnSelectBalticFile.EditValueChanged += new System.EventHandler(this.BtnSelectFfaSpotFileEditValueChanged);
            // 
            // btnImporBalticValues
            // 
            this.btnImporBalticValues.ImageIndex = 0;
            this.btnImporBalticValues.ImageList = this.imageList16x16;
            this.btnImporBalticValues.Location = new System.Drawing.Point(524, 105);
            this.btnImporBalticValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImporBalticValues.MaximumSize = new System.Drawing.Size(91, 27);
            this.btnImporBalticValues.MinimumSize = new System.Drawing.Size(91, 27);
            this.btnImporBalticValues.Name = "btnImporBalticValues";
            this.btnImporBalticValues.Size = new System.Drawing.Size(91, 27);
            this.btnImporBalticValues.StyleController = this.layCtlImportData;
            this.btnImporBalticValues.TabIndex = 13;
            this.btnImporBalticValues.Text = "Import";
            this.btnImporBalticValues.Click += new System.EventHandler(this.BtnImportFfaSpotValuesClick);
            // 
            // imageList16x16
            // 
            this.imageList16x16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16x16.ImageStream")));
            this.imageList16x16.Images.SetKeyName(0, "import.ico");
            this.imageList16x16.Images.SetKeyName(1, "error2.ico");
            this.imageList16x16.Images.SetKeyName(2, "warning.ico");
            this.imageList16x16.Images.SetKeyName(3, "ok.ico");
            // 
            // btnDownloadFromFTPServer
            // 
            this.btnDownloadFromFTPServer.ImageIndex = 0;
            this.btnDownloadFromFTPServer.ImageList = this.imageList16x16;
            this.btnDownloadFromFTPServer.Location = new System.Drawing.Point(226, 32);
            this.btnDownloadFromFTPServer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDownloadFromFTPServer.MaximumSize = new System.Drawing.Size(105, 27);
            this.btnDownloadFromFTPServer.MinimumSize = new System.Drawing.Size(105, 27);
            this.btnDownloadFromFTPServer.Name = "btnDownloadFromFTPServer";
            this.btnDownloadFromFTPServer.Size = new System.Drawing.Size(105, 27);
            this.btnDownloadFromFTPServer.StyleController = this.layCtlImportData;
            this.btnDownloadFromFTPServer.TabIndex = 15;
            this.btnDownloadFromFTPServer.Text = "Download";
            this.btnDownloadFromFTPServer.ToolTip = "Dowload files from ftp";
            this.btnDownloadFromFTPServer.Click += new System.EventHandler(this.BtnDownloadFromFtpServerClick);
            // 
            // lblSelectFromFTP
            // 
            this.lblSelectFromFTP.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblSelectFromFTP.Location = new System.Drawing.Point(12, 32);
            this.lblSelectFromFTP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblSelectFromFTP.Name = "lblSelectFromFTP";
            this.lblSelectFromFTP.Size = new System.Drawing.Size(210, 22);
            this.lblSelectFromFTP.StyleController = this.layCtlImportData;
            this.lblSelectFromFTP.TabIndex = 15;
            this.lblSelectFromFTP.Text = "Download and import from FTP:";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgIndexBalticValues,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(807, 328);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // lcgIndexBalticValues
            // 
            this.lcgIndexBalticValues.CustomizationFormText = "Index Baltic Values";
            this.lcgIndexBalticValues.GroupBordersVisible = false;
            this.lcgIndexBalticValues.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemLabelOr,
            this.layoutBtnSelectBalticFile,
            this.layoutControlGroup1,
            this.layoutControlItem1});
            this.lcgIndexBalticValues.Location = new System.Drawing.Point(0, 0);
            this.lcgIndexBalticValues.Name = "lcgIndexBalticValues";
            this.lcgIndexBalticValues.Size = new System.Drawing.Size(787, 119);
            this.lcgIndexBalticValues.Text = "Index Baltic Values";
            // 
            // layCtlItemLabelOr
            // 
            this.layCtlItemLabelOr.Control = this.lblOr;
            this.layCtlItemLabelOr.CustomizationFormText = "layCtlItemLabelOr";
            this.layCtlItemLabelOr.Location = new System.Drawing.Point(0, 46);
            this.layCtlItemLabelOr.MaxSize = new System.Drawing.Size(594, 47);
            this.layCtlItemLabelOr.MinSize = new System.Drawing.Size(594, 47);
            this.layCtlItemLabelOr.Name = "layCtlItemLabelOr";
            this.layCtlItemLabelOr.Size = new System.Drawing.Size(787, 47);
            this.layCtlItemLabelOr.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemLabelOr.Text = "layCtlItemLabelOr";
            this.layCtlItemLabelOr.TextSize = new System.Drawing.Size(0, 0);
            this.layCtlItemLabelOr.TextToControlDistance = 0;
            this.layCtlItemLabelOr.TextVisible = false;
            // 
            // layoutBtnSelectBalticFile
            // 
            this.layoutBtnSelectBalticFile.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.layoutBtnSelectBalticFile.AppearanceItemCaption.Options.UseFont = true;
            this.layoutBtnSelectBalticFile.Control = this.btnSelectBalticFile;
            this.layoutBtnSelectBalticFile.CustomizationFormText = "Select a file:";
            this.layoutBtnSelectBalticFile.Location = new System.Drawing.Point(0, 93);
            this.layoutBtnSelectBalticFile.MaxSize = new System.Drawing.Size(512, 26);
            this.layoutBtnSelectBalticFile.MinSize = new System.Drawing.Size(512, 26);
            this.layoutBtnSelectBalticFile.Name = "layoutBtnSelectBalticFile";
            this.layoutBtnSelectBalticFile.Size = new System.Drawing.Size(512, 26);
            this.layoutBtnSelectBalticFile.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutBtnSelectBalticFile.Text = "Import from a specific XML file:";
            this.layoutBtnSelectBalticFile.TextSize = new System.Drawing.Size(337, 17);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layVtlItemLblSelectFromFTP,
            this.layCtlItemBtnImportFromFTP,
            this.layCtlItemLblLastDownLoadDate,
            this.layCtlItemLblLastImportDate,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(787, 46);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            // 
            // layVtlItemLblSelectFromFTP
            // 
            this.layVtlItemLblSelectFromFTP.Control = this.lblSelectFromFTP;
            this.layVtlItemLblSelectFromFTP.CustomizationFormText = "layVtlItemLblSelectFromFTP";
            this.layVtlItemLblSelectFromFTP.Location = new System.Drawing.Point(0, 20);
            this.layVtlItemLblSelectFromFTP.MaxSize = new System.Drawing.Size(214, 26);
            this.layVtlItemLblSelectFromFTP.MinSize = new System.Drawing.Size(214, 26);
            this.layVtlItemLblSelectFromFTP.Name = "layVtlItemLblSelectFromFTP";
            this.layVtlItemLblSelectFromFTP.Size = new System.Drawing.Size(214, 26);
            this.layVtlItemLblSelectFromFTP.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layVtlItemLblSelectFromFTP.Text = "layVtlItemLblSelectFromFTP";
            this.layVtlItemLblSelectFromFTP.TextSize = new System.Drawing.Size(0, 0);
            this.layVtlItemLblSelectFromFTP.TextToControlDistance = 0;
            this.layVtlItemLblSelectFromFTP.TextVisible = false;
            // 
            // layCtlItemLblLastDownLoadDate
            // 
            this.layCtlItemLblLastDownLoadDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layCtlItemLblLastDownLoadDate.AppearanceItemCaption.Options.UseFont = true;
            this.layCtlItemLblLastDownLoadDate.Control = this.lblLastDownloadDate;
            this.layCtlItemLblLastDownLoadDate.CustomizationFormText = "Last Download Date from Baltic:";
            this.layCtlItemLblLastDownLoadDate.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemLblLastDownLoadDate.MaxSize = new System.Drawing.Size(0, 20);
            this.layCtlItemLblLastDownLoadDate.MinSize = new System.Drawing.Size(171, 20);
            this.layCtlItemLblLastDownLoadDate.Name = "layCtlItemLblLastDownLoadDate";
            this.layCtlItemLblLastDownLoadDate.Size = new System.Drawing.Size(379, 20);
            this.layCtlItemLblLastDownLoadDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemLblLastDownLoadDate.Text = "Last Download Date from Baltic:";
            this.layCtlItemLblLastDownLoadDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layCtlItemLblLastDownLoadDate.TextSize = new System.Drawing.Size(197, 17);
            this.layCtlItemLblLastDownLoadDate.TextToControlDistance = 5;
            // 
            // layCtlItemBtnImportFromFTP
            // 
            this.layCtlItemBtnImportFromFTP.Control = this.btnDownloadFromFTPServer;
            this.layCtlItemBtnImportFromFTP.CustomizationFormText = "layCtlItemBtnImportFromFTP";
            this.layCtlItemBtnImportFromFTP.Location = new System.Drawing.Point(214, 20);
            this.layCtlItemBtnImportFromFTP.MaxSize = new System.Drawing.Size(380, 26);
            this.layCtlItemBtnImportFromFTP.MinSize = new System.Drawing.Size(380, 26);
            this.layCtlItemBtnImportFromFTP.Name = "layCtlItemBtnImportFromFTP";
            this.layCtlItemBtnImportFromFTP.Size = new System.Drawing.Size(573, 26);
            this.layCtlItemBtnImportFromFTP.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemBtnImportFromFTP.Text = "layCtlItemBtnImportFromFTP";
            this.layCtlItemBtnImportFromFTP.TextSize = new System.Drawing.Size(0, 0);
            this.layCtlItemBtnImportFromFTP.TextToControlDistance = 0;
            this.layCtlItemBtnImportFromFTP.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnImporBalticValues;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(512, 93);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(275, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 224);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(787, 84);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // xtraTabPageImportNasdaq
            // 
            this.xtraTabPageImportNasdaq.Controls.Add(this.grControlNasdaq2);
            this.xtraTabPageImportNasdaq.Controls.Add(this.grControlNasdaq1);
            this.xtraTabPageImportNasdaq.Name = "xtraTabPageImportNasdaq";
            this.xtraTabPageImportNasdaq.Size = new System.Drawing.Size(807, 328);
            this.xtraTabPageImportNasdaq.Text = "Import Nasdaq Data";
            // 
            // grControlNasdaq2
            // 
            this.grControlNasdaq2.Controls.Add(this.btnImportNasdaqFile);
            this.grControlNasdaq2.Controls.Add(this.btnNasdaqChooseFile);
            this.grControlNasdaq2.Location = new System.Drawing.Point(9, 95);
            this.grControlNasdaq2.Name = "grControlNasdaq2";
            this.grControlNasdaq2.Size = new System.Drawing.Size(783, 100);
            this.grControlNasdaq2.TabIndex = 1;
            this.grControlNasdaq2.Text = "Import from file";
            // 
            // btnImportNasdaqFile
            // 
            this.btnImportNasdaqFile.Image = ((System.Drawing.Image)(resources.GetObject("btnImportNasdaqFile.Image")));
            this.btnImportNasdaqFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnImportNasdaqFile.Location = new System.Drawing.Point(365, 42);
            this.btnImportNasdaqFile.Name = "btnImportNasdaqFile";
            this.btnImportNasdaqFile.Size = new System.Drawing.Size(139, 32);
            this.btnImportNasdaqFile.TabIndex = 13;
            this.btnImportNasdaqFile.Text = "Import File";
            // 
            // btnNasdaqChooseFile
            // 
            this.btnNasdaqChooseFile.Location = new System.Drawing.Point(20, 47);
            this.btnNasdaqChooseFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNasdaqChooseFile.Name = "btnNasdaqChooseFile";
            this.btnNasdaqChooseFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnNasdaqChooseFile.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.btnNasdaqChooseFile.Properties.ReadOnly = true;
            this.btnNasdaqChooseFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnNasdaqChooseFile.Size = new System.Drawing.Size(324, 22);
            this.btnNasdaqChooseFile.StyleController = this.layCtlImportData;
            this.btnNasdaqChooseFile.TabIndex = 12;
            this.btnNasdaqChooseFile.EditValueChanged += new System.EventHandler(this.btnNasdaqChooseFile_EditValueChanged);
            this.btnNasdaqChooseFile.Click += new System.EventHandler(this.btnNasdaqChooseFile_Click);
            // 
            // grControlNasdaq1
            // 
            this.grControlNasdaq1.Controls.Add(this.grpImportNasdaqLblDate);
            this.grControlNasdaq1.Controls.Add(this.grpImportNasdaqLblDatelabel);
            this.grControlNasdaq1.Controls.Add(this.btnImportNasdaqFtp);
            this.grControlNasdaq1.Location = new System.Drawing.Point(9, 12);
            this.grControlNasdaq1.Name = "grControlNasdaq1";
            this.grControlNasdaq1.Size = new System.Drawing.Size(783, 77);
            this.grControlNasdaq1.TabIndex = 0;
            this.grControlNasdaq1.Text = "Download from FTP and Import";
            // 
            // grpImportNasdaqLblDate
            // 
            this.grpImportNasdaqLblDate.Location = new System.Drawing.Point(150, 40);
            this.grpImportNasdaqLblDate.Name = "grpImportNasdaqLblDate";
            this.grpImportNasdaqLblDate.Size = new System.Drawing.Size(66, 16);
            this.grpImportNasdaqLblDate.StyleController = this.layCtlImportData;
            this.grpImportNasdaqLblDate.TabIndex = 2;
            this.grpImportNasdaqLblDate.Text = "01/01/2017";
            // 
            // grpImportNasdaqLblDatelabel
            // 
            this.grpImportNasdaqLblDatelabel.Location = new System.Drawing.Point(11, 39);
            this.grpImportNasdaqLblDatelabel.Name = "grpImportNasdaqLblDatelabel";
            this.grpImportNasdaqLblDatelabel.Size = new System.Drawing.Size(122, 16);
            this.grpImportNasdaqLblDatelabel.StyleController = this.layCtlImportData;
            this.grpImportNasdaqLblDatelabel.TabIndex = 1;
            this.grpImportNasdaqLblDatelabel.Text = "Last Download Date: ";
            // 
            // btnImportNasdaqFtp
            // 
            this.btnImportNasdaqFtp.Image = ((System.Drawing.Image)(resources.GetObject("btnImportNasdaqFtp.Image")));
            this.btnImportNasdaqFtp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnImportNasdaqFtp.Location = new System.Drawing.Point(261, 32);
            this.btnImportNasdaqFtp.Name = "btnImportNasdaqFtp";
            this.btnImportNasdaqFtp.Size = new System.Drawing.Size(139, 32);
            this.btnImportNasdaqFtp.TabIndex = 0;
            this.btnImportNasdaqFtp.Text = "FTP Import";
            this.btnImportNasdaqFtp.Click += new System.EventHandler(this.btnImportNasdaqFtp_Click);
            // 
            // xtraTabPageImportEEX
            // 
            this.xtraTabPageImportEEX.Name = "xtraTabPageImportEEX";
            this.xtraTabPageImportEEX.Size = new System.Drawing.Size(807, 328);
            this.xtraTabPageImportEEX.Text = "Import EEX Data";
            // 
            // xtraTabPageValidationErrors
            // 
            this.xtraTabPageValidationErrors.Controls.Add(this.layCtlErrors);
            this.xtraTabPageValidationErrors.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPageValidationErrors.Name = "xtraTabPageValidationErrors";
            this.xtraTabPageValidationErrors.Size = new System.Drawing.Size(807, 328);
            this.xtraTabPageValidationErrors.Text = "Log";
            // 
            // layCtlErrors
            // 
            this.layCtlErrors.Controls.Add(this.grdErrors);
            this.layCtlErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layCtlErrors.Location = new System.Drawing.Point(0, 0);
            this.layCtlErrors.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layCtlErrors.Name = "layCtlErrors";
            this.layCtlErrors.Root = this.layoutControlGroup4;
            this.layCtlErrors.Size = new System.Drawing.Size(807, 328);
            this.layCtlErrors.TabIndex = 0;
            this.layCtlErrors.Text = "layoutControl1";
            // 
            // grdErrors
            // 
            this.grdErrors.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grdErrors.Location = new System.Drawing.Point(12, 12);
            this.grdErrors.MainView = this.grvErrors;
            this.grdErrors.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grdErrors.Name = "grdErrors";
            this.grdErrors.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpsiErrorType});
            this.grdErrors.Size = new System.Drawing.Size(783, 304);
            this.grdErrors.TabIndex = 4;
            this.grdErrors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvErrors});
            // 
            // grvErrors
            // 
            this.grvErrors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcErrorType,
            this.gcError});
            this.grvErrors.GridControl = this.grdErrors;
            this.grvErrors.Name = "grvErrors";
            this.grvErrors.OptionsBehavior.Editable = false;
            this.grvErrors.OptionsView.ShowGroupPanel = false;
            this.grvErrors.OptionsView.ShowIndicator = false;
            // 
            // gcErrorType
            // 
            this.gcErrorType.Caption = "Severity";
            this.gcErrorType.ColumnEdit = this.rpsiErrorType;
            this.gcErrorType.FieldName = "ErrorType";
            this.gcErrorType.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.gcErrorType.Name = "gcErrorType";
            this.gcErrorType.Visible = true;
            this.gcErrorType.VisibleIndex = 0;
            this.gcErrorType.Width = 30;
            // 
            // rpsiErrorType
            // 
            this.rpsiErrorType.AutoHeight = false;
            this.rpsiErrorType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpsiErrorType.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rpsiErrorType.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", "0", 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", "1", 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", "2", 3)});
            this.rpsiErrorType.Name = "rpsiErrorType";
            this.rpsiErrorType.SmallImages = this.imageList16x16;
            // 
            // gcError
            // 
            this.gcError.Caption = "Description";
            this.gcError.FieldName = "ErrorDescription";
            this.gcError.Name = "gcError";
            this.gcError.Visible = true;
            this.gcError.VisibleIndex = 1;
            this.gcError.Width = 506;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemGridErrors});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(807, 328);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layCtlItemGridErrors
            // 
            this.layCtlItemGridErrors.Control = this.grdErrors;
            this.layCtlItemGridErrors.CustomizationFormText = "layCt6lItemGridErrors";
            this.layCtlItemGridErrors.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemGridErrors.Name = "layCtlItemGridErrors";
            this.layCtlItemGridErrors.Size = new System.Drawing.Size(787, 308);
            this.layCtlItemGridErrors.Text = "layCtlItemGridErrors";
            this.layCtlItemGridErrors.TextSize = new System.Drawing.Size(0, 0);
            this.layCtlItemGridErrors.TextToControlDistance = 0;
            this.layCtlItemGridErrors.TextVisible = false;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutControlGroup1";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemTabControl});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(817, 363);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layCtlItemTabControl
            // 
            this.layCtlItemTabControl.Control = this.xtraTabControl;
            this.layCtlItemTabControl.CustomizationFormText = "layCtlItemTabControl";
            this.layCtlItemTabControl.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemTabControl.Name = "layCtlItemTabControl";
            this.layCtlItemTabControl.Size = new System.Drawing.Size(817, 363);
            this.layCtlItemTabControl.Text = "layCtlItemTabControl";
            this.layCtlItemTabControl.TextSize = new System.Drawing.Size(0, 0);
            this.layCtlItemTabControl.TextToControlDistance = 0;
            this.layCtlItemTabControl.TextVisible = false;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "exit.ico");
            this.imageList24.Images.SetKeyName(1, "import.ico");
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.FloatLocation = new System.Drawing.Point(2184, 490);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 0;
            this.btnClose.LargeImageIndex = 0;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCloseClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(817, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 363);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(817, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 363);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(817, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 363);
            // 
            // lblLastImportDate
            // 
            this.lblLastImportDate.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblLastImportDate.Location = new System.Drawing.Point(622, 12);
            this.lblLastImportDate.Name = "lblLastImportDate";
            this.lblLastImportDate.Size = new System.Drawing.Size(144, 16);
            this.lblLastImportDate.StyleController = this.layCtlImportData;
            this.lblLastImportDate.TabIndex = 17;
            this.lblLastImportDate.Text = "1/1/2000";
            // 
            // layCtlItemLblLastImportDate
            // 
            this.layCtlItemLblLastImportDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layCtlItemLblLastImportDate.AppearanceItemCaption.Options.UseFont = true;
            this.layCtlItemLblLastImportDate.Control = this.lblLastImportDate;
            this.layCtlItemLblLastImportDate.CustomizationFormText = "Last Import Data from either source:";
            this.layCtlItemLblLastImportDate.Location = new System.Drawing.Point(379, 0);
            this.layCtlItemLblLastImportDate.MaxSize = new System.Drawing.Size(0, 20);
            this.layCtlItemLblLastImportDate.MinSize = new System.Drawing.Size(171, 20);
            this.layCtlItemLblLastImportDate.Name = "layCtlItemLblLastImportDate";
            this.layCtlItemLblLastImportDate.Size = new System.Drawing.Size(379, 20);
            this.layCtlItemLblLastImportDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemLblLastImportDate.Text = "Last Import Data from either source:";
            this.layCtlItemLblLastImportDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layCtlItemLblLastImportDate.TextSize = new System.Drawing.Size(226, 17);
            this.layCtlItemLblLastImportDate.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(758, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(29, 20);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelControl1.Location = new System.Drawing.Point(12, 131);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(586, 39);
            this.labelControl1.StyleController = this.layCtlImportData;
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "Or";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(590, 43);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(590, 43);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(787, 43);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // btnSelectFFAExcelFile
            // 
            this.btnSelectFFAExcelFile.Location = new System.Drawing.Point(352, 174);
            this.btnSelectFFAExcelFile.MenuManager = this.barManager;
            this.btnSelectFFAExcelFile.Name = "btnSelectFFAExcelFile";
            this.btnSelectFFAExcelFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Exis.WinClient.Properties.Resources.browseFileSystem16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnSelectFFAExcelFile.Properties.ReadOnly = true;
            this.btnSelectFFAExcelFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSelectFFAExcelFile.Size = new System.Drawing.Size(168, 22);
            this.btnSelectFFAExcelFile.StyleController = this.layCtlImportData;
            this.btnSelectFFAExcelFile.TabIndex = 18;
            this.btnSelectFFAExcelFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnSelectFfaExcelFileButtonClick);
            this.btnSelectFFAExcelFile.EditValueChanged += new System.EventHandler(this.BtnSelectFfaExcelFileEditValueChanged);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.btnSelectFFAExcelFile;
            this.layoutControlItem3.CustomizationFormText = "Import FFA/BOA values from a specific Excel file:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(512, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(512, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(512, 31);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Import FFA/BOA values from a specific Excel file:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(337, 17);
            // 
            // btnSelectSpotExcelFile
            // 
            this.btnSelectSpotExcelFile.Location = new System.Drawing.Point(352, 205);
            this.btnSelectSpotExcelFile.MenuManager = this.barManager;
            this.btnSelectSpotExcelFile.Name = "btnSelectSpotExcelFile";
            this.btnSelectSpotExcelFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Exis.WinClient.Properties.Resources.browseFileSystem16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnSelectSpotExcelFile.Properties.ReadOnly = true;
            this.btnSelectSpotExcelFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSelectSpotExcelFile.Size = new System.Drawing.Size(168, 22);
            this.btnSelectSpotExcelFile.StyleController = this.layCtlImportData;
            this.btnSelectSpotExcelFile.TabIndex = 19;
            this.btnSelectSpotExcelFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnSelectSpotExcelFileButtonClick);
            this.btnSelectSpotExcelFile.EditValueChanged += new System.EventHandler(this.BtnSelectSpotExcelFileEditValueChanged);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.btnSelectSpotExcelFile;
            this.layoutControlItem4.CustomizationFormText = "Import Spot values from a specific Excel file:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 193);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(512, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(512, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(512, 31);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Import Spot values from a specific Excel file:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(337, 17);
            // 
            // btnImportFFAExcel
            // 
            this.btnImportFFAExcel.ImageIndex = 0;
            this.btnImportFFAExcel.ImageList = this.imageList16x16;
            this.btnImportFFAExcel.Location = new System.Drawing.Point(524, 174);
            this.btnImportFFAExcel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportFFAExcel.MaximumSize = new System.Drawing.Size(91, 27);
            this.btnImportFFAExcel.MinimumSize = new System.Drawing.Size(91, 27);
            this.btnImportFFAExcel.Name = "btnImportFFAExcel";
            this.btnImportFFAExcel.Size = new System.Drawing.Size(91, 27);
            this.btnImportFFAExcel.StyleController = this.layCtlImportData;
            this.btnImportFFAExcel.TabIndex = 14;
            this.btnImportFFAExcel.Text = "Import";
            this.btnImportFFAExcel.Click += new System.EventHandler(this.BtnImportFFAExcelClick);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnImportFFAExcel;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(512, 162);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(275, 31);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // btnImportSpotExcel
            // 
            this.btnImportSpotExcel.ImageIndex = 0;
            this.btnImportSpotExcel.ImageList = this.imageList16x16;
            this.btnImportSpotExcel.Location = new System.Drawing.Point(524, 205);
            this.btnImportSpotExcel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportSpotExcel.MaximumSize = new System.Drawing.Size(91, 27);
            this.btnImportSpotExcel.MinimumSize = new System.Drawing.Size(91, 27);
            this.btnImportSpotExcel.Name = "btnImportSpotExcel";
            this.btnImportSpotExcel.Size = new System.Drawing.Size(91, 27);
            this.btnImportSpotExcel.StyleController = this.layCtlImportData;
            this.btnImportSpotExcel.TabIndex = 15;
            this.btnImportSpotExcel.Text = "Import";
            this.btnImportSpotExcel.Click += new System.EventHandler(this.BtnImportSpotExcelClick);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnImportSpotExcel;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(512, 193);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(275, 31);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // BalticExchangeDataInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 401);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BalticExchangeDataInputForm";
            this.Load += new System.EventHandler(this.BalticExchangeDataInputFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageImportData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layCtlImportData)).EndInit();
            this.layCtlImportData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectBalticFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16x16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgIndexBalticValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemLabelOr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnSelectBalticFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layVtlItemLblSelectFromFTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemLblLastDownLoadDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnImportFromFTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.xtraTabPageImportNasdaq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grControlNasdaq2)).EndInit();
            this.grControlNasdaq2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnNasdaqChooseFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grControlNasdaq1)).EndInit();
            this.grControlNasdaq1.ResumeLayout(false);
            this.grControlNasdaq1.PerformLayout();
            this.xtraTabPageValidationErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layCtlErrors)).EndInit();
            this.layCtlErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiErrorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemGridErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemTabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemLblLastImportDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectFFAExcelFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectSpotExcelFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.ButtonEdit btnSelectBalticFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private DevExpress.Utils.ImageCollection imageList16x16;
        private DevExpress.XtraEditors.SimpleButton btnImporBalticValues;
        private DevExpress.XtraEditors.SimpleButton btnDownloadFromFTPServer;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageImportData;
        private DevExpress.XtraLayout.LayoutControl layCtlImportData;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageValidationErrors;
        private DevExpress.XtraLayout.LayoutControl layCtlErrors;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemTabControl;
        private DevExpress.XtraGrid.GridControl grdErrors;
        private DevExpress.XtraGrid.Views.Grid.GridView grvErrors;
        private DevExpress.XtraGrid.Columns.GridColumn gcError;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemGridErrors;
        private DevExpress.XtraGrid.Columns.GridColumn gcErrorType;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rpsiErrorType;
        private DevExpress.XtraEditors.LabelControl lblSelectFromFTP;
        private DevExpress.XtraEditors.LabelControl lblOr;
        private DevExpress.XtraEditors.LabelControl lblLastDownloadDate;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageImportNasdaq;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageImportEEX;
        private DevExpress.XtraLayout.LayoutControlGroup lcgIndexBalticValues;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemLabelOr;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnSelectBalticFile;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layVtlItemLblSelectFromFTP;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemLblLastDownLoadDate;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemBtnImportFromFTP;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.GroupControl grControlNasdaq2;
        private DevExpress.XtraEditors.GroupControl grControlNasdaq1;
        private DevExpress.XtraEditors.SimpleButton btnImportNasdaqFtp;
        private DevExpress.XtraEditors.LabelControl grpImportNasdaqLblDate;
        private DevExpress.XtraEditors.LabelControl grpImportNasdaqLblDatelabel;
        private DevExpress.XtraEditors.SimpleButton btnImportNasdaqFile;
        private DevExpress.XtraEditors.ButtonEdit btnNasdaqChooseFile;
        private DevExpress.XtraEditors.LabelControl lblLastImportDate;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemLblLastImportDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ButtonEdit btnSelectSpotExcelFile;
        private DevExpress.XtraEditors.ButtonEdit btnSelectFFAExcelFile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnImportSpotExcel;
        private DevExpress.XtraEditors.SimpleButton btnImportFFAExcel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}