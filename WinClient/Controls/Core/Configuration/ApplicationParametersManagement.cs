﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.SpecialForms;
using Exis.WinClient.General;
using Exis.Domain;

namespace Exis.WinClient.Controls
{
    public partial class ApplicationParametersManagement : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public ApplicationParametersManagement()
        {
            InitializeComponent();
        }

        #endregion

        #region GUI Event Handlers

        private void ApplicationParametersManagementLoad(object sender, EventArgs e)
        {
            BeginApplicationParametersInitializationData();
        }

        private void ApplicationParametersManagementActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void ApplicationParametersManagementDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void BtnAddClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof(AppParameter));
        }

        private void BtnEditClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (grvAppParameters.GetFocusedRow() == null) return;

            var focusedAppParameter = (AppParameter) grvAppParameters.GetFocusedRow();
            if(focusedAppParameter.IsSystem) return;

            if (EditEntityEvent != null) EditEntityEvent((AppParameter)grvAppParameters.GetFocusedRow());
        }

        private void BtnRefreshClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }


        private void GrvAppParametersFocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvAppParameters.GetFocusedRow() == null) return;

            var focusedAppParameter = (AppParameter) grvAppParameters.GetFocusedRow();
            btnEdit.Enabled = !focusedAppParameter.IsSystem;
        }

        #endregion

        #region Service Calls

        private void BeginApplicationParametersInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginApplicationParametersInitializationData(EndApplicationParametersInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndApplicationParametersInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndApplicationParametersInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<AppParameter> appParameters;
            try
            {
                result = SessionRegistry.Client.EndApplicationParametersInitializationData(out appParameters, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                grdAppParameters.DataSource = appParameters;
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion
    }
}