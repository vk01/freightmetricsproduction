﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;

namespace Exis.WinClient.Controls
{
    public partial class InterestReferenceRatesManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public InterestReferenceRatesManagementForm()
        {
            InitializeComponent();

            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            grdInterestRates.BeginUpdate();

            int gridViewColumnindex = 0;

            grvInterestRates.Columns.Clear();

            grvInterestRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Type,
                FieldName = "Type",
                VisibleIndex = gridViewColumnindex++
            });
            grvInterestRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Currency,
                FieldName = "Currency",
                VisibleIndex = gridViewColumnindex++
            });
            grvInterestRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Period_Type,
                FieldName = "PeriodType",
                VisibleIndex = gridViewColumnindex++
            });
            grvInterestRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Date,
                FieldName = "Date",
                VisibleIndex = gridViewColumnindex++
            });
            grvInterestRates.Columns.Add(new GridColumn
            {
                Caption = "Rate",
                FieldName = "Rate",
                VisibleIndex = gridViewColumnindex++
            });

            //grvInterestRates.ClearGrouping();
            //grvInterestRates.Columns["Type"].GroupIndex = 0;
            //grvInterestRates.Columns["Currency"].GroupIndex = 1;
                
            grdInterestRates.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetInterestReferenceRates()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new InterestReferenceRate(), EndGetInterestReferenceRates, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetInterestReferenceRates(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetInterestReferenceRates;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                List<InterestReferenceRate> rates = entities.Cast<InterestReferenceRate>().ToList();
                grdInterestRates.DataSource = rates.OrderBy(a => a.Date).OrderBy(a => a.CurrencyId).ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginDeleteInterestReferenceRate()
        {
            var focusedReferenceRate =  (InterestReferenceRate)grvInterestRates.GetFocusedRow();

            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginDeleteInterestReferenceRate(focusedReferenceRate.Id, EndDeleteInterestReferenceRate, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndDeleteInterestReferenceRate(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndDeleteInterestReferenceRate;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndDeleteInterestReferenceRate(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                grvInterestRates.DeleteRow(grvInterestRates.FocusedRowHandle);

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region GUI Event Handlers

        private void InterestReferenceRatesManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetInterestReferenceRates();
        }

        private void BtnAddClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null)
                InsertEntityEvent(typeof(InterestReferenceRate));
        }

        private void BtnEditClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (grvInterestRates.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((InterestReferenceRate)grvInterestRates.GetFocusedRow());
        }

        private void BtnDeleteClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginDeleteInterestReferenceRate();
        }

        private void BtnRefreshClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetInterestReferenceRates();
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion
    }
}