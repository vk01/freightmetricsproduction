﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors.Controls;
using System.Collections.Generic;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.ViewInfo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;
using System.Data;
using System.ComponentModel;

namespace Exis.WinClient.Controls
{
    public partial class InstallmentAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private Loan _loan;
        
        #endregion

        #region Constructors

        public InstallmentAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public InstallmentAEVForm(Loan loan, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _loan = loan;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);
            cmbStatus.SelectedItem = ActivationStatusEnum.Active;

            cmbBorrower.Properties.DisplayMember = "Name";
            cmbBorrower.Properties.ValueMember = "Id";
            cmbBorrower.Properties.NullValuePrompt = Strings.Select_a_Borrower___;

            lookupObligor.Properties.DisplayMember = "Name";
            lookupObligor.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Obligor_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupObligor.Properties.Columns.Add(col);
            lookupObligor.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupObligor.Properties.SearchMode = SearchMode.AutoFilter;
            lookupObligor.Properties.NullText = "";
            lookupObligor.Properties.NullValuePrompt = Strings.Select_an_Obligor___;

            lookupGuarantor.Properties.DisplayMember = "Name";
            lookupGuarantor.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Gurantor_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupGuarantor.Properties.Columns.Add(col);
            lookupGuarantor.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupGuarantor.Properties.SearchMode = SearchMode.AutoFilter;
            lookupGuarantor.Properties.NullValuePrompt = Strings.Select_a_Guarantor___;
            lookupGuarantor.Properties.NullText = "";

            lookupCurrency.Properties.DisplayMember = "Name";
            lookupCurrency.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupCurrency.Properties.Columns.Add(col);
            lookupCurrency.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCurrency.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCurrency.Properties.NullValuePrompt = Strings.Select_a_Currency___;
            lookupCurrency.Properties.NullText = "";

            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            cmbPrincipalFrequency_1.SelectedItem = LoanRepaymentFrequencyEnum.Month;

            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            cmbInterestFrequency_1.SelectedItem = LoanRepaymentFrequencyEnum.Month;

            cmbInterestRateType_1.Properties.Items.Add(LoanInterestRateTypeEnum.Fixed);
            cmbInterestRateType_1.Properties.Items.Add(LoanInterestRateTypeEnum.Floating);
            cmbInterestRateType_1.SelectedItem = LoanInterestRateTypeEnum.Fixed;

            cmbPeriodType_1.Properties.Items.Add("");
            foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
            {
                cmbPeriodType_1.Properties.Items.Add(type);
            }

            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void LoadData()
        {
            lblId.Text = _loan.Id.ToString();
            txtCode.Text = _loan.ExternalCode;
            txtAdminFee.EditValue = _loan.AdminFee;
            txtAmount.EditValue = _loan.Amount;
            txtArrangeFee.EditValue = _loan.ArrangeFee;
            lookupCurrency.EditValue = _loan.CurrencyId;
            dtpSignDate.EditValue = _loan.SignDate;
            cmbStatus.SelectedItem = _loan.Status;
            lookupGuarantor.EditValue = _loan.GuarantorId;
            lookupObligor.EditValue = _loan.ObligorId;
            txtPrepayFee.EditValue = _loan.PrepayFee;
            txtVMC.EditValue = _loan.Vmc;
            chkRetentionAccount.EditValue = _loan.RetentionAccount;

            txtCode.Properties.ReadOnly = true;
            txtAdminFee.Properties.ReadOnly = true;
            txtAmount.Properties.ReadOnly = true;
            txtArrangeFee.Properties.ReadOnly = true;
            lookupCurrency.Properties.ReadOnly = true;
            dtpSignDate.Properties.ReadOnly = true;
            cmbStatus.Properties.ReadOnly = true;
            lookupGuarantor.Properties.ReadOnly = true;
            lookupObligor.Properties.ReadOnly = true;
            txtPrepayFee.Properties.ReadOnly = true;
            txtVMC.Properties.ReadOnly = true;
            chkRetentionAccount.Properties.ReadOnly = true;
            cmbBorrower.Properties.ReadOnly = true;

            string borrowersStr = String.Join(",", _loan.LoanBorrowers.Select(a => a.BorrowerId.ToString()));
            cmbBorrower.SetEditValue(borrowersStr);
            
            #region Loan Tranches

            lcgTranches.Clear();
            lcTranches.BeginUpdate();

            foreach(LoanTranche loanTranche in _loan.LoanTranches)
            {
                int counter = Convert.ToInt32(lcgTranches.Items.Count + 1);

                #region Controls

                // 
                // txtAmount
                // 
                var txtTranchAmount_new = new SpinEdit();
                txtTranchAmount_new.Name = "txtTranchAmount_" + counter;
                txtTranchAmount_new.Properties.EditMask = "D";
                txtTranchAmount_new.Properties.MinValue = 1;
                txtTranchAmount_new.Properties.MaxValue = 999999999999;
                txtTranchAmount_new.Properties.MaxLength = 12;
                txtTranchAmount_new.Value = 0;
                txtTranchAmount_new.Properties.ReadOnly = true;
                
                // 
                // txtBalloonAmount
                // 
                var txtBalloonAmount_new = new SpinEdit();
                txtBalloonAmount_new.Name = "txtBalloonAmount_" + counter;
                txtBalloonAmount_new.Properties.EditMask = "D";
                txtBalloonAmount_new.Properties.MinValue = 1;
                txtBalloonAmount_new.Properties.MaxValue = 999999999;
                txtBalloonAmount_new.Properties.MaxLength = 9;
                txtBalloonAmount_new.Value = 0;
                txtBalloonAmount_new.Properties.ReadOnly = true;

                //Principal Frequency
                var cmbPrincipalFrequency_new = new ComboBoxEdit();
                cmbPrincipalFrequency_new.Name = "cmbPrincipalFrequency_" + counter;
                cmbPrincipalFrequency_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
                cmbPrincipalFrequency_new.SelectedItem = LoanRepaymentFrequencyEnum.Month;
                cmbPrincipalFrequency_new.Properties.ReadOnly = true;

                //Interest Frequency
                var cmbInterestFrequency_new = new ComboBoxEdit();
                cmbInterestFrequency_new.Name = "cmbInterestFrequency_" + counter;
                cmbInterestFrequency_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
                cmbInterestFrequency_new.SelectedItem = LoanRepaymentFrequencyEnum.Month;
                cmbInterestFrequency_new.Properties.ReadOnly = true;

                // 
                // txtRepayPeriod
                // 
                var txtRepayPeriod_new = new SpinEdit();
                txtRepayPeriod_new.Name = "txtRepayPeriod_" + counter;
                txtRepayPeriod_new.Properties.EditMask = "D";
                txtRepayPeriod_new.Properties.MinValue = 1;
                txtRepayPeriod_new.Properties.MaxValue = 99;
                txtRepayPeriod_new.Properties.MaxLength = 2;
                txtRepayPeriod_new.Value = 0;
                txtRepayPeriod_new.Properties.ReadOnly = true;

                //
                //cmbInterestRateType
                //
                var cmbInterestRateType_new = new ComboBoxEdit();
                cmbInterestRateType_new.Name = "cmbInterestRateType_" + counter;
                cmbInterestRateType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbInterestRateType_new.Properties.Items.Add(LoanInterestRateTypeEnum.Fixed);
                cmbInterestRateType_new.Properties.Items.Add(LoanInterestRateTypeEnum.Floating);
                cmbInterestRateType_new.SelectedItem = LoanInterestRateTypeEnum.Fixed;
                cmbInterestRateType_new.Properties.ReadOnly = true;

                //
                //txtInterestRate
                //
                var txtInterestRate_new = new SpinEdit();
                txtInterestRate_new.Name = "txtInterestRate_" + counter;
                txtInterestRate_new.Properties.EditMask = "P0";
                txtInterestRate_new.Properties.MinValue = 1;
                txtInterestRate_new.Properties.MaxValue = 100;
                txtInterestRate_new.Properties.MaxLength = 3;
                txtInterestRate_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtInterestRate_new.Value = 0;
                txtInterestRate_new.Properties.ReadOnly = true;

                //
                //cmbPeriodType
                //
                var cmbPeriodType_new = new ComboBoxEdit();
                cmbPeriodType_new.Name = "cmbPeriodType_" + counter;
                cmbPeriodType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
                {
                    cmbPeriodType_new.Properties.Items.Add(type);
                }
                cmbPeriodType_new.Properties.Items.Add("");
                cmbPeriodType_new.Properties.ReadOnly = true;
                
                //
                //txtInterestCorFactor
                //
                var txtInterestCorFactor_new = new SpinEdit();
                txtInterestCorFactor_new.Name = "txtInterestCorFactor_" + counter;
                txtInterestCorFactor_new.Properties.EditMask = "P0";
                txtInterestCorFactor_new.Properties.MinValue = 1;
                txtInterestCorFactor_new.Properties.MaxValue = 100;
                txtInterestCorFactor_new.Properties.MaxLength = 3;
                txtInterestCorFactor_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtInterestCorFactor_new.Value = 0;
                txtInterestCorFactor_new.Properties.ReadOnly = true;

                #endregion

                #region Layout Items

                LayoutControlGroup lcgTranchInfo_new = lcgTranches.AddGroup();
                lcgTranchInfo_new.GroupBordersVisible = true;
                lcgTranchInfo_new.Name = "lcgTranchInfo_" + counter;
                lcgTranchInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfo_new.Tag = counter;
                lcgTranchInfo_new.Text = "Tranche " + counter;
                lcgTranchInfo_new.TextVisible = true;
                //lcgTranchInfo_new.DefaultLayoutType = LayoutType.Vertical;
                lcgTranchInfo_new.AppearanceGroup.Font = new Font("Tahoma", lcgTranchInfo_new.AppearanceGroup.Font.Size, FontStyle.Bold);
                lcgTranchInfo_new.ExpandButtonVisible = true;
                
                LayoutControlGroup lcgTranchMainInfo_new = lcgTranchInfo_new.AddGroup();
                lcgTranchMainInfo_new.GroupBordersVisible = false;
                lcgTranchMainInfo_new.Name = "lcgTranchMainInfo_" + counter;
                lcgTranchMainInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchMainInfo_new.Tag = counter;
                lcgTranchMainInfo_new.Text = "lcgTranchMainInfo_" + counter;
                lcgTranchMainInfo_new.TextVisible = false;
                lcgTranchMainInfo_new.DefaultLayoutType = LayoutType.Vertical;
                
                LayoutControlGroup lcgTranchInfoPart1_new = lcgTranchMainInfo_new.AddGroup();
                lcgTranchInfoPart1_new.GroupBordersVisible = false;
                lcgTranchInfoPart1_new.Name = "lcgTranchMainInfoPart1_" + counter;
                lcgTranchInfoPart1_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfoPart1_new.Tag = counter;
                lcgTranchInfoPart1_new.Text = "lcgTranchMainInfoPart1_" + counter;
                lcgTranchInfoPart1_new.TextVisible = false;
                lcgTranchInfoPart1_new.DefaultLayoutType = LayoutType.Horizontal;

                LayoutControlGroup lcgTranchInfoPart2_new = lcgTranchMainInfo_new.AddGroup();
                lcgTranchInfoPart2_new.GroupBordersVisible = false;
                lcgTranchInfoPart2_new.Name = "lcgTranchMainInfoPart2_" + counter;
                lcgTranchInfoPart2_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfoPart2_new.Tag = counter;
                lcgTranchInfoPart2_new.Text = "lcgTranchMainInfoPart2_" + counter;
                lcgTranchInfoPart2_new.TextVisible = false;
                lcgTranchInfoPart2_new.DefaultLayoutType = LayoutType.Horizontal;

                #region Layout Items in Group Tranch Info

                // 
                // layoutTranchAmount
                // 
                LayoutControlItem layoutAmount_new = lcgTranchInfoPart1_new.AddItem();
                layoutAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutAmount_new.Control = txtTranchAmount_new;
                layoutAmount_new.Name = "layoutTranchAmount_" + counter;
                layoutAmount_new.Text = "Tranche Amount:";

                // 
                // layoutBalloonAmount
                // 
                LayoutControlItem layoutBalloonAmount_new = lcgTranchInfoPart1_new.AddItem();
                layoutBalloonAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutBalloonAmount_new.Control = txtBalloonAmount_new;
                layoutBalloonAmount_new.Name = "layoutBalloonAmount_" + counter;
                layoutBalloonAmount_new.Text = "Balloon Amount:";

                //
                //layoutPrincipalFrequency
                //
                LayoutControlItem layoutPrincipalFrequency_new = lcgTranchInfoPart1_new.AddItem();
                layoutPrincipalFrequency_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutPrincipalFrequency_new.Control = cmbPrincipalFrequency_new;
                layoutPrincipalFrequency_new.Name = "layoutPrincipalFrequency_" + counter;
                layoutPrincipalFrequency_new.Text = "Principal Frequency:";

                //
                //layoutInterestFrequency
                //
                LayoutControlItem layoutInterestFrequency_new = lcgTranchInfoPart1_new.AddItem();
                layoutInterestFrequency_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestFrequency_new.Control = cmbInterestFrequency_new;
                layoutInterestFrequency_new.Name = "layoutInterestFrequency_" + counter;
                layoutInterestFrequency_new.Text = "Interest Frequency:";

                //
                //layoutRepayPeriod
                //
                LayoutControlItem layoutItemRepayPeriod_new = lcgTranchInfoPart1_new.AddItem();
                layoutItemRepayPeriod_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutItemRepayPeriod_new.Control = txtRepayPeriod_new;
                layoutItemRepayPeriod_new.Name = "layoutRepayPeriod_" + counter;
                layoutItemRepayPeriod_new.Text = "Repay Period:";

                //
                //layoutInterestRateType_
                //
                LayoutControlItem layoutInterestRateType_new = lcgTranchInfoPart2_new.AddItem();
                layoutInterestRateType_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestRateType_new.Control = cmbInterestRateType_new;
                layoutInterestRateType_new.Name = "layoutInterestRateType_" + counter;
                layoutInterestRateType_new.Text = "Interest Rate Type:";

                //
                //layoutInterestRate_
                //
                LayoutControlItem layoutInterestRate_new = lcgTranchInfoPart2_new.AddItem();
                layoutInterestRate_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestRate_new.Control = txtInterestRate_new;
                layoutInterestRate_new.Name = "layoutInterestRate_" + counter;
                layoutInterestRate_new.Text = "Interest Rate:";

                //
                //layoutPeriodType_
                //
                LayoutControlItem layoutPeriodType_new = lcgTranchInfoPart2_new.AddItem();
                layoutPeriodType_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutPeriodType_new.Control = cmbPeriodType_new;
                layoutPeriodType_new.Name = "layoutPeriodType_" + counter;
                layoutPeriodType_new.Text = "Period Type:";

                //
                //layoutInterestCorFactor_
                //
                LayoutControlItem layoutInterestCorFactor_new = lcgTranchInfoPart2_new.AddItem();
                layoutInterestCorFactor_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestCorFactor_new.Control = txtInterestCorFactor_new;
                layoutInterestCorFactor_new.Name = "layoutInterestCorFactor_" + counter;
                layoutInterestCorFactor_new.Text = "Correction Factor:";
                
                #endregion


                LayoutControlGroup lcgInstallments_new = lcgTranchInfo_new.AddGroup();
                lcgInstallments_new.GroupBordersVisible = false;
                lcgInstallments_new.Name = "lcgInstallments_" + counter;
                lcgInstallments_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgInstallments_new.Tag = loanTranche;
                lcgInstallments_new.Text = "Installments";
                lcgInstallments_new.TextVisible = false;
                lcgInstallments_new.DefaultLayoutType = LayoutType.Horizontal;
                lcgInstallments_new.AppearanceGroup.Font = new Font("Tahoma",
                                                                 lcgInstallments_new.AppearanceGroup.Font.Size,
                                                                 FontStyle.Italic);

                #region Installments Grids
                
                #region Principal Installments Grid Control

                LayoutControlGroup lcgPInstallments_new = lcgInstallments_new.AddGroup();
                lcgPInstallments_new.GroupBordersVisible = true;
                lcgPInstallments_new.Name = "lcgPInstallments_" + counter;
                lcgPInstallments_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgPInstallments_new.Tag = counter;
                lcgPInstallments_new.Text = "Principal Installments";
                lcgPInstallments_new.TextVisible = true;
                lcgPInstallments_new.DefaultLayoutType = LayoutType.Horizontal;
                lcgPInstallments_new.AppearanceGroup.Font = new Font("Tahoma",
                                                                 lcgPInstallments_new.AppearanceGroup.Font.Size,
                                                                 FontStyle.Italic);

                GridControl grdPrincipalInstallments = new GridControl();
                GridView grvPrincipalInstallments = new GridView();

                grdPrincipalInstallments.BeginInit();
                // 
                // rspiInstallmentDate
                // 
                var repDate = new RepositoryItemDateEdit();
                repDate.AutoHeight = false;
                repDate.Name = "rspiInstallmentDate_" + counter;
                repDate.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[]
                                                                 {
                                                                     new DevExpress.XtraEditors.Controls.EditorButton()
                                                                 });

                // 
                // rpsiInstallmentAmount
                // 
                var repAmount = new RepositoryItemSpinEdit();
                repAmount.AutoHeight = false;
                repAmount.Mask.EditMask = "D";
                repAmount.Name = "rpsiInstallmentAmount_" + counter;
                repAmount.MinValue = 0;
                repAmount.MaxLength = 12;
                repAmount.AllowNullInput = DefaultBoolean.False;
                repAmount.Mask.UseMaskAsDisplayFormat = true;
                
                // 
                // gcInstallmentDate
                // 
                var gcInstallmentDate = new GridColumn();
                gcInstallmentDate.Caption = "Installment Date";
                gcInstallmentDate.ColumnEdit = repDate;
                gcInstallmentDate.FieldName = "PrincipalInstallmentDate";
                gcInstallmentDate.Name = "PrincipalInstallmentDate_" + counter;
                gcInstallmentDate.Visible = true;
                gcInstallmentDate.VisibleIndex = 0;
                gcInstallmentDate.Width = 221;

                // 
                // gcInstallmentAmount
                // 
                var gcInstallmentAmount = new GridColumn();
                gcInstallmentAmount.Caption = "Installment Amount";
                gcInstallmentAmount.ColumnEdit = repAmount;
                gcInstallmentAmount.FieldName = "PrincipalInstallmentAmount";
                gcInstallmentAmount.Name = "PrincipalInstallmentAmount_" + counter;
                gcInstallmentAmount.Visible = true;
                gcInstallmentAmount.VisibleIndex = 1;
                gcInstallmentAmount.Width = 551;

                // 
                // grdPrincipalInstallments
                // 
                grdPrincipalInstallments.MainView = grvPrincipalInstallments;
                grdPrincipalInstallments.Name = "grdPrincipalInstallments_" + counter;
                grdPrincipalInstallments.UseEmbeddedNavigator = true;
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.ImageList = imageList16;
                grdPrincipalInstallments.EmbeddedNavigator.Font = new Font("Tahoma", 10, FontStyle.Bold);

                NavigatorCustomButton removeButton = grdPrincipalInstallments.EmbeddedNavigator.Buttons.CustomButtons.Add();
                removeButton.Enabled = true;
                removeButton.Hint = "Delete";
                removeButton.ImageIndex = 1;
                removeButton.Index = 8;
                removeButton.Tag = counter;
                removeButton.Visible = true;

                grdPrincipalInstallments.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.CancelEdit.ImageIndex = 2;
                
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.Edit.Visible = false;
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.Remove.Visible = false;

                grdPrincipalInstallments.EmbeddedNavigator.Buttons.Append.Hint = "Add";
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.CancelEdit.Hint = "Differ";
                
                grdPrincipalInstallments.EmbeddedNavigator.Buttons.CancelEdit.Tag = counter;
                grdPrincipalInstallments.EmbeddedNavigator.ButtonClick += new NavigatorButtonClickEventHandler(EmbeddedNavigatorButtonClick);
                grdPrincipalInstallments.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[]
                                                                      {
                                                                          repDate, repAmount
                                                                      });
                grdPrincipalInstallments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[]
                                                                           {
                                                                               grvPrincipalInstallments
                                                                           });
                
                // 
                //grvPrincipalInstallments
                //
                grvPrincipalInstallments.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                    gcInstallmentDate,
                    gcInstallmentAmount});
                grvPrincipalInstallments.GridControl = grdPrincipalInstallments;
                grvPrincipalInstallments.OptionsView.NewItemRowPosition = NewItemRowPosition.Top;
                grvPrincipalInstallments.Name = "grvPrincipalInstallments_" + counter;
                grvPrincipalInstallments.OptionsView.ShowGroupPanel = false;
                grvPrincipalInstallments.OptionsView.EnableAppearanceEvenRow = true;
                grvPrincipalInstallments.OptionsView.EnableAppearanceOddRow = true;
                grvPrincipalInstallments.OptionsBehavior.AllowAddRows = DefaultBoolean.True;
                grvPrincipalInstallments.OptionsBehavior.Editable = true;
                grvPrincipalInstallments.OptionsBehavior.ReadOnly = false;
                grvPrincipalInstallments.Columns["PrincipalInstallmentDate"].SortOrder = ColumnSortOrder.Ascending;
                // 
                // layoutGrdPrincipalInstallments
                // 
                var layoutGrdPrincipalInstallments = lcgPInstallments_new.AddItem();
                layoutGrdPrincipalInstallments.Control = grdPrincipalInstallments;
                layoutGrdPrincipalInstallments.Name = "layoutGrdPrincipalInstallments_" + counter;
                layoutGrdPrincipalInstallments.TextToControlDistance = 0;
                layoutGrdPrincipalInstallments.TextVisible = false;

                grdPrincipalInstallments.EndInit();

                #endregion

                #region Interest Installments Grid Control

                LayoutControlGroup lcgIntInstallments_new = lcgInstallments_new.AddGroup();
                lcgIntInstallments_new.GroupBordersVisible = true;
                lcgIntInstallments_new.Name = "lcgIntInstallments_" + counter;
                lcgIntInstallments_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgIntInstallments_new.Tag = counter;
                lcgIntInstallments_new.Text = "Interest Installments";
                lcgIntInstallments_new.TextVisible = true;
                lcgIntInstallments_new.DefaultLayoutType = LayoutType.Horizontal;
                lcgIntInstallments_new.AppearanceGroup.Font = new Font("Tahoma",
                                                                 lcgIntInstallments_new.AppearanceGroup.Font.Size,
                                                                 FontStyle.Italic);

                GridControl grdInterestInstallments = new GridControl();
                GridView grvInterestInstallments = new GridView();

                grdInterestInstallments.BeginInit();

                // 
                // rspiInstallmentDate
                // 
                var repIntDate = new RepositoryItemDateEdit();
                repIntDate.AutoHeight = false;
                repIntDate.Name = "rspiIntInstallmentDate_" + counter;
                repIntDate.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[]
                                                                 {
                                                                     new DevExpress.XtraEditors.Controls.EditorButton()
                                                                 });

                //
                // rpsiReferenceType
                //
                var repPeriodType_new = new RepositoryItemComboBox();
                repPeriodType_new.TextEditStyle = TextEditStyles.DisableTextEditor;
                repPeriodType_new.Name = "rpsiInstallmentPeriodType_" + counter;
                repPeriodType_new.Items.Add("");
                foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
                {
                    repPeriodType_new.Properties.Items.Add(type);
                }

                //
                // rpsiInterestFixing
                //
                var repFixing = new RepositoryItemSpinEdit();
                repFixing.Name = "rpsiInstallmentFixing_" + counter;
                repFixing.Mask.EditMask = "f6";
                repFixing.MaxLength = 13;
                repFixing.Mask.UseMaskAsDisplayFormat = true;
                repFixing.AllowNullInput = DefaultBoolean.True;

                //// 
                //// gcInstallmentDate
                //// 
                var gcIntInstallmentDate = new GridColumn();
                gcIntInstallmentDate.Caption = "Installment Date";
                gcIntInstallmentDate.ColumnEdit = repIntDate;
                gcIntInstallmentDate.FieldName = "InterestInstallmentDate";
                gcIntInstallmentDate.Name = "InterestInstallmentDate_" + counter;
                gcIntInstallmentDate.Visible = true;
                gcIntInstallmentDate.VisibleIndex = 0;

                //// 
                //// gcInstallmentPeriodType
                //// 
                var gcInstallmentPeriodType = new GridColumn();
                gcInstallmentPeriodType.Caption = "Period Type";
                gcInstallmentPeriodType.ColumnEdit = repPeriodType_new;
                gcInstallmentPeriodType.FieldName = "InterestInstallmentPeriodType";
                gcInstallmentPeriodType.Name = "InterestInstallmentPeriodType_" + counter;
                gcInstallmentPeriodType.Visible = true;
                gcInstallmentPeriodType.VisibleIndex = 2;

                //// 
                //// gcInstallmentFixing
                //// 
                var gcInstallmentFixing = new GridColumn();
                gcInstallmentFixing.Caption = "Fixing";
                gcInstallmentFixing.ColumnEdit = repFixing;
                gcInstallmentFixing.FieldName = "InterestInstallmentFixing";
                gcInstallmentFixing.Name = "InterestInstallmentFixing_" + counter;
                gcInstallmentFixing.Visible = true;
                gcInstallmentFixing.VisibleIndex = 3;

                // grdInstallmentInstallments
                grdInterestInstallments.MainView = grvInterestInstallments;
                grdInterestInstallments.Name = "grdInterestInstallments_" + counter;

                grdInterestInstallments.UseEmbeddedNavigator = true;

                grdInterestInstallments.EmbeddedNavigator.Buttons.ImageList = imageList16;
                grdInterestInstallments.EmbeddedNavigator.Font = new Font("Tahoma", 10, FontStyle.Bold);

                NavigatorCustomButton removeIntButton = grdInterestInstallments.EmbeddedNavigator.Buttons.CustomButtons.Add();
                removeIntButton.Enabled = true;
                removeIntButton.Hint = "Delete";
                removeIntButton.ImageIndex = 1;
                removeIntButton.Index = 8;
                removeIntButton.Tag = counter;
                removeIntButton.Visible = true;

                grdInterestInstallments.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;

                grdInterestInstallments.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
                grdInterestInstallments.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
                grdInterestInstallments.EmbeddedNavigator.Buttons.Remove.Visible = false;
                grdInterestInstallments.EmbeddedNavigator.Buttons.Edit.Visible = false;

                grdInterestInstallments.EmbeddedNavigator.Buttons.Append.Hint = "Add";
                grdInterestInstallments.EmbeddedNavigator.Buttons.CancelEdit.Tag = counter;
                grdInterestInstallments.EmbeddedNavigator.ButtonClick += new NavigatorButtonClickEventHandler(EmbeddedNavigatorButtonClick);

                grdInterestInstallments.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[]
                                                                      {
                                                                          repIntDate, repPeriodType_new
                                                                      });
                grdInterestInstallments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[]
                                                                           {
                                                                               grvInterestInstallments
                                                                           });
                
                //grvInstallmentInstallments
                grvInterestInstallments.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[]
                                                             {
                                                                 gcIntInstallmentDate,
                                                                 gcInstallmentPeriodType,
                                                                 gcInstallmentFixing
                                                             });
                grvInterestInstallments.GridControl = grdInterestInstallments;
                grvInterestInstallments.OptionsView.NewItemRowPosition = NewItemRowPosition.Top;
                grvInterestInstallments.Name = "grvInterestInstallments_" + counter;
                grvInterestInstallments.OptionsView.ShowGroupPanel = false;
                grvInterestInstallments.OptionsView.EnableAppearanceEvenRow = true;
                grvInterestInstallments.OptionsView.EnableAppearanceOddRow = true;
                grvInterestInstallments.OptionsBehavior.AllowAddRows = DefaultBoolean.True;
                grvInterestInstallments.ClearSorting();
                grvInterestInstallments.Columns["InterestInstallmentDate"].SortOrder = ColumnSortOrder.Ascending;
                // 
                // layoutGrdInterestInstallments
                // 
                var layoutGrdInterestInstallments = lcgIntInstallments_new.AddItem();
                layoutGrdInterestInstallments.Control = grdInterestInstallments;
                layoutGrdInterestInstallments.Name = "layoutGrdInterestInstallments_" + counter;
                layoutGrdInterestInstallments.Text = "layoutGrdInterestInstallments_" + counter;
                layoutGrdInterestInstallments.TextToControlDistance = 0;
                layoutGrdInterestInstallments.TextVisible = false;

                grdInterestInstallments.EndInit();

                #endregion

                #endregion
                
                #endregion

                txtTranchAmount_new.EditValue = loanTranche.Amount;
                txtBalloonAmount_new.EditValue = loanTranche.BalloonAmount;
                cmbPrincipalFrequency_new.EditValue = loanTranche.PrincipalFrequency;
                cmbInterestFrequency_new.EditValue = loanTranche.InterestFrequency;
                txtRepayPeriod_new.EditValue = loanTranche.RepayPeriod;
                txtInterestRate_new.EditValue = loanTranche.InterestRate;
                cmbPeriodType_new.EditValue = loanTranche.PeriodType;
                txtInterestCorFactor_new.EditValue = loanTranche.InterestCorFactor;
                cmbInterestRateType_new.EditValue = loanTranche.InterestRateType;
                grdPrincipalInstallments.Tag = loanTranche.LoanPrincipalInstallments;
                grdInterestInstallments.Tag = loanTranche.LoanInterestInstallments;

                DataSet PrincipalInstallemntSource = new DataSet();
                BindingSource PrincipalInstallemntBs = new BindingSource();
                DataTable PrincipalInstallemntTable = new DataTable("PrincipalInstallments");

                PrincipalInstallemntTable.Columns.Add("PrincipalInstallmentDate" , typeof(DateTime));
                PrincipalInstallemntTable.Columns.Add("PrincipalInstallmentAmount" , typeof(decimal));
                PrincipalInstallemntSource.Tables.Add(PrincipalInstallemntTable);
                foreach(LoanPrincipalInstallment installment in loanTranche.LoanPrincipalInstallments)
                {
                    PrincipalInstallemntTable.Rows.Add(new object[] { installment.Date, installment.Amount });
                }
                PrincipalInstallemntBs.DataSource = PrincipalInstallemntSource;
                PrincipalInstallemntBs.DataMember = PrincipalInstallemntTable.TableName;
                grdPrincipalInstallments.DataSource = PrincipalInstallemntBs;

                DataSet InterestInstallemntSource = new DataSet();
                BindingSource InterestInstallemntBs = new BindingSource();
                DataTable InterestInstallmentTable = new DataTable("InterestInstallments");

                InterestInstallmentTable.Columns.Add("InterestInstallmentDate" , typeof(DateTime));
                InterestInstallmentTable.Columns.Add("InterestInstallmentAmount", typeof(decimal));
                InterestInstallmentTable.Columns.Add("InterestInstallmentPeriodType", typeof(string));
                InterestInstallmentTable.Columns.Add("InterestInstallmentFixing", typeof(decimal));
                InterestInstallemntSource.Tables.Add(InterestInstallmentTable);

                foreach (LoanInterestInstallment installment in loanTranche.LoanInterestInstallments)
                {
                    InterestInstallmentTable.Rows.Add(new object[]
                                                          {
                                                              installment.Date, installment.Amount,
                                                              installment.PeriodType.ToString(), installment.Fixing
                                                          });
                }
                InterestInstallemntBs.DataSource = InterestInstallemntSource;
                InterestInstallemntBs.DataMember = InterestInstallmentTable.TableName;
                grdInterestInstallments.DataSource = InterestInstallemntBs;
                
            }

            lcTranches.EndUpdate();
            lcTranches.BestFit();

            #endregion
        }

        private int ValidateData()
        {
            bool hasInstallmentsError = false;
            
            foreach (object item in lcgTranches.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutTrancheInfo = (LayoutControlGroup) item;
                    if (layoutTrancheInfo.Name.StartsWith("lcgTranchInfo_"))
                    {
                        string layoutTranchSuffix =
                            layoutTrancheInfo.Name.Substring(layoutTrancheInfo.Name.IndexOf("_") + 1);

                        var layoutTranchMainInfo =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgTranchMainInfo_" + layoutTranchSuffix);

                        var layoutTranchMainPart1Info =
                            (LayoutControlGroup)
                            layoutTranchMainInfo.Items.FindByName("lcgTranchMainInfoPart1_" + layoutTranchSuffix);
                        
                        var txtTranchAmount =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutTranchAmount_" + layoutTranchSuffix))
                                .Control;

                        long loanTrancheAmount = Convert.ToInt64(txtTranchAmount.EditValue);

                        var layoutTranchInstallments =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgInstallments_" + layoutTranchSuffix);

                        var layoutTranchPrincipalInstallments =
                            (LayoutControlGroup)
                            layoutTranchInstallments.Items.FindByName("lcgPInstallments_" + layoutTranchSuffix);

                        var layoutTranchInterestInstallments =
                            (LayoutControlGroup)
                            layoutTranchInstallments.Items.FindByName("lcgIntInstallments_" + layoutTranchSuffix);

                        var layoutPrincipalInstallmentsGridItem = (LayoutControlItem)
                            layoutTranchPrincipalInstallments.Items.FindByName("layoutGrdPrincipalInstallments_" +
                                                                               layoutTranchSuffix);

                        var layoutInterestInstallmentsGridItem = (LayoutControlItem)
                            layoutTranchInterestInstallments.Items.FindByName("layoutGrdInterestInstallments_" +
                                                                               layoutTranchSuffix);

                        var grdPrincipalInstallemnts = (GridControl)layoutPrincipalInstallmentsGridItem.Control;
                        decimal totalTrancheAmount = 0;
                        var grvPrincipalInstallemnt = (GridView)grdPrincipalInstallemnts.MainView;
                        grvPrincipalInstallemnt.ClearColumnErrors();
                        for (int i = 0; i < grvPrincipalInstallemnt.RowCount; i++)
                        {
                            DataRow row = grvPrincipalInstallemnt.GetDataRow(i);
                            var installmentAmount = (decimal)row["PrincipalInstallmentAmount"];
                            totalTrancheAmount += installmentAmount;
                        }

                        decimal roundedValue = Math.Round(totalTrancheAmount);
                        if (roundedValue != loanTrancheAmount)
                            errorProvider.SetError(txtTranchAmount, Strings.The_sum_of_installemnts_amounts_should_be_equal_to_tranche_amount_);

                        var grdInterestInstallemnts = (GridControl)layoutInterestInstallmentsGridItem.Control;
                        var grvInterestInstallemnt = (GridView)grdInterestInstallemnts.MainView;
                        grvInterestInstallemnt.ClearColumnErrors();
                        for (int i = 0; i < grvInterestInstallemnt.RowCount; i++)
                        {
                            DataRow row = grvInterestInstallemnt.GetDataRow(i);
                            var installmentDate = (DateTime)row["InterestInstallmentDate"];
                            DataRow firstPrincipalInstallemntRow = grvPrincipalInstallemnt.GetDataRow(0);
                            var firstPrincipalInstallmentDate = (DateTime)firstPrincipalInstallemntRow["PrincipalInstallmentDate"];

                            if(i == 0)//If it is the first interest unstallemnt, check that the date is grater than the first principal installment
                            {
                                if(installmentDate < firstPrincipalInstallmentDate)
                                {
                                    grvInterestInstallemnt.FocusedRowHandle = 0;
                                    grvInterestInstallemnt.SetColumnError(grvInterestInstallemnt.Columns[0],
                                                                          "First interest installment date cannot be earlier than first principal installment.",
                                                                          ErrorType.Warning);
                                    hasInstallmentsError = true;
                                    break;
                                }
                            }
                            else if(i > 0)
                            //Check that there is a principal installment before this interest installment and that previous interest installment is earlier than that 
                            //principal i.e. there are no consecutive interest installments without a principal installment in the middle.
                            {

                                DateTime? previousPrincipalDate = null;
                                DateTime? nextPrincipalDate = null;

                                //Find the principal installment which is right before this interest installment
                                for (int j = 0; j < grvPrincipalInstallemnt.RowCount; j++)
                                {
                                    DataRow prinInstRow = grvPrincipalInstallemnt.GetDataRow(j);
                                    var prinInstDate = (DateTime)prinInstRow["PrincipalInstallmentDate"];

                                    if (prinInstDate == installmentDate)
                                    {
                                        prinInstRow = grvPrincipalInstallemnt.GetDataRow(j);
                                        previousPrincipalDate = (DateTime)prinInstRow["PrincipalInstallmentDate"];
                                        if (j == grvPrincipalInstallemnt.RowCount - 1)
                                            nextPrincipalDate = DateTime.MaxValue.Date;
                                        else
                                        {
                                            prinInstRow = grvPrincipalInstallemnt.GetDataRow(j + 1);
                                            nextPrincipalDate = (DateTime)prinInstRow["PrincipalInstallmentDate"];
                                        }
                                        break;
                                    }
                                    if (prinInstDate > installmentDate)
                                    {
                                        prinInstRow = grvPrincipalInstallemnt.GetDataRow(j - 1);
                                        previousPrincipalDate = (DateTime) prinInstRow["PrincipalInstallmentDate"];
                                        if (j == grvPrincipalInstallemnt.RowCount - 1)
                                            nextPrincipalDate = DateTime.MaxValue.Date;
                                        else
                                        {
                                            prinInstRow = grvPrincipalInstallemnt.GetDataRow(j);
                                            nextPrincipalDate = (DateTime)prinInstRow["PrincipalInstallmentDate"];
                                        }
                                        break;
                                    }
                                }
                                for (int k = 0; k < grvInterestInstallemnt.RowCount; k++)
                                {
                                    if(k == i) continue;
                                    
                                    DataRow interestInstRow = grvInterestInstallemnt.GetDataRow(k);
                                    var interestInstDate = (DateTime)interestInstRow["InterestInstallmentDate"];

                                    if(interestInstDate >= previousPrincipalDate && interestInstDate < nextPrincipalDate)
                                    {
                                        hasInstallmentsError = true;
                                        grvInterestInstallemnt.FocusedRowHandle = i;
                                        grvInterestInstallemnt.SetColumnError(grvInterestInstallemnt.Columns[0],
                                                                              "This interest installment does not correspond to a principal installment.",
                                                                              ErrorType.Warning);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (errorProvider.HasErrors)
                return 1;
            if (hasInstallmentsError)
                return 2;
            return 0;
        }

        private void GetData()
        {
            List<LoanPrincipalInstallment> principalInstallments = new List<LoanPrincipalInstallment>();
            List<LoanInterestInstallment> interestInstallments = new List<LoanInterestInstallment>();

            foreach (object item in lcgTranches.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutTrancheInfo = (LayoutControlGroup) item;
                    if (layoutTrancheInfo.Name.StartsWith("lcgTranchInfo_"))
                    {
                        string layoutTranchSuffix =
                            layoutTrancheInfo.Name.Substring(layoutTrancheInfo.Name.IndexOf("_") + 1);

                        var layoutTranchInstallments =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgInstallments_" + layoutTranchSuffix);
                        
                        var layoutTranchPrincipalInstallments =
                            (LayoutControlGroup)
                            layoutTranchInstallments.Items.FindByName("lcgPInstallments_" + layoutTranchSuffix);

                        var layoutTranchInterestInstallments =
                            (LayoutControlGroup)
                            layoutTranchInstallments.Items.FindByName("lcgIntInstallments_" + layoutTranchSuffix);

                        var layoutPrincipalInstallmentsGridItem = (LayoutControlItem)
                                                                  layoutTranchPrincipalInstallments.Items.FindByName(
                                                                      "layoutGrdPrincipalInstallments_" +
                                                                      layoutTranchSuffix);

                        var layoutInterestInstallmentsGridItem = (LayoutControlItem)
                                                                 layoutTranchInterestInstallments.Items.FindByName(
                                                                     "layoutGrdInterestInstallments_" +
                                                                     layoutTranchSuffix);

                        var tranche = (LoanTranche) layoutTranchInstallments.Tag;
                        var grdPrincipalInstallemnts = (GridControl) layoutPrincipalInstallmentsGridItem.Control;
                        var grvPrincipalInstallemnt = (GridView) grdPrincipalInstallemnts.MainView;
                        for (int i = 0; i < grvPrincipalInstallemnt.RowCount; i++)
                        {
                            DataRow row = grvPrincipalInstallemnt.GetDataRow(i);
                            var installmentDate = (DateTime)row["PrincipalInstallmentDate"];
                            var installmentAmount = Convert.ToInt64(row["PrincipalInstallmentAmount"]);

                            var loanPrincipalInstallment = new LoanPrincipalInstallment()
                            {
                                Amount = installmentAmount,
                                Date = installmentDate.Date,
                                LoanTrancheId = tranche.Id
                            };
                            principalInstallments.Add(loanPrincipalInstallment);
                        }
                        
                        var grdInterestInstallemnts = (GridControl) layoutInterestInstallmentsGridItem.Control;
                        var grvInterestInstallemnt = (GridView) grdInterestInstallemnts.MainView;
                        for (int i = 0; i < grvInterestInstallemnt.RowCount; i++)
                        {
                            DataRow row = grvInterestInstallemnt.GetDataRow(i);
                            var installmentDate = (DateTime) row["InterestInstallmentDate"];
                            var installmentPeriodType = (string)row["InterestInstallmentPeriodType"];
                            decimal? installmentFixing = row["InterestInstallmentFixing"] == DBNull.Value ? (decimal?)null : (decimal)row["InterestInstallmentFixing"];

                            var loanInterestInstallment = new LoanInterestInstallment()
                                                              {
                                                                  Date = installmentDate.Date,
                                                                  PeriodType =
                                                                      string.IsNullOrEmpty(installmentPeriodType)
                                                                          ? (InterestReferencePeriodTypeEnum?) null
                                                                          : (InterestReferencePeriodTypeEnum)
                                                                            Enum.Parse(
                                                                                typeof (
                                                                                    InterestReferencePeriodTypeEnum),
                                                                                installmentPeriodType),
                                                                  InterestRate = tranche.InterestRate,
                                                                  InterestRateType = tranche.InterestRateType,
                                                                  Fixing = installmentFixing,
                                                                  LoanTrancheId = tranche.Id
                                                              };
                            interestInstallments.Add(loanInterestInstallment);
                        }
                    }
                }
            }
            BeginAEInstallments(_loan.Id, principalInstallments, interestInstallments);
        }

        #endregion

        #region GUI Events

        private void LoanAevFormLoad(object sender, EventArgs e)
        {
            BeginAevLoanInitializationData();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            int validationResult = ValidateData();
            if (validationResult == 1)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (validationResult == 2)
            {
                DialogResult dialogResult = XtraMessageBox.Show(this,
                                                                Strings.
                                                                    There_are_Validation_Warnings__Would_you_like_to_procceed_anyway_,
                                                                "Warnings", MessageBoxButtons.YesNo,
                                                                MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    GetData();
                }
                else
                    return;
            }
            else if(validationResult == 0)
            {
                GetData();
            }
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void InstallmentAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void InstallmentAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void EmbeddedNavigatorButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.GetType() == typeof(NavigatorButton) && e.Button.ButtonType == NavigatorButtonType.CancelEdit)
            {
                var navigator = sender as ControlNavigator;
                var gridView = ((GridControl)navigator.NavigatableControl).MainView as GridView;

                int focusedRowHandle = gridView.FocusedRowHandle;
                if (gridView.RowCount == focusedRowHandle + 1) return;

                var amountDifferred = Convert.ToDecimal(gridView.GetFocusedRowCellValue("PrincipalInstallmentAmount"));
                decimal amountToAdd = amountDifferred/(gridView.RowCount - 1 - focusedRowHandle);

                gridView.BeginUpdate();
                
                DataRow focusedDataRow = gridView.GetFocusedDataRow();
                focusedDataRow["PrincipalInstallmentAmount"] = 0;
                for (int i = focusedRowHandle + 1; i < gridView.RowCount; i++)
                {
                    DataRow row = gridView.GetDataRow(i);
                    object value = gridView.GetRowCellValue(i, "PrincipalInstallmentAmount");
                    var previousAmount = value == DBNull.Value ? 0 : Convert.ToDecimal(gridView.GetRowCellValue(i, "PrincipalInstallmentAmount"));
                    row["PrincipalInstallmentAmount"] = previousAmount + amountToAdd;
                }
                gridView.EndUpdate();
            }
            else if (e.Button.GetType() == typeof(NavigatorCustomButton))
            {
                var navigator = sender as ControlNavigator;
                var gridView = ((GridControl)navigator.NavigatableControl).MainView as GridView;

                int focusedRowHandle = gridView.FocusedRowHandle;

                gridView.DeleteRow(focusedRowHandle);
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAevLoanInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVLoanInitializationData(EndAevLoanInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAevLoanInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAevLoanInitializationData;
                Invoke(action, ar);
                return;
            }
            
            int? result;

            List<Company> banks;
            List<Vessel> vessels;
            List<Company> obligors;
            List<Company> guarantors;
            List<Currency> currencies;
            List<Company> borrowers;

            try
            {
                result = SessionRegistry.Client.EndAEVLoanInitializationData(out banks, out vessels, out obligors,
                                                                             out guarantors,
                                                                             out currencies, out borrowers,
                                                                             ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookupCurrency.Properties.DataSource = currencies;
                lookupCurrency.Tag = currencies;
                lookupGuarantor.Tag = guarantors;
                lookupGuarantor.Properties.DataSource = guarantors;
                lookupObligor.Properties.DataSource = obligors;
                lookupObligor.Tag = obligors;
                cmbBorrower.Properties.DataSource = borrowers;
                cmbBorrower.Tag = borrowers;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                {
                    LoadData();
                }
            }
        }

        private void BeginAEInstallments(long loanId, List<LoanPrincipalInstallment> principalInstallments, List<LoanInterestInstallment> interestInstallments)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEInstallments(loanId, principalInstallments, interestInstallments,
                                                           EndAEInstallments, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEInstallments(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEInstallments;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEInstallments(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }
        
        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Loan Loan
        {
            get { return _loan; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}