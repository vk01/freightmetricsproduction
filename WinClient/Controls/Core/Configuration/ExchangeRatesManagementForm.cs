﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraGrid.Columns;
using Exis.WinClient.General;
using Exis.Domain;

namespace Exis.WinClient.Controls
{
    public partial class ExchangeRatesManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public ExchangeRatesManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        } 

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            grdExchangeRates.BeginUpdate();

            int gridViewColumnindex = 0;

            grvExchangeRates.Columns.Clear();

            grvExchangeRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Source_Currency,
                FieldName = "SourceCurrency",
                VisibleIndex = gridViewColumnindex++
            });
            grvExchangeRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Target_Currency,
                FieldName = "TargetCurrency",
                VisibleIndex = gridViewColumnindex++
            });
            grvExchangeRates.Columns.Add(new GridColumn
            {
                Caption = Strings.Date,
                FieldName = "Date",
                VisibleIndex = gridViewColumnindex++
            });
            grvExchangeRates.Columns.Add(new GridColumn
            {
                Caption = "Rate",
                FieldName = "Rate",
                VisibleIndex = gridViewColumnindex++
            });

            //grvExchangeRates.ClearGrouping();
            //grvExchangeRates.Columns["Type"].GroupIndex = 0;
            //grvExchangeRates.Columns["Currency"].GroupIndex = 1;

            grvExchangeRates.EndUpdate();
        }

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion

        #region GUI Event Handlers

        private void ExchangeRatesManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetExchangeRates();
        }

        private void btnAdd_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null)
                InsertEntityEvent(typeof(ExchangeRate));
        }

        private void btnEdit_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (grvExchangeRates.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((ExchangeRate)grvExchangeRates.GetFocusedRow());
        }

        private void btnDelete_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginBeginDeleteExchangeRate();
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetExchangeRates();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        #endregion

        #region Proxy Calls

        private void BeginGetExchangeRates()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new ExchangeRate(), EndGetExchangeRates, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetExchangeRates(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetExchangeRates;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                List<ExchangeRate> rates = entities.Cast<ExchangeRate>().ToList();
                grdExchangeRates.DataSource = rates.OrderBy(a => a.Date).OrderBy(a => a.SourceCurrency).ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginBeginDeleteExchangeRate()
        {
            var focusedRate = (ExchangeRate)grvExchangeRates.GetFocusedRow();

            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginDeleteExchangeRate(focusedRate.Id, EndDeleteExchangeRate, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndDeleteExchangeRate(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndDeleteExchangeRate;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndDeleteExchangeRate(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                //grvExchangeRates.DeleteRow(grvExchangeRates.FocusedRowHandle);
                
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                btnRefresh.PerformClick();
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}