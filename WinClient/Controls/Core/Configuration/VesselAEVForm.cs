﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class VesselAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private Vessel _vessel;

        #endregion

        #region Constructors

        public VesselAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public VesselAEVForm(Vessel vessel, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _vessel = vessel;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            lookupCompany.Properties.DisplayMember = "Name";
            lookupCompany.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
                          {
                              Caption = Strings.Company_Name,
                              FieldName = "Name",
                              Width = 0
                          };
            lookupCompany.Properties.Columns.Add(col);
            lookupCompany.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCompany.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCompany.Properties.NullValuePrompt = Strings.Select_a_Company___;

            lookupMarket.Properties.DisplayMember = "Name";
            lookupMarket.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Market_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupMarket.Properties.Columns.Add(col);
            lookupMarket.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMarket.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMarket.Properties.NullValuePrompt = Strings.Select_a_Market___;

            lookupContact.Properties.DisplayMember = "Name";
            lookupContact.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Contact_Last_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupContact.Properties.Columns.Add(col);
            lookupContact.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupContact.Properties.SearchMode = SearchMode.AutoFilter;
            lookupContact.Properties.NullValuePrompt = Strings.Select_a_Contact__;

            lookUpChiefOfficer.Properties.DisplayMember = "Name";
            lookUpChiefOfficer.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Contact_Last_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookUpChiefOfficer.Properties.Columns.Add(col);
            lookUpChiefOfficer.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpChiefOfficer.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpChiefOfficer.Properties.NullValuePrompt = Strings.Select_a_Contact__;

            lookUpMasterOfficer.Properties.DisplayMember = "Name";
            lookUpMasterOfficer.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Contact_Last_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookUpMasterOfficer.Properties.Columns.Add(col);
            lookUpMasterOfficer.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpMasterOfficer.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpMasterOfficer.Properties.NullValuePrompt = Strings.Select_a_Contact__;

            lookUpTechnicalManager.Properties.DisplayMember = "Name";
            lookUpTechnicalManager.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Contact_Last_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookUpTechnicalManager.Properties.Columns.Add(col);
            lookUpTechnicalManager.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpTechnicalManager.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpTechnicalManager.Properties.NullValuePrompt = Strings.Select_a_Contact__;
            
            var expenses = new List<VesselExpense>();
            for (int j = 1; j < 31; j++)
            {
                expenses.Add(new VesselExpense()
                                 {
                                     Age = j,
                                     DepreciationProfile = 0,
                                     DryDockExpenses = 0,
                                     DryDockOffHire = 0,
                                     OperationalExpenses = 0
                                 });
            }
            grdExpenses.DataSource = expenses;

            if (_formActionType == FormActionTypeEnum.View)
            {
                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                lookupCompany.Properties.ReadOnly = true;
                lookupMarket.Properties.ReadOnly = true;
                txtDeadWeightWinter.Properties.ReadOnly = true;
                txtDeadweightSummer.Properties.ReadOnly = true;
                txtDeadweightTropic.Properties.ReadOnly = true;
                txtDraftWinter.Properties.ReadOnly = true;
                txtLightweight.Properties.ReadOnly = true;
                txtDraftWinter.Properties.ReadOnly = true;
                txtDraftSummer.Properties.ReadOnly = true;
                txtDraftTropic.Properties.ReadOnly = true;
                txtGrainCapacity.Properties.ReadOnly = true;
                txtTPC.Properties.ReadOnly = true;
                txtLoa.Properties.ReadOnly = true;
                txtBeam.Properties.ReadOnly = true;
                txtClassification.Properties.ReadOnly = true;
                txtFlag.Properties.ReadOnly = true;
                txtYard.Properties.ReadOnly = true;
                txtPiClub.Properties.ReadOnly = true;
                txtFuelBallastSea.Properties.ReadOnly = true;
                txtFuelLadenSea.Properties.ReadOnly = true;
                txtFuelOilPort.Properties.ReadOnly = true;
                txtDieselBallastSea.Properties.ReadOnly = true;
                txtDieselLadenPort.Properties.ReadOnly = true;
                txtOilLadenSea.Properties.ReadOnly = true;
                txtManagementFee.Properties.ReadOnly = true;
                txtCommercialFee.Properties.ReadOnly = true;
                txtUsefulLife.Properties.ReadOnly = true;
                dtpSaleDate.Properties.ReadOnly = true;
                txtSaleAmount.Properties.ReadOnly = true;
                dtpDeliveryDate.Properties.ReadOnly = true;
                txtEquity.Properties.ReadOnly = true;
                dtpMOADate.Properties.ReadOnly = true;
                txtDeadWeight.Properties.ReadOnly = true;

                lookupCompany.Properties.ReadOnly = true;
                lookupMarket.Properties.ReadOnly = true;
                txtImoNumber.Properties.ReadOnly = true;
                txtAcquisitionPrice.Properties.ReadOnly = true;
                txtBenchAgeFrequency.Properties.ReadOnly = true;
                txtBenchAgePoints.Properties.ReadOnly = true;
                txtBenchInitial.Properties.ReadOnly = true;
                txtBenchmark.Properties.ReadOnly = true;
                dtpDateBuilt.Properties.ReadOnly = true;
                txtMarketPrice.Properties.ReadOnly = true;
                lookUpChiefOfficer.Properties.ReadOnly = true;
                lookUpMasterOfficer.Properties.ReadOnly = true;
                lookUpTechnicalManager.Properties.ReadOnly = true;
                lookupContact.Properties.ReadOnly = true;
                
                grvExpenses.OptionsBehavior.Editable = false;

                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void InitializeData()
        {
            BeginAEVVesselInitializationData();
        }

        private void LoadData()
        {
            lblId.Text = _vessel.Id.ToString();
            txtName.Text = _vessel.Name;
            cmbStatus.SelectedItem = _vessel.Status;

            lookupCompany.EditValue = _vessel.CompanyId;
            lookupMarket.EditValue = _vessel.MarketId;

            txtCreationUser.Text = _vessel.Cruser;
            dtpCreationDate.DateTime = _vessel.Crd;
            txtUpdateUser.Text = _vessel.Chuser;
            dtpUpdateDate.DateTime = _vessel.Chd;

            txtImoNumber.Text = _vessel.ImoNumber;
            txtAcquisitionPrice.Value = _vessel.AcquisitionPrice;
            txtBenchAgeFrequency.Value = _vessel.BenchAgeFrequency;
            txtBenchAgePoints.Value = _vessel.BenchAgePoints;
            txtBenchInitial.Value = _vessel.BenchInitial;
            txtBenchmark.Value = _vessel.Benchmark;
            dtpDateBuilt.EditValue = _vessel.DateBuilt;
            txtMarketPrice.Value = _vessel.MarketPrice;
            
            txtBeam.EditValue = _vessel.Info.Beam;
            lookUpChiefOfficer.EditValue = _vessel.Info.ChiefOfficerId;
            lookUpChiefOfficer.EditValue = _vessel.Info.ChiefOfficerId;
            txtClassification.EditValue = _vessel.Info.Classification;
            txtCommercialFee.EditValue = _vessel.Info.CommercialFee;
            lookupContact.EditValue = _vessel.Info.ContactId;
            txtDeadweightSummer.EditValue = _vessel.Info.DeadWeightSummer;
            txtDeadweightTropic.EditValue = _vessel.Info.DeadWeightTropic;
            txtDeadWeightWinter.EditValue = _vessel.Info.DeadWeightWinter;
            txtDieselBallastSea.EditValue = _vessel.Info.DieselOilBallastSea;
            txtOilLadenSea.EditValue = _vessel.Info.DieselOilLadenSea;
            txtDieselLadenPort.EditValue = _vessel.Info.DieselOilPort;
            txtDraftSummer.EditValue = _vessel.Info.DraftSummer;
            txtDraftTropic.EditValue = _vessel.Info.DraftTropic;
            txtDraftWinter.EditValue = _vessel.Info.DraftWinter;
            txtFlag.EditValue = _vessel.Info.Flag;
            txtFuelBallastSea.EditValue = _vessel.Info.FuelOilBallastSea;
            txtFuelLadenSea.EditValue = _vessel.Info.FuelOilLadenSea;
            txtFuelOilPort.EditValue = _vessel.Info.FuelOilPort;
            txtGrainCapacity.EditValue = _vessel.Info.GrainCapacity;
            txtLightweight.EditValue = _vessel.Info.LightWeight;
            txtLoa.EditValue = _vessel.Info.LOA;
            txtManagementFee.EditValue = _vessel.Info.ManagementFee;
            lookUpMasterOfficer.EditValue = _vessel.Info.MasterOfficerId;
            txtPiClub.EditValue = _vessel.Info.PiClub;
            txtSaleAmount.EditValue = _vessel.Info.SaleAmount;
            dtpSaleDate.EditValue = _vessel.Info.SaleDate;
            lookUpTechnicalManager.EditValue = _vessel.Info.TechnicalManagerId;
            txtTPC.EditValue = _vessel.Info.Tpc;
            txtUsefulLife.EditValue = _vessel.Info.UsefulLife;
            txtYard.EditValue = _vessel.Info.Yard;
            txtEquity.EditValue = _vessel.Info.Equity;
            dtpDeliveryDate.EditValue = _vessel.Info.DeliveryDate;
            dtpMOADate.EditValue = _vessel.Info.MoaDate;
            txtDeadWeight.EditValue = _vessel.Info.DeadWeight;

            if(_vessel.VesselExpenses != null && _vessel.VesselExpenses.Count > 0)
                grdExpenses.DataSource = _vessel.VesselExpenses;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (lookupCompany.EditValue == null) errorProvider.SetError(lookupCompany, Strings.Field_is_mandatory);
            if (lookupMarket.EditValue == null) errorProvider.SetError(lookupMarket, Strings.Field_is_mandatory);
            if (txtDeadweightSummer.EditValue == null)
                errorProvider.SetError(txtDeadweightSummer, Strings.Field_is_mandatory);
            if (txtBenchInitial.EditValue == null) errorProvider.SetError(txtBenchInitial, Strings.Field_is_mandatory);
            if (txtBenchAgePoints.EditValue == null) errorProvider.SetError(txtBenchAgePoints, Strings.Field_is_mandatory);
            if (txtBenchAgeFrequency.EditValue == null) errorProvider.SetError(txtBenchAgeFrequency, Strings.Field_is_mandatory);
            if (txtAcquisitionPrice.EditValue == null) errorProvider.SetError(txtAcquisitionPrice, Strings.Field_is_mandatory);
            if (txtMarketPrice.EditValue == null) errorProvider.SetError(txtMarketPrice, Strings.Field_is_mandatory);
            if (dtpDateBuilt.EditValue == null)
            {
                if (lookupCompany.EditValue != null)
                {
                    var selectedCompany =
                        ((List<Company>) lookupCompany.Tag).Where(a => a.Id == (long) lookupCompany.EditValue).Single();
                    if (selectedCompany.Type != CompanyTypeEnum.External)
                        errorProvider.SetError(dtpDateBuilt, Strings.Field_is_mandatory);
                }
            }
            if(txtImoNumber.EditValue == null || string.IsNullOrEmpty(txtImoNumber.EditValue.ToString()))
            {
                if(lookupCompany.EditValue != null)
                {
                    var selectedCompany =
                        ((List<Company>) lookupCompany.Tag).Where(a => a.Id == (long) lookupCompany.EditValue).Single();
                    if (selectedCompany.Type != CompanyTypeEnum.External)
                        errorProvider.SetError(txtImoNumber, Strings.Field_is_mandatory);
                }
            }
            if(txtUsefulLife.EditValue == null) errorProvider.SetError(txtUsefulLife, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void VesselAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _vessel = new Vessel();
                _vessel.Info = new VesselInfo();
            }

            _vessel.Name = txtName.Text.Trim();
            _vessel.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;

            _vessel.CompanyId = (long) lookupCompany.EditValue;
            _vessel.MarketId = (long) lookupMarket.EditValue;
            _vessel.ImoNumber = txtImoNumber.Text;
            _vessel.AcquisitionPrice = (int)txtAcquisitionPrice.Value;
            _vessel.BenchAgeFrequency = (int)txtBenchAgeFrequency.Value;
            _vessel.BenchAgePoints = txtBenchAgePoints.Value;
            _vessel.BenchInitial = txtBenchInitial.Value;
            _vessel.Benchmark = (int)txtBenchmark.Value;
            _vessel.DateBuilt = dtpDateBuilt.EditValue == null
                                    ? new DateTime(2000, 1, 1)
                                    : (DateTime) dtpDateBuilt.EditValue;
            _vessel.MarketPrice = (int)txtMarketPrice.Value;

            _vessel.Info.Beam = (int?)txtBeam.Value;
            _vessel.Info.ChiefOfficerId = (long?)lookUpChiefOfficer.EditValue;
            _vessel.Info.Classification = txtClassification.Text;
            _vessel.Info.CommercialFee = (int?)txtCommercialFee.Value;
            _vessel.Info.ContactId = (long?)lookupContact.EditValue;
            _vessel.Info.DeadWeightSummer = (int)txtDeadweightSummer.Value;
            _vessel.Info.DeadWeightTropic = (int)txtDeadweightTropic.Value;
            _vessel.Info.DeadWeightWinter = (int?)txtDeadWeightWinter.Value;
            _vessel.Info.DieselOilBallastSea = (decimal?)txtDieselBallastSea.Value;
            _vessel.Info.DieselOilLadenSea = (decimal?)txtOilLadenSea.Value;
            _vessel.Info.DieselOilPort = (decimal?)txtDieselLadenPort.Value;
            _vessel.Info.DraftSummer = (decimal?)txtDraftSummer.Value;
            _vessel.Info.DraftTropic = (decimal?)txtDraftTropic.Value;
            _vessel.Info.DraftWinter = (decimal?)txtDraftWinter.Value;
            _vessel.Info.Flag = txtFlag.Text;
            _vessel.Info.FuelOilBallastSea = (decimal?)txtFuelBallastSea.Value;
            _vessel.Info.FuelOilLadenSea = (decimal?)txtFuelLadenSea.Value;
            _vessel.Info.FuelOilPort = (decimal?)txtFuelOilPort.Value;
            _vessel.Info.GrainCapacity = (long?)txtGrainCapacity.Value;
            _vessel.Info.LightWeight = (int?)txtLightweight.Value;
            _vessel.Info.LOA = (int?)txtLoa.Value;
            _vessel.Info.ManagementFee = (int?)txtManagementFee.Value;
            _vessel.Info.MasterOfficerId = (long?)lookUpMasterOfficer.EditValue;
            _vessel.Info.PiClub = txtPiClub.Text;
            _vessel.Info.SaleAmount = (int?)txtSaleAmount.Value;
            _vessel.Info.SaleDate = dtpSaleDate.EditValue == null ? null : ((DateTime?)dtpSaleDate.EditValue);
            _vessel.Info.TechnicalManagerId = (long?)lookUpTechnicalManager.EditValue;
            _vessel.Info.Tpc = (decimal?)txtTPC.Value;
            _vessel.Info.UsefulLife = (int?)txtUsefulLife.Value;
            _vessel.Info.Yard = txtYard.Text;
            _vessel.Info.Equity = txtEquity.Value;
            _vessel.Info.DeliveryDate = dtpDeliveryDate.EditValue == null ? null : ((DateTime?)dtpDeliveryDate.EditValue);
            _vessel.Info.MoaDate = dtpMOADate.EditValue == null ? null : ((DateTime?)dtpMOADate.EditValue);
            _vessel.Info.DeadWeight = (int?) txtDeadWeight.Value;

            var vesselExpenses = new List<VesselExpense>();
            for (int i = 0; i < grvExpenses.DataRowCount; i++)
            {
                int age = (int)grvExpenses.GetRowCellValue(i, "Age");
                decimal operationalExpenses = (decimal)grvExpenses.GetRowCellValue(i, "OperationalExpenses");
                decimal dryDockExpenses = (decimal)grvExpenses.GetRowCellValue(i, "DryDockExpenses");
                int dryDockOffHire = (int)grvExpenses.GetRowCellValue(i, "DryDockOffHire");
                int depreciationProfile = (int)grvExpenses.GetRowCellValue(i, "DepreciationProfile");

                vesselExpenses.Add(new VesselExpense()
                                       {
                                           Age = age,
                                           DepreciationProfile = depreciationProfile,
                                           DryDockExpenses = dryDockExpenses,
                                           DryDockOffHire = dryDockOffHire,
                                           OperationalExpenses = operationalExpenses
                                       });
            }
            _vessel.VesselExpenses = vesselExpenses;

            decimal benchInitial = _vessel.BenchInitial;
            int benchFrequency = _vessel.BenchAgeFrequency;
            decimal benchPoints = _vessel.BenchAgePoints;
            decimal currentBenchmark = benchInitial;
            DateTime dateFrom = _vessel.DateBuilt.Date;
            DateTime dateTo = dateFrom.AddYears(benchFrequency).Date;
            
            VesselBenchmarking newVesselBenchmarking;
            var vesselBenchmarkings = new List<VesselBenchmarking>();

            for (int i = 0; i < 40; i++ )
            {
                if (dateFrom.Year > _vessel.DateBuilt.AddYears(40).Year)//|| dateFrom.Year > DateTime.Now.Year
                    break;

                currentBenchmark = currentBenchmark - benchPoints;
                newVesselBenchmarking = new VesselBenchmarking()
                                            {
                                                PeriodFrom = dateFrom,
                                                PeriodTo = dateTo,
                                                Benchmark = currentBenchmark
                                            };
                vesselBenchmarkings.Add(newVesselBenchmarking);

                dateFrom = dateTo.Date;
                dateTo = dateTo.AddYears(benchFrequency).Date;
            }
            _vessel.VesselBenchmarkings = vesselBenchmarkings;
            
            BeginAEVVessel(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void VesselAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void VesselAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVVesselInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVVesselInitializationData(
                    EndAEVVesselInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVVesselInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVVesselInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Company> companies;
            List<Market> markets;
            List<Contact> contacts;
            try
            {
                result = SessionRegistry.Client.EndAEVVesselInitializationData(out companies, out markets, out contacts, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookupMarket.Tag = markets;
                lookupMarket.Properties.DataSource = markets;

                lookupCompany.Tag = companies;
                lookupCompany.Properties.DataSource = companies;

                lookupContact.Tag = contacts;
                lookupContact.Properties.DataSource = contacts;

                lookUpChiefOfficer.Tag = contacts;
                lookUpChiefOfficer.Properties.DataSource = contacts;
                
                lookUpMasterOfficer.Tag = contacts;
                lookUpMasterOfficer.Properties.DataSource = contacts;

                lookUpTechnicalManager.Tag = contacts;
                lookUpTechnicalManager.Properties.DataSource = contacts;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVVessel(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _vessel, EndAEVVessel, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVVessel(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVVessel;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Vessel Vessel
        {
            get { return _vessel; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}