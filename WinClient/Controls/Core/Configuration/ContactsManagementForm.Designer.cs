﻿namespace Exis.WinClient.Controls
{
    partial class ContactsManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactsManagementForm));
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.gridContacts = new DevExpress.XtraGrid.GridControl();
            this.gridContactsMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupContacts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutContacts = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnAdd = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnView = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridContactsMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "exit.ico");
            this.imageList24.Images.SetKeyName(4, "viewItem24x24.ico");
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.gridContacts);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(569, 275, 250, 350);
            this.layoutControl.Root = this.layoutRoot;
            this.layoutControl.Size = new System.Drawing.Size(892, 500);
            this.layoutControl.TabIndex = 3;
            this.layoutControl.Text = "layoutControl1";
            // 
            // gridContacts
            // 
            this.gridContacts.Location = new System.Drawing.Point(14, 33);
            this.gridContacts.MainView = this.gridContactsMainView;
            this.gridContacts.Name = "gridContacts";
            this.gridContacts.Size = new System.Drawing.Size(864, 453);
            this.gridContacts.TabIndex = 9;
            this.gridContacts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridContactsMainView});
            // 
            // gridContactsMainView
            // 
            this.gridContactsMainView.GridControl = this.gridContacts;
            this.gridContactsMainView.Name = "gridContactsMainView";
            this.gridContactsMainView.OptionsBehavior.Editable = false;
            this.gridContactsMainView.OptionsCustomization.AllowGroup = false;
            this.gridContactsMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridContactsMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridContactsMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridContactsMainView.OptionsView.ShowDetailButtons = false;
            this.gridContactsMainView.OptionsView.ShowGroupPanel = false;
            this.gridContactsMainView.OptionsView.ShowIndicator = false;
            // 
            // layoutRoot
            // 
            this.layoutRoot.CustomizationFormText = "layoutRoot";
            this.layoutRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRoot.GroupBordersVisible = false;
            this.layoutRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupContacts});
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRoot.Size = new System.Drawing.Size(892, 500);
            this.layoutRoot.Text = "layoutRoot";
            this.layoutRoot.TextVisible = false;
            // 
            // layoutGroupContacts
            // 
            this.layoutGroupContacts.CustomizationFormText = "Information";
            this.layoutGroupContacts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutContacts});
            this.layoutGroupContacts.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupContacts.Name = "layoutGroupContacts";
            this.layoutGroupContacts.Size = new System.Drawing.Size(892, 500);
            this.layoutGroupContacts.Text = "Contacts";
            // 
            // layoutContacts
            // 
            this.layoutContacts.Control = this.gridContacts;
            this.layoutContacts.CustomizationFormText = "layoutContacts";
            this.layoutContacts.Location = new System.Drawing.Point(0, 0);
            this.layoutContacts.Name = "layoutContacts";
            this.layoutContacts.Size = new System.Drawing.Size(868, 457);
            this.layoutContacts.Text = "layoutContacts";
            this.layoutContacts.TextSize = new System.Drawing.Size(0, 0);
            this.layoutContacts.TextToControlDistance = 0;
            this.layoutContacts.TextVisible = false;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnView,
            this.btnRefresh,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 7;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnView),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnAdd
            // 
            this.btnAdd.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnAdd.Caption = "Add";
            this.btnAdd.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnAdd.Id = 0;
            this.btnAdd.LargeImageIndex = 0;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnEdit.Caption = "Edit";
            this.btnEdit.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnEdit.Id = 2;
            this.btnEdit.LargeImageIndex = 1;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_Click);
            // 
            // btnView
            // 
            this.btnView.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnView.Caption = "View";
            this.btnView.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnView.Id = 4;
            this.btnView.LargeImageIndex = 4;
            this.btnView.Name = "btnView";
            this.btnView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnView_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnRefresh.Caption = "Refresh";
            this.btnRefresh.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnRefresh.Id = 5;
            this.btnRefresh.LargeImageIndex = 2;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(892, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 500);
            this.barDockControlBottom.Size = new System.Drawing.Size(892, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 500);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(892, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 500);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 6;
            this.btnClose.LargeImageIndex = 3;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // ContactsManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 532);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ContactsManagementForm";
            this.Activated += new System.EventHandler(this.ContactsManagementForm_Activated);
            this.Deactivate += new System.EventHandler(this.ContactsManagementForm_Deactivate);
            this.Load += new System.EventHandler(this.ContactsManagementForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridContactsMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraGrid.GridControl gridContacts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridContactsMainView;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupContacts;
        private DevExpress.XtraLayout.LayoutControlItem layoutContacts;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnAdd;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnEdit;
        private DevExpress.XtraBars.BarLargeButtonItem btnView;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}