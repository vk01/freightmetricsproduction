﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class IndexesManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public IndexesManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void IndexesManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetIndexes();
        }

        private void btnAdd_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof (Index));
        }

        private void btnEdit_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridIndexesMainView.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((Index) gridIndexesMainView.GetFocusedRow());
        }

        private void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridIndexesMainView.GetFocusedRow() == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((Index) gridIndexesMainView.GetFocusedRow());
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetIndexes();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void IndexesManagementForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void IndexesManagementForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridIndexes.BeginUpdate();

            int gridViewColumnindex = 0;

            gridIndexesMainView.Columns.Clear();

            gridIndexesMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Code,
                                                    FieldName = "Code",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridIndexesMainView.Columns.Add(new GridColumn
                                                       {
                                                           Caption = Strings.Name,
                                                           FieldName = "Name",
                                                           VisibleIndex = gridViewColumnindex++
                                                       });
            gridIndexesMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Market_Name,
                                                    FieldName = "MarketName",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridIndexesMainView.Columns.Add(new GridColumn
                                                       {
                                                           Caption = Strings.Status,
                                                           FieldName = "Status",
                                                           VisibleIndex = gridViewColumnindex++
                                                       });
            gridIndexesMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Is_Custom,
                                                    FieldName = "IsCustom",
                                                    VisibleIndex = gridViewColumnindex
                                                });

            gridIndexesMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Is_Market_Default,
                                                    FieldName = "IsMarketDefault",
                                                    VisibleIndex = gridViewColumnindex
                                                });
            gridIndexesMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Is_Assoc_Index,
                                                    FieldName = "IsAssocIndex",
                                                    VisibleIndex = gridViewColumnindex
                                                });

            gridIndexes.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetIndexes()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new Index(), EndGetIndexes, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetIndexes(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetIndexes;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridIndexes.DataSource = entities.Cast<Index>().ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion

    }
}