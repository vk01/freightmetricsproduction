﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraGrid.Columns;
using Exis.WinClient.General;
using Exis.Domain;

namespace Exis.WinClient.Controls
{
    public partial class UsersManagementForm : XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Contructor

        public UsersManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridUsers.BeginUpdate();

            int gridViewColumnindex = 0;

            gridUsersMainView.Columns.Clear();

            gridUsersMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Login,
                FieldName = "Login",
                VisibleIndex = gridViewColumnindex++
            });
            gridUsersMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Last_Name,
                FieldName = "LastName",
                VisibleIndex = gridViewColumnindex++
            });
            gridUsersMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.First_Name,
                FieldName = "FirstName",
                VisibleIndex = gridViewColumnindex++
            });
            gridUsersMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Status,
                FieldName = "Status",
                VisibleIndex = gridViewColumnindex
            });

            gridUsers.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetUsers()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new User(), EndGetUsers, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetUsers(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetUsers;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridUsers.DataSource = entities.Cast<User>().ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region GUI Events

        private void UsersManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetUsers();
        }

        private void BtnAddClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof(User));
        }

        private void BtnEditClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridUsersMainView.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((User)gridUsersMainView.GetFocusedRow());
        }

        private void BtnViewClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridUsersMainView.GetFocusedRow() == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((User)gridUsersMainView.GetFocusedRow());
        }

        private void BtnRefreshClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetUsers();
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void UsersManagementFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void UsersManagementFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion
        
    }
}