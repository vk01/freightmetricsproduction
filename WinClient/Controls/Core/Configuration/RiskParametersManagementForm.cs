﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraLayout;
using Exis.Domain;
using DevExpress.XtraLayout.Utils;
using Exis.WinClient.General;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace Exis.WinClient.Controls
{
    public partial class RiskParametersManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors 

        public RiskParametersManagementForm()
        {
            InitializeComponent();

            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            lookupIndex_1.Properties.DisplayMember = "Name";
            lookupIndex_1.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupIndex_1.Properties.Columns.Add(col);
            lookupIndex_1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupIndex_1.Properties.SearchMode = SearchMode.AutoFilter;
            lookupIndex_1.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;
        }
        
        private bool ValidateData()
        {
            dxErrorProvider.ClearErrors();
            foreach (object item in lcgRiskVolatilitiesInfos.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var lcgRiskVolatility = (LayoutControlGroup)item;
                    if (lcgRiskVolatility.Name.StartsWith("lcgRiskVolatility_"))
                    {
                        string layoutSuffix = lcgRiskVolatility.Name.Substring(lcgRiskVolatility.Name.IndexOf("_") + 1);
                        var index = (LookUpEdit)((LayoutControlItem)lcgRiskVolatility.Items.FindByName("layoutRiskIndex_" + layoutSuffix)).Control;

                        if(index.EditValue == null)
                           dxErrorProvider.SetError(index, Strings.Field_is_mandatory);
                    }
                }
            }
            return !dxErrorProvider.HasErrors;
        }

        #endregion

        #region Proxy Calls

        private void BeginGetInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAERiskParametersInitializationData(EndGetInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Index> indexes;
            List<RiskVolatility> riskVolatilities;
            List<RiskCorrelation> riskCorrelations;
            try
            {
                result = SessionRegistry.Client.EndAERiskParametersInitializationData(out indexes, out riskVolatilities,
                                                                                      out riskCorrelations, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookupIndex_1.Tag = indexes;
                lookupIndex_1.Properties.DataSource = indexes;

                #region Load Correlations
                grdRiskCorrelations.BeginUpdate();

                //Add a column for each target index
                var itemSpinEdit = new RepositoryItemSpinEdit
                                       {
                                           Name = "rpsiValue",
                                           EditMask = "P0",
                                           MinValue = 0,
                                           MaxValue = 100,
                                           MaxLength = 3,
                                           AllowNullInput = DevExpress.Utils.DefaultBoolean.False
                                       };

                foreach (Index index in indexes)
                {
                    var gcTargetIndex = new BandedGridColumn();
                    gcTargetIndex.Caption = index.Name;
                    gcTargetIndex.ColumnEdit = itemSpinEdit;
                    gcTargetIndex.FieldName = "TargetIndex_" + index.Id;
                    gcTargetIndex.Name = "TargetIndex_" + index.Id;
                    gcTargetIndex.Visible = true;

                    grvRiskCorrelations.Bands[1].Columns.Add(gcTargetIndex);
                }

                grdRiskCorrelations.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[]
                                                                      {
                                                                          itemSpinEdit
                                                                      });

                grdRiskCorrelations.EndUpdate();
                
                DataSet riskCorrelationsDS = new DataSet();
                BindingSource riskCorrelationsBS = new BindingSource();
                DataTable riskCorrelationDT = new DataTable("riskCorrelations");

                riskCorrelationDT.Columns.Add("SourceIndexId", typeof(long));
                riskCorrelationDT.Columns.Add("SourceIndex", typeof(string));  
                foreach (Index index in indexes)
                {
                    riskCorrelationDT.Columns.Add("TargetIndex_"  + index.Id, typeof(decimal));    
                }
                riskCorrelationsDS.Tables.Add(riskCorrelationDT);

                foreach (Index index in indexes)
                {
                    var values = new object[indexes.Count + 2];
                    values.SetValue(index.Id, 0);
                    values.SetValue(index.Name, 1);

                    for (int i = 0; i < indexes.Count; i++)
                    {
                        RiskCorrelation riskCorrelation =
                            riskCorrelations.Where(a => a.SourceIndexId == index.Id && a.TargetIndexId == indexes[i].Id)
                                .FirstOrDefault();
                        values.SetValue(riskCorrelation != null ? riskCorrelation.Value : 0, i + 2);
                    }
                    riskCorrelationDT.Rows.Add(values);
                }
                riskCorrelationsBS.DataSource = riskCorrelationsDS;
                riskCorrelationsBS.DataMember = riskCorrelationDT.TableName;
                grdRiskCorrelations.DataSource = riskCorrelationsBS;

                for (int i = 0; i < grvRiskCorrelations.RowCount; i++)
                {
                    long currentSourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(i, "SourceIndexId"));
                    DataRow currentDataRow = grvRiskCorrelations.GetDataRow(i);
                    currentDataRow["TargetIndex_" + currentSourceIndexId] = 100;
                }

                #endregion

                #region Load Volatilities

                lcRiskVolatilities.BeginUpdate();
                lcgRiskVolatilitiesInfos.Clear();

                foreach (RiskVolatility riskVolatility in riskVolatilities)
                {
                    int counter = Convert.ToInt32(lcgRiskVolatilitiesInfos.Items.Count + 1);

                    var lookupIndex_new = new LookUpEdit();
                    var txtPercentage_new = new SpinEdit();

                    // Index
                    // 
                    lookupIndex_new.Name = "lookupIndex_" + counter;
                    lookupIndex_new.Properties.DisplayMember = "Name";
                    lookupIndex_new.Properties.ValueMember = "Id";
                    var col = new LookUpColumnInfo
                    {
                        Caption = Strings.Market_Index,
                        FieldName = "Name",
                        Width = 0
                    };
                    lookupIndex_new.Properties.Columns.Add(col);
                    lookupIndex_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                    lookupIndex_new.Properties.SearchMode = SearchMode.AutoFilter;
                    lookupIndex_new.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;
                    lookupIndex_new.Properties.DataSource = indexes;
                    lookupIndex_new.EditValue = riskVolatility.IndexId;
                    lookupIndex_new.QueryPopUp += LookupIndexNewQueryPopUp;
                    

                    //
                    //txtPercentage
                    //
                    txtPercentage_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
                    txtPercentage_new.Name = "txtRiskValue_" + counter;
                    txtPercentage_new.Properties.EditMask = "P0";
                    txtPercentage_new.Properties.MinValue = 1;
                    txtPercentage_new.Properties.MaxValue = 100;
                    txtPercentage_new.Properties.MaxLength = 3;
                    txtPercentage_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                    txtPercentage_new.EditValue = riskVolatility.Value;

                    LayoutControlGroup lcgRiskVolatility_new = lcgRiskVolatilitiesInfos.AddGroup();

                    // 
                    // lcgRiskVolatility_new
                    // 
                    lcgRiskVolatility_new.GroupBordersVisible = false;
                    lcgRiskVolatility_new.Name = "lcgRiskVolatility_" + counter;
                    lcgRiskVolatility_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                    lcgRiskVolatility_new.Tag = counter;
                    lcgRiskVolatility_new.Text = "lcgRiskVolatility";
                    lcgRiskVolatility_new.TextVisible = false;
                    lcgRiskVolatility_new.DefaultLayoutType = LayoutType.Horizontal;

                    // 
                    // layoutIndex_new
                    // 
                    LayoutControlItem layoutIndex_new = lcgRiskVolatility_new.AddItem();
                    layoutIndex_new.TextAlignMode = TextAlignModeItem.AutoSize;
                    layoutIndex_new.Control = lookupIndex_new;
                    layoutIndex_new.Name = "layoutRiskIndex_" + counter;
                    layoutIndex_new.Text = "Index:";
                    layoutIndex_new.SizeConstraintsType = SizeConstraintsType.Custom;
                    layoutIndex_new.MinSize = new Size(86, 24);
                    layoutIndex_new.MaxSize = new Size(0, 24);

                    // 
                    // layoutPercentage
                    // 
                    LayoutControlItem layoutPercentage_new = lcgRiskVolatility_new.AddItem();
                    layoutPercentage_new.Control = txtPercentage_new;
                    layoutPercentage_new.TextAlignMode = TextAlignModeItem.AutoSize;
                    layoutPercentage_new.Name = "layoutRiskValue_" + counter;
                    layoutPercentage_new.Text = "Value:";
                    
                }

                lcRiskVolatilities.EndUpdate();
                lcRiskVolatilities.BestFit();

                #endregion

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAeRiskParameters(List<RiskVolatility> riskVolatilities, List<RiskCorrelation> riskCorrelations)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAERiskParameteres(riskVolatilities, riskCorrelations, EndAeRiskParameters, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAeRiskParameters(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAeRiskParameters;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndAERiskParameteres(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, false);
            }
        }

        #endregion

        #region GUI Events

        private void RiskParametersManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetInitializationData();
        }

        private void BtnAddRiskVolatilityClick(object sender, EventArgs e)
        {
            foreach (object item in lcgRiskVolatilitiesInfos.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutRiskVolatility = (LayoutControlGroup)item;
                    if (layoutRiskVolatility.Name.StartsWith("lcgRiskVolatility_"))
                    {
                        string layoutSuffix = layoutRiskVolatility.Name.Substring(layoutRiskVolatility.Name.IndexOf("_") + 1);
                        var index = (LookUpEdit)((LayoutControlItem)layoutRiskVolatility.Items.FindByName("layoutRiskIndex_" + layoutSuffix)).Control;
                        if (index.EditValue == null)
                            return;
                    }
                }
            }

            lcRiskVolatilities.BeginUpdate();

            int counter = Convert.ToInt32(lcgRiskVolatilitiesInfos.Items[lcgRiskVolatilitiesInfos.Items.Count - 1].Tag) + 1;

            var lookupIndex_new = new LookUpEdit();
            var txtPercentage_new = new SpinEdit();

            // Index
            // 
            lookupIndex_new.Name = "lookupIndex_" + counter;
            lookupIndex_new.Properties.DisplayMember = "Name";
            lookupIndex_new.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupIndex_new.Properties.Columns.Add(col);
            lookupIndex_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupIndex_new.Properties.SearchMode = SearchMode.AutoFilter;
            lookupIndex_new.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;
            lookupIndex_new.QueryPopUp += new CancelEventHandler(LookupIndexNewQueryPopUp);
            if (lookupIndex_1.Tag != null)
            {
                lookupIndex_new.Properties.DataSource = lookupIndex_1.Tag;
                lookupIndex_new.Tag = lookupIndex_1.Tag;
            }

            //
            //txtPercentage
            //
            txtPercentage_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
            txtPercentage_new.Name = "txtRiskValue_" + counter;
            txtPercentage_new.Properties.EditMask = "P0";
            txtPercentage_new.Properties.MinValue = 1;
            txtPercentage_new.Properties.MaxValue = 100;
            txtPercentage_new.Properties.MaxLength = 3;
            txtPercentage_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtPercentage_new.Value = 0;

            LayoutControlGroup lcgRiskVolatility_new = lcgRiskVolatilitiesInfos.AddGroup();

            // 
            // lcgRiskVolatility_new
            // 
            lcgRiskVolatility_new.GroupBordersVisible = false;
            lcgRiskVolatility_new.Name = "lcgRiskVolatility_" + counter;
            lcgRiskVolatility_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgRiskVolatility_new.Tag = counter;
            lcgRiskVolatility_new.Text = "lcgRiskVolatility";
            lcgRiskVolatility_new.TextVisible = false;
            lcgRiskVolatility_new.DefaultLayoutType = LayoutType.Horizontal;
            
            // 
            // layoutIndex_new
            // 
            LayoutControlItem layoutIndex_new = lcgRiskVolatility_new.AddItem();
            layoutIndex_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutIndex_new.Control = lookupIndex_new;
            layoutIndex_new.Name = "layoutRiskIndex_" + counter;
            layoutIndex_new.Text = "Index:";
            layoutIndex_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutIndex_new.MinSize = new Size(86, 24);
            layoutIndex_new.MaxSize = new Size(0, 24);

            // 
            // layoutPercentage
            // 
            LayoutControlItem layoutPercentage_new = lcgRiskVolatility_new.AddItem();
            layoutPercentage_new.Control = txtPercentage_new;
            layoutPercentage_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutPercentage_new.Name = "layoutRiskValue_" + counter;
            layoutPercentage_new.Text = "Value:";

            lcRiskVolatilities.EndUpdate();
            lcRiskVolatilities.BestFit();
        }

        private void LookupIndexNewQueryPopUp(object sender, CancelEventArgs e)
        {
            var indexes = (List<Index>)((LookUpEdit)lookupIndex_1).Tag;

            var selectedIndexIds = new List<long>();
            foreach (object item in lcgRiskVolatilitiesInfos.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutRiskVolatility = (LayoutControlGroup)item;
                    if (layoutRiskVolatility.Name.StartsWith("lcgRiskVolatility_"))
                    {
                        string layoutSuffix = layoutRiskVolatility.Name.Substring(layoutRiskVolatility.Name.IndexOf("_") + 1);
                        var index = (LookUpEdit)((LayoutControlItem)layoutRiskVolatility.Items.FindByName("layoutRiskIndex_" + layoutSuffix)).Control;
                        if (index.EditValue != null && index.Name != ((LookUpEdit)sender).Name)
                            selectedIndexIds.Add((long)index.EditValue);
                    }
                }
            }
            ((LookUpEdit)sender).Properties.DataSource = indexes.Where(a => !selectedIndexIds.Contains(a.Id)).ToList();
        }

        private void BtnRemoveRiskVolatilityClick(object sender, EventArgs e)
        {
            if (lcgRiskVolatilitiesInfos.Items.Count == 1) return;

            lcRiskVolatilities.BeginUpdate();

            foreach (
                LayoutControlItem layoutControlItem in
                    ((LayoutControlGroup)lcgRiskVolatilitiesInfos.Items[lcgRiskVolatilitiesInfos.Items.Count - 1]).Items)
            {
                lcRiskVolatilities.Controls.Remove(layoutControlItem.Control);
            }

            lcgRiskVolatilitiesInfos.RemoveAt(lcgRiskVolatilitiesInfos.Items.Count - 1);
            lcRiskVolatilities.EndUpdate();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var riskVolatilities = new List<RiskVolatility>();
            foreach (object item in lcgRiskVolatilitiesInfos.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var lcgRiskVolatility = (LayoutControlGroup)item;
                    if (lcgRiskVolatility.Name.StartsWith("lcgRiskVolatility_"))
                    {
                        string layoutSuffix = lcgRiskVolatility.Name.Substring(lcgRiskVolatility.Name.IndexOf("_") + 1);
                        var index = (LookUpEdit)((LayoutControlItem)lcgRiskVolatility.Items.FindByName("layoutRiskIndex_" + layoutSuffix)).Control;
                        var value = (SpinEdit)((LayoutControlItem)lcgRiskVolatility.Items.FindByName("layoutRiskValue_" + layoutSuffix)).Control;

                        var riskVolatility = new RiskVolatility() { IndexId = (long)index.EditValue, Value = value.Value };
                        riskVolatilities.Add(riskVolatility);
                    }
                }
            }

            List<RiskCorrelation> riskCorrelations = new List<RiskCorrelation>();
            for(int i=0; i<grvRiskCorrelations.DataRowCount; i++)
            {
                long sourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(i, "SourceIndexId"));

                foreach (BandedGridColumn column in grvRiskCorrelations.Columns)
                {
                    if(column.FieldName.Contains("TargetIndex_"))
                    {
                        long targetIndexId = Convert.ToInt32(column.FieldName.Replace("TargetIndex_", ""));
                        decimal value = Convert.ToDecimal(grvRiskCorrelations.GetRowCellValue(i, column));
                        riskCorrelations.Add(new RiskCorrelation()
                                                 {
                                                     SourceIndexId = sourceIndexId,
                                                     TargetIndexId = targetIndexId,
                                                     Value = value
                                                 });
                    }
                }
            }

            BeginAeRiskParameters(riskVolatilities, riskCorrelations);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }
        
        private void GrvRiskCorrelationsRowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if ( e.CellValue != DBNull.Value && e.Column.FieldName.Contains("TargetIndex_"))
            {
                long sourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(e.RowHandle, "SourceIndexId"));

                if(e.Column.FieldName == "TargetIndex_" + sourceIndexId)
                    e.Appearance.BackColor = Color.LightBlue;
            }
        }

        private void GrvRiskCorrelationsCellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            long targetIndexId = Convert.ToInt32(e.Column.FieldName.Replace("TargetIndex_", ""));
            long sourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(e.RowHandle, "SourceIndexId"));

            for(int i=0; i<grvRiskCorrelations.DataRowCount; i++)
            {
                long curSourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(i, "SourceIndexId"));
                if(curSourceIndexId == targetIndexId)
                {
                    DataRow curRow = grvRiskCorrelations.GetDataRow(i);
                    curRow["TargetIndex_" + sourceIndexId] = e.Value;
                }
            }
        }

        private void grvRiskCorrelations_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            //if (e.CellValue != DBNull.Value && e.Column.FieldName.Contains("TargetIndex_") && grvRiskCorrelations.FocusedColumn.FieldName.Contains("TargetIndex_"))
            //{
            //    long targetIndexId = Convert.ToInt32(e.Column.FieldName.Replace("TargetIndex_", ""));
            //    long sourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(e.RowHandle, "SourceIndexId"));

            //    long targetFocusedIndexId = Convert.ToInt32(grvRiskCorrelations.FocusedColumn.FieldName.Replace("TargetIndex_", ""));
            //    long sourceFocusedIndexId = Convert.ToInt32(grvRiskCorrelations.GetFocusedRowCellValue("SourceIndexId"));

            //    if(targetFocusedIndexId == sourceIndexId && targetIndexId == sourceFocusedIndexId)
            //    {
            //        e.Appearance.BackColor = Color.YellowGreen;
            //    }
            //}
        }

        private void GrvRiskCorrelationsShowingEditor(object sender, CancelEventArgs e)
        {
            long targetIndexId = Convert.ToInt32(grvRiskCorrelations.FocusedColumn.FieldName.Replace("TargetIndex_", ""));
            long sourceIndexId = Convert.ToInt32(grvRiskCorrelations.GetRowCellValue(grvRiskCorrelations.FocusedRowHandle, "SourceIndexId"));

            if (targetIndexId == sourceIndexId)
                e.Cancel = true;
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}