﻿namespace Exis.WinClient.Controls
{
    partial class IndexAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IndexAEVForm));
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.SplineSeriesView splineSeriesView1 = new DevExpress.XtraCharts.SplineSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.StepLineSeriesView stepLineSeriesView1 = new DevExpress.XtraCharts.StepLineSeriesView();
            DevExpress.XtraCharts.SplineSeriesView splineSeriesView2 = new DevExpress.XtraCharts.SplineSeriesView();
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.chkIsMarketDefaultForReports = new DevExpress.XtraEditors.CheckEdit();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.chkHasSpotFormula = new DevExpress.XtraEditors.CheckEdit();
            this.txtSpotMappingName = new DevExpress.XtraEditors.TextEdit();
            this.chkIsCalculatedSpotFormula = new DevExpress.XtraEditors.CheckEdit();
            this.seCalculatedSpotOffset = new DevExpress.XtraEditors.SpinEdit();
            this.lookupCalculatedSpotIndex = new DevExpress.XtraEditors.LookUpEdit();
            this.seCoverAssocOffset = new DevExpress.XtraEditors.SpinEdit();
            this.lookupCoverAssoc = new DevExpress.XtraEditors.LookUpEdit();
            this.txtBOAMappingName = new DevExpress.XtraEditors.TextEdit();
            this.txtFFAMappingName = new DevExpress.XtraEditors.TextEdit();
            this.chkIsAssocIndex = new DevExpress.XtraEditors.CheckEdit();
            this.txtAssocOffset = new DevExpress.XtraEditors.SpinEdit();
            this.btnRemoveIndex = new DevExpress.XtraEditors.SimpleButton();
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.btnAddIndex = new DevExpress.XtraEditors.SimpleButton();
            this.txtIndexWeight_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lookupIndex_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.chkIsMarketDefault = new DevExpress.XtraEditors.CheckEdit();
            this.btnExportCurveValues = new DevExpress.XtraEditors.SimpleButton();
            this.txtValuesPercentageOffset = new DevExpress.XtraEditors.SpinEdit();
            this.txtValuesAbsoluteOffset = new DevExpress.XtraEditors.SpinEdit();
            this.chartValues = new DevExpress.XtraCharts.ChartControl();
            this.gridCurveValues = new DevExpress.XtraGrid.GridControl();
            this.gridCurveValuesMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridNormalizedBFAValues = new DevExpress.XtraGrid.GridControl();
            this.gridNormalizedBFAValuesMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridOriginalBFAValues = new DevExpress.XtraGrid.GridControl();
            this.gridOriginalBFAValuesMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dtpViewValuesDate = new DevExpress.XtraEditors.DateEdit();
            this.btnViewValues = new DevExpress.XtraEditors.SimpleButton();
            this.lookupSourceIndex = new DevExpress.XtraEditors.LookUpEdit();
            this.chkIsCustom = new DevExpress.XtraEditors.CheckEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.lookupMarket = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupIndex = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlGrpMarketDefault = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemIsAssocIndex = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgIndexAssocs = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgAssoc_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutIndex_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIndexWeight_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAssocOffset = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemCalculatedSpotFormula = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemHasSpotMappingName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutMappingName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemIsMarketDefaultForReports = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSourceIndex = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutIsCustom = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarket = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemIsMarketDefault = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupViewValues = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutViewValuesDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutOriginalBFAValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutNormalizedBFAValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCurveValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutChartValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutValuesAbsoluteOffset = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutValuesPercentageOffset = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutViewValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gridSpotValues = new DevExpress.XtraGrid.GridControl();
            this.gridSpotValuesMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutSpotValues = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsMarketDefaultForReports.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHasSpotFormula.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpotMappingName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsCalculatedSpotFormula.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seCalculatedSpotOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCalculatedSpotIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seCoverAssocOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCoverAssoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBOAMappingName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFFAMappingName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAssocIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssocOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndexWeight_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupIndex_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsMarketDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuesPercentageOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuesAbsoluteOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(splineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(stepLineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(splineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCurveValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCurveValuesMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridNormalizedBFAValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridNormalizedBFAValuesMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginalBFAValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginalBFAValuesMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpViewValuesDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpViewValuesDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSourceIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsCustom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlGrpMarketDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemIsAssocIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgIndexAssocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAssoc_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndex_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndexWeight_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAssocOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemCalculatedSpotFormula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemHasSpotMappingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMappingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemIsMarketDefaultForReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSourceIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIsCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemIsMarketDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupViewValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewValuesDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOriginalBFAValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNormalizedBFAValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCurveValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChartValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValuesAbsoluteOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValuesPercentageOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpotValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpotValuesMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSpotValues)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.gridSpotValues);
            this.layoutRootControl.Controls.Add(this.chkIsMarketDefaultForReports);
            this.layoutRootControl.Controls.Add(this.chkHasSpotFormula);
            this.layoutRootControl.Controls.Add(this.txtSpotMappingName);
            this.layoutRootControl.Controls.Add(this.chkIsCalculatedSpotFormula);
            this.layoutRootControl.Controls.Add(this.seCalculatedSpotOffset);
            this.layoutRootControl.Controls.Add(this.lookupCalculatedSpotIndex);
            this.layoutRootControl.Controls.Add(this.seCoverAssocOffset);
            this.layoutRootControl.Controls.Add(this.lookupCoverAssoc);
            this.layoutRootControl.Controls.Add(this.txtBOAMappingName);
            this.layoutRootControl.Controls.Add(this.txtFFAMappingName);
            this.layoutRootControl.Controls.Add(this.chkIsAssocIndex);
            this.layoutRootControl.Controls.Add(this.txtAssocOffset);
            this.layoutRootControl.Controls.Add(this.btnRemoveIndex);
            this.layoutRootControl.Controls.Add(this.btnAddIndex);
            this.layoutRootControl.Controls.Add(this.txtIndexWeight_1);
            this.layoutRootControl.Controls.Add(this.lookupIndex_1);
            this.layoutRootControl.Controls.Add(this.chkIsMarketDefault);
            this.layoutRootControl.Controls.Add(this.btnExportCurveValues);
            this.layoutRootControl.Controls.Add(this.txtValuesPercentageOffset);
            this.layoutRootControl.Controls.Add(this.txtValuesAbsoluteOffset);
            this.layoutRootControl.Controls.Add(this.chartValues);
            this.layoutRootControl.Controls.Add(this.gridCurveValues);
            this.layoutRootControl.Controls.Add(this.gridNormalizedBFAValues);
            this.layoutRootControl.Controls.Add(this.gridOriginalBFAValues);
            this.layoutRootControl.Controls.Add(this.dtpViewValuesDate);
            this.layoutRootControl.Controls.Add(this.btnViewValues);
            this.layoutRootControl.Controls.Add(this.lookupSourceIndex);
            this.layoutRootControl.Controls.Add(this.chkIsCustom);
            this.layoutRootControl.Controls.Add(this.txtCode);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.lookupMarket);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtName);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(250, 182, 250, 301);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(1202, 650);
            this.layoutRootControl.TabIndex = 2;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // chkIsMarketDefaultForReports
            // 
            this.chkIsMarketDefaultForReports.Location = new System.Drawing.Point(196, 264);
            this.chkIsMarketDefaultForReports.MenuManager = this.barManager;
            this.chkIsMarketDefaultForReports.Name = "chkIsMarketDefaultForReports";
            this.chkIsMarketDefaultForReports.Properties.Caption = "";
            this.chkIsMarketDefaultForReports.Size = new System.Drawing.Size(20, 19);
            this.chkIsMarketDefaultForReports.StyleController = this.layoutRootControl;
            this.chkIsMarketDefaultForReports.TabIndex = 47;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1202, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 650);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1202, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 650);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1202, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 650);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            this.imageList24.Images.SetKeyName(6, "search24x24.ico");
            // 
            // chkHasSpotFormula
            // 
            this.chkHasSpotFormula.Location = new System.Drawing.Point(397, 132);
            this.chkHasSpotFormula.MenuManager = this.barManager;
            this.chkHasSpotFormula.Name = "chkHasSpotFormula";
            this.chkHasSpotFormula.Properties.Caption = "";
            this.chkHasSpotFormula.Size = new System.Drawing.Size(53, 19);
            this.chkHasSpotFormula.StyleController = this.layoutRootControl;
            this.chkHasSpotFormula.TabIndex = 46;
            this.chkHasSpotFormula.CheckedChanged += new System.EventHandler(this.chkHasSpotFormula_CheckedChanged);
            // 
            // txtSpotMappingName
            // 
            this.txtSpotMappingName.Location = new System.Drawing.Point(379, 155);
            this.txtSpotMappingName.MenuManager = this.barManager;
            this.txtSpotMappingName.Name = "txtSpotMappingName";
            this.txtSpotMappingName.Size = new System.Drawing.Size(71, 22);
            this.txtSpotMappingName.StyleController = this.layoutRootControl;
            this.txtSpotMappingName.TabIndex = 45;
            // 
            // chkIsCalculatedSpotFormula
            // 
            this.chkIsCalculatedSpotFormula.Location = new System.Drawing.Point(1116, 132);
            this.chkIsCalculatedSpotFormula.MenuManager = this.barManager;
            this.chkIsCalculatedSpotFormula.Name = "chkIsCalculatedSpotFormula";
            this.chkIsCalculatedSpotFormula.Properties.Caption = "";
            this.chkIsCalculatedSpotFormula.Size = new System.Drawing.Size(27, 19);
            this.chkIsCalculatedSpotFormula.StyleController = this.layoutRootControl;
            this.chkIsCalculatedSpotFormula.TabIndex = 44;
            this.chkIsCalculatedSpotFormula.CheckedChanged += new System.EventHandler(this.chkIsCalculatedSpotFormula_CheckedChanged);
            // 
            // seCalculatedSpotOffset
            // 
            this.seCalculatedSpotOffset.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seCalculatedSpotOffset.Location = new System.Drawing.Point(933, 182);
            this.seCalculatedSpotOffset.MenuManager = this.barManager;
            this.seCalculatedSpotOffset.Name = "seCalculatedSpotOffset";
            this.seCalculatedSpotOffset.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.seCalculatedSpotOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seCalculatedSpotOffset.Size = new System.Drawing.Size(210, 22);
            this.seCalculatedSpotOffset.StyleController = this.layoutRootControl;
            this.seCalculatedSpotOffset.TabIndex = 43;
            // 
            // lookupCalculatedSpotIndex
            // 
            this.lookupCalculatedSpotIndex.Location = new System.Drawing.Point(930, 156);
            this.lookupCalculatedSpotIndex.MenuManager = this.barManager;
            this.lookupCalculatedSpotIndex.Name = "lookupCalculatedSpotIndex";
            this.lookupCalculatedSpotIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookupCalculatedSpotIndex.Properties.NullText = "";
            this.lookupCalculatedSpotIndex.Size = new System.Drawing.Size(213, 22);
            this.lookupCalculatedSpotIndex.StyleController = this.layoutRootControl;
            this.lookupCalculatedSpotIndex.TabIndex = 42;
            this.lookupCalculatedSpotIndex.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookupCalculatedSpotIndex_ButtonClick);
            // 
            // seCoverAssocOffset
            // 
            this.seCoverAssocOffset.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seCoverAssocOffset.Location = new System.Drawing.Point(68, 313);
            this.seCoverAssocOffset.MenuManager = this.barManager;
            this.seCoverAssocOffset.Name = "seCoverAssocOffset";
            this.seCoverAssocOffset.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.seCoverAssocOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seCoverAssocOffset.Size = new System.Drawing.Size(148, 22);
            this.seCoverAssocOffset.StyleController = this.layoutRootControl;
            this.seCoverAssocOffset.TabIndex = 41;
            // 
            // lookupCoverAssoc
            // 
            this.lookupCoverAssoc.Location = new System.Drawing.Point(65, 287);
            this.lookupCoverAssoc.MenuManager = this.barManager;
            this.lookupCoverAssoc.Name = "lookupCoverAssoc";
            this.lookupCoverAssoc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookupCoverAssoc.Properties.NullText = "";
            this.lookupCoverAssoc.Size = new System.Drawing.Size(151, 22);
            this.lookupCoverAssoc.StyleController = this.layoutRootControl;
            this.lookupCoverAssoc.TabIndex = 40;
            this.lookupCoverAssoc.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookupCoverAssoc_ButtonClick);
            // 
            // txtBOAMappingName
            // 
            this.txtBOAMappingName.Location = new System.Drawing.Point(147, 192);
            this.txtBOAMappingName.MenuManager = this.barManager;
            this.txtBOAMappingName.Name = "txtBOAMappingName";
            this.txtBOAMappingName.Size = new System.Drawing.Size(69, 22);
            this.txtBOAMappingName.StyleController = this.layoutRootControl;
            this.txtBOAMappingName.TabIndex = 39;
            // 
            // txtFFAMappingName
            // 
            this.txtFFAMappingName.Location = new System.Drawing.Point(145, 120);
            this.txtFFAMappingName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFFAMappingName.MenuManager = this.barManager;
            this.txtFFAMappingName.Name = "txtFFAMappingName";
            this.txtFFAMappingName.Size = new System.Drawing.Size(71, 22);
            this.txtFFAMappingName.StyleController = this.layoutRootControl;
            this.txtFFAMappingName.TabIndex = 38;
            // 
            // chkIsAssocIndex
            // 
            this.chkIsAssocIndex.Location = new System.Drawing.Point(570, 132);
            this.chkIsAssocIndex.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkIsAssocIndex.MenuManager = this.barManager;
            this.chkIsAssocIndex.Name = "chkIsAssocIndex";
            this.chkIsAssocIndex.Properties.Caption = "";
            this.chkIsAssocIndex.Size = new System.Drawing.Size(293, 19);
            this.chkIsAssocIndex.StyleController = this.layoutRootControl;
            this.chkIsAssocIndex.TabIndex = 37;
            this.chkIsAssocIndex.CheckedChanged += new System.EventHandler(this.ChkIsAssocIndexCheckedChanged);
            // 
            // txtAssocOffset
            // 
            this.txtAssocOffset.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAssocOffset.Location = new System.Drawing.Point(520, 223);
            this.txtAssocOffset.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAssocOffset.MenuManager = this.barManager;
            this.txtAssocOffset.Name = "txtAssocOffset";
            this.txtAssocOffset.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtAssocOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAssocOffset.Properties.Mask.EditMask = "D";
            this.txtAssocOffset.Properties.MaxLength = 8;
            this.txtAssocOffset.Size = new System.Drawing.Size(160, 22);
            this.txtAssocOffset.StyleController = this.layoutRootControl;
            this.txtAssocOffset.TabIndex = 36;
            // 
            // btnRemoveIndex
            // 
            this.btnRemoveIndex.ImageIndex = 1;
            this.btnRemoveIndex.ImageList = this.imageList16;
            this.btnRemoveIndex.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveIndex.Location = new System.Drawing.Point(833, 189);
            this.btnRemoveIndex.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRemoveIndex.Name = "btnRemoveIndex";
            this.btnRemoveIndex.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveIndex.StyleController = this.layoutRootControl;
            this.btnRemoveIndex.TabIndex = 12;
            this.btnRemoveIndex.Click += new System.EventHandler(this.BtnRemoveIndexClick);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList16.Images.SetKeyName(1, "clear16x16.ico");
            // 
            // btnAddIndex
            // 
            this.btnAddIndex.ImageIndex = 0;
            this.btnAddIndex.ImageList = this.imageList16;
            this.btnAddIndex.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddIndex.Location = new System.Drawing.Point(833, 155);
            this.btnAddIndex.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddIndex.Name = "btnAddIndex";
            this.btnAddIndex.Size = new System.Drawing.Size(30, 30);
            this.btnAddIndex.StyleController = this.layoutRootControl;
            this.btnAddIndex.TabIndex = 11;
            this.btnAddIndex.Click += new System.EventHandler(this.BtnAddIndexClick);
            // 
            // txtIndexWeight_1
            // 
            this.txtIndexWeight_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtIndexWeight_1.Location = new System.Drawing.Point(733, 180);
            this.txtIndexWeight_1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIndexWeight_1.MaximumSize = new System.Drawing.Size(93, 20);
            this.txtIndexWeight_1.MinimumSize = new System.Drawing.Size(93, 20);
            this.txtIndexWeight_1.Name = "txtIndexWeight_1";
            this.txtIndexWeight_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtIndexWeight_1.Properties.Mask.EditMask = "P2";
            this.txtIndexWeight_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtIndexWeight_1.Properties.MaxLength = 6;
            this.txtIndexWeight_1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtIndexWeight_1.Size = new System.Drawing.Size(93, 22);
            this.txtIndexWeight_1.StyleController = this.layoutRootControl;
            this.txtIndexWeight_1.TabIndex = 8;
            // 
            // lookupIndex_1
            // 
            this.lookupIndex_1.Location = new System.Drawing.Point(520, 180);
            this.lookupIndex_1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupIndex_1.Name = "lookupIndex_1";
            this.lookupIndex_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupIndex_1.Properties.NullText = "";
            this.lookupIndex_1.Size = new System.Drawing.Size(161, 22);
            this.lookupIndex_1.StyleController = this.layoutRootControl;
            this.lookupIndex_1.TabIndex = 10;
            // 
            // chkIsMarketDefault
            // 
            this.chkIsMarketDefault.Location = new System.Drawing.Point(349, 60);
            this.chkIsMarketDefault.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkIsMarketDefault.Name = "chkIsMarketDefault";
            this.chkIsMarketDefault.Properties.Caption = "";
            this.chkIsMarketDefault.Size = new System.Drawing.Size(21, 19);
            this.chkIsMarketDefault.StyleController = this.layoutRootControl;
            this.chkIsMarketDefault.TabIndex = 32;
            // 
            // btnExportCurveValues
            // 
            this.btnExportCurveValues.Location = new System.Drawing.Point(718, 397);
            this.btnExportCurveValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExportCurveValues.Name = "btnExportCurveValues";
            this.btnExportCurveValues.Size = new System.Drawing.Size(143, 23);
            this.btnExportCurveValues.StyleController = this.layoutRootControl;
            this.btnExportCurveValues.TabIndex = 35;
            this.btnExportCurveValues.Text = "Export Curve Values";
            this.btnExportCurveValues.Click += new System.EventHandler(this.BtnExportCurveValuesClick);
            // 
            // txtValuesPercentageOffset
            // 
            this.txtValuesPercentageOffset.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtValuesPercentageOffset.Location = new System.Drawing.Point(530, 397);
            this.txtValuesPercentageOffset.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtValuesPercentageOffset.Name = "txtValuesPercentageOffset";
            this.txtValuesPercentageOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtValuesPercentageOffset.Properties.Mask.EditMask = "D";
            this.txtValuesPercentageOffset.Properties.MaxLength = 3;
            this.txtValuesPercentageOffset.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtValuesPercentageOffset.Properties.MinValue = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.txtValuesPercentageOffset.Size = new System.Drawing.Size(80, 22);
            this.txtValuesPercentageOffset.StyleController = this.layoutRootControl;
            this.txtValuesPercentageOffset.TabIndex = 10;
            // 
            // txtValuesAbsoluteOffset
            // 
            this.txtValuesAbsoluteOffset.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtValuesAbsoluteOffset.Location = new System.Drawing.Point(303, 397);
            this.txtValuesAbsoluteOffset.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtValuesAbsoluteOffset.Name = "txtValuesAbsoluteOffset";
            this.txtValuesAbsoluteOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtValuesAbsoluteOffset.Properties.Mask.EditMask = "D";
            this.txtValuesAbsoluteOffset.Properties.MaxLength = 5;
            this.txtValuesAbsoluteOffset.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtValuesAbsoluteOffset.Properties.MinValue = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.txtValuesAbsoluteOffset.Size = new System.Drawing.Size(113, 22);
            this.txtValuesAbsoluteOffset.StyleController = this.layoutRootControl;
            this.txtValuesAbsoluteOffset.TabIndex = 10;
            // 
            // chartValues
            // 
            xyDiagram1.AxisX.DateTimeScaleOptions.AutoGrid = false;
            xyDiagram1.AxisX.DateTimeScaleOptions.GridAlignment = DevExpress.XtraCharts.DateTimeGridAlignment.Month;
            xyDiagram1.AxisX.Label.DateTimeOptions.AutoFormat = false;
            xyDiagram1.AxisX.Label.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            xyDiagram1.AxisX.Label.DateTimeOptions.FormatString = "MMM-yy";
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.WholeRange.AlwaysShowZeroLevel = false;
            xyDiagram1.AxisY.WholeRange.AutoSideMargins = true;
            xyDiagram1.EnableAxisXScrolling = true;
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisYScrolling = true;
            xyDiagram1.EnableAxisYZooming = true;
            xyDiagram1.ScrollingOptions.UseMouse = false;
            xyDiagram1.ScrollingOptions.UseTouchDevice = false;
            xyDiagram1.ZoomingOptions.UseKeyboardWithMouse = false;
            this.chartValues.Diagram = xyDiagram1;
            this.chartValues.Legend.Antialiasing = true;
            this.chartValues.Legend.Shadow.Visible = true;
            this.chartValues.Legend.Visible = false;
            this.chartValues.Location = new System.Drawing.Point(14, 645);
            this.chartValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chartValues.Name = "chartValues";
            this.chartValues.RuntimeSeriesSelectionMode = DevExpress.XtraCharts.SeriesSelectionMode.Point;
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            series1.Label = pointSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series1.LegendText = "Curve";
            series1.Name = "seriesCurve";
            splineSeriesView1.LineStyle.Thickness = 3;
            splineSeriesView1.MarkerVisibility = DevExpress.Utils.DefaultBoolean.False;
            series1.View = splineSeriesView1;
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series2.LegendText = "Norm. BFA";
            series2.Name = "seriesNormalizedBFA";
            stepLineSeriesView1.InvertedStep = true;
            series2.View = stepLineSeriesView1;
            this.chartValues.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            splineSeriesView2.LineStyle.Thickness = 1;
            this.chartValues.SeriesTemplate.View = splineSeriesView2;
            this.chartValues.Size = new System.Drawing.Size(1153, 200);
            this.chartValues.TabIndex = 34;
            this.chartValues.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartValues_MouseDown);
            this.chartValues.MouseLeave += new System.EventHandler(this.chartValues_MouseLeave);
            this.chartValues.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartValues_MouseMove);
            this.chartValues.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartValues_MouseUp);
            // 
            // gridCurveValues
            // 
            this.gridCurveValues.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridCurveValues.Location = new System.Drawing.Point(693, 443);
            this.gridCurveValues.MainView = this.gridCurveValuesMainView;
            this.gridCurveValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridCurveValues.Name = "gridCurveValues";
            this.gridCurveValues.Size = new System.Drawing.Size(474, 198);
            this.gridCurveValues.TabIndex = 12;
            this.gridCurveValues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridCurveValuesMainView});
            // 
            // gridCurveValuesMainView
            // 
            this.gridCurveValuesMainView.GridControl = this.gridCurveValues;
            this.gridCurveValuesMainView.Name = "gridCurveValuesMainView";
            this.gridCurveValuesMainView.OptionsBehavior.Editable = false;
            this.gridCurveValuesMainView.OptionsCustomization.AllowGroup = false;
            this.gridCurveValuesMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridCurveValuesMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridCurveValuesMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridCurveValuesMainView.OptionsView.ShowDetailButtons = false;
            this.gridCurveValuesMainView.OptionsView.ShowGroupPanel = false;
            this.gridCurveValuesMainView.OptionsView.ShowIndicator = false;
            // 
            // gridNormalizedBFAValues
            // 
            this.gridNormalizedBFAValues.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridNormalizedBFAValues.Location = new System.Drawing.Point(448, 443);
            this.gridNormalizedBFAValues.MainView = this.gridNormalizedBFAValuesMainView;
            this.gridNormalizedBFAValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridNormalizedBFAValues.Name = "gridNormalizedBFAValues";
            this.gridNormalizedBFAValues.Size = new System.Drawing.Size(241, 198);
            this.gridNormalizedBFAValues.TabIndex = 11;
            this.gridNormalizedBFAValues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridNormalizedBFAValuesMainView});
            // 
            // gridNormalizedBFAValuesMainView
            // 
            this.gridNormalizedBFAValuesMainView.GridControl = this.gridNormalizedBFAValues;
            this.gridNormalizedBFAValuesMainView.Name = "gridNormalizedBFAValuesMainView";
            this.gridNormalizedBFAValuesMainView.OptionsBehavior.Editable = false;
            this.gridNormalizedBFAValuesMainView.OptionsCustomization.AllowGroup = false;
            this.gridNormalizedBFAValuesMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridNormalizedBFAValuesMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridNormalizedBFAValuesMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridNormalizedBFAValuesMainView.OptionsView.ShowDetailButtons = false;
            this.gridNormalizedBFAValuesMainView.OptionsView.ShowGroupPanel = false;
            this.gridNormalizedBFAValuesMainView.OptionsView.ShowIndicator = false;
            // 
            // gridOriginalBFAValues
            // 
            this.gridOriginalBFAValues.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridOriginalBFAValues.Location = new System.Drawing.Point(242, 443);
            this.gridOriginalBFAValues.MainView = this.gridOriginalBFAValuesMainView;
            this.gridOriginalBFAValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridOriginalBFAValues.Name = "gridOriginalBFAValues";
            this.gridOriginalBFAValues.Size = new System.Drawing.Size(202, 198);
            this.gridOriginalBFAValues.TabIndex = 10;
            this.gridOriginalBFAValues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridOriginalBFAValuesMainView});
            // 
            // gridOriginalBFAValuesMainView
            // 
            this.gridOriginalBFAValuesMainView.GridControl = this.gridOriginalBFAValues;
            this.gridOriginalBFAValuesMainView.Name = "gridOriginalBFAValuesMainView";
            this.gridOriginalBFAValuesMainView.OptionsBehavior.Editable = false;
            this.gridOriginalBFAValuesMainView.OptionsCustomization.AllowGroup = false;
            this.gridOriginalBFAValuesMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridOriginalBFAValuesMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridOriginalBFAValuesMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridOriginalBFAValuesMainView.OptionsView.ShowDetailButtons = false;
            this.gridOriginalBFAValuesMainView.OptionsView.ShowGroupPanel = false;
            this.gridOriginalBFAValuesMainView.OptionsView.ShowIndicator = false;
            // 
            // dtpViewValuesDate
            // 
            this.dtpViewValuesDate.EditValue = null;
            this.dtpViewValuesDate.Location = new System.Drawing.Point(48, 397);
            this.dtpViewValuesDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpViewValuesDate.Name = "dtpViewValuesDate";
            this.dtpViewValuesDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpViewValuesDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpViewValuesDate.Size = new System.Drawing.Size(156, 22);
            this.dtpViewValuesDate.StyleController = this.layoutRootControl;
            this.dtpViewValuesDate.TabIndex = 33;
            // 
            // btnViewValues
            // 
            this.btnViewValues.Location = new System.Drawing.Point(614, 397);
            this.btnViewValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnViewValues.Name = "btnViewValues";
            this.btnViewValues.Size = new System.Drawing.Size(100, 22);
            this.btnViewValues.StyleController = this.layoutRootControl;
            this.btnViewValues.TabIndex = 34;
            this.btnViewValues.Text = "View Values";
            this.btnViewValues.Click += new System.EventHandler(this.btnViewValues_Click);
            // 
            // lookupSourceIndex
            // 
            this.lookupSourceIndex.Enabled = false;
            this.lookupSourceIndex.Location = new System.Drawing.Point(697, 60);
            this.lookupSourceIndex.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupSourceIndex.Name = "lookupSourceIndex";
            this.lookupSourceIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupSourceIndex.Properties.NullText = "";
            this.lookupSourceIndex.Size = new System.Drawing.Size(50, 22);
            this.lookupSourceIndex.StyleController = this.layoutRootControl;
            this.lookupSourceIndex.TabIndex = 21;
            // 
            // chkIsCustom
            // 
            this.chkIsCustom.Location = new System.Drawing.Point(589, 60);
            this.chkIsCustom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkIsCustom.Name = "chkIsCustom";
            this.chkIsCustom.Properties.Caption = "";
            this.chkIsCustom.Size = new System.Drawing.Size(21, 19);
            this.chkIsCustom.StyleController = this.layoutRootControl;
            this.chkIsCustom.TabIndex = 31;
            this.chkIsCustom.CheckedChanged += new System.EventHandler(this.chkIsCustom_CheckedChanged);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(153, 36);
            this.txtCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.MaxLength = 1000;
            this.txtCode.Size = new System.Drawing.Size(137, 22);
            this.txtCode.StyleController = this.layoutRootControl;
            this.txtCode.TabIndex = 10;
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(1076, 895);
            this.dtpUpdateDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Size = new System.Drawing.Size(91, 22);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 8;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(654, 895);
            this.txtUpdateUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(340, 22);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 11;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(483, 895);
            this.dtpCreationDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Size = new System.Drawing.Size(89, 22);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 7;
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(100, 895);
            this.txtCreationUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(293, 22);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 10;
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(33, 36);
            this.lblId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(79, 20);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // lookupMarket
            // 
            this.lookupMarket.Location = new System.Drawing.Point(61, 60);
            this.lookupMarket.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupMarket.Name = "lookupMarket";
            this.lookupMarket.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMarket.Properties.NullText = "";
            this.lookupMarket.Size = new System.Drawing.Size(177, 22);
            this.lookupMarket.StyleController = this.layoutRootControl;
            this.lookupMarket.TabIndex = 20;
            this.lookupMarket.EditValueChanged += new System.EventHandler(this.lookupMarket_EditValueChanged);
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(1072, 36);
            this.cmbStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(95, 22);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(335, 36);
            this.txtName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtName.Name = "txtName";
            this.txtName.Properties.MaxLength = 1000;
            this.txtName.Size = new System.Drawing.Size(689, 22);
            this.txtName.StyleController = this.layoutRootControl;
            this.txtName.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupIndex,
            this.layoutGroupUserInfo,
            this.layoutGroupViewValues});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(1181, 931);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupIndex
            // 
            this.layoutGroupIndex.CustomizationFormText = "Company";
            this.layoutGroupIndex.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutId,
            this.layoutStatus,
            this.layoutCode,
            this.layCtlGrpMarketDefault,
            this.layoutSourceIndex,
            this.emptySpaceItem4,
            this.layoutIsCustom,
            this.layoutMarket,
            this.layCtlItemIsMarketDefault});
            this.layoutGroupIndex.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupIndex.Name = "layoutGroupIndex";
            this.layoutGroupIndex.Size = new System.Drawing.Size(1181, 361);
            this.layoutGroupIndex.Text = "Index";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "Name:";
            this.layoutName.Location = new System.Drawing.Point(280, 0);
            this.layoutName.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutName.MinSize = new System.Drawing.Size(139, 24);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(734, 24);
            this.layoutName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(38, 16);
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MaxSize = new System.Drawing.Size(102, 24);
            this.layoutId.MinSize = new System.Drawing.Size(102, 24);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(102, 24);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(16, 16);
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(1014, 0);
            this.layoutStatus.MaxSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.MinSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(143, 24);
            this.layoutStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextSize = new System.Drawing.Size(41, 16);
            // 
            // layoutCode
            // 
            this.layoutCode.Control = this.txtCode;
            this.layoutCode.CustomizationFormText = "Code:";
            this.layoutCode.Location = new System.Drawing.Point(102, 0);
            this.layoutCode.MaxSize = new System.Drawing.Size(237, 24);
            this.layoutCode.MinSize = new System.Drawing.Size(137, 24);
            this.layoutCode.Name = "layoutCode";
            this.layoutCode.Size = new System.Drawing.Size(178, 24);
            this.layoutCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCode.Text = "Code:";
            this.layoutCode.TextSize = new System.Drawing.Size(34, 16);
            // 
            // layCtlGrpMarketDefault
            // 
            this.layCtlGrpMarketDefault.CustomizationFormText = "Market Default";
            this.layCtlGrpMarketDefault.GroupBordersVisible = false;
            this.layCtlGrpMarketDefault.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layCtlGrpMarketDefault.Location = new System.Drawing.Point(0, 50);
            this.layCtlGrpMarketDefault.Name = "layCtlGrpMarketDefault";
            this.layCtlGrpMarketDefault.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layCtlGrpMarketDefault.Size = new System.Drawing.Size(1157, 265);
            this.layCtlGrpMarketDefault.Text = "Market Default";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Spot Values";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(218, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(939, 265);
            this.layoutControlGroup1.Text = "Spot Values";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemIsAssocIndex,
            this.lcgIndexAssocs,
            this.layoutAssocOffset,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(222, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(413, 219);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layCtlItemIsAssocIndex
            // 
            this.layCtlItemIsAssocIndex.Control = this.chkIsAssocIndex;
            this.layCtlItemIsAssocIndex.CustomizationFormText = "Is Assoc Index:";
            this.layCtlItemIsAssocIndex.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemIsAssocIndex.MaxSize = new System.Drawing.Size(0, 23);
            this.layCtlItemIsAssocIndex.MinSize = new System.Drawing.Size(107, 23);
            this.layCtlItemIsAssocIndex.Name = "layCtlItemIsAssocIndex";
            this.layCtlItemIsAssocIndex.Size = new System.Drawing.Size(389, 23);
            this.layCtlItemIsAssocIndex.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemIsAssocIndex.Text = "Is Assoc Index:";
            this.layCtlItemIsAssocIndex.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layCtlItemIsAssocIndex.TextSize = new System.Drawing.Size(87, 16);
            this.layCtlItemIsAssocIndex.TextToControlDistance = 5;
            // 
            // lcgIndexAssocs
            // 
            this.lcgIndexAssocs.CustomizationFormText = "Index Associations";
            this.lcgIndexAssocs.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgAssoc_1});
            this.lcgIndexAssocs.Location = new System.Drawing.Point(0, 23);
            this.lcgIndexAssocs.Name = "lcgIndexAssocs";
            this.lcgIndexAssocs.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgIndexAssocs.Size = new System.Drawing.Size(355, 68);
            this.lcgIndexAssocs.Text = "Index Associations";
            // 
            // lcgAssoc_1
            // 
            this.lcgAssoc_1.CustomizationFormText = "lcgAssoc_1";
            this.lcgAssoc_1.GroupBordersVisible = false;
            this.lcgAssoc_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutIndex_1,
            this.layoutIndexWeight_1});
            this.lcgAssoc_1.Location = new System.Drawing.Point(0, 0);
            this.lcgAssoc_1.Name = "lcgAssoc_1";
            this.lcgAssoc_1.Size = new System.Drawing.Size(349, 40);
            this.lcgAssoc_1.Text = "lcgAssoc_1";
            // 
            // layoutIndex_1
            // 
            this.layoutIndex_1.Control = this.lookupIndex_1;
            this.layoutIndex_1.CustomizationFormText = "Index:";
            this.layoutIndex_1.Location = new System.Drawing.Point(0, 0);
            this.layoutIndex_1.Name = "layoutIndex_1";
            this.layoutIndex_1.Size = new System.Drawing.Size(204, 40);
            this.layoutIndex_1.Text = "Index:";
            this.layoutIndex_1.TextSize = new System.Drawing.Size(36, 16);
            // 
            // layoutIndexWeight_1
            // 
            this.layoutIndexWeight_1.Control = this.txtIndexWeight_1;
            this.layoutIndexWeight_1.CustomizationFormText = "Weight:";
            this.layoutIndexWeight_1.Location = new System.Drawing.Point(204, 0);
            this.layoutIndexWeight_1.Name = "layoutIndexWeight_1";
            this.layoutIndexWeight_1.Size = new System.Drawing.Size(145, 40);
            this.layoutIndexWeight_1.Text = "Weight:";
            this.layoutIndexWeight_1.TextSize = new System.Drawing.Size(45, 16);
            // 
            // layoutAssocOffset
            // 
            this.layoutAssocOffset.Control = this.txtAssocOffset;
            this.layoutAssocOffset.CustomizationFormText = "Offset:";
            this.layoutAssocOffset.Location = new System.Drawing.Point(0, 91);
            this.layoutAssocOffset.Name = "layoutAssocOffset";
            this.layoutAssocOffset.Size = new System.Drawing.Size(206, 104);
            this.layoutAssocOffset.Text = "Offset:";
            this.layoutAssocOffset.TextSize = new System.Drawing.Size(39, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnAddIndex;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(355, 23);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(34, 34);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnRemoveIndex;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(355, 57);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(34, 34);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(206, 91);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(183, 104);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layCtlItemCalculatedSpotFormula});
            this.layoutControlGroup7.Location = new System.Drawing.Point(635, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(280, 219);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lookupCalculatedSpotIndex;
            this.layoutControlItem7.CustomizationFormText = "Index:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(256, 26);
            this.layoutControlItem7.Text = "Index:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(36, 16);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.seCalculatedSpotOffset;
            this.layoutControlItem8.CustomizationFormText = "Offset:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(256, 145);
            this.layoutControlItem8.Text = "Offset:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(39, 16);
            // 
            // layCtlItemCalculatedSpotFormula
            // 
            this.layCtlItemCalculatedSpotFormula.Control = this.chkIsCalculatedSpotFormula;
            this.layCtlItemCalculatedSpotFormula.CustomizationFormText = "Calculated Spot Formula from derived:";
            this.layCtlItemCalculatedSpotFormula.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemCalculatedSpotFormula.MaxSize = new System.Drawing.Size(0, 24);
            this.layCtlItemCalculatedSpotFormula.MinSize = new System.Drawing.Size(86, 24);
            this.layCtlItemCalculatedSpotFormula.Name = "layCtlItemCalculatedSpotFormula";
            this.layCtlItemCalculatedSpotFormula.Size = new System.Drawing.Size(256, 24);
            this.layCtlItemCalculatedSpotFormula.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemCalculatedSpotFormula.Text = "Calculated Spot Formula from derived:";
            this.layCtlItemCalculatedSpotFormula.TextSize = new System.Drawing.Size(222, 16);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layCtlItemHasSpotMappingName});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(222, 219);
            this.layoutControlGroup8.Text = "layoutControlGroup8";
            this.layoutControlGroup8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtSpotMappingName;
            this.layoutControlItem9.CustomizationFormText = "Spot Mapping Name:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(198, 172);
            this.layoutControlItem9.Text = "Spot Mapping Name:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(120, 16);
            // 
            // layCtlItemHasSpotMappingName
            // 
            this.layCtlItemHasSpotMappingName.Control = this.chkHasSpotFormula;
            this.layCtlItemHasSpotMappingName.CustomizationFormText = "Standard Spot Formula:";
            this.layCtlItemHasSpotMappingName.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemHasSpotMappingName.Name = "layCtlItemHasSpotMappingName";
            this.layCtlItemHasSpotMappingName.Size = new System.Drawing.Size(198, 23);
            this.layCtlItemHasSpotMappingName.Text = "Standard Spot Formula:";
            this.layCtlItemHasSpotMappingName.TextSize = new System.Drawing.Size(138, 16);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "FFA Values";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutMappingName});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(218, 72);
            this.layoutControlGroup2.Text = "FFA Values";
            // 
            // layoutMappingName
            // 
            this.layoutMappingName.Control = this.txtFFAMappingName;
            this.layoutMappingName.CustomizationFormText = "Mapping Name:";
            this.layoutMappingName.Location = new System.Drawing.Point(0, 0);
            this.layoutMappingName.Name = "layoutMappingName";
            this.layoutMappingName.Size = new System.Drawing.Size(194, 26);
            this.layoutMappingName.Text = "FFA Mapping Name:";
            this.layoutMappingName.TextSize = new System.Drawing.Size(116, 16);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "BOA Values";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(218, 72);
            this.layoutControlGroup3.Text = "BOA Values";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtBOAMappingName;
            this.layoutControlItem4.CustomizationFormText = "BOA Mapping Name:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(194, 26);
            this.layoutControlItem4.Text = "BOA Mapping Name:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Cover Report Index Equivalent";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layCtlItemIsMarketDefaultForReports});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 144);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(218, 121);
            this.layoutControlGroup4.Text = "Cover Report Index Equivalent";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lookupCoverAssoc;
            this.layoutControlItem5.CustomizationFormText = "Index:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(194, 26);
            this.layoutControlItem5.Text = "Index:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(36, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.seCoverAssocOffset;
            this.layoutControlItem6.CustomizationFormText = "Offset:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(194, 26);
            this.layoutControlItem6.Text = "Offset:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(39, 16);
            // 
            // layCtlItemIsMarketDefaultForReports
            // 
            this.layCtlItemIsMarketDefaultForReports.Control = this.chkIsMarketDefaultForReports;
            this.layCtlItemIsMarketDefaultForReports.CustomizationFormText = "Is Market Default for reports:";
            this.layCtlItemIsMarketDefaultForReports.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemIsMarketDefaultForReports.Name = "layCtlItemIsMarketDefaultForReports";
            this.layCtlItemIsMarketDefaultForReports.Size = new System.Drawing.Size(194, 23);
            this.layCtlItemIsMarketDefaultForReports.Text = "Is Market Default for reports:";
            this.layCtlItemIsMarketDefaultForReports.TextSize = new System.Drawing.Size(167, 16);
            // 
            // layoutSourceIndex
            // 
            this.layoutSourceIndex.Control = this.lookupSourceIndex;
            this.layoutSourceIndex.CustomizationFormText = "Source Index:";
            this.layoutSourceIndex.Location = new System.Drawing.Point(600, 24);
            this.layoutSourceIndex.Name = "layoutSourceIndex";
            this.layoutSourceIndex.Size = new System.Drawing.Size(137, 26);
            this.layoutSourceIndex.Text = "Source Index:";
            this.layoutSourceIndex.TextSize = new System.Drawing.Size(80, 16);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(737, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(420, 26);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutIsCustom
            // 
            this.layoutIsCustom.Control = this.chkIsCustom;
            this.layoutIsCustom.CustomizationFormText = "Is Custom Based on another index:";
            this.layoutIsCustom.Location = new System.Drawing.Point(360, 24);
            this.layoutIsCustom.MinSize = new System.Drawing.Size(83, 23);
            this.layoutIsCustom.Name = "layoutIsCustom";
            this.layoutIsCustom.Size = new System.Drawing.Size(240, 26);
            this.layoutIsCustom.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutIsCustom.Text = "Is Custom (based on another index):";
            this.layoutIsCustom.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutIsCustom.TextSize = new System.Drawing.Size(210, 16);
            this.layoutIsCustom.TextToControlDistance = 5;
            // 
            // layoutMarket
            // 
            this.layoutMarket.Control = this.lookupMarket;
            this.layoutMarket.CustomizationFormText = "Profit Centre:";
            this.layoutMarket.Location = new System.Drawing.Point(0, 24);
            this.layoutMarket.Name = "layoutMarket";
            this.layoutMarket.Size = new System.Drawing.Size(228, 26);
            this.layoutMarket.Text = "Market:";
            this.layoutMarket.TextSize = new System.Drawing.Size(44, 16);
            // 
            // layCtlItemIsMarketDefault
            // 
            this.layCtlItemIsMarketDefault.Control = this.chkIsMarketDefault;
            this.layCtlItemIsMarketDefault.CustomizationFormText = "Is Market Default:";
            this.layCtlItemIsMarketDefault.Location = new System.Drawing.Point(228, 24);
            this.layCtlItemIsMarketDefault.MaxSize = new System.Drawing.Size(0, 23);
            this.layCtlItemIsMarketDefault.MinSize = new System.Drawing.Size(119, 23);
            this.layCtlItemIsMarketDefault.Name = "layCtlItemIsMarketDefault";
            this.layCtlItemIsMarketDefault.Size = new System.Drawing.Size(132, 26);
            this.layCtlItemIsMarketDefault.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layCtlItemIsMarketDefault.Text = "Is Market Default:";
            this.layCtlItemIsMarketDefault.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layCtlItemIsMarketDefault.TextSize = new System.Drawing.Size(102, 16);
            this.layCtlItemIsMarketDefault.TextToControlDistance = 5;
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "User Info";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 859);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(1181, 72);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "Creation User:";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(383, 26);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(83, 16);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(383, 0);
            this.layoutCreationDate.MaxSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.MinSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(179, 26);
            this.layoutCreationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(83, 16);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(562, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(422, 26);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(984, 0);
            this.layoutUpdateDate.MaxSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.MinSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(173, 26);
            this.layoutUpdateDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutGroupViewValues
            // 
            this.layoutGroupViewValues.CustomizationFormText = "Values";
            this.layoutGroupViewValues.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewValuesDate,
            this.emptySpaceItem1,
            this.layoutOriginalBFAValues,
            this.layoutNormalizedBFAValues,
            this.layoutCurveValues,
            this.layoutChartValues,
            this.layoutValuesAbsoluteOffset,
            this.layoutValuesPercentageOffset,
            this.layoutViewValues,
            this.layoutControlItem1,
            this.layoutSpotValues});
            this.layoutGroupViewValues.Location = new System.Drawing.Point(0, 361);
            this.layoutGroupViewValues.Name = "layoutGroupViewValues";
            this.layoutGroupViewValues.Size = new System.Drawing.Size(1181, 498);
            this.layoutGroupViewValues.Text = "Values";
            // 
            // layoutViewValuesDate
            // 
            this.layoutViewValuesDate.Control = this.dtpViewValuesDate;
            this.layoutViewValuesDate.CustomizationFormText = "Date:";
            this.layoutViewValuesDate.Location = new System.Drawing.Point(0, 0);
            this.layoutViewValuesDate.Name = "layoutViewValuesDate";
            this.layoutViewValuesDate.Size = new System.Drawing.Size(194, 27);
            this.layoutViewValuesDate.Text = "Date:";
            this.layoutViewValuesDate.TextSize = new System.Drawing.Size(31, 16);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(851, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(306, 27);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutOriginalBFAValues
            // 
            this.layoutOriginalBFAValues.Control = this.gridOriginalBFAValues;
            this.layoutOriginalBFAValues.CustomizationFormText = "Original BFA Values:";
            this.layoutOriginalBFAValues.Location = new System.Drawing.Point(228, 27);
            this.layoutOriginalBFAValues.MinSize = new System.Drawing.Size(104, 221);
            this.layoutOriginalBFAValues.Name = "layoutOriginalBFAValues";
            this.layoutOriginalBFAValues.Size = new System.Drawing.Size(206, 221);
            this.layoutOriginalBFAValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOriginalBFAValues.Text = "Original BFA Values:";
            this.layoutOriginalBFAValues.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutOriginalBFAValues.TextSize = new System.Drawing.Size(117, 16);
            // 
            // layoutNormalizedBFAValues
            // 
            this.layoutNormalizedBFAValues.Control = this.gridNormalizedBFAValues;
            this.layoutNormalizedBFAValues.CustomizationFormText = "Normalized BFA Values:";
            this.layoutNormalizedBFAValues.Location = new System.Drawing.Point(434, 27);
            this.layoutNormalizedBFAValues.Name = "layoutNormalizedBFAValues";
            this.layoutNormalizedBFAValues.Size = new System.Drawing.Size(245, 221);
            this.layoutNormalizedBFAValues.Text = "Normalized BFA Values:";
            this.layoutNormalizedBFAValues.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutNormalizedBFAValues.TextSize = new System.Drawing.Size(137, 16);
            // 
            // layoutCurveValues
            // 
            this.layoutCurveValues.Control = this.gridCurveValues;
            this.layoutCurveValues.CustomizationFormText = "Curve Values:";
            this.layoutCurveValues.Location = new System.Drawing.Point(679, 27);
            this.layoutCurveValues.Name = "layoutCurveValues";
            this.layoutCurveValues.Size = new System.Drawing.Size(478, 221);
            this.layoutCurveValues.Text = "Curve Values:";
            this.layoutCurveValues.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutCurveValues.TextSize = new System.Drawing.Size(80, 16);
            // 
            // layoutChartValues
            // 
            this.layoutChartValues.Control = this.chartValues;
            this.layoutChartValues.CustomizationFormText = "layoutChartValues";
            this.layoutChartValues.Location = new System.Drawing.Point(0, 248);
            this.layoutChartValues.MinSize = new System.Drawing.Size(196, 204);
            this.layoutChartValues.Name = "layoutChartValues";
            this.layoutChartValues.Size = new System.Drawing.Size(1157, 204);
            this.layoutChartValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutChartValues.Text = "layoutChartValues";
            this.layoutChartValues.TextSize = new System.Drawing.Size(0, 0);
            this.layoutChartValues.TextToControlDistance = 0;
            this.layoutChartValues.TextVisible = false;
            // 
            // layoutValuesAbsoluteOffset
            // 
            this.layoutValuesAbsoluteOffset.Control = this.txtValuesAbsoluteOffset;
            this.layoutValuesAbsoluteOffset.CustomizationFormText = "Absolute Offset:";
            this.layoutValuesAbsoluteOffset.Location = new System.Drawing.Point(194, 0);
            this.layoutValuesAbsoluteOffset.Name = "layoutValuesAbsoluteOffset";
            this.layoutValuesAbsoluteOffset.Size = new System.Drawing.Size(212, 27);
            this.layoutValuesAbsoluteOffset.Text = "Absolute Offset:";
            this.layoutValuesAbsoluteOffset.TextSize = new System.Drawing.Size(92, 16);
            // 
            // layoutValuesPercentageOffset
            // 
            this.layoutValuesPercentageOffset.Control = this.txtValuesPercentageOffset;
            this.layoutValuesPercentageOffset.CustomizationFormText = "Percentage Offset:";
            this.layoutValuesPercentageOffset.Location = new System.Drawing.Point(406, 0);
            this.layoutValuesPercentageOffset.Name = "layoutValuesPercentageOffset";
            this.layoutValuesPercentageOffset.Size = new System.Drawing.Size(194, 27);
            this.layoutValuesPercentageOffset.Text = "Percentage Offset:";
            this.layoutValuesPercentageOffset.TextSize = new System.Drawing.Size(107, 16);
            // 
            // layoutViewValues
            // 
            this.layoutViewValues.Control = this.btnViewValues;
            this.layoutViewValues.CustomizationFormText = "layoutViewValues";
            this.layoutViewValues.Location = new System.Drawing.Point(600, 0);
            this.layoutViewValues.MaxSize = new System.Drawing.Size(104, 26);
            this.layoutViewValues.MinSize = new System.Drawing.Size(104, 26);
            this.layoutViewValues.Name = "layoutViewValues";
            this.layoutViewValues.Size = new System.Drawing.Size(104, 27);
            this.layoutViewValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewValues.Text = "layoutViewValues";
            this.layoutViewValues.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewValues.TextToControlDistance = 0;
            this.layoutViewValues.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnExportCurveValues;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(704, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(147, 27);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // toolTipController
            // 
            this.toolTipController.Rounded = true;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Location = new System.Drawing.Point(356, 26);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(170, 123);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            // 
            // gridSpotValues
            // 
            this.gridSpotValues.Location = new System.Drawing.Point(14, 443);
            this.gridSpotValues.MainView = this.gridSpotValuesMainView;
            this.gridSpotValues.MenuManager = this.barManager;
            this.gridSpotValues.Name = "gridSpotValues";
            this.gridSpotValues.Size = new System.Drawing.Size(224, 198);
            this.gridSpotValues.TabIndex = 48;
            this.gridSpotValues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridSpotValuesMainView});
            // 
            // gridSpotValuesMainView
            // 
            this.gridSpotValuesMainView.GridControl = this.gridSpotValues;
            this.gridSpotValuesMainView.Name = "gridSpotValuesMainView";
            this.gridSpotValuesMainView.OptionsBehavior.Editable = false;
            this.gridSpotValuesMainView.OptionsCustomization.AllowGroup = false;
            this.gridSpotValuesMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridSpotValuesMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridSpotValuesMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridSpotValuesMainView.OptionsView.ShowDetailButtons = false;
            this.gridSpotValuesMainView.OptionsView.ShowGroupPanel = false;
            this.gridSpotValuesMainView.OptionsView.ShowIndicator = false;
            // 
            // layoutSpotValues
            // 
            this.layoutSpotValues.Control = this.gridSpotValues;
            this.layoutSpotValues.CustomizationFormText = "Spot Values:";
            this.layoutSpotValues.Location = new System.Drawing.Point(0, 27);
            this.layoutSpotValues.MinSize = new System.Drawing.Size(180, 24);
            this.layoutSpotValues.Name = "layoutSpotValues";
            this.layoutSpotValues.Size = new System.Drawing.Size(228, 221);
            this.layoutSpotValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutSpotValues.Text = "Spot Values:";
            this.layoutSpotValues.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutSpotValues.TextSize = new System.Drawing.Size(73, 16);
            // 
            // IndexAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 688);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "IndexAEVForm";
            this.Activated += new System.EventHandler(this.IndexAEVForm_Activated);
            this.Deactivate += new System.EventHandler(this.IndexAEVForm_Deactivate);
            this.Load += new System.EventHandler(this.IndexAEVForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsMarketDefaultForReports.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHasSpotFormula.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpotMappingName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsCalculatedSpotFormula.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seCalculatedSpotOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCalculatedSpotIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seCoverAssocOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCoverAssoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBOAMappingName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFFAMappingName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAssocIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssocOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndexWeight_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupIndex_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsMarketDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuesPercentageOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuesAbsoluteOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(splineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(stepLineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(splineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCurveValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCurveValuesMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridNormalizedBFAValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridNormalizedBFAValuesMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginalBFAValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginalBFAValuesMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpViewValuesDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpViewValuesDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSourceIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsCustom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlGrpMarketDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemIsAssocIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgIndexAssocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAssoc_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndex_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndexWeight_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAssocOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemCalculatedSpotFormula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemHasSpotMappingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMappingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemIsMarketDefaultForReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSourceIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIsCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemIsMarketDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupViewValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewValuesDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOriginalBFAValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNormalizedBFAValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCurveValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChartValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValuesAbsoluteOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValuesPercentageOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpotValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpotValuesMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSpotValues)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraEditors.LookUpEdit lookupMarket;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarket;
        private DevExpress.XtraLayout.LayoutControlItem layoutCode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.CheckEdit chkIsCustom;
        private DevExpress.XtraLayout.LayoutControlItem layoutIsCustom;
        private DevExpress.XtraEditors.DateEdit dtpViewValuesDate;
        private DevExpress.XtraEditors.SimpleButton btnViewValues;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupViewValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutViewValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutViewValuesDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.GridControl gridOriginalBFAValues;
        private DevExpress.XtraGrid.Views.Grid.GridView gridOriginalBFAValuesMainView;
        private DevExpress.XtraLayout.LayoutControlItem layoutOriginalBFAValues;
        private DevExpress.XtraGrid.GridControl gridCurveValues;
        private DevExpress.XtraGrid.Views.Grid.GridView gridCurveValuesMainView;
        private DevExpress.XtraGrid.GridControl gridNormalizedBFAValues;
        private DevExpress.XtraGrid.Views.Grid.GridView gridNormalizedBFAValuesMainView;
        private DevExpress.XtraLayout.LayoutControlItem layoutNormalizedBFAValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutCurveValues;
        private DevExpress.XtraCharts.ChartControl chartValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutChartValues;
        private DevExpress.XtraEditors.LookUpEdit lookupSourceIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutSourceIndex;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraEditors.SpinEdit txtValuesPercentageOffset;
        private DevExpress.XtraEditors.SpinEdit txtValuesAbsoluteOffset;
        private DevExpress.XtraLayout.LayoutControlItem layoutValuesAbsoluteOffset;
        private DevExpress.XtraLayout.LayoutControlItem layoutValuesPercentageOffset;
        private DevExpress.XtraEditors.SimpleButton btnExportCurveValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit chkIsMarketDefault;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemIsMarketDefault;
        private DevExpress.XtraEditors.LookUpEdit lookupIndex_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutIndex_1;
        private DevExpress.XtraEditors.SpinEdit txtIndexWeight_1;
        private DevExpress.XtraLayout.LayoutControlGroup layCtlGrpMarketDefault;
        private DevExpress.XtraLayout.LayoutControlGroup lcgIndexAssocs;
        private DevExpress.XtraLayout.LayoutControlItem layoutIndexWeight_1;
        private DevExpress.XtraEditors.SimpleButton btnAddIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnRemoveIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraLayout.LayoutControlGroup lcgAssoc_1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraEditors.SpinEdit txtAssocOffset;
        private DevExpress.XtraLayout.LayoutControlItem layoutAssocOffset;
        private DevExpress.XtraEditors.CheckEdit chkIsAssocIndex;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemIsAssocIndex;
        private DevExpress.XtraEditors.TextEdit txtFFAMappingName;
        private DevExpress.XtraLayout.LayoutControlItem layoutMappingName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit txtBOAMappingName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SpinEdit seCoverAssocOffset;
        private DevExpress.XtraEditors.LookUpEdit lookupCoverAssoc;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.CheckEdit chkIsCalculatedSpotFormula;
        private DevExpress.XtraEditors.SpinEdit seCalculatedSpotOffset;
        private DevExpress.XtraEditors.LookUpEdit lookupCalculatedSpotIndex;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemCalculatedSpotFormula;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.CheckEdit chkHasSpotFormula;
        private DevExpress.XtraEditors.TextEdit txtSpotMappingName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemHasSpotMappingName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.CheckEdit chkIsMarketDefaultForReports;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemIsMarketDefaultForReports;
        private DevExpress.XtraGrid.GridControl gridSpotValues;
        private DevExpress.XtraGrid.Views.Grid.GridView gridSpotValuesMainView;
        private DevExpress.XtraLayout.LayoutControlItem layoutSpotValues;
    }
}