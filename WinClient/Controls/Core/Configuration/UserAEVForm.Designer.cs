﻿namespace Exis.WinClient.Controls
{
    partial class UserAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtPassword2 = new DevExpress.XtraEditors.TextEdit();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.txtLogin = new DevExpress.XtraEditors.TextEdit();
            this.tlBooks = new DevExpress.XtraTreeList.TreeList();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.txtLastName = new DevExpress.XtraEditors.TextEdit();
            this.txtFirstName = new DevExpress.XtraEditors.TextEdit();
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.clbTraders = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupUser = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemTraders = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemBooks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemLogin = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPassword2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemFirstName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbTraders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTraders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPassword2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtPassword2);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtPassword);
            this.layoutRootControl.Controls.Add(this.txtLogin);
            this.layoutRootControl.Controls.Add(this.tlBooks);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.txtLastName);
            this.layoutRootControl.Controls.Add(this.txtFirstName);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.clbTraders);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(293, 167, 250, 350);
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(926, 448);
            this.layoutRootControl.TabIndex = 0;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(805, 86);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(107, 20);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 31;
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(710, 62);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.Properties.MaxLength = 1000;
            this.txtPassword2.Properties.UseSystemPasswordChar = true;
            this.txtPassword2.Size = new System.Drawing.Size(202, 20);
            this.txtPassword2.StyleController = this.layoutRootControl;
            this.txtPassword2.TabIndex = 20;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(805, 353);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Size = new System.Drawing.Size(107, 20);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 9;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(580, 353);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(128, 20);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 12;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(376, 353);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Size = new System.Drawing.Size(107, 20);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 8;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(403, 62);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.MaxLength = 1000;
            this.txtPassword.Properties.UseSystemPasswordChar = true;
            this.txtPassword.Size = new System.Drawing.Size(210, 20);
            this.txtPassword.StyleController = this.layoutRootControl;
            this.txtPassword.TabIndex = 12;
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(107, 62);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Properties.MaxLength = 1000;
            this.txtLogin.Size = new System.Drawing.Size(199, 20);
            this.txtLogin.StyleController = this.layoutRootControl;
            this.txtLogin.TabIndex = 11;
            // 
            // tlBooks
            // 
            this.tlBooks.Location = new System.Drawing.Point(520, 110);
            this.tlBooks.Name = "tlBooks";
            this.tlBooks.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tlBooks.OptionsBehavior.Editable = false;
            this.tlBooks.OptionsView.ShowCheckBoxes = true;
            this.tlBooks.OptionsView.ShowColumns = false;
            this.tlBooks.OptionsView.ShowHorzLines = false;
            this.tlBooks.OptionsView.ShowIndicator = false;
            this.tlBooks.OptionsView.ShowVertLines = false;
            this.tlBooks.Size = new System.Drawing.Size(392, 196);
            this.tlBooks.TabIndex = 19;
            this.tlBooks.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.TlBooksBeforeCheckNode);
            this.tlBooks.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.TlBooksAfterCheckNode);
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(107, 353);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(172, 20);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 11;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(483, 86);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Properties.MaxLength = 1000;
            this.txtLastName.Size = new System.Drawing.Size(278, 20);
            this.txtLastName.StyleController = this.layoutRootControl;
            this.txtLastName.TabIndex = 10;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(107, 86);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Properties.MaxLength = 1000;
            this.txtFirstName.Size = new System.Drawing.Size(279, 20);
            this.txtFirstName.StyleController = this.layoutRootControl;
            this.txtFirstName.TabIndex = 10;
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(29, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(883, 25);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 17;
            this.lblId.Text = "Id";
            // 
            // clbTraders
            // 
            this.clbTraders.CheckOnClick = true;
            this.clbTraders.Location = new System.Drawing.Point(107, 110);
            this.clbTraders.Name = "clbTraders";
            this.clbTraders.Size = new System.Drawing.Size(316, 196);
            this.clbTraders.StyleController = this.layoutRootControl;
            this.clbTraders.TabIndex = 18;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutControlGroup1";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupUser,
            this.layoutGroupUserInfo,
            this.emptySpaceItem1});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(926, 448);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupUser
            // 
            this.layoutGroupUser.CustomizationFormText = "User";
            this.layoutGroupUser.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutId,
            this.layoutItemTraders,
            this.layoutItemBooks,
            this.layoutItemLogin,
            this.layoutItemPassword,
            this.layoutItemPassword2,
            this.layoutItemFirstName,
            this.layoutItemLastName,
            this.layoutItemStatus});
            this.layoutGroupUser.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupUser.Name = "layoutGroupUser";
            this.layoutGroupUser.Size = new System.Drawing.Size(926, 320);
            this.layoutGroupUser.Text = "User";
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MinSize = new System.Drawing.Size(32, 20);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(902, 29);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id";
            this.layoutId.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutId.TextSize = new System.Drawing.Size(10, 13);
            this.layoutId.TextToControlDistance = 5;
            // 
            // layoutItemTraders
            // 
            this.layoutItemTraders.Control = this.clbTraders;
            this.layoutItemTraders.CustomizationFormText = "layoutItemTraders";
            this.layoutItemTraders.Location = new System.Drawing.Point(0, 77);
            this.layoutItemTraders.MaxSize = new System.Drawing.Size(413, 200);
            this.layoutItemTraders.MinSize = new System.Drawing.Size(413, 200);
            this.layoutItemTraders.Name = "layoutItemTraders";
            this.layoutItemTraders.Size = new System.Drawing.Size(413, 200);
            this.layoutItemTraders.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemTraders.Text = "Traders:";
            this.layoutItemTraders.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemBooks
            // 
            this.layoutItemBooks.Control = this.tlBooks;
            this.layoutItemBooks.CustomizationFormText = "Books:";
            this.layoutItemBooks.Location = new System.Drawing.Point(413, 77);
            this.layoutItemBooks.MinSize = new System.Drawing.Size(179, 24);
            this.layoutItemBooks.Name = "layoutItemBooks";
            this.layoutItemBooks.Size = new System.Drawing.Size(489, 200);
            this.layoutItemBooks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemBooks.Text = "Books:";
            this.layoutItemBooks.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemLogin
            // 
            this.layoutItemLogin.Control = this.txtLogin;
            this.layoutItemLogin.CustomizationFormText = "Login:";
            this.layoutItemLogin.Location = new System.Drawing.Point(0, 29);
            this.layoutItemLogin.Name = "layoutItemLogin";
            this.layoutItemLogin.Size = new System.Drawing.Size(296, 24);
            this.layoutItemLogin.Text = "Login:";
            this.layoutItemLogin.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemPassword
            // 
            this.layoutItemPassword.Control = this.txtPassword;
            this.layoutItemPassword.CustomizationFormText = "Password:";
            this.layoutItemPassword.Location = new System.Drawing.Point(296, 29);
            this.layoutItemPassword.MinSize = new System.Drawing.Size(129, 24);
            this.layoutItemPassword.Name = "layoutItemPassword";
            this.layoutItemPassword.Size = new System.Drawing.Size(307, 24);
            this.layoutItemPassword.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemPassword.Text = "Password:";
            this.layoutItemPassword.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemPassword2
            // 
            this.layoutItemPassword2.Control = this.txtPassword2;
            this.layoutItemPassword2.CustomizationFormText = "Confirm Password:";
            this.layoutItemPassword2.Location = new System.Drawing.Point(603, 29);
            this.layoutItemPassword2.MinSize = new System.Drawing.Size(151, 24);
            this.layoutItemPassword2.Name = "layoutItemPassword2";
            this.layoutItemPassword2.Size = new System.Drawing.Size(299, 24);
            this.layoutItemPassword2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemPassword2.Text = "Confirm Password:";
            this.layoutItemPassword2.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemFirstName
            // 
            this.layoutItemFirstName.Control = this.txtFirstName;
            this.layoutItemFirstName.CustomizationFormText = "First Name:";
            this.layoutItemFirstName.Location = new System.Drawing.Point(0, 53);
            this.layoutItemFirstName.MinSize = new System.Drawing.Size(151, 24);
            this.layoutItemFirstName.Name = "layoutItemFirstName";
            this.layoutItemFirstName.Size = new System.Drawing.Size(376, 24);
            this.layoutItemFirstName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemFirstName.Text = "First Name:";
            this.layoutItemFirstName.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemLastName
            // 
            this.layoutItemLastName.Control = this.txtLastName;
            this.layoutItemLastName.CustomizationFormText = "Last Name:";
            this.layoutItemLastName.Location = new System.Drawing.Point(376, 53);
            this.layoutItemLastName.MinSize = new System.Drawing.Size(151, 24);
            this.layoutItemLastName.Name = "layoutItemLastName";
            this.layoutItemLastName.Size = new System.Drawing.Size(375, 24);
            this.layoutItemLastName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemLastName.Text = "Last Name:";
            this.layoutItemLastName.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutItemStatus
            // 
            this.layoutItemStatus.Control = this.cmbStatus;
            this.layoutItemStatus.CustomizationFormText = "Status:";
            this.layoutItemStatus.Location = new System.Drawing.Point(751, 53);
            this.layoutItemStatus.Name = "layoutItemStatus";
            this.layoutItemStatus.Size = new System.Drawing.Size(151, 24);
            this.layoutItemStatus.Text = "Status:";
            this.layoutItemStatus.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemStatus.TextSize = new System.Drawing.Size(35, 13);
            this.layoutItemStatus.TextToControlDistance = 5;
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 320);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(926, 67);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "layoutControlItem1";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(269, 24);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(269, 0);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(204, 24);
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(473, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(225, 24);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(698, 0);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(204, 24);
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 387);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(926, 61);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(926, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 448);
            this.barDockControlBottom.Size = new System.Drawing.Size(926, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 448);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(926, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 448);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // UserAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 480);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "UserAEVForm";
            this.Activated += new System.EventHandler(this.UserAevFormActivated);
            this.Deactivate += new System.EventHandler(this.UserAevFormDeactivate);
            this.Load += new System.EventHandler(this.UserAevFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbTraders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTraders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPassword2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraEditors.TextEdit txtFirstName;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemFirstName;
        private DevExpress.XtraEditors.TextEdit txtLastName;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemLastName;
        private DevExpress.XtraEditors.CheckedListBoxControl clbTraders;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTraders;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraTreeList.TreeList tlBooks;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBooks;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.TextEdit txtLogin;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemLogin;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPassword;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.TextEdit txtPassword2;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPassword2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemStatus;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}