﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.WinClient.Controls.Core;
using Exis.WinClient.General;
using Exis.Domain;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;

namespace Exis.WinClient.Controls
{
    public partial class InactiveTradesForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        public event Action OnTradeActivated;
        #endregion

        #region Constructors

        public InactiveTradesForm()
        {
            InitializeComponent();

            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridTradeDetails.BeginUpdate();

            int gridViewColumnindex = 0;
            gridTradeDetailsView.Columns.Clear();
            gridTradeDetailsView.OptionsView.ColumnAutoWidth = false;
            GridColumn column = AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                          UnboundColumnType.Bound, null);
            column.SummaryItem.SummaryType = SummaryItemType.Count;
            column.SummaryItem.DisplayFormat = "Count: {0:N0}";
            gridTradeDetailsView.Columns.Add(column);
            gridTradeDetailsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                                       UnboundColumnType.String, null));

            gridTradeDetailsView.OptionsLayout.StoreDataSettings = true;
            gridTradeDetailsView.OptionsLayout.StoreVisualOptions = true;

            gridTradeDetails.EndUpdate();
            gridTradeDetailsView.OptionsLayout.StoreDataSettings = true;
            gridTradeDetailsView.OptionsLayout.StoreVisualOptions = true;
        }

        private GridColumn AddColumn(string name, string caption, int columnIndex,
                                     UnboundColumnType columnType, object Tag)
        {
            var column = new GridColumn();
            column.Visible = true;
            column.AppearanceCell.Options.UseTextOptions = true;
            column.OptionsColumn.FixedWidth = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsColumn.AllowEdit = false;
            column.FieldName = name;
            column.Caption = caption;
            column.Tag = Tag;
            column.VisibleIndex = columnIndex;
            if (columnType != UnboundColumnType.Bound)
                column.UnboundType = columnType;

            switch (columnType)
            {
                case UnboundColumnType.String:
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.DateTime:
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.DisplayFormat.FormatType = FormatType.DateTime;
                    column.DisplayFormat.FormatString = "G";
                    break;
                case UnboundColumnType.Decimal:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.UnboundType = UnboundColumnType.Decimal;
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "N3";
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Integer:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.UnboundType = UnboundColumnType.Integer;
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Boolean:
                    column.UnboundType = UnboundColumnType.Boolean;
                    column.OptionsColumn.AllowEdit = false;
                    break;
            }

            return column;
        }

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            BeginGetInactiveTrades();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetInactiveTrades()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);
            btnActivate.Enabled = false;
            try
            {
                SessionRegistry.Client.BeginGetInactiveTrades(EndGetInactiveTrades, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnActivate.Enabled = true;
            }
        }

        private void EndGetInactiveTrades(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetInactiveTrades;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Trade> trades;

            try
            {
                result = SessionRegistry.Client.EndGetInactiveTrades(out trades, ar);
            }
            catch (Exception)
            {
                btnActivate.Enabled = true;
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (result == null) //Exception Occurred
            {
                btnActivate.Enabled = true;
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                var tradeInformations = new List<TradeInformation>();
                foreach (Trade tradeTC in trades.Where(a => a.Type == TradeTypeEnum.TC))
                {
                    foreach (TradeTcInfoLeg tradeTCleg in tradeTC.Info.TCInfo.Legs)
                    {
                        tradeInformations.Add(new TradeInformation
                        {
                            Identifier = tradeTCleg.Id + "|TC",
                            Brokers = tradeTC.Info.BrokersName,
                            Company = tradeTC.Info.CompanyName,
                            Counterparty = tradeTC.Info.CounterpartyName,
                            Direction = tradeTC.Info.Direction.ToString("g"),
                            ExternalCode = tradeTC.Info.ExternalCode,
                            LegNumber = tradeTCleg.Identifier,
                            Status = tradeTC.Status.ToString("g"),
                            Type = tradeTC.Type.ToString("g"),
                            PhysicalFinancial =
                                tradeTC.Type == TradeTypeEnum.TC ||
                                tradeTC.Type == TradeTypeEnum.Cargo
                                    ? "Physical"
                                    : "Financial",
                            Price = tradeTCleg.Rate.HasValue ? tradeTCleg.Rate.Value : 0,
                            SignDate = tradeTC.Info.SignDate,
                            PeriodFrom = tradeTCleg.PeriodFrom,
                            PeriodTo = tradeTCleg.PeriodTo,
                            Vessel = tradeTC.Info.TCInfo.VesselName,
                            ForwardCurve = tradeTC.Info.MTMFwdIndexName,
                            StrikePrice = 0,
                            MarketTonnes = tradeTC.Info.MarketTonnes,
                            Trade = tradeTC,
                            TradeInfo = tradeTC.Info
                        });
                    }
                }


                foreach (Trade tradeFFA in trades.Where(a => a.Type == TradeTypeEnum.FFA))
                {
                    tradeInformations.Add(new TradeInformation
                    {
                        Identifier = tradeFFA.Info.FFAInfo.Id + "|FFA",
                        Brokers = tradeFFA.Info.BrokersName,
                        Company = tradeFFA.Info.CompanyName,
                        Counterparty = tradeFFA.Info.CounterpartyName,
                        Direction = tradeFFA.Info.Direction.ToString("g"),
                        ExternalCode = tradeFFA.Info.ExternalCode,
                        LegNumber = 0,
                        Status = tradeFFA.Status.ToString("g"),
                        Type = tradeFFA.Type.ToString("g"),
                        PhysicalFinancial =
                            tradeFFA.Type == TradeTypeEnum.TC ||
                            tradeFFA.Type == TradeTypeEnum.Cargo
                                ? "Physical"
                                : "Financial",
                        Price = tradeFFA.Info.FFAInfo.Price,
                        SignDate = tradeFFA.Info.SignDate,
                        PeriodFrom = tradeFFA.Info.PeriodFrom,
                        PeriodTo = tradeFFA.Info.PeriodTo,
                        Vessel = tradeFFA.Info.FFAInfo.VesselName,
                        ForwardCurve = tradeFFA.Info.MTMFwdIndexName,
                        StrikePrice = 0,
                        MarketTonnes = tradeFFA.Info.MarketTonnes,
                        Trade = tradeFFA,
                        TradeInfo = tradeFFA.Info
                    });
                }

                foreach (Trade tradeCargo in trades.Where(a => a.Type == TradeTypeEnum.Cargo))
                {
                    foreach (TradeCargoInfoLeg tradeCargoLeg in tradeCargo.Info.CargoInfo.Legs)
                    {
                        tradeInformations.Add(new TradeInformation
                        {
                            Identifier = tradeCargoLeg.Id + "|CARGO",
                            Brokers = tradeCargo.Info.BrokersName,
                            Company = tradeCargo.Info.CompanyName,
                            Counterparty = tradeCargo.Info.CounterpartyName,
                            Direction = tradeCargo.Info.Direction.ToString("g"),
                            ExternalCode = tradeCargo.Info.ExternalCode,
                            LegNumber = tradeCargoLeg.Identifier,
                            Status = tradeCargo.Status.ToString("g"),
                            Type = tradeCargo.Type.ToString("g"),
                            PhysicalFinancial = "Physical",
                            Price = tradeCargoLeg.Tce.HasValue ? tradeCargoLeg.Tce.Value : 0,
                            SignDate = tradeCargo.Info.SignDate,
                            PeriodFrom = tradeCargoLeg.PeriodFrom,
                            PeriodTo = tradeCargoLeg.PeriodTo,
                            Vessel = tradeCargo.Info.CargoInfo.VesselName,
                            ForwardCurve = tradeCargo.Info.MTMFwdIndexName,
                            StrikePrice = 0,
                            MarketTonnes = tradeCargo.Info.MarketTonnes,
                            Trade = tradeCargo,
                            TradeInfo = tradeCargo.Info
                        });
                    }
                }

                foreach (Trade tradeOption in trades.Where(a => a.Type == TradeTypeEnum.Option))
                {
                    tradeInformations.Add(new TradeInformation
                    {
                        Identifier = tradeOption.Info.OptionInfo.Id + "|OPTION",
                        Brokers = tradeOption.Info.BrokersName,
                        Company = tradeOption.Info.CompanyName,
                        Counterparty = tradeOption.Info.CounterpartyName,
                        Direction = tradeOption.Info.Direction.ToString("g"),
                        ExternalCode = tradeOption.Info.ExternalCode,
                        LegNumber = 0,
                        Status = tradeOption.Status.ToString("g"),
                        Type = tradeOption.Info.OptionInfo.Type.ToString("g"),
                        PhysicalFinancial =
                            tradeOption.Type == TradeTypeEnum.TC ||
                            tradeOption.Type == TradeTypeEnum.Cargo
                                ? "Physical"
                                : "Financial",
                        Price = tradeOption.Info.OptionInfo.Premium,
                        SignDate = tradeOption.Info.SignDate,
                        PeriodFrom = tradeOption.Info.PeriodFrom,
                        PeriodTo = tradeOption.Info.PeriodTo,
                        Vessel = tradeOption.Info.OptionInfo.VesselName,
                        ForwardCurve = tradeOption.Info.MTMFwdIndexName,
                        StrikePrice = tradeOption.Info.OptionInfo.Strike,
                        MarketTonnes = tradeOption.Info.MarketTonnes,
                        Trade = tradeOption,
                        TradeInfo = tradeOption.Info
                    });
                }

                gridTradeDetails.DataSource = tradeInformations;
                btnActivate.Enabled = tradeInformations.Count > 0;
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginUpdateTradeStatus(TradeTypeEnum tradeTypeEnum, long identifierId, ActivationStatusEnum statusEnum)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginUpdateTradeStatus(tradeTypeEnum, identifierId, statusEnum, EndUpdateTradeStatus, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndUpdateTradeStatus(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndUpdateTradeStatus;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndUpdateTradeStatus(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                RefreshData();
                if (OnTradeActivated != null)
                    OnTradeActivated();
            }
        }

        #endregion

        #region Event Handlers

        private void BtnActivateClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var tradeInformation = (TradeInformation)gridTradeDetailsView.GetFocusedRow();

            if (tradeInformation == null) return;

            DialogResult result = XtraMessageBox.Show(this,
                                                          "The selected trade will be activated. Are you sure you want to procceed?",
                                                          Strings.Freight_Metrics,
                                                          MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.No) return;

            long identifierId = Convert.ToInt32(tradeInformation.Identifier.Split('|')[0]);

            TradeTypeEnum typeEnum = (tradeInformation.Type == "Call" || tradeInformation.Type == "Put")
                                         ? TradeTypeEnum.Option
                                         : (TradeTypeEnum)Enum.Parse(typeof(TradeTypeEnum), tradeInformation.Type);

            BeginUpdateTradeStatus(typeEnum, identifierId, ActivationStatusEnum.Active);
        }

        private void InactiveTradesFormLoad(object sender, EventArgs e)
        {
            BeginGetInactiveTrades();
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void BtnRefreshClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        #endregion
    }
}