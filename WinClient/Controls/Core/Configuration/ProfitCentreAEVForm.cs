﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class ProfitCentreAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private ProfitCentre _profitCentre;

        #endregion

        #region Constructors

        public ProfitCentreAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public ProfitCentreAEVForm(ProfitCentre profitCentre, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _profitCentre = profitCentre;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            txtVaRLimit.Text = null;

            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;

                txtVaRLimit.Properties.ReadOnly = true;
                txtUpdateUser.Properties.ReadOnly = true;
                txtNotionalLimit.Properties.ReadOnly = true;
                txtName.Properties.ReadOnly = true;
                txtMTMLimit.Properties.ReadOnly = true;
                txtExpirationLimit.Properties.ReadOnly = true;
                txtDurationLimit.Properties.ReadOnly = true;
                txtCapitalLimit.Properties.ReadOnly = true;
                txtExposureLimit.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
            }
        }

        private void InitializeData()
        {
            if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                LoadData();
        }

        private void LoadData()
        {
            lblId.Text = _profitCentre.Id.ToString();
            txtName.Text = _profitCentre.Name;
            cmbStatus.SelectedItem = _profitCentre.Status;
            txtVaRLimit.Text = _profitCentre.VarLimit == null ? null : _profitCentre.VarLimit.Value.ToString();
            txtNotionalLimit.Text = _profitCentre.NotionalLimit == null ? null : _profitCentre.NotionalLimit.Value.ToString();
            txtMTMLimit.Text = _profitCentre.MtmLimit == null ? null : _profitCentre.MtmLimit.Value.ToString();
            txtExposureLimit.Text = _profitCentre.ExposureLimit == null ? null : _profitCentre.ExposureLimit.Value.ToString();
            txtExpirationLimit.Text = _profitCentre.ExpirationLimit == null ? null : _profitCentre.ExpirationLimit.Value.ToString();
            txtDurationLimit.Text = _profitCentre.DurationLimit == null ? null : _profitCentre.DurationLimit.Value.ToString();
            txtCapitalLimit.Text = _profitCentre.CapitalLimit == null ? null : _profitCentre.CapitalLimit.Value.ToString();

            txtCreationUser.Text = _profitCentre.Cruser;
            dtpCreationDate.DateTime = _profitCentre.Crd;
            txtUpdateUser.Text = _profitCentre.Chuser;
            dtpUpdateDate.DateTime = _profitCentre.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void ProfitCentreAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _profitCentre = new ProfitCentre();
            }

            _profitCentre.Name = txtName.Text.Trim();
            _profitCentre.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _profitCentre.VarLimit = String.IsNullOrEmpty(txtVaRLimit.Text) ? null : (int?) txtVaRLimit.Value;
            _profitCentre.CapitalLimit = String.IsNullOrEmpty(txtCapitalLimit.Text) ? null : (int?)txtCapitalLimit.Value;
            _profitCentre.DurationLimit = String.IsNullOrEmpty(txtDurationLimit.Text) ? null : (int?)txtDurationLimit.Value;
            _profitCentre.ExpirationLimit = String.IsNullOrEmpty(txtExpirationLimit.Text) ? null : (int?)txtExpirationLimit.Value;
            _profitCentre.ExposureLimit = String.IsNullOrEmpty(txtExposureLimit.Text) ? null : (int?)txtExposureLimit.Value;
            _profitCentre.MtmLimit = String.IsNullOrEmpty(txtMTMLimit.Text) ? null : (int?)txtMTMLimit.Value;
            _profitCentre.NotionalLimit = String.IsNullOrEmpty(txtNotionalLimit.Text) ? null : (int?)txtNotionalLimit.Value;

            BeginAEVProfitCentre(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void ProfitCentreAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void ProfitCentreAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVProfitCentre(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _profitCentre, EndAEVProfitCentre, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVProfitCentre(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVProfitCentre;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public ProfitCentre ProfitCentre
        {
            get { return _profitCentre; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}