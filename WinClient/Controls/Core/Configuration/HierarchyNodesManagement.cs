﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.SpecialForms;
using Exis.WinClient.General;
using Exis.Domain;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Controls;

namespace Exis.WinClient.Controls
{
    public partial class HierarchyNodesManagement : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private List<HierarchyNodeType> _hierarchyNodeTypes;
        private int counter = 1;

        #endregion

        #region Constructor

        public HierarchyNodesManagement()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            lookupNodeType.DisplayMember = "Name";
            lookupNodeType.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Entity,
                FieldName = "Name",
                Width = 0
            };
            lookupNodeType.Columns.Add(col);
            lookupNodeType.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupNodeType.SearchMode = SearchMode.AutoFilter;
            lookupNodeType.NullValuePrompt = Strings.Select_a_Company___;
        }

        private void PopulateTree(TreeListNode parentNode, List<HierarchyNode> childrenNodes, ref List<HierarchyNode> allNodes)
        {
            foreach (HierarchyNode node in childrenNodes)
            {
                TreeListNode childNode =
                    treeListHierarchyNodes.AppendNode(new object[] {  node.NodeTypeId }, parentNode, counter);
                counter++;
                PopulateTree(childNode, allNodes.Where(a => a.ParentId == node.Id).ToList(), ref allNodes);
            }
        }

        #endregion

        #region GUI Event Handlers

        private void btnAddNodeClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TreeListNode parentNode = null;
            treeListHierarchyNodes.NodesIterator.DoOperation(a =>
            {
                if (!a.HasChildren)
                {
                    parentNode = a;
                }
            });

            if (parentNode.GetValue("NodeType") != null)
            {
                TreeListNode newNode = treeListHierarchyNodes.AppendNode(null, parentNode, counter);
                counter++;
                treeListHierarchyNodes.MakeNodeVisible(newNode);
            }
        }

        private void btnDeleteNodeClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(treeListHierarchyNodes.FocusedNode != null)
            {
                TreeListNode childNode = treeListHierarchyNodes.FocusedNode.HasChildren
                    ? treeListHierarchyNodes.FocusedNode.Nodes[0] : null;
                TreeListNode parentNode = treeListHierarchyNodes.FocusedNode.ParentNode ?? null;

                if (childNode != null)
                    treeListHierarchyNodes.MoveNode(childNode, parentNode);
                treeListHierarchyNodes.DeleteNode(treeListHierarchyNodes.FocusedNode);
            }
        }

        private void HierarchyNodesManagementLoad(object sender, EventArgs e)
        {
            BeginAEVHierarchyInitializationData();
        }

        private void LookupNodeTypeQueryPopUp(object sender, CancelEventArgs e)
        {
            var lookup = ((LookUpEdit) sender);

            var selectedNodeTypeIds = new List<long>();
            treeListHierarchyNodes.NodesIterator.DoOperation(a =>
                                                                 {
                                                                     if (a.GetValue("NodeType") != ((LookUpEdit)sender).EditValue)
                                                                         selectedNodeTypeIds.Add(
                                                                             Convert.ToInt64(a.GetValue("NodeType")));
                                                                 });
            lookup.Properties.DataSource = _hierarchyNodeTypes.Where(a => !selectedNodeTypeIds.Contains(a.Id)).ToList();
        }

        private void btnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var hierarchyNodes = new List<HierarchyNode>();
            treeListHierarchyNodes.NodesIterator.DoOperation(a =>
            {
                if (a.GetValue("NodeType") != null)
                {
                    var newNode = new HierarchyNode()
                                      {
                                          Id = Convert.ToInt64(a.Tag),
                                          NodeTypeId = Convert.ToInt64(a.GetValue("NodeType")),
                                          ParentId =
                                              a.ParentNode != null ? Convert.ToInt64(a.ParentNode.Tag) : (long?) null
                                      };
                    hierarchyNodes.Add(newNode);
                }
            });

            BeginAEVHierarchyNodes(hierarchyNodes);
        }

        private void btnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void btnMoveUpClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (treeListHierarchyNodes.FocusedNode != null && treeListHierarchyNodes.FocusedNode.ParentNode != null && treeListHierarchyNodes.FocusedNode.GetValue("NodeType") != null)
            {
                TreeListNode curParentNode = treeListHierarchyNodes.FocusedNode.ParentNode;
                TreeListNode newParentNode = treeListHierarchyNodes.FocusedNode.ParentNode.ParentNode;
                TreeListNode childNode = treeListHierarchyNodes.FocusedNode.HasChildren
                    ? treeListHierarchyNodes.FocusedNode.Nodes[0] : null;

                treeListHierarchyNodes.MoveNode(treeListHierarchyNodes.FocusedNode, newParentNode);
                treeListHierarchyNodes.MoveNode(curParentNode, treeListHierarchyNodes.FocusedNode);
                treeListHierarchyNodes.MoveNode(childNode, curParentNode);
            }
        }

        private void btnMoveDownClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (treeListHierarchyNodes.FocusedNode != null && treeListHierarchyNodes.FocusedNode.HasChildren && treeListHierarchyNodes.FocusedNode.GetValue("NodeType") != null)
            {
                TreeListNode curParentNode = treeListHierarchyNodes.FocusedNode.ParentNode;
                TreeListNode childNode = treeListHierarchyNodes.FocusedNode.HasChildren
                    ? treeListHierarchyNodes.FocusedNode.Nodes[0] : null;
                TreeListNode childchildNode = treeListHierarchyNodes.FocusedNode.Nodes[0].HasChildren
                                                  ? treeListHierarchyNodes.FocusedNode.Nodes[0].Nodes[0]
                                                  : null;

                treeListHierarchyNodes.MoveNode(childNode, curParentNode);
                treeListHierarchyNodes.MoveNode(treeListHierarchyNodes.FocusedNode, childNode);
                if (childchildNode != null)
                    treeListHierarchyNodes.MoveNode(childchildNode, treeListHierarchyNodes.FocusedNode);
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVHierarchyInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVHiearchyInitializationData(EndAEVHierarchyInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVHierarchyInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVHierarchyInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<HierarchyNode> hierarchyNodes;
            try
            {
                result = SessionRegistry.Client.EndAEVHiearchyInitializationData(out hierarchyNodes, out _hierarchyNodeTypes, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                treeListHierarchyNodes.Tag = hierarchyNodes;
                PopulateTree(null, hierarchyNodes.Where(a => a.ParentId == null).ToList(), ref hierarchyNodes);
                treeListHierarchyNodes.ExpandAll();

                lookupNodeType.DataSource = _hierarchyNodeTypes;
                lookupNodeType.Tag = _hierarchyNodeTypes;
                
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVHierarchyNodes(List<HierarchyNode> hierarchyNodes)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVHierarchyNodes(hierarchyNodes, EndAEVHierarchyNodes, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVHierarchyNodes(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVHierarchyNodes;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEVHierarchyNodes(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, false);
            }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

    }
}