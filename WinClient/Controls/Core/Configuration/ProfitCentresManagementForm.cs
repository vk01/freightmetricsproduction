﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class ProfitCentresManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public ProfitCentresManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void ProfitCentresManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetProfitCentres();
        }

        private void btnAdd_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof (ProfitCentre));
        }

        private void btnEdit_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridProfitCentresMainView.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((ProfitCentre) gridProfitCentresMainView.GetFocusedRow());
        }

        private void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridProfitCentresMainView.GetFocusedRow() == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((ProfitCentre) gridProfitCentresMainView.GetFocusedRow());
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetProfitCentres();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void ProfitCentresManagementForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void ProfitCentresManagementForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridProfitCentres.BeginUpdate();

            int gridViewColumnindex = 0;

            gridProfitCentresMainView.Columns.Clear();

            gridProfitCentresMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Name,
                                                    FieldName = "Name",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridProfitCentresMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Status,
                                                    FieldName = "Status",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridProfitCentresMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.VaR_Limit,
                                                    FieldName = "VarLimit",
                                                    VisibleIndex = gridViewColumnindex
                                                });

            gridProfitCentres.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetProfitCentres()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new ProfitCentre(),  EndGetProfitCentres, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetProfitCentres(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetProfitCentres;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridProfitCentres.DataSource = entities.Cast<ProfitCentre>().ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion
    }
}