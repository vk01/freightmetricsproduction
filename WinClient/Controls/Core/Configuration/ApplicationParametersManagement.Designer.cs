﻿namespace Exis.WinClient.Controls
{
    partial class ApplicationParametersManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationParametersManagement));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.grdAppParameters = new DevExpress.XtraGrid.GridControl();
            this.grvAppParameters = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemGrdAppParameters = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnAdd = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAppParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvAppParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemGrdAppParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.grdAppParameters);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlRootGroup;
            this.layoutControl.Size = new System.Drawing.Size(624, 379);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // grdAppParameters
            // 
            this.grdAppParameters.Location = new System.Drawing.Point(12, 12);
            this.grdAppParameters.MainView = this.grvAppParameters;
            this.grdAppParameters.Name = "grdAppParameters";
            this.grdAppParameters.Size = new System.Drawing.Size(600, 355);
            this.grdAppParameters.TabIndex = 4;
            this.grdAppParameters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAppParameters});
            // 
            // grvAppParameters
            // 
            this.grvAppParameters.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcCode,
            this.gcDescription,
            this.gcValue});
            this.grvAppParameters.GridControl = this.grdAppParameters;
            this.grvAppParameters.Name = "grvAppParameters";
            this.grvAppParameters.OptionsBehavior.Editable = false;
            this.grvAppParameters.OptionsView.ShowGroupPanel = false;
            this.grvAppParameters.OptionsView.ShowIndicator = false;
            this.grvAppParameters.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GrvAppParametersFocusedRowChanged);
            // 
            // gcCode
            // 
            this.gcCode.Caption = "Code";
            this.gcCode.FieldName = "Code";
            this.gcCode.Name = "gcCode";
            this.gcCode.Visible = true;
            this.gcCode.VisibleIndex = 0;
            this.gcCode.Width = 174;
            // 
            // gcDescription
            // 
            this.gcDescription.Caption = "Description";
            this.gcDescription.FieldName = "Description";
            this.gcDescription.Name = "gcDescription";
            this.gcDescription.Visible = true;
            this.gcDescription.VisibleIndex = 1;
            this.gcDescription.Width = 174;
            // 
            // gcValue
            // 
            this.gcValue.Caption = "Value";
            this.gcValue.FieldName = "Value";
            this.gcValue.Name = "gcValue";
            this.gcValue.Visible = true;
            this.gcValue.VisibleIndex = 2;
            this.gcValue.Width = 174;
            // 
            // layoutControlRootGroup
            // 
            this.layoutControlRootGroup.CustomizationFormText = "layoutControlRootGroup";
            this.layoutControlRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlRootGroup.GroupBordersVisible = false;
            this.layoutControlRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemGrdAppParameters});
            this.layoutControlRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRootGroup.Name = "layoutControlRootGroup";
            this.layoutControlRootGroup.Size = new System.Drawing.Size(624, 379);
            this.layoutControlRootGroup.Text = "layoutControlRootGroup";
            this.layoutControlRootGroup.TextVisible = false;
            // 
            // layCtlItemGrdAppParameters
            // 
            this.layCtlItemGrdAppParameters.Control = this.grdAppParameters;
            this.layCtlItemGrdAppParameters.CustomizationFormText = "layCtlItemGrdAppParameters";
            this.layCtlItemGrdAppParameters.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemGrdAppParameters.Name = "layCtlItemGrdAppParameters";
            this.layCtlItemGrdAppParameters.Size = new System.Drawing.Size(604, 359);
            this.layCtlItemGrdAppParameters.Text = "layCtlItemGrdAppParameters";
            this.layCtlItemGrdAppParameters.TextSize = new System.Drawing.Size(0, 0);
            this.layCtlItemGrdAppParameters.TextToControlDistance = 0;
            this.layCtlItemGrdAppParameters.TextVisible = false;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "exit.ico");
            this.imageList24.Images.SetKeyName(4, "viewItem24x24.ico");
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnRefresh,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 4;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnAdd
            // 
            this.btnAdd.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnAdd.Caption = "Add";
            this.btnAdd.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnAdd.Id = 0;
            this.btnAdd.LargeImageIndex = 0;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnAddClick);
            // 
            // btnEdit
            // 
            this.btnEdit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnEdit.Caption = "Edit";
            this.btnEdit.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnEdit.Id = 1;
            this.btnEdit.LargeImageIndex = 1;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnEditClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnRefresh.Caption = "Refresh";
            this.btnRefresh.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnRefresh.Id = 2;
            this.btnRefresh.LargeImageIndex = 2;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnRefreshClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 3;
            this.btnClose.LargeImageIndex = 3;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCloseClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(624, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 379);
            this.barDockControlBottom.Size = new System.Drawing.Size(624, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 379);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(624, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 379);
            // 
            // ApplicationParametersManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 411);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ApplicationParametersManagement";
            this.Text = "Application Parameters";
            this.Activated += new System.EventHandler(this.ApplicationParametersManagementActivated);
            this.Deactivate += new System.EventHandler(this.ApplicationParametersManagementDeactivate);
            this.Load += new System.EventHandler(this.ApplicationParametersManagementLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAppParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvAppParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemGrdAppParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraGrid.GridControl grdAppParameters;
        private DevExpress.XtraGrid.Views.Grid.GridView grvAppParameters;
        private DevExpress.XtraGrid.Columns.GridColumn gcCode;
        private DevExpress.XtraGrid.Columns.GridColumn gcValue;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlRootGroup;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemGrdAppParameters;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraGrid.Columns.GridColumn gcDescription;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnAdd;
        private DevExpress.XtraBars.BarLargeButtonItem btnEdit;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}