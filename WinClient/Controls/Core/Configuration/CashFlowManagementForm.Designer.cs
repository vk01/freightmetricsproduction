﻿namespace Exis.WinClient.Controls
{
    partial class CashFlowManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CashFlowManagementForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.btnItemButtons = new DevExpress.XtraEditors.ButtonEdit();
            this.btnRemoveFromGroup = new DevExpress.XtraEditors.SimpleButton();
            this.imageListTree16x16 = new DevExpress.Utils.ImageCollection(this.components);
            this.trlGroups = new DevExpress.XtraTreeList.TreeList();
            this.tlcCashFlowObjectName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.rpsiCashFlowObjectName = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnAddRemoveModel = new DevExpress.XtraEditors.ButtonEdit();
            this.btnMoveToGroup = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddRemoveGroup = new DevExpress.XtraEditors.ButtonEdit();
            this.chklItems = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.btnChangeOperator = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveGroupFromModel = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveGroupToModel = new DevExpress.XtraEditors.SimpleButton();
            this.trlModels = new DevExpress.XtraTreeList.TreeList();
            this.tcCashFlowObject = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.rpsiCashFlowObject = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.lcgRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgCashFlowModels = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutTrlModels = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgModelButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBtnAddRemoceModelGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgGroupsAndItems = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgItemsAndButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgCashFlowItems = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgItemsButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemButtons = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgGroupsItemsButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgGroupsAndButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgCashFlowGroups = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgGroupButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBtnAddGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTrlGroups = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgModelGroupButtons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBtnRemoveFromModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBtnMoveToModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBtnChangeOperator = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageList16x16 = new DevExpress.Utils.ImageCollection(this.components);
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnItemButtons.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListTree16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiCashFlowObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddRemoveModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddRemoveGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chklItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiCashFlowObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCashFlowModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrlModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgModelButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnAddRemoceModelGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupsAndItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgItemsAndButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCashFlowItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgItemsButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupsItemsButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupsAndButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCashFlowGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnAddGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrlGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgModelGroupButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnRemoveFromModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnMoveToModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnChangeOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.btnItemButtons);
            this.layoutControl.Controls.Add(this.btnRemoveFromGroup);
            this.layoutControl.Controls.Add(this.trlGroups);
            this.layoutControl.Controls.Add(this.btnAddRemoveModel);
            this.layoutControl.Controls.Add(this.btnMoveToGroup);
            this.layoutControl.Controls.Add(this.btnAddRemoveGroup);
            this.layoutControl.Controls.Add(this.chklItems);
            this.layoutControl.Controls.Add(this.btnChangeOperator);
            this.layoutControl.Controls.Add(this.btnRemoveGroupFromModel);
            this.layoutControl.Controls.Add(this.btnMoveGroupToModel);
            this.layoutControl.Controls.Add(this.trlModels);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1554, 365, 547, 536);
            this.layoutControl.Root = this.lcgRootGroup;
            this.layoutControl.Size = new System.Drawing.Size(902, 427);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // btnItemButtons
            // 
            this.btnItemButtons.Location = new System.Drawing.Point(445, 241);
            this.btnItemButtons.MaximumSize = new System.Drawing.Size(0, 32);
            this.btnItemButtons.MinimumSize = new System.Drawing.Size(0, 32);
            this.btnItemButtons.Name = "btnItemButtons";
            this.btnItemButtons.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.btnItemButtons.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, 0, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnItemButtons.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Add Cash Flow Item", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnItemButtons.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Edit Cash Flow Item", "Edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnItemButtons.Properties.Buttons2"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "View Cash Flow Item", "View", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, 0, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnItemButtons.Properties.Buttons3"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "Delete Cash Flow Item", "Delete", null, true)});
            this.btnItemButtons.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnItemButtons.Size = new System.Drawing.Size(442, 22);
            this.btnItemButtons.StyleController = this.layoutControl;
            this.btnItemButtons.TabIndex = 17;
            this.btnItemButtons.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnItemButtonsButtonClick);
            // 
            // btnRemoveFromGroup
            // 
            this.btnRemoveFromGroup.ImageIndex = 2;
            this.btnRemoveFromGroup.ImageList = this.imageListTree16x16;
            this.btnRemoveFromGroup.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveFromGroup.Location = new System.Drawing.Point(676, 185);
            this.btnRemoveFromGroup.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveFromGroup.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveFromGroup.Name = "btnRemoveFromGroup";
            this.btnRemoveFromGroup.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveFromGroup.StyleController = this.layoutControl;
            this.btnRemoveFromGroup.TabIndex = 16;
            this.btnRemoveFromGroup.ToolTip = "Remove Cash FLow Item from Group";
            this.btnRemoveFromGroup.Click += new System.EventHandler(this.BtnRemoveItemFromGroupClick);
            // 
            // imageListTree16x16
            // 
            this.imageListTree16x16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageListTree16x16.ImageStream")));
            this.imageListTree16x16.Images.SetKeyName(0, "add2.png");
            this.imageListTree16x16.Images.SetKeyName(1, "delete2.ico");
            this.imageListTree16x16.Images.SetKeyName(2, "arrow_down_blue.ico");
            this.imageListTree16x16.Images.SetKeyName(3, "arrow_left_blue.ico");
            this.imageListTree16x16.Images.SetKeyName(4, "arrow_right_blue.ico");
            this.imageListTree16x16.Images.SetKeyName(5, "arrow_up_blue.ico");
            this.imageListTree16x16.Images.SetKeyName(6, "changeoperator.png");
            this.imageListTree16x16.Images.SetKeyName(7, "model.png");
            this.imageListTree16x16.Images.SetKeyName(8, "component_add.png");
            this.imageListTree16x16.Images.SetKeyName(9, "groupMinus.png");
            this.imageListTree16x16.Images.SetKeyName(10, "element.ico");
            this.imageListTree16x16.Images.SetKeyName(11, "group.ico");
            // 
            // trlGroups
            // 
            this.trlGroups.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlcCashFlowObjectName});
            this.trlGroups.Location = new System.Drawing.Point(445, 70);
            this.trlGroups.Name = "trlGroups";
            this.trlGroups.OptionsBehavior.Editable = false;
            this.trlGroups.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.trlGroups.OptionsView.ShowColumns = false;
            this.trlGroups.OptionsView.ShowIndicator = false;
            this.trlGroups.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpsiCashFlowObjectName});
            this.trlGroups.SelectImageList = this.imageListTree16x16;
            this.trlGroups.Size = new System.Drawing.Size(442, 108);
            this.trlGroups.TabIndex = 18;
            this.trlGroups.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.TrlGroupsFocusedNodeChanged);
            this.trlGroups.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.TrlGroupsCustomDrawNodeCell);
            // 
            // tlcCashFlowObjectName
            // 
            this.tlcCashFlowObjectName.Caption = "CashFlowObjectName";
            this.tlcCashFlowObjectName.FieldName = "CashFlowObjectName";
            this.tlcCashFlowObjectName.MinWidth = 33;
            this.tlcCashFlowObjectName.Name = "tlcCashFlowObjectName";
            this.tlcCashFlowObjectName.Visible = true;
            this.tlcCashFlowObjectName.VisibleIndex = 0;
            // 
            // rpsiCashFlowObjectName
            // 
            this.rpsiCashFlowObjectName.AutoHeight = false;
            this.rpsiCashFlowObjectName.Name = "rpsiCashFlowObjectName";
            this.rpsiCashFlowObjectName.ReadOnly = true;
            // 
            // btnAddRemoveModel
            // 
            this.btnAddRemoveModel.EditValue = global::Exis.WinClient.Strings.Eve;
            this.btnAddRemoveModel.Location = new System.Drawing.Point(15, 34);
            this.btnAddRemoveModel.MaximumSize = new System.Drawing.Size(0, 32);
            this.btnAddRemoveModel.MinimumSize = new System.Drawing.Size(0, 32);
            this.btnAddRemoveModel.Name = "btnAddRemoveModel";
            this.btnAddRemoveModel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.btnAddRemoveModel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.btnAddRemoveModel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.btnAddRemoveModel.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.btnAddRemoveModel.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.btnAddRemoveModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, 0, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "Add new Model", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "Edit Model name", "Edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, 0, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons2"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "Delete Model", "Delete", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons3"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject8, "Save Model", "Save", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Change Status", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons4"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, "Toggle status", "Status", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons5"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject10, "Increase Group Order", "MoveUp", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveModel.Properties.Buttons6"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject11, "Decrease Group Order", "MoveDown", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Cancel", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject12, global::Exis.WinClient.Strings.Eve, "Close", null, true)});
            this.btnAddRemoveModel.Properties.NullValuePrompt = "Model Name";
            this.btnAddRemoveModel.Properties.NullValuePromptShowForEmptyValue = true;
            this.btnAddRemoveModel.Size = new System.Drawing.Size(386, 22);
            this.btnAddRemoveModel.StyleController = this.layoutControl;
            this.btnAddRemoveModel.TabIndex = 17;
            this.btnAddRemoveModel.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnAddRemoveModelButtonClick);
            // 
            // btnMoveToGroup
            // 
            this.btnMoveToGroup.ImageIndex = 5;
            this.btnMoveToGroup.ImageList = this.imageListTree16x16;
            this.btnMoveToGroup.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveToGroup.Location = new System.Drawing.Point(642, 185);
            this.btnMoveToGroup.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnMoveToGroup.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnMoveToGroup.Name = "btnMoveToGroup";
            this.btnMoveToGroup.Size = new System.Drawing.Size(30, 30);
            this.btnMoveToGroup.StyleController = this.layoutControl;
            this.btnMoveToGroup.TabIndex = 15;
            this.btnMoveToGroup.ToolTip = "Add Cash Flow Item to Group";
            this.btnMoveToGroup.Click += new System.EventHandler(this.BtnMoveItemToGroupClick);
            // 
            // btnAddRemoveGroup
            // 
            this.btnAddRemoveGroup.Location = new System.Drawing.Point(445, 34);
            this.btnAddRemoveGroup.MaximumSize = new System.Drawing.Size(0, 32);
            this.btnAddRemoveGroup.MinimumSize = new System.Drawing.Size(0, 32);
            this.btnAddRemoveGroup.Name = "btnAddRemoveGroup";
            this.btnAddRemoveGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.btnAddRemoveGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, 0, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, "Add new Group", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject14, "Edit Group name", "Edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, 0, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons2"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject15, "Delete Group", "Delete", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons3"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject16, "Save Group", "Save", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Change Status", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons4"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, "Toggle Status", "Status", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons5"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject18, "Increase Item Order", "MoveUp", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnAddRemoveGroup.Properties.Buttons6"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject19, "Decrease Item Order", "MoveDown", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Cancel", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject20, global::Exis.WinClient.Strings.Eve, "Close", null, true)});
            this.btnAddRemoveGroup.Properties.NullValuePrompt = "Group Name";
            this.btnAddRemoveGroup.Properties.NullValuePromptShowForEmptyValue = true;
            this.btnAddRemoveGroup.Size = new System.Drawing.Size(442, 22);
            this.btnAddRemoveGroup.StyleController = this.layoutControl;
            this.btnAddRemoveGroup.TabIndex = 16;
            this.btnAddRemoveGroup.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnAddRemoveGroupButtonClick);
            // 
            // chklItems
            // 
            this.chklItems.CheckOnClick = true;
            this.chklItems.Location = new System.Drawing.Point(445, 277);
            this.chklItems.Name = "chklItems";
            this.chklItems.Size = new System.Drawing.Size(442, 135);
            this.chklItems.StyleController = this.layoutControl;
            this.chklItems.TabIndex = 6;
            // 
            // btnChangeOperator
            // 
            this.btnChangeOperator.ImageIndex = 6;
            this.btnChangeOperator.ImageList = this.imageListTree16x16;
            this.btnChangeOperator.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChangeOperator.Location = new System.Drawing.Point(408, 139);
            this.btnChangeOperator.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnChangeOperator.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnChangeOperator.Name = "btnChangeOperator";
            this.btnChangeOperator.Size = new System.Drawing.Size(30, 30);
            this.btnChangeOperator.StyleController = this.layoutControl;
            this.btnChangeOperator.TabIndex = 15;
            this.btnChangeOperator.ToolTip = "Change Operator";
            this.btnChangeOperator.Click += new System.EventHandler(this.BtnChangeOperatorClick);
            // 
            // btnRemoveGroupFromModel
            // 
            this.btnRemoveGroupFromModel.ImageIndex = 4;
            this.btnRemoveGroupFromModel.ImageList = this.imageListTree16x16;
            this.btnRemoveGroupFromModel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnRemoveGroupFromModel.Location = new System.Drawing.Point(408, 105);
            this.btnRemoveGroupFromModel.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveGroupFromModel.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveGroupFromModel.Name = "btnRemoveGroupFromModel";
            this.btnRemoveGroupFromModel.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveGroupFromModel.StyleController = this.layoutControl;
            this.btnRemoveGroupFromModel.TabIndex = 12;
            this.btnRemoveGroupFromModel.ToolTip = "Remove Group from Model";
            this.btnRemoveGroupFromModel.Click += new System.EventHandler(this.BtnRemoveGroupFromModelClick);
            // 
            // btnMoveGroupToModel
            // 
            this.btnMoveGroupToModel.ImageIndex = 3;
            this.btnMoveGroupToModel.ImageList = this.imageListTree16x16;
            this.btnMoveGroupToModel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMoveGroupToModel.Location = new System.Drawing.Point(408, 71);
            this.btnMoveGroupToModel.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnMoveGroupToModel.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnMoveGroupToModel.Name = "btnMoveGroupToModel";
            this.btnMoveGroupToModel.Size = new System.Drawing.Size(30, 30);
            this.btnMoveGroupToModel.StyleController = this.layoutControl;
            this.btnMoveGroupToModel.TabIndex = 11;
            this.btnMoveGroupToModel.ToolTip = "Move Group to Model";
            this.btnMoveGroupToModel.Click += new System.EventHandler(this.BtnMoveGroupToModelClick);
            // 
            // trlModels
            // 
            this.trlModels.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tcCashFlowObject});
            this.trlModels.Location = new System.Drawing.Point(15, 70);
            this.trlModels.Name = "trlModels";
            this.trlModels.OptionsBehavior.Editable = false;
            this.trlModels.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.trlModels.OptionsView.ShowColumns = false;
            this.trlModels.OptionsView.ShowIndicator = false;
            this.trlModels.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpsiCashFlowObject});
            this.trlModels.SelectImageList = this.imageListTree16x16;
            this.trlModels.Size = new System.Drawing.Size(386, 342);
            this.trlModels.TabIndex = 4;
            this.trlModels.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.TrlModelsFocusedNodeChanged);
            this.trlModels.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.TrlModelsCustomDrawNodeCell);
            // 
            // tcCashFlowObject
            // 
            this.tcCashFlowObject.Caption = "Cash Flow Object";
            this.tcCashFlowObject.ColumnEdit = this.rpsiCashFlowObject;
            this.tcCashFlowObject.FieldName = "CashFlowObject";
            this.tcCashFlowObject.MinWidth = 33;
            this.tcCashFlowObject.Name = "tcCashFlowObject";
            this.tcCashFlowObject.Visible = true;
            this.tcCashFlowObject.VisibleIndex = 0;
            // 
            // rpsiCashFlowObject
            // 
            this.rpsiCashFlowObject.AutoHeight = false;
            this.rpsiCashFlowObject.Name = "rpsiCashFlowObject";
            // 
            // lcgRootGroup
            // 
            this.lcgRootGroup.CustomizationFormText = "lcgRootGroup";
            this.lcgRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRootGroup.GroupBordersVisible = false;
            this.lcgRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgCashFlowModels,
            this.lcgGroupsAndItems,
            this.lcgModelGroupButtons});
            this.lcgRootGroup.Location = new System.Drawing.Point(0, 0);
            this.lcgRootGroup.Name = "lcgRootGroup";
            this.lcgRootGroup.Size = new System.Drawing.Size(902, 427);
            this.lcgRootGroup.Text = "lcgRootGroup";
            this.lcgRootGroup.TextVisible = false;
            // 
            // lcgCashFlowModels
            // 
            this.lcgCashFlowModels.CustomizationFormText = "Cash Flow Models";
            this.lcgCashFlowModels.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutTrlModels,
            this.lcgModelButtons});
            this.lcgCashFlowModels.Location = new System.Drawing.Point(0, 0);
            this.lcgCashFlowModels.Name = "lcgCashFlowModels";
            this.lcgCashFlowModels.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgCashFlowModels.Size = new System.Drawing.Size(396, 407);
            this.lcgCashFlowModels.Text = "Cash Flow Models";
            // 
            // layoutTrlModels
            // 
            this.layoutTrlModels.Control = this.trlModels;
            this.layoutTrlModels.CustomizationFormText = "Cash Flow Models";
            this.layoutTrlModels.Location = new System.Drawing.Point(0, 36);
            this.layoutTrlModels.Name = "layoutTrlModels";
            this.layoutTrlModels.Size = new System.Drawing.Size(390, 346);
            this.layoutTrlModels.Text = "Cash Flow Models";
            this.layoutTrlModels.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutTrlModels.TextSize = new System.Drawing.Size(0, 0);
            this.layoutTrlModels.TextToControlDistance = 0;
            this.layoutTrlModels.TextVisible = false;
            // 
            // lcgModelButtons
            // 
            this.lcgModelButtons.CustomizationFormText = "lcgModelButtons";
            this.lcgModelButtons.GroupBordersVisible = false;
            this.lcgModelButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBtnAddRemoceModelGroup});
            this.lcgModelButtons.Location = new System.Drawing.Point(0, 0);
            this.lcgModelButtons.Name = "lcgModelButtons";
            this.lcgModelButtons.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgModelButtons.Size = new System.Drawing.Size(390, 36);
            this.lcgModelButtons.Text = "lcgModelButtons";
            // 
            // layoutBtnAddRemoceModelGroup
            // 
            this.layoutBtnAddRemoceModelGroup.Control = this.btnAddRemoveModel;
            this.layoutBtnAddRemoceModelGroup.CustomizationFormText = "layoutBtnAddRemoceModelGroup";
            this.layoutBtnAddRemoceModelGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutBtnAddRemoceModelGroup.Name = "layoutBtnAddRemoceModelGroup";
            this.layoutBtnAddRemoceModelGroup.Size = new System.Drawing.Size(390, 36);
            this.layoutBtnAddRemoceModelGroup.Text = "layoutBtnAddRemoceModelGroup";
            this.layoutBtnAddRemoceModelGroup.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBtnAddRemoceModelGroup.TextToControlDistance = 0;
            this.layoutBtnAddRemoceModelGroup.TextVisible = false;
            // 
            // lcgGroupsAndItems
            // 
            this.lcgGroupsAndItems.CustomizationFormText = "lcgGroupsAndItems";
            this.lcgGroupsAndItems.GroupBordersVisible = false;
            this.lcgGroupsAndItems.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgItemsAndButtons,
            this.lcgGroupsAndButtons});
            this.lcgGroupsAndItems.Location = new System.Drawing.Point(430, 0);
            this.lcgGroupsAndItems.Name = "lcgGroupsAndItems";
            this.lcgGroupsAndItems.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgGroupsAndItems.Size = new System.Drawing.Size(452, 407);
            this.lcgGroupsAndItems.Text = "lcgGroupsAndItems";
            // 
            // lcgItemsAndButtons
            // 
            this.lcgItemsAndButtons.CustomizationFormText = "lcgItemsAndButtons";
            this.lcgItemsAndButtons.GroupBordersVisible = false;
            this.lcgItemsAndButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgCashFlowItems,
            this.lcgGroupsItemsButtons});
            this.lcgItemsAndButtons.Location = new System.Drawing.Point(0, 173);
            this.lcgItemsAndButtons.Name = "lcgItemsAndButtons";
            this.lcgItemsAndButtons.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgItemsAndButtons.Size = new System.Drawing.Size(452, 234);
            this.lcgItemsAndButtons.Text = "lcgItemsAndButtons";
            // 
            // lcgCashFlowItems
            // 
            this.lcgCashFlowItems.CustomizationFormText = "Cash Flow Items";
            this.lcgCashFlowItems.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgItemsButtons,
            this.layoutControlItem7});
            this.lcgCashFlowItems.Location = new System.Drawing.Point(0, 34);
            this.lcgCashFlowItems.Name = "lcgCashFlowItems";
            this.lcgCashFlowItems.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgCashFlowItems.Size = new System.Drawing.Size(452, 200);
            this.lcgCashFlowItems.Text = "Cash Flow Items";
            // 
            // lcgItemsButtons
            // 
            this.lcgItemsButtons.CustomizationFormText = "lcgItemsButtons";
            this.lcgItemsButtons.GroupBordersVisible = false;
            this.lcgItemsButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemButtons});
            this.lcgItemsButtons.Location = new System.Drawing.Point(0, 0);
            this.lcgItemsButtons.Name = "lcgItemsButtons";
            this.lcgItemsButtons.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgItemsButtons.Size = new System.Drawing.Size(446, 36);
            this.lcgItemsButtons.Text = "lcgItemsButtons";
            // 
            // layoutItemButtons
            // 
            this.layoutItemButtons.Control = this.btnItemButtons;
            this.layoutItemButtons.CustomizationFormText = "layoutItemButtons";
            this.layoutItemButtons.Location = new System.Drawing.Point(0, 0);
            this.layoutItemButtons.Name = "layoutItemButtons";
            this.layoutItemButtons.Size = new System.Drawing.Size(446, 36);
            this.layoutItemButtons.Text = "layoutItemButtons";
            this.layoutItemButtons.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemButtons.TextToControlDistance = 0;
            this.layoutItemButtons.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.chklItems;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(446, 139);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // lcgGroupsItemsButtons
            // 
            this.lcgGroupsItemsButtons.CustomizationFormText = "lcgGroupsItemsButtons";
            this.lcgGroupsItemsButtons.GroupBordersVisible = false;
            this.lcgGroupsItemsButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.emptySpaceItem2,
            this.emptySpaceItem4});
            this.lcgGroupsItemsButtons.Location = new System.Drawing.Point(0, 0);
            this.lcgGroupsItemsButtons.Name = "lcgGroupsItemsButtons";
            this.lcgGroupsItemsButtons.Size = new System.Drawing.Size(452, 34);
            this.lcgGroupsItemsButtons.Text = "lcgGroupsItemsButtons";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnRemoveFromGroup;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(234, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(34, 34);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnMoveToGroup;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(34, 34);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(200, 34);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(268, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(184, 34);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgGroupsAndButtons
            // 
            this.lcgGroupsAndButtons.CustomizationFormText = "lcgGroupsAndButtons";
            this.lcgGroupsAndButtons.GroupBordersVisible = false;
            this.lcgGroupsAndButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgCashFlowGroups});
            this.lcgGroupsAndButtons.Location = new System.Drawing.Point(0, 0);
            this.lcgGroupsAndButtons.Name = "lcgGroupsAndButtons";
            this.lcgGroupsAndButtons.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgGroupsAndButtons.Size = new System.Drawing.Size(452, 173);
            this.lcgGroupsAndButtons.Text = "lcgGroupsAndButtons";
            // 
            // lcgCashFlowGroups
            // 
            this.lcgCashFlowGroups.CustomizationFormText = "Cash Flow Groups";
            this.lcgCashFlowGroups.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgGroupButtons,
            this.layoutTrlGroups});
            this.lcgCashFlowGroups.Location = new System.Drawing.Point(0, 0);
            this.lcgCashFlowGroups.Name = "lcgCashFlowGroups";
            this.lcgCashFlowGroups.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgCashFlowGroups.Size = new System.Drawing.Size(452, 173);
            this.lcgCashFlowGroups.Text = "Cash Flow Groups";
            // 
            // lcgGroupButtons
            // 
            this.lcgGroupButtons.CustomizationFormText = "lcgGroupButtons";
            this.lcgGroupButtons.GroupBordersVisible = false;
            this.lcgGroupButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBtnAddGroup});
            this.lcgGroupButtons.Location = new System.Drawing.Point(0, 0);
            this.lcgGroupButtons.Name = "lcgGroupButtons";
            this.lcgGroupButtons.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgGroupButtons.Size = new System.Drawing.Size(446, 36);
            this.lcgGroupButtons.Text = "lcgGroupButtons";
            // 
            // layoutBtnAddGroup
            // 
            this.layoutBtnAddGroup.Control = this.btnAddRemoveGroup;
            this.layoutBtnAddGroup.CustomizationFormText = "layoutBtnAddGroup";
            this.layoutBtnAddGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutBtnAddGroup.Name = "layoutBtnAddGroup";
            this.layoutBtnAddGroup.Size = new System.Drawing.Size(446, 36);
            this.layoutBtnAddGroup.Text = "layoutBtnAddGroup";
            this.layoutBtnAddGroup.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBtnAddGroup.TextToControlDistance = 0;
            this.layoutBtnAddGroup.TextVisible = false;
            // 
            // layoutTrlGroups
            // 
            this.layoutTrlGroups.Control = this.trlGroups;
            this.layoutTrlGroups.CustomizationFormText = "layoutTrlGroups";
            this.layoutTrlGroups.Location = new System.Drawing.Point(0, 36);
            this.layoutTrlGroups.MinSize = new System.Drawing.Size(104, 24);
            this.layoutTrlGroups.Name = "layoutTrlGroups";
            this.layoutTrlGroups.Size = new System.Drawing.Size(446, 112);
            this.layoutTrlGroups.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutTrlGroups.Text = "layoutTrlGroups";
            this.layoutTrlGroups.TextSize = new System.Drawing.Size(0, 0);
            this.layoutTrlGroups.TextToControlDistance = 0;
            this.layoutTrlGroups.TextVisible = false;
            // 
            // lcgModelGroupButtons
            // 
            this.lcgModelGroupButtons.CustomizationFormText = "lcgModelGroupButtons";
            this.lcgModelGroupButtons.GroupBordersVisible = false;
            this.lcgModelGroupButtons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBtnRemoveFromModel,
            this.layoutBtnMoveToModel,
            this.layoutBtnChangeOperator,
            this.emptySpaceItem3,
            this.emptySpaceItem5});
            this.lcgModelGroupButtons.Location = new System.Drawing.Point(396, 0);
            this.lcgModelGroupButtons.Name = "lcgModelGroupButtons";
            this.lcgModelGroupButtons.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgModelGroupButtons.Size = new System.Drawing.Size(34, 407);
            this.lcgModelGroupButtons.Text = "lcgModelGroupButtons";
            // 
            // layoutBtnRemoveFromModel
            // 
            this.layoutBtnRemoveFromModel.Control = this.btnRemoveGroupFromModel;
            this.layoutBtnRemoveFromModel.CustomizationFormText = "layoutBtnRemoveFromModel";
            this.layoutBtnRemoveFromModel.Location = new System.Drawing.Point(0, 93);
            this.layoutBtnRemoveFromModel.Name = "layoutBtnRemoveFromModel";
            this.layoutBtnRemoveFromModel.Size = new System.Drawing.Size(34, 34);
            this.layoutBtnRemoveFromModel.Text = "layoutBtnRemoveFromModel";
            this.layoutBtnRemoveFromModel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBtnRemoveFromModel.TextToControlDistance = 0;
            this.layoutBtnRemoveFromModel.TextVisible = false;
            // 
            // layoutBtnMoveToModel
            // 
            this.layoutBtnMoveToModel.Control = this.btnMoveGroupToModel;
            this.layoutBtnMoveToModel.CustomizationFormText = "layoutBtnMoveToModel";
            this.layoutBtnMoveToModel.Location = new System.Drawing.Point(0, 59);
            this.layoutBtnMoveToModel.Name = "layoutBtnMoveToModel";
            this.layoutBtnMoveToModel.Size = new System.Drawing.Size(34, 34);
            this.layoutBtnMoveToModel.Text = "layoutBtnMoveToModel";
            this.layoutBtnMoveToModel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBtnMoveToModel.TextToControlDistance = 0;
            this.layoutBtnMoveToModel.TextVisible = false;
            // 
            // layoutBtnChangeOperator
            // 
            this.layoutBtnChangeOperator.Control = this.btnChangeOperator;
            this.layoutBtnChangeOperator.CustomizationFormText = "layoutBtnChangeOperator";
            this.layoutBtnChangeOperator.Location = new System.Drawing.Point(0, 127);
            this.layoutBtnChangeOperator.Name = "layoutBtnChangeOperator";
            this.layoutBtnChangeOperator.Size = new System.Drawing.Size(34, 34);
            this.layoutBtnChangeOperator.Text = "layoutBtnChangeOperator";
            this.layoutBtnChangeOperator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBtnChangeOperator.TextToControlDistance = 0;
            this.layoutBtnChangeOperator.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 161);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(34, 246);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 59);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 59);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(34, 59);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // imageList16x16
            // 
            this.imageList16x16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16x16.ImageStream")));
            this.imageList16x16.Images.SetKeyName(0, "save.ico");
            this.imageList16x16.Images.SetKeyName(1, "exit.ico");
            this.imageList16x16.Images.SetKeyName(2, "edit.ico");
            this.imageList16x16.Images.SetKeyName(3, "view.ico");
            this.imageList16x16.Images.SetKeyName(4, "moveDown.ico");
            this.imageList16x16.Images.SetKeyName(5, "moveUp.ico");
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(902, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 427);
            this.barDockControlBottom.Size = new System.Drawing.Size(902, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 427);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(902, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 427);
            // 
            // CashFlowManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 459);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "CashFlowManagementForm";
            this.Text = "Cash Flows";
            this.Load += new System.EventHandler(this.CashFlowManagementFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnItemButtons.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListTree16x16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiCashFlowObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddRemoveModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddRemoveGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chklItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlModels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsiCashFlowObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCashFlowModels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrlModels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgModelButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnAddRemoceModelGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupsAndItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgItemsAndButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCashFlowItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgItemsButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupsItemsButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupsAndButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCashFlowGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGroupButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnAddGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrlGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgModelGroupButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnRemoveFromModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnMoveToModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnChangeOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16x16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRootGroup;
        private DevExpress.XtraEditors.CheckedListBoxControl chklItems;
        private DevExpress.XtraTreeList.TreeList trlModels;
        private DevExpress.XtraLayout.LayoutControlGroup lcgCashFlowModels;
        private DevExpress.XtraLayout.LayoutControlItem layoutTrlModels;
        private DevExpress.XtraLayout.LayoutControlGroup lcgCashFlowItems;
        private DevExpress.XtraEditors.SimpleButton btnMoveGroupToModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnMoveToModel;
        private DevExpress.XtraEditors.SimpleButton btnRemoveGroupFromModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnRemoveFromModel;
        private DevExpress.XtraEditors.SimpleButton btnChangeOperator;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnChangeOperator;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tcCashFlowObject;
        private DevExpress.XtraLayout.LayoutControlGroup lcgItemsButtons;
        private DevExpress.XtraLayout.LayoutControlGroup lcgCashFlowGroups;
        private DevExpress.XtraLayout.LayoutControlGroup lcgGroupButtons;
        private DevExpress.XtraLayout.LayoutControlGroup lcgModelGroupButtons;
        private DevExpress.XtraLayout.LayoutControlGroup lcgGroupsAndItems;
        private DevExpress.XtraLayout.LayoutControlGroup lcgModelButtons;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rpsiCashFlowObject;
        private DevExpress.XtraEditors.ButtonEdit btnAddRemoveGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnAddGroup;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.ButtonEdit btnAddRemoveModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnAddRemoceModelGroup;
        private DevExpress.XtraTreeList.TreeList trlGroups;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcCashFlowObjectName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rpsiCashFlowObjectName;
        private DevExpress.XtraLayout.LayoutControlItem layoutTrlGroups;
        private DevExpress.XtraEditors.SimpleButton btnRemoveFromGroup;
        private DevExpress.XtraEditors.SimpleButton btnMoveToGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup lcgItemsAndButtons;
        private DevExpress.XtraLayout.LayoutControlGroup lcgGroupsAndButtons;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.Utils.ImageCollection imageList16x16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControlGroup lcgGroupsItemsButtons;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.Utils.ImageCollection imageListTree16x16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.ButtonEdit btnItemButtons;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemButtons;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}