﻿namespace Exis.WinClient.Controls
{
    partial class BookAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.cmbPositionCalculatedByDays = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.treeParentBook = new DevExpress.XtraTreeList.TreeList();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupBook = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutParentBook = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionCalculatedDays = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpace1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionCalculatedByDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeParentBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutParentBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionCalculatedDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpace1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.cmbPositionCalculatedByDays);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.treeParentBook);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtName);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 426, 250, 350);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(1336, 752);
            this.layoutRootControl.TabIndex = 1;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // cmbPositionCalculatedByDays
            // 
            this.cmbPositionCalculatedByDays.Location = new System.Drawing.Point(1272, 36);
            this.cmbPositionCalculatedByDays.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPositionCalculatedByDays.Name = "cmbPositionCalculatedByDays";
            this.cmbPositionCalculatedByDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPositionCalculatedByDays.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPositionCalculatedByDays.Size = new System.Drawing.Size(50, 22);
            this.cmbPositionCalculatedByDays.StyleController = this.layoutRootControl;
            this.cmbPositionCalculatedByDays.TabIndex = 31;
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(1231, 370);
            this.dtpUpdateDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Size = new System.Drawing.Size(91, 22);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 8;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(703, 370);
            this.txtUpdateUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(446, 22);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 11;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(532, 370);
            this.dtpCreationDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Size = new System.Drawing.Size(89, 22);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 7;
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(100, 370);
            this.txtCreationUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(342, 22);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 10;
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(33, 36);
            this.lblId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(79, 20);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // treeParentBook
            // 
            this.treeParentBook.Location = new System.Drawing.Point(90, 62);
            this.treeParentBook.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeParentBook.Name = "treeParentBook";
            this.treeParentBook.OptionsBehavior.Editable = false;
            this.treeParentBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeParentBook.OptionsView.ShowColumns = false;
            this.treeParentBook.OptionsView.ShowHorzLines = false;
            this.treeParentBook.OptionsView.ShowIndicator = false;
            this.treeParentBook.OptionsView.ShowVertLines = false;
            this.treeParentBook.Size = new System.Drawing.Size(1232, 258);
            this.treeParentBook.TabIndex = 14;
            this.treeParentBook.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeParentBook_FocusedNodeChanged);
            this.treeParentBook.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeParentBook_KeyDown);
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(1003, 36);
            this.cmbStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(95, 22);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(157, 36);
            this.txtName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtName.Name = "txtName";
            this.txtName.Properties.MaxLength = 1000;
            this.txtName.Size = new System.Drawing.Size(798, 22);
            this.txtName.StyleController = this.layoutRootControl;
            this.txtName.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupBook,
            this.layoutGroupUserInfo,
            this.emptySpace1});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(1336, 752);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupBook
            // 
            this.layoutGroupBook.CustomizationFormText = "Company";
            this.layoutGroupBook.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutId,
            this.layoutStatus,
            this.layoutParentBook,
            this.layoutPositionCalculatedDays});
            this.layoutGroupBook.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupBook.Name = "layoutGroupBook";
            this.layoutGroupBook.Size = new System.Drawing.Size(1336, 334);
            this.layoutGroupBook.Text = "Book";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "Name:";
            this.layoutName.Location = new System.Drawing.Point(102, 0);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(843, 26);
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(38, 16);
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MaxSize = new System.Drawing.Size(102, 24);
            this.layoutId.MinSize = new System.Drawing.Size(102, 24);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(102, 26);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(16, 16);
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(945, 0);
            this.layoutStatus.MaxSize = new System.Drawing.Size(193, 24);
            this.layoutStatus.MinSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(143, 26);
            this.layoutStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextSize = new System.Drawing.Size(41, 16);
            // 
            // layoutParentBook
            // 
            this.layoutParentBook.Control = this.treeParentBook;
            this.layoutParentBook.CustomizationFormText = "Parent Company:";
            this.layoutParentBook.Location = new System.Drawing.Point(0, 26);
            this.layoutParentBook.MinSize = new System.Drawing.Size(1, 204);
            this.layoutParentBook.Name = "layoutParentBook";
            this.layoutParentBook.Size = new System.Drawing.Size(1312, 262);
            this.layoutParentBook.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutParentBook.Text = "Parent Book:";
            this.layoutParentBook.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutPositionCalculatedDays
            // 
            this.layoutPositionCalculatedDays.Control = this.cmbPositionCalculatedByDays;
            this.layoutPositionCalculatedDays.CustomizationFormText = "Position calculated by (days):";
            this.layoutPositionCalculatedDays.Location = new System.Drawing.Point(1088, 0);
            this.layoutPositionCalculatedDays.Name = "layoutPositionCalculatedDays";
            this.layoutPositionCalculatedDays.Size = new System.Drawing.Size(224, 26);
            this.layoutPositionCalculatedDays.Text = "Position calculated by (days):";
            this.layoutPositionCalculatedDays.TextSize = new System.Drawing.Size(167, 16);
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "User Info";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 334);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(1336, 72);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "Creation User:";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(432, 26);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(83, 16);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(432, 0);
            this.layoutCreationDate.MaxSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.MinSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(179, 26);
            this.layoutCreationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(83, 16);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(611, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(528, 26);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(1139, 0);
            this.layoutUpdateDate.MaxSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.MinSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(173, 26);
            this.layoutUpdateDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(75, 16);
            // 
            // emptySpace1
            // 
            this.emptySpace1.AllowHotTrack = false;
            this.emptySpace1.CustomizationFormText = "emptySpace1";
            this.emptySpace1.Location = new System.Drawing.Point(0, 406);
            this.emptySpace1.Name = "emptySpace1";
            this.emptySpace1.Size = new System.Drawing.Size(1336, 346);
            this.emptySpace1.Text = "emptySpace1";
            this.emptySpace1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnClose,
            this.btnSave});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 4;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1336, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 752);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1336, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 752);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1336, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 752);
            // 
            // BookAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 790);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BookAEVForm";
            this.Activated += new System.EventHandler(this.BookAEVForm_Activated);
            this.Deactivate += new System.EventHandler(this.BookAEVForm_Deactivate);
            this.Load += new System.EventHandler(this.BookAEVForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionCalculatedByDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeParentBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutParentBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionCalculatedDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpace1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraTreeList.TreeList treeParentBook;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupBook;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutParentBook;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpace1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPositionCalculatedByDays;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionCalculatedDays;
    }
}