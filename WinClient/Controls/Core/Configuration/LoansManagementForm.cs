﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class LoansManagementForm : XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public LoansManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void BtnAddClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof(Loan));
        }

        private void BtnEditClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridLoansMainView.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((Loan)gridLoansMainView.GetFocusedRow());
        }

        private void BtnViewClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridLoansMainView.GetFocusedRow() == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((Loan) gridLoansMainView.GetFocusedRow());
        }

        private void BtnRefreshClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetLoans();
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void BtnManageInstallmentsClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridLoansMainView.GetFocusedRow() == null) return;

            if (ManageInstallmentsEvent != null) ManageInstallmentsEvent((Loan)gridLoansMainView.GetFocusedRow());
        }

        private void LoansManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetLoans();
        }

        private void LoansManagementFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void LoansManagementFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridLoans.BeginUpdate();

            int gridViewColumnindex = 0;

            gridLoansMainView.Columns.Clear();

            gridLoansMainView.Columns.Add(new GridColumn
                                              {
                                                  Caption = Strings.External_Code,
                                                  FieldName = "ExternalCode",
                                                  VisibleIndex = gridViewColumnindex++
                                              });

            gridLoansMainView.Columns.Add(new GridColumn
                                                       {
                                                           Caption = Strings.Amount,
                                                           FieldName = "Amount",
                                                           VisibleIndex = gridViewColumnindex++
                                                       });
            gridLoansMainView.Columns.Add(new GridColumn
                                                       {
                                                           Caption = Strings.Status,
                                                           FieldName = "Status",
                                                           VisibleIndex = gridViewColumnindex
                                                       });

            gridLoansMainView.Columns.Add(new GridColumn
                                              {
                                                  Caption = Strings.Sign_Date,
                                                  FieldName = "SignDate",
                                                  VisibleIndex = gridViewColumnindex
                                              });

            gridLoansMainView.Columns.Add(new GridColumn
                                              {
                                                  Caption = Strings.Creation_User,
                                                  FieldName = "Cruser",
                                                  VisibleIndex = gridViewColumnindex
                                              });

            gridLoans.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetLoans()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new Loan(), EndGetLoans, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetLoans(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetLoans;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridLoans.DataSource = entities.Cast<Loan>().ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<Loan> ManageInstallmentsEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion

    }
}