﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.Controls.Core;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using DTG.Spreadsheet;
using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;
using System.ServiceModel;
using System.Text;

namespace Exis.WinClient.Controls
{
    public partial class BalticExchangeDataInputForm : DevExpress.XtraEditors.XtraForm
    {

        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        private const string licenceCode = "SOZT-CSWE-M2SN-CXMT";

        private List<Index> _indexes;
        private List<AppParameter> _appParameters;
        private DataTable dtErrors;
        private bool isFtpDownload;
        private DateTime lastDownLoadDate;
        private DateTime lastImportDate;

        const string ftpArchiveUri = "BALTIC_EXCHANGE_FTP_ARCHIVE";
        const string ftpUri = "BALTIC_EXCHANGE_FTP";
        const string ftpUsername = "BALTIC_EXCHANGE_FTP_USERNAME";
        const string ftpPassword = "BALTIC_EXCHANGE_FTP_PASSWORD";
        const string ftpXmlFolder = "BALTIC_EXHANGE_XML_FOLDER";
        const string ftpLastDownloadDate = "LAST_DOWNLOADED_BALTIC_XML_DATE";
        const string ftpNasdaqRisk = "NASDAQ_NORDIC_FTP_RISK";
        const string ftpNasdaqUri = "NASDAQ_NORDIC_FTP";
        const string ftpNasdaqRiskFile = "NASDAQ_NORDIC_FTP_RISK_FILE";
        const string ftpNasdaqLastDownloadDate = "NASDAQ_NORDIC_LAST_DOWNLOAD_DATE";
        const string lastImportIndexesValuesDate = "LAST_IMPORT_INDEXES_VALUES_DATE";


        private OpenFileDialog ffaDialog;
        private OpenFileDialog boaDialog;

        private Dictionary<string, int> monthStr = new Dictionary<string, int>();

        #endregion

        #region Constructors

        public BalticExchangeDataInputForm()
        {
            InitializeComponent();
            InitializeControl();
        }

        #endregion

        #region Private Methods

        private void InitializeControl()
        {
            btnImporBalticValues.Enabled = false;
            btnImporBalticValues.Enabled = false;
            btnImportFFAExcel.Enabled = false;
            btnImportSpotExcel.Enabled = false;
            btnImportNasdaqFile.Enabled = false;

            isFtpDownload = false;
            dtErrors = new DataTable("Errors");
            dtErrors.Columns.Add("ErrorType", typeof(string));
            dtErrors.Columns.Add("ErrorDescription", typeof (string));
            
            monthStr.Add("Jan", 1);
            monthStr.Add("Feb", 2);
            monthStr.Add("Mar", 3);
            monthStr.Add("Apr", 4);
            monthStr.Add("May", 5);
            monthStr.Add("Jun", 6);
            monthStr.Add("Jul", 7);
            monthStr.Add("Aug", 8); 
            monthStr.Add("Sep", 9);
            monthStr.Add("Oct", 10);
            monthStr.Add("Nov", 11);
            monthStr.Add("Dec", 12);

        }

        private void InitializeData()
        {
            BeginBalticExchangeInputDataInitializationData();
        }
        
        private DateTime GetLastBusinessDay(int year, int month)
        {
            DateTime lastBusinessDay;
            var lastOfMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            
            if(lastOfMonth.DayOfWeek == DayOfWeek.Sunday )
                lastBusinessDay = lastOfMonth.AddDays(-2);
            else if(lastOfMonth.DayOfWeek == DayOfWeek.Saturday)
                lastBusinessDay = lastOfMonth.AddDays(-1);
            else
                lastBusinessDay = lastOfMonth;
            
            return lastBusinessDay;
        }

        #endregion

        #region GUI Events

        private void btnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void BtnSelectFfaSpotFileButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (ffaDialog.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = ffaDialog.FileName;

            if (!string.IsNullOrEmpty(ffaDialog.InitialDirectory))
                ffaDialog.InitialDirectory = "";
        }

        private void BtnSelectFfaSpotFileEditValueChanged(object sender, EventArgs e)
        {
            btnImporBalticValues.Enabled = !string.IsNullOrEmpty(((ButtonEdit)sender).EditValue.ToString());
        }

        private void BtnImportFfaSpotValuesClick(object sender, EventArgs e)
        {
            string fileName = btnSelectBalticFile.EditValue.ToString();

            try
            {
                isFtpDownload = false;

                var xmlDocument = new XmlDocument();
                XmlElement objRequestElement;
                StreamReader isoStreamReader = null;

                try
                {
                    isoStreamReader = new StreamReader(fileName);
                }
                catch (Exception exc)
                {
                    XtraMessageBox.Show(this, "There was an error trying to read file " + fileName,
                                   Strings.Freight_Metrics,
                                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (isoStreamReader != null && isoStreamReader.BaseStream.Length > 0)
                {
                    isoStreamReader.BaseStream.Position = 0;
                    xmlDocument.Load(isoStreamReader);
                    objRequestElement = xmlDocument.DocumentElement;

                    isoStreamReader.Close();

                    if (objRequestElement != null)
                    {
                        //parse from server
                        BeginLocalProcessingFileXML(objRequestElement, fileName, _indexes, monthStr, lastDownLoadDate, lastImportDate);
                    }
                    else
                    {
                        //errorMessages.Add("There was an error parsing file " + strDownloadFilepath);
                    } //objRequestElement != null
                }
                else
                {
                    // errorMessages.Add("There was an error trying to read file " + strDownloadFilepath);
                }
            }
            catch (Exception)
            {
                // errorMessages.Add("There was an error parsing file " + strDownloadFilepath);
                XtraMessageBox.Show(this, "There was an error parsing file " + fileName,
                                   Strings.Freight_Metrics,
                                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


        }

        private void BtnSelectFfaExcelFileButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = @"Excel files (*.xls)|*.xls;*.xlsx",
                RestoreDirectory = true,
                Title = Strings.Open_File
            };
            if (ofd.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = ofd.FileName;
        }

        private void BtnSelectFfaExcelFileEditValueChanged(object sender, EventArgs e)
        {
            btnImportFFAExcel.Enabled = !string.IsNullOrEmpty(((ButtonEdit)sender).EditValue.ToString());
        }

        private void BtnImportFFAExcelClick(object sender, EventArgs e)
        {
            if (btnSelectFFAExcelFile.EditValue == null)
            {
                XtraMessageBox.Show(this,
                                    "Select a file to import data from.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ParseFFAExcelFileAndImport();
        }

        private void ParseFFAExcelFileAndImport()
        {
            try
            {
                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbook;
                string fileName = btnSelectFFAExcelFile.EditValue.ToString();
                string extension = Path.GetExtension(fileName);
                switch (extension.ToLower())
                {
                    case ".xlsx":
                        {
                            wbook = ExcelWorkbook.ReadXLSX(fileName);
                            break;
                        }
                    case ".xls":
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                    case ".csv":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName);
                            break;
                        }
                    case ".txt":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                            break;
                        }
                    default:
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                }

                var columnNames = new List<string>()
                                               {
                                                   "Date",
                                                   "RouteId",
                                                   "ReportDesc",
                                                   "Closing Price",
                                                   "Unit"
                                               };

                ExcelCellCollection cells = wbook.Worksheets[0].Cells;
                //Create a dictionary which holds column name and index
                var columnIndexes = new Dictionary<string, int>();
                int dataFirstRow = 0;
                bool found = false;
                int k, l = 0;

                for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                        }
                    }
                    if (found)
                        break;
                }


                string missingNames =
                    columnNames.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                                                                                                        (current,
                                                                                                         columnName) =>
                                                                                                        current +
                                                                                                        columnName +
                                                                                                        ", ");
                if (!String.IsNullOrEmpty(missingNames))
                {
                    XtraMessageBox.Show(this,
                                        "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                                        "' do(es) not exist in the selected file.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var tmpImportFFAIndexValues = new List<ParseExcelIndexValue>();
                for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                {
                    ParseExcelIndexValue indexValue;
                    indexValue = new ParseExcelIndexValue
                    {
                        Date = wbook.Worksheets[0].Cells[i, columnIndexes["Date"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Date"]].Value.
                                            ToString()
                                      : null,
                        RouteId = wbook.Worksheets[0].Cells[i, columnIndexes["RouteId"]].Value != null
                                     ? wbook.Worksheets[0].Cells[i, columnIndexes["RouteId"]].Value.
                                           ToString()
                                     : null,
                        ReportDesc =
                            wbook.Worksheets[0].Cells[i, columnIndexes["ReportDesc"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["ReportDesc"]].Value.
                                      ToString()
                                : null,
                        ClosingPrice = wbook.Worksheets[0].Cells[i, columnIndexes["Closing Price"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Closing Price"]].Value.
                                            ToString()
                                      : null,
                        Unit = wbook.Worksheets[0].Cells[i, columnIndexes["Unit"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Unit"]].Value.
                                            ToString()
                                      : null
                    };
                    tmpImportFFAIndexValues.Add(indexValue);
                }
                if (tmpImportFFAIndexValues.Any(a => a.Date == null || a.RouteId == null || a.ClosingPrice == null || a.ReportDesc == null))
                {
                    XtraMessageBox.Show(this,
                                        "Error occurred while parsing excel file. Error: There are missing data in the selected file.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
                BeginLocalProcessingFileExcel(tmpImportFFAIndexValues, fileName, _indexes, monthStr);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                                    "Error occurred while parsing excel file. Error: '" + exc.Message + "'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnSelectSpotExcelFileButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = @"Excel files (*.xls)|*.xls;*.xlsx",
                RestoreDirectory = true,
                Title = Strings.Open_File
            };
            if (ofd.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = ofd.FileName;
        }

        private void BtnSelectSpotExcelFileEditValueChanged(object sender, EventArgs e)
        {
            btnImportSpotExcel.Enabled = !string.IsNullOrEmpty(((ButtonEdit)sender).EditValue.ToString());
        }

        private void BtnImportSpotExcelClick(object sender, EventArgs e)
        {
            if (btnSelectSpotExcelFile.EditValue == null)
            {
                XtraMessageBox.Show(this,
                                    "Select a file to import data from.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ParseSpotExcelFileAndImport();
        }

        private void ParseSpotExcelFileAndImport()
        {
            try
            {
                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbook;
                string fileName = btnSelectSpotExcelFile.EditValue.ToString();
                string extension = Path.GetExtension(fileName);
                switch (extension.ToLower())
                {
                    case ".xlsx":
                        {
                            wbook = ExcelWorkbook.ReadXLSX(fileName);
                            break;
                        }
                    case ".xls":
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                    case ".csv":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName);
                            break;
                        }
                    case ".txt":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                            break;
                        }
                    default:
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                }

                var columnNames = new List<string>()
                                               {
                                                   "Date",
                                                   "RouteId",
                                                   "Closing Price"
                                               };

                ExcelCellCollection cells = wbook.Worksheets[0].Cells;
                //Create a dictionary which holds column name and index
                var columnIndexes = new Dictionary<string, int>();
                int dataFirstRow = 0;
                bool found = false;
                int k, l = 0;

                for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                        }
                    }
                    if (found)
                        break;
                }


                string missingNames =
                    columnNames.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                                                                                                        (current,
                                                                                                         columnName) =>
                                                                                                        current +
                                                                                                        columnName +
                                                                                                        ", ");
                if (!String.IsNullOrEmpty(missingNames))
                {
                    XtraMessageBox.Show(this,
                                        "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                                        "' do(es) not exist in the selected file.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var tmpImportSpotIndexValues = new List<ParseExcelIndexValue>();
                for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                {
                    ParseExcelIndexValue indexValue;
                    indexValue = new ParseExcelIndexValue
                    {
                        Date = wbook.Worksheets[0].Cells[i, columnIndexes["Date"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Date"]].Value.
                                            ToString()
                                      : null,
                        RouteId = wbook.Worksheets[0].Cells[i, columnIndexes["RouteId"]].Value != null
                                     ? wbook.Worksheets[0].Cells[i, columnIndexes["RouteId"]].Value.
                                           ToString()
                                     : null,                        
                        ClosingPrice = wbook.Worksheets[0].Cells[i, columnIndexes["Closing Price"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Closing Price"]].Value.
                                            ToString()
                                      : null
                    };
                    tmpImportSpotIndexValues.Add(indexValue);
                }
                if (tmpImportSpotIndexValues.Any(a=>a.Date == null || a.RouteId == null || a.ClosingPrice == null))
                {
                    XtraMessageBox.Show(this,
                                        "Error occurred while parsing excel file. Error: There are missing data in the selected file.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                BeginLocalProcessingFileExcel(tmpImportSpotIndexValues, fileName, _indexes, monthStr);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                                    "Error occurred while parsing excel file. Error: '" + exc.Message + "'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BalticExchangeDataInputFormLoad(object sender, EventArgs e)
        {
            InitializeData();
        }        

        private void BtnDownloadFromFtpServerClick(object sender, EventArgs e)
        {
            //Check the application parameters needed for ftp downloading
            if (_appParameters.Where(a => a.Code == ftpArchiveUri).SingleOrDefault() == null ||
                string.IsNullOrEmpty(_appParameters.Where(a => a.Code == ftpArchiveUri).SingleOrDefault().Value.Trim()))
            {
                XtraMessageBox.Show(this,
                                    "Application parameter " + ftpArchiveUri + " is not provided.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_appParameters.Where(a => a.Code == ftpUri).SingleOrDefault() == null ||
                string.IsNullOrEmpty(_appParameters.Where(a => a.Code == ftpUri).SingleOrDefault().Value.Trim()))
            {
                XtraMessageBox.Show(this,
                                    "Application parameter " + ftpUri + " is not provided.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            if (_appParameters.Where(a => a.Code == ftpUsername).SingleOrDefault() == null ||
                string.IsNullOrEmpty(_appParameters.Where(a => a.Code == ftpUsername).SingleOrDefault().Value.Trim()))
            {
                XtraMessageBox.Show(this,
                                    "Application parameter " + ftpUsername + " is not provided.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_appParameters.Where(a => a.Code == ftpPassword).SingleOrDefault() == null ||
                string.IsNullOrEmpty(_appParameters.Where(a => a.Code == ftpPassword).SingleOrDefault().Value.Trim()))
            {
                XtraMessageBox.Show(this,
                                    "Application parameter " + ftpPassword + " is not provided.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault() == null ||
                string.IsNullOrEmpty(_appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault().Value.Trim()))
            {
                XtraMessageBox.Show(this,
                                    "Application parameter " + ftpXmlFolder + " is not provided.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_appParameters.Where(a => a.Code == ftpLastDownloadDate).SingleOrDefault() == null ||
                string.IsNullOrEmpty(
                    _appParameters.Where(a => a.Code == ftpLastDownloadDate).SingleOrDefault().Value.Trim()))
            {
                XtraMessageBox.Show(this,
                                    "Application parameter " + ftpLastDownloadDate + " is not provided.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var serverArchiveUri = _appParameters.Where(a => a.Code == ftpArchiveUri).Single().Value;
            var serverUri = _appParameters.Where(a => a.Code == ftpUri).Single().Value; //Uri serverUri = new Uri("ftp://ftp.balticexchange.org/");
            string downloadPath = _appParameters.Where(a => a.Code == ftpXmlFolder).Single().Value; //const string downloadPath = @"C:\FreightMetrics\";
            var username = _appParameters.Where(a => a.Code == ftpUsername).Single().Value;
            var password = _appParameters.Where(a => a.Code == ftpPassword).Single().Value;

            //Cursor = Cursors.WaitCursor;
            dtErrors.Rows.Clear();

            BeginFtpProcessingFiles("", serverArchiveUri, serverUri, downloadPath, username, password,
                                    lastDownLoadDate, lastImportDate, isFtpDownload, _indexes, monthStr);
        }

        #endregion

        #region Proxy Calls

        private void BeginBalticExchangeInputDataInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginBalticExchangeInputDataInitializationData(
                    EndBalticExchangeInputDataInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndBalticExchangeInputDataInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndBalticExchangeInputDataInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndBalticExchangeInputDataInitializationData(out _indexes, out _appParameters, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                var greekCultureInfo = new CultureInfo("el-GR", false);

                if (_appParameters.Where(a => a.Code == ftpLastDownloadDate).SingleOrDefault() != null &&
                !string.IsNullOrEmpty(
                    _appParameters.Where(a => a.Code == ftpLastDownloadDate).SingleOrDefault().Value.Trim()))
                {
                    lastDownLoadDate = Convert.ToDateTime(_appParameters.Where(a => a.Code == ftpLastDownloadDate).Single().Value, greekCultureInfo);
                    lblLastDownloadDate.Text = lastDownLoadDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);
                }

                if (_appParameters.Where(a => a.Code == lastImportIndexesValuesDate).SingleOrDefault() != null &&
                !string.IsNullOrEmpty(
                    _appParameters.Where(a => a.Code == lastImportIndexesValuesDate).SingleOrDefault().Value.Trim()))
                {
                    lastImportDate = Convert.ToDateTime(_appParameters.Where(a => a.Code == lastImportIndexesValuesDate).Single().Value, greekCultureInfo);
                    lblLastImportDate.Text = lastImportDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);
                }

                if (_appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault() == null ||
                string.IsNullOrEmpty(_appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault().Value.Trim()))
                {
                    XtraMessageBox.Show(this,
                                        "Application parameter " + ftpXmlFolder + " is not provided.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
                //FFA/Option dialog
                ffaDialog = new OpenFileDialog();
                ffaDialog.Filter = @"XML files (*.xml)|*.xml";
                ffaDialog.InitialDirectory = _appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault() == null
                                           ? ""
                                           : _appParameters.Where(a => a.Code == ftpXmlFolder).Single().Value;
                ffaDialog.RestoreDirectory = true;
                ffaDialog.Title = Strings.Open_File;

                //BOA dialog
                boaDialog = new OpenFileDialog();
                boaDialog.Filter = @"Xml files (*.xml)|*.xml";
                boaDialog.InitialDirectory = _appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault() == null
                                           ? ""
                                           : _appParameters.Where(a => a.Code == ftpXmlFolder).Single().Value;
                boaDialog.RestoreDirectory = true;
                boaDialog.Title = Strings.Open_File;
            }
        }

        private void BeginFtpProcessingFiles(string ftpsource, string ftpArchiveUri, string ftpUri, string ftpXmlFolder, string ftpUsername, string ftpPassword,
            DateTime lastDownLoadDate, DateTime lastImportDate, bool isFtpDownload, List<Index> indexes,
            Dictionary<string, int> monthStr)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginFtpBatchProcessingFiles(ftpsource, ftpArchiveUri,
                                                                 ftpUri,
                                                                 ftpXmlFolder,
                                                                 ftpUsername,
                                                                 ftpPassword,                                                                 
                                                                 isFtpDownload,
                                                                 indexes,
                                                                 monthStr,
                                                                 EndFtpBatchProcessingFiles, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BeginFtpProcessingFilesNAS(string ftpsource, string ftpArchiveUri, string ftpUri, string ftpXmlFolder, string ftpUsername, string ftpPassword,
           DateTime lastDownLoadDate, DateTime lastImportDate, bool isFtpDownload, List<Index> indexes,
           Dictionary<string, int> monthStr)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {





                ////////////////////////////////////////////add code from server to import files//////////////////////////////////////////////////

                //ftp Nasdaq VOl
                //1  ---- : add to parameters!!!!!!! var ftpNasdaqUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities";
                var ftpNasdaqUri1 = _appParameters.Where(n => n.Description == "ftpNasdaqUri").ToList();
                var ftpNasdaqUri = string.Empty;
                if (ftpNasdaqUri1.Count == 0)
                {
                    ftpNasdaqUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities";
                }
                else
                {
                    ftpNasdaqUri = ftpNasdaqUri1.First().Value;
                }
                //2  ---- : add to parameters!!!!!!! ftpUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities/Risk";
                var ftpUri1 = _appParameters.Where(n => n.Description == "ftpUri").ToList();
                if (ftpUri1.Count == 0)
                {
                    ftpUri = @"ftp://ftp.nordic.nasdaqomxtrader.com/Commodities/Risk";
                }
                else
                {
                    ftpUri = ftpUri1.First().Value;
                }
                //var ftpFileVolCorr = @"\Vol and Corr matrices Dry Freight.xlsx"; //"\\Vol%20and%20Corr%20matrices%20Dry%20Freight.xlsx";
                var ftpFileVolCorr1 = _appParameters.Where(n => n.Description == "ftpFileVolCorr").ToList();
                var ftpFileVolCorr = string.Empty;
                if (ftpFileVolCorr1.Count == 0)
                {
                    ftpFileVolCorr = @"\Vol and Corr matrices Dry Freight.xlsx"; //"\\Vol%20and%20Corr%20matrices%20Dry%20Freight.xlsx";
                }
                else
                {
                    ftpFileVolCorr = ftpFileVolCorr1.First().Value;
                }
                //var filenewfilename = @"\Vol_and_Corr_matrices_Dry_Freight" + DateTime.Today.ToString("yyMMdd") + ".xlsx";
                var filenewfilename1 = _appParameters.Where(n => n.Description == "filenewfilename").ToList();
                var filenewfilename = string.Empty;
                if (filenewfilename1.Count == 0)
                {
                     filenewfilename = @"\Vol_and_Corr_matrices_Dry_Freight" + DateTime.Today.ToString("yyMMdd") + ".xlsx";
                }
                else
                {
                    filenewfilename  = filenewfilename1.First().Value;
                }
                //var ftpFolder = @"C:\FreightMetrics\Data\";
                var ftpFolder1 = _appParameters.Where(n => n.Description == "ftpFolder").ToList();
                var ftpFolder = string.Empty;
                if (ftpFolder1.Count == 0)
                {
                    ftpFolder = @"C:\FreightMetrics\Data\";
                }
                else
                {
                    ftpFolder = ftpFolder1.First().Value;
                }
                var ftpFileVolCorrUri = ftpUri + ftpFileVolCorr;
                var ftpfilelist = new List<FileName>();
                getFileList(ftpUri, "", "", ftpfilelist);

                WebClient client = new WebClient();
                //client.Credentials = new NetworkCredential("username", "password");
                client.DownloadFile(ftpFileVolCorrUri, ftpFolder + ftpFileVolCorr);
                if (!File.Exists(ftpFolder + filenewfilename))
                {
                    File.Move(ftpFolder + ftpFileVolCorr, ftpFolder + filenewfilename);
                }
                //ftp Nasdaq Span
                //Create folder name
                //var spanfolder = DateTime.Today.ToString("yyMMdd");
                var spantoday = DateTime.Today;
                if (spantoday.DayOfWeek == DayOfWeek.Monday)
                {
                    DateTime lastFriday = DateTime.Now.AddDays(-1);
                    while (lastFriday.DayOfWeek != DayOfWeek.Friday)
                        lastFriday = lastFriday.AddDays(-1);
                    spantoday = lastFriday;
                }
                var spanfolder = spantoday.AddDays(-1).ToString("yyMMdd");
                var ftpNasdaqUriSpan = ftpNasdaqUri + "/PROD/Common/" + spanfolder;
                var request = (FtpWebRequest)WebRequest.Create(ftpNasdaqUriSpan.ToString());
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                try
                {
                    var response = (FtpWebResponse) request.GetResponse();

                    if (response != null)
                    {
                        Stream responseStream = response.GetResponseStream();
                        if (responseStream != null)
                        {
                            var reader = new StreamReader(responseStream);
                            string datastring = reader.ReadToEnd();

                            FileStruct[] list = (new ParseListDirectory()).GetList(datastring);
                            var webClient = new WebClient();
                            webClient.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                            var spanfilenames = list.Where(a => !a.IsDirectory && a.Name.Contains("NSPANPAR")).ToList();
                            var spnfilename = spanfilenames.OrderByDescending(f => f.Name).First();
                            //var nspanfilenames = list.Where(a => !a.IsDirectory && a.Name.Contains("NSPANPAR")).ToList();                            
                            //var nspnfilename = nspanfilenames.OrderByDescending(f => f.Name).First();
                            string fileName = spnfilename.Name;
                            //string fileNameSpan = nspnfilename.Name;
                            //set full file path to download (Path+fileName)
                            string strDownloadFilepath = ftpFolder + fileName;
                            //string strDownloadFilepath2 = ftpFolder + fileNameSpan;
                            //set the URI
                            var uri = new Uri(ftpNasdaqUriSpan.ToString() + "/" + fileName);
                            //var uri2 = new Uri(ftpNasdaqUriSpan.ToString() + "/" + fileNameSpan);
                            try
                            {
                                //Download file
                                webClient.DownloadFile(uri, strDownloadFilepath);
                                //webClient.DownloadFile(uri2, strDownloadFilepath2);
                            }
                            catch (WebException ex)
                            {
                            }

                            reader.Close();

                        }

                    }
                    response.Close();

                    Cursor = Cursors.Default;
                    lock (_syncObject)
                    {
                        _dialogForm.Close();
                        _dialogForm = null;
                    }
                    XtraMessageBox.Show(this,

                                        "The files were downloaded successfully!!",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                   

                }
                catch (Exception ex)
                {
                    Cursor = Cursors.Default;
                    lock (_syncObject)
                    {
                        _dialogForm.Close();
                        _dialogForm = null;
                    }
                    SessionRegistry.ResetClientService();
                    XtraMessageBox.Show(this,
                                        Strings.
                                            There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                ////////////////////////////////////////////end of code to import the files////////////////////////////////////////////////////////
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public class FileName : IComparable<FileName>
        {
            public string fName { get; set; }
            public int CompareTo(FileName other)
            {
                return fName.CompareTo(other.fName);
            }
        }


        public static void getFileList(string sourceURI, string sourceUser, string sourcePass, List<FileName> sourceFileList)
        {
            string line = "";
            FtpWebRequest sourceRequest;
            sourceRequest = (FtpWebRequest)WebRequest.Create(sourceURI);
            //sourceRequest.Credentials = new NetworkCredential(sourceUser, sourcePass);
            sourceRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            sourceRequest.UseBinary = true;
            sourceRequest.KeepAlive = false;
            sourceRequest.Timeout = -1;
            sourceRequest.UsePassive = true;
            FtpWebResponse sourceRespone = (FtpWebResponse)sourceRequest.GetResponse();
            //Creates a list(fileList) of the file names
            using (Stream responseStream = sourceRespone.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    line = reader.ReadLine();
                    while (line != null)
                    {
                        var fileName = new FileName
                        {
                            fName = line
                        };
                        sourceFileList.Add(fileName);
                        line = reader.ReadLine();
                    }
                }
            }
        }
               
        private void EndFtpBatchProcessingFiles(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndFtpBatchProcessingFiles;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<string> errorMessages;
           

            try
            {
                result = SessionRegistry.Client.EndFtpBatchProcessingFiles(out lastDownLoadDate, out lastImportDate, out errorMessages, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result ==1 )//information message
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    errorMessages.Max(),
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            else if (result == 2 || result == 3)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    errorMessages.Max(),
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);

               
            }
            else if (result == 0) //everything is OK
            {
            
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                foreach (var msg in errorMessages)
                {
                    if (msg.Contains("success") && !msg.Contains("failed"))
                    {
                        dtErrors.Rows.Add(new object[] { 2, msg });
                    }
                    else if (msg.Contains("error"))
                    {
                        dtErrors.Rows.Add(new object[] { 1, msg });
                    }
                    else
                    {
                        dtErrors.Rows.Add(new object[] { 0, msg });
                    }
                }
                grdErrors.DataSource = dtErrors;
                
                 if (dtErrors.Rows.Count == 0)
                  xtraTabControl.SelectedTabPage = xtraTabPageValidationErrors;

                lblLastDownloadDate.Text = lastDownLoadDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);
                lblLastImportDate.Text = lastImportDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);

                Cursor = Cursors.Default;

                XtraMessageBox.Show(this,
                                    "Download from server" + //+ _appParameters.Where(a => a.Code == ftpUri).Single().Value +
                                    " completed.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                //ParseExcelFiles(@"c:\Freightmetrics\Data\Vol and Corr matrices Dry Freight.xlsx");
            }
        }

        private void BeginLocalProcessingFileXML(XmlElement objRequestElement, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr, DateTime lastDownLoadDate, DateTime lastImportDate)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginLocalProcessingFileXML(objRequestElement, fileName, indexes, monthStr, 
                                                                EndLocalProcessingFileXML, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndLocalProcessingFileXML(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndLocalProcessingFileXML;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<string> errorMessages;
          

            try
            {
                result = SessionRegistry.Client.EndLocalProcessingFileXML(out lastDownLoadDate, out lastImportDate, out errorMessages, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    errorMessages.FirstOrDefault(),
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            else if (result == 0) //everything is OK
            {

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                lblLastDownloadDate.Text = lastDownLoadDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);
                lblLastImportDate.Text = lastImportDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);


                foreach (var msg in errorMessages)
                {
                    if (msg.Contains("success"))
                    {
                        dtErrors.Rows.Add(new object[] { 2, msg });
                    }
                    else if (msg.Contains("error"))
                    {
                        dtErrors.Rows.Add(new object[] { 1, msg });
                    }
                    else
                    {
                        dtErrors.Rows.Add(new object[] { 0, msg });
                    }
                }
                grdErrors.DataSource = dtErrors;

                if (dtErrors.Rows.Count == 0)
                    xtraTabControl.SelectedTabPage = xtraTabPageValidationErrors;
            }
        }

        private void BeginLocalProcessingFileExcel(List<ParseExcelIndexValue> indexValues, string fileName, List<Index> indexes,
            Dictionary<string, int> monthStr)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginLocalProcessingFileExcel(indexValues, fileName, indexes, monthStr, 
                                                                EndLocalProcessingFileExcel, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndLocalProcessingFileExcel(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndLocalProcessingFileExcel;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<string> errorMessages;
            try
            {
                result = SessionRegistry.Client.EndLocalProcessingFileExcel(out lastDownLoadDate, out lastImportDate, out errorMessages, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    errorMessages.FirstOrDefault(),
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            else if (result == 0) //everything is OK
            {

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                lblLastDownloadDate.Text = lastDownLoadDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);
                lblLastImportDate.Text = lastImportDate.Date.ToString(SessionRegistry.ClientCultureInfo.DateTimeFormat.ShortDatePattern);                

                foreach (var msg in errorMessages)
                {
                    if (msg.Contains("success"))
                    {
                        dtErrors.Rows.Add(new object[] { 2, msg });
                    }
                    else if (msg.Contains("error"))
                    {
                        dtErrors.Rows.Add(new object[] { 1, msg });
                    }
                    else
                    {
                        dtErrors.Rows.Add(new object[] { 0, msg });
                    }
                }
                grdErrors.DataSource = dtErrors;

                if (dtErrors.Rows.Count == 0)
                    xtraTabControl.SelectedTabPage = xtraTabPageValidationErrors;
            }
        }

        #endregion

        public struct FileStruct
        {
            public string Flags;
            public string Owner;
            public string Group;
            public bool IsDirectory;
            public DateTime CreateTime;
            public string Name;
        }

        public enum FileListStyle
        {
            UnixStyle,
            WindowsStyle,
            Unknown
        }

        public class ParseListDirectory
        {
            public FileStruct[] GetList(string datastring)
            {
                List<FileStruct> myListArray = new List<FileStruct>();
                string[] dataRecords = datastring.Split('\n');
                FileListStyle _directoryListStyle = GuessFileListStyle(dataRecords);
                foreach (string s in dataRecords)
                {
                    if (_directoryListStyle != FileListStyle.Unknown && s != "")
                    {
                        FileStruct f = new FileStruct();
                        f.Name = "..";
                        switch (_directoryListStyle)
                        {
                            case FileListStyle.UnixStyle:
                                f = ParseFileStructFromUnixStyleRecord(s);
                                break;
                            case FileListStyle.WindowsStyle:
                                f = ParseFileStructFromWindowsStyleRecord(s);
                                break;
                        }
                        if (!(f.Name == "." || f.Name == ".."))
                        {
                            myListArray.Add(f);
                        }
                    }
                }
                return myListArray.ToArray();
            }
            public FileStruct ParseFileStructFromWindowsStyleRecord(string Record)
            {
                FileStruct f = new FileStruct();
                string processstr = Record.Trim();
                string dateStr = processstr.Substring(0, 8);
                processstr = (processstr.Substring(8, processstr.Length - 8)).Trim();
                string timeStr = processstr.Substring(0, 7);
                processstr = (processstr.Substring(7, processstr.Length - 7)).Trim();
                f.CreateTime = DateTime.Parse(dateStr + " " + timeStr);
                if (processstr.Substring(0, 5) == "<DIR>")
                {
                    f.IsDirectory = true;
                    processstr = (processstr.Substring(5, processstr.Length - 5)).Trim();
                }
                else
                {
                    string[] strs = processstr.Split(new char[] { ' ' });
                    processstr = strs[1].Trim();
                    f.IsDirectory = false;
                }
                f.Name = processstr;  //Rest is name   
                return f;
            }
            public FileListStyle GuessFileListStyle(string[] recordList)
            {
                foreach (string s in recordList)
                {
                    if (s.Length > 10
                     && Regex.IsMatch(s.Substring(0, 10), "(-|d)(-|r)(-|w)(-|x)(-|r)(-|w)(-|x)(-|r)(-|w)(-|x)"))
                    {
                        return FileListStyle.UnixStyle;
                    }
                    else if (s.Length > 8
                     && Regex.IsMatch(s.Substring(0, 8), "[0-9][0-9]-[0-9][0-9]-[0-9][0-9]"))
                    {
                        return FileListStyle.WindowsStyle;
                    }
                }
                return FileListStyle.Unknown;
            }
            private FileStruct ParseFileStructFromUnixStyleRecord(string Record)
            {
                ///Assuming record style as
                /// dr-xr-xr-x   1 owner    group               0 Nov 25  2002 bussys
                FileStruct f = new FileStruct();
                string processstr = Record.Trim();
                f.Flags = processstr.Substring(0, 9);
                f.IsDirectory = (f.Flags[0] == 'd');
                processstr = (processstr.Substring(11)).Trim();
                CutSubstringFromStringWithTrim(ref processstr, ' ', 0);   //skip one part
                f.Owner = CutSubstringFromStringWithTrim(ref processstr, ' ', 0);
                f.Group = CutSubstringFromStringWithTrim(ref processstr, ' ', 0);
                CutSubstringFromStringWithTrim(ref processstr, ' ', 0);   //skip one part

                var tokens = processstr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length > 3)
                {
                    //f.CreateTime = DateTime.ParseExact(tokens[0] + tokens[1], "MM-dd-yyHH:mmtt",
                    //                                   CultureInfo.InvariantCulture);
                    f.Name = tokens[3];
                }

                //f.CreateTime = DateTime.Parse(_cutSubstringFromStringWithTrim(ref processstr, ' ', 8));
                //f.Name = processstr;   //Rest of the part is name
                return f;
            }
            public string CutSubstringFromStringWithTrim(ref string s, char c, int startIndex)
            {
                int pos1 = s.IndexOf(c, startIndex);
                string retString = s.Substring(0, pos1);
                s = (s.Substring(pos1)).Trim();
                return retString;
            }
        }       

        private void btnNasdaqChooseFile_Click(object sender, EventArgs e)
        {
            if (ffaDialog.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = ffaDialog.FileName;

            if (!string.IsNullOrEmpty(ffaDialog.InitialDirectory))
                ffaDialog.InitialDirectory = "";
        }

        private void btnNasdaqChooseFile_EditValueChanged(object sender, EventArgs e)
        {
            btnImportNasdaqFile.Enabled = !string.IsNullOrEmpty(((ButtonEdit)sender).EditValue.ToString());
        }

        private void btnImportNasdaqFtp_Click(object sender, EventArgs e)
        {
            ////Check the application parameters needed for ftp downloading           
            var serverArchiveUri = _appParameters.Where(a => a.Code == ftpArchiveUri).Single().Value;
            var serverUri = _appParameters.Where(a => a.Code == ftpUri).Single().Value; //Uri serverUri = new Uri("ftp://ftp.balticexchange.org/");
            string downloadPath = _appParameters.Where(a => a.Code == ftpXmlFolder).Single().Value; //const string downloadPath = @"C:\FreightMetrics\";

            Cursor = Cursors.WaitCursor;
            dtErrors.Rows.Clear();

            BeginFtpProcessingFilesNAS("NASDAQ", serverArchiveUri, serverUri, downloadPath, "", "",
                                    lastDownLoadDate, lastImportDate, isFtpDownload, _indexes, monthStr);          
        }

        #region Parse IM Excel files
        public void ParseExcelFiles(string excelFilename)
        {
            try
            {
                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbookws;
                string fileName = excelFilename;
                string extension = Path.GetExtension(fileName);
                ////Read .xlsx file.  
                //ExcelWorkbook Workbook = ExcelWorkbook.ReadXLSX(@"..\..\..\ExcelFile.xlsx");

                //for (int i = 0; i < Workbook.Worksheets[0].Rows.Count; i++)
                    switch (extension.ToLower())
                {
                    case ".xlsx":
                        {
                            wbookws = ExcelWorkbook.ReadXLSX(fileName);                            
                            break;
                        }
                    case ".xls":
                        {
                            wbookws = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                    case ".csv":
                        {
                            wbookws = ExcelWorkbook.ReadCSV(fileName);
                            break;
                        }
                    case ".txt":
                        {
                            wbookws = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                            break;
                        }
                    default:
                        {
                            wbookws = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                }

                var numSheets = wbookws.Worksheets.Count;

               
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                                    "Error occurred while parsing excel file1. Error: '" + exc.Message + "'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        #endregion
    }
}