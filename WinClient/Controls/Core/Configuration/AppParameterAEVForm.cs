﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.Domain;
using Exis.WinClient.SpecialForms;
using Exis.WinClient.General;
using DevExpress.XtraLayout.Utils;

namespace Exis.WinClient.Controls
{
    public partial class AppParameterAEVForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private AppParameter _appParameter;
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructor
        
        public AppParameterAEVForm()
        {
            InitializeComponent();
            _formActionType = FormActionTypeEnum.Add;
        }

        public AppParameterAEVForm(AppParameter appParameter)
        {
            InitializeComponent();

            _formActionType = FormActionTypeEnum.Edit;
            _appParameter = appParameter;
        }

        #endregion

        #region Private Methods

        private void LoadData()
        {
            txtDescription.Text = _appParameter.Description;
            txtValue.Text = _appParameter.Value;
            txtCode.Text = _appParameter.Code;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtCode.Text.Trim()))
                errorProvider.SetError(txtCode, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtValue.Text.Trim()))
                errorProvider.SetError(txtValue, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtDescription.Text.Trim()))
                errorProvider.SetError(txtDescription, Strings.Field_is_mandatory);
            return !errorProvider.HasErrors;
        }

        #endregion
        
        #region GUI Events

        private void AppParameterAevFormLoad(object sender, EventArgs e)
        {
            if(_formActionType == FormActionTypeEnum.Edit)
                LoadData();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _appParameter = new AppParameter();
            }

            _appParameter.Description = txtDescription.Text;
            _appParameter.Code = txtCode.Text.Trim();
            _appParameter.Value = txtValue.Text;
            _appParameter.IsSystem = false;

            BeginAEVAppParameter(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void AppParameterAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void AppParameterAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Service Calls

        private void BeginAEVAppParameter(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _appParameter, EndAEVAppParameter, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVAppParameter(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVAppParameter;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtCode, "Code already exists.");
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;

                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public AppParameter AppParameter
        {
            get { return _appParameter; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

    }
}