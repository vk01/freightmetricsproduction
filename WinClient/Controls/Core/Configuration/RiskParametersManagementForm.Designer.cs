﻿namespace Exis.WinClient.Controls
{
    partial class RiskParametersManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RiskParametersManagementForm));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.grdRiskCorrelations = new DevExpress.XtraGrid.GridControl();
            this.grvRiskCorrelations = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gcbSourceIndexes = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gcSourceIndexId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gcSourceIndex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gcbTargetIndexes = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.panelRiskVolatilities = new DevExpress.XtraEditors.PanelControl();
            this.lcRiskVolatilities = new DevExpress.XtraLayout.LayoutControl();
            this.txtRiskValue_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lookupIndex_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lcgRootRiskVolatilities = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgRiskVolatilities = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgRiskVolatilitiesInfos = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgRiskVolatility_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutRiskValue_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRiskIndex_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnRemoveRiskVolatility = new DevExpress.XtraEditors.SimpleButton();
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.btnAddRiskVolatility = new DevExpress.XtraEditors.SimpleButton();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgParameters = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgRiskCorrelations = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGrdRiskCorrelations = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgPanelRiskVolatilities = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPanelRiskVolatilities = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRiskCorrelations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvRiskCorrelations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelRiskVolatilities)).BeginInit();
            this.panelRiskVolatilities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcRiskVolatilities)).BeginInit();
            this.lcRiskVolatilities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRiskValue_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupIndex_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootRiskVolatilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskVolatilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskVolatilitiesInfos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskVolatility_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRiskValue_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRiskIndex_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskCorrelations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGrdRiskCorrelations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgPanelRiskVolatilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPanelRiskVolatilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.grdRiskCorrelations);
            this.layoutControl.Controls.Add(this.panelRiskVolatilities);
            this.layoutControl.Controls.Add(this.btnRemoveRiskVolatility);
            this.layoutControl.Controls.Add(this.btnAddRiskVolatility);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1410, 312, 616, 350);
            this.layoutControl.Root = this.lcgRoot;
            this.layoutControl.Size = new System.Drawing.Size(821, 457);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // grdRiskCorrelations
            // 
            this.grdRiskCorrelations.Location = new System.Drawing.Point(6, 189);
            this.grdRiskCorrelations.MainView = this.grvRiskCorrelations;
            this.grdRiskCorrelations.Name = "grdRiskCorrelations";
            this.grdRiskCorrelations.Size = new System.Drawing.Size(809, 262);
            this.grdRiskCorrelations.TabIndex = 10;
            this.grdRiskCorrelations.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvRiskCorrelations});
            // 
            // grvRiskCorrelations
            // 
            this.grvRiskCorrelations.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gcbSourceIndexes,
            this.gcbTargetIndexes});
            this.grvRiskCorrelations.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gcSourceIndex,
            this.gcSourceIndexId});
            this.grvRiskCorrelations.GridControl = this.grdRiskCorrelations;
            this.grvRiskCorrelations.Name = "grvRiskCorrelations";
            this.grvRiskCorrelations.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.grvRiskCorrelations.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grvRiskCorrelations.OptionsView.ShowGroupPanel = false;
            this.grvRiskCorrelations.OptionsView.ShowIndicator = false;
            this.grvRiskCorrelations.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvRiskCorrelations_CustomDrawCell);
            this.grvRiskCorrelations.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GrvRiskCorrelationsRowCellStyle);
            this.grvRiskCorrelations.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.GrvRiskCorrelationsShowingEditor);
            this.grvRiskCorrelations.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.GrvRiskCorrelationsCellValueChanged);
            // 
            // gcbSourceIndexes
            // 
            this.gcbSourceIndexes.Caption = "Source Indexes";
            this.gcbSourceIndexes.Columns.Add(this.gcSourceIndexId);
            this.gcbSourceIndexes.Columns.Add(this.gcSourceIndex);
            this.gcbSourceIndexes.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gcbSourceIndexes.Name = "gcbSourceIndexes";
            this.gcbSourceIndexes.Width = 90;
            // 
            // gcSourceIndexId
            // 
            this.gcSourceIndexId.Caption = "SourceIndexId";
            this.gcSourceIndexId.FieldName = "SourceIndexId";
            this.gcSourceIndexId.Name = "gcSourceIndexId";
            // 
            // gcSourceIndex
            // 
            this.gcSourceIndex.Caption = "Source Index";
            this.gcSourceIndex.FieldName = "SourceIndex";
            this.gcSourceIndex.Name = "gcSourceIndex";
            this.gcSourceIndex.OptionsColumn.AllowEdit = false;
            this.gcSourceIndex.OptionsColumn.ReadOnly = true;
            this.gcSourceIndex.Visible = true;
            this.gcSourceIndex.Width = 90;
            // 
            // gcbTargetIndexes
            // 
            this.gcbTargetIndexes.Caption = "Target Indexes";
            this.gcbTargetIndexes.Name = "gcbTargetIndexes";
            this.gcbTargetIndexes.Width = 605;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            // 
            // panelRiskVolatilities
            // 
            this.panelRiskVolatilities.Controls.Add(this.lcRiskVolatilities);
            this.panelRiskVolatilities.Location = new System.Drawing.Point(6, 44);
            this.panelRiskVolatilities.Name = "panelRiskVolatilities";
            this.panelRiskVolatilities.Size = new System.Drawing.Size(773, 116);
            this.panelRiskVolatilities.TabIndex = 9;
            // 
            // lcRiskVolatilities
            // 
            this.lcRiskVolatilities.Controls.Add(this.txtRiskValue_1);
            this.lcRiskVolatilities.Controls.Add(this.lookupIndex_1);
            this.lcRiskVolatilities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcRiskVolatilities.Location = new System.Drawing.Point(2, 2);
            this.lcRiskVolatilities.Name = "lcRiskVolatilities";
            this.lcRiskVolatilities.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1660, 252, 546, 350);
            this.lcRiskVolatilities.Root = this.lcgRootRiskVolatilities;
            this.lcRiskVolatilities.Size = new System.Drawing.Size(769, 112);
            this.lcRiskVolatilities.TabIndex = 0;
            this.lcRiskVolatilities.Text = "layoutControl1";
            // 
            // txtRiskValue_1
            // 
            this.txtRiskValue_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRiskValue_1.Location = new System.Drawing.Point(373, 2);
            this.txtRiskValue_1.Name = "txtRiskValue_1";
            this.txtRiskValue_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRiskValue_1.Properties.Mask.EditMask = "P0";
            this.txtRiskValue_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtRiskValue_1.Properties.MaxLength = 3;
            this.txtRiskValue_1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtRiskValue_1.Size = new System.Drawing.Size(394, 20);
            this.txtRiskValue_1.StyleController = this.lcRiskVolatilities;
            this.txtRiskValue_1.TabIndex = 5;
            // 
            // lookupIndex_1
            // 
            this.lookupIndex_1.Location = new System.Drawing.Point(37, 2);
            this.lookupIndex_1.Name = "lookupIndex_1";
            this.lookupIndex_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupIndex_1.Size = new System.Drawing.Size(297, 20);
            this.lookupIndex_1.StyleController = this.lcRiskVolatilities;
            this.lookupIndex_1.TabIndex = 4;
            // 
            // lcgRootRiskVolatilities
            // 
            this.lcgRootRiskVolatilities.CustomizationFormText = "lcgRootRiskVolatilities";
            this.lcgRootRiskVolatilities.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRootRiskVolatilities.GroupBordersVisible = false;
            this.lcgRootRiskVolatilities.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgRiskVolatilities});
            this.lcgRootRiskVolatilities.Location = new System.Drawing.Point(0, 0);
            this.lcgRootRiskVolatilities.Name = "lcgRootRiskVolatilities";
            this.lcgRootRiskVolatilities.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRootRiskVolatilities.Size = new System.Drawing.Size(769, 112);
            this.lcgRootRiskVolatilities.Text = "lcgRootRiskVolatilities";
            // 
            // lcgRiskVolatilities
            // 
            this.lcgRiskVolatilities.CustomizationFormText = "Risk Volatilities";
            this.lcgRiskVolatilities.ExpandButtonVisible = true;
            this.lcgRiskVolatilities.GroupBordersVisible = false;
            this.lcgRiskVolatilities.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgRiskVolatilitiesInfos});
            this.lcgRiskVolatilities.Location = new System.Drawing.Point(0, 0);
            this.lcgRiskVolatilities.Name = "lcgRiskVolatilities";
            this.lcgRiskVolatilities.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRiskVolatilities.Size = new System.Drawing.Size(769, 112);
            this.lcgRiskVolatilities.Text = "Risk Volatilities";
            this.lcgRiskVolatilities.TextVisible = false;
            // 
            // lcgRiskVolatilitiesInfos
            // 
            this.lcgRiskVolatilitiesInfos.CustomizationFormText = "lcgRiskVolatility_1";
            this.lcgRiskVolatilitiesInfos.GroupBordersVisible = false;
            this.lcgRiskVolatilitiesInfos.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgRiskVolatility_1});
            this.lcgRiskVolatilitiesInfos.Location = new System.Drawing.Point(0, 0);
            this.lcgRiskVolatilitiesInfos.Name = "lcgRiskVolatilitiesInfos";
            this.lcgRiskVolatilitiesInfos.Size = new System.Drawing.Size(769, 112);
            this.lcgRiskVolatilitiesInfos.Text = "lcgRiskVolatilitiesInfos";
            // 
            // lcgRiskVolatility_1
            // 
            this.lcgRiskVolatility_1.CustomizationFormText = "lcgRiskVolatility_1";
            this.lcgRiskVolatility_1.GroupBordersVisible = false;
            this.lcgRiskVolatility_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutRiskValue_1,
            this.layoutRiskIndex_1});
            this.lcgRiskVolatility_1.Location = new System.Drawing.Point(0, 0);
            this.lcgRiskVolatility_1.Name = "lcgRiskVolatility_1";
            this.lcgRiskVolatility_1.Size = new System.Drawing.Size(769, 112);
            this.lcgRiskVolatility_1.Text = "lcgRiskVolatility_1";
            // 
            // layoutRiskValue_1
            // 
            this.layoutRiskValue_1.Control = this.txtRiskValue_1;
            this.layoutRiskValue_1.CustomizationFormText = "Value:";
            this.layoutRiskValue_1.Location = new System.Drawing.Point(336, 0);
            this.layoutRiskValue_1.Name = "layoutRiskValue_1";
            this.layoutRiskValue_1.Size = new System.Drawing.Size(433, 112);
            this.layoutRiskValue_1.Text = "Value:";
            this.layoutRiskValue_1.TextSize = new System.Drawing.Size(32, 13);
            // 
            // layoutRiskIndex_1
            // 
            this.layoutRiskIndex_1.Control = this.lookupIndex_1;
            this.layoutRiskIndex_1.CustomizationFormText = "Index:";
            this.layoutRiskIndex_1.Location = new System.Drawing.Point(0, 0);
            this.layoutRiskIndex_1.Name = "layoutRiskIndex_1";
            this.layoutRiskIndex_1.Size = new System.Drawing.Size(336, 112);
            this.layoutRiskIndex_1.Text = "Index:";
            this.layoutRiskIndex_1.TextSize = new System.Drawing.Size(32, 13);
            // 
            // btnRemoveRiskVolatility
            // 
            this.btnRemoveRiskVolatility.ImageIndex = 1;
            this.btnRemoveRiskVolatility.ImageList = this.imageList16;
            this.btnRemoveRiskVolatility.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveRiskVolatility.Location = new System.Drawing.Point(783, 80);
            this.btnRemoveRiskVolatility.MaximumSize = new System.Drawing.Size(32, 32);
            this.btnRemoveRiskVolatility.MinimumSize = new System.Drawing.Size(32, 32);
            this.btnRemoveRiskVolatility.Name = "btnRemoveRiskVolatility";
            this.btnRemoveRiskVolatility.Size = new System.Drawing.Size(32, 32);
            this.btnRemoveRiskVolatility.StyleController = this.layoutControl;
            this.btnRemoveRiskVolatility.TabIndex = 12;
            this.btnRemoveRiskVolatility.Click += new System.EventHandler(this.BtnRemoveRiskVolatilityClick);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList16.Images.SetKeyName(1, "clear16x16.ico");
            // 
            // btnAddRiskVolatility
            // 
            this.btnAddRiskVolatility.ImageIndex = 0;
            this.btnAddRiskVolatility.ImageList = this.imageList16;
            this.btnAddRiskVolatility.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddRiskVolatility.Location = new System.Drawing.Point(783, 44);
            this.btnAddRiskVolatility.MaximumSize = new System.Drawing.Size(32, 32);
            this.btnAddRiskVolatility.MinimumSize = new System.Drawing.Size(32, 32);
            this.btnAddRiskVolatility.Name = "btnAddRiskVolatility";
            this.btnAddRiskVolatility.Size = new System.Drawing.Size(32, 32);
            this.btnAddRiskVolatility.StyleController = this.layoutControl;
            this.btnAddRiskVolatility.TabIndex = 11;
            this.btnAddRiskVolatility.Click += new System.EventHandler(this.BtnAddRiskVolatilityClick);
            // 
            // lcgRoot
            // 
            this.lcgRoot.CustomizationFormText = "Risk Parameters Management";
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgParameters});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRoot.Size = new System.Drawing.Size(821, 457);
            this.lcgRoot.Text = "Risk Parameters Management";
            // 
            // lcgParameters
            // 
            this.lcgParameters.CustomizationFormText = "lcgParameters";
            this.lcgParameters.GroupBordersVisible = false;
            this.lcgParameters.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgRiskCorrelations,
            this.lcgPanelRiskVolatilities});
            this.lcgParameters.Location = new System.Drawing.Point(0, 0);
            this.lcgParameters.Name = "lcgParameters";
            this.lcgParameters.Size = new System.Drawing.Size(819, 436);
            this.lcgParameters.Text = "lcgParameters";
            // 
            // lcgRiskCorrelations
            // 
            this.lcgRiskCorrelations.CustomizationFormText = "Risk Correlations";
            this.lcgRiskCorrelations.ExpandButtonVisible = true;
            this.lcgRiskCorrelations.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGrdRiskCorrelations});
            this.lcgRiskCorrelations.Location = new System.Drawing.Point(0, 145);
            this.lcgRiskCorrelations.Name = "lcgRiskCorrelations";
            this.lcgRiskCorrelations.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRiskCorrelations.Size = new System.Drawing.Size(819, 291);
            this.lcgRiskCorrelations.Text = "Risk Correlations";
            // 
            // layoutGrdRiskCorrelations
            // 
            this.layoutGrdRiskCorrelations.Control = this.grdRiskCorrelations;
            this.layoutGrdRiskCorrelations.CustomizationFormText = "layoutControlItem4";
            this.layoutGrdRiskCorrelations.Location = new System.Drawing.Point(0, 0);
            this.layoutGrdRiskCorrelations.Name = "layoutGrdRiskCorrelations";
            this.layoutGrdRiskCorrelations.Size = new System.Drawing.Size(813, 266);
            this.layoutGrdRiskCorrelations.Text = "layoutGrdRiskCorrelations";
            this.layoutGrdRiskCorrelations.TextSize = new System.Drawing.Size(0, 0);
            this.layoutGrdRiskCorrelations.TextToControlDistance = 0;
            this.layoutGrdRiskCorrelations.TextVisible = false;
            // 
            // lcgPanelRiskVolatilities
            // 
            this.lcgPanelRiskVolatilities.CustomizationFormText = "lcgPanelRiskVolatilities";
            this.lcgPanelRiskVolatilities.ExpandButtonVisible = true;
            this.lcgPanelRiskVolatilities.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPanelRiskVolatilities,
            this.layoutControlGroup1});
            this.lcgPanelRiskVolatilities.Location = new System.Drawing.Point(0, 0);
            this.lcgPanelRiskVolatilities.Name = "lcgPanelRiskVolatilities";
            this.lcgPanelRiskVolatilities.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgPanelRiskVolatilities.Size = new System.Drawing.Size(819, 145);
            this.lcgPanelRiskVolatilities.Text = "Risk Volatilities";
            // 
            // layoutPanelRiskVolatilities
            // 
            this.layoutPanelRiskVolatilities.Control = this.panelRiskVolatilities;
            this.layoutPanelRiskVolatilities.CustomizationFormText = "layoutPanelRiskVolatilities";
            this.layoutPanelRiskVolatilities.Location = new System.Drawing.Point(0, 0);
            this.layoutPanelRiskVolatilities.Name = "layoutPanelRiskVolatilities";
            this.layoutPanelRiskVolatilities.Size = new System.Drawing.Size(777, 120);
            this.layoutPanelRiskVolatilities.Text = "layoutPanelRiskVolatilities";
            this.layoutPanelRiskVolatilities.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPanelRiskVolatilities.TextToControlDistance = 0;
            this.layoutPanelRiskVolatilities.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(777, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(36, 120);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnAddRiskVolatility;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(36, 36);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnRemoveRiskVolatility;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(36, 36);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(36, 84);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(821, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 457);
            this.barDockControlBottom.Size = new System.Drawing.Size(821, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 457);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(821, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 457);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // RiskParametersManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 489);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "RiskParametersManagementForm";
            this.Load += new System.EventHandler(this.RiskParametersManagementFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdRiskCorrelations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvRiskCorrelations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelRiskVolatilities)).EndInit();
            this.panelRiskVolatilities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcRiskVolatilities)).EndInit();
            this.lcRiskVolatilities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRiskValue_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupIndex_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootRiskVolatilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskVolatilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskVolatilitiesInfos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskVolatility_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRiskValue_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRiskIndex_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRiskCorrelations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGrdRiskCorrelations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgPanelRiskVolatilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPanelRiskVolatilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraEditors.PanelControl panelRiskVolatilities;
        private DevExpress.XtraLayout.LayoutControlItem layoutPanelRiskVolatilities;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControl lcRiskVolatilities;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRootRiskVolatilities;
        private DevExpress.XtraGrid.GridControl grdRiskCorrelations;
        private DevExpress.XtraEditors.LookUpEdit lookupIndex_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRiskVolatilities;
        private DevExpress.XtraLayout.LayoutControlItem layoutRiskIndex_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRiskCorrelations;
        private DevExpress.XtraLayout.LayoutControlItem layoutGrdRiskCorrelations;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRiskVolatilitiesInfos;
        private DevExpress.XtraEditors.SpinEdit txtRiskValue_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRiskValue_1;
        private DevExpress.XtraEditors.SimpleButton btnAddRiskVolatility;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraEditors.SimpleButton btnRemoveRiskVolatility;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRiskVolatility_1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grvRiskCorrelations;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gcSourceIndex;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gcbSourceIndexes;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gcSourceIndexId;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gcbTargetIndexes;
        private DevExpress.XtraLayout.LayoutControlGroup lcgParameters;
        private DevExpress.XtraLayout.LayoutControlGroup lcgPanelRiskVolatilities;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}