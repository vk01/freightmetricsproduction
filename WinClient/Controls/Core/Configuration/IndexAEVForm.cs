﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraLayout;

namespace Exis.WinClient.Controls
{
    public partial class IndexAEVForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private Index _index;
        private bool _isMousePressed;
        private SeriesPoint _selectedChartValuePoint;
        private List<AverageIndexesAssoc> _averageIndexesAssocs;
        private List<Index> _indexes;

        #endregion

        #region Constructors

        public IndexAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public IndexAEVForm(Index index, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _index = index;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            lookupMarket.Properties.DisplayMember = "Name";
            lookupMarket.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
                          {
                              Caption = Strings.Market_Name,
                              FieldName = "Name",
                              Width = 0
                          };
            lookupMarket.Properties.Columns.Add(col);
            lookupMarket.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMarket.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMarket.Properties.NullValuePrompt = Strings.Select_a_Market___;

            lookupSourceIndex.Properties.DisplayMember = "Name";
            lookupSourceIndex.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupSourceIndex.Properties.Columns.Add(col);
            lookupSourceIndex.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupSourceIndex.Properties.SearchMode = SearchMode.AutoFilter;
            lookupSourceIndex.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;

            lookupIndex_1.Properties.DisplayMember = "Name";
            lookupIndex_1.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupIndex_1.Properties.Columns.Add(col);
            lookupIndex_1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupIndex_1.Properties.SearchMode = SearchMode.AutoFilter;
            lookupIndex_1.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;

            lookupCoverAssoc.Properties.DisplayMember = "Name";
            lookupCoverAssoc.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupCoverAssoc.Properties.Columns.Add(col);
            lookupCoverAssoc.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCoverAssoc.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCoverAssoc.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;

            lookupCalculatedSpotIndex.Properties.DisplayMember = "Name";
            lookupCalculatedSpotIndex.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupCalculatedSpotIndex.Properties.Columns.Add(col);
            lookupCalculatedSpotIndex.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCalculatedSpotIndex.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCalculatedSpotIndex.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;

            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;

                txtCode.Properties.ReadOnly = true;
                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                lookupMarket.Properties.ReadOnly = true;
                chkIsMarketDefault.Properties.ReadOnly = true;
                chkIsCustom.Properties.ReadOnly = true;
                lookupSourceIndex.Properties.ReadOnly = true;

                txtFFAMappingName.Properties.ReadOnly = true;
                txtBOAMappingName.Properties.ReadOnly = true;

                chkIsMarketDefaultForReports.Properties.ReadOnly = true;
                lookupCoverAssoc.Properties.ReadOnly = true;
                seCoverAssocOffset.Properties.ReadOnly = true;

                chkHasSpotFormula.Properties.ReadOnly = true;
                txtSpotMappingName.Properties.ReadOnly = true;

                chkIsAssocIndex.Properties.ReadOnly = true;
                btnAddIndex.Enabled = false;
                btnRemoveIndex.Enabled = false;

                chkIsCalculatedSpotFormula.Properties.ReadOnly = true;
                lookupCalculatedSpotIndex.Properties.ReadOnly = true;
                seCalculatedSpotOffset.Properties.ReadOnly = true;

                txtAssocOffset.Properties.ReadOnly = true;

            }
            else if(_formActionType == FormActionTypeEnum.Edit)
            {
                layoutGroupViewValues.Visibility = LayoutVisibility.Never;
                layoutOriginalBFAValues.Visibility = LayoutVisibility.Never;
            }
            else if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
                layoutGroupViewValues.Visibility = LayoutVisibility.Never;
                layoutOriginalBFAValues.Visibility = LayoutVisibility.Never;
                layoutControlItem1.Visibility = LayoutVisibility.Never;
                chkIsMarketDefault.Checked = false;
                chkIsAssocIndex.Checked = false;
                lcgIndexAssocs.Enabled = false;
                btnAddIndex.Enabled = false;
                btnRemoveIndex.Enabled = false;
                chkHasSpotFormula.Checked = false;
                txtSpotMappingName.Enabled = false;
                chkIsCalculatedSpotFormula.Checked = false;
                lookupCalculatedSpotIndex.Enabled = false;
                seCalculatedSpotOffset.Enabled = false;
                txtAssocOffset.Enabled = false;
                chkIsMarketDefaultForReports.Checked = false;
            }

            dtpViewValuesDate.DateTime = DateTime.Now.Date;

            

            int gridViewColumnindex = 0;
            gridSpotValues.BeginUpdate();
            gridSpotValuesMainView.Columns.Clear();
            gridSpotValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Date,
                FieldName = "Date",
                VisibleIndex = gridViewColumnindex++
            });
            gridSpotValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Price,
                FieldName = "Rate",
                VisibleIndex = gridViewColumnindex
            });

            gridSpotValues.EndUpdate();

            gridOriginalBFAValues.BeginUpdate();
            gridViewColumnindex = 0;
            gridOriginalBFAValuesMainView.Columns.Clear();
            gridOriginalBFAValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Period_From,
                FieldName = "PeriodFrom",
                VisibleIndex = gridViewColumnindex++
            });
            gridOriginalBFAValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Period_To,
                FieldName = "PeriodTo",
                VisibleIndex = gridViewColumnindex++
            });
            gridOriginalBFAValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Price,
                FieldName = "Rate",
                VisibleIndex = gridViewColumnindex
            });
            gridOriginalBFAValues.EndUpdate();

            gridNormalizedBFAValues.BeginUpdate();

            gridViewColumnindex = 0;
            gridNormalizedBFAValuesMainView.Columns.Clear();

            gridNormalizedBFAValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Date,
                FieldName = "Date",
                VisibleIndex = gridViewColumnindex++
            });
            gridNormalizedBFAValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Price,
                FieldName = "Price",
                VisibleIndex = gridViewColumnindex
            });
            gridNormalizedBFAValues.EndUpdate();

            gridCurveValues.BeginUpdate();

            gridViewColumnindex = 0;
            gridCurveValuesMainView.Columns.Clear();

            gridCurveValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Date,
                FieldName = "Date",
                VisibleIndex = gridViewColumnindex++
            });
            gridCurveValuesMainView.Columns.Add(new GridColumn
            {
                Caption = Strings.Price,
                FieldName = "Price",
                VisibleIndex = gridViewColumnindex
            });
            gridCurveValues.EndUpdate();

            btnExportCurveValues.Enabled = false;

        }

        private void InitializeData()
        {
            BeginAEVIndexInitializationData();
        }

        private void LoadData()
        {
            lblId.Text = _index.Id.ToString();
            txtCode.Text = _index.Code;
            txtName.Text = _index.Name;
            cmbStatus.SelectedItem = _index.Status;
            lookupMarket.EditValue = _index.MarketId;            
            chkIsMarketDefault.Checked = _index.IsMarketDefault;
            chkIsCustom.Checked = _index.IsCustom;

            txtFFAMappingName.Text = _index.FFAMappingName;
            txtBOAMappingName.Text = _index.BOAMappingName;

            chkIsMarketDefaultForReports.Checked = _index.IsReportsMarketDefault;
            lookupCoverAssoc.EditValue = _index.CoverAssocIndexId;
            if (_index.CoverAssocOffset != null) seCoverAssocOffset.Value = _index.CoverAssocOffset.Value;

            if (_index.IsAssocIndex != null) chkIsAssocIndex.Checked = (bool)_index.IsAssocIndex;

            if(chkIsCustom.Checked)
            {
                lookupSourceIndex.EditValue = _index.CustBaseIndexId;
                txtValuesAbsoluteOffset.Value = _index.CustAbsOffset.Value;
                txtValuesPercentageOffset.Value = _index.CustPercOffset.Value;
            }

            if(!string.IsNullOrEmpty(_index.SpotMappingName))
            {
                chkHasSpotFormula.Checked = true;
                txtSpotMappingName.Enabled = true;
                txtSpotMappingName.Text = _index.SpotMappingName;
            }
            else
            {
                chkHasSpotFormula.Checked = false;
                txtSpotMappingName.Enabled = false;
            }


            if (chkIsAssocIndex.Checked)
            {
                lcgIndexAssocs.Enabled = true;
                btnAddIndex.Enabled = _formActionType == FormActionTypeEnum.Edit;
                btnRemoveIndex.Enabled = _formActionType == FormActionTypeEnum.Edit;
                txtAssocOffset.Enabled = _formActionType == FormActionTypeEnum.Edit;
                if (_index.AssocOffset != null) txtAssocOffset.Value = _index.AssocOffset.Value;

                layoutRootControl.BeginUpdate();
                lcgIndexAssocs.Clear();
                foreach (AverageIndexesAssoc assoc in _averageIndexesAssocs)
                {
                    int counter = Convert.ToInt32(lcgIndexAssocs.Items.Count + 1);

                    var lookupIndex_new = new LookUpEdit();
                    lookupIndex_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View;
                    var txtIndexWeight_new = new SpinEdit();
                    txtIndexWeight_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View;

                    // 
                    // lookupIndex_new
                    // 
                    lookupIndex_new.Name = "lookupIndex_" + counter;
                    lookupIndex_new.Properties.DisplayMember = "Name";
                    lookupIndex_new.Properties.ValueMember = "Id";
                    var col = new LookUpColumnInfo
                    {
                        Caption = Strings.Broker_Name,
                        FieldName = "Name",
                        Width = 0
                    };
                    lookupIndex_new.Properties.Columns.Add(col);
                    lookupIndex_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                    lookupIndex_new.Properties.SearchMode = SearchMode.AutoFilter;
                    lookupIndex_new.Properties.NullValuePrompt =
                        Strings.Select_a_Broker___;
                    lookupIndex_new.QueryPopUp += new CancelEventHandler(LookupIndexNewQueryPopUp);
                    lookupIndex_new.Properties.DataSource =
                        _indexes.Where(a => a.MarketId == (long) lookupMarket.EditValue).ToList();
                    // 
                    // txtIndexWeight_new
                    // 
                    txtIndexWeight_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
                    txtIndexWeight_new.Name = "txtIndexWeight_" + counter;
                    txtIndexWeight_new.Properties.Mask.EditMask = @"P2";
                    txtIndexWeight_new.Properties.MinValue = 0;
                    txtIndexWeight_new.Properties.MaxValue = 100;
                    txtIndexWeight_new.Properties.MaxLength = 6;
                    txtIndexWeight_new.Value = 0;
                    txtIndexWeight_new.Properties.Mask.UseMaskAsDisplayFormat = true;

                    LayoutControlGroup layoutIndexAssoc_new = lcgIndexAssocs.AddGroup();

                    // 
                    // layoutIndexAssoc_new
                    // 
                    layoutIndexAssoc_new.GroupBordersVisible = false;
                    layoutIndexAssoc_new.Name = "lcgAssoc_" + counter;
                    layoutIndexAssoc_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                    layoutIndexAssoc_new.Tag = counter;
                    layoutIndexAssoc_new.Text = "lcgAssoc";
                    layoutIndexAssoc_new.TextVisible = false;
                    layoutIndexAssoc_new.DefaultLayoutType = LayoutType.Horizontal;
                    // 
                    // layoutIndexName
                    // 
                    LayoutControlItem layoutIndexName = layoutIndexAssoc_new.AddItem();
                    layoutIndexName.Control = lookupIndex_new;
                    layoutIndexName.Name = "layoutIndex_" + counter;
                    layoutIndexName.Text = "Index:";
                    // 
                    // layoutIndexWeight
                    // 
                    LayoutControlItem layoutIndexWeight_new = layoutIndexAssoc_new.AddItem();
                    layoutIndexWeight_new.Control = txtIndexWeight_new;
                    layoutIndexWeight_new.ControlMaxSize = new Size(80, 20);
                    layoutIndexWeight_new.ControlMinSize = new Size(80, 20);
                    layoutIndexWeight_new.Name = "layoutIndexWeight_" + counter;
                    layoutIndexWeight_new.SizeConstraintsType = SizeConstraintsType.Custom;
                    layoutIndexWeight_new.Text = "Weight:";

                    lookupIndex_new.EditValue = assoc.IndexId;
                    txtIndexWeight_new.EditValue = assoc.Weight;

                }

                //AvgOffsetValue

                layoutRootControl.EndUpdate();
                layoutRootControl.BestFit();
            }
            else
            {
                lcgIndexAssocs.Enabled = false;
                btnAddIndex.Enabled = false;
                btnRemoveIndex.Enabled = false;
                txtAssocOffset.Enabled = false;

                lookupIndex_1.Properties.DataSource =
                    ((List<Index>)lookupIndex_1.Tag).Where(a => a.MarketId == (long)lookupMarket.EditValue);
            }

            if (_index.DerivedFromIndexId != null)
            {
                chkIsCalculatedSpotFormula.Checked = true;
                lookupCalculatedSpotIndex.Enabled = true;
                seCalculatedSpotOffset.Enabled = true;
                lookupCalculatedSpotIndex.EditValue = _index.DerivedFromIndexId.Value;
                seCalculatedSpotOffset.Value = _index.DerivedOffset.Value;
            }
            else
            {
                chkIsCalculatedSpotFormula.Checked = false;
                lookupCalculatedSpotIndex.Enabled = false;
                seCalculatedSpotOffset.Enabled = false;
            }


            txtCreationUser.Text = _index.Cruser;
            dtpCreationDate.DateTime = _index.Crd;
            txtUpdateUser.Text = _index.Chuser;
            dtpUpdateDate.DateTime = _index.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtCode.Text.Trim()))
                errorProvider.SetError(txtCode, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (lookupMarket.EditValue == null) errorProvider.SetError(lookupMarket, Strings.Field_is_mandatory);

            if (chkIsCustom.Checked && lookupSourceIndex.EditValue == null) errorProvider.SetError(lookupSourceIndex, Strings.Field_is_mandatory);

            if (lookupCoverAssoc.EditValue != null && (seCoverAssocOffset.EditValue == null || seCoverAssocOffset.Value == 0))
                errorProvider.SetError(seCoverAssocOffset, Strings.Field_is_mandatory);

            if (lookupCoverAssoc.EditValue == null && (seCoverAssocOffset.EditValue != null && seCoverAssocOffset.Value != 0))
                errorProvider.SetError(lookupCoverAssoc, Strings.Field_is_mandatory);

            if (chkHasSpotFormula.Checked)
            {
                if (String.IsNullOrEmpty(txtSpotMappingName.Text.Trim()))
                    errorProvider.SetError(txtSpotMappingName, Strings.Field_is_mandatory);
            }

            if (chkIsAssocIndex.Checked)
            {
                if(lcgIndexAssocs.Items.Count == 0)
                    errorProvider.SetError(lookupMarket, "There must be at least one index association for an index which is market default.");

                foreach (object item in lcgIndexAssocs.Items)
                {
                    if (item.GetType() == typeof(LayoutControlGroup))
                    {
                        var layoutAssoc = (LayoutControlGroup)item;
                        if (layoutAssoc.Name.StartsWith("lcgAssoc_"))
                        {
                            string layoutSuffix = layoutAssoc.Name.Substring(layoutAssoc.Name.IndexOf("_") + 1);
                            LookUpEdit index = (LookUpEdit)((LayoutControlItem)layoutAssoc.Items.FindByName("layoutIndex_" + layoutSuffix)).Control;
                            if (index.EditValue == null) errorProvider.SetError(index, Strings.Field_is_mandatory);

                            var weight = (SpinEdit)((LayoutControlItem)layoutAssoc.Items.FindByName("layoutIndexWeight_" + layoutSuffix)).Control;
                            if (weight.EditValue == null || weight.Value == 0) errorProvider.SetError(weight, Strings.Field_is_mandatory);
                        }
                    }
                }
            }

            if(chkIsCalculatedSpotFormula.Checked)
            {
                if (lookupCalculatedSpotIndex.EditValue == null)
                    errorProvider.SetError(lookupCalculatedSpotIndex, Strings.Field_is_mandatory);
                if(seCalculatedSpotOffset.EditValue == null || seCalculatedSpotOffset.Value == 0)
                    errorProvider.SetError(seCalculatedSpotOffset, Strings.Field_is_mandatory);
            }
                

            return !errorProvider.HasErrors;
        }

        private void ClearIndexesAssociation()
        {
            layoutRootControl.BeginUpdate();

            foreach (LayoutControlGroup layoutControlGroup in lcgIndexAssocs.Items)
            {
                foreach (LayoutControlItem layoutControlItem in layoutControlGroup.Items)
                {
                    layoutRootControl.Controls.Remove(layoutControlItem.Control);
                }
            }

            lcgIndexAssocs.Clear();
            layoutRootControl.EndUpdate();
        }

        #endregion

        #region GUI Events

        private void IndexAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _index = new Index();
            }

            _index.Code = txtCode.Text.Trim();
            _index.Name = txtName.Text.Trim();
            _index.Status = (ActivationStatusEnum)cmbStatus.SelectedItem;
            _index.MarketId = (long)lookupMarket.EditValue;
            _index.IsMarketDefault = chkIsMarketDefault.Checked;
            _index.IsCustom = chkIsCustom.Checked;
            _index.CustBaseIndexId = null;
            _index.CustAbsOffset = null;
            _index.CustPercOffset = null;

            _index.FFAMappingName = txtFFAMappingName.Text.Trim();
            _index.BOAMappingName = txtBOAMappingName.Text.Trim();

            _index.IsReportsMarketDefault = chkIsMarketDefaultForReports.Checked;

            _index.CoverAssocIndexId = null;
            _index.CoverAssocOffset = null;
            if(lookupCoverAssoc.EditValue != null)
            {
                _index.CoverAssocIndexId = Convert.ToInt64(lookupCoverAssoc.EditValue);
                _index.CoverAssocOffset = Convert.ToDecimal(seCoverAssocOffset.Value);
            }                              

            if(_index.IsCustom)
            {
                _index.CustBaseIndexId = Convert.ToInt64(lookupSourceIndex.EditValue);
                _index.CustAbsOffset = Convert.ToInt32(txtValuesAbsoluteOffset.Value);
                _index.CustPercOffset = Convert.ToInt32(txtValuesPercentageOffset.Value);
            }

            _index.SpotMappingName = null;
            if (chkHasSpotFormula.Checked)
            {
                _index.SpotMappingName = txtSpotMappingName.Text.Trim();
            }
           
            _index.IsAssocIndex = chkIsAssocIndex.Checked;
            var averageIndexesAssocs = new List<AverageIndexesAssoc>();
            
            if (_index.IsAssocIndex)
            {
                _index.AssocOffset = Convert.ToInt32(txtAssocOffset.Value);
                foreach (object item in lcgIndexAssocs.Items)
                {
                    if (item.GetType() == typeof(LayoutControlGroup))
                    {
                        var layoutAssoc = (LayoutControlGroup)item;
                        if (layoutAssoc.Name.StartsWith("lcgAssoc_"))
                        {
                            string layoutSuffix = layoutAssoc.Name.Substring(layoutAssoc.Name.IndexOf("_") + 1);
                            var weight = (SpinEdit)((LayoutControlItem)layoutAssoc.Items.FindByName("layoutIndexWeight_" + layoutSuffix)).Control;
                            var index = (LookUpEdit)((LayoutControlItem)layoutAssoc.Items.FindByName("layoutIndex_" + layoutSuffix)).Control;

                            var assoc = new AverageIndexesAssoc { IndexId = (long)index.EditValue, Weight = weight.Value };
                            averageIndexesAssocs.Add(assoc);
                        }
                    }
                }
            }

            _index.DerivedFromIndexId = null;
            _index.DerivedOffset = null;
            if (chkIsCalculatedSpotFormula.Checked)
            {
                _index.DerivedFromIndexId = Convert.ToInt64(lookupCalculatedSpotIndex.EditValue);
                _index.DerivedOffset = Convert.ToDecimal(seCalculatedSpotOffset.Value);
            }
            
            BeginAEVIndex(_formActionType != FormActionTypeEnum.Add, averageIndexesAssocs);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void btnViewValues_Click(object sender, EventArgs e)
        {
            if (dtpViewValuesDate.DateTime.Date >= DateTime.Now.Date) return;

            if(_formActionType == FormActionTypeEnum.Add)
            {
                if (chkIsCustom.Checked && lookupSourceIndex.EditValue != null)
                {
                    BeginGetIndexValues((long)lookupSourceIndex.EditValue, dtpViewValuesDate.DateTime.Date, Convert.ToInt32(txtValuesAbsoluteOffset.Value), Convert.ToInt32(txtValuesPercentageOffset.Value));
                }
            }
            else if (_formActionType == FormActionTypeEnum.Edit)
            {
                if (chkIsCustom.Checked && lookupSourceIndex.EditValue != null)
                {
                    BeginGetIndexValues((long)lookupSourceIndex.EditValue, dtpViewValuesDate.DateTime.Date, Convert.ToInt32(txtValuesAbsoluteOffset.Value), Convert.ToInt32(txtValuesPercentageOffset.Value));
                }
            }
            else if(_formActionType == FormActionTypeEnum.View)
            {
                if (chkIsCustom.Checked)
                {
                    BeginGetIndexValues((long)lookupSourceIndex.EditValue, dtpViewValuesDate.DateTime.Date, Convert.ToInt32(txtValuesAbsoluteOffset.Value), Convert.ToInt32(txtValuesPercentageOffset.Value));
                }
                else
                {
                    BeginGetIndexValues(_index.Id, dtpViewValuesDate.DateTime.Date, Convert.ToInt32(txtValuesAbsoluteOffset.Value), Convert.ToInt32(txtValuesPercentageOffset.Value));
                }
            }
        }

        private void IndexAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void IndexAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void chkIsCustom_CheckedChanged(object sender, EventArgs e)
        {
            if(chkIsCustom.Checked)
            {
                lookupSourceIndex.Enabled = true;
                layoutGroupViewValues.Visibility = LayoutVisibility.Always;
            }
            else
            {
                lookupSourceIndex.EditValue = null;
                lookupSourceIndex.Enabled = false;
                layoutGroupViewValues.Visibility = LayoutVisibility.Never;
            }
        }

        private void lookupMarket_EditValueChanged(object sender, EventArgs e)
        {
            ClearIndexesAssociation();
            lookupSourceIndex.Properties.DataSource = null;
            if (lookupMarket.EditValue != null)
            {
                if (_formActionType == FormActionTypeEnum.Add)
                {
                    lookupSourceIndex.Properties.DataSource =
                        ((List<Index>) lookupSourceIndex.Tag).Where(
                            a => a.MarketId == Convert.ToInt64(lookupMarket.EditValue) && !a.IsCustom).ToList();

                }
                else
                {
                    lookupSourceIndex.Properties.DataSource =
                        ((List<Index>) lookupSourceIndex.Tag).Where(
                            a =>
                            a.MarketId == Convert.ToInt64(lookupMarket.EditValue) && !a.IsCustom && a.Id != _index.Id).
                            ToList();
                }
            }
        }

        private void chartValues_MouseMove(object sender, MouseEventArgs e)
        {
            // TODO If requested by client
            ChartHitInfo hi = chartValues.CalcHitInfo(e.X, e.Y);

            if (hi != null && hi.Series != null && hi.SeriesPoint != null && hi.Series.ToString() == "seriesNormalizedBFA") { _selectedChartValuePoint = hi.SeriesPoint; }

            if (_selectedChartValuePoint != null && _isMousePressed)
            {
                DiagramCoordinates point =
                    ((XYDiagram)(sender as ChartControl).Diagram).PointToDiagram(hi.HitPoint);

                toolTipController.ShowHint("Price: " + Math.Round(point.NumericalValue, 0));
                _selectedChartValuePoint.Values[0] = Math.Round(point.NumericalValue, 0);
                ((ChartControl)sender).RefreshData();
            }
        }

        private void chartValues_MouseDown(object sender, MouseEventArgs e)
        {
            _isMousePressed = true;
        }

        private void chartValues_MouseUp(object sender, MouseEventArgs e)
        {
            _isMousePressed = false;
            _selectedChartValuePoint = null;
            toolTipController.HideHint();
        }

        private void chartValues_MouseLeave(object sender, EventArgs e)
        {
            _isMousePressed = false;
            _selectedChartValuePoint = null;
            toolTipController.HideHint();
        }

        private void BtnExportCurveValuesClick(object sender, EventArgs e)
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Excel";
            saveFileDialog.Filter = "Excel(*.xls)|*.xls";

            string fileName = "CurveValues_" + ((DateTime)dtpViewValuesDate.EditValue).Date.Day + "_" +
                              ((DateTime)dtpViewValuesDate.EditValue).Date.Month + "_" +
                              ((DateTime)dtpViewValuesDate.EditValue).Date.Year + "_Index(" +
                              (_index.IsCustom ? lookupSourceIndex.Text : txtName.Text) + ")_AbsOffset(" +
                              txtValuesAbsoluteOffset.Text + ")_PerxOffset(" + txtValuesPercentageOffset.Text + ")";

            saveFileDialog.FileName = fileName;

            DialogResult dialogResult = saveFileDialog.ShowDialog(this);

            if (dialogResult == DialogResult.OK)
            {
                var options = new XlsExportOptions();

                options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value;
                options.ExportMode = DevExpress.XtraPrinting.XlsExportMode.SingleFile;
                options.ShowGridLines = true;
                gridCurveValues.ExportToXls(saveFileDialog.FileName, options);

                XtraMessageBox.Show("Export Complete", "Export Result", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

            }

        }

        private void BtnAddIndexClick(object sender, EventArgs e)
        {
            foreach (object item in lcgIndexAssocs.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutAssoc = (LayoutControlGroup)item;
                    if (layoutAssoc.Name.StartsWith("lcgAssoc_"))
                    {
                        string layoutSuffix = layoutAssoc.Name.Substring(layoutAssoc.Name.IndexOf("_") + 1);
                        var index = (LookUpEdit)((LayoutControlItem)layoutAssoc.Items.FindByName("layoutIndex_" + layoutSuffix)).Control;
                        if (index.EditValue == null)
                            return;
                    }
                }
            }
            layoutRootControl.BeginUpdate();

            int counter;
            if (lcgIndexAssocs.Items.Count == 0)
                counter = 1;
            else
                counter = Convert.ToInt32(lcgIndexAssocs.Items[lcgIndexAssocs.Items.Count - 1].Tag) + 1;

            var lookupIndex_new = new LookUpEdit();
            var txtIndexWeight_new = new SpinEdit();
            // 
            // lookupIndex_new
            // 
            lookupIndex_new.Name = "lookupIndex_" + counter;
            lookupIndex_new.Properties.DisplayMember = "Name";
            lookupIndex_new.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupIndex_new.Properties.Columns.Add(col);
            lookupIndex_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupIndex_new.Properties.SearchMode = SearchMode.AutoFilter;
            lookupIndex_new.Properties.NullValuePrompt = Strings.Select_a_Market_Index___;
            lookupIndex_new.QueryPopUp += new CancelEventHandler(LookupIndexNewQueryPopUp);

            // 
            // txtIndexWeight_new
            // 
            txtIndexWeight_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
            txtIndexWeight_new.Name = "txtIndexWeight_" + counter;
            txtIndexWeight_new.Properties.Mask.EditMask = @"P2";
            txtIndexWeight_new.Properties.MinValue = 0;
            txtIndexWeight_new.Properties.MaxValue = 100;
            txtIndexWeight_new.Properties.MaxLength = 6;
            txtIndexWeight_new.Value = 0;
            txtIndexWeight_new.Properties.Mask.UseMaskAsDisplayFormat = true;

            LayoutControlGroup layoutIndexAssoc_new = lcgIndexAssocs.AddGroup();

            // 
            // layoutIndexAssoc_new
            // 
            layoutIndexAssoc_new.GroupBordersVisible = false;
            layoutIndexAssoc_new.Name = "lcgAssoc_" + counter;
            layoutIndexAssoc_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            layoutIndexAssoc_new.Tag = counter;
            layoutIndexAssoc_new.Text = "lcgAssoc";
            layoutIndexAssoc_new.TextVisible = false;
            layoutIndexAssoc_new.DefaultLayoutType = LayoutType.Horizontal;
            // 
            // layoutIndexName
            // 
            LayoutControlItem layoutIndexName = layoutIndexAssoc_new.AddItem();
            layoutIndexName.Control = lookupIndex_new;
            layoutIndexName.Name = "layoutIndex_" + counter;
            layoutIndexName.Text = "Index:";
            // 
            // layoutIndexWeight
            // 
            LayoutControlItem layoutIndexWeight_new = layoutIndexAssoc_new.AddItem();
            layoutIndexWeight_new.Control = txtIndexWeight_new;
            layoutIndexWeight_new.ControlMaxSize = new Size(80, 20);
            layoutIndexWeight_new.ControlMinSize = new Size(80, 20);
            layoutIndexWeight_new.Name = "layoutIndexWeight_" + counter;
            layoutIndexWeight_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutIndexWeight_new.Text = "Weight:";

            layoutRootControl.EndUpdate();
            layoutRootControl.BestFit();
        }

        void LookupIndexNewQueryPopUp(object sender, CancelEventArgs e)
        {
            var selectedIndexIds = new List<long>();
            foreach (object item in lcgIndexAssocs.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutIndex = (LayoutControlGroup)item;
                    if (layoutIndex.Name.StartsWith("lcgAssoc_"))
                    {
                        string layoutSuffix = layoutIndex.Name.Substring(layoutIndex.Name.IndexOf("_") + 1);
                        var index = (LookUpEdit)((LayoutControlItem)layoutIndex.Items.FindByName("layoutIndex_" + layoutSuffix)).Control;
                        if (index.EditValue != null && index.Name != ((LookUpEdit)sender).Name)
                            selectedIndexIds.Add((long)index.EditValue);
                    }
                }
            }

            var indexes =
                ((List<Index>)lookupSourceIndex.Tag).Where(
                    a => a.MarketId == (long)lookupMarket.EditValue && a.IsMarketDefault == false);
            ((LookUpEdit)sender).Properties.DataSource = indexes.Where(a => !selectedIndexIds.Contains(a.Id)).ToList();
        }

        private void BtnRemoveIndexClick(object sender, EventArgs e)
        {
            if (lcgIndexAssocs.Items.Count == 1) return;

            layoutRootControl.BeginUpdate();

            foreach (
                LayoutControlItem layoutControlItem in
                    ((LayoutControlGroup)lcgIndexAssocs.Items[lcgIndexAssocs.Items.Count - 1]).Items)
            {
                layoutRootControl.Controls.Remove(layoutControlItem.Control);
            }

            lcgIndexAssocs.RemoveAt(lcgIndexAssocs.Items.Count - 1);
            layoutRootControl.EndUpdate();
        }

        private void chkHasSpotFormula_CheckedChanged(object sender, EventArgs e)
        {
            if(chkHasSpotFormula.Checked)
            {
                txtSpotMappingName.Enabled = true;

                chkIsAssocIndex.Checked = false;
                lcgIndexAssocs.Enabled = false;
                btnAddIndex.Enabled = false;
                btnRemoveIndex.Enabled = false;
                txtAssocOffset.Enabled = false;

                chkIsCalculatedSpotFormula.Checked = false;
                lookupCalculatedSpotIndex.Enabled = false;
                seCalculatedSpotOffset.Enabled = false;
            }
            else
            {
                txtSpotMappingName.Enabled = false;
            }
        }
        
        private void ChkIsAssocIndexCheckedChanged(object sender, EventArgs e)
        {
            if (chkIsAssocIndex.Checked)
            {
                lcgIndexAssocs.Enabled = true;
                btnAddIndex.Enabled = true;
                btnRemoveIndex.Enabled = true;
                txtAssocOffset.Enabled = true;

                lookupIndex_1.Properties.DataSource = _indexes.Where(a => a.MarketId == (long)lookupMarket.EditValue);

                chkHasSpotFormula.Checked = false;
                txtSpotMappingName.Enabled = false;

                chkIsCalculatedSpotFormula.Checked = false;
                lookupCalculatedSpotIndex.Enabled = false;
                seCalculatedSpotOffset.Enabled = false;
            }
            else
            {
                lcgIndexAssocs.Enabled = false;
                btnAddIndex.Enabled = false;
                btnRemoveIndex.Enabled = false;
                txtAssocOffset.Enabled = false;
            }
        }

        private void chkIsCalculatedSpotFormula_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsCalculatedSpotFormula.Checked)
            {
                lookupCalculatedSpotIndex.Enabled = true;
                seCalculatedSpotOffset.Enabled = true;

                chkIsAssocIndex.Checked = false;
                lcgIndexAssocs.Enabled = false;
                btnAddIndex.Enabled = false;
                btnRemoveIndex.Enabled = false;
                txtAssocOffset.Enabled = false;

                chkHasSpotFormula.Checked = false;
                txtSpotMappingName.Enabled = false;
            }
            else
            {
                lookupCalculatedSpotIndex.Enabled = false;
                seCalculatedSpotOffset.Enabled = false;
            }
        }

        private void lookupCoverAssoc_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == ButtonPredefines.Delete)
            {
                lookupCoverAssoc.EditValue = null;
                seCoverAssocOffset.Value = 0;
            }
        }

        private void lookupCalculatedSpotIndex_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == ButtonPredefines.Delete)
            {
                lookupCalculatedSpotIndex.EditValue = null;
                seCalculatedSpotOffset.Value = 0;
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVIndexInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                //SessionRegistry.Client.BeginAEVIndexInitializationData(_formActionType == FormActionTypeEnum.Edit && _index.IsCustom ? _index.Id : (long?) null,
                //    EndAEVIndexInitializationData, null);
                SessionRegistry.Client.BeginAEVIndexInitializationData(_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View ? _index.Id : (long?)null,
                    EndAEVIndexInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVIndexInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVIndexInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;

            List<Market> markets;
            List<IndexCustomValue> customValues;
            
            
            try
            {
                result = SessionRegistry.Client.EndAEVIndexInitializationData(out markets, out _indexes, out customValues, out _averageIndexesAssocs, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookupMarket.Tag = markets;
                lookupMarket.Properties.DataSource = markets;

                lookupSourceIndex.Tag = _indexes;
                
                lookupIndex_1.Tag = _indexes;
                lookupIndex_1.Properties.DataSource = _indexes;

                lookupCoverAssoc.Tag = _indexes;
                lookupCoverAssoc.Properties.DataSource = _indexes;

                lookupCalculatedSpotIndex.Tag = _indexes;
                lookupCalculatedSpotIndex.Properties.DataSource = _indexes;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();
                
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVIndex(bool isEdit, List<AverageIndexesAssoc> assocs)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEIndex(isEdit, _index, assocs, EndAEVIndex, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVIndex(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVIndex;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEIndex(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName,  Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 2)
            {
                // Average index for the selected market already exists
                errorProvider.SetError(chkIsMarketDefault, "There is already defined a default index for the selected market.");
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 3)
            {                
                errorProvider.SetError(chkIsMarketDefaultForReports, "There is already defined a default index for the selected market for reports.");
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        private void BeginGetIndexValues(long indexId, DateTime date, int absoluteOffset, int percentageOffset)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetIndexValues(indexId, date, absoluteOffset, percentageOffset, EndGetIndexValues, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetIndexValues(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetIndexValues;
                Invoke(action, ar);
                return;
            }
            int? result;

            List<IndexSpotValue> spotValues;
            List<IndexFFAValue> originalBFAValues;
            Dictionary<DateTime, decimal> normalizedBFAValues;
            Dictionary<DateTime, decimal> curveValues;

            try
            {
                result = SessionRegistry.Client.EndGetIndexValues(out spotValues, out originalBFAValues, out normalizedBFAValues, out curveValues, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridSpotValues.DataSource = spotValues.OrderBy(a => a.Date).ToList();
                gridOriginalBFAValues.DataSource = originalBFAValues.OrderBy(a=>a.PeriodFrom).ThenBy(b=>b.PeriodTo).ToList();
                
                

                chartValues.Series["seriesCurve"].Points.Clear();
                chartValues.Series["seriesNormalizedBFA"].Points.Clear();

                if (curveValues != null)
                {
                    gridCurveValues.DataSource = curveValues.Select(a => new { Date = a.Key, Price = Math.Round(a.Value, 4) }).ToList();

                    foreach (KeyValuePair<DateTime, decimal> finalCurveValueKeyValue in curveValues)
                    {
                        if (finalCurveValueKeyValue.Key.Day == 1 || finalCurveValueKeyValue.Key.Day == 15) chartValues.Series["seriesCurve"].Points.Add(new SeriesPoint(finalCurveValueKeyValue.Key, finalCurveValueKeyValue.Value));
                    }
                }
                else
                {
                    gridCurveValues.DataSource = null;
                }

                if (normalizedBFAValues != null)
                {
                    gridNormalizedBFAValues.DataSource = normalizedBFAValues.Select(a => new { Date = a.Key, Price = a.Value }).ToList();
                    foreach (KeyValuePair<DateTime, decimal> finalCurveValueKeyValue in normalizedBFAValues)
                    {
                        chartValues.Series["seriesNormalizedBFA"].Points.Add(new SeriesPoint(finalCurveValueKeyValue.Key, finalCurveValueKeyValue.Value));
                    }
                }
                else
                {
                    gridNormalizedBFAValues.DataSource = null;
                }                            
               

                btnExportCurveValues.Enabled = gridCurveValuesMainView.DataRowCount > 0;

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Index Index
        {
            get { return _index; }
        }

        public FormActionTypeEnum FormActionType { get { return _formActionType; } }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
       
    }
}