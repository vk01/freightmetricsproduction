﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class TraderAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private Trader _trader;

        #endregion

        #region Constructors

        public TraderAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public TraderAEVForm(Trader trader, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _trader = trader;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            treeBooks.BeginUpdate();
            treeBooks.Columns.Add();
            treeBooks.Columns[0].Caption = Strings.Book_Name;
            treeBooks.Columns[0].VisibleIndex = 0;
            treeBooks.EndUpdate();

            lookupProfitCentre.Properties.DisplayMember = "Name";
            lookupProfitCentre.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
                          {
                              Caption = Strings.Profit_Centre_Name,
                              FieldName = "Name",
                              Width = 0
                          };
            lookupProfitCentre.Properties.Columns.Add(col);
            lookupProfitCentre.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupProfitCentre.Properties.SearchMode = SearchMode.AutoFilter;
            lookupProfitCentre.Properties.NullValuePrompt = Strings.Select_a_Profit_Centre___;
            lookupProfitCentre.Properties.NullText = Strings.Select_a_Profit_Centre___;

            lookupDesk.Properties.DisplayMember = "Name";
            lookupDesk.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Desk_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupDesk.Properties.Columns.Add(col);
            lookupDesk.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupDesk.Properties.SearchMode = SearchMode.AutoFilter;
            lookupDesk.Properties.NullValuePrompt = Strings.Select_a_Desk___;
            lookupDesk.Properties.NullText = Strings.Select_a_Desk___;

            if (_formActionType == FormActionTypeEnum.View)
            {
                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                lstCompanies.Enabled = false;
                lstMarkets.Enabled = false;
                lstTradeTypes.Enabled = false;
                lookupDesk.Properties.ReadOnly = true;
                lookupProfitCentre.Properties.ReadOnly = true;

                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void InitializeData()
        {
            BeginAEVTraderInitializationData();
        }

        private void PopulateBookTree(TreeListNode parentNode, List<CustomBook> childrenBooks,
                                  ref List<CustomBook> allBooks)
        {
            foreach (CustomBook book in childrenBooks)
            {
                TreeListNode childNode =
                    treeBooks.AppendNode(
                        new object[] { book.Name }, parentNode, book);
                PopulateBookTree(childNode, allBooks.Where(a => a.ParentBookId == book.Id).ToList(), ref allBooks);
            }
        }

        private void LoadData()
        {
            lblId.Text = _trader.Id.ToString();
            txtName.Text = _trader.Name;
            cmbStatus.SelectedItem = _trader.Status;
            lookupProfitCentre.EditValue = _trader.ProfitCentreId;
            lookupDesk.EditValue = _trader.DeskId;

            foreach (CustomBook book in _trader.Books)
            {
                treeBooks.NodesIterator.DoOperation(a =>
                {
                    if (a.Tag != null &&
                        ((CustomBook)a.Tag).
                            Id ==
                        book.Id)
                    {
                        a.Checked = true;
                        a.RootNode.ExpandAll();
                    }
                });
            }
            treeBooks.ExpandAll();

            foreach (Company company in _trader.Companies)
            {
                for (int i = 0; i < lstCompanies.Items.Count; i++ )
                {
                    var item = lstCompanies.Items[i];
                    if (((Company)item.Value).Id == company.Id)
                        lstCompanies.SetItemChecked(i, true);
                }
            }
            foreach (Market market in _trader.Markets)
            {
                for (int i = 0; i < lstMarkets.Items.Count; i++)
                {
                    var item = lstMarkets.Items[i];
                    if (((Market)item.Value).Id == market.Id)
                        lstMarkets.SetItemChecked(i, true);
                }
            }

            foreach (TradeTypeEnum tradeType in _trader.TradeTypesList)
            {
                for (int i = 0; i < lstTradeTypes.Items.Count; i++)
                {
                    var item = lstTradeTypes.Items[i];
                    if ((TradeTypeEnum)item.Value == tradeType)
                        lstTradeTypes.SetItemChecked(i, true);
                }
            }

            txtCreationUser.Text = _trader.Cruser;
            dtpCreationDate.DateTime = _trader.Crd;
            txtUpdateUser.Text = _trader.Chuser;
            dtpUpdateDate.DateTime = _trader.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            
            //if (lstMarkets.CheckedItems.Count == 0) errorProvider.SetError(lstMarkets, Strings.Field_is_mandatory);
            //bool blCheckedCompanyFound = false;
            //treeCompanies.NodesIterator.DoOperation(a =>
            //                                            {
            //                                                if (a.Checked && !blCheckedCompanyFound)
            //                                                {
            //                                                    blCheckedCompanyFound = true;
            //                                                }
            //                                            }
            //    );
            //if (!blCheckedCompanyFound) errorProvider.SetError(treeCompanies, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        private void PropagateCheckStateToChildren(TreeListNode node)
        {
            foreach (TreeListNode treeListNode in node.Nodes)
            {
                treeListNode.Checked = node.Checked;
                PropagateCheckStateToChildren(treeListNode);
            }
        }

        private void PropagateCheckState(TreeListNode node)
        {
            PropagateCheckStateToChildren(node);

            bool checkState = node.Checked;
            node = node.ParentNode;
            while (node != null)
            {
                if (checkState)
                {
                    node.Checked = true;
                }
                else
                {
                    bool allChildNodesUnchecked = true;
                    foreach (TreeListNode treeListNode in node.Nodes)
                    {
                        if (!treeListNode.Checked) continue;
                        allChildNodesUnchecked = false;
                        break;
                    }
                    if (allChildNodesUnchecked)
                    {
                        node.Checked = false;
                    }
                }
                node = node.ParentNode;
            }
        }

        #endregion

        #region GUI Events

        private void TraderAevFormLoad(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (lstTradeTypes.CheckedItems.Count == 0)
            {
                XtraMessageBox.Show(this, Strings.Field_Trade_Types_is_mandatory, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _trader = new Trader();
            }

            _trader.Name = txtName.Text.Trim();
            _trader.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _trader.ProfitCentreId = (long?) lookupProfitCentre.EditValue;
            _trader.DeskId = (long?) lookupProfitCentre.EditValue;

            var companies = new List<Company>();
            foreach (int checkedIndex in lstCompanies.CheckedIndices)
            {
                companies.Add((Company)lstCompanies.GetItemValue(checkedIndex));
            }

            var books = new List<CustomBook>();
            treeBooks.NodesIterator.DoOperation(a => { if (a.Checked) books.Add((CustomBook)a.Tag); });
            
            var markets = new List<Market>();
            foreach (int checkedIndex in lstMarkets.CheckedIndices)
            {
                markets.Add((Market) lstMarkets.GetItemValue(checkedIndex));
            }
            
            var tradeTypes = new List<TradeTypeEnum>();
            foreach (int checkedIndex in lstTradeTypes.CheckedIndices)
            {
                tradeTypes.Add((TradeTypeEnum)lstTradeTypes.GetItemValue(checkedIndex));
            }

            _trader.Companies = companies;
            _trader.Markets = markets;
            _trader.Books =
                books.Select(
                    a => new CustomBook() {Id = a.Id, Name = a.Name, ParentBookId = a.ParentBookId, Status = a.Status}).
                    ToList();
            _trader.TradeTypes = tradeTypes.Count > 0 ? tradeTypes.Select(a => a.ToString("d")).Aggregate((i, j) => i + "," + j)  : "";

            BeginAEVTrader(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void TraderAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void TraderAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void lookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back) ((LookUpEdit)sender).EditValue = null;
        }

        private void TreeBooksAfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            PropagateCheckState(e.Node);
        }

        private void TreeCompaniesBeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            if (_formActionType == FormActionTypeEnum.View)
                e.CanCheck = false;
        }

        private void TreeBooksBeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            if (_formActionType == FormActionTypeEnum.View)
                e.CanCheck = false;
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVTraderInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVTraderInitializationData(
                    EndAEVTraderInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVTraderInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVTraderInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Company> companies;
            List<Market> markets;
            List<ProfitCentre> profitCentres;
            List<Desk> desks;
            List<Book> books;
            try
            {
                result = SessionRegistry.Client.EndAEVTraderInitializationData(out companies, out markets,
                                                                               out profitCentres, out desks, out books,
                                                                               ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                List<CustomBook> traderBooks = books.Select(
                        a =>
                        new CustomBook() { Id = a.Id, Name = a.Name, ParentBookId = a.ParentBookId, Status = a.Status }).
                        ToList();
                treeBooks.Tag = traderBooks;

                PopulateBookTree(null, traderBooks.Where(a => a.ParentBookId == null).ToList(), ref traderBooks);

                foreach (Company company in companies)
                {
                    lstCompanies.Items.Add(new CheckedListBoxItem(company, company.Name));
                }

                foreach (Market market in markets)
                {
                    lstMarkets.Items.Add(new CheckedListBoxItem(market, market.Name));
                }

                foreach (var enumValue in Enum.GetValues(typeof(TradeTypeEnum)))
                {
                    lstTradeTypes.Items.Add(new CheckedListBoxItem(Enum.Parse(typeof(TradeTypeEnum), enumValue.ToString(), true), Enum.GetName(typeof(TradeTypeEnum), enumValue)));
                }

                lookupProfitCentre.Tag = profitCentres;
                lookupProfitCentre.Properties.DataSource = profitCentres;

                lookupDesk.Tag = desks;
                lookupDesk.Properties.DataSource = desks;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVTrader(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _trader, EndAEVTrader, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVTrader(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVTrader;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName,  Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Trader Trader
        {
            get { return _trader; }
        }

        public FormActionTypeEnum FormActionType { get { return _formActionType; } }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
        
    }
}