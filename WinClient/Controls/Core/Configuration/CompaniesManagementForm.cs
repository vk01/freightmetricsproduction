﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class CompaniesManagementForm : XtraForm
    {
        #region Private Properties

        private WaitDialogForm _dialogForm;
        private object _syncObject = new object();

#endregion

        #region Constructors

        public CompaniesManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void CompaniesManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetCompanies();
        }

        private void btnAdd_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof (Company));
        }

        private void btnEdit_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (treeCompanies.FocusedNode == null) return;

            if (EditEntityEvent != null) EditEntityEvent((Company) treeCompanies.FocusedNode.Tag);
        }

        private void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (treeCompanies.FocusedNode == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((Company)treeCompanies.FocusedNode.Tag);
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetCompanies();
        }

        private void btnSubtypes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnSubtypesEvent != null)
                OnSubtypesEvent();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void CompaniesManagementForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void CompaniesManagementForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            treeCompanies.BeginUpdate();

            treeCompanies.OptionsBehavior.EnableFiltering = true;

            treeCompanies.Columns.Add();
            treeCompanies.Columns[0].Caption = Strings.Name;
            treeCompanies.Columns[0].VisibleIndex = 0;
            treeCompanies.Columns[0].UnboundType = UnboundColumnType.String;

            treeCompanies.Columns.Add();
            treeCompanies.Columns[1].Caption = Strings.Status;
            treeCompanies.Columns[1].VisibleIndex = 1;
            treeCompanies.Columns[1].UnboundType = UnboundColumnType.Object;

            treeCompanies.Columns.Add();
            treeCompanies.Columns[2].Caption = Strings.Type;
            treeCompanies.Columns[2].VisibleIndex = 2;
            treeCompanies.Columns[2].UnboundType = UnboundColumnType.Object;

            treeCompanies.Columns.Add();
            treeCompanies.Columns[3].Caption = Strings.Subtype;
            treeCompanies.Columns[3].VisibleIndex = 3;
            treeCompanies.Columns[3].UnboundType = UnboundColumnType.String;

            treeCompanies.Columns.Add();
            treeCompanies.Columns[4].Caption = Strings.Risk_Rating;
            treeCompanies.Columns[4].VisibleIndex = 4;
            treeCompanies.Columns[4].UnboundType = UnboundColumnType.Integer;

            treeCompanies.Columns.Add();
            treeCompanies.Columns[5].Caption = Strings.Manager;
            treeCompanies.Columns[5].VisibleIndex = 5;
            treeCompanies.Columns[5].UnboundType = UnboundColumnType.String;

            treeCompanies.EndUpdate();
        }

        private void PopulateTree(TreeListNode parentNode, List<Company> childrenCompanies,
                                  ref List<Company> allCompanies)
        {
            foreach (Company company in childrenCompanies)
            {
                TreeListNode childNode =
                    treeCompanies.AppendNode(
                        new object[]
                            {
                                company.Name, company.Status, company.Type, company.SubtypeName, company.RiskRating,
                                company.ManagerName
                            }, parentNode, company);
                PopulateTree(childNode, allCompanies.Where(a => a.ParentId == company.Id).ToList(), ref allCompanies);
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginGetCompanies()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

                try
                {
                    SessionRegistry.Client.BeginGetEntities(new Company(), EndGetCompanies, null);
                }
                catch (Exception)
                {
                    Cursor = Cursors.Default;
                    lock (_syncObject)
                    {
                        _dialogForm.Close();
                        _dialogForm = null;
                    }
                    SessionRegistry.ResetClientService();
                    XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void EndGetCompanies(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetCompanies;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            List<Company> companies;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                Strings.
                                    There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                Strings.Freight_Metrics,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            companies = entities.Cast<Company>().ToList();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                treeCompanies.ClearNodes();
                PopulateTree(null, companies.Where(a => a.ParentId == null).ToList(), ref companies);
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<object, bool> OnDataSaved;
        public event Action OnSubtypesEvent;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

#endregion

        
    }
}