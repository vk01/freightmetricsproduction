﻿namespace Exis.WinClient.Controls
{
    partial class InactiveTradesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InactiveTradesForm));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridTradeDetails = new DevExpress.XtraGrid.GridControl();
            this.gridTradeDetailsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTradesGridView = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnActivate = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetailsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTradesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.gridTradeDetails);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlRootGroup;
            this.layoutControl.Size = new System.Drawing.Size(806, 482);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "activate24x24.png");
            this.imageList24.Images.SetKeyName(1, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(2, "exit.ico");
            // 
            // gridTradeDetails
            // 
            this.gridTradeDetails.Location = new System.Drawing.Point(12, 12);
            this.gridTradeDetails.MainView = this.gridTradeDetailsView;
            this.gridTradeDetails.Name = "gridTradeDetails";
            this.gridTradeDetails.Size = new System.Drawing.Size(782, 458);
            this.gridTradeDetails.TabIndex = 6;
            this.gridTradeDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridTradeDetailsView});
            // 
            // gridTradeDetailsView
            // 
            this.gridTradeDetailsView.GridControl = this.gridTradeDetails;
            this.gridTradeDetailsView.Name = "gridTradeDetailsView";
            this.gridTradeDetailsView.OptionsBehavior.Editable = false;
            this.gridTradeDetailsView.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridTradeDetailsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridTradeDetailsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridTradeDetailsView.OptionsView.ShowFooter = true;
            // 
            // layoutControlRootGroup
            // 
            this.layoutControlRootGroup.CustomizationFormText = "layoutControlRootGroup";
            this.layoutControlRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlRootGroup.GroupBordersVisible = false;
            this.layoutControlRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTradesGridView});
            this.layoutControlRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRootGroup.Name = "layoutControlRootGroup";
            this.layoutControlRootGroup.Size = new System.Drawing.Size(806, 482);
            this.layoutControlRootGroup.Text = "layoutControlRootGroup";
            this.layoutControlRootGroup.TextVisible = false;
            // 
            // lciTradesGridView
            // 
            this.lciTradesGridView.Control = this.gridTradeDetails;
            this.lciTradesGridView.CustomizationFormText = "lciTradesGridView";
            this.lciTradesGridView.Location = new System.Drawing.Point(0, 0);
            this.lciTradesGridView.Name = "lciTradesGridView";
            this.lciTradesGridView.Size = new System.Drawing.Size(786, 462);
            this.lciTradesGridView.Text = "lciTradesGridView";
            this.lciTradesGridView.TextSize = new System.Drawing.Size(0, 0);
            this.lciTradesGridView.TextToControlDistance = 0;
            this.lciTradesGridView.TextVisible = false;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnActivate,
            this.btnClose,
            this.btnRefresh});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(806, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 482);
            this.barDockControlBottom.Size = new System.Drawing.Size(806, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(806, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 482);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnActivate),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnActivate
            // 
            this.btnActivate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnActivate.Caption = "Activate";
            this.btnActivate.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnActivate.Id = 0;
            this.btnActivate.LargeImageIndex = 0;
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnActivateClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 2;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnRefresh.Caption = "Refresh";
            this.btnRefresh.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnRefresh.Id = 2;
            this.btnRefresh.LargeImageIndex = 1;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnRefreshClick);
            // 
            // InactiveTradesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 514);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "InactiveTradesForm";
            this.Text = "Application Parameters";
            this.Load += new System.EventHandler(this.InactiveTradesFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetailsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTradesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlRootGroup;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraGrid.GridControl gridTradeDetails;
        private DevExpress.XtraGrid.Views.Grid.GridView gridTradeDetailsView;
        private DevExpress.XtraLayout.LayoutControlItem lciTradesGridView;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnActivate;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}