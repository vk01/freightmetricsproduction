﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class CompanySubtypeAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private CompanySubtype _companySubtype;
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public CompanySubtypeAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public CompanySubtypeAEVForm(CompanySubtype companySubtype, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _companySubtype = companySubtype;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            foreach (var enumValue in Enum.GetValues(typeof(CompanySubtypeEnum)))
            {
                cmbSubtype.Properties.Items.Add(Enum.Parse(typeof (CompanySubtypeEnum), enumValue.ToString(), true));
            }

            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;

                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                cmbSubtype.Properties.ReadOnly = true;
            }
        }

        private void InitializeData()
        {
            if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                LoadData();
        }

        private void LoadData()
        {
            lblId.Text = _companySubtype.Id.ToString();
            txtName.Text = _companySubtype.Name;
            cmbStatus.SelectedItem = _companySubtype.Status;
            cmbSubtype.SelectedItem = _companySubtype.Subtype;

            txtCreationUser.Text = _companySubtype.Cruser;
            dtpCreationDate.DateTime = _companySubtype.Crd;
            txtUpdateUser.Text = _companySubtype.Chuser;
            dtpUpdateDate.DateTime = _companySubtype.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (cmbSubtype.SelectedItem == null) errorProvider.SetError(cmbSubtype, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void CompanySubtypeAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _companySubtype = new CompanySubtype();
            }

            _companySubtype.Name = txtName.Text.Trim();
            _companySubtype.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _companySubtype.Subtype = (CompanySubtypeEnum) cmbSubtype.SelectedItem;

            BeginAEVCompanySubtype(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void CompanySubtypeAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void CompanySubtypeAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVCompanySubtype(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _companySubtype, EndAEVCompanySubtype, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVCompanySubtype(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVCompanySubtype;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public CompanySubtype CompanySubtype
        {
            get { return _companySubtype; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}