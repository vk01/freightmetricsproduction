﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class BookAEVForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private Book _book;
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public BookAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public BookAEVForm(Book book, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _book = book;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);
            cmbStatus.SelectedIndex = 1;

            foreach (var enumValue in Enum.GetValues(typeof(BookPositionCalculatedByDaysEnum)).Cast<BookPositionCalculatedByDaysEnum>())
            {
                cmbPositionCalculatedByDays.Properties.Items.Add(enumValue);
            }

            cmbPositionCalculatedByDays.SelectedIndex = 0;

            treeParentBook.BeginUpdate();
            treeParentBook.Columns.Add();
            treeParentBook.Columns[0].Caption = Strings.Book_Name;
            treeParentBook.Columns[0].VisibleIndex = 0;
            treeParentBook.EndUpdate();

            if (_formActionType == FormActionTypeEnum.View)
            {
                //layoutSave.Visibility = LayoutVisibility.Never;
                btnSave.Visibility = BarItemVisibility.Never;

                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                cmbPositionCalculatedByDays.Properties.ReadOnly = true;
                treeParentBook.OptionsBehavior.Editable = false;
            }
        }

        private void InitializeData()
        {
            BeginAEVBookInitializationData();
        }

        private void PopulateTree(TreeListNode parentNode, List<Book> childrenBooks,
                                  ref List<Book> allBooks)
        {
            foreach (Book book in childrenBooks)
            {
                if (_formActionType == FormActionTypeEnum.Edit && book.Id == _book.Id) continue;
                TreeListNode childNode =
                    treeParentBook.AppendNode(
                        new object[] {book.Name}, parentNode, book);
                PopulateTree(childNode, allBooks.Where(a => a.ParentBookId == book.Id).ToList(), ref allBooks);
            }
        }

        private void LoadData()
        {
            lblId.Text = _book.Id.ToString();
            txtName.Text = _book.Name;
            cmbStatus.SelectedItem = _book.Status;
            cmbPositionCalculatedByDays.SelectedItem = _book.PositionCalculatedByDays;

            if (_book.ParentBookId != null)
            {
                treeParentBook.NodesIterator.DoOperation(a =>
                                                                {
                                                                    if (a.Tag != null &&
                                                                        ((Book) a.Tag).
                                                                            Id ==
                                                                        _book.ParentBookId)
                                                                    {
                                                                        treeParentBook.
                                                                            FocusedNode =
                                                                            a;
                                                                        return;
                                                                    }
                                                                });
            }
            else
            {
                treeParentBook.FocusedNode = null;
            }
            txtCreationUser.Text = _book.Cruser;
            dtpCreationDate.DateTime = _book.Crd;
            txtUpdateUser.Text = _book.Chuser;
            dtpUpdateDate.DateTime = _book.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (cmbPositionCalculatedByDays.SelectedItem == null) errorProvider.SetError(cmbPositionCalculatedByDays, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void BookAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _book = new Book();
            }

            _book.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _book.PositionCalculatedByDays = (BookPositionCalculatedByDaysEnum)cmbPositionCalculatedByDays.SelectedItem;
            _book.Name = txtName.Text.Trim();
            _book.ParentBookId = (treeParentBook.FocusedNode == null || treeParentBook.FocusedNode.Tag == null)
                                    ? (long?)null
                                    : ((Book)treeParentBook.FocusedNode.Tag).Id;

            BeginAEVBook(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void BookAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void BookAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void treeParentBook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back) treeParentBook.FocusedNode = null;
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVBookInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

                try
                {
                    SessionRegistry.Client.BeginAEVBookInitializationData(
                        EndAEVBookInitializationData, null);
                }
                catch (Exception)
                {
                    lock (_syncObject)
                    {
                        _dialogForm.Close();
                        _dialogForm = null;
                    }
                    SessionRegistry.ResetClientService();
                    XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void EndAEVBookInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVBookInitializationData;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Book> books;

            try
            {
                result = SessionRegistry.Client.EndAEVBookInitializationData(out books, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                Strings.
                                    There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                Strings.Freight_Metrics,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                treeParentBook.Tag = books;
                treeParentBook.AppendNode(new object[] { "None" }, null, null);
                PopulateTree(null, books.Where(a => a.ParentBookId == null).ToList(), ref books);

                if (_formActionType == FormActionTypeEnum.Edit|| _formActionType == FormActionTypeEnum.View) LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVBook(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _book, EndAEVBook, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVBook(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVBook;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Book Book { get { return _book; } }

        public FormActionTypeEnum FormActionType{get { return _formActionType; }}

        #endregion
        
        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

        private void treeParentBook_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if(_formActionType != FormActionTypeEnum.View)
            {
                if (treeParentBook.FocusedNode == null || treeParentBook.FocusedNode.Tag == null)
                    cmbPositionCalculatedByDays.Properties.ReadOnly = false;
                else
                {
                    cmbPositionCalculatedByDays.SelectedItem = ((Book)(treeParentBook.FocusedNode.Tag)).PositionCalculatedByDays;
                    cmbPositionCalculatedByDays.Properties.ReadOnly = true;
                }
                    

            }
            

        }
    }
}