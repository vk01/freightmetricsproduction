﻿namespace Exis.WinClient.Controls
{
    partial class TraderAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TraderAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.lstCompanies = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.treeBooks = new DevExpress.XtraTreeList.TreeList();
            this.lstTradeTypes = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.lstMarkets = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.lookupDesk = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.lookupProfitCentre = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTrader = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarkets = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutProfitCentre = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDesk = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTradeTypes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBooks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCompanies = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstCompanies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstTradeTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstMarkets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupDesk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupProfitCentre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarkets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProfitCentre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDesk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCompanies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.lstCompanies);
            this.layoutRootControl.Controls.Add(this.treeBooks);
            this.layoutRootControl.Controls.Add(this.lstTradeTypes);
            this.layoutRootControl.Controls.Add(this.lstMarkets);
            this.layoutRootControl.Controls.Add(this.lookupDesk);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.lookupProfitCentre);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtName);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 426, 250, 350);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(1086, 600);
            this.layoutRootControl.TabIndex = 1;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // lstCompanies
            // 
            this.lstCompanies.CheckOnClick = true;
            this.lstCompanies.Location = new System.Drawing.Point(73, 57);
            this.lstCompanies.Name = "lstCompanies";
            this.lstCompanies.Size = new System.Drawing.Size(281, 211);
            this.lstCompanies.StyleController = this.layoutRootControl;
            this.lstCompanies.TabIndex = 32;
            // 
            // treeBooks
            // 
            this.treeBooks.Location = new System.Drawing.Point(858, 57);
            this.treeBooks.Name = "treeBooks";
            this.treeBooks.OptionsBehavior.AllowExpandOnDblClick = false;
            this.treeBooks.OptionsBehavior.Editable = false;
            this.treeBooks.OptionsView.ShowCheckBoxes = true;
            this.treeBooks.OptionsView.ShowColumns = false;
            this.treeBooks.OptionsView.ShowHorzLines = false;
            this.treeBooks.OptionsView.ShowIndicator = false;
            this.treeBooks.OptionsView.ShowVertLines = false;
            this.treeBooks.Size = new System.Drawing.Size(214, 211);
            this.treeBooks.TabIndex = 20;
            this.treeBooks.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.TreeBooksBeforeCheckNode);
            this.treeBooks.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.TreeBooksAfterCheckNode);
            // 
            // lstTradeTypes
            // 
            this.lstTradeTypes.CheckOnClick = true;
            this.lstTradeTypes.Location = new System.Drawing.Point(627, 57);
            this.lstTradeTypes.Name = "lstTradeTypes";
            this.lstTradeTypes.Size = new System.Drawing.Size(192, 211);
            this.lstTradeTypes.StyleController = this.layoutRootControl;
            this.lstTradeTypes.TabIndex = 32;
            // 
            // lstMarkets
            // 
            this.lstMarkets.CheckOnClick = true;
            this.lstMarkets.Location = new System.Drawing.Point(403, 57);
            this.lstMarkets.Name = "lstMarkets";
            this.lstMarkets.Size = new System.Drawing.Size(153, 211);
            this.lstMarkets.StyleController = this.layoutRootControl;
            this.lstMarkets.TabIndex = 31;
            // 
            // lookupDesk
            // 
            this.lookupDesk.Location = new System.Drawing.Point(590, 272);
            this.lookupDesk.Name = "lookupDesk";
            this.lookupDesk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupDesk.Size = new System.Drawing.Size(482, 20);
            this.lookupDesk.StyleController = this.layoutRootControl;
            this.lookupDesk.TabIndex = 21;
            this.lookupDesk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(971, 339);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Size = new System.Drawing.Size(101, 20);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 8;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(610, 339);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(289, 20);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 11;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(438, 339);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Size = new System.Drawing.Size(101, 20);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 7;
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(87, 339);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(273, 20);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 10;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(33, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(13, 16);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // lookupProfitCentre
            // 
            this.lookupProfitCentre.Location = new System.Drawing.Point(83, 272);
            this.lookupProfitCentre.Name = "lookupProfitCentre";
            this.lookupProfitCentre.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupProfitCentre.Size = new System.Drawing.Size(473, 20);
            this.lookupProfitCentre.StyleController = this.layoutRootControl;
            this.lookupProfitCentre.TabIndex = 20;
            this.lookupProfitCentre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(962, 33);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(110, 20);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(86, 33);
            this.txtName.Name = "txtName";
            this.txtName.Properties.MaxLength = 1000;
            this.txtName.Size = new System.Drawing.Size(832, 20);
            this.txtName.StyleController = this.layoutRootControl;
            this.txtName.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTrader,
            this.emptySpaceItem1,
            this.layoutGroupUserInfo});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(1086, 600);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupTrader
            // 
            this.layoutGroupTrader.CustomizationFormText = "Company";
            this.layoutGroupTrader.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutId,
            this.layoutStatus,
            this.layoutMarkets,
            this.layoutProfitCentre,
            this.layoutDesk,
            this.layoutTradeTypes,
            this.layoutBooks,
            this.layoutItemCompanies});
            this.layoutGroupTrader.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTrader.Name = "layoutGroupTrader";
            this.layoutGroupTrader.Size = new System.Drawing.Size(1086, 306);
            this.layoutGroupTrader.Text = "Trader";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "Name:";
            this.layoutName.Location = new System.Drawing.Point(36, 0);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(872, 24);
            this.layoutName.Text = "Name:";
            this.layoutName.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutName.TextSize = new System.Drawing.Size(31, 13);
            this.layoutName.TextToControlDistance = 5;
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(36, 24);
            this.layoutId.Text = "Id:";
            this.layoutId.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutId.TextSize = new System.Drawing.Size(14, 13);
            this.layoutId.TextToControlDistance = 5;
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(908, 0);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(154, 24);
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutStatus.TextSize = new System.Drawing.Size(35, 13);
            this.layoutStatus.TextToControlDistance = 5;
            // 
            // layoutMarkets
            // 
            this.layoutMarkets.Control = this.lstMarkets;
            this.layoutMarkets.CustomizationFormText = "Markets:";
            this.layoutMarkets.Location = new System.Drawing.Point(344, 24);
            this.layoutMarkets.Name = "layoutMarkets";
            this.layoutMarkets.Size = new System.Drawing.Size(202, 215);
            this.layoutMarkets.Text = "Markets:";
            this.layoutMarkets.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutProfitCentre
            // 
            this.layoutProfitCentre.Control = this.lookupProfitCentre;
            this.layoutProfitCentre.CustomizationFormText = "Profit Centre:";
            this.layoutProfitCentre.Location = new System.Drawing.Point(0, 239);
            this.layoutProfitCentre.Name = "layoutProfitCentre";
            this.layoutProfitCentre.Size = new System.Drawing.Size(546, 24);
            this.layoutProfitCentre.Text = "Profit Centre:";
            this.layoutProfitCentre.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutDesk
            // 
            this.layoutDesk.Control = this.lookupDesk;
            this.layoutDesk.CustomizationFormText = "Desk:";
            this.layoutDesk.Location = new System.Drawing.Point(546, 239);
            this.layoutDesk.Name = "layoutDesk";
            this.layoutDesk.Size = new System.Drawing.Size(516, 24);
            this.layoutDesk.Text = "Desk:";
            this.layoutDesk.TextSize = new System.Drawing.Size(27, 13);
            // 
            // layoutTradeTypes
            // 
            this.layoutTradeTypes.Control = this.lstTradeTypes;
            this.layoutTradeTypes.CustomizationFormText = "Trade Types:";
            this.layoutTradeTypes.Location = new System.Drawing.Point(546, 24);
            this.layoutTradeTypes.Name = "layoutTradeTypes";
            this.layoutTradeTypes.Size = new System.Drawing.Size(263, 215);
            this.layoutTradeTypes.Text = "Trade Types:";
            this.layoutTradeTypes.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutBooks
            // 
            this.layoutBooks.Control = this.treeBooks;
            this.layoutBooks.CustomizationFormText = "Books:";
            this.layoutBooks.Location = new System.Drawing.Point(809, 24);
            this.layoutBooks.Name = "layoutBooks";
            this.layoutBooks.Size = new System.Drawing.Size(253, 215);
            this.layoutBooks.Text = "Books:";
            this.layoutBooks.TextSize = new System.Drawing.Size(32, 13);
            // 
            // layoutItemCompanies
            // 
            this.layoutItemCompanies.Control = this.lstCompanies;
            this.layoutItemCompanies.CustomizationFormText = "layoutItemCompanies";
            this.layoutItemCompanies.Location = new System.Drawing.Point(0, 24);
            this.layoutItemCompanies.Name = "layoutItemCompanies";
            this.layoutItemCompanies.Size = new System.Drawing.Size(344, 215);
            this.layoutItemCompanies.Text = "Companies:";
            this.layoutItemCompanies.TextSize = new System.Drawing.Size(56, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 373);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1086, 227);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "User Info";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 306);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(1086, 67);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "Creation User:";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(350, 24);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(350, 0);
            this.layoutCreationDate.MaxSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.MinSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(529, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(360, 24);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(889, 0);
            this.layoutUpdateDate.MaxSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.MinSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1086, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 600);
            this.barDockControlBottom.Size = new System.Drawing.Size(1086, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 600);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1086, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 600);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // TraderAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 632);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TraderAEVForm";
            this.Activated += new System.EventHandler(this.TraderAevFormActivated);
            this.Deactivate += new System.EventHandler(this.TraderAevFormDeactivate);
            this.Load += new System.EventHandler(this.TraderAevFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstCompanies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstTradeTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstMarkets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupDesk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupProfitCentre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarkets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProfitCentre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDesk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCompanies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraEditors.LookUpEdit lookupProfitCentre;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTrader;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutProfitCentre;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.LookUpEdit lookupDesk;
        private DevExpress.XtraLayout.LayoutControlItem layoutDesk;
        private DevExpress.XtraEditors.CheckedListBoxControl lstMarkets;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarkets;
        private DevExpress.XtraEditors.CheckedListBoxControl lstTradeTypes;
        private DevExpress.XtraLayout.LayoutControlItem layoutTradeTypes;
        private DevExpress.XtraTreeList.TreeList treeBooks;
        private DevExpress.XtraLayout.LayoutControlItem layoutBooks;
        private DevExpress.XtraEditors.CheckedListBoxControl lstCompanies;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCompanies;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}