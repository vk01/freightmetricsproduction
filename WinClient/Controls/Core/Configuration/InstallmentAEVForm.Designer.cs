﻿namespace Exis.WinClient.Controls
{
    partial class InstallmentAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstallmentAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.cmbBorrower = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.panelTranches = new DevExpress.XtraEditors.PanelControl();
            this.lcTranches = new DevExpress.XtraLayout.LayoutControl();
            this.cmbPeriodType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtBalloonAmount_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbPrincipalFrequency_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtRepayPeriod_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbInterestFrequency_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTranchAmount_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbInterestRateType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtInterestRate_1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtInterestCorFactor_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lcgRootTranches = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranches = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranchInfo_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranchMainInfo_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranchMainInfoPart1_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutTranchAmount_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBalloonAmount_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPrincipalFrequency_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterestFrequency_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRepayPeriod_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgTranchMainInfoPart2_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutInterestRateType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterestRate_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterestCorFactor_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgInstallments = new DevExpress.XtraLayout.LayoutControlGroup();
            this.chkRetentionAccount = new DevExpress.XtraEditors.CheckEdit();
            this.txtPrepayFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtArrangeFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtAdminFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtVMC = new DevExpress.XtraEditors.SpinEdit();
            this.txtAmount = new DevExpress.XtraEditors.SpinEdit();
            this.lookupCurrency = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpSignDate = new DevExpress.XtraEditors.DateEdit();
            this.lookupGuarantor = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupObligor = new DevExpress.XtraEditors.LookUpEdit();
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemPanelTranches = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupLoan = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemGuarantor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemObligor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemSignDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemVMC = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAdminFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemArrangeFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPrepayFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemRetentionAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCurrency = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBorrower = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBorrower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTranches)).BeginInit();
            this.panelTranches.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcTranches)).BeginInit();
            this.lcTranches.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalloonAmount_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPrincipalFrequency_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepayPeriod_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestFrequency_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTranchAmount_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestRateType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestRate_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestCorFactor_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootTranches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchInfo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTranchAmount_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBalloonAmount_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrincipalFrequency_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestFrequency_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRepayPeriod_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRateType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestCorFactor_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInstallments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetentionAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepayFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArrangeFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVMC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupGuarantor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupObligor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelTranches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupLoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGuarantor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemObligor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSignDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemVMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAdminFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemArrangeFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPrepayFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRetentionAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBorrower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.cmbBorrower);
            this.layoutRootControl.Controls.Add(this.panelTranches);
            this.layoutRootControl.Controls.Add(this.chkRetentionAccount);
            this.layoutRootControl.Controls.Add(this.txtPrepayFee);
            this.layoutRootControl.Controls.Add(this.txtArrangeFee);
            this.layoutRootControl.Controls.Add(this.txtAdminFee);
            this.layoutRootControl.Controls.Add(this.txtVMC);
            this.layoutRootControl.Controls.Add(this.txtAmount);
            this.layoutRootControl.Controls.Add(this.lookupCurrency);
            this.layoutRootControl.Controls.Add(this.dtpSignDate);
            this.layoutRootControl.Controls.Add(this.lookupGuarantor);
            this.layoutRootControl.Controls.Add(this.lookupObligor);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtCode);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1632, 365, 521, 452);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(890, 566);
            this.layoutRootControl.TabIndex = 2;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // cmbBorrower
            // 
            this.cmbBorrower.Location = new System.Drawing.Point(539, 57);
            this.cmbBorrower.Name = "cmbBorrower";
            this.cmbBorrower.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBorrower.Size = new System.Drawing.Size(171, 20);
            this.cmbBorrower.StyleController = this.layoutRootControl;
            this.cmbBorrower.TabIndex = 49;
            // 
            // panelTranches
            // 
            this.panelTranches.Controls.Add(this.lcTranches);
            this.panelTranches.Location = new System.Drawing.Point(2, 133);
            this.panelTranches.Name = "panelTranches";
            this.panelTranches.Size = new System.Drawing.Size(886, 431);
            this.panelTranches.TabIndex = 44;
            // 
            // lcTranches
            // 
            this.lcTranches.Controls.Add(this.cmbPeriodType_1);
            this.lcTranches.Controls.Add(this.txtBalloonAmount_1);
            this.lcTranches.Controls.Add(this.cmbPrincipalFrequency_1);
            this.lcTranches.Controls.Add(this.txtRepayPeriod_1);
            this.lcTranches.Controls.Add(this.cmbInterestFrequency_1);
            this.lcTranches.Controls.Add(this.txtTranchAmount_1);
            this.lcTranches.Controls.Add(this.cmbInterestRateType_1);
            this.lcTranches.Controls.Add(this.txtInterestRate_1);
            this.lcTranches.Controls.Add(this.txtInterestCorFactor_1);
            this.lcTranches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcTranches.Location = new System.Drawing.Point(2, 2);
            this.lcTranches.Name = "lcTranches";
            this.lcTranches.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(252, 286, 250, 350);
            this.lcTranches.Root = this.lcgRootTranches;
            this.lcTranches.Size = new System.Drawing.Size(882, 427);
            this.lcTranches.TabIndex = 0;
            this.lcTranches.Text = "layoutControl1";
            // 
            // cmbPeriodType_1
            // 
            this.cmbPeriodType_1.Location = new System.Drawing.Point(447, 51);
            this.cmbPeriodType_1.Name = "cmbPeriodType_1";
            this.cmbPeriodType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodType_1.Size = new System.Drawing.Size(173, 20);
            this.cmbPeriodType_1.StyleController = this.lcTranches;
            this.cmbPeriodType_1.TabIndex = 43;
            // 
            // txtBalloonAmount_1
            // 
            this.txtBalloonAmount_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBalloonAmount_1.Location = new System.Drawing.Point(281, 27);
            this.txtBalloonAmount_1.Name = "txtBalloonAmount_1";
            this.txtBalloonAmount_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBalloonAmount_1.Properties.Mask.EditMask = "D";
            this.txtBalloonAmount_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtBalloonAmount_1.Properties.MaxLength = 9;
            this.txtBalloonAmount_1.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtBalloonAmount_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtBalloonAmount_1.Size = new System.Drawing.Size(75, 20);
            this.txtBalloonAmount_1.StyleController = this.lcTranches;
            this.txtBalloonAmount_1.TabIndex = 36;
            // 
            // cmbPrincipalFrequency_1
            // 
            this.cmbPrincipalFrequency_1.Location = new System.Drawing.Point(462, 27);
            this.cmbPrincipalFrequency_1.Name = "cmbPrincipalFrequency_1";
            this.cmbPrincipalFrequency_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPrincipalFrequency_1.Size = new System.Drawing.Size(78, 20);
            this.cmbPrincipalFrequency_1.StyleController = this.lcTranches;
            this.cmbPrincipalFrequency_1.TabIndex = 40;
            // 
            // txtRepayPeriod_1
            // 
            this.txtRepayPeriod_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRepayPeriod_1.Location = new System.Drawing.Point(801, 27);
            this.txtRepayPeriod_1.Name = "txtRepayPeriod_1";
            this.txtRepayPeriod_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRepayPeriod_1.Properties.Mask.EditMask = "D";
            this.txtRepayPeriod_1.Properties.MaxLength = 2;
            this.txtRepayPeriod_1.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtRepayPeriod_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtRepayPeriod_1.Size = new System.Drawing.Size(73, 20);
            this.txtRepayPeriod_1.StyleController = this.lcTranches;
            this.txtRepayPeriod_1.TabIndex = 36;
            // 
            // cmbInterestFrequency_1
            // 
            this.cmbInterestFrequency_1.Location = new System.Drawing.Point(646, 27);
            this.cmbInterestFrequency_1.Name = "cmbInterestFrequency_1";
            this.cmbInterestFrequency_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbInterestFrequency_1.Size = new System.Drawing.Size(78, 20);
            this.cmbInterestFrequency_1.StyleController = this.lcTranches;
            this.cmbInterestFrequency_1.TabIndex = 41;
            // 
            // txtTranchAmount_1
            // 
            this.txtTranchAmount_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTranchAmount_1.Location = new System.Drawing.Point(96, 27);
            this.txtTranchAmount_1.Name = "txtTranchAmount_1";
            this.txtTranchAmount_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtTranchAmount_1.Properties.Mask.EditMask = "D";
            this.txtTranchAmount_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTranchAmount_1.Properties.MaxLength = 12;
            this.txtTranchAmount_1.Properties.MaxValue = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.txtTranchAmount_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtTranchAmount_1.Size = new System.Drawing.Size(98, 20);
            this.txtTranchAmount_1.StyleController = this.lcTranches;
            this.txtTranchAmount_1.TabIndex = 35;
            // 
            // cmbInterestRateType_1
            // 
            this.cmbInterestRateType_1.Location = new System.Drawing.Point(109, 51);
            this.cmbInterestRateType_1.Name = "cmbInterestRateType_1";
            this.cmbInterestRateType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbInterestRateType_1.Size = new System.Drawing.Size(119, 20);
            this.cmbInterestRateType_1.StyleController = this.lcTranches;
            this.cmbInterestRateType_1.TabIndex = 42;
            // 
            // txtInterestRate_1
            // 
            this.txtInterestRate_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtInterestRate_1.Location = new System.Drawing.Point(306, 51);
            this.txtInterestRate_1.Name = "txtInterestRate_1";
            this.txtInterestRate_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtInterestRate_1.Properties.Mask.EditMask = "P0";
            this.txtInterestRate_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInterestRate_1.Properties.MaxLength = 3;
            this.txtInterestRate_1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtInterestRate_1.Size = new System.Drawing.Size(73, 20);
            this.txtInterestRate_1.StyleController = this.lcTranches;
            this.txtInterestRate_1.TabIndex = 15;
            // 
            // txtInterestCorFactor_1
            // 
            this.txtInterestCorFactor_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtInterestCorFactor_1.Location = new System.Drawing.Point(717, 51);
            this.txtInterestCorFactor_1.Name = "txtInterestCorFactor_1";
            this.txtInterestCorFactor_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtInterestCorFactor_1.Properties.Mask.EditMask = "P0";
            this.txtInterestCorFactor_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInterestCorFactor_1.Properties.MaxLength = 3;
            this.txtInterestCorFactor_1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtInterestCorFactor_1.Size = new System.Drawing.Size(157, 20);
            this.txtInterestCorFactor_1.StyleController = this.lcTranches;
            this.txtInterestCorFactor_1.TabIndex = 16;
            // 
            // lcgRootTranches
            // 
            this.lcgRootTranches.CustomizationFormText = "layoutControlGroup1";
            this.lcgRootTranches.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRootTranches.GroupBordersVisible = false;
            this.lcgRootTranches.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranches});
            this.lcgRootTranches.Location = new System.Drawing.Point(0, 0);
            this.lcgRootTranches.Name = "lcgRootTranches";
            this.lcgRootTranches.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRootTranches.Size = new System.Drawing.Size(882, 427);
            this.lcgRootTranches.Text = "lcgRootTranches";
            this.lcgRootTranches.TextVisible = false;
            // 
            // lcgTranches
            // 
            this.lcgTranches.CustomizationFormText = "lcgTranches";
            this.lcgTranches.GroupBordersVisible = false;
            this.lcgTranches.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranchInfo_1});
            this.lcgTranches.Location = new System.Drawing.Point(0, 0);
            this.lcgTranches.Name = "lcgTranches";
            this.lcgTranches.Size = new System.Drawing.Size(882, 427);
            this.lcgTranches.Text = "lcgTranches";
            this.lcgTranches.TextVisible = false;
            // 
            // lcgTranchInfo_1
            // 
            this.lcgTranchInfo_1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lcgTranchInfo_1.AppearanceGroup.Options.UseFont = true;
            this.lcgTranchInfo_1.CustomizationFormText = "layoutControlGroup1";
            this.lcgTranchInfo_1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgTranchInfo_1.ExpandButtonVisible = true;
            this.lcgTranchInfo_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranchMainInfo_1,
            this.lcgInstallments});
            this.lcgTranchInfo_1.Location = new System.Drawing.Point(0, 0);
            this.lcgTranchInfo_1.Name = "lcgTranchInfo_1";
            this.lcgTranchInfo_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchInfo_1.Size = new System.Drawing.Size(882, 427);
            this.lcgTranchInfo_1.Tag = "1";
            this.lcgTranchInfo_1.Text = "Tranche 1";
            // 
            // lcgTranchMainInfo_1
            // 
            this.lcgTranchMainInfo_1.CustomizationFormText = "lcgTranchMainInfo_1";
            this.lcgTranchMainInfo_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranchMainInfoPart1_1,
            this.lcgTranchMainInfoPart2_1});
            this.lcgTranchMainInfo_1.Location = new System.Drawing.Point(0, 0);
            this.lcgTranchMainInfo_1.Name = "lcgTranchMainInfo_1";
            this.lcgTranchMainInfo_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchMainInfo_1.Size = new System.Drawing.Size(876, 54);
            this.lcgTranchMainInfo_1.Text = "lcgTranchMainInfo_1";
            this.lcgTranchMainInfo_1.TextVisible = false;
            // 
            // lcgTranchMainInfoPart1_1
            // 
            this.lcgTranchMainInfoPart1_1.CustomizationFormText = "lcgTranchMainInfoPart1_1";
            this.lcgTranchMainInfoPart1_1.GroupBordersVisible = false;
            this.lcgTranchMainInfoPart1_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutTranchAmount_1,
            this.layoutBalloonAmount_1,
            this.layoutPrincipalFrequency_1,
            this.layoutInterestFrequency_1,
            this.layoutRepayPeriod_1});
            this.lcgTranchMainInfoPart1_1.Location = new System.Drawing.Point(0, 0);
            this.lcgTranchMainInfoPart1_1.Name = "lcgTranchMainInfoPart1_1";
            this.lcgTranchMainInfoPart1_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchMainInfoPart1_1.Size = new System.Drawing.Size(870, 24);
            this.lcgTranchMainInfoPart1_1.Text = "lcgTranchMainInfoPart1_1";
            this.lcgTranchMainInfoPart1_1.TextVisible = false;
            // 
            // layoutTranchAmount_1
            // 
            this.layoutTranchAmount_1.Control = this.txtTranchAmount_1;
            this.layoutTranchAmount_1.CustomizationFormText = "Tranche Amount:";
            this.layoutTranchAmount_1.Location = new System.Drawing.Point(0, 0);
            this.layoutTranchAmount_1.Name = "layoutTranchAmount_1";
            this.layoutTranchAmount_1.Size = new System.Drawing.Size(190, 24);
            this.layoutTranchAmount_1.Text = "Tranche Amount:";
            this.layoutTranchAmount_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutTranchAmount_1.TextSize = new System.Drawing.Size(83, 13);
            this.layoutTranchAmount_1.TextToControlDistance = 5;
            // 
            // layoutBalloonAmount_1
            // 
            this.layoutBalloonAmount_1.Control = this.txtBalloonAmount_1;
            this.layoutBalloonAmount_1.CustomizationFormText = "Balloon Amount:";
            this.layoutBalloonAmount_1.Location = new System.Drawing.Point(190, 0);
            this.layoutBalloonAmount_1.Name = "layoutBalloonAmount_1";
            this.layoutBalloonAmount_1.Size = new System.Drawing.Size(162, 24);
            this.layoutBalloonAmount_1.Text = "Balloon Amount:";
            this.layoutBalloonAmount_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutBalloonAmount_1.TextSize = new System.Drawing.Size(78, 13);
            this.layoutBalloonAmount_1.TextToControlDistance = 5;
            // 
            // layoutPrincipalFrequency_1
            // 
            this.layoutPrincipalFrequency_1.Control = this.cmbPrincipalFrequency_1;
            this.layoutPrincipalFrequency_1.CustomizationFormText = "Principal Frequency:";
            this.layoutPrincipalFrequency_1.Location = new System.Drawing.Point(352, 0);
            this.layoutPrincipalFrequency_1.Name = "layoutPrincipalFrequency_1";
            this.layoutPrincipalFrequency_1.Size = new System.Drawing.Size(184, 24);
            this.layoutPrincipalFrequency_1.Text = "Principal Frequency:";
            this.layoutPrincipalFrequency_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutPrincipalFrequency_1.TextSize = new System.Drawing.Size(97, 13);
            this.layoutPrincipalFrequency_1.TextToControlDistance = 5;
            // 
            // layoutInterestFrequency_1
            // 
            this.layoutInterestFrequency_1.Control = this.cmbInterestFrequency_1;
            this.layoutInterestFrequency_1.CustomizationFormText = "Interest Frequency:";
            this.layoutInterestFrequency_1.Location = new System.Drawing.Point(536, 0);
            this.layoutInterestFrequency_1.Name = "layoutInterestFrequency_1";
            this.layoutInterestFrequency_1.Size = new System.Drawing.Size(184, 24);
            this.layoutInterestFrequency_1.Text = "Interest Frequency:";
            this.layoutInterestFrequency_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestFrequency_1.TextSize = new System.Drawing.Size(97, 13);
            this.layoutInterestFrequency_1.TextToControlDistance = 5;
            // 
            // layoutRepayPeriod_1
            // 
            this.layoutRepayPeriod_1.Control = this.txtRepayPeriod_1;
            this.layoutRepayPeriod_1.CustomizationFormText = "Repay Period:";
            this.layoutRepayPeriod_1.Location = new System.Drawing.Point(720, 0);
            this.layoutRepayPeriod_1.Name = "layoutRepayPeriod_1";
            this.layoutRepayPeriod_1.Size = new System.Drawing.Size(150, 24);
            this.layoutRepayPeriod_1.Text = "Repay Period:";
            this.layoutRepayPeriod_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutRepayPeriod_1.TextSize = new System.Drawing.Size(68, 13);
            this.layoutRepayPeriod_1.TextToControlDistance = 5;
            // 
            // lcgTranchMainInfoPart2_1
            // 
            this.lcgTranchMainInfoPart2_1.CustomizationFormText = "lcgTranchMainInfoPart2_1";
            this.lcgTranchMainInfoPart2_1.GroupBordersVisible = false;
            this.lcgTranchMainInfoPart2_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutInterestRateType_1,
            this.layoutInterestRate_1,
            this.layoutInterestCorFactor_1,
            this.layoutPeriodType_1});
            this.lcgTranchMainInfoPart2_1.Location = new System.Drawing.Point(0, 24);
            this.lcgTranchMainInfoPart2_1.Name = "lcgTranchMainInfoPart2_1";
            this.lcgTranchMainInfoPart2_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchMainInfoPart2_1.Size = new System.Drawing.Size(870, 24);
            this.lcgTranchMainInfoPart2_1.Text = "lcgTranchMainInfoPart2_1";
            this.lcgTranchMainInfoPart2_1.TextVisible = false;
            // 
            // layoutInterestRateType_1
            // 
            this.layoutInterestRateType_1.Control = this.cmbInterestRateType_1;
            this.layoutInterestRateType_1.CustomizationFormText = "Interest Rate Type:";
            this.layoutInterestRateType_1.Location = new System.Drawing.Point(0, 0);
            this.layoutInterestRateType_1.Name = "layoutInterestRateType_1";
            this.layoutInterestRateType_1.Size = new System.Drawing.Size(224, 24);
            this.layoutInterestRateType_1.Text = "Interest Rate Type:";
            this.layoutInterestRateType_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestRateType_1.TextSize = new System.Drawing.Size(96, 13);
            this.layoutInterestRateType_1.TextToControlDistance = 5;
            // 
            // layoutInterestRate_1
            // 
            this.layoutInterestRate_1.Control = this.txtInterestRate_1;
            this.layoutInterestRate_1.CustomizationFormText = "Interest Rate:";
            this.layoutInterestRate_1.Location = new System.Drawing.Point(224, 0);
            this.layoutInterestRate_1.Name = "layoutInterestRate_1";
            this.layoutInterestRate_1.Size = new System.Drawing.Size(151, 24);
            this.layoutInterestRate_1.Text = "Interest Rate:";
            this.layoutInterestRate_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestRate_1.TextSize = new System.Drawing.Size(69, 13);
            this.layoutInterestRate_1.TextToControlDistance = 5;
            // 
            // layoutInterestCorFactor_1
            // 
            this.layoutInterestCorFactor_1.Control = this.txtInterestCorFactor_1;
            this.layoutInterestCorFactor_1.CustomizationFormText = "Correction Factor:";
            this.layoutInterestCorFactor_1.Location = new System.Drawing.Point(616, 0);
            this.layoutInterestCorFactor_1.Name = "layoutInterestCorFactor_1";
            this.layoutInterestCorFactor_1.Size = new System.Drawing.Size(254, 24);
            this.layoutInterestCorFactor_1.Text = "Correction Factor:";
            this.layoutInterestCorFactor_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestCorFactor_1.TextSize = new System.Drawing.Size(88, 13);
            this.layoutInterestCorFactor_1.TextToControlDistance = 5;
            // 
            // layoutPeriodType_1
            // 
            this.layoutPeriodType_1.Control = this.cmbPeriodType_1;
            this.layoutPeriodType_1.CustomizationFormText = "Period Type:";
            this.layoutPeriodType_1.Location = new System.Drawing.Point(375, 0);
            this.layoutPeriodType_1.Name = "layoutPeriodType_1";
            this.layoutPeriodType_1.Size = new System.Drawing.Size(241, 24);
            this.layoutPeriodType_1.Text = "Period Type:";
            this.layoutPeriodType_1.TextSize = new System.Drawing.Size(61, 13);
            // 
            // lcgInstallments
            // 
            this.lcgInstallments.CustomizationFormText = "Installments";
            this.lcgInstallments.Location = new System.Drawing.Point(0, 54);
            this.lcgInstallments.Name = "lcgInstallments";
            this.lcgInstallments.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgInstallments.Size = new System.Drawing.Size(876, 348);
            this.lcgInstallments.Text = "Installments";
            // 
            // chkRetentionAccount
            // 
            this.chkRetentionAccount.Location = new System.Drawing.Point(796, 81);
            this.chkRetentionAccount.Name = "chkRetentionAccount";
            this.chkRetentionAccount.Properties.Caption = global::Exis.WinClient.Strings.Eve;
            this.chkRetentionAccount.Size = new System.Drawing.Size(80, 19);
            this.chkRetentionAccount.StyleController = this.layoutRootControl;
            this.chkRetentionAccount.TabIndex = 37;
            // 
            // txtPrepayFee
            // 
            this.txtPrepayFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPrepayFee.Location = new System.Drawing.Point(632, 81);
            this.txtPrepayFee.Name = "txtPrepayFee";
            this.txtPrepayFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPrepayFee.Properties.Mask.EditMask = "P0";
            this.txtPrepayFee.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPrepayFee.Properties.MaxLength = 3;
            this.txtPrepayFee.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtPrepayFee.Size = new System.Drawing.Size(64, 20);
            this.txtPrepayFee.StyleController = this.layoutRootControl;
            this.txtPrepayFee.TabIndex = 14;
            // 
            // txtArrangeFee
            // 
            this.txtArrangeFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtArrangeFee.Location = new System.Drawing.Point(477, 81);
            this.txtArrangeFee.Name = "txtArrangeFee";
            this.txtArrangeFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtArrangeFee.Properties.Mask.EditMask = "D";
            this.txtArrangeFee.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtArrangeFee.Properties.MaxLength = 9;
            this.txtArrangeFee.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtArrangeFee.Size = new System.Drawing.Size(65, 20);
            this.txtArrangeFee.StyleController = this.layoutRootControl;
            this.txtArrangeFee.TabIndex = 36;
            // 
            // txtAdminFee
            // 
            this.txtAdminFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAdminFee.Location = new System.Drawing.Point(316, 81);
            this.txtAdminFee.Name = "txtAdminFee";
            this.txtAdminFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAdminFee.Properties.Mask.EditMask = "D";
            this.txtAdminFee.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAdminFee.Properties.MaxLength = 9;
            this.txtAdminFee.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtAdminFee.Size = new System.Drawing.Size(66, 20);
            this.txtAdminFee.StyleController = this.layoutRootControl;
            this.txtAdminFee.TabIndex = 35;
            // 
            // txtVMC
            // 
            this.txtVMC.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtVMC.Location = new System.Drawing.Point(157, 81);
            this.txtVMC.Name = "txtVMC";
            this.txtVMC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVMC.Properties.Mask.EditMask = "P0";
            this.txtVMC.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtVMC.Properties.MaxLength = 3;
            this.txtVMC.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtVMC.Size = new System.Drawing.Size(59, 20);
            this.txtVMC.StyleController = this.layoutRootControl;
            this.txtVMC.TabIndex = 13;
            // 
            // txtAmount
            // 
            this.txtAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAmount.Location = new System.Drawing.Point(58, 81);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAmount.Properties.Mask.EditMask = "D";
            this.txtAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAmount.Properties.MaxLength = 12;
            this.txtAmount.Properties.MaxValue = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.txtAmount.Size = new System.Drawing.Size(67, 20);
            this.txtAmount.StyleController = this.layoutRootControl;
            this.txtAmount.TabIndex = 34;
            // 
            // lookupCurrency
            // 
            this.lookupCurrency.Location = new System.Drawing.Point(765, 57);
            this.lookupCurrency.Name = "lookupCurrency";
            this.lookupCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupCurrency.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupCurrency.Size = new System.Drawing.Size(111, 20);
            this.lookupCurrency.StyleController = this.layoutRootControl;
            this.lookupCurrency.TabIndex = 23;
            // 
            // dtpSignDate
            // 
            this.dtpSignDate.EditValue = null;
            this.dtpSignDate.Location = new System.Drawing.Point(631, 33);
            this.dtpSignDate.Name = "dtpSignDate";
            this.dtpSignDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpSignDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpSignDate.Size = new System.Drawing.Size(245, 20);
            this.dtpSignDate.StyleController = this.layoutRootControl;
            this.dtpSignDate.TabIndex = 33;
            // 
            // lookupGuarantor
            // 
            this.lookupGuarantor.Location = new System.Drawing.Point(306, 57);
            this.lookupGuarantor.Name = "lookupGuarantor";
            this.lookupGuarantor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupGuarantor.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupGuarantor.Size = new System.Drawing.Size(178, 20);
            this.lookupGuarantor.StyleController = this.layoutRootControl;
            this.lookupGuarantor.TabIndex = 32;
            // 
            // lookupObligor
            // 
            this.lookupObligor.Location = new System.Drawing.Point(55, 57);
            this.lookupObligor.Name = "lookupObligor";
            this.lookupObligor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupObligor.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupObligor.Size = new System.Drawing.Size(191, 20);
            this.lookupObligor.StyleController = this.layoutRootControl;
            this.lookupObligor.TabIndex = 31;
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(31, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(81, 20);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(473, 33);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(101, 20);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(148, 33);
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.MaxLength = 1000;
            this.txtCode.Size = new System.Drawing.Size(283, 20);
            this.txtCode.StyleController = this.layoutRootControl;
            this.txtCode.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemPanelTranches,
            this.layoutGroupLoan});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(890, 566);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutItemPanelTranches
            // 
            this.layoutItemPanelTranches.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutItemPanelTranches.AppearanceItemCaption.Options.UseFont = true;
            this.layoutItemPanelTranches.Control = this.panelTranches;
            this.layoutItemPanelTranches.CustomizationFormText = "layoutItemPanelTranches";
            this.layoutItemPanelTranches.Location = new System.Drawing.Point(0, 115);
            this.layoutItemPanelTranches.Name = "layoutItemPanelTranches";
            this.layoutItemPanelTranches.Size = new System.Drawing.Size(890, 451);
            this.layoutItemPanelTranches.Text = "Tranches:";
            this.layoutItemPanelTranches.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutItemPanelTranches.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutGroupLoan
            // 
            this.layoutGroupLoan.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutGroupLoan.AppearanceGroup.Options.UseFont = true;
            this.layoutGroupLoan.CustomizationFormText = "Loan";
            this.layoutGroupLoan.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemCode,
            this.layoutId,
            this.layoutStatus,
            this.layoutItemGuarantor,
            this.layoutItemObligor,
            this.layoutItemSignDate,
            this.layoutItemAmount,
            this.layoutItemVMC,
            this.layoutItemAdminFee,
            this.layoutItemArrangeFee,
            this.layoutItemPrepayFee,
            this.layoutItemRetentionAccount,
            this.layoutCurrency,
            this.layoutBorrower});
            this.layoutGroupLoan.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupLoan.Name = "layoutGroupLoan";
            this.layoutGroupLoan.Size = new System.Drawing.Size(890, 115);
            this.layoutGroupLoan.Text = "Loan";
            // 
            // layoutItemCode
            // 
            this.layoutItemCode.Control = this.txtCode;
            this.layoutItemCode.CustomizationFormText = "Code:";
            this.layoutItemCode.Location = new System.Drawing.Point(102, 0);
            this.layoutItemCode.Name = "layoutItemCode";
            this.layoutItemCode.Size = new System.Drawing.Size(319, 24);
            this.layoutItemCode.Text = "Code:";
            this.layoutItemCode.TextSize = new System.Drawing.Size(29, 13);
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MaxSize = new System.Drawing.Size(102, 24);
            this.layoutId.MinSize = new System.Drawing.Size(102, 24);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(102, 24);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(14, 13);
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(421, 0);
            this.layoutStatus.MaxSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.MinSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(143, 24);
            this.layoutStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutItemGuarantor
            // 
            this.layoutItemGuarantor.Control = this.lookupGuarantor;
            this.layoutItemGuarantor.CustomizationFormText = "Guarantor:";
            this.layoutItemGuarantor.Location = new System.Drawing.Point(236, 24);
            this.layoutItemGuarantor.Name = "layoutItemGuarantor";
            this.layoutItemGuarantor.Size = new System.Drawing.Size(238, 24);
            this.layoutItemGuarantor.Text = "Guarantor:";
            this.layoutItemGuarantor.TextSize = new System.Drawing.Size(53, 13);
            // 
            // layoutItemObligor
            // 
            this.layoutItemObligor.Control = this.lookupObligor;
            this.layoutItemObligor.CustomizationFormText = "Obligor:";
            this.layoutItemObligor.Location = new System.Drawing.Point(0, 24);
            this.layoutItemObligor.Name = "layoutItemObligor";
            this.layoutItemObligor.Size = new System.Drawing.Size(236, 24);
            this.layoutItemObligor.Text = "Obligor:";
            this.layoutItemObligor.TextSize = new System.Drawing.Size(38, 13);
            // 
            // layoutItemSignDate
            // 
            this.layoutItemSignDate.Control = this.dtpSignDate;
            this.layoutItemSignDate.CustomizationFormText = "Sign Date:";
            this.layoutItemSignDate.Location = new System.Drawing.Point(564, 0);
            this.layoutItemSignDate.Name = "layoutItemSignDate";
            this.layoutItemSignDate.Size = new System.Drawing.Size(302, 24);
            this.layoutItemSignDate.Text = "Sign Date:";
            this.layoutItemSignDate.TextSize = new System.Drawing.Size(50, 13);
            // 
            // layoutItemAmount
            // 
            this.layoutItemAmount.Control = this.txtAmount;
            this.layoutItemAmount.CustomizationFormText = "Amount:";
            this.layoutItemAmount.Location = new System.Drawing.Point(0, 48);
            this.layoutItemAmount.Name = "layoutItemAmount";
            this.layoutItemAmount.Size = new System.Drawing.Size(115, 24);
            this.layoutItemAmount.Text = "Amount:";
            this.layoutItemAmount.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutItemVMC
            // 
            this.layoutItemVMC.Control = this.txtVMC;
            this.layoutItemVMC.CustomizationFormText = "VMC:";
            this.layoutItemVMC.Location = new System.Drawing.Point(115, 48);
            this.layoutItemVMC.Name = "layoutItemVMC";
            this.layoutItemVMC.Size = new System.Drawing.Size(91, 24);
            this.layoutItemVMC.Text = "VMC:";
            this.layoutItemVMC.TextSize = new System.Drawing.Size(25, 13);
            // 
            // layoutItemAdminFee
            // 
            this.layoutItemAdminFee.Control = this.txtAdminFee;
            this.layoutItemAdminFee.CustomizationFormText = "Administration Fee:";
            this.layoutItemAdminFee.Location = new System.Drawing.Point(206, 48);
            this.layoutItemAdminFee.Name = "layoutItemAdminFee";
            this.layoutItemAdminFee.Size = new System.Drawing.Size(166, 24);
            this.layoutItemAdminFee.Text = "Administration Fee:";
            this.layoutItemAdminFee.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutItemArrangeFee
            // 
            this.layoutItemArrangeFee.Control = this.txtArrangeFee;
            this.layoutItemArrangeFee.CustomizationFormText = "Arrangement Fee:";
            this.layoutItemArrangeFee.Location = new System.Drawing.Point(372, 48);
            this.layoutItemArrangeFee.Name = "layoutItemArrangeFee";
            this.layoutItemArrangeFee.Size = new System.Drawing.Size(160, 24);
            this.layoutItemArrangeFee.Text = "Arrangement Fee:";
            this.layoutItemArrangeFee.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutItemPrepayFee
            // 
            this.layoutItemPrepayFee.Control = this.txtPrepayFee;
            this.layoutItemPrepayFee.CustomizationFormText = "Prepayment Fee:";
            this.layoutItemPrepayFee.Location = new System.Drawing.Point(532, 48);
            this.layoutItemPrepayFee.Name = "layoutItemPrepayFee";
            this.layoutItemPrepayFee.Size = new System.Drawing.Size(154, 24);
            this.layoutItemPrepayFee.Text = "Prepayment Fee:";
            this.layoutItemPrepayFee.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutItemRetentionAccount
            // 
            this.layoutItemRetentionAccount.Control = this.chkRetentionAccount;
            this.layoutItemRetentionAccount.CustomizationFormText = "Retention Account:";
            this.layoutItemRetentionAccount.Location = new System.Drawing.Point(686, 48);
            this.layoutItemRetentionAccount.Name = "layoutItemRetentionAccount";
            this.layoutItemRetentionAccount.Size = new System.Drawing.Size(180, 24);
            this.layoutItemRetentionAccount.Text = "Retention Account:";
            this.layoutItemRetentionAccount.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutCurrency
            // 
            this.layoutCurrency.Control = this.lookupCurrency;
            this.layoutCurrency.CustomizationFormText = "Currency:";
            this.layoutCurrency.Location = new System.Drawing.Point(700, 24);
            this.layoutCurrency.Name = "layoutCurrency";
            this.layoutCurrency.Size = new System.Drawing.Size(166, 24);
            this.layoutCurrency.Text = "Currency:";
            this.layoutCurrency.TextSize = new System.Drawing.Size(48, 13);
            // 
            // layoutBorrower
            // 
            this.layoutBorrower.Control = this.cmbBorrower;
            this.layoutBorrower.CustomizationFormText = "Borrower:";
            this.layoutBorrower.Location = new System.Drawing.Point(474, 24);
            this.layoutBorrower.Name = "layoutBorrower";
            this.layoutBorrower.Size = new System.Drawing.Size(226, 24);
            this.layoutBorrower.Text = "Borrower:";
            this.layoutBorrower.TextSize = new System.Drawing.Size(48, 13);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList16.Images.SetKeyName(1, "clear16x16.ico");
            this.imageList16.Images.SetKeyName(2, "differ16x16.ico");
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTranchAmount_1;
            this.layoutControlItem2.CustomizationFormText = "Tranche Amount:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutTranchAmount_1";
            this.layoutControlItem2.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItem2.Text = "Tranche Amount:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(83, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(890, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 566);
            this.barDockControlBottom.Size = new System.Drawing.Size(890, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 566);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(890, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 566);
            // 
            // InstallmentAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 598);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "InstallmentAEVForm";
            this.Activated += new System.EventHandler(this.InstallmentAevFormActivated);
            this.Deactivate += new System.EventHandler(this.InstallmentAevFormDeactivate);
            this.Load += new System.EventHandler(this.LoanAevFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbBorrower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTranches)).EndInit();
            this.panelTranches.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcTranches)).EndInit();
            this.lcTranches.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalloonAmount_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPrincipalFrequency_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepayPeriod_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestFrequency_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTranchAmount_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestRateType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestRate_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestCorFactor_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootTranches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchInfo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTranchAmount_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBalloonAmount_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrincipalFrequency_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestFrequency_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRepayPeriod_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRateType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestCorFactor_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInstallments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetentionAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepayFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArrangeFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVMC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupGuarantor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupObligor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelTranches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupLoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGuarantor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemObligor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSignDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemVMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAdminFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemArrangeFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPrepayFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRetentionAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBorrower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupLoan;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.DateEdit dtpSignDate;
        private DevExpress.XtraEditors.LookUpEdit lookupGuarantor;
        private DevExpress.XtraEditors.LookUpEdit lookupObligor;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemGuarantor;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemObligor;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemSignDate;
        private DevExpress.XtraEditors.LookUpEdit lookupCurrency;
        private DevExpress.XtraLayout.LayoutControlItem layoutCurrency;
        private DevExpress.XtraEditors.SpinEdit txtAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAmount;
        private DevExpress.XtraEditors.SpinEdit txtVMC;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemVMC;
        private DevExpress.XtraEditors.CheckEdit chkRetentionAccount;
        private DevExpress.XtraEditors.SpinEdit txtPrepayFee;
        private DevExpress.XtraEditors.SpinEdit txtArrangeFee;
        private DevExpress.XtraEditors.SpinEdit txtAdminFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAdminFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemArrangeFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPrepayFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemRetentionAccount;
        private DevExpress.XtraEditors.SpinEdit txtInterestCorFactor_1;
        private DevExpress.XtraEditors.SpinEdit txtInterestRate_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbInterestRateType_1;
        private DevExpress.XtraEditors.SpinEdit txtRepayPeriod_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbInterestFrequency_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPrincipalFrequency_1;
        private DevExpress.XtraEditors.SpinEdit txtBalloonAmount_1;
        private DevExpress.XtraEditors.SpinEdit txtTranchAmount_1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PanelControl panelTranches;
        private DevExpress.XtraLayout.LayoutControl lcTranches;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRootTranches;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPanelTranches;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchInfo_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutTranchAmount_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBalloonAmount_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPrincipalFrequency_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRepayPeriod_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestFrequency_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestRateType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestRate_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestCorFactor_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranches;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfo_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfoPart1_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfoPart2_1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbBorrower;
        private DevExpress.XtraLayout.LayoutControlItem layoutBorrower;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraLayout.LayoutControlGroup lcgInstallments;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodType_1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}