﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraLayout.Utils;

namespace Exis.WinClient.Controls
{
    public partial class CashFlowItemAEVForm : DevExpress.XtraEditors.XtraForm
    {

        #region Public Members

        public CashFlowItem _cashFlowItem;
        public readonly FormActionTypeEnum _formActionType;

        #endregion

        #region Private Members

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public CashFlowItemAEVForm()
        {
            InitializeComponent();
            InitializeControl();
        }

        public CashFlowItemAEVForm(CashFlowItem cashFlowItem, FormActionTypeEnum formActionType)
        {
            InitializeComponent();

            _formActionType = formActionType;
            _cashFlowItem = cashFlowItem;

            InitializeControl();
            LoadData();
        }

        #endregion

        #region Private Methods

        private void InitializeControl()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            lookUpCurrency.Properties.DisplayMember = "Name";
            lookUpCurrency.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookUpCurrency.Properties.Columns.Add(col);
            lookUpCurrency.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpCurrency.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpCurrency.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            cmbItemStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbItemStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            if (_formActionType == FormActionTypeEnum.View)
            {
                //layoutGroupActions.Visibility = LayoutVisibility.Never;
                btnSave.Visibility = BarItemVisibility.Never;

                txtItemName.Properties.ReadOnly = true;
                cmbItemStatus.Properties.ReadOnly = true;
                lookUpCurrency.Properties.ReadOnly = true;
                dtpDueDate.Properties.ReadOnly = true;
                txtAmount.Properties.ReadOnly = true;
            }
        }

        private void LoadData()
        {
            txtItemName.EditValue = _cashFlowItem.Name;
            txtAmount.EditValue = _cashFlowItem.CustomAmount;
            dtpDueDate.EditValue = _cashFlowItem.CustomDueDate;
            lookUpCurrency.EditValue = _cashFlowItem.CustomCurrencyId;
            cmbItemStatus.SelectedItem = _cashFlowItem.Status;

            txtCreationUser.EditValue = _cashFlowItem.Cruser;
            dtpCreationDate.EditValue = _cashFlowItem.Crd;
            txtUpdateUser.EditValue = _cashFlowItem.Chd;
            dtpUpdateDate.EditValue = _cashFlowItem.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtItemName.Text.Trim()))
                errorProvider.SetError(txtItemName, Strings.Field_is_mandatory);
            if (cmbItemStatus.SelectedItem == null) errorProvider.SetError(cmbItemStatus, Strings.Field_is_mandatory);
            if (txtAmount.EditValue == null) errorProvider.SetError(txtAmount, Strings.Field_is_mandatory);
            if (lookUpCurrency.EditValue == null) errorProvider.SetError(lookUpCurrency, Strings.Field_is_mandatory);
            if (dtpDueDate.EditValue == null) errorProvider.SetError(dtpDueDate, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Event Handlers

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _cashFlowItem = new CashFlowItem();
            }

            _cashFlowItem.Status = (ActivationStatusEnum)cmbItemStatus.SelectedItem;
            _cashFlowItem.Name = txtItemName.Text.Trim();
            _cashFlowItem.CustomAmount = Convert.ToDecimal(txtAmount.EditValue);
            _cashFlowItem.CustomCurrencyId = Convert.ToInt64(lookUpCurrency.EditValue);
            _cashFlowItem.CustomDueDate = (DateTime)dtpDueDate.EditValue;
            _cashFlowItem.IsCustom = true;
            _cashFlowItem.SystemType = null;

            BeginAEVCashFlowItem(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void CashFlowItemAevFormLoad(object sender, EventArgs e)
        {
            BeginAEVCashFlowItemInitializationData();
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVCashFlowItemInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVCashFlowItemInitializationData(EndAEVCashFlowItemInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                Strings.
                                    There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                Strings.Freight_Metrics,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVCashFlowItemInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVCashFlowItemInitializationData;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Currency> currencies;

            try
            {
                result = SessionRegistry.Client.EndAEVCashFlowItemInitializationData(out currencies, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                Strings.
                                    There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                Strings.Freight_Metrics,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookUpCurrency.Tag = currencies;
                lookUpCurrency.Properties.DataSource = currencies;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View) LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVCashFlowItem(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _cashFlowItem, EndAEVCashFlowItem, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVCashFlowItem(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVCashFlowItem;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtItemName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                if (AddEditViewCashFlowItemClosedEvent != null)
                    AddEditViewCashFlowItemClosedEvent(null);

                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Events

        public event Action<long?> AddEditViewCashFlowItemClosedEvent;

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
        
    }
}