﻿namespace Exis.WinClient.Controls.Core
{
    partial class AddEditViewTradeFFAInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.txtClearingFees = new DevExpress.XtraEditors.SpinEdit();
            this.cmbPurpose = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbSettlementType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtPrice = new DevExpress.XtraEditors.SpinEdit();
            this.txtQuantityDays = new DevExpress.XtraEditors.SpinEdit();
            this.cmbQuantityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPeriodDayCountType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPeriod = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cmbPeriodType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookupClearingHouse = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupClearingAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupAllocationPool = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupAllocationVessel = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbAllocationType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookupClearingBank = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupUnderlyingIndex = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAllocationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAllocationPool = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingBank = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUnderlyingIndex = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAllocationVessel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingHouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodDayCountType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutQuantityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutQuantityDays = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSettlementType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPurpose = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingFees = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtClosedOutQuantityTotal = new DevExpress.XtraEditors.SpinEdit();
            this.layoutClosedOutQuantityTotal = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClearingFees.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPurpose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSettlementType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuantityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodDayCountType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingHouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationPool.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationVessel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAllocationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupUnderlyingIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationPool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnderlyingIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationVessel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodDayCountType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettlementType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedOutQuantityTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClosedOutQuantityTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.Controls.Add(this.txtClosedOutQuantityTotal);
            this.layoutRoot.Controls.Add(this.txtClearingFees);
            this.layoutRoot.Controls.Add(this.cmbPurpose);
            this.layoutRoot.Controls.Add(this.cmbSettlementType);
            this.layoutRoot.Controls.Add(this.txtPrice);
            this.layoutRoot.Controls.Add(this.txtQuantityDays);
            this.layoutRoot.Controls.Add(this.cmbQuantityType);
            this.layoutRoot.Controls.Add(this.cmbPeriodDayCountType);
            this.layoutRoot.Controls.Add(this.cmbPeriod);
            this.layoutRoot.Controls.Add(this.cmbPeriodType);
            this.layoutRoot.Controls.Add(this.lookupClearingHouse);
            this.layoutRoot.Controls.Add(this.lookupClearingAccount);
            this.layoutRoot.Controls.Add(this.lookupAllocationPool);
            this.layoutRoot.Controls.Add(this.lookupAllocationVessel);
            this.layoutRoot.Controls.Add(this.cmbAllocationType);
            this.layoutRoot.Controls.Add(this.lookupClearingBank);
            this.layoutRoot.Controls.Add(this.lookupUnderlyingIndex);
            this.layoutRoot.Controls.Add(this.cmbType);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(537, 394, 250, 350);
            this.layoutRoot.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRoot.Root = this.layoutRootGroup;
            this.layoutRoot.Size = new System.Drawing.Size(1240, 346);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // txtClearingFees
            // 
            this.txtClearingFees.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtClearingFees.Location = new System.Drawing.Point(1099, 54);
            this.txtClearingFees.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtClearingFees.Name = "txtClearingFees";
            this.txtClearingFees.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtClearingFees.Properties.Mask.EditMask = "D";
            this.txtClearingFees.Properties.MaxLength = 6;
            this.txtClearingFees.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtClearingFees.Size = new System.Drawing.Size(139, 22);
            this.txtClearingFees.StyleController = this.layoutRoot;
            this.txtClearingFees.TabIndex = 13;
            // 
            // cmbPurpose
            // 
            this.cmbPurpose.Location = new System.Drawing.Point(1088, 2);
            this.cmbPurpose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPurpose.Name = "cmbPurpose";
            this.cmbPurpose.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPurpose.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPurpose.Size = new System.Drawing.Size(150, 22);
            this.cmbPurpose.StyleController = this.layoutRoot;
            this.cmbPurpose.TabIndex = 20;
            // 
            // cmbSettlementType
            // 
            this.cmbSettlementType.Location = new System.Drawing.Point(928, 2);
            this.cmbSettlementType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbSettlementType.Name = "cmbSettlementType";
            this.cmbSettlementType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSettlementType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbSettlementType.Size = new System.Drawing.Size(102, 22);
            this.cmbSettlementType.StyleController = this.layoutRoot;
            this.cmbSettlementType.TabIndex = 19;
            // 
            // txtPrice
            // 
            this.txtPrice.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtPrice.Location = new System.Drawing.Point(1163, 80);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPrice.Properties.Mask.EditMask = "f2";
            this.txtPrice.Properties.MaxLength = 11;
            this.txtPrice.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.txtPrice.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtPrice.Size = new System.Drawing.Size(75, 22);
            this.txtPrice.StyleController = this.layoutRoot;
            this.txtPrice.TabIndex = 11;
            // 
            // txtQuantityDays
            // 
            this.txtQuantityDays.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQuantityDays.Location = new System.Drawing.Point(706, 80);
            this.txtQuantityDays.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtQuantityDays.Name = "txtQuantityDays";
            this.txtQuantityDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtQuantityDays.Properties.Mask.EditMask = "d";
            this.txtQuantityDays.Properties.MaxLength = 5;
            this.txtQuantityDays.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtQuantityDays.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQuantityDays.Size = new System.Drawing.Size(39, 22);
            this.txtQuantityDays.StyleController = this.layoutRoot;
            this.txtQuantityDays.TabIndex = 8;
            // 
            // cmbQuantityType
            // 
            this.cmbQuantityType.Location = new System.Drawing.Point(836, 80);
            this.cmbQuantityType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbQuantityType.Name = "cmbQuantityType";
            this.cmbQuantityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQuantityType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbQuantityType.Size = new System.Drawing.Size(90, 22);
            this.cmbQuantityType.StyleController = this.layoutRoot;
            this.cmbQuantityType.TabIndex = 9;
            // 
            // cmbPeriodDayCountType
            // 
            this.cmbPeriodDayCountType.Location = new System.Drawing.Point(526, 80);
            this.cmbPeriodDayCountType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPeriodDayCountType.Name = "cmbPeriodDayCountType";
            this.cmbPeriodDayCountType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodDayCountType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriodDayCountType.Size = new System.Drawing.Size(81, 22);
            this.cmbPeriodDayCountType.StyleController = this.layoutRoot;
            this.cmbPeriodDayCountType.TabIndex = 17;
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Location = new System.Drawing.Point(215, 80);
            this.cmbPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriod.Properties.PopupFormMinSize = new System.Drawing.Size(200, 100);
            this.cmbPeriod.Properties.PopupFormSize = new System.Drawing.Size(200, 200);
            this.cmbPeriod.Size = new System.Drawing.Size(169, 22);
            this.cmbPeriod.StyleController = this.layoutRoot;
            this.cmbPeriod.TabIndex = 16;
            // 
            // cmbPeriodType
            // 
            this.cmbPeriodType.Location = new System.Drawing.Point(78, 80);
            this.cmbPeriodType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPeriodType.Name = "cmbPeriodType";
            this.cmbPeriodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriodType.Size = new System.Drawing.Size(89, 22);
            this.cmbPeriodType.StyleController = this.layoutRoot;
            this.cmbPeriodType.TabIndex = 15;
            this.cmbPeriodType.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodType_SelectedIndexChanged);
            // 
            // lookupClearingHouse
            // 
            this.lookupClearingHouse.Location = new System.Drawing.Point(726, 54);
            this.lookupClearingHouse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupClearingHouse.Name = "lookupClearingHouse";
            this.lookupClearingHouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupClearingHouse.Size = new System.Drawing.Size(283, 22);
            this.lookupClearingHouse.StyleController = this.layoutRoot;
            this.lookupClearingHouse.TabIndex = 14;
            this.lookupClearingHouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // lookupClearingAccount
            // 
            this.lookupClearingAccount.Location = new System.Drawing.Point(403, 54);
            this.lookupClearingAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupClearingAccount.Name = "lookupClearingAccount";
            this.lookupClearingAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupClearingAccount.Size = new System.Drawing.Size(225, 22);
            this.lookupClearingAccount.StyleController = this.layoutRoot;
            this.lookupClearingAccount.TabIndex = 13;
            this.lookupClearingAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // lookupAllocationPool
            // 
            this.lookupAllocationPool.Location = new System.Drawing.Point(764, 28);
            this.lookupAllocationPool.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupAllocationPool.Name = "lookupAllocationPool";
            this.lookupAllocationPool.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupAllocationPool.Size = new System.Drawing.Size(474, 22);
            this.lookupAllocationPool.StyleController = this.layoutRoot;
            this.lookupAllocationPool.TabIndex = 12;
            // 
            // lookupAllocationVessel
            // 
            this.lookupAllocationVessel.Location = new System.Drawing.Point(271, 28);
            this.lookupAllocationVessel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupAllocationVessel.Name = "lookupAllocationVessel";
            this.lookupAllocationVessel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupAllocationVessel.Size = new System.Drawing.Size(398, 22);
            this.lookupAllocationVessel.StyleController = this.layoutRoot;
            this.lookupAllocationVessel.TabIndex = 11;
            // 
            // cmbAllocationType
            // 
            this.cmbAllocationType.Location = new System.Drawing.Point(97, 28);
            this.cmbAllocationType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbAllocationType.Name = "cmbAllocationType";
            this.cmbAllocationType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbAllocationType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbAllocationType.Size = new System.Drawing.Size(66, 22);
            this.cmbAllocationType.StyleController = this.layoutRoot;
            this.cmbAllocationType.TabIndex = 10;
            this.cmbAllocationType.SelectedIndexChanged += new System.EventHandler(this.cmbAllocationType_SelectedIndexChanged);
            // 
            // lookupClearingBank
            // 
            this.lookupClearingBank.Location = new System.Drawing.Point(88, 54);
            this.lookupClearingBank.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupClearingBank.Name = "lookupClearingBank";
            this.lookupClearingBank.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupClearingBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupClearingBank.Size = new System.Drawing.Size(207, 22);
            this.lookupClearingBank.StyleController = this.layoutRoot;
            this.lookupClearingBank.TabIndex = 7;
            this.lookupClearingBank.EditValueChanged += new System.EventHandler(this.lookupClearingBank_EditValueChanged);
            this.lookupClearingBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // lookupUnderlyingIndex
            // 
            this.lookupUnderlyingIndex.Location = new System.Drawing.Point(211, 2);
            this.lookupUnderlyingIndex.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupUnderlyingIndex.Name = "lookupUnderlyingIndex";
            this.lookupUnderlyingIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupUnderlyingIndex.Size = new System.Drawing.Size(611, 22);
            this.lookupUnderlyingIndex.StyleController = this.layoutRoot;
            this.lookupUnderlyingIndex.TabIndex = 6;
            this.lookupUnderlyingIndex.EditValueChanged += new System.EventHandler(this.lookupUnderlyingIndex_EditValueChanged);
            // 
            // cmbType
            // 
            this.cmbType.Location = new System.Drawing.Point(38, 2);
            this.cmbType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbType.Name = "cmbType";
            this.cmbType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbType.Size = new System.Drawing.Size(66, 22);
            this.cmbType.StyleController = this.layoutRoot;
            this.cmbType.TabIndex = 5;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutControlGroup1";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutType,
            this.layoutAllocationType,
            this.layoutAllocationPool,
            this.layoutClearingBank,
            this.layoutUnderlyingIndex,
            this.layoutAllocationVessel,
            this.layoutClearingAccount,
            this.layoutClearingHouse,
            this.layoutPeriodType,
            this.layoutPeriod,
            this.layoutPeriodDayCountType,
            this.layoutQuantityType,
            this.layoutQuantityDays,
            this.layoutPrice,
            this.layoutSettlementType,
            this.layoutPurpose,
            this.layoutClearingFees,
            this.layoutClosedOutQuantityTotal});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(1240, 346);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutType
            // 
            this.layoutType.Control = this.cmbType;
            this.layoutType.CustomizationFormText = "Type:";
            this.layoutType.Location = new System.Drawing.Point(0, 0);
            this.layoutType.MaxSize = new System.Drawing.Size(106, 24);
            this.layoutType.MinSize = new System.Drawing.Size(106, 24);
            this.layoutType.Name = "layoutType";
            this.layoutType.Size = new System.Drawing.Size(106, 26);
            this.layoutType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutType.Text = "Type:";
            this.layoutType.TextSize = new System.Drawing.Size(33, 16);
            // 
            // layoutAllocationType
            // 
            this.layoutAllocationType.Control = this.cmbAllocationType;
            this.layoutAllocationType.CustomizationFormText = "Allocation Type:";
            this.layoutAllocationType.Location = new System.Drawing.Point(0, 26);
            this.layoutAllocationType.MaxSize = new System.Drawing.Size(165, 24);
            this.layoutAllocationType.MinSize = new System.Drawing.Size(165, 24);
            this.layoutAllocationType.Name = "layoutAllocationType";
            this.layoutAllocationType.Size = new System.Drawing.Size(165, 26);
            this.layoutAllocationType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAllocationType.Text = "Allocation Type:";
            this.layoutAllocationType.TextSize = new System.Drawing.Size(92, 16);
            // 
            // layoutAllocationPool
            // 
            this.layoutAllocationPool.Control = this.lookupAllocationPool;
            this.layoutAllocationPool.CustomizationFormText = "Allocation Pool:";
            this.layoutAllocationPool.Location = new System.Drawing.Point(671, 26);
            this.layoutAllocationPool.Name = "layoutAllocationPool";
            this.layoutAllocationPool.Size = new System.Drawing.Size(569, 26);
            this.layoutAllocationPool.Text = "Allocation Pool:";
            this.layoutAllocationPool.TextSize = new System.Drawing.Size(88, 16);
            // 
            // layoutClearingBank
            // 
            this.layoutClearingBank.Control = this.lookupClearingBank;
            this.layoutClearingBank.CustomizationFormText = "Clearing Bank:";
            this.layoutClearingBank.Location = new System.Drawing.Point(0, 52);
            this.layoutClearingBank.Name = "layoutClearingBank";
            this.layoutClearingBank.Size = new System.Drawing.Size(297, 26);
            this.layoutClearingBank.Text = "Clearing Bank:";
            this.layoutClearingBank.TextSize = new System.Drawing.Size(83, 16);
            // 
            // layoutUnderlyingIndex
            // 
            this.layoutUnderlyingIndex.Control = this.lookupUnderlyingIndex;
            this.layoutUnderlyingIndex.CustomizationFormText = "Index:";
            this.layoutUnderlyingIndex.Location = new System.Drawing.Point(106, 0);
            this.layoutUnderlyingIndex.Name = "layoutUnderlyingIndex";
            this.layoutUnderlyingIndex.Size = new System.Drawing.Size(718, 26);
            this.layoutUnderlyingIndex.Text = "Underlying Index:";
            this.layoutUnderlyingIndex.TextSize = new System.Drawing.Size(100, 16);
            // 
            // layoutAllocationVessel
            // 
            this.layoutAllocationVessel.Control = this.lookupAllocationVessel;
            this.layoutAllocationVessel.CustomizationFormText = "Allocation Vessel:";
            this.layoutAllocationVessel.Location = new System.Drawing.Point(165, 26);
            this.layoutAllocationVessel.Name = "layoutAllocationVessel";
            this.layoutAllocationVessel.Size = new System.Drawing.Size(506, 26);
            this.layoutAllocationVessel.Text = "Allocation Vessel:";
            this.layoutAllocationVessel.TextSize = new System.Drawing.Size(101, 16);
            // 
            // layoutClearingAccount
            // 
            this.layoutClearingAccount.Control = this.lookupClearingAccount;
            this.layoutClearingAccount.CustomizationFormText = "Clearing Account:";
            this.layoutClearingAccount.Location = new System.Drawing.Point(297, 52);
            this.layoutClearingAccount.Name = "layoutClearingAccount";
            this.layoutClearingAccount.Size = new System.Drawing.Size(333, 26);
            this.layoutClearingAccount.Text = "Clearing Account:";
            this.layoutClearingAccount.TextSize = new System.Drawing.Size(101, 16);
            // 
            // layoutClearingHouse
            // 
            this.layoutClearingHouse.Control = this.lookupClearingHouse;
            this.layoutClearingHouse.CustomizationFormText = "Clearing House:";
            this.layoutClearingHouse.Location = new System.Drawing.Point(630, 52);
            this.layoutClearingHouse.Name = "layoutClearingHouse";
            this.layoutClearingHouse.Size = new System.Drawing.Size(381, 26);
            this.layoutClearingHouse.Text = "Clearing House:";
            this.layoutClearingHouse.TextSize = new System.Drawing.Size(91, 16);
            // 
            // layoutPeriodType
            // 
            this.layoutPeriodType.Control = this.cmbPeriodType;
            this.layoutPeriodType.CustomizationFormText = "Period Type:";
            this.layoutPeriodType.Location = new System.Drawing.Point(0, 78);
            this.layoutPeriodType.MaxSize = new System.Drawing.Size(169, 24);
            this.layoutPeriodType.MinSize = new System.Drawing.Size(169, 24);
            this.layoutPeriodType.Name = "layoutPeriodType";
            this.layoutPeriodType.Size = new System.Drawing.Size(169, 268);
            this.layoutPeriodType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodType.Text = "Period Type:";
            this.layoutPeriodType.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutPeriod
            // 
            this.layoutPeriod.Control = this.cmbPeriod;
            this.layoutPeriod.CustomizationFormText = "Period:";
            this.layoutPeriod.Location = new System.Drawing.Point(169, 78);
            this.layoutPeriod.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutPeriod.MinSize = new System.Drawing.Size(142, 24);
            this.layoutPeriod.Name = "layoutPeriod";
            this.layoutPeriod.Size = new System.Drawing.Size(217, 268);
            this.layoutPeriod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriod.Text = "Period:";
            this.layoutPeriod.TextSize = new System.Drawing.Size(41, 16);
            // 
            // layoutPeriodDayCountType
            // 
            this.layoutPeriodDayCountType.Control = this.cmbPeriodDayCountType;
            this.layoutPeriodDayCountType.CustomizationFormText = "Period Day Count Type:";
            this.layoutPeriodDayCountType.Location = new System.Drawing.Point(386, 78);
            this.layoutPeriodDayCountType.MaxSize = new System.Drawing.Size(223, 24);
            this.layoutPeriodDayCountType.MinSize = new System.Drawing.Size(223, 24);
            this.layoutPeriodDayCountType.Name = "layoutPeriodDayCountType";
            this.layoutPeriodDayCountType.Size = new System.Drawing.Size(223, 268);
            this.layoutPeriodDayCountType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodDayCountType.Text = "Period Day Count Type:";
            this.layoutPeriodDayCountType.TextSize = new System.Drawing.Size(135, 16);
            // 
            // layoutQuantityType
            // 
            this.layoutQuantityType.Control = this.cmbQuantityType;
            this.layoutQuantityType.CustomizationFormText = "Quantity Type:";
            this.layoutQuantityType.Location = new System.Drawing.Point(747, 78);
            this.layoutQuantityType.MaxSize = new System.Drawing.Size(181, 24);
            this.layoutQuantityType.MinSize = new System.Drawing.Size(181, 24);
            this.layoutQuantityType.Name = "layoutQuantityType";
            this.layoutQuantityType.Size = new System.Drawing.Size(181, 268);
            this.layoutQuantityType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutQuantityType.Text = "Quantity Type:";
            this.layoutQuantityType.TextSize = new System.Drawing.Size(84, 16);
            // 
            // layoutQuantityDays
            // 
            this.layoutQuantityDays.Control = this.txtQuantityDays;
            this.layoutQuantityDays.CustomizationFormText = "Quantity (days):";
            this.layoutQuantityDays.Location = new System.Drawing.Point(609, 78);
            this.layoutQuantityDays.MaxSize = new System.Drawing.Size(138, 24);
            this.layoutQuantityDays.MinSize = new System.Drawing.Size(138, 24);
            this.layoutQuantityDays.Name = "layoutQuantityDays";
            this.layoutQuantityDays.Size = new System.Drawing.Size(138, 268);
            this.layoutQuantityDays.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutQuantityDays.Text = "Quantity (days):";
            this.layoutQuantityDays.TextSize = new System.Drawing.Size(92, 16);
            // 
            // layoutPrice
            // 
            this.layoutPrice.Control = this.txtPrice;
            this.layoutPrice.CustomizationFormText = "Price:";
            this.layoutPrice.Location = new System.Drawing.Point(1125, 78);
            this.layoutPrice.MaxSize = new System.Drawing.Size(115, 24);
            this.layoutPrice.MinSize = new System.Drawing.Size(115, 24);
            this.layoutPrice.Name = "layoutPrice";
            this.layoutPrice.Size = new System.Drawing.Size(115, 268);
            this.layoutPrice.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPrice.Text = "Price:";
            this.layoutPrice.TextSize = new System.Drawing.Size(33, 16);
            // 
            // layoutSettlementType
            // 
            this.layoutSettlementType.Control = this.cmbSettlementType;
            this.layoutSettlementType.CustomizationFormText = "Settlement Type:";
            this.layoutSettlementType.Location = new System.Drawing.Point(824, 0);
            this.layoutSettlementType.Name = "layoutSettlementType";
            this.layoutSettlementType.Size = new System.Drawing.Size(208, 26);
            this.layoutSettlementType.Text = "Settlement Type:";
            this.layoutSettlementType.TextSize = new System.Drawing.Size(99, 16);
            // 
            // layoutPurpose
            // 
            this.layoutPurpose.Control = this.cmbPurpose;
            this.layoutPurpose.CustomizationFormText = "Purpose:";
            this.layoutPurpose.Location = new System.Drawing.Point(1032, 0);
            this.layoutPurpose.Name = "layoutPurpose";
            this.layoutPurpose.Size = new System.Drawing.Size(208, 26);
            this.layoutPurpose.Text = "Purpose:";
            this.layoutPurpose.TextSize = new System.Drawing.Size(51, 16);
            // 
            // layoutClearingFees
            // 
            this.layoutClearingFees.Control = this.txtClearingFees;
            this.layoutClearingFees.CustomizationFormText = "Clearing Fees:";
            this.layoutClearingFees.Location = new System.Drawing.Point(1011, 52);
            this.layoutClearingFees.Name = "layoutClearingFees";
            this.layoutClearingFees.Size = new System.Drawing.Size(229, 26);
            this.layoutClearingFees.Text = "Clearing Fees:";
            this.layoutClearingFees.TextSize = new System.Drawing.Size(83, 16);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtQuantityDays;
            this.layoutControlItem1.CustomizationFormText = "Quantity (days):";
            this.layoutControlItem1.Location = new System.Drawing.Point(806, 78);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(138, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(138, 24);
            this.layoutControlItem1.Name = "layoutQuantityDays";
            this.layoutControlItem1.Size = new System.Drawing.Size(138, 268);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Quantity (days):";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(92, 16);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtQuantityDays;
            this.layoutControlItem2.CustomizationFormText = "Quantity (days):";
            this.layoutControlItem2.Location = new System.Drawing.Point(806, 78);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(138, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(138, 24);
            this.layoutControlItem2.Name = "layoutQuantityDays";
            this.layoutControlItem2.Size = new System.Drawing.Size(138, 268);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Quantity (days):";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(92, 16);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // txtClosedOutQuantityTotal
            // 
            this.txtClosedOutQuantityTotal.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtClosedOutQuantityTotal.Location = new System.Drawing.Point(1084, 80);
            this.txtClosedOutQuantityTotal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtClosedOutQuantityTotal.Name = "txtClosedOutQuantityTotal";
            this.txtClosedOutQuantityTotal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtClosedOutQuantityTotal.Properties.Mask.EditMask = "d";
            this.txtClosedOutQuantityTotal.Properties.MaxLength = 5;
            this.txtClosedOutQuantityTotal.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtClosedOutQuantityTotal.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtClosedOutQuantityTotal.Size = new System.Drawing.Size(39, 22);
            this.txtClosedOutQuantityTotal.StyleController = this.layoutRoot;
            this.txtClosedOutQuantityTotal.TabIndex = 10;
            // 
            // layoutClosedOutQuantityTotal
            // 
            this.layoutClosedOutQuantityTotal.Control = this.txtClosedOutQuantityTotal;
            this.layoutClosedOutQuantityTotal.CustomizationFormText = "Closed Out Quantity Total:";
            this.layoutClosedOutQuantityTotal.Location = new System.Drawing.Point(928, 78);
            this.layoutClosedOutQuantityTotal.MaxSize = new System.Drawing.Size(197, 24);
            this.layoutClosedOutQuantityTotal.MinSize = new System.Drawing.Size(197, 24);
            this.layoutClosedOutQuantityTotal.Name = "layoutClosedOutQuantityTotal";
            this.layoutClosedOutQuantityTotal.Size = new System.Drawing.Size(197, 268);
            this.layoutClosedOutQuantityTotal.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutClosedOutQuantityTotal.Text = "Closed Out Quantity Total:";
            this.layoutClosedOutQuantityTotal.TextSize = new System.Drawing.Size(151, 16);
            // 
            // AddEditViewTradeFFAInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutRoot);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AddEditViewTradeFFAInfoControl";
            this.Size = new System.Drawing.Size(1240, 346);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtClearingFees.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPurpose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSettlementType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuantityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodDayCountType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingHouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationPool.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationVessel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAllocationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupUnderlyingIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationPool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnderlyingIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationVessel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodDayCountType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettlementType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedOutQuantityTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClosedOutQuantityTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraEditors.ComboBoxEdit cmbType;
        private DevExpress.XtraLayout.LayoutControlItem layoutType;
        private DevExpress.XtraEditors.LookUpEdit lookupUnderlyingIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutUnderlyingIndex;
        private DevExpress.XtraEditors.LookUpEdit lookupClearingBank;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingBank;
        private DevExpress.XtraEditors.ComboBoxEdit cmbAllocationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutAllocationType;
        private DevExpress.XtraEditors.LookUpEdit lookupAllocationVessel;
        private DevExpress.XtraLayout.LayoutControlItem layoutAllocationVessel;
        private DevExpress.XtraEditors.LookUpEdit lookupAllocationPool;
        private DevExpress.XtraLayout.LayoutControlItem layoutAllocationPool;
        private DevExpress.XtraEditors.LookUpEdit lookupClearingHouse;
        private DevExpress.XtraEditors.LookUpEdit lookupClearingAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingHouse;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodType;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodDayCountType;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodDayCountType;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.ComboBoxEdit cmbQuantityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutQuantityType;
        private DevExpress.XtraEditors.SpinEdit txtQuantityDays;
        private DevExpress.XtraLayout.LayoutControlItem layoutQuantityDays;
        private DevExpress.XtraEditors.SpinEdit txtPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutPrice;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPurpose;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSettlementType;
        private DevExpress.XtraLayout.LayoutControlItem layoutSettlementType;
        private DevExpress.XtraLayout.LayoutControlItem layoutPurpose;
        private DevExpress.XtraEditors.SpinEdit txtClearingFees;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingFees;
        private DevExpress.XtraEditors.SpinEdit txtClosedOutQuantityTotal;
        private DevExpress.XtraLayout.LayoutControlItem layoutClosedOutQuantityTotal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}
