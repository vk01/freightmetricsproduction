﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class BankAccountsManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public BankAccountsManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void BankAccountsManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetBankAccounts();
        }

        private void btnAdd_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof (Account));
        }

        private void btnEdit_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridBankAccountsMainView.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((Account) gridBankAccountsMainView.GetFocusedRow());
        }

        private void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridBankAccountsMainView.GetFocusedRow() == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((Account) gridBankAccountsMainView.GetFocusedRow());
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetBankAccounts();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void BankAccountsManagementForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void BankAccountsManagementForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridBankAccounts.BeginUpdate();

            int gridViewColumnindex = 0;

            gridBankAccountsMainView.Columns.Clear();

            gridBankAccountsMainView.Columns.Add(new GridColumn
                                                     {
                                                         Caption = Strings.Code,
                                                         FieldName = "Code",
                                                         VisibleIndex = gridViewColumnindex++
                                                     });
            gridBankAccountsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Name,
                                                    FieldName = "Name",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridBankAccountsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Status,
                                                    FieldName = "Status",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridBankAccountsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Company,
                                                    FieldName = "CompanyName",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridBankAccountsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Bank,
                                                    FieldName = "BankName",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridBankAccountsMainView.Columns.Add(new GridColumn
                                                     {
                                                         Caption = Strings.Currency,
                                                         FieldName = "CurrencyCode",
                                                         VisibleIndex = gridViewColumnindex
                                                     });

            gridBankAccounts.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetBankAccounts()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new Account(),  EndGetAccounts, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetAccounts(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetAccounts;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridBankAccounts.DataSource = entities.Cast<Account>().ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion
    }
}