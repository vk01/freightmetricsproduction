﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Exis.Domain;
using DevExpress.XtraLayout;
using Exis.WinClient.General;

namespace Exis.WinClient.Controls.Core
{
    public partial class AddEditViewTradeFFAInfoControl : XtraUserControl
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private Company _company;
        private Market _market;
        private DateTime _signDate;
        private TradeInfo _tradeInfo;

        #endregion

        #region Constructors

        public AddEditViewTradeFFAInfoControl()
        {
            InitializeComponent();
            
            _formActionType = FormActionTypeEnum.Add;
            InitializeControls();
        }

        public AddEditViewTradeFFAInfoControl(FormActionTypeEnum formActionType)
        {
            InitializeComponent();

            _formActionType = formActionType;
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            cmbType.Properties.Items.Add(TradeInfoDirectionEnum.InOrBuy);
            cmbType.Properties.Items.Add(TradeInfoDirectionEnum.OutOrSell);
            cmbType.SelectedItem = TradeInfoDirectionEnum.InOrBuy;

            cmbPeriodType.Properties.Items.Add(PeriodTypeEnum.Month);
            cmbPeriodType.Properties.Items.Add(PeriodTypeEnum.Quarter);
            cmbPeriodType.Properties.Items.Add(PeriodTypeEnum.Calendar);
            cmbPeriodType.SelectedItem = PeriodTypeEnum.Month;

            cmbPeriod.Properties.ValueMember = "EditValue";
            cmbPeriod.Properties.DisplayMember = "Description";

            cmbPeriodDayCountType.Properties.Items.Add(TradeInfoPeriodDayCountTypeEnum.Thirty);
            cmbPeriodDayCountType.Properties.Items.Add(TradeInfoPeriodDayCountTypeEnum.Actual);
            cmbPeriodDayCountType.SelectedItem = TradeInfoPeriodDayCountTypeEnum.Thirty;

            cmbSettlementType.Properties.Items.Add(TradeInfoSettlementTypeEnum.Last7Days);
            cmbSettlementType.Properties.Items.Add(TradeInfoSettlementTypeEnum.AllDays);
            cmbSettlementType.SelectedItem = TradeInfoSettlementTypeEnum.AllDays;

            cmbPurpose.Properties.Items.Add(TradeFFAInfoPurposeEnum.Hedge);
            cmbPurpose.Properties.Items.Add(TradeFFAInfoPurposeEnum.Spec);
            cmbPurpose.SelectedItem = TradeFFAInfoPurposeEnum.Hedge;

            lookupUnderlyingIndex.Properties.DisplayMember = "Name";
            lookupUnderlyingIndex.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
                          {
                              Caption = Strings.Market_Index,
                              FieldName = "Name",
                              Width = 0
                          };
            lookupUnderlyingIndex.Properties.Columns.Add(col);
            lookupUnderlyingIndex.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupUnderlyingIndex.Properties.SearchMode = SearchMode.AutoFilter;
            lookupUnderlyingIndex.Properties.NullValuePrompt =
                Strings.Select_a_Market_Index___;

            cmbAllocationType.Properties.Items.Add(TradeInfoVesselAllocationTypeEnum.None);
            cmbAllocationType.Properties.Items.Add(TradeInfoVesselAllocationTypeEnum.Single);
            cmbAllocationType.Properties.Items.Add(TradeInfoVesselAllocationTypeEnum.Pool);
            cmbAllocationType.SelectedItem = TradeInfoVesselAllocationTypeEnum.None;

            cmbQuantityType.Properties.Items.Add(TradeInfoQuantityTypeEnum.PerMonth);
            cmbQuantityType.Properties.Items.Add(TradeInfoQuantityTypeEnum.Total);
            cmbQuantityType.SelectedItem = TradeInfoQuantityTypeEnum.PerMonth;

            lookupAllocationVessel.Properties.DisplayMember = "Name";
            lookupAllocationVessel.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Vessel_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupAllocationVessel.Properties.Columns.Add(col);
            lookupAllocationVessel.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupAllocationVessel.Properties.SearchMode = SearchMode.AutoFilter;
            lookupAllocationVessel.Properties.NullValuePrompt =
                Strings.Select_a_Vessel___;
            lookupAllocationVessel.Properties.NullText = null;

            lookupAllocationPool.Properties.DisplayMember = "Name";
            lookupAllocationPool.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Vessel_Pool_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupAllocationPool.Properties.Columns.Add(col);
            lookupAllocationPool.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupAllocationPool.Properties.SearchMode = SearchMode.AutoFilter;
            lookupAllocationPool.Properties.NullValuePrompt = Strings.Select_a_Vessel_Pool___;
            lookupAllocationPool.Properties.NullText = null;

            lookupClearingBank.Properties.DisplayMember = "Name";
            lookupClearingBank.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Bank_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupClearingBank.Properties.Columns.Add(col);
            lookupClearingBank.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupClearingBank.Properties.SearchMode = SearchMode.AutoFilter;
            lookupClearingBank.Properties.NullValuePrompt = Strings.Select_a_Bank___;
            lookupClearingBank.Properties.NullText = "";

            lookupClearingAccount.Properties.DisplayMember = "Name";
            lookupClearingAccount.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Account_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupClearingAccount.Properties.Columns.Add(col);
            lookupClearingAccount.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupClearingAccount.Properties.SearchMode = SearchMode.AutoFilter;
            lookupClearingAccount.Properties.NullValuePrompt = Strings.Select_an_Account___;
            lookupClearingAccount.Properties.NullText = "";

            lookupClearingHouse.Properties.DisplayMember = "Name";
            lookupClearingHouse.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Clearing_House_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupClearingHouse.Properties.Columns.Add(col);
            lookupClearingHouse.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupClearingHouse.Properties.SearchMode = SearchMode.AutoFilter;
            lookupClearingHouse.Properties.NullValuePrompt = Strings.Select_a_Clearing_House___;
            lookupClearingHouse.Properties.NullText = "";
        }

        private void FilterAccounts()
        {
            lookupClearingAccount.EditValue = null;
            lookupClearingAccount.Properties.DataSource = null;

            if (_company == null || lookupClearingBank.EditValue == null) return;

            lookupClearingAccount.Properties.DataSource =
                ((List<Account>) lookupClearingAccount.Tag).Where(
                    a => a.CompanyId == _company.Id && a.BankId == Convert.ToInt64(lookupClearingBank.EditValue)).ToList
                    ();
        }

        private void FilterPeriods()
        {
            cmbPeriod.Properties.Items.Clear();
            if (cmbPeriodType.EditValue == null) return;

            var customPeriods = new List<CustomPeriod>();
            DateTime currentDate = _signDate;

            if ((PeriodTypeEnum) cmbPeriodType.SelectedItem == PeriodTypeEnum.Month)
            {
                while (currentDate <= _signDate.AddYears(10))
                {
                    var period = new CustomPeriod()
                    {
                        Date = currentDate,
                        Description = currentDate.ToString("MMM-yy", new CultureInfo("en-GB")),
                        EditValue = currentDate.ToString("MMM-yy", new CultureInfo("en-GB"))
                    };
                    customPeriods.Add(period);
                    currentDate = currentDate.AddMonths(1);
                }
                cmbPeriod.Properties.DataSource = customPeriods;
                cmbPeriod.Tag = customPeriods;
            }
            else if ((PeriodTypeEnum) cmbPeriodType.SelectedItem == PeriodTypeEnum.Quarter)
            {
                while (currentDate <= _signDate.AddYears(10))
                {
                    int intQuarter = ((currentDate.Month - 1) / 3) + 1;
                    string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                    var period = new CustomPeriod()
                    {
                        Date = currentDate,
                        Description = strQuarter + currentDate.ToString("-yy", new CultureInfo("en-GB")),
                        EditValue = strQuarter + currentDate.ToString("-yy", new CultureInfo("en-GB"))
                    };
                    customPeriods.Add(period);
                    currentDate = currentDate.AddMonths(3);
                }
                cmbPeriod.Properties.DataSource = customPeriods;
                cmbPeriod.Tag = customPeriods;
            }
            else if ((PeriodTypeEnum) cmbPeriodType.SelectedItem == PeriodTypeEnum.Calendar)
            {
                while (currentDate <= _signDate.AddYears(10))
                {
                    var period = new CustomPeriod()
                    {
                        Date = currentDate,
                        Description = "Cal" + currentDate.ToString("-yy", new CultureInfo("en-GB")),
                        EditValue = "Cal" + currentDate.ToString("-yy", new CultureInfo("en-GB"))
                    };
                    customPeriods.Add(period);
                    currentDate = currentDate.AddYears(1);
                }
                cmbPeriod.Properties.DataSource = customPeriods;
                cmbPeriod.Tag = customPeriods;
            }
        }

        private void FilterVesselPools(List<VesselPool> vesselPools)
        {
            if (_formActionType == FormActionTypeEnum.Add)
                lookupAllocationPool.EditValue = null;
            lookupAllocationPool.Properties.DataSource = null;

            lookupAllocationPool.Tag = vesselPools;
            lookupAllocationPool.Properties.DataSource = vesselPools;
        }

        #endregion

        #region Public Methods

        public void SetInitializationData(List<Index> indexes, List<Vessel> vessels, List<VesselPool> vesselPools,
                                          List<Company> banks, List<Account> accounts,
                                          List<ClearingHouse> clearingHouses)
        {
            lookupUnderlyingIndex.Tag = indexes;
            lookupAllocationVessel.Tag = vessels;
            lookupAllocationVessel.Properties.DataSource = vessels;
            lookupAllocationPool.Tag = vesselPools;
            lookupAllocationPool.Properties.DataSource = vesselPools;
            lookupClearingBank.Tag = banks;
            lookupClearingBank.Properties.DataSource = banks;
            lookupClearingAccount.Tag = accounts;
            lookupClearingHouse.Tag = clearingHouses;
            lookupClearingHouse.Properties.DataSource = clearingHouses;
        }

        public void SetMarket(Market market)
        {
            _market = market;

            lookupUnderlyingIndex.EditValue = null;
            lookupUnderlyingIndex.Properties.DataSource = null;

            if (_market != null)
            {
                lookupUnderlyingIndex.Properties.DataSource =
                    ((List<Index>) lookupUnderlyingIndex.Tag).Where(a => a.MarketId == _market.Id).ToList();

                lookupUnderlyingIndex.EditValue =
                    ((List<Index>)lookupUnderlyingIndex.Tag).Where(a => a.IsMarketDefault && a.MarketId == _market.Id).Any()
                        ? ((List<Index>)lookupUnderlyingIndex.Tag).Where(a => a.IsMarketDefault && a.MarketId == _market.Id).First().Id
                        : (long?)null;
            }
        }

        public void SetCompany(Company company)
        {
            _company = company;

            FilterAccounts();
        }

        public void SetSignDate(DateTime signDate, List<VesselPool> vesselPools)
        {
            _signDate = signDate;

            FilterPeriods();
            if(vesselPools != null)
                FilterVesselPools(vesselPools);
        }

        public void GetData(out TradeInfoDirectionEnum direction, out long indexId, out TradeInfoVesselAllocationTypeEnum allocationType, out long? allocationVesselId, out long? allocationVesselPoolId, out long? clearingBankId, out long? clearingAccountId, out long? clearingHouseId, out PeriodTypeEnum periodType, out string periodString, out List<DateTime> periodDates, out TradeInfoPeriodDayCountTypeEnum periodDayCountType, out TradeInfoQuantityTypeEnum quantityType, out decimal quantity, out decimal price, out TradeInfoSettlementTypeEnum settlementType, out TradeFFAInfoPurposeEnum purpose, out VesselPool vesselPool, out int clearingFees, out decimal closedOutQuantityTotal)
        {
            direction = (TradeInfoDirectionEnum)cmbType.SelectedItem;
            indexId = (long) lookupUnderlyingIndex.EditValue;
            allocationType = (TradeInfoVesselAllocationTypeEnum)cmbAllocationType.SelectedItem;
            allocationVesselId = lookupAllocationVessel.EditValue == null ? null : (long?) lookupAllocationVessel.EditValue;
            allocationVesselPoolId = lookupAllocationPool.EditValue == null ? null : (long?) lookupAllocationPool.EditValue;
            clearingBankId = lookupClearingBank.EditValue == null ? null : (long?) lookupClearingBank.EditValue;
            clearingAccountId = lookupClearingAccount.EditValue == null ? null : (long?) lookupClearingAccount.EditValue;
            clearingHouseId = lookupClearingHouse.EditValue == null ? null : (long?) lookupClearingHouse.EditValue;
            periodType = (PeriodTypeEnum) cmbPeriodType.SelectedItem;
            periodString = cmbPeriod.Text;
            List<DateTime> tmpDates = (from CheckedListBoxItem item in cmbPeriod.Properties.Items
                                       where item.CheckState == CheckState.Checked
                                       select item.Value
                                       into periodStr
                                       select
                                           ((List<CustomPeriod>) cmbPeriod.Tag).Where(a => a.EditValue == periodStr).
                                           First().Date).ToList();

            periodDates = tmpDates;
            periodDayCountType = (TradeInfoPeriodDayCountTypeEnum) cmbPeriodDayCountType.SelectedItem;
            quantityType = (TradeInfoQuantityTypeEnum) cmbQuantityType.SelectedItem;
            quantity = Convert.ToDecimal(txtQuantityDays.Value);
            price = txtPrice.Value;
            settlementType = (TradeInfoSettlementTypeEnum) cmbSettlementType.SelectedItem;
            purpose = (TradeFFAInfoPurposeEnum) cmbPurpose.SelectedItem;
            vesselPool = lookupAllocationPool.EditValue == null
                             ? null
                             : ((List<VesselPool>) lookupAllocationPool.Tag).Where(
                                 a => a.Id == (long) lookupAllocationPool.EditValue).Single();
            clearingFees = (int)txtClearingFees.Value;
            closedOutQuantityTotal = Convert.ToDecimal(txtClosedOutQuantityTotal.Value);
        }

        public bool ValidateData()
        {
            errorProvider.ClearErrors();

            if (cmbType.SelectedItem == null)
                errorProvider.SetError(cmbType, Strings.Field_is_mandatory);
            if (lookupUnderlyingIndex.EditValue == null)
                errorProvider.SetError(lookupUnderlyingIndex, Strings.Field_is_mandatory);
            if (cmbAllocationType.SelectedItem == null)
                errorProvider.SetError(cmbAllocationType, Strings.Field_is_mandatory);
            if (cmbAllocationType.SelectedItem != null &&
                (TradeInfoVesselAllocationTypeEnum)cmbAllocationType.SelectedItem == TradeInfoVesselAllocationTypeEnum.Single &&
                lookupAllocationVessel.EditValue == null)
                errorProvider.SetError(lookupAllocationVessel, Strings.Field_is_mandatory);
            if (cmbAllocationType.SelectedItem != null &&
                (TradeInfoVesselAllocationTypeEnum)cmbAllocationType.SelectedItem == TradeInfoVesselAllocationTypeEnum.Pool &&
                lookupAllocationPool.EditValue == null)
                errorProvider.SetError(lookupAllocationPool, Strings.Field_is_mandatory);
            if (cmbPeriodType.SelectedItem == null)
                errorProvider.SetError(cmbPeriodType, Strings.Field_is_mandatory);
            if (cmbPeriod.Properties.Items.GetCheckedValues().Count == 0)
                errorProvider.SetError(cmbPeriod, Strings.Field_is_mandatory);
            if (cmbPeriodDayCountType.SelectedItem == null)
                errorProvider.SetError(cmbPeriodDayCountType, Strings.Field_is_mandatory);
            if (cmbQuantityType.SelectedItem == null)
                errorProvider.SetError(cmbQuantityType, Strings.Field_is_mandatory);
            if (cmbSettlementType.SelectedItem == null)
                errorProvider.SetError(cmbSettlementType, Strings.Field_is_mandatory);
            if (cmbPurpose.SelectedItem == null)
                errorProvider.SetError(cmbPurpose, Strings.Field_is_mandatory);

            List<int> checkedItems = new List<int>();
            for (int i = 0; i < cmbPeriod.Properties.Items.Count; i++)
            {
                CheckedListBoxItem item = cmbPeriod.Properties.Items[i];
                if (item.CheckState == CheckState.Checked)
                {
                    if(checkedItems.Count > 0 && !checkedItems.Contains(i-1))
                    {
                        errorProvider.SetError(cmbPeriod, Strings.Periods_should_be_continuous);
                        break;
                    }
                    checkedItems.Add(i);
                }
            }
            return !errorProvider.HasErrors;
        }

        public void LoadData(TradeInfo tradeInfo)
        {
            _tradeInfo = tradeInfo;

            cmbType.EditValue = _tradeInfo.Direction;
            lookupUnderlyingIndex.EditValue = _tradeInfo.FFAInfo.IndexId;
            cmbSettlementType.EditValue = _tradeInfo.FFAInfo.SettlementType;
            cmbPurpose.EditValue = _tradeInfo.FFAInfo.Purpose;
            cmbAllocationType.EditValue = _tradeInfo.FFAInfo.AllocationType;
            lookupAllocationVessel.EditValue = _tradeInfo.FFAInfo.VesselId;
            lookupAllocationPool.EditValue = _tradeInfo.FFAInfo.VesselPoolId;
            lookupClearingBank.EditValue = _tradeInfo.FFAInfo.BankId;
            lookupClearingHouse.EditValue = _tradeInfo.FFAInfo.ClearingHouseId;
            lookupClearingAccount.EditValue = _tradeInfo.FFAInfo.AccountId;
            cmbPeriodType.EditValue = _tradeInfo.FFAInfo.PeriodType;
            cmbPeriod.SetEditValue(_tradeInfo.FFAInfo.Period);
            
            cmbPeriodDayCountType.EditValue = _tradeInfo.FFAInfo.PeriodDayCountType;
            txtQuantityDays.EditValue = _tradeInfo.FFAInfo.QuantityDays;
            cmbQuantityType.EditValue = _tradeInfo.FFAInfo.QuantityType;
            txtPrice.EditValue = _tradeInfo.FFAInfo.Price;
            txtClearingFees.EditValue = _tradeInfo.FFAInfo.ClearingFees;
            txtClosedOutQuantityTotal.EditValue = _tradeInfo.FFAInfo.ClosedOutQuantityTotal;
        }

        public void SetReadOnlyState()
        {
            cmbType.Properties.ReadOnly = true;
            lookupUnderlyingIndex.Properties.ReadOnly = true;
            cmbAllocationType.Properties.ReadOnly = true;
            lookupAllocationVessel.Properties.ReadOnly = true;
            lookupAllocationPool.Properties.ReadOnly = true;
            lookupClearingBank.Properties.ReadOnly = true;
            lookupClearingHouse.Properties.ReadOnly = true;
            lookupClearingAccount.Properties.ReadOnly = true;
            cmbPeriodType.Properties.ReadOnly = true;
            cmbPeriod.Properties.ReadOnly = true;
            cmbPeriodDayCountType.Properties.ReadOnly = true;
            cmbQuantityType.Properties.ReadOnly = true;
            txtQuantityDays.Properties.ReadOnly = true;
            txtPrice.Properties.ReadOnly = true;
            cmbSettlementType.Properties.ReadOnly = true;
            cmbPurpose.Properties.ReadOnly = true;
            txtClearingFees.Properties.ReadOnly = true;
            txtClosedOutQuantityTotal.Properties.ReadOnly = true;
        }

        #endregion

        #region GUI Events

        private void cmbAllocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lookupAllocationVessel.EditValue = null;
            lookupAllocationPool.EditValue = null;

            if (((TradeInfoVesselAllocationTypeEnum) cmbAllocationType.SelectedItem) == TradeInfoVesselAllocationTypeEnum.None)
            {
                lookupAllocationVessel.Properties.ReadOnly = true;
                lookupAllocationPool.Properties.ReadOnly = true;
            }
            else if (((TradeInfoVesselAllocationTypeEnum) cmbAllocationType.SelectedItem) ==
                     TradeInfoVesselAllocationTypeEnum.Single)
            {
                lookupAllocationVessel.Properties.ReadOnly = false;
                lookupAllocationPool.Properties.ReadOnly = true;
            }
            else if (((TradeInfoVesselAllocationTypeEnum) cmbAllocationType.SelectedItem) ==
                     TradeInfoVesselAllocationTypeEnum.Pool)
            {
                lookupAllocationVessel.Properties.ReadOnly = true;
                lookupAllocationPool.Properties.ReadOnly = false;
            }
        }

        private void lookupClearingBank_EditValueChanged(object sender, EventArgs e)
        {
            FilterAccounts();
        }

        private void cmbPeriodType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterPeriods();
        }

        private void lookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back) ((LookUpEdit) sender).EditValue = null;
        }

        private void lookupUnderlyingIndex_EditValueChanged(object sender, EventArgs e)
        {
            var parentCtrl = (AddEditViewTradeForm)((LayoutControl)this.Parent).Parent;
            parentCtrl.SetIndexesAccordingToUnderlyingIndex((long?)lookupUnderlyingIndex.EditValue);
        }

        #endregion

        #region Custom class

        public class CustomPeriod
        {
            public DateTime Date { get; set; }
            public string Description { get; set; }
            public object EditValue { get; set; }
        }

        #endregion
    }
}