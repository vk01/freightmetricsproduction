﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using Exis.Domain;
using DevExpress.XtraLayout.Utils;

namespace Exis.WinClient.Controls
{
    public partial class InterestReferenceRateAEVForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private InterestReferenceRate _interestReferenceRate;

        #endregion

        #region Constructors 

        public InterestReferenceRateAEVForm()
        {
            InitializeComponent();

            _formActionType = FormActionTypeEnum.Add;

            InitializeControls();
        }

        public InterestReferenceRateAEVForm(InterestReferenceRate interestReferenceRate, FormActionTypeEnum formActionTypeEnum)
        {
            InitializeComponent();

            _formActionType = formActionTypeEnum;
            _interestReferenceRate = interestReferenceRate;

            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            cmbType.Properties.Items.Add(InterestReferenceRateTypeEnum.Libor);
            cmbType.Properties.Items.Add(InterestReferenceRateTypeEnum.RiskFree);
            cmbType.Properties.Items.Add(InterestReferenceRateTypeEnum.Deposit);

            foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
            {
                cmbPeriodType.Properties.Items.Add(type);
            }

            lookUpCurrency.Properties.DisplayMember = "Name";
            lookUpCurrency.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookUpCurrency.Properties.Columns.Add(col);
            lookUpCurrency.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpCurrency.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpCurrency.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            if (_formActionType == FormActionTypeEnum.View)
            {
                cmbType.Properties.ReadOnly = true;
                cmbPeriodType.Properties.ReadOnly = true;
                txtRate.Enabled = false;
                dtpDate.Enabled = false;

                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void LoadData()
        {
            cmbType.SelectedItem = _interestReferenceRate.Type;
            cmbPeriodType.SelectedItem = _interestReferenceRate.PeriodType;
            txtRate.EditValue = _interestReferenceRate.Rate;
            dtpDate.EditValue = _interestReferenceRate.Date;
            lookUpCurrency.EditValue = _interestReferenceRate.CurrencyId;
        }

        private bool ValidateData()
        {
            if (cmbType.SelectedItem == null) errorProvider.SetError(cmbType, Strings.Field_is_mandatory);
            if (cmbPeriodType.SelectedItem == null) errorProvider.SetError(cmbPeriodType, Strings.Field_is_mandatory);
            if (string.IsNullOrEmpty(txtRate.Text)) errorProvider.SetError(txtRate, Strings.Field_is_mandatory);
            if (dtpDate.EditValue == null) errorProvider.SetError(dtpDate, Strings.Field_is_mandatory);
            if(lookUpCurrency.EditValue == null) errorProvider.SetError(lookUpCurrency, Strings.Field_is_mandatory);
            
            return !errorProvider.HasErrors;
        }

        #endregion
        
        #region GUI Event Handlers

        private void InterestReferenceRateAevFormLoad(object sender, EventArgs e)
        {
            BeginAEVInterestReferenceRateInitializationData();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _interestReferenceRate = new InterestReferenceRate();
            }

            _interestReferenceRate.Type = (InterestReferenceRateTypeEnum)cmbType.SelectedItem;
            _interestReferenceRate.PeriodType = (InterestReferencePeriodTypeEnum) cmbPeriodType.SelectedItem;
            _interestReferenceRate.Rate = Convert.ToDecimal(txtRate.EditValue);
            _interestReferenceRate.Date = (DateTime)dtpDate.EditValue;
            _interestReferenceRate.CurrencyId = Convert.ToInt64(lookUpCurrency.EditValue);

            BeginAEVInterestReferenceType(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void InterestReferenceRateAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void InterestReferenceRateAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVInterestReferenceRateInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVInterestReferenceRateInitializationData(EndAEVInterestReferenceRateInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVInterestReferenceRateInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVInterestReferenceRateInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Currency> currencies;
            try
            {
                result = SessionRegistry.Client.EndAEVInterestReferenceRateInitializationData(out currencies, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookUpCurrency.Tag = currencies;
                lookUpCurrency.Properties.DataSource = currencies;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVInterestReferenceType(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEInterestReferenceRate(isEdit, _interestReferenceRate, EndAEVInterestReferenceType, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVInterestReferenceType(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVInterestReferenceType;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEInterestReferenceRate(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
            else if(result == 1)// there is already a similar reference rate type
            {
                XtraMessageBox.Show(this, "There is already a similar interest reference rate in the system.", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                DialogResult = DialogResult.OK;
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public InterestReferenceRate InterestReferenceRate
        {
            get { return _interestReferenceRate; }
        }

        public FormActionTypeEnum FormActionType { get { return _formActionType; } }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

    }
}