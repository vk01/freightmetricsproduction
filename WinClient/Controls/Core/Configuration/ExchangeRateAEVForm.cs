﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using Exis.Domain;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;

namespace Exis.WinClient.Controls
{
    public partial class ExchangeRateAEVForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private ExchangeRate _exchangeRate;

        #endregion

        #region Constructors
        
        public ExchangeRateAEVForm()
        {
            InitializeComponent();

            _formActionType = FormActionTypeEnum.Add;
            InitializeControls();
        }

        public ExchangeRateAEVForm(ExchangeRate exchangeRate, FormActionTypeEnum formActionTypeEnum)
        {
            InitializeComponent();

            _formActionType = formActionTypeEnum;
            _exchangeRate = exchangeRate;
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            lookUpSourceCurrency1.Properties.DisplayMember = "Name";
            lookUpSourceCurrency1.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookUpSourceCurrency1.Properties.Columns.Add(col);
            lookUpSourceCurrency1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpSourceCurrency1.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpSourceCurrency1.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            lookUpTargetCurrency1.Properties.DisplayMember = "Name";
            lookUpTargetCurrency1.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookUpTargetCurrency1.Properties.Columns.Add(col);
            lookUpTargetCurrency1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpTargetCurrency1.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpTargetCurrency1.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            lookUpSourceCurrency2.Properties.DisplayMember = "Name";
            lookUpSourceCurrency2.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookUpSourceCurrency2.Properties.Columns.Add(col);
            lookUpSourceCurrency2.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpSourceCurrency2.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpSourceCurrency2.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            lookUpTargetCurrency2.Properties.DisplayMember = "Name";
            lookUpTargetCurrency2.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookUpTargetCurrency2.Properties.Columns.Add(col);
            lookUpTargetCurrency2.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpTargetCurrency2.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpTargetCurrency2.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            if (_formActionType == FormActionTypeEnum.View)
            {
                txtRate1.Enabled = false;
                dtpDate.Enabled = false;

                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void LoadData()
        {
            txtRate1.EditValue = _exchangeRate.Rate;
            dtpDate.EditValue = _exchangeRate.Date;
            lookUpSourceCurrency1.EditValue = _exchangeRate.SourceCurrencyId;
            lookUpTargetCurrency1.EditValue = _exchangeRate.TargetCurrencyId;
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtRate1.Text)) errorProvider.SetError(txtRate1, Strings.Field_is_mandatory);
            if (dtpDate.EditValue == null) errorProvider.SetError(dtpDate, Strings.Field_is_mandatory);
            if (lookUpSourceCurrency1.EditValue == null) errorProvider.SetError(lookUpSourceCurrency1, Strings.Field_is_mandatory);
            if (lookUpTargetCurrency1.EditValue == null) errorProvider.SetError(lookUpTargetCurrency1, Strings.Field_is_mandatory);

            if (lookUpSourceCurrency1.EditValue != null && lookUpTargetCurrency1.EditValue != null &&
                Convert.ToInt64(lookUpSourceCurrency1.EditValue) == Convert.ToInt64(lookUpTargetCurrency1.EditValue))
                errorProvider.SetError(lookUpTargetCurrency1, "Wrong Target Currency");

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Event Handlers

        private void ExchangeRateAevFormLoad(object sender, EventArgs e)
        {
            BeginAEVExchangeRateInitializationData();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _exchangeRate = new ExchangeRate();
            }

            _exchangeRate.Rate = Convert.ToDecimal(txtRate1.EditValue);
            _exchangeRate.Date = (DateTime)dtpDate.EditValue;
            _exchangeRate.SourceCurrencyId = Convert.ToInt64(lookUpSourceCurrency1.EditValue);
            _exchangeRate.TargetCurrencyId = Convert.ToInt64(lookUpTargetCurrency1.EditValue);

            BeginAEVExchangeRate(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void ExchangeRateAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void ExchangeRateAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void LookUpSourceCurrency1EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpSourceCurrency1.EditValue != null)
            {
                lookUpTargetCurrency2.EditValue = lookUpSourceCurrency1.EditValue;

                lcgExchange1.Text = lookUpSourceCurrency1.Text + " -> " + lookUpTargetCurrency1.Text;
            }
        }

        private void LookUpTargetCurrency1EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpTargetCurrency1.EditValue != null)
            {
                lookUpSourceCurrency2.EditValue = lookUpTargetCurrency1.EditValue;
                lcgExchange1.Text = lookUpSourceCurrency1.Text + " -> " + lookUpTargetCurrency1.Text;
            }
        }

        private void LookUpSourceCurrency2EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpSourceCurrency2.EditValue != null)
            {
                lookUpTargetCurrency1.EditValue = lookUpSourceCurrency2.EditValue;
                lcgExchange2.Text = lookUpSourceCurrency2.Text + " -> " + lookUpTargetCurrency2.Text;
            }
        }

        private void LookUpTargetCurrency2EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpTargetCurrency2.EditValue != null)
            {
                lookUpSourceCurrency1.EditValue = lookUpTargetCurrency2.EditValue;
                lcgExchange2.Text = lookUpSourceCurrency2.Text + " -> " + lookUpTargetCurrency2.Text;
            }
        }

        private void TxtRate1EditValueChanged(object sender, EventArgs e)
        {
            if (txtRate1.EditValue != null && Convert.ToDecimal(txtRate1.EditValue) != 0)
                txtRate2.EditValue = 1/Convert.ToDecimal(txtRate1.EditValue);
        }

        private void TxtRate2EditValueChanged(object sender, EventArgs e)
        {
            if (txtRate2.EditValue != null)
                txtRate1.EditValue = 1 / Convert.ToDecimal(txtRate2.EditValue);
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVExchangeRateInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVExchangeRateInitializationData(EndAEVExchangeRateInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVExchangeRateInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVExchangeRateInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Currency> currencies;
            try
            {
                result = SessionRegistry.Client.EndAEVExchangeRateInitializationData(out currencies, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookUpSourceCurrency1.Tag = currencies;
                lookUpSourceCurrency1.Properties.DataSource = currencies;

                lookUpTargetCurrency1.Tag = currencies;
                lookUpTargetCurrency1.Properties.DataSource = currencies;

                lookUpSourceCurrency2.Tag = currencies;
                lookUpSourceCurrency2.Properties.DataSource = currencies;

                lookUpTargetCurrency2.Tag = currencies;
                lookUpTargetCurrency2.Properties.DataSource = currencies;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVExchangeRate(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _exchangeRate, EndAEVExchangeRate, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVExchangeRate(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVExchangeRate;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
            else if (result == 1)// there is already a similar reference rate type
            {
                XtraMessageBox.Show(this, "There is already a similar exchange rate in the system.", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public ExchangeRate ExchangeRate
        {
            get { return _exchangeRate; }
        }

        public FormActionTypeEnum FormActionType { get { return _formActionType; } }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

        
    }
}