﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class CompanyAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private Company _company;
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public CompanyAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public CompanyAEVForm(Company company, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _company = company;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            foreach (var enumValue in Enum.GetValues(typeof(CompanyTypeEnum)))
            {
                cmbType.Properties.Items.Add(Enum.Parse(typeof(CompanyTypeEnum), enumValue.ToString(), true));
            }

            lookupSubtype.Properties.DisplayMember = "Name";
            lookupSubtype.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Subtype_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupSubtype.Properties.Columns.Add(col);
            lookupSubtype.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupSubtype.Properties.SearchMode = SearchMode.AutoFilter;
            lookupSubtype.Properties.NullValuePrompt = Strings.Select_a_Subtype___;

            //treeParentCompany.BeginUpdate();
           // treeParentCompany.Columns.Add();
           // treeParentCompany.Columns[0].Caption = Strings.Company_Name;
           // treeParentCompany.Columns[0].VisibleIndex = 0;
           // treeParentCompany.EndUpdate();

            lookupCompany.Properties.DisplayMember = "Name";
            lookupCompany.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Company_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupCompany.Properties.Columns.Add(col);
            lookupCompany.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCompany.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCompany.Properties.NullValuePrompt = Strings.Select_a_Parent_Company___;

            lookupManager.Properties.DisplayMember = "Name";
            lookupManager.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                          {
                              Caption = Strings.Manager_Name,
                              FieldName = "Name",
                              Width = 0
                          };
            lookupManager.Properties.Columns.Add(col);
            lookupManager.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupManager.Properties.SearchMode = SearchMode.AutoFilter;
            lookupManager.Properties.NullValuePrompt = Strings.Select_a_Manager___;

            if (_formActionType == FormActionTypeEnum.View)
            {
                txtName.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                cmbType.Properties.ReadOnly = true;
                lookupSubtype.Properties.ReadOnly = true;
                //treeParentCompany.OptionsBehavior.Editable = false;
                txtRiskRating.Properties.ReadOnly = true;
                txtOwnership.Properties.ReadOnly = true;
                lookupCompany.Properties.ReadOnly = true;
                lookupManager.Properties.ReadOnly = true;

                btnSave.Visibility = BarItemVisibility.Never;
            }
        }

        private void InitializeData()
        {
            BeginAEVCompanyInitializationData();
        }

      /*  private void PopulateTree(TreeListNode parentNode, List<Company> childrenCompanies,
                                  ref List<Company> allCompanies)
        {
            foreach (Company company in childrenCompanies)
            {
                if (_formActionType == FormActionTypeEnum.Edit && company.Id == _company.Id) continue;
                TreeListNode childNode =
                    treeParentCompany.AppendNode(
                        new object[] {company.Name}, parentNode, company);
                PopulateTree(childNode, allCompanies.Where(a => a.ParentId == company.Id).ToList(), ref allCompanies);
            }
        }*/

        private void LoadData()
        {
            lblId.Text = _company.Id.ToString();
            txtName.Text = _company.Name;
            cmbStatus.SelectedItem = _company.Status;
            cmbType.SelectedItem = _company.Type;
            lookupSubtype.EditValue = _company.SubtypeId;
            txtRiskRating.Value = _company.RiskRating;
            txtOwnership.Value = Convert.ToDecimal(_company.Ownership);
            lookupManager.EditValue = _company.ManagerId;
            lookupCompany.EditValue = _company.ParentId;

          /*  if (_company.ParentId != null)
            {
                treeParentCompany.NodesIterator.DoOperation(a =>
                                                                {
                                                                    if (a.Tag != null &&
                                                                        ((Company) a.Tag).
                                                                            Id ==
                                                                        _company.ParentId)
                                                                    {
                                                                        treeParentCompany.
                                                                            FocusedNode =
                                                                            a;
                                                                        return;
                                                                    }
                                                                });
            }
            else
            {
                treeParentCompany.FocusedNode = null;
            }*/
            txtCreationUser.Text = _company.Cruser;
            dtpCreationDate.DateTime = _company.Crd;
            txtUpdateUser.Text = _company.Chuser;
            dtpUpdateDate.DateTime = _company.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbType.SelectedItem == null) errorProvider.SetError(cmbType, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (lookupSubtype.EditValue == null) errorProvider.SetError(lookupSubtype, Strings.Field_is_mandatory);
            if (cmbType.EditValue != null && lookupCompany.EditValue != null)
          //  if (cmbType.EditValue != null && treeParentCompany.FocusedNode != null && treeParentCompany.FocusedNode.Tag != null)
            {

                if (((CompanyTypeEnum)lookupCompany.GetColumnValue("Type")) != (CompanyTypeEnum)cmbType.SelectedItem)
                    //treeParentCompany.FocusedNode.Tag).Type != (CompanyTypeEnum)cmbType.SelectedItem)
                {
                   errorProvider.SetError(cmbType, Strings.Parent_and_child_company_must_have_the_same_Type);
            }
            }
            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void CompanyAEVControl_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _company = new Company();
            }

            _company.Type = (CompanyTypeEnum) cmbType.SelectedItem;
            _company.SubtypeId = (long) lookupSubtype.EditValue;
            if (_company.SubtypeId == 0) _company.Subtype = lookupSubtype.GetSelectedDataRow() as CompanySubtype;
            _company.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _company.RiskRating = (int) txtRiskRating.Value;
            _company.Ownership = (long)txtOwnership.Value;
            _company.Name = txtName.Text.Trim();
            _company.ManagerId = (long?) lookupManager.EditValue;
            _company.ParentId = (long?) lookupCompany.EditValue;
           // _company.ParentId = (treeParentCompany.FocusedNode == null || treeParentCompany.FocusedNode.Tag == null)
           //                         ? (long?) null
           //                         : ((Company) treeParentCompany.FocusedNode.Tag).Id;

            BeginAEVCompany(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void CompanyAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void CompanyAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void treeParentCompany_KeyDown(object sender, KeyEventArgs e)
        {
     //       if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back) treeParentCompany.FocusedNode = null;
        }

        private void lookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)((LookUpEdit)sender).EditValue = null;

        }

        private void lookupCompany_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            { 
                ((LookUpEdit)sender).EditValue = null;
                txtOwnership.Value = 100;
            }

        }

        private void lookupSubtype_ProcessNewValue(object sender, ProcessNewValueEventArgs e)
        {
            if(String.IsNullOrEmpty(e.DisplayValue.ToString().Trim())) return;
            var companySubtype = new CompanySubtype {Name = e.DisplayValue.ToString().Trim(), Id = 0};
            (lookupSubtype.Properties.DataSource as List<CompanySubtype>).Add(companySubtype);
            e.Handled = true;
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVCompanyInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

                try
                {
                    SessionRegistry.Client.BeginAEVCompanyInitializationData(
                        EndAEVCompanyInitializationData, null);
                }
                catch (Exception)
                {
                    lock (_syncObject)
                    {
                        _dialogForm.Close();
                        _dialogForm = null;
                    }
                    SessionRegistry.ResetClientService();
                    XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void EndAEVCompanyInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVCompanyInitializationData;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Company> companies;
            List<Contact> managers;
            List<CompanySubtype> companySubtypes;

            try
            {
                result = SessionRegistry.Client.EndAEVCompanyInitializationData(out companies, out companySubtypes, out managers, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                Strings.
                                    There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                Strings.Freight_Metrics,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                //treeParentCompany.Tag = companies;

               // treeParentCompany.AppendNode(new object[] { "None" }, null, null);
              //  PopulateTree(null, companies.Where(a => a.ParentId == null).ToList(), ref companies);

                lookupCompany.Tag = companies;
                lookupCompany.Properties.DataSource = companies;
                lookupManager.Tag = managers;
                lookupManager.Properties.DataSource = managers;

                lookupSubtype.Tag = companySubtypes;
                lookupSubtype.Properties.DataSource = companySubtypes;

                if (_formActionType == FormActionTypeEnum.Edit|| _formActionType == FormActionTypeEnum.View) LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVCompany(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _company, EndAEVCompany, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVCompany(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVCompany;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Name_already_exists_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 2)
            {
                // Company is used as Company in a Bank Account
                errorProvider.SetError(cmbType, "The company is used as a 'Company' in a Bank Account entity. The company has to be of Type 'Internal'.");
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 3)
            {
                // Company is used as Bank in a Bank Account
                errorProvider.SetError(lookupSubtype, "The company is used as a 'Bank' in a Bank Account entity. Field 'Subtype' of the company must be of type 'Bank'.");
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 4)
            {
                // Company is associated with a Trader
                errorProvider.SetError(cmbType,
                                       "The company is associated with a Trader. The company has to be of Type 'Internal' or 'Affiliate'.");
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Company Company { get { return _company; } }

        public FormActionTypeEnum FormActionType{get { return _formActionType; }}

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

      }
}