﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors.Controls;
using System.Collections.Generic;

namespace Exis.WinClient.Controls
{
    public partial class LoanAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private Loan _loan;

        #endregion

        #region Constructors

        public LoanAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public LoanAEVForm(Loan loan, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _loan = loan;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);
            cmbStatus.SelectedItem = ActivationStatusEnum.Active;

            cmbBorrower.Properties.DisplayMember = "Name";
            cmbBorrower.Properties.ValueMember = "Id";
            cmbBorrower.Properties.NullValuePrompt = Strings.Select_a_Borrower___;

            lookupObligor.Properties.DisplayMember = "Name";
            lookupObligor.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Obligor_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupObligor.Properties.Columns.Add(col);
            lookupObligor.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupObligor.Properties.SearchMode = SearchMode.AutoFilter;
            lookupObligor.Properties.NullText = "";
            lookupObligor.Properties.NullValuePrompt = Strings.Select_an_Obligor___;

            lookupGuarantor.Properties.DisplayMember = "Name";
            lookupGuarantor.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Gurantor_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupGuarantor.Properties.Columns.Add(col);
            lookupGuarantor.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupGuarantor.Properties.SearchMode = SearchMode.AutoFilter;
            lookupGuarantor.Properties.NullValuePrompt = Strings.Select_a_Guarantor___;
            lookupGuarantor.Properties.NullText = "";

            lookupBank_1.Properties.DisplayMember = "Name";
            lookupBank_1.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Bank_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupBank_1.Properties.Columns.Add(col);
            lookupBank_1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupBank_1.Properties.SearchMode = SearchMode.AutoFilter;
            lookupBank_1.Properties.NullValuePrompt = Strings.Select_a_Bank___;
            lookupBank_1.Properties.NullText = "";

            lookupVessel_1.Properties.DisplayMember = "Name";
            lookupVessel_1.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Vessel_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupVessel_1.Properties.Columns.Add(col);
            lookupVessel_1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupVessel_1.Properties.SearchMode = SearchMode.AutoFilter;
            lookupVessel_1.Properties.NullText = "";
            lookupVessel_1.Properties.NullValuePrompt = Strings.Select_a_Vessel___;

            lookupCurrency.Properties.DisplayMember = "Name";
            lookupCurrency.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Currency_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupCurrency.Properties.Columns.Add(col);
            lookupCurrency.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCurrency.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCurrency.Properties.NullValuePrompt = Strings.Select_a_Currency___;
            lookupCurrency.Properties.NullText = "";

            cmbAssetType_1.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbAssetType_1.Properties.Items.Add(LoanCollateralTypeEnum.Cash);
            cmbAssetType_1.Properties.Items.Add(LoanCollateralTypeEnum.Vessel);
            cmbAssetType_1.Properties.Items.Add(LoanCollateralTypeEnum.Other);
            cmbAssetType_1.SelectedItem = LoanCollateralTypeEnum.Vessel;

            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
            cmbPrincipalFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            cmbPrincipalFrequency_1.SelectedItem = LoanRepaymentFrequencyEnum.Month;

            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
            cmbInterestFrequency_1.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            cmbInterestFrequency_1.SelectedItem = LoanRepaymentFrequencyEnum.Month;

            cmbInterestRateType_1.Properties.Items.Add(LoanInterestRateTypeEnum.Fixed);
            cmbInterestRateType_1.Properties.Items.Add(LoanInterestRateTypeEnum.Floating);
            cmbInterestRateType_1.SelectedItem = LoanInterestRateTypeEnum.Fixed;

            foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
            {
                cmbPeriodType_1.Properties.Items.Add(type);
            }
            
            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;

                btnAddAsset.Enabled = false;
                btnRemoveAsset.Enabled = false;
                btnAddBank.Enabled = false;
                btnRemoveBank.Enabled = false;
                btnAddTranch.Enabled = false;
                btnRemoveTranch.Enabled = false;

                txtCreationUser.Properties.ReadOnly = true;
                dtpCreationDate.Properties.ReadOnly = true;
                txtUpdateUser.Properties.ReadOnly = true;
                dtpUpdateDate.Properties.ReadOnly = true;
            }
        }

        private void LoadData()
        {
            lblId.Text = _loan.Id.ToString();
            txtCode.Text = _loan.ExternalCode;
            txtAdminFee.EditValue = _loan.AdminFee;
            txtAmount.EditValue = _loan.Amount;
            txtArrangeFee.EditValue = _loan.ArrangeFee;
            lookupCurrency.EditValue = _loan.CurrencyId;
            dtpSignDate.EditValue = _loan.SignDate;
            cmbStatus.SelectedItem = _loan.Status;
            lookupGuarantor.EditValue = _loan.GuarantorId;
            lookupObligor.EditValue = _loan.ObligorId;
            txtPrepayFee.EditValue = _loan.PrepayFee;
            txtVMC.EditValue = _loan.Vmc;
            chkRetentionAccount.EditValue = _loan.RetentionAccount;
            
            if(_formActionType == FormActionTypeEnum.View)
            {
                txtCode.Properties.ReadOnly = true;
                txtAdminFee.Properties.ReadOnly = true;
                txtAmount.Properties.ReadOnly = true;
                txtArrangeFee.Properties.ReadOnly = true;
                lookupCurrency.Properties.ReadOnly = true;
                dtpSignDate.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                lookupGuarantor.Properties.ReadOnly = true;
                lookupObligor.Properties.ReadOnly = true;
                txtPrepayFee.Properties.ReadOnly = true;
                txtVMC.Properties.ReadOnly = true;
                chkRetentionAccount.Properties.ReadOnly = true;
                cmbBorrower.Properties.ReadOnly = true;
            }

            string borrowersStr = String.Join(",", _loan.LoanBorrowers.Select(a => a.BorrowerId.ToString()));
            cmbBorrower.SetEditValue(borrowersStr);
            
            #region Load Banks

            lcBanks.BeginUpdate();
            lcgBanks.Clear();

            foreach (LoanBank loanBank in _loan.LoanBanks)
            {
                int counter = Convert.ToInt32(lcgBanks.Items.Count + 1);

                var lookupBank_new = new LookUpEdit();
                var chkIsArranger_new = new CheckEdit();
                var txtPercentage_new = new SpinEdit();
                // 
                // lookupBank
                // 
                lookupBank_new.Name = "lookupBank_" + counter;
                lookupBank_new.Properties.DisplayMember = "Name";
                lookupBank_new.Properties.ValueMember = "Id";
                var col = new LookUpColumnInfo
                {
                    Caption = Strings.Bank_Name,
                    FieldName = "Name",
                    Width = 0
                };
                lookupBank_new.Properties.Columns.Add(col);
                lookupBank_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                lookupBank_new.Properties.SearchMode = SearchMode.AutoFilter;
                lookupBank_new.Properties.NullValuePrompt = Strings.Select_a_Bank___;
                if (lookupBank_1.Tag != null)
                    lookupBank_new.Properties.DataSource = (List<Company>)lookupBank_1.Tag;
                lookupBank_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                // 
                // chkIsArranger
                // 
                chkIsArranger_new.EditValue = false;
                chkIsArranger_new.Name = "chkIsArranger_" + counter;
                chkIsArranger_new.Text = "";
                chkIsArranger_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                //
                //txtPercentage
                //
                txtPercentage_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
                txtPercentage_new.Name = "txtBankPercentage_" + counter;
                txtPercentage_new.Properties.EditMask = "P0";
                txtPercentage_new.Properties.MinValue = 1;
                txtPercentage_new.Properties.MaxValue = 100;
                txtPercentage_new.Properties.MaxLength = 3;
                txtPercentage_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtPercentage_new.Value = 0;
                txtPercentage_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                LayoutControlGroup lcgBank_new = lcgBanks.AddGroup();

                // 
                // lcgBank_new
                // 
                lcgBank_new.GroupBordersVisible = false;
                lcgBank_new.Name = "lcgBank_" + counter;
                lcgBank_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgBank_new.Tag = counter;
                lcgBank_new.Text = "lcgBank";
                lcgBank_new.TextVisible = false;
                lcgBank_new.DefaultLayoutType = LayoutType.Horizontal;
                // 
                // layoutBank_new
                // 
                LayoutControlItem layoutBank_new = lcgBank_new.AddItem();
                layoutBank_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutBank_new.Control = lookupBank_new;
                layoutBank_new.Name = "layoutBank_" + counter;
                layoutBank_new.Text = "Bank:";
                layoutBank_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutBank_new.MinSize = new Size(86, 24);
                layoutBank_new.MaxSize = new Size(0, 24);

                // 
                // layoutArranger_new
                // 
                LayoutControlItem layoutArranger_new = lcgBank_new.AddItem();
                layoutArranger_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutArranger_new.Control = chkIsArranger_new;
                layoutArranger_new.Name = "layoutArranger_" + counter;
                layoutArranger_new.Text = "Arranger:";
                layoutBank_new.MinSize = new Size(79, 24);
                layoutBank_new.MaxSize = new Size(0, 24);

                // 
                // layoutPercentage
                // 
                LayoutControlItem layoutPercentage_new = lcgBank_new.AddItem();
                layoutPercentage_new.Control = txtPercentage_new;
                layoutPercentage_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutPercentage_new.Name = "layoutBankPercentage_" + counter;
                layoutPercentage_new.Text = "Perc.:";

                lookupBank_new.EditValue = loanBank.BankId;
                chkIsArranger_new.EditValue = loanBank.IsArranger;
                txtPercentage_new.EditValue = loanBank.Percentage;

            }

            lcBanks.EndUpdate();
            lcBanks.BestFit();
            
            #endregion

            #region Loan Assets

            lcAssets.BeginUpdate();
            lcgAssets.Clear();

            foreach (LoanCollateralAsset asset in _loan.LoanCollateralAssets)
            {
                int counter = Convert.ToInt32(lcgAssets.Items.Count + 1);

                var cmbAssetType_new = new ComboBoxEdit();
                var lookupVessel_new = new LookUpEdit();
                var txtAsset_new = new TextEdit();
                var txtAmount_new = new SpinEdit();

                //Asset Type
                cmbAssetType_new.Name = "cmbAssetType_" + counter;
                cmbAssetType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbAssetType_new.Properties.Items.Add(LoanCollateralTypeEnum.Cash);
                cmbAssetType_new.Properties.Items.Add(LoanCollateralTypeEnum.Vessel);
                cmbAssetType_new.Properties.Items.Add(LoanCollateralTypeEnum.Other);
                cmbAssetType_new.SelectedItem = LoanCollateralTypeEnum.Vessel;
                cmbAssetType_new.SelectedIndexChanged += new EventHandler(CmbAssetTypeSelectedIndexChanged);
                cmbAssetType_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                // 
                // lookupVessel
                // 
                lookupVessel_new.Name = "lookupVessel_" + counter;
                lookupVessel_new.Properties.DisplayMember = "Name";
                lookupVessel_new.Properties.ValueMember = "Id";
                var col = new LookUpColumnInfo
                              {
                                  Caption = Strings.Vessel_Name,
                                  FieldName = "Name",
                                  Width = 0
                              };
                lookupVessel_new.Properties.Columns.Add(col);
                lookupVessel_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                lookupVessel_new.Properties.SearchMode = SearchMode.AutoFilter;
                lookupVessel_new.Properties.NullValuePrompt = Strings.Select_a_Vessel___;
                lookupVessel_new.Properties.NullText = "";
                if (lookupVessel_1.Tag != null)
                    lookupVessel_new.Properties.DataSource = (List<Vessel>) lookupVessel_1.Tag;
                lookupVessel_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                // 
                // txtAsset
                // 
                txtAsset_new.Name = "txtAsset_" + counter;
                txtAsset_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                // 
                // txtAmount
                // 
                txtAmount_new.Name = "txtAssetAmount_" + counter;
                txtAmount_new.Properties.EditMask = "D";
                txtAmount_new.Properties.MinValue = 1;
                txtAmount_new.Properties.MaxValue = 999999999;
                txtAmount_new.Properties.MaxLength = 9;
                txtAmount_new.Value = 0;
                txtAmount_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                LayoutControlGroup lcgAsset_new = lcgAssets.AddGroup();

                // 
                // layoutAsset
                // 
                lcgAsset_new.GroupBordersVisible = false;
                lcgAsset_new.Name = "lcgAsset_" + counter;
                lcgAsset_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgAsset_new.Tag = counter;
                lcgAsset_new.Text = "lcgAsset_";
                lcgAsset_new.TextVisible = false;
                lcgAsset_new.DefaultLayoutType = LayoutType.Horizontal;

                // 
                // layoutAssetType
                // 
                LayoutControlItem layoutAssetType_new = lcgAsset_new.AddItem();
                layoutAssetType_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutAssetType_new.Control = cmbAssetType_new;
                layoutAssetType_new.Name = "layoutAssetType_" + counter;
                layoutAssetType_new.Text = "Type:";
                layoutAssetType_new.MinSize = new Size(109, 24);
                layoutAssetType_new.MaxSize = new Size(109, 24);
                layoutAssetType_new.SizeConstraintsType = SizeConstraintsType.Custom;

                // 
                // layoutVessel
                // 
                LayoutControlItem layoutVessel_new = lcgAsset_new.AddItem();
                layoutVessel_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutVessel_new.Control = lookupVessel_new;
                layoutVessel_new.Name = "layoutVessel_" + counter;
                layoutVessel_new.Text = "Vessel:";

                // 
                // layoutAsset
                // 
                LayoutControlItem layoutAsset_new = lcgAsset_new.AddItem();
                layoutAsset_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutAsset_new.Control = txtAsset_new;
                layoutAsset_new.Name = "layoutAsset_" + counter;
                layoutAsset_new.Text = "Asset:";

                // 
                // layoutAmount
                // 
                LayoutControlItem layoutAmount_new = lcgAsset_new.AddItem();
                layoutAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutAmount_new.Control = txtAmount_new;
                layoutAmount_new.Name = "layoutAssetAmount_" + counter;
                layoutAmount_new.Text = "Amount:";

                cmbAssetType_new.EditValue = asset.Type;
                lookupVessel_new.EditValue = asset.VesselId;
                txtAsset_new.EditValue = asset.Asset;
                txtAmount_new.EditValue = asset.Amount;
            }
            lcAssets.EndUpdate();
            lcAssets.BestFit();


            #endregion

            #region Loan Tranches

            lcTranches.BeginUpdate();
            lcgTranches.Clear();

            foreach(LoanTranche loanTranche in _loan.LoanTranches)
            {
                int counter = Convert.ToInt32(lcgTranches.Items.Count + 1);

                #region Controls

                // 
                // txtAmount
                // 
                var txtTranchAmount_new = new SpinEdit();
                txtTranchAmount_new.Name = "txtTranchAmount_" + counter;
                txtTranchAmount_new.Properties.EditMask = "D";
                txtTranchAmount_new.Properties.MinValue = 1;
                txtTranchAmount_new.Properties.MaxValue = 999999999999;
                txtTranchAmount_new.Properties.MaxLength = 12;
                txtTranchAmount_new.Value = 0;
                txtTranchAmount_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                // 
                // txtBalloonAmount
                // 
                var txtBalloonAmount_new = new SpinEdit();
                txtBalloonAmount_new.Name = "txtBalloonAmount_" + counter;
                txtBalloonAmount_new.Properties.EditMask = "D";
                txtBalloonAmount_new.Properties.MinValue = 1;
                txtBalloonAmount_new.Properties.MaxValue = 999999999;
                txtBalloonAmount_new.Properties.MaxLength = 9;
                txtBalloonAmount_new.Value = 0;
                txtBalloonAmount_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                //Principal Frequency
                var cmbPrincipalFrequency_new = new ComboBoxEdit();
                cmbPrincipalFrequency_new.Name = "cmbPrincipalFrequency_" + counter;
                cmbPrincipalFrequency_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
                cmbPrincipalFrequency_new.SelectedItem = LoanRepaymentFrequencyEnum.Month;
                //cmbPrincipalFrequency_new.SelectedIndexChanged += CmbPrincipalFrequencySelectedIndexChanged;
                cmbPrincipalFrequency_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.Add ? false : true;

                //Interest Frequency
                var cmbInterestFrequency_new = new ComboBoxEdit();
                cmbInterestFrequency_new.Name = "cmbInterestFrequency_" + counter;
                cmbInterestFrequency_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
                cmbInterestFrequency_new.SelectedItem = LoanRepaymentFrequencyEnum.Month;
                cmbInterestFrequency_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.Add ? false : true;

                // 
                // txtRepayPeriod
                // 
                var txtRepayPeriod_new = new SpinEdit();
                txtRepayPeriod_new.Name = "txtRepayPeriod_" + counter;
                txtRepayPeriod_new.Properties.EditMask = "D";
                txtRepayPeriod_new.Properties.MinValue = 1;
                txtRepayPeriod_new.Properties.MaxValue = 99;
                txtRepayPeriod_new.Properties.MaxLength = 2;
                txtRepayPeriod_new.Value = 0;
                txtRepayPeriod_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.Add ? false : true;

                //
                //cmbInterestRateType
                //
                var cmbInterestRateType_new = new ComboBoxEdit();
                cmbInterestRateType_new.Name = "cmbInterestRateType_" + counter;
                cmbInterestRateType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbInterestRateType_new.Properties.Items.Add(LoanInterestRateTypeEnum.Fixed);
                cmbInterestRateType_new.Properties.Items.Add(LoanInterestRateTypeEnum.Floating);
                cmbInterestRateType_new.SelectedItem = LoanInterestRateTypeEnum.Fixed;
                cmbInterestRateType_new.SelectedIndexChanged += CmbInterestRateTypeSelectedIndexChanged;
                cmbInterestRateType_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.Add ? false : true;

                //
                //txtInterestRate
                //
                var txtInterestRate_new = new SpinEdit();
                txtInterestRate_new.Name = "txtInterestRate_" + counter;
                txtInterestRate_new.Properties.EditMask = "f6";
                txtInterestRate_new.Properties.MaxLength = 13;
                txtInterestRate_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtInterestRate_new.Value = 0;
                txtInterestRate_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                //
                //cmbPeriodType
                //
                var cmbPeriodType_new = new ComboBoxEdit();
                cmbPeriodType_new.Name = "cmbPeriodType_" + counter;
                cmbPeriodType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbPeriodType_new.Properties.Items.Add("");
                foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
                {
                    cmbPeriodType_new.Properties.Items.Add(type);
                }
                cmbPeriodType_new.SelectedItem = InterestReferencePeriodTypeEnum.Week1;
                cmbPeriodType_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.Add ? false : true;

                //
                //txtInterestCorFactor
                //
                var txtInterestCorFactor_new = new SpinEdit();
                txtInterestCorFactor_new.Name = "txtInterestCorFactor_" + counter;
                txtInterestCorFactor_new.Properties.EditMask = "f4";
                txtInterestCorFactor_new.Properties.MaxLength = 8;
                txtInterestCorFactor_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtInterestCorFactor_new.Value = 0;
                txtInterestCorFactor_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;
                
                //
                //txtCommitmentFee
                //
                var txtCommitmentFee_new = new SpinEdit();
                txtCommitmentFee_new.Name = "txtCommitmentFee_" + counter;
                txtCommitmentFee_new.Properties.EditMask = "f4";
                txtCommitmentFee_new.Properties.MaxLength = 8;
                txtCommitmentFee_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtCommitmentFee_new.Value = 0;
                txtCommitmentFee_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                //FirstInstallmentDate
                var dtpFirstInstallmentDate_new = new DateEdit();
                dtpFirstInstallmentDate_new.Name = "dtpFirstInstallmentDate_" + counter;
                dtpFirstInstallmentDate_new.EditValue = null;
                dtpFirstInstallmentDate_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                #endregion

                #region Layout Items

                LayoutControlGroup lcgTranchInfo_new = lcgTranches.AddGroup();
                lcgTranchInfo_new.GroupBordersVisible = true;
                lcgTranchInfo_new.Name = "lcgTranchInfo_" + counter;
                lcgTranchInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfo_new.Tag = counter;
                lcgTranchInfo_new.Text = "Tranche " + counter;
                lcgTranchInfo_new.TextVisible = true;
                lcgTranchInfo_new.DefaultLayoutType = LayoutType.Vertical;
                lcgTranchInfo_new.AppearanceGroup.Font = new Font("Tahoma", lcgTranchInfo_new.AppearanceGroup.Font.Size, FontStyle.Bold);
                lcgTranchInfo_new.ExpandButtonVisible = true;

                LayoutControlGroup lcgTranchMainInfo_new = lcgTranchInfo_new.AddGroup();
                lcgTranchMainInfo_new.GroupBordersVisible = false;
                lcgTranchMainInfo_new.Name = "lcgTranchMainInfo_" + counter;
                lcgTranchMainInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchMainInfo_new.Tag = loanTranche.Id;
                lcgTranchMainInfo_new.Text = "lcgTranchMainInfo_" + counter;
                lcgTranchMainInfo_new.TextVisible = false;
                lcgTranchMainInfo_new.DefaultLayoutType = LayoutType.Vertical;

                LayoutControlGroup lcgTranchInfoPart1_new = lcgTranchMainInfo_new.AddGroup();
                lcgTranchInfoPart1_new.GroupBordersVisible = false;
                lcgTranchInfoPart1_new.Name = "lcgTranchMainInfoPart1_" + counter;
                lcgTranchInfoPart1_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfoPart1_new.Tag = counter;
                lcgTranchInfoPart1_new.Text = "lcgTranchMainInfoPart1_" + counter;
                lcgTranchInfoPart1_new.TextVisible = false;
                lcgTranchInfoPart1_new.DefaultLayoutType = LayoutType.Horizontal;

                LayoutControlGroup lcgTranchInfoPart2_new = lcgTranchMainInfo_new.AddGroup();
                lcgTranchInfoPart2_new.GroupBordersVisible = false;
                lcgTranchInfoPart2_new.Name = "lcgTranchMainInfoPart2_" + counter;
                lcgTranchInfoPart2_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfoPart2_new.Tag = counter;
                lcgTranchInfoPart2_new.Text = "lcgTranchMainInfoPart2_" + counter;
                lcgTranchInfoPart2_new.TextVisible = false;
                lcgTranchInfoPart2_new.DefaultLayoutType = LayoutType.Horizontal;

                LayoutControlGroup lcgTranchInfoPart3_new = lcgTranchMainInfo_new.AddGroup();
                lcgTranchInfoPart3_new.GroupBordersVisible = false;
                lcgTranchInfoPart3_new.Name = "lcgTranchMainInfoPart3_" + counter;
                lcgTranchInfoPart3_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgTranchInfoPart3_new.Tag = counter;
                lcgTranchInfoPart3_new.Text = "lcgTranchMainInfoPart3_" + counter;
                lcgTranchInfoPart3_new.TextVisible = false;
                lcgTranchInfoPart3_new.DefaultLayoutType = LayoutType.Horizontal;

                #region Layout Items in Group Tranch Info

                // 
                // layoutTranchAmount
                // 
                LayoutControlItem layoutAmount_new = lcgTranchInfoPart1_new.AddItem();
                layoutAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutAmount_new.Control = txtTranchAmount_new;
                layoutAmount_new.Name = "layoutTranchAmount_" + counter;
                layoutAmount_new.Text = "Tranche Amount:";

                // 
                // layoutBalloonAmount
                // 
                LayoutControlItem layoutBalloonAmount_new = lcgTranchInfoPart1_new.AddItem();
                layoutBalloonAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutBalloonAmount_new.Control = txtBalloonAmount_new;
                layoutBalloonAmount_new.Name = "layoutBalloonAmount_" + counter;
                layoutBalloonAmount_new.Text = "Balloon Amount:";

                //
                //layoutPrincipalFrequency
                //
                LayoutControlItem layoutPrincipalFrequency_new = lcgTranchInfoPart1_new.AddItem();
                layoutPrincipalFrequency_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutPrincipalFrequency_new.Control = cmbPrincipalFrequency_new;
                layoutPrincipalFrequency_new.Name = "layoutPrincipalFrequency_" + counter;
                layoutPrincipalFrequency_new.Text = "Principal Frequency:";

                //
                //layoutInterestFrequency
                //
                LayoutControlItem layoutInterestFrequency_new = lcgTranchInfoPart1_new.AddItem();
                layoutInterestFrequency_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestFrequency_new.Control = cmbInterestFrequency_new;
                layoutInterestFrequency_new.Name = "layoutInterestFrequency_" + counter;
                layoutInterestFrequency_new.Text = "Interest Frequency:";

                //
                //layoutRepayPeriod
                //
                LayoutControlItem layoutItemRepayPeriod_new = lcgTranchInfoPart1_new.AddItem();
                layoutItemRepayPeriod_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutItemRepayPeriod_new.Control = txtRepayPeriod_new;
                layoutItemRepayPeriod_new.Name = "layoutRepayPeriod_" + counter;
                layoutItemRepayPeriod_new.Text = "Repay Period:";

                //
                //layoutInterestRateType_
                //
                LayoutControlItem layoutInterestRateType_new = lcgTranchInfoPart2_new.AddItem();
                layoutInterestRateType_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestRateType_new.Control = cmbInterestRateType_new;
                layoutInterestRateType_new.Name = "layoutInterestRateType_" + counter;
                layoutInterestRateType_new.Text = "Interest Rate Type:";

                //
                //layoutInterestRate_
                //
                LayoutControlItem layoutInterestRate_new = lcgTranchInfoPart2_new.AddItem();
                layoutInterestRate_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestRate_new.Control = txtInterestRate_new;
                layoutInterestRate_new.Name = "layoutInterestRate_" + counter;
                layoutInterestRate_new.Text = "Interest Rate:";

                //
                //layoutPeriodType_
                //
                LayoutControlItem layoutPeriodType_new = lcgTranchInfoPart2_new.AddItem();
                layoutPeriodType_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutPeriodType_new.Control = cmbPeriodType_new;
                layoutPeriodType_new.Name = "layoutPeriodType_" + counter;
                layoutPeriodType_new.Text = "Period Type:";

                //
                //layoutInterestCorFactor_
                //
                LayoutControlItem layoutInterestCorFactor_new = lcgTranchInfoPart2_new.AddItem();
                layoutInterestCorFactor_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutInterestCorFactor_new.Control = txtInterestCorFactor_new;
                layoutInterestCorFactor_new.Name = "layoutInterestCorFactor_" + counter;
                layoutInterestCorFactor_new.Text = "Correction Factor:";

                //
                //layoutCommitmentFee
                //
                LayoutControlItem layoutCommitmentFee_new = lcgTranchInfoPart2_new.AddItem();
                layoutCommitmentFee_new.TextAlignMode = TextAlignModeItem.AutoSize;
                layoutCommitmentFee_new.Control = txtCommitmentFee_new;
                layoutCommitmentFee_new.Name = "layoutCommitmentFee_" + counter;
                layoutCommitmentFee_new.Text = "Commitment Fee:";

                LayoutControlItem layoutFirstInstallmentDate_new = lcgTranchInfoPart3_new.AddItem();
                layoutFirstInstallmentDate_new.Control = dtpFirstInstallmentDate_new;
                layoutFirstInstallmentDate_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutFirstInstallmentDate_new.ControlMaxSize =
                    layoutFirstInstallmentDate_new.ControlMinSize = new Size(150, 20);
                layoutFirstInstallmentDate_new.Name = "layoutFirstInstallmentDate_" + counter;
                layoutFirstInstallmentDate_new.Text = "First Installment Date:";

                LayoutControlItem emptySpaceItem = lcgTranchInfoPart3_new.AddItem(new EmptySpaceItem());

                #endregion

                //
                //btnAddDrawdown_
                //
                var btnAddDrawdown_new = new SimpleButton();
                btnAddDrawdown_new.ImageIndex = 0;
                btnAddDrawdown_new.ImageList = this.imageList1;
                btnAddDrawdown_new.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
                btnAddDrawdown_new.MaximumSize = new System.Drawing.Size(30, 30);
                btnAddDrawdown_new.MinimumSize = new System.Drawing.Size(30, 30);
                btnAddDrawdown_new.Name = "btnAddDrawdown_" + counter;
                btnAddDrawdown_new.Size = new System.Drawing.Size(30, 30);
                btnAddDrawdown_new.Click += new System.EventHandler(this.BtnAddDrawdownClick);
                btnAddDrawdown_new.Enabled = _formActionType == FormActionTypeEnum.View ? false : true;
                //
                //btnRemoveDrawdown_
                //
                var btnRemoveDrawdown_new = new SimpleButton();
                btnRemoveDrawdown_new.ImageIndex = 1;
                btnRemoveDrawdown_new.ImageList = this.imageList1;
                btnRemoveDrawdown_new.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
                btnRemoveDrawdown_new.MaximumSize = new System.Drawing.Size(30, 30);
                btnRemoveDrawdown_new.MinimumSize = new System.Drawing.Size(30, 30);
                btnRemoveDrawdown_new.Name = "btnRemoveDrawdown_" + counter;
                btnRemoveDrawdown_new.Size = new System.Drawing.Size(30, 30);
                btnRemoveDrawdown_new.Click += new EventHandler(this.BtnRemoveDrawdownClick);
                btnRemoveDrawdown_new.Enabled = _formActionType == FormActionTypeEnum.View ? false : true;

                LayoutControlGroup lcgDrawDowns_new = lcgTranchInfo_new.AddGroup();
                lcgDrawDowns_new.GroupBordersVisible = true;
                lcgDrawDowns_new.Name = "lcgDrawdowns_" + counter;
                lcgDrawDowns_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgDrawDowns_new.Tag = counter;
                lcgDrawDowns_new.Text = "Draw-downs";
                lcgDrawDowns_new.TextVisible = true;
                lcgDrawDowns_new.DefaultLayoutType = LayoutType.Horizontal;
                lcgDrawDowns_new.AppearanceGroup.Font = new Font("Tahoma",
                                                                 lcgDrawDowns_new.AppearanceGroup.Font.Size,
                                                                 FontStyle.Italic);
                lcgDrawDowns_new.ExpandButtonVisible = true;

                LayoutControlGroup lcgDrawDownsInfo_new = lcgDrawDowns_new.AddGroup();
                lcgDrawDownsInfo_new.GroupBordersVisible = false;
                lcgDrawDownsInfo_new.Name = "lcgDrawDownsInfo_" + counter;
                lcgDrawDownsInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgDrawDownsInfo_new.Tag = counter;
                lcgDrawDownsInfo_new.Text = "lcgDrawDownsInfo_" + counter;
                lcgDrawDownsInfo_new.TextVisible = false;
                lcgDrawDownsInfo_new.DefaultLayoutType = LayoutType.Vertical;

                LayoutControlGroup lcgButtons_new = lcgDrawDowns_new.AddGroup();
                lcgButtons_new.GroupBordersVisible = false;
                lcgButtons_new.Name = "lcgButtons_" + counter;
                lcgButtons_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                lcgButtons_new.Tag = counter;
                lcgButtons_new.Text = "lcgButtons_" + counter;
                lcgButtons_new.TextVisible = false;
                lcgButtons_new.DefaultLayoutType = LayoutType.Horizontal;

                int count = 1;
                foreach (LoanDrawdown loanDrawdown in loanTranche.LoanDrawdowns)
                {
                    LayoutControlGroup lcgDrawDown_new = lcgDrawDownsInfo_new.AddGroup();
                    lcgDrawDown_new.GroupBordersVisible = false;
                    lcgDrawDown_new.Name = "lcgDrawdown" + counter + "_" + count;
                    lcgDrawDown_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                    lcgDrawDown_new.Tag = counter;
                    lcgDrawDown_new.Text = "lcgDrawdown" + counter + "_" + count;
                    lcgDrawDown_new.TextVisible = false;
                    lcgDrawDown_new.DefaultLayoutType = LayoutType.Horizontal;

                    #region Controls 
                    //
                    //dtpDrawdownDate
                    //
                    var dtpDrawdownDate_new = new DateEdit();
                    dtpDrawdownDate_new.EditValue = null;
                    dtpDrawdownDate_new.Name = "dtpDrawdownDate" + counter + "_" + count;
                    dtpDrawdownDate_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                    //
                    //txtDrawdownAmount_
                    //
                    var txtDrawdownAmount_new = new SpinEdit();
                    txtDrawdownAmount_new.Name = "txtDrawdownAmount" + counter + "_" + count;
                    txtDrawdownAmount_new.Properties.EditMask = "D";
                    txtDrawdownAmount_new.Properties.MinValue = 1;
                    txtDrawdownAmount_new.Properties.MaxValue = 999999999999;
                    txtDrawdownAmount_new.Properties.MaxLength = 12;
                    txtDrawdownAmount_new.Value = 0;
                    txtDrawdownAmount_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

                    #endregion

                    #region Layout Items in Group Draw-Downs

                    //
                    //layoutDrawdownDate_
                    //
                    LayoutControlItem layoutDrawdownDate_new = lcgDrawDown_new.AddItem();
                    layoutDrawdownDate_new.TextAlignMode = TextAlignModeItem.AutoSize;
                    layoutDrawdownDate_new.Control = dtpDrawdownDate_new;
                    layoutDrawdownDate_new.MaxSize = new Size(183, 24);
                    layoutDrawdownDate_new.MinSize = new Size(183, 24);
                    layoutDrawdownDate_new.SizeConstraintsType = SizeConstraintsType.Custom;
                    layoutDrawdownDate_new.Name = "layoutDrawdownDate" + counter + "_" + count; 
                    layoutDrawdownDate_new.Text = "Draw-down date:";

                    //
                    //layoutDrawdownAmount_
                    //
                    LayoutControlItem layoutDrawdownAmount_new = lcgDrawDown_new.AddItem();
                    layoutDrawdownAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
                    layoutDrawdownAmount_new.Control = txtDrawdownAmount_new;
                    layoutDrawdownAmount_new.Name = "layoutDrawdownAmount" + counter + "_" + count;
                    layoutDrawdownAmount_new.Text = "Draw-down amount:";
                    
                    dtpDrawdownDate_new.EditValue = loanDrawdown.Date;
                    txtDrawdownAmount_new.EditValue = loanDrawdown.Amount;

                    #endregion

                    count++;
                }

                //
                // layoutAddDrawdown_
                //
                LayoutControlItem layoutAddDrawdown_new = lcgButtons_new.AddItem();
                layoutAddDrawdown_new.Control = btnAddDrawdown_new;
                layoutAddDrawdown_new.Name = "layoutAddDrawdown_" + counter;
                layoutAddDrawdown_new.TextVisible = false;

                //
                // layoutRemoveDrawdown_
                //
                LayoutControlItem layoutRemoveDrawdown_new = lcgButtons_new.AddItem();
                layoutRemoveDrawdown_new.Control = btnRemoveDrawdown_new;
                layoutRemoveDrawdown_new.Name = "layoutRemoveDrawdown_" + counter;
                layoutRemoveDrawdown_new.TextVisible = false;

                lcgDrawDowns_new.AddItem(new EmptySpaceItem());

                #endregion

                txtTranchAmount_new.EditValue = loanTranche.Amount;
                txtBalloonAmount_new.EditValue = loanTranche.BalloonAmount;
                cmbPrincipalFrequency_new.EditValue = loanTranche.PrincipalFrequency;
                cmbInterestFrequency_new.EditValue = loanTranche.InterestFrequency;
                txtRepayPeriod_new.EditValue = loanTranche.RepayPeriod;
                txtInterestRate_new.EditValue = loanTranche.InterestRate;
                cmbPeriodType_new.EditValue = loanTranche.PeriodType;
                txtInterestCorFactor_new.EditValue = loanTranche.InterestCorFactor;
                cmbInterestRateType_new.EditValue = loanTranche.InterestRateType;
                txtCommitmentFee_new.EditValue = loanTranche.CommitmentFee;
                cmbPeriodType_new.EditValue = loanTranche.PeriodType;
                dtpFirstInstallmentDate_new.EditValue = loanTranche.FirstInstallmentDate;
            }

            lcTranches.EndUpdate();
            lcTranches.BestFit();

            #endregion

            txtCreationUser.Text = _loan.Cruser;
            dtpCreationDate.EditValue = _loan.Crd;
            txtUpdateUser.EditValue = _loan.Chuser;
            dtpUpdateDate.EditValue = _loan.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtCode.Text.Trim()))
                errorProvider.SetError(txtCode, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (lookupObligor.EditValue == null) errorProvider.SetError(lookupObligor, Strings.Field_is_mandatory);
            if (lookupCurrency.EditValue == null) errorProvider.SetError(lookupCurrency, Strings.Field_is_mandatory);
            if (dtpSignDate.EditValue == null) errorProvider.SetError(dtpSignDate, Strings.Field_is_mandatory);
            if (txtAmount.EditValue == null) errorProvider.SetError(txtAmount, Strings.Field_is_mandatory);
            if (txtVMC.EditValue == null) errorProvider.SetError(txtVMC, Strings.Field_is_mandatory);

            int arrangerCount = 0;
            decimal percentangeSum = 0;
            foreach (object item in lcgBanks.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutBank = (LayoutControlGroup) item;
                    if (layoutBank.Name.StartsWith("lcgBank_"))
                    {
                        string layoutBankSuffix = layoutBank.Name.Substring(layoutBank.Name.IndexOf("_") + 1);
                        var lookupBank =
                            (LookUpEdit)
                            ((LayoutControlItem) layoutBank.Items.FindByName("layoutBank_" + layoutBankSuffix)).Control;
                        var chkArranger =
                            (CheckEdit)
                            ((LayoutControlItem) layoutBank.Items.FindByName("layoutArranger_" + layoutBankSuffix)).
                                Control;
                        var txtPercentage =
                            (SpinEdit)
                            ((LayoutControlItem) layoutBank.Items.FindByName("layoutBankPercentage_" + layoutBankSuffix))
                                .Control;

                        if (lookupBank.EditValue == null)
                            errorProvider.SetError(lookupBank, Strings.Field_is_mandatory);
                        if ((bool) chkArranger.EditValue)
                            arrangerCount++;
                        percentangeSum = percentangeSum + (decimal) txtPercentage.EditValue;
                    }
                }
            }
            if (arrangerCount == 0)
            {
                foreach (object item in lcgBanks.Items)
                {
                    if (item.GetType() == typeof (LayoutControlGroup))
                    {
                        var layoutBank = (LayoutControlGroup) item;
                        if (layoutBank.Name.StartsWith("lcgBank_"))
                        {
                            string layoutBankSuffix = layoutBank.Name.Substring(layoutBank.Name.IndexOf("_") + 1);
                            var chkArranger =
                            (CheckEdit)
                            ((LayoutControlItem)layoutBank.Items.FindByName("layoutArranger_" + layoutBankSuffix)).
                                Control;
                            errorProvider.SetError(chkArranger, Strings.At_least__one_bank_should_be_declared_as_the_arranger_);
                        }
                    }
                }
            }
            if (percentangeSum != 100)
            {
                foreach (object item in lcgBanks.Items)
                {
                    if (item.GetType() == typeof(LayoutControlGroup))
                    {
                        var layoutBank = (LayoutControlGroup)item;
                        if (layoutBank.Name.StartsWith("lcgBank_"))
                        {
                            string layoutBankSuffix = layoutBank.Name.Substring(layoutBank.Name.IndexOf("_") + 1);
                            var txtPercentage =
                            (SpinEdit)
                            ((LayoutControlItem)layoutBank.Items.FindByName("layoutBankPercentage_" + layoutBankSuffix))
                                .Control;
                            errorProvider.SetError(txtPercentage, Strings.The_sum_of__the_percentages_should_be_equal_to_100__);
                        }
                    }
                }
            }

            foreach (object item in lcgAssets.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutAsset = (LayoutControlGroup) item;
                    if (layoutAsset.Name.StartsWith("lcgAsset_"))
                    {
                        string layoutBankSuffix = layoutAsset.Name.Substring(layoutAsset.Name.IndexOf("_") + 1);

                        var assetType =
                            (ComboBoxEdit)
                            ((LayoutControlItem) layoutAsset.Items.FindByName("layoutAssetType_" + layoutBankSuffix)).
                                Control;
                        var lookupVessel =
                            (LookUpEdit)
                            ((LayoutControlItem) layoutAsset.Items.FindByName("layoutVessel_" + layoutBankSuffix)).
                                Control;
                        var txtAssetAmount =
                            (SpinEdit)
                            ((LayoutControlItem) layoutAsset.Items.FindByName("layoutAssetAmount_" + layoutBankSuffix)).
                                Control;

                        if (assetType.EditValue != null &&
                            (LoanCollateralTypeEnum) assetType.SelectedItem == LoanCollateralTypeEnum.Vessel &&
                            lookupVessel.EditValue == null)
                            errorProvider.SetError(lookupVessel, Strings.Field_is_mandatory);

                        if (assetType.EditValue != null &&
                            (LoanCollateralTypeEnum) assetType.SelectedItem == LoanCollateralTypeEnum.Cash &&
                            txtAssetAmount.EditValue == null)
                            errorProvider.SetError(txtAssetAmount, Strings.Field_is_mandatory);
                    }
                }
            }

            int sumOfTranches = 0;
            int loanAmount = Convert.ToInt32(txtAmount.EditValue);
            foreach (object item in lcgTranches.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutTrancheInfo = (LayoutControlGroup) item;
                    if (layoutTrancheInfo.Name.StartsWith("lcgTranchInfo_"))
                    {
                        string layoutTranchSuffix =
                            layoutTrancheInfo.Name.Substring(layoutTrancheInfo.Name.IndexOf("_") + 1);

                        var layoutTranchMainInfo =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgTranchMainInfo_" + layoutTranchSuffix);

                        var layoutTranchMainPart1Info =
                            (LayoutControlGroup)
                            layoutTranchMainInfo.Items.FindByName("lcgTranchMainInfoPart1_" + layoutTranchSuffix);
                        var layoutTranchMainPart2Info =
                            (LayoutControlGroup)
                            layoutTranchMainInfo.Items.FindByName("lcgTranchMainInfoPart2_" + layoutTranchSuffix);

                        var txtTranchAmount =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutTranchAmount_" + layoutTranchSuffix))
                                .Control;
                        var txtTranchBalloonAmount =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutBalloonAmount_" + layoutTranchSuffix))
                                .Control;
                        var cmbPrincipalFrequency =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutPrincipalFrequency_" +
                                                                        layoutTranchSuffix)).Control;
                        var cmbInterestFrequency =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutInterestFrequency_" +
                                                                        layoutTranchSuffix)).Control;
                        var cmbInterestRateType =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutInterestRateType_" +
                                                                        layoutTranchSuffix)).Control;
                        var txtRepayPeriod =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutRepayPeriod_" + layoutTranchSuffix)).
                                Control;
                        var txtInterestRate =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutInterestRate_" + layoutTranchSuffix))
                                .Control;
                        
                        if(txtTranchAmount.EditValue == null) errorProvider.SetError(txtTranchAmount, Strings.Field_is_mandatory);
                        if (txtTranchBalloonAmount.EditValue == null) errorProvider.SetError(txtTranchBalloonAmount, Strings.Field_is_mandatory);
                        if (cmbPrincipalFrequency.EditValue == null) errorProvider.SetError(cmbPrincipalFrequency, Strings.Field_is_mandatory);
                        if (cmbInterestFrequency.EditValue == null) errorProvider.SetError(cmbInterestFrequency, Strings.Field_is_mandatory);
                        if (txtRepayPeriod.EditValue == null || Convert.ToInt32(txtRepayPeriod.EditValue) == 0) errorProvider.SetError(txtRepayPeriod, Strings.Field_is_mandatory);
                        if (cmbInterestRateType.EditValue == null) errorProvider.SetError(cmbInterestRateType, Strings.Field_is_mandatory);
                        if (txtInterestRate.EditValue == null) errorProvider.SetError(txtInterestRate, Strings.Field_is_mandatory);

                        sumOfTranches = sumOfTranches + Convert.ToInt32(txtTranchAmount.EditValue);

                        int sumOfDrawdows = 0;
                        int trancheAmount = Convert.ToInt32(txtTranchAmount.EditValue);
                        var layoutTranchDrawdowns =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgDrawdowns_" + layoutTranchSuffix);
                        var layoutTranchDrawDownInfos =
                            (LayoutControlGroup)
                            layoutTranchDrawdowns.Items.FindByName("lcgDrawDownsInfo_" + layoutTranchSuffix);

                        foreach (object item2 in layoutTranchDrawDownInfos.Items)
                        {
                            if (item2.GetType() == typeof (LayoutControlGroup))
                            {
                                var drawDown = (LayoutControlGroup) item2;

                                string layoutDrawDownSuffix =
                                    drawDown.Name.Substring(drawDown.Name.IndexOf("_") + 1);

                                var dtpDrawdownDate =
                                    (DateEdit)
                                    ((LayoutControlItem)
                                     drawDown.Items.FindByName("layoutDrawdownDate" + layoutTranchSuffix + "_" +
                                                               layoutDrawDownSuffix)).Control;

                                var txtDrawdownAmount =
                                    (SpinEdit)
                                    ((LayoutControlItem)
                                     drawDown.Items.FindByName("layoutDrawdownAmount" + layoutTranchSuffix + "_" +
                                                               layoutDrawDownSuffix)).Control;

                                if (dtpDrawdownDate.EditValue == null)
                                    errorProvider.SetError(dtpDrawdownDate, Strings.Field_is_mandatory);

                                sumOfDrawdows = sumOfDrawdows + Convert.ToInt32(txtDrawdownAmount.EditValue);
                            }
                        }
                        if (sumOfDrawdows != trancheAmount)
                            errorProvider.SetError(txtTranchAmount, Strings.The_sum_of_draw_dows_amounts_should_be_equal_to_tranche_amount_);
                    }
                }
            }
            if (sumOfTranches != loanAmount)
                errorProvider.SetError(txtAmount, Strings.The_sum_of_tranche_amounts_should_be_equal_to_loan_amount_);

            return !errorProvider.HasErrors;
        }

        private void RemoveItemsFromGroup(LayoutControl layoutControl, LayoutControlGroup group)
        {
            foreach (BaseLayoutItem layoutControlItem in group.Items)
            {
                if (layoutControlItem.GetType() == typeof(LayoutControlItem))
                    layoutControl.Controls.Remove(((LayoutControlItem)layoutControlItem).Control);
                else if (layoutControlItem.GetType() == typeof(LayoutControlGroup))
                {
                    RemoveItemsFromGroup(layoutControl, (LayoutControlGroup)layoutControlItem);
                }
            }
        }

        #endregion

        #region GUI Events

        private void LoanAevFormLoad(object sender, EventArgs e)
        {
            BeginAevLoanInitializationData();
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _loan = new Loan();
            }

            _loan.Vmc = Convert.ToDecimal(txtVMC.EditValue);
            _loan.RetentionAccount =(bool)chkRetentionAccount.EditValue;
            _loan.ExternalCode = txtCode.Text.Trim();
            _loan.AdminFee = String.IsNullOrEmpty(txtAdminFee.Text) ? null : (int?)txtAdminFee.Value;
            _loan.Amount = (long)txtAmount.Value;
            _loan.ArrangeFee = String.IsNullOrEmpty(txtArrangeFee.Text) ? null : (int?)txtArrangeFee.Value;
            _loan.CurrencyId = (long)lookupCurrency.EditValue;
            _loan.SignDate = ((DateTime)dtpSignDate.EditValue).Date;
            _loan.Status = (ActivationStatusEnum)cmbStatus.SelectedItem;
            _loan.GuarantorId = lookupGuarantor.EditValue == null ? null : (long?) lookupGuarantor.EditValue;
            _loan.ObligorId = (long) lookupObligor.EditValue;
            _loan.PrepayFee = String.IsNullOrEmpty(txtPrepayFee.Text) ? null : (decimal?) txtPrepayFee.Value;

            List<string> borrowerIds = cmbBorrower.EditValue.ToString().Split(',').ToList();
            var loanBorrowers = borrowerIds.Select(borrowerId => new LoanBorrower()
                                                                     {
                                                                         BorrowerId = Convert.ToInt32(borrowerId)
                                                                     }).ToList();

            _loan.LoanBorrowers = loanBorrowers;

            #region Banks

            var loanBanks = new List<LoanBank>();
            foreach (object item in lcgBanks.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBank = (LayoutControlGroup)item;
                    if (layoutBank.Name.StartsWith("lcgBank_"))
                    {
                        string layoutBankSuffix = layoutBank.Name.Substring(layoutBank.Name.IndexOf("_") + 1);
                        var lookupBank = (LookUpEdit)((LayoutControlItem)layoutBank.Items.FindByName("layoutBank_" + layoutBankSuffix)).Control;
                        var chkArranger = (CheckEdit)((LayoutControlItem)layoutBank.Items.FindByName("layoutArranger_" + layoutBankSuffix)).Control;
                        var txtPercentage =
                            (SpinEdit) ((LayoutControlItem) layoutBank.Items.FindByName("layoutBankPercentage_" + layoutBankSuffix)).Control;

                        var loanBank = new LoanBank()
                                           {
                                               BankId = (long) lookupBank.EditValue,
                                               IsArranger = (bool) chkArranger.EditValue,
                                               Percentage = (decimal) txtPercentage.EditValue
                                           };
                        loanBanks.Add(loanBank);
                    }
                }
            }
            #endregion

            #region Collateral Assets

            var loanCollateralAssets = new List<LoanCollateralAsset>();
            foreach (object item in lcgAssets.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutAsset = (LayoutControlGroup)item;
                    if (layoutAsset.Name.StartsWith("lcgAsset_"))
                    {
                        string layoutBankSuffix = layoutAsset.Name.Substring(layoutAsset.Name.IndexOf("_") + 1);

                        var assetType = (ComboBoxEdit)((LayoutControlItem)layoutAsset.Items.FindByName("layoutAssetType_" + layoutBankSuffix)).Control;
                        var lookupVessel = (LookUpEdit)((LayoutControlItem)layoutAsset.Items.FindByName("layoutVessel_" + layoutBankSuffix)).Control;
                        var txtAsset = (TextEdit)((LayoutControlItem)layoutAsset.Items.FindByName("layoutAsset_" + layoutBankSuffix)).Control;
                        var txtAssetAmount =
                            (SpinEdit)((LayoutControlItem)layoutAsset.Items.FindByName("layoutAssetAmount_" + layoutBankSuffix)).Control;

                        var loanCollateralAsset = new LoanCollateralAsset()
                                                      {
                                                          Type = (LoanCollateralTypeEnum) assetType.EditValue,
                                                          Amount =
                                                              txtAssetAmount.EditValue == null
                                                                  ? (int?) null
                                                                  : Convert.ToInt32(txtAssetAmount.EditValue),
                                                          Asset =
                                                              string.IsNullOrEmpty(txtAsset.Text) ? null : txtAsset.Text,
                                                          VesselId =
                                                              lookupVessel.EditValue == null
                                                                  ? (long?) null
                                                                  : Convert.ToInt64(lookupVessel.EditValue)
                                                      };
                        loanCollateralAssets.Add(loanCollateralAsset);
                    }
                }
            }
            #endregion

            #region Tranches

            var loanTranches = new List<LoanTranche>();
            foreach (object item in lcgTranches.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutTrancheInfo = (LayoutControlGroup)item;
                    if (layoutTrancheInfo.Name.StartsWith("lcgTranchInfo_"))
                    {
                        var loanDrawdowns = new List<LoanDrawdown>();

                        string layoutTranchSuffix =
                            layoutTrancheInfo.Name.Substring(layoutTrancheInfo.Name.IndexOf("_") + 1);

                        var layoutTranchMainInfo =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgTranchMainInfo_" + layoutTranchSuffix);

                        long trancheId = Convert.ToInt64(layoutTranchMainInfo.Tag);

                        var layoutTranchMainPart1Info =
                            (LayoutControlGroup)
                            layoutTranchMainInfo.Items.FindByName("lcgTranchMainInfoPart1_" + layoutTranchSuffix);
                        var layoutTranchMainPart2Info =
                            (LayoutControlGroup)
                            layoutTranchMainInfo.Items.FindByName("lcgTranchMainInfoPart2_" + layoutTranchSuffix);
                        var layoutTranchMainPart3Info =
                            (LayoutControlGroup)
                            layoutTranchMainInfo.Items.FindByName("lcgTranchMainInfoPart3_" + layoutTranchSuffix);

                        var txtTranchAmount =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutTranchAmount_" + layoutTranchSuffix))
                                .Control;
                        var txtTranchBalloonAmount =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutBalloonAmount_" + layoutTranchSuffix))
                                .Control;
                        var cmbPrincipalFrequency =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutPrincipalFrequency_" +
                                                                        layoutTranchSuffix)).Control;
                        var cmbInterestFrequency =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutInterestFrequency_" +
                                                                        layoutTranchSuffix)).Control;
                        var cmbInterestRateType =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutInterestRateType_" +
                                                                        layoutTranchSuffix)).Control;
                        var txtRepayPeriod =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart1Info.Items.FindByName("layoutRepayPeriod_" + layoutTranchSuffix)).
                                Control;
                        var txtInterestRate =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutInterestRate_" + layoutTranchSuffix))
                                .Control;
                        var cmbPeriodType =
                            (ComboBoxEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutPeriodType_" +
                                                                        layoutTranchSuffix)).Control;
                        var txtInterestCorFactor =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutInterestCorFactor_" +
                                                                        layoutTranchSuffix)).Control;

                        var txtCommitmentFee =
                            (SpinEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart2Info.Items.FindByName("layoutCommitmentFee_" +
                                                                        layoutTranchSuffix)).Control;

                        var dtpFirstInstallmentDate =
                            (DateEdit)
                            ((LayoutControlItem)
                             layoutTranchMainPart3Info.Items.FindByName("layoutFirstInstallmentDate_" +
                                                                        layoutTranchSuffix)).Control;

                        var loanTranche = new LoanTranche()
                                              {
                                                  Id = trancheId,
                                                  LoanId = _formActionType == FormActionTypeEnum.Add ? 0 : _loan.Id,
                                                  Amount = Convert.ToInt64(txtTranchAmount.EditValue),
                                                  BalloonAmount = Convert.ToInt32(txtTranchBalloonAmount.EditValue),
                                                  InterestCorFactor =
                                                      txtInterestCorFactor.EditValue == null
                                                          ? (decimal?) null
                                                          : Convert.ToDecimal(txtInterestCorFactor.EditValue),
                                                  InterestFrequency =
                                                      (LoanRepaymentFrequencyEnum) cmbInterestFrequency.EditValue,
                                                  InterestRate = Convert.ToDecimal(txtInterestRate.EditValue),
                                                  InterestRateType =
                                                      (LoanInterestRateTypeEnum) cmbInterestRateType.EditValue,
                                                  PeriodType =
                                                      cmbPeriodType.EditValue == null
                                                          ? (InterestReferencePeriodTypeEnum?) null
                                                          : (InterestReferencePeriodTypeEnum) cmbPeriodType.EditValue,
                                                  PrincipalFrequency =
                                                      (LoanRepaymentFrequencyEnum) cmbPrincipalFrequency.EditValue,
                                                  RepayPeriod = Convert.ToInt32(txtRepayPeriod.EditValue),
                                                  CommitmentFee = txtCommitmentFee.EditValue == null
                                                                      ? (decimal?) null
                                                                      : Convert.ToDecimal(txtCommitmentFee.EditValue),
                                                  FirstInstallmentDate =
                                                      dtpFirstInstallmentDate.EditValue == null
                                                          ? (DateTime?) null
                                                          : dtpFirstInstallmentDate.DateTime.Date
                                              };
                        
                        var layoutTranchDrawdowns =
                            (LayoutControlGroup)
                            layoutTrancheInfo.Items.FindByName("lcgDrawdowns_" + layoutTranchSuffix);
                        var layoutTranchDrawDownInfos =
                            (LayoutControlGroup)
                            layoutTranchDrawdowns.Items.FindByName("lcgDrawDownsInfo_" + layoutTranchSuffix);

                        foreach (object item2 in layoutTranchDrawDownInfos.Items)
                        {
                            if (item2.GetType() == typeof (LayoutControlGroup))
                            {
                                var drawDown = (LayoutControlGroup) item2;

                                string layoutDrawDownSuffix =
                                    drawDown.Name.Substring(drawDown.Name.IndexOf("_") + 1);

                                var dtpDrawDate =
                                    (DateEdit)
                                    ((LayoutControlItem)
                                     drawDown.Items.FindByName("layoutDrawdownDate" + layoutTranchSuffix + "_" +
                                                               layoutDrawDownSuffix)).Control;
                                var txtDrawdownAmount =
                                    (SpinEdit)
                                    ((LayoutControlItem)
                                     drawDown.Items.FindByName("layoutDrawdownAmount" + layoutTranchSuffix + "_" +
                                                               layoutDrawDownSuffix)).Control;
                                var loanDrawDown = new LoanDrawdown()
                                                       {
                                                           Amount = Convert.ToInt32(txtDrawdownAmount.EditValue),
                                                           Date = ((DateTime) dtpDrawDate.EditValue).Date
                                                       };

                                loanDrawdowns.Add(loanDrawDown);
                            }
                        }
                        loanTranche.LoanDrawdowns = loanDrawdowns;
                        loanTranches.Add(loanTranche);
                    }
                }
            }

            #endregion

            //Calculate Installments only for new tranches
            {
                #region Principal Installments
                
                foreach (LoanTranche loanTranche in loanTranches.Where(a=>a.Id == 0))
                {
                    var loanPrincipalInstallments = new List<LoanPrincipalInstallment>();

                    DateTime lastDrawdownDate = loanTranche.LoanDrawdowns.OrderByDescending(a => a.Date).First().Date;
                    long trancheAmount = loanTranche.Amount;
                    LoanRepaymentFrequencyEnum principalFrequency = loanTranche.PrincipalFrequency;
                    int repayPeriod = loanTranche.RepayPeriod;
                    int balloonAmount = loanTranche.BalloonAmount;
                    DateTime currentDate = loanTranche.FirstInstallmentDate == null
                                               ? lastDrawdownDate
                                               : loanTranche.FirstInstallmentDate.Value;

                    long amountPerInstallment;
                    if (principalFrequency == LoanRepaymentFrequencyEnum.Month)
                    {
                        int period = (12*repayPeriod);
                        amountPerInstallment = (trancheAmount - balloonAmount)/period;
                        for (int i = 0; i < period; i++)
                        {
                            if (i + 1 == period)
                                amountPerInstallment = amountPerInstallment + balloonAmount;
                            currentDate = currentDate.AddMonths(1);
                            var loanPrincipalInstallment = new LoanPrincipalInstallment()
                                                               {
                                                                   Date = currentDate,
                                                                   Amount = amountPerInstallment
                                                               };
                            loanPrincipalInstallments.Add(loanPrincipalInstallment);
                        }
                    }
                    else if (principalFrequency == LoanRepaymentFrequencyEnum.Quarter)
                    {
                        int period = (4*repayPeriod);
                        amountPerInstallment = (trancheAmount - balloonAmount)/period;
                        for (int i = 0; i < period; i++)
                        {
                            if (i + 1 == period)
                                amountPerInstallment = amountPerInstallment + balloonAmount;
                            currentDate = currentDate.AddMonths(3);
                            var loanPrincipalInstallment = new LoanPrincipalInstallment()
                                                               {
                                                                   Date = currentDate,
                                                                   Amount = amountPerInstallment
                                                               };
                            loanPrincipalInstallments.Add(loanPrincipalInstallment);
                        }
                    }
                    else if (principalFrequency == LoanRepaymentFrequencyEnum.Semester)
                    {
                        int period = (2*repayPeriod);
                        amountPerInstallment = (trancheAmount - balloonAmount)/period;
                        for (int i = 0; i < period; i++)
                        {
                            if (i + 1 == period)
                                amountPerInstallment = amountPerInstallment + balloonAmount;
                            currentDate = currentDate.AddMonths(6);
                            var loanPrincipalInstallment = new LoanPrincipalInstallment()
                                                               {
                                                                   Date = currentDate,
                                                                   Amount = amountPerInstallment
                                                               };
                            loanPrincipalInstallments.Add(loanPrincipalInstallment);
                        }
                    }
                    else if (principalFrequency == LoanRepaymentFrequencyEnum.Year)
                    {
                        int period = repayPeriod;
                        amountPerInstallment = (trancheAmount - balloonAmount)/period;
                        for (int i = 0; i < period; i++)
                        {
                            currentDate = currentDate.AddYears(1);
                            if (i + 1 == period)
                                amountPerInstallment = amountPerInstallment + balloonAmount;
                            var loanPrincipalInstallment = new LoanPrincipalInstallment()
                                                               {
                                                                   Date = currentDate,
                                                                   Amount = amountPerInstallment
                                                               };
                            loanPrincipalInstallments.Add(loanPrincipalInstallment);
                        }
                    }
                    loanTranche.LoanPrincipalInstallments = loanPrincipalInstallments;
                }

                #endregion

                #region Interest Installments

                foreach (LoanTranche loanTranche in loanTranches.Where(a => a.Id == 0))
                {
                    var loanInterestInstallments = new List<LoanInterestInstallment>();

                    DateTime lastDrawdownDate = loanTranche.LoanDrawdowns.OrderByDescending(a => a.Date).First().Date;
                    LoanRepaymentFrequencyEnum interestFrequency = loanTranche.InterestFrequency;
                    InterestReferencePeriodTypeEnum? periodType = loanTranche.PeriodType;
                    LoanInterestRateTypeEnum interestType = loanTranche.InterestRateType;
                    decimal interestRate = loanTranche.InterestRate;
                    int repayPeriod = loanTranche.RepayPeriod;

                    DateTime currentDate = loanTranche.FirstInstallmentDate == null
                                               ? lastDrawdownDate
                                               : loanTranche.FirstInstallmentDate.Value;

                    if (interestFrequency == LoanRepaymentFrequencyEnum.Month)
                    {
                        int period = (12*repayPeriod);
                        for (int i = 0; i < period; i++)
                        {
                            currentDate = currentDate.AddMonths(1);
                            var loanInterestInstallment = new LoanInterestInstallment()
                                                              {
                                                                  Amount = null,
                                                                  Date = currentDate,
                                                                  InterestRate = interestRate,
                                                                  InterestRateType = interestType,
                                                                  PeriodType = 
                                                                      interestType == LoanInterestRateTypeEnum.Fixed
                                                                          ? null
                                                                          : periodType
                                                              };

                            loanInterestInstallments.Add(loanInterestInstallment);
                        }
                    }
                    else if (interestFrequency == LoanRepaymentFrequencyEnum.Quarter)
                    {
                        int period = (4*repayPeriod);
                        for (int i = 0; i < period; i++)
                        {
                            currentDate = currentDate.AddMonths(3);
                            var loanInterestInstallment = new LoanInterestInstallment()
                                                              {
                                                                  Amount = null,
                                                                  Date = currentDate,
                                                                  InterestRate = interestRate,
                                                                  InterestRateType = interestType,
                                                                  PeriodType = 
                                                                      interestType == LoanInterestRateTypeEnum.Fixed
                                                                          ? null
                                                                          : periodType
                                                              };
                            loanInterestInstallments.Add(loanInterestInstallment);
                        }
                    }
                    else if (interestFrequency == LoanRepaymentFrequencyEnum.Semester)
                    {
                        int period = (2*repayPeriod);
                        for (int i = 0; i < period; i++)
                        {
                            currentDate = currentDate.AddMonths(6);
                            var loanInterestInstallment = new LoanInterestInstallment()
                                                              {
                                                                  Amount = null,
                                                                  Date = currentDate,
                                                                  InterestRate = interestRate,
                                                                  InterestRateType = interestType,
                                                                  PeriodType =
                                                                      interestType == LoanInterestRateTypeEnum.Fixed
                                                                          ? null
                                                                          : periodType
                                                              };
                            loanInterestInstallments.Add(loanInterestInstallment);
                        }
                    }
                    else if (interestFrequency == LoanRepaymentFrequencyEnum.Year)
                    {
                        int period = repayPeriod;
                        for (int i = 0; i < period; i++)
                        {
                            currentDate = currentDate.AddYears(1);
                            var loanInterestInstallment = new LoanInterestInstallment()
                                                              {
                                                                  Amount = null,
                                                                  Date = currentDate,
                                                                  InterestRate = interestRate,
                                                                  InterestRateType = interestType,
                                                                  PeriodType =
                                                                      interestType == LoanInterestRateTypeEnum.Fixed
                                                                          ? null
                                                                          : periodType
                                                              };
                            loanInterestInstallments.Add(loanInterestInstallment);
                        }
                    }
                    loanTranche.LoanInterestInstallments = loanInterestInstallments;
                }

                #endregion
            }

            _loan.LoanBanks = loanBanks;
            _loan.LoanCollateralAssets = loanCollateralAssets;
            _loan.LoanTranches = loanTranches;

            BeginAevLoan(_formActionType != FormActionTypeEnum.Add);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void LoanAevFormActivated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void LoanAevFormDeactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void BtnAddTranchClick(object sender, EventArgs e)
        {
            int counter = Convert.ToInt32(lcgTranches.Items[lcgTranches.Items.Count - 1].Tag) + 1;

            #region Controls

            // 
            // txtAmount
            // 
            var txtTranchAmount_new = new SpinEdit();
            txtTranchAmount_new.Name = "txtTranchAmount_" + counter;
            txtTranchAmount_new.Properties.EditMask = "D";
            txtTranchAmount_new.Properties.MinValue = 1;
            txtTranchAmount_new.Properties.MaxValue = 999999999999;
            txtTranchAmount_new.Properties.MaxLength = 12;
            txtTranchAmount_new.Value = 0;

            // 
            // txtBalloonAmount
            // 
            var txtBalloonAmount_new = new SpinEdit();
            txtBalloonAmount_new.Name = "txtBalloonAmount_" + counter;
            txtBalloonAmount_new.Properties.EditMask = "D";
            txtBalloonAmount_new.Properties.MinValue = 1;
            txtBalloonAmount_new.Properties.MaxValue = 999999999;
            txtBalloonAmount_new.Properties.MaxLength = 9;
            txtBalloonAmount_new.Value = 0;

            //Principal Frequency
            var cmbPrincipalFrequency_new = new ComboBoxEdit();
            cmbPrincipalFrequency_new.Name = "cmbPrincipalFrequency_" + counter;
            cmbPrincipalFrequency_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
            cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
            cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
            cmbPrincipalFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            cmbPrincipalFrequency_new.SelectedItem = LoanRepaymentFrequencyEnum.Month;
            //cmbPrincipalFrequency_new.SelectedIndexChanged += CmbPrincipalFrequencySelectedIndexChanged;

            //Interest Frequency
            var cmbInterestFrequency_new = new ComboBoxEdit();
            cmbInterestFrequency_new.Name = "cmbInterestFrequency_" + counter;
            cmbInterestFrequency_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
            cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
            cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
            cmbInterestFrequency_new.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            cmbInterestFrequency_new.SelectedItem = LoanRepaymentFrequencyEnum.Month;

            // 
            // txtRepayPeriod
            // 
            var txtRepayPeriod_new = new SpinEdit();
            txtRepayPeriod_new.Name = "txtRepayPeriod_" + counter;
            txtRepayPeriod_new.Properties.EditMask = "D";
            txtRepayPeriod_new.Properties.MinValue = 1;
            txtRepayPeriod_new.Properties.MaxValue = 99;
            txtRepayPeriod_new.Properties.MaxLength = 2;
            txtRepayPeriod_new.Value = 0;

            //
            //cmbInterestRateType
            //
            var cmbInterestRateType_new = new ComboBoxEdit();
            cmbInterestRateType_new.Name = "cmbInterestRateType_" + counter;
            cmbInterestRateType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbInterestRateType_new.Properties.Items.Add(LoanInterestRateTypeEnum.Fixed);
            cmbInterestRateType_new.Properties.Items.Add(LoanInterestRateTypeEnum.Floating);
            cmbInterestRateType_new.SelectedItem = LoanInterestRateTypeEnum.Fixed;
            cmbInterestRateType_new.SelectedIndexChanged += CmbInterestRateTypeSelectedIndexChanged;

            //
            //txtInterestRate
            //
            var txtInterestRate_new = new SpinEdit();
            txtInterestRate_new.Name = "txtInterestRate_" + counter;
            txtInterestRate_new.Properties.EditMask = "f6";
            txtInterestRate_new.Properties.MaxLength = 13;
            txtInterestRate_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtInterestRate_new.Value = 0;

            //
            //cmbPeriodType
            //
            var cmbPeriodType_new = new ComboBoxEdit();
            cmbPeriodType_new.Name = "cmbPeriodType_" + counter;
            cmbPeriodType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            foreach (InterestReferencePeriodTypeEnum type in Enum.GetValues(typeof(InterestReferencePeriodTypeEnum)))
            {
                cmbPeriodType_new.Properties.Items.Add(type);
            }
            cmbPeriodType_new.SelectedItem = InterestReferencePeriodTypeEnum.Week1;
            cmbPeriodType_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View ? true : false;

            //
            //txtInterestCorFactor
            //
            var txtInterestCorFactor_new = new SpinEdit();
            txtInterestCorFactor_new.Name = "txtInterestCorFactor_" + counter;
            txtInterestCorFactor_new.Properties.EditMask = "f4";
            txtInterestCorFactor_new.Properties.MaxLength = 8;
            txtInterestCorFactor_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtInterestCorFactor_new.Value = 0;

            //
            //txtCommitmentFee
            //
            var txtCommitmentFee_new = new SpinEdit();
            txtCommitmentFee_new.Name = "txtCommitmentFee_" + counter;
            txtCommitmentFee_new.Properties.EditMask = "f4";
            txtCommitmentFee_new.Properties.MaxLength = 8;
            txtCommitmentFee_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtCommitmentFee_new.Value = 0;

            //FirstInstallmentDate
            var dtpFirstInstallmentDate_new = new DateEdit();
            dtpFirstInstallmentDate_new.Name = "dtpFirstInstallmentDate_" + counter;
            dtpFirstInstallmentDate_new.EditValue = null;

            //
            //dtpDrawdownDate
            //
            var dtpDrawdownDate_new = new DateEdit();
            dtpDrawdownDate_new.EditValue = null;
            dtpDrawdownDate_new.Name = "dtpDrawdownDate" + counter + "_1";

            //
            //txtDrawdownAmount_
            //
            var txtDrawdownAmount_new = new SpinEdit();
            txtDrawdownAmount_new.Name = "txtDrawdownAmount" + counter + "_1";
            txtDrawdownAmount_new.Properties.EditMask = "D";
            txtDrawdownAmount_new.Properties.MinValue = 1;
            txtDrawdownAmount_new.Properties.MaxValue = 999999999999;
            txtDrawdownAmount_new.Properties.MaxLength = 12;
            txtDrawdownAmount_new.Value = 0;

            //
            //btnAddDrawdown_
            //
            var btnAddDrawdown_new = new SimpleButton();
            btnAddDrawdown_new.ImageIndex = 0;
            btnAddDrawdown_new.ImageList = this.imageList1;
            btnAddDrawdown_new.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            btnAddDrawdown_new.MaximumSize = new System.Drawing.Size(30, 30);
            btnAddDrawdown_new.MinimumSize = new System.Drawing.Size(30, 30);
            btnAddDrawdown_new.Name = "btnAddDrawdown_" + counter;
            btnAddDrawdown_new.Size = new System.Drawing.Size(30, 30);
            btnAddDrawdown_new.Click += new System.EventHandler(this.BtnAddDrawdownClick);

            //
            //btnRemoveDrawdown_
            //
            var btnRemoveDrawdown_new = new SimpleButton();
            btnRemoveDrawdown_new.ImageIndex = 1;
            btnRemoveDrawdown_new.ImageList = this.imageList1;
            btnRemoveDrawdown_new.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            btnRemoveDrawdown_new.MaximumSize = new System.Drawing.Size(30, 30);
            btnRemoveDrawdown_new.MinimumSize = new System.Drawing.Size(30, 30);
            btnRemoveDrawdown_new.Name = "btnRemoveDrawdown_" + counter;
            btnRemoveDrawdown_new.Size = new System.Drawing.Size(30, 30);
            btnRemoveDrawdown_new.Click += new EventHandler(this.BtnRemoveDrawdownClick);

            #endregion

            #region Layout Items

            lcTranches.BeginUpdate();

            LayoutControlGroup lcgTranchInfo_new = lcgTranches.AddGroup();
            lcgTranchInfo_new.GroupBordersVisible = true;
            lcgTranchInfo_new.Name = "lcgTranchInfo_" + counter;
            lcgTranchInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgTranchInfo_new.Tag = counter;
            lcgTranchInfo_new.Text = "Tranche " + counter;
            lcgTranchInfo_new.TextVisible = true;
            lcgTranchInfo_new.DefaultLayoutType = LayoutType.Vertical;
            lcgTranchInfo_new.AppearanceGroup.Font = new Font("Tahoma", lcgTranchInfo_new.AppearanceGroup.Font.Size, FontStyle.Bold);
            lcgTranchInfo_new.ExpandButtonVisible = true;

            LayoutControlGroup lcgTranchMainInfo_new = lcgTranchInfo_new.AddGroup();
            lcgTranchMainInfo_new.GroupBordersVisible = false;
            lcgTranchMainInfo_new.Name = "lcgTranchMainInfo_" + counter;
            lcgTranchMainInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgTranchMainInfo_new.Tag = 0;
            lcgTranchMainInfo_new.Text = "lcgTranchMainInfo_" + counter;
            lcgTranchMainInfo_new.TextVisible = false;
            lcgTranchMainInfo_new.DefaultLayoutType = LayoutType.Vertical;

            LayoutControlGroup lcgTranchInfoPart1_new = lcgTranchMainInfo_new.AddGroup();
            lcgTranchInfoPart1_new.GroupBordersVisible = false;
            lcgTranchInfoPart1_new.Name = "lcgTranchMainInfoPart1_" + counter;
            lcgTranchInfoPart1_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgTranchInfoPart1_new.Tag = counter;
            lcgTranchInfoPart1_new.Text = "lcgTranchMainInfoPart1_" + counter;
            lcgTranchInfoPart1_new.TextVisible = false;
            lcgTranchInfoPart1_new.DefaultLayoutType = LayoutType.Horizontal;

            LayoutControlGroup lcgTranchInfoPart2_new = lcgTranchMainInfo_new.AddGroup();
            lcgTranchInfoPart2_new.GroupBordersVisible = false;
            lcgTranchInfoPart2_new.Name = "lcgTranchMainInfoPart2_" + counter;
            lcgTranchInfoPart2_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgTranchInfoPart2_new.Tag = counter;
            lcgTranchInfoPart2_new.Text = "lcgTranchMainInfoPart2_" + counter;
            lcgTranchInfoPart2_new.TextVisible = false;
            lcgTranchInfoPart2_new.DefaultLayoutType = LayoutType.Horizontal;

            LayoutControlGroup lcgTranchInfoPart3_new = lcgTranchMainInfo_new.AddGroup();
            lcgTranchInfoPart3_new.GroupBordersVisible = false;
            lcgTranchInfoPart3_new.Name = "lcgTranchMainInfoPart3_" + counter;
            lcgTranchInfoPart3_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgTranchInfoPart3_new.Tag = counter;
            lcgTranchInfoPart3_new.Text = "lcgTranchMainInfoPart3_" + counter;
            lcgTranchInfoPart3_new.TextVisible = false;
            lcgTranchInfoPart3_new.DefaultLayoutType = LayoutType.Horizontal;

            #region Layout Items in Group Tranch Info

            // 
            // layoutTranchAmount
            // 
            LayoutControlItem layoutAmount_new = lcgTranchInfoPart1_new.AddItem();
            layoutAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutAmount_new.Control = txtTranchAmount_new;
            layoutAmount_new.Name = "layoutTranchAmount_" + counter;
            layoutAmount_new.Text = "Tranche Amount:";

            // 
            // layoutBalloonAmount
            // 
            LayoutControlItem layoutBalloonAmount_new = lcgTranchInfoPart1_new.AddItem();
            layoutBalloonAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutBalloonAmount_new.Control = txtBalloonAmount_new;
            layoutBalloonAmount_new.Name = "layoutBalloonAmount_" + counter;
            layoutBalloonAmount_new.Text = "Balloon Amount:";

            //
            //layoutPrincipalFrequency
            //
            LayoutControlItem layoutPrincipalFrequency_new = lcgTranchInfoPart1_new.AddItem();
            layoutPrincipalFrequency_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutPrincipalFrequency_new.Control = cmbPrincipalFrequency_new;
            layoutPrincipalFrequency_new.Name = "layoutPrincipalFrequency_" + counter;
            layoutPrincipalFrequency_new.Text = "Principal Frequency:";

            //
            //layoutInterestFrequency
            //
            LayoutControlItem layoutInterestFrequency_new = lcgTranchInfoPart1_new.AddItem();
            layoutInterestFrequency_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutInterestFrequency_new.Control = cmbInterestFrequency_new;
            layoutInterestFrequency_new.Name = "layoutInterestFrequency_" + counter;
            layoutInterestFrequency_new.Text = "Interest Frequency:";

            //
            //layoutRepayPeriod
            //
            LayoutControlItem layoutItemRepayPeriod_new = lcgTranchInfoPart1_new.AddItem();
            layoutItemRepayPeriod_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutItemRepayPeriod_new.Control = txtRepayPeriod_new;
            layoutItemRepayPeriod_new.Name = "layoutRepayPeriod_" + counter;
            layoutItemRepayPeriod_new.Text = "Repay Period:";

            //
            //layoutInterestRateType_
            //
            LayoutControlItem layoutInterestRateType_new = lcgTranchInfoPart2_new.AddItem();
            layoutInterestRateType_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutInterestRateType_new.Control = cmbInterestRateType_new;
            layoutInterestRateType_new.Name = "layoutInterestRateType_" + counter;
            layoutInterestRateType_new.Text = "Interest Rate Type:";

            //
            //layoutInterestRate_
            //
            LayoutControlItem layoutInterestRate_new = lcgTranchInfoPart2_new.AddItem();
            layoutInterestRate_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutInterestRate_new.Control = txtInterestRate_new;
            layoutInterestRate_new.Name = "layoutInterestRate_" + counter;
            layoutInterestRate_new.Text = "Interest Rate:";

            //
            //layoutPeriodType_new
            //
            LayoutControlItem layoutPeriodType_new = lcgTranchInfoPart2_new.AddItem();
            layoutPeriodType_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutPeriodType_new.Control = cmbPeriodType_new;
            layoutPeriodType_new.Name = "layoutPeriodType_" + counter;
            layoutPeriodType_new.Text = "Period Type:";

            //
            //layoutInterestCorFactor_
            //
            LayoutControlItem layoutInterestCorFactor_new = lcgTranchInfoPart2_new.AddItem();
            layoutInterestCorFactor_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutInterestCorFactor_new.Control = txtInterestCorFactor_new;
            layoutInterestCorFactor_new.Name = "layoutInterestCorFactor_" + counter;
            layoutInterestCorFactor_new.Text = "Correction Factor:";

            //
            //layoutCommitmentFee
            //
            LayoutControlItem layoutCommitmentFee_new = lcgTranchInfoPart2_new.AddItem();
            layoutCommitmentFee_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutCommitmentFee_new.Control = txtCommitmentFee_new;
            layoutCommitmentFee_new.Name = "layoutCommitmentFee_" + counter;
            layoutCommitmentFee_new.Text = "Commitment Fee:";

            LayoutControlItem layoutFirstInstallmentDate_new = lcgTranchInfoPart3_new.AddItem();
            layoutFirstInstallmentDate_new.Control = dtpFirstInstallmentDate_new;
            layoutFirstInstallmentDate_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutFirstInstallmentDate_new.ControlMaxSize =
                layoutFirstInstallmentDate_new.ControlMinSize = new Size(150, 20);
            layoutFirstInstallmentDate_new.Name = "layoutFirstInstallmentDate_" + counter;
            layoutFirstInstallmentDate_new.Text = "First Installment Date:";

            lcgTranchInfoPart3_new.AddItem(new EmptySpaceItem());

            #endregion

            LayoutControlGroup lcgDrawDowns_new = lcgTranchInfo_new.AddGroup();
            lcgDrawDowns_new.GroupBordersVisible = true;
            lcgDrawDowns_new.Name = "lcgDrawdowns_" + counter;
            lcgDrawDowns_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgDrawDowns_new.Tag = counter;
            lcgDrawDowns_new.Text = "Draw-downs";
            lcgDrawDowns_new.TextVisible = true;
            lcgDrawDowns_new.DefaultLayoutType = LayoutType.Horizontal;
            lcgDrawDowns_new.AppearanceGroup.Font = new Font("Tahoma", lcgDrawDowns_new.AppearanceGroup.Font.Size, FontStyle.Italic);
            lcgDrawDowns_new.ExpandButtonVisible = true;

            LayoutControlGroup lcgDrawDownsInfo_new = lcgDrawDowns_new.AddGroup();
            lcgDrawDownsInfo_new.GroupBordersVisible = false;
            lcgDrawDownsInfo_new.Name = "lcgDrawDownsInfo_" + counter;
            lcgDrawDownsInfo_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgDrawDownsInfo_new.Tag = counter;
            lcgDrawDownsInfo_new.Text = "lcgDrawDownsInfo_" + counter;
            lcgDrawDownsInfo_new.TextVisible = false;
            lcgDrawDownsInfo_new.DefaultLayoutType = LayoutType.Vertical;

            LayoutControlGroup lcgDrawDown_new = lcgDrawDownsInfo_new.AddGroup();
            lcgDrawDown_new.GroupBordersVisible = false;
            lcgDrawDown_new.Name = "lcgDrawdown" + counter + "_1";
            lcgDrawDown_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgDrawDown_new.Tag = counter;
            lcgDrawDown_new.Text = "lcgDrawdown" + counter + "_1";
            lcgDrawDown_new.TextVisible = false;
            lcgDrawDown_new.DefaultLayoutType = LayoutType.Horizontal;

            LayoutControlGroup lcgButtons_new = lcgDrawDowns_new.AddGroup();
            lcgButtons_new.GroupBordersVisible = false;
            lcgButtons_new.Name = "lcgButtons_" + counter;
            lcgButtons_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgButtons_new.Tag = counter;
            lcgButtons_new.Text = "lcgButtons_" + counter;
            lcgButtons_new.TextVisible = false;
            lcgButtons_new.DefaultLayoutType = LayoutType.Horizontal;


            #region Layout Items in Group Draw-Downs

            //
            //layoutDrawdownDate_
            //
            LayoutControlItem layoutDrawdownDate_new = lcgDrawDown_new.AddItem();
            layoutDrawdownDate_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutDrawdownDate_new.Control = dtpDrawdownDate_new;
            layoutDrawdownDate_new.MaxSize = new Size(183, 24);
            layoutDrawdownDate_new.MinSize = new Size(183, 24);
            layoutDrawdownDate_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutDrawdownDate_new.Name = "layoutDrawdownDate" + counter + "_1";
            layoutDrawdownDate_new.Text = "Draw-down date:";

            //
            //layoutDrawdownAmount_
            //
            LayoutControlItem layoutDrawdownAmount_new = lcgDrawDown_new.AddItem();
            layoutDrawdownAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutDrawdownAmount_new.Control = txtDrawdownAmount_new;
            layoutDrawdownAmount_new.Name = "layoutDrawdownAmount" + counter + "_1";
            layoutDrawdownAmount_new.Text = "Draw-down amount:";


            //
            // layoutAddDrawdown_
            //
            LayoutControlItem layoutAddDrawdown_new = lcgButtons_new.AddItem();
            layoutAddDrawdown_new.Control = btnAddDrawdown_new;
            layoutAddDrawdown_new.Name = "layoutAddDrawdown_" + counter;
            layoutAddDrawdown_new.TextVisible = false;

            //
            // layoutRemoveDrawdown_
            //
            LayoutControlItem layoutRemoveDrawdown_new = lcgButtons_new.AddItem();
            layoutRemoveDrawdown_new.Control = btnRemoveDrawdown_new;
            layoutRemoveDrawdown_new.Name = "layoutRemoveDrawdown_" + counter;
            layoutRemoveDrawdown_new.TextVisible = false;

            lcgDrawDowns_new.AddItem(new EmptySpaceItem());

            #endregion

            lcTranches.EndUpdate();
            lcTranches.BestFit();

            #endregion
        }

        private void BtnRemoveTranchClick(object sender, EventArgs e)
        {
            if (lcgTranches.Items.Count == 1) return;

            lcgTranches.BeginUpdate();

            RemoveItemsFromGroup(lcTranches, (LayoutControlGroup)lcgTranches.Items[lcgTranches.Items.Count - 1]);

            lcgTranches.RemoveAt(lcgTranches.Items.Count - 1);
            lcgTranches.EndUpdate();
        }

        private void BtnAddBankClick(object sender, EventArgs e)
        {
            foreach (object item in lcgBanks.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBank = (LayoutControlGroup)item;
                    if (layoutBank.Name.StartsWith("lcgBank_"))
                    {
                        string layoutLegSuffix = layoutBank.Name.Substring(layoutBank.Name.IndexOf("_") + 1);
                        var bank = (LookUpEdit)((LayoutControlItem)layoutBank.Items.FindByName("layoutBank_" + layoutLegSuffix)).Control;
                        if (bank.EditValue == null)
                            return;
                    }
                }
            }

            lcBanks.BeginUpdate();

            int counter = Convert.ToInt32(lcgBanks.Items[lcgBanks.Items.Count - 1].Tag) + 1;

            var lookupBank_new = new LookUpEdit();
            var chkIsArranger_new = new CheckEdit();
            var txtPercentage_new = new SpinEdit(); 
            // 
            // lookupBank
            // 
            lookupBank_new.Name = "lookupBank_" + counter;
            lookupBank_new.Properties.DisplayMember = "Name";
            lookupBank_new.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Bank_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupBank_new.Properties.Columns.Add(col);
            lookupBank_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupBank_new.Properties.SearchMode = SearchMode.AutoFilter;
            lookupBank_new.Properties.NullValuePrompt = Strings.Select_a_Bank___;
            if (lookupBank_1.Tag != null)
                lookupBank_new.Properties.DataSource = (List<Company>) lookupBank_1.Tag;

            // 
            // chkIsArranger
            // 
            chkIsArranger_new.EditValue = false;
            chkIsArranger_new.Name = "chkIsArranger_" + counter;
            chkIsArranger_new.Text = "";

            //
            //txtPercentage
            //
            txtPercentage_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
            txtPercentage_new.Name = "txtBankPercentage_" + counter;
            txtPercentage_new.Properties.EditMask = "P0";
            txtPercentage_new.Properties.MinValue = 1;
            txtPercentage_new.Properties.MaxValue = 100;
            txtPercentage_new.Properties.MaxLength = 3;
            txtPercentage_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtPercentage_new.Value = 0;

            LayoutControlGroup lcgBank_new = lcgBanks.AddGroup();

            // 
            // lcgBank_new
            // 
            lcgBank_new.GroupBordersVisible = false;
            lcgBank_new.Name = "lcgBank_" + counter;
            lcgBank_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgBank_new.Tag = counter;
            lcgBank_new.Text = "lcgBank";
            lcgBank_new.TextVisible = false;
            lcgBank_new.DefaultLayoutType = LayoutType.Horizontal;
            // 
            // layoutBank_new
            // 
            LayoutControlItem layoutBank_new = lcgBank_new.AddItem();
            layoutBank_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutBank_new.Control = lookupBank_new;
            layoutBank_new.Name = "layoutBank_" + counter;
            layoutBank_new.Text = "Bank:";
            layoutBank_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutBank_new.MinSize = new Size(86, 24);
            layoutBank_new.MaxSize = new Size(0, 24);

            // 
            // layoutArranger_new
            // 
            LayoutControlItem layoutArranger_new = lcgBank_new.AddItem();
            layoutArranger_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutArranger_new.Control = chkIsArranger_new;
            layoutArranger_new.Name = "layoutArranger_" + counter;
            layoutArranger_new.Text = "Arranger:";
            layoutArranger_new.MinSize = new Size(79, 24);
            layoutArranger_new.MaxSize = new Size(0, 24);

            // 
            // layoutPercentage
            // 
            LayoutControlItem layoutPercentage_new = lcgBank_new.AddItem();
            layoutPercentage_new.Control = txtPercentage_new;
            layoutPercentage_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutPercentage_new.Name = "layoutBankPercentage_" + counter;
            //layoutPercentage_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutPercentage_new.Text = "Perc.:";
            //layoutPercentage_new.MinSize = new Size(117, 24);
            //layoutPercentage_new.MaxSize = new Size(117, 24);

            lcBanks.EndUpdate();
            lcBanks.BestFit();
        }

        private void BtnRemoveBankClick(object sender, EventArgs e)
        {
            if (lcgBanks.Items.Count == 1) return;

            lcgBanks.BeginUpdate();

            foreach (
                LayoutControlItem layoutControlItem in
                    ((LayoutControlGroup)lcgBanks.Items[lcgBanks.Items.Count - 1]).Items)
            {
                lcBanks.Controls.Remove(layoutControlItem.Control);
            }

            lcgBanks.RemoveAt(lcgBanks.Items.Count - 1);
            lcBanks.EndUpdate();
        }

        private void BtnAddAssetClick(object sender, EventArgs e)
        {
            foreach (object item in lcgAssets.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var lcgAsset = (LayoutControlGroup)item;
                    if (lcgAsset.Name.StartsWith("lcgAsset_"))
                    {
                        string layoutLegSuffix = lcgAsset.Name.Substring(lcgAsset.Name.IndexOf("_") + 1);
                        var asset = (ComboBoxEdit)((LayoutControlItem)lcgAsset.Items.FindByName("layoutAssetType_" + layoutLegSuffix)).Control;
                        if (asset.EditValue == null)
                            return;
                    }
                }
            }

            lcAssets.BeginUpdate();

            int counter = Convert.ToInt32(lcgAssets.Items[lcgAssets.Items.Count - 1].Tag) + 1;

            var cmbAssetType_new = new ComboBoxEdit();
            var lookupVessel_new = new LookUpEdit();
            var txtAsset_new = new TextEdit();
            var txtAmount_new = new SpinEdit();

            //Asset Type
            cmbAssetType_new.Name = "cmbAssetType_" + counter;
            cmbAssetType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbAssetType_new.Properties.Items.Add(LoanCollateralTypeEnum.Cash);
            cmbAssetType_new.Properties.Items.Add(LoanCollateralTypeEnum.Vessel);
            cmbAssetType_new.Properties.Items.Add(LoanCollateralTypeEnum.Other);
            cmbAssetType_new.SelectedItem = LoanCollateralTypeEnum.Vessel;
            cmbAssetType_new.SelectedIndexChanged += new EventHandler(CmbAssetTypeSelectedIndexChanged);

            // 
            // lookupVessel
            // 
            lookupVessel_new.Name = "lookupVessel_" + counter;
            lookupVessel_new.Properties.DisplayMember = "Name";
            lookupVessel_new.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Vessel_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupVessel_new.Properties.Columns.Add(col);
            lookupVessel_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupVessel_new.Properties.SearchMode = SearchMode.AutoFilter;
            lookupVessel_new.Properties.NullValuePrompt = Strings.Select_a_Vessel___;
            lookupVessel_new.Properties.NullText = "";
            if (lookupVessel_1.Tag != null)
                lookupVessel_new.Properties.DataSource = (List<Vessel>)lookupVessel_1.Tag;

            // 
            // txtAsset
            // 
            txtAsset_new.Name = "txtAsset_" + counter;
            txtAsset_new.Properties.ReadOnly = true;
            // 
            // txtAmount
            // 
            txtAmount_new.Name = "txtAssetAmount_" + counter;
            txtAmount_new.Properties.EditMask = "D";
            txtAmount_new.Properties.MinValue = 1;
            txtAmount_new.Properties.MaxValue = 999999999;
            txtAmount_new.Properties.MaxLength = 9;
            txtAmount_new.Value = 0;
            txtAmount_new.Properties.ReadOnly = true;

            LayoutControlGroup lcgAsset_new = lcgAssets.AddGroup();

            // 
            // layoutAsset
            // 
            lcgAsset_new.GroupBordersVisible = false;
            lcgAsset_new.Name = "lcgAsset_" + counter;
            lcgAsset_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgAsset_new.Tag = counter;
            lcgAsset_new.Text = "lcgAsset_";
            lcgAsset_new.TextVisible = false;
            lcgAsset_new.DefaultLayoutType = LayoutType.Horizontal;

            // 
            // layoutAssetType
            // 
            LayoutControlItem layoutAssetType_new = lcgAsset_new.AddItem();
            layoutAssetType_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutAssetType_new.Control = cmbAssetType_new;
            layoutAssetType_new.Name = "layoutAssetType_" + counter;
            layoutAssetType_new.Text = "Type:";
            layoutAssetType_new.MinSize = new Size(109, 24);
            layoutAssetType_new.MaxSize = new Size(109, 24);
            layoutAssetType_new.SizeConstraintsType = SizeConstraintsType.Custom;

            // 
            // layoutVessel
            // 
            LayoutControlItem layoutVessel_new = lcgAsset_new.AddItem();
            layoutVessel_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutVessel_new.Control = lookupVessel_new;
            layoutVessel_new.Name = "layoutVessel_" + counter;
            layoutVessel_new.Text = "Vessel:";

            // 
            // layoutAsset
            // 
            LayoutControlItem layoutAsset_new = lcgAsset_new.AddItem();
            layoutAsset_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutAsset_new.Control = txtAsset_new;
            layoutAsset_new.Name = "layoutAsset_" + counter;
            layoutAsset_new.Text = "Asset:";
            
            // 
            // layoutAmount
            // 
            LayoutControlItem layoutAmount_new = lcgAsset_new.AddItem();
            layoutAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutAmount_new.Control = txtAmount_new;
            layoutAmount_new.Name = "layoutAssetAmount_" + counter;
            layoutAmount_new.Text = "Amount:";

            lcAssets.EndUpdate();
            lcAssets.BestFit();
        }

        private void BtnRemoveAssetClick(object sender, EventArgs e)
        {
            if (lcgAssets.Items.Count == 1) return;

            lcgAssets.BeginUpdate();

            foreach (
                LayoutControlItem layoutControlItem in
                    ((LayoutControlGroup)lcgAssets.Items[lcgAssets.Items.Count - 1]).Items)
            {
                lcAssets.Controls.Remove(layoutControlItem.Control);
            }

            lcgAssets.RemoveAt(lcgAssets.Items.Count - 1);
            lcAssets.EndUpdate();
        }

        private void CmbAssetTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            var cmbType = sender as ComboBoxEdit;

            string layoutAssetSuffix = ((ComboBoxEdit)sender).Name.Replace("cmbAssetType_", "");

            if(cmbType.EditValue != null)
            {
                var assetGroup = (LayoutControlGroup)lcgAssets.Items.FindByName("lcgAsset_" + layoutAssetSuffix);

                var lookupVessel = (LookUpEdit)((LayoutControlItem)assetGroup.Items.FindByName("layoutVessel_" + layoutAssetSuffix)).Control;
                var txtAsset = (TextEdit)((LayoutControlItem)assetGroup.Items.FindByName("layoutAsset_" + layoutAssetSuffix)).Control;
                var spinAmount = (SpinEdit)((LayoutControlItem)assetGroup.Items.FindByName("layoutAssetAmount_" + layoutAssetSuffix)).Control;
                var selectedType = (LoanCollateralTypeEnum)cmbType.EditValue;
                
                if(_formActionType == FormActionTypeEnum.View)
                {
                    lookupVessel.Properties.ReadOnly = true;
                    txtAsset.Properties.ReadOnly = true;
                    spinAmount.Properties.ReadOnly = true;
                }
                else
                {
                    if (selectedType == LoanCollateralTypeEnum.Vessel)
                    {
                        lookupVessel.Properties.ReadOnly = false;
                        txtAsset.Properties.ReadOnly = true;
                        txtAsset.EditValue = null;
                        spinAmount.Properties.ReadOnly = true;
                        spinAmount.EditValue = null;
                    }
                    else if (selectedType == LoanCollateralTypeEnum.Cash)
                    {
                        lookupVessel.Properties.ReadOnly = true;
                        lookupVessel.EditValue = null;
                        txtAsset.Properties.ReadOnly = true;
                        txtAsset.EditValue = null;
                        spinAmount.Properties.ReadOnly = false;
                    }
                    else if (selectedType == LoanCollateralTypeEnum.Other)
                    {
                        lookupVessel.Properties.ReadOnly = true;
                        lookupVessel.EditValue = null;
                        txtAsset.Properties.ReadOnly = false;
                        spinAmount.Properties.ReadOnly = false;
                    }
                }
            }
        }

        private void BtnAddDrawdownClick(object sender, EventArgs e)
        {
            int buttonIndex = Convert.ToInt32(((SimpleButton)sender).Name.Replace("btnAddDrawdown_", ""));
            var drawdownsGroup = (LayoutControlGroup)lcTranches.Items.FindByName("lcgDrawDownsInfo_" + buttonIndex);

            lcTranches.BeginUpdate();

            int counter = Convert.ToInt32(drawdownsGroup.Items[drawdownsGroup.Items.Count - 1].Tag) + 1;

            //
            //dtpDrawdownDate
            //
            var dtpDrawdownDate_new = new DateEdit();
            dtpDrawdownDate_new.EditValue = null;
            dtpDrawdownDate_new.Name = "dtpDrawdownDate" + buttonIndex + "_" + counter;

            //
            //txtDrawdownAmount_
            //
            var txtDrawdownAmount_new = new SpinEdit();
            txtDrawdownAmount_new.Name = "txtDrawdownAmount" + buttonIndex + "_" + counter;
            txtDrawdownAmount_new.Properties.EditMask = "D";
            txtDrawdownAmount_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtDrawdownAmount_new.Properties.MinValue = 1;
            txtDrawdownAmount_new.Properties.MaxValue = 999999999999;
            txtDrawdownAmount_new.Properties.MaxLength = 12;
            txtDrawdownAmount_new.Value = 0;

            LayoutControlGroup lcgDrawDown_new = drawdownsGroup.AddGroup();
            lcgDrawDown_new.GroupBordersVisible = false;
            lcgDrawDown_new.Name = "lcgDrawdown" + buttonIndex + "_" + counter;
            lcgDrawDown_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            lcgDrawDown_new.Tag = counter;
            lcgDrawDown_new.Text = "lcgDrawdown" + buttonIndex + "_" + counter;
            lcgDrawDown_new.TextVisible = false;
            lcgDrawDown_new.DefaultLayoutType = LayoutType.Horizontal;

            //
            //layoutDrawdownDate_
            //
            LayoutControlItem layoutDrawdownDate_new = lcgDrawDown_new.AddItem();
            layoutDrawdownDate_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutDrawdownDate_new.Control = dtpDrawdownDate_new;
            layoutDrawdownDate_new.MaxSize = new Size(183, 24);
            layoutDrawdownDate_new.MinSize = new Size(183, 24);
            layoutDrawdownDate_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutDrawdownDate_new.Name = "layoutDrawdownDate" + buttonIndex + "_" + counter;
            layoutDrawdownDate_new.Text = "Draw-down date:";

            //
            //layoutDrawdownAmount_
            //
            LayoutControlItem layoutDrawdownAmount_new = lcgDrawDown_new.AddItem();
            layoutDrawdownAmount_new.TextAlignMode = TextAlignModeItem.AutoSize;
            layoutDrawdownAmount_new.Control = txtDrawdownAmount_new;
            layoutDrawdownAmount_new.Name = "layoutDrawdownAmount" + buttonIndex + "_" + counter;
            layoutDrawdownAmount_new.Text = "Draw-down amount:";

            lcTranches.EndUpdate();
        }

        private void BtnRemoveDrawdownClick(object sender, EventArgs e)
        {
            int buttonIndex = Convert.ToInt32(((SimpleButton)sender).Name.Replace("btnRemoveDrawdown_", ""));
            var drawdownsGroup = (LayoutControlGroup)lcTranches.Items.FindByName("lcgDrawDownsInfo_" + buttonIndex);

            if (drawdownsGroup.Items.Count == 1) return;

            lcTranches.BeginUpdate();

            RemoveItemsFromGroup(lcTranches, (LayoutControlGroup)drawdownsGroup.Items[drawdownsGroup.Items.Count - 1]);

            drawdownsGroup.RemoveAt(drawdownsGroup.Items.Count - 1);
            lcTranches.EndUpdate();
        }

        private void TxtAmountEditValueChanged(object sender, EventArgs e)
        {
            txtTranchAmount_1.Text = txtAmount.Text;
        }

        private void CmbInterestRateTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            var comboRateType = (ComboBoxEdit) sender;

            int index = Convert.ToInt32(comboRateType.Name.Replace("cmbInterestRateType_", ""));

            var tranchInfoGroup = (LayoutControlGroup) lcTranches.Items.FindByName("lcgTranchInfo_" + index);
            var tranchMainInfoGroup =
                (LayoutControlGroup) tranchInfoGroup.Items.FindByName("lcgTranchMainInfo_" + index);
            var tranchMainInfoPart2Group =
                (LayoutControlGroup) tranchMainInfoGroup.Items.FindByName("lcgTranchMainInfoPart2_" + index);
            
            var lookupRefRateType =
                (ComboBoxEdit)
                ((LayoutControlItem)tranchMainInfoPart2Group.Items.FindByName("layoutPeriodType_" + index)).Control;

            if ((LoanInterestRateTypeEnum)comboRateType.SelectedItem == LoanInterestRateTypeEnum.Fixed)
            {
                lookupRefRateType.EditValue = null;
                lookupRefRateType.Properties.ReadOnly = true;
            }
            else if ((LoanInterestRateTypeEnum)comboRateType.SelectedItem == LoanInterestRateTypeEnum.Floating)
            {
                lookupRefRateType.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View;
            }

        }

        private void CmbPrincipalFrequencySelectedIndexChanged(object sender, EventArgs e)
        {
            var comboRateType = (ComboBoxEdit)sender;

            int index = Convert.ToInt32(comboRateType.Name.Replace("cmbPrincipalFrequency_", ""));

            var tranchInfoGroup = (LayoutControlGroup)lcTranches.Items.FindByName("lcgTranchInfo_" + index);
            var tranchMainInfoGroup =
                (LayoutControlGroup)tranchInfoGroup.Items.FindByName("lcgTranchMainInfo_" + index);
            var tranchMainInfoPart1Group =
                (LayoutControlGroup)tranchMainInfoGroup.Items.FindByName("lcgTranchMainInfoPart1_" + index);

            var cmbInterestFrequncy =
                (ComboBoxEdit)
                ((LayoutControlItem)tranchMainInfoPart1Group.Items.FindByName("layoutInterestFrequency_" + index)).Control;

            cmbInterestFrequncy.EditValue = null;
            cmbInterestFrequncy.Properties.Items.Clear();

            if ((LoanRepaymentFrequencyEnum)comboRateType.SelectedItem == LoanRepaymentFrequencyEnum.Quarter)
            {
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            }
            else if ((LoanRepaymentFrequencyEnum)comboRateType.SelectedItem == LoanRepaymentFrequencyEnum.Semester)
            {
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            }
            else if ((LoanRepaymentFrequencyEnum)comboRateType.SelectedItem == LoanRepaymentFrequencyEnum.Year)
            {
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            }
            else if ((LoanRepaymentFrequencyEnum)comboRateType.SelectedItem == LoanRepaymentFrequencyEnum.Month)
            {
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Month);
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Quarter);
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Semester);
                cmbInterestFrequncy.Properties.Items.Add(LoanRepaymentFrequencyEnum.Year);
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAevLoanInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVLoanInitializationData(EndAevLoanInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAevLoanInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAevLoanInitializationData;
                Invoke(action, ar);
                return;
            }
            
            int? result;

            List<Company> banks;
            List<Vessel> vessels;
            List<Company> obligors;
            List<Company> guarantors;
            List<Currency> currencies;
            List<Company> borrowers;

            try
            {
                result = SessionRegistry.Client.EndAEVLoanInitializationData(out banks, out vessels, out obligors,
                                                                             out guarantors,
                                                                             out currencies, out borrowers,
                                                                             ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookupBank_1.Properties.DataSource = banks;
                lookupBank_1.Tag = banks;
                lookupCurrency.Properties.DataSource = currencies;
                lookupCurrency.Tag = currencies;
                lookupGuarantor.Tag = guarantors;
                lookupGuarantor.Properties.DataSource = guarantors;
                lookupObligor.Properties.DataSource = obligors;
                lookupObligor.Tag = obligors;
                lookupVessel_1.Properties.DataSource = vessels;
                lookupVessel_1.Tag = vessels;
                cmbBorrower.Properties.DataSource = borrowers;
                cmbBorrower.Tag = borrowers;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                {
                    LoadData();
                }
            }
        }

        private void BeginAevLoan(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _loan, EndAevLoan, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAevLoan(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAevLoan;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                //Code already exists
                errorProvider.SetError(txtCode, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Loan Loan
        {
            get { return _loan; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}