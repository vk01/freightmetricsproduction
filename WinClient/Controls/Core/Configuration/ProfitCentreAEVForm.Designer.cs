﻿namespace Exis.WinClient.Controls
{
    partial class ProfitCentreAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfitCentreAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.txtExposureLimit = new DevExpress.XtraEditors.SpinEdit();
            this.txtExpirationLimit = new DevExpress.XtraEditors.SpinEdit();
            this.txtDurationLimit = new DevExpress.XtraEditors.SpinEdit();
            this.txtNotionalLimit = new DevExpress.XtraEditors.SpinEdit();
            this.txtCapitalLimit = new DevExpress.XtraEditors.SpinEdit();
            this.txtMTMLimit = new DevExpress.XtraEditors.SpinEdit();
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.txtVaRLimit = new DevExpress.XtraEditors.SpinEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupProfitCentre = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVarLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemMTMLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCapitalLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemNotionalLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemDurationLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemExpirationLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemExposureLimit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpace1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExposureLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpirationLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDurationLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotionalLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCapitalLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTMLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVaRLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupProfitCentre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVarLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemMTMLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCapitalLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemNotionalLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDurationLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemExpirationLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemExposureLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpace1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.txtExposureLimit);
            this.layoutRootControl.Controls.Add(this.txtExpirationLimit);
            this.layoutRootControl.Controls.Add(this.txtDurationLimit);
            this.layoutRootControl.Controls.Add(this.txtNotionalLimit);
            this.layoutRootControl.Controls.Add(this.txtCapitalLimit);
            this.layoutRootControl.Controls.Add(this.txtMTMLimit);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.txtVaRLimit);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtName);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 426, 250, 350);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(942, 588);
            this.layoutRootControl.TabIndex = 2;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // txtExposureLimit
            // 
            this.txtExposureLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtExposureLimit.Location = new System.Drawing.Point(865, 57);
            this.txtExposureLimit.Name = "txtExposureLimit";
            this.txtExposureLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtExposureLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtExposureLimit.Properties.Mask.EditMask = "N0";
            this.txtExposureLimit.Properties.MaxLength = 9;
            this.txtExposureLimit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtExposureLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtExposureLimit.Size = new System.Drawing.Size(63, 20);
            this.txtExposureLimit.StyleController = this.layoutRootControl;
            this.txtExposureLimit.TabIndex = 21;
            // 
            // txtExpirationLimit
            // 
            this.txtExpirationLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtExpirationLimit.Location = new System.Drawing.Point(722, 57);
            this.txtExpirationLimit.Name = "txtExpirationLimit";
            this.txtExpirationLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtExpirationLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtExpirationLimit.Properties.Mask.EditMask = "N0";
            this.txtExpirationLimit.Properties.MaxLength = 9;
            this.txtExpirationLimit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtExpirationLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtExpirationLimit.Size = new System.Drawing.Size(63, 20);
            this.txtExpirationLimit.StyleController = this.layoutRootControl;
            this.txtExpirationLimit.TabIndex = 20;
            // 
            // txtDurationLimit
            // 
            this.txtDurationLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtDurationLimit.Location = new System.Drawing.Point(555, 57);
            this.txtDurationLimit.Name = "txtDurationLimit";
            this.txtDurationLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtDurationLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDurationLimit.Properties.Mask.EditMask = "N0";
            this.txtDurationLimit.Properties.MaxLength = 9;
            this.txtDurationLimit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtDurationLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtDurationLimit.Size = new System.Drawing.Size(84, 20);
            this.txtDurationLimit.StyleController = this.layoutRootControl;
            this.txtDurationLimit.TabIndex = 19;
            // 
            // txtNotionalLimit
            // 
            this.txtNotionalLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtNotionalLimit.Location = new System.Drawing.Point(388, 57);
            this.txtNotionalLimit.Name = "txtNotionalLimit";
            this.txtNotionalLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtNotionalLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtNotionalLimit.Properties.Mask.EditMask = "N0";
            this.txtNotionalLimit.Properties.MaxLength = 9;
            this.txtNotionalLimit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtNotionalLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtNotionalLimit.Size = new System.Drawing.Size(91, 20);
            this.txtNotionalLimit.StyleController = this.layoutRootControl;
            this.txtNotionalLimit.TabIndex = 18;
            // 
            // txtCapitalLimit
            // 
            this.txtCapitalLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtCapitalLimit.Location = new System.Drawing.Point(232, 57);
            this.txtCapitalLimit.Name = "txtCapitalLimit";
            this.txtCapitalLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtCapitalLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtCapitalLimit.Properties.Mask.EditMask = "N0";
            this.txtCapitalLimit.Properties.MaxLength = 9;
            this.txtCapitalLimit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtCapitalLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtCapitalLimit.Size = new System.Drawing.Size(82, 20);
            this.txtCapitalLimit.StyleController = this.layoutRootControl;
            this.txtCapitalLimit.TabIndex = 17;
            // 
            // txtMTMLimit
            // 
            this.txtMTMLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtMTMLimit.Location = new System.Drawing.Point(67, 57);
            this.txtMTMLimit.Name = "txtMTMLimit";
            this.txtMTMLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtMTMLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMTMLimit.Properties.Mask.EditMask = "N0";
            this.txtMTMLimit.Properties.MaxLength = 9;
            this.txtMTMLimit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtMTMLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtMTMLimit.Size = new System.Drawing.Size(97, 20);
            this.txtMTMLimit.StyleController = this.layoutRootControl;
            this.txtMTMLimit.TabIndex = 16;
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(827, 124);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Size = new System.Drawing.Size(101, 20);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 8;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(563, 124);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(192, 20);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 11;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(391, 124);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Size = new System.Drawing.Size(101, 20);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 7;
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(87, 124);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(226, 20);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 10;
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(31, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(81, 20);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // txtVaRLimit
            // 
            this.txtVaRLimit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtVaRLimit.Location = new System.Drawing.Point(827, 33);
            this.txtVaRLimit.Name = "txtVaRLimit";
            this.txtVaRLimit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtVaRLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVaRLimit.Properties.Mask.EditMask = "N0";
            this.txtVaRLimit.Properties.MaxLength = 6;
            this.txtVaRLimit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtVaRLimit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtVaRLimit.Size = new System.Drawing.Size(101, 20);
            this.txtVaRLimit.StyleController = this.layoutRootControl;
            this.txtVaRLimit.TabIndex = 15;
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(672, 33);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(101, 20);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(150, 33);
            this.txtName.Name = "txtName";
            this.txtName.Properties.MaxLength = 1000;
            this.txtName.Size = new System.Drawing.Size(480, 20);
            this.txtName.StyleController = this.layoutRootControl;
            this.txtName.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupProfitCentre,
            this.layoutGroupUserInfo,
            this.emptySpace1});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(942, 588);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupProfitCentre
            // 
            this.layoutGroupProfitCentre.CustomizationFormText = "Company";
            this.layoutGroupProfitCentre.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutId,
            this.layoutStatus,
            this.layoutVarLimit,
            this.layoutItemMTMLimit,
            this.layoutItemCapitalLimit,
            this.layoutItemNotionalLimit,
            this.layoutItemDurationLimit,
            this.layoutItemExpirationLimit,
            this.layoutItemExposureLimit});
            this.layoutGroupProfitCentre.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupProfitCentre.Name = "layoutGroupProfitCentre";
            this.layoutGroupProfitCentre.Size = new System.Drawing.Size(942, 91);
            this.layoutGroupProfitCentre.Text = "Profit Centre";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "Name:";
            this.layoutName.Location = new System.Drawing.Point(102, 0);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(518, 24);
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(31, 13);
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MaxSize = new System.Drawing.Size(102, 24);
            this.layoutId.MinSize = new System.Drawing.Size(102, 24);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(102, 24);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(14, 13);
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(620, 0);
            this.layoutStatus.MaxSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.MinSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(143, 24);
            this.layoutStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutVarLimit
            // 
            this.layoutVarLimit.Control = this.txtVaRLimit;
            this.layoutVarLimit.CustomizationFormText = "Risk Rating:";
            this.layoutVarLimit.Location = new System.Drawing.Point(763, 0);
            this.layoutVarLimit.MaxSize = new System.Drawing.Size(155, 24);
            this.layoutVarLimit.MinSize = new System.Drawing.Size(155, 24);
            this.layoutVarLimit.Name = "layoutVarLimit";
            this.layoutVarLimit.Size = new System.Drawing.Size(155, 24);
            this.layoutVarLimit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutVarLimit.Text = "VaR Limit:";
            this.layoutVarLimit.TextSize = new System.Drawing.Size(47, 13);
            // 
            // layoutItemMTMLimit
            // 
            this.layoutItemMTMLimit.Control = this.txtMTMLimit;
            this.layoutItemMTMLimit.CustomizationFormText = "MTM Limit:";
            this.layoutItemMTMLimit.Location = new System.Drawing.Point(0, 24);
            this.layoutItemMTMLimit.Name = "layoutItemMTMLimit";
            this.layoutItemMTMLimit.Size = new System.Drawing.Size(154, 24);
            this.layoutItemMTMLimit.Text = "MTM Limit:";
            this.layoutItemMTMLimit.TextSize = new System.Drawing.Size(50, 13);
            // 
            // layoutItemCapitalLimit
            // 
            this.layoutItemCapitalLimit.Control = this.txtCapitalLimit;
            this.layoutItemCapitalLimit.CustomizationFormText = "Capital Limit:";
            this.layoutItemCapitalLimit.Location = new System.Drawing.Point(154, 24);
            this.layoutItemCapitalLimit.Name = "layoutItemCapitalLimit";
            this.layoutItemCapitalLimit.Size = new System.Drawing.Size(150, 24);
            this.layoutItemCapitalLimit.Text = "Capital Limit:";
            this.layoutItemCapitalLimit.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutItemNotionalLimit
            // 
            this.layoutItemNotionalLimit.Control = this.txtNotionalLimit;
            this.layoutItemNotionalLimit.CustomizationFormText = "Notional Limit:";
            this.layoutItemNotionalLimit.Location = new System.Drawing.Point(304, 24);
            this.layoutItemNotionalLimit.Name = "layoutItemNotionalLimit";
            this.layoutItemNotionalLimit.Size = new System.Drawing.Size(165, 24);
            this.layoutItemNotionalLimit.Text = "Notional Limit:";
            this.layoutItemNotionalLimit.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutItemDurationLimit
            // 
            this.layoutItemDurationLimit.Control = this.txtDurationLimit;
            this.layoutItemDurationLimit.CustomizationFormText = "Duration Limit:";
            this.layoutItemDurationLimit.Location = new System.Drawing.Point(469, 24);
            this.layoutItemDurationLimit.Name = "layoutItemDurationLimit";
            this.layoutItemDurationLimit.Size = new System.Drawing.Size(160, 24);
            this.layoutItemDurationLimit.Text = "Duration Limit:";
            this.layoutItemDurationLimit.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutItemExpirationLimit
            // 
            this.layoutItemExpirationLimit.Control = this.txtExpirationLimit;
            this.layoutItemExpirationLimit.CustomizationFormText = "Expiration Limit:";
            this.layoutItemExpirationLimit.Location = new System.Drawing.Point(629, 24);
            this.layoutItemExpirationLimit.Name = "layoutItemExpirationLimit";
            this.layoutItemExpirationLimit.Size = new System.Drawing.Size(146, 24);
            this.layoutItemExpirationLimit.Text = "Expiration Limit:";
            this.layoutItemExpirationLimit.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutItemExposureLimit
            // 
            this.layoutItemExposureLimit.Control = this.txtExposureLimit;
            this.layoutItemExposureLimit.CustomizationFormText = "Exposure Limit:";
            this.layoutItemExposureLimit.Location = new System.Drawing.Point(775, 24);
            this.layoutItemExposureLimit.Name = "layoutItemExposureLimit";
            this.layoutItemExposureLimit.Size = new System.Drawing.Size(143, 24);
            this.layoutItemExposureLimit.Text = "Exposure Limit:";
            this.layoutItemExposureLimit.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "User Info";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 91);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(942, 67);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "Creation User:";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(303, 24);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(303, 0);
            this.layoutCreationDate.MaxSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.MinSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(482, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(263, 24);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(745, 0);
            this.layoutUpdateDate.MaxSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.MinSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // emptySpace1
            // 
            this.emptySpace1.AllowHotTrack = false;
            this.emptySpace1.CustomizationFormText = "emptySpace1";
            this.emptySpace1.Location = new System.Drawing.Point(0, 158);
            this.emptySpace1.Name = "emptySpace1";
            this.emptySpace1.Size = new System.Drawing.Size(942, 430);
            this.emptySpace1.Text = "emptySpace1";
            this.emptySpace1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(942, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 588);
            this.barDockControlBottom.Size = new System.Drawing.Size(942, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(942, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 588);
            // 
            // ProfitCentreAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 620);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ProfitCentreAEVForm";
            this.Activated += new System.EventHandler(this.ProfitCentreAEVForm_Activated);
            this.Deactivate += new System.EventHandler(this.ProfitCentreAEVForm_Deactivate);
            this.Load += new System.EventHandler(this.ProfitCentreAEVForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtExposureLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpirationLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDurationLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotionalLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCapitalLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTMLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVaRLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupProfitCentre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVarLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemMTMLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCapitalLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemNotionalLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemDurationLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemExpirationLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemExposureLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpace1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraEditors.SpinEdit txtVaRLimit;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupProfitCentre;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutVarLimit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpace1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.SpinEdit txtExposureLimit;
        private DevExpress.XtraEditors.SpinEdit txtExpirationLimit;
        private DevExpress.XtraEditors.SpinEdit txtDurationLimit;
        private DevExpress.XtraEditors.SpinEdit txtNotionalLimit;
        private DevExpress.XtraEditors.SpinEdit txtCapitalLimit;
        private DevExpress.XtraEditors.SpinEdit txtMTMLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemMTMLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCapitalLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemNotionalLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemDurationLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemExpirationLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemExposureLimit;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}