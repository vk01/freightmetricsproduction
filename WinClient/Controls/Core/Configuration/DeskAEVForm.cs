﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class DeskAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private Desk _desk;
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public DeskAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public DeskAEVForm(Desk desk, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _desk = desk;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            txtVaRLimit.Text = null;

            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;

                txtVaRLimit.Properties.ReadOnly = true;
                txtUpdateUser.Properties.ReadOnly = true;
                txtNotionalLimit.Properties.ReadOnly = true;
                txtName.Properties.ReadOnly = true;
                txtMTMLimit.Properties.ReadOnly = true;
                txtExpirationLimit.Properties.ReadOnly = true;
                txtDurationLimit.Properties.ReadOnly = true;
                txtCapitalLimit.Properties.ReadOnly = true;
                txtExposureLimit.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
            }
        }

        private void InitializeData()
        {
            if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                LoadData();
        }

        private void LoadData()
        {
            lblId.Text = _desk.Id.ToString();
            txtName.Text = _desk.Name;
            cmbStatus.SelectedItem = _desk.Status;
            txtVaRLimit.Text = _desk.VarLimit == null ? null : _desk.VarLimit.Value.ToString();
            txtNotionalLimit.Text = _desk.NotionalLimit == null ? null : _desk.NotionalLimit.Value.ToString();
            txtMTMLimit.Text = _desk.MtmLimit == null ? null : _desk.MtmLimit.Value.ToString();
            txtExposureLimit.Text = _desk.ExposureLimit == null ? null : _desk.ExposureLimit.Value.ToString();
            txtExpirationLimit.Text = _desk.ExpirationLimit == null ? null : _desk.ExpirationLimit.Value.ToString();
            txtDurationLimit.Text = _desk.DurationLimit == null ? null : _desk.DurationLimit.Value.ToString();
            txtCapitalLimit.Text = _desk.CapitalLimit == null ? null : _desk.CapitalLimit.Value.ToString();

            txtCreationUser.Text = _desk.Cruser;
            dtpCreationDate.DateTime = _desk.Crd;
            txtUpdateUser.Text = _desk.Chuser;
            dtpUpdateDate.DateTime = _desk.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);

            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void DeskAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _desk = new Desk();
            }

            _desk.Name = txtName.Text.Trim();
            _desk.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _desk.VarLimit = String.IsNullOrEmpty(txtVaRLimit.Text) ? null : (int?) txtVaRLimit.Value;
            _desk.CapitalLimit = String.IsNullOrEmpty(txtCapitalLimit.Text) ? null : (int?)txtCapitalLimit.Value;
            _desk.DurationLimit = String.IsNullOrEmpty(txtDurationLimit.Text) ? null : (int?)txtDurationLimit.Value;
            _desk.ExpirationLimit = String.IsNullOrEmpty(txtExpirationLimit.Text) ? null : (int?)txtExpirationLimit.Value;
            _desk.ExposureLimit = String.IsNullOrEmpty(txtExposureLimit.Text) ? null : (int?)txtExposureLimit.Value;
            _desk.MtmLimit = String.IsNullOrEmpty(txtMTMLimit.Text) ? null : (int?)txtMTMLimit.Value;
            _desk.NotionalLimit = String.IsNullOrEmpty(txtNotionalLimit.Text) ? null : (int?)txtNotionalLimit.Value;

            BeginAEVDesk(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void DeskAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void DeskAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVDesk(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _desk, EndAEVDesk, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVDesk(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVDesk;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Desk Desk
        {
            get { return _desk; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}