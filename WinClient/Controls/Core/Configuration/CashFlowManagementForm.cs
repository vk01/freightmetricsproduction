﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.SpecialForms;
using Exis.WinClient.General;
using Exis.Domain;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;

namespace Exis.WinClient.Controls
{
    public partial class CashFlowManagementForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        private List<CashFlowModelGroup> _modelGroups;
        private List<CashFlowGroupItem> _groupsItems;
        private List<CashFlowModel> _models;
        private List<CashFlowGroup> _groups;

        private int counter = -1;

        #endregion

        #region Constructors

        public CashFlowManagementForm()
        {
            InitializeComponent();

            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            chklItems.DisplayMember = "Name";
            chklItems.ValueMember = "Id";

            btnAddRemoveGroup.Properties.Buttons[3].Enabled = false;//Button Save
            btnAddRemoveModel.Properties.Buttons[3].Enabled = false;//Button Save

            btnAddRemoveModel.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            btnAddRemoveGroup.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
        }

        private void LoadData()
        {
            LoadGroups();
            LoadModels();

            var items = (List<CashFlowItem>)chklItems.Tag;
            chklItems.DataSource = items.OrderBy(a=>a.Name).ToList();
        }

        private void LoadModels()
        {
            trlModels.ClearNodes();

            foreach (var cashFlowModel in _models.OrderBy(a=>a.Name).ToList())
            {
                TreeListNode modelNode = trlModels.AppendNode(new object[] { cashFlowModel.Name }, null, cashFlowModel);
                modelNode.ImageIndex = modelNode.SelectImageIndex = modelNode.StateImageIndex = 7;
                List<long> groupIds =
                    _modelGroups.Where(a => a.ModelId == cashFlowModel.Id && a.ParentGroupId == null).Select(a => a.GroupId).ToList();
                List<CashFlowGroup> modelGroups = _groups.Where(a => groupIds.Contains(a.Id)).ToList();

                PopulateModelsTree(modelNode, modelGroups, cashFlowModel.Id);
            }
        }

        private void LoadGroups()
        {
            trlGroups.ClearNodes();

            foreach (var cashFlowGroup in _groups.OrderBy(a=>a.Name).ToList())
            {
                TreeListNode groupNode = trlGroups.AppendNode(new object[] {cashFlowGroup.Name}, null, cashFlowGroup);
                groupNode.ImageIndex = groupNode.SelectImageIndex = groupNode.StateImageIndex = 11;

                List<long> groupItemIds =
                    _groupsItems.Where(a => a.GroupId == cashFlowGroup.Id).Select(a => a.ItemId).ToList();
                var items = ((List<CashFlowItem>) chklItems.Tag).Where(a => groupItemIds.Contains(a.Id)).ToList();

                foreach (var cashFlowItem in items)
                {
                    TreeListNode itemNode = trlGroups.AppendNode(new object[] {cashFlowItem.Name}, groupNode,
                                                                 cashFlowItem);

                    int index = _groupsItems.Where(a => a.GroupId == cashFlowGroup.Id && a.ItemId == cashFlowItem.Id).Single().Order;
                    trlGroups.SetNodeIndex(itemNode, index);
                    itemNode.ImageIndex = itemNode.SelectImageIndex = itemNode.StateImageIndex = 10;
                }
            }
        }

        private void PopulateModelsTree(TreeListNode parentNode, List<CashFlowGroup> childrenNodes, long modelId)
        {
            if(childrenNodes == null) return;

            foreach (CashFlowGroup node in childrenNodes)
            {
                CashFlowGroup parentGroup = null;
                CashFlowModelGroup modelGroup = null;

                if(parentNode.Tag.GetType() == typeof(CashFlowGroup))
                    parentGroup = parentNode.Tag as CashFlowGroup;

                TreeListNode childNode = trlModels.AppendNode(new object[] { node.Name }, parentNode, node);

                if (parentGroup != null)
                    modelGroup =
                        _modelGroups.Where(
                            a => a.GroupId == node.Id && a.ModelId == modelId && a.ParentGroupId == parentGroup.Id).
                            Single();
                else
                    modelGroup =
                        _modelGroups.Where(
                            a => a.GroupId == node.Id && a.ModelId == modelId).Single();

                trlModels.SetNodeIndex(childNode, modelGroup.Order);
                childNode.ImageIndex =
                    childNode.SelectImageIndex =
                    childNode.StateImageIndex = (modelGroup.Operator == CashFlowModelGroupOperatorEnum.Addition ? 8 : 9);
                
                
                List<long> childrenIds =
                    _modelGroups.Where(a => a.ModelId == modelId && a.ParentGroupId == node.Id).Select(a => a.GroupId).ToList();
                List<CashFlowGroup> children = _groups.Where(a => childrenIds.Contains(a.Id)).ToList();

                PopulateModelsTree(childNode, children, modelId);

                int groupsCount = childNode.Nodes.Count;

                List<long> groupItemIds = _groupsItems.Where(a => a.GroupId == node.Id).Select(a => a.ItemId).ToList();
                var items = ((List<CashFlowItem>) chklItems.Tag).Where(a=> groupItemIds.Contains(a.Id)).ToList();
                foreach (var cashFlowItem in items)
                {
                    TreeListNode itemNode = trlModels.AppendNode(new object[] { cashFlowItem.Name }, childNode, cashFlowItem);
                    trlModels.SetNodeIndex(itemNode,
                                           _groupsItems.Where(a => a.ItemId == cashFlowItem.Id && a.GroupId == node.Id).
                                               Single().Order + groupsCount);
                    itemNode.ImageIndex = itemNode.SelectImageIndex = itemNode.StateImageIndex = 10;
                }
            }
        }

        private bool TreeListModelContainsGroup(CashFlowGroup cashFlowGroup,TreeListNodes nodes)
        {
            bool found = false;
            foreach (TreeListNode node in nodes)
            {
                if ((Type)node.Tag.GetType() == typeof(CashFlowGroup))
                {
                    var group = (CashFlowGroup)node.Tag;
                    if (group.Id == cashFlowGroup.Id)
                        return true;
                }
                if ((Type)node.Tag.GetType() == typeof(CashFlowModel) || (Type)node.Tag.GetType() == typeof(CashFlowGroup))
                {
                    if(TreeListModelContainsGroup(cashFlowGroup, node.Nodes))
                        return true;
                }
            }
            return found;
        }

        private int MoveGroupNodeToModel(TreeListNode parentNode, TreeListNode node)
        {
            var newGroup = node.Tag as CashFlowGroup;
            
            TreeListNode newGroupNode = parentNode.Nodes.Add(new object[] { newGroup.Name });
            newGroupNode.SelectImageIndex = newGroupNode.StateImageIndex = newGroupNode.ImageIndex = 8;
            newGroupNode.Tag = newGroup;
            
            foreach (TreeListNode treeListNode in node.Nodes)
            {
                if (treeListNode.Tag.GetType() == typeof(CashFlowItem))
                {
                    var item = treeListNode.Tag as CashFlowItem;
                    TreeListNode newNode = newGroupNode.Nodes.Add(new object[] { item.Name });
                    newNode.SelectImageIndex = newNode.StateImageIndex = newNode.ImageIndex = 10;
                    newNode.Tag = item;
                }
            }

            trlModels.SetNodeIndex(newGroupNode, 0);

            return trlModels.GetNodeIndex(newGroupNode);
        }

        private void RemoveGroupFromModel(TreeListNode parentNode, long cashFlowNodelId)
        {
            foreach (TreeListNode treeListNode in parentNode.Nodes)
            {
                if(treeListNode.Tag.GetType() == typeof(CashFlowGroup))
                {
                    RemoveGroupFromModel(treeListNode, cashFlowNodelId);
                }
            }

            CashFlowModelGroup cfmg =
                _modelGroups.Where(
                    a =>
                    a.ModelId == cashFlowNodelId &&
                    a.GroupId == ((CashFlowGroup) parentNode.Tag).Id).Single();

            _modelGroups.Remove(cfmg);
        }

        private void MoveGroupUp()
        {
            if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup) &&
                trlModels.FocusedNode.PrevNode != null &&
                trlModels.FocusedNode.PrevNode.Tag.GetType() == typeof(CashFlowGroup))
            {
                int focusedNodeIndex = trlModels.GetNodeIndex(trlModels.FocusedNode);
                int previousNodeIndex = trlModels.GetNodeIndex(trlModels.FocusedNode.PrevNode);

                var focusedCashFlowModel = trlModels.FocusedNode.RootNode.Tag as CashFlowModel;
                var focusedGroup = trlModels.FocusedNode.Tag as CashFlowGroup;
                var previousFocusedGroup = trlModels.FocusedNode.PrevNode.Tag as CashFlowGroup;

                CashFlowModelGroup modelGroupAssoc = _modelGroups.Where(a => a.ModelId == focusedCashFlowModel.Id && a.GroupId == focusedGroup.Id).Single();
                modelGroupAssoc.Order = previousNodeIndex;

                modelGroupAssoc = _modelGroups.Where(a => a.ModelId == focusedCashFlowModel.Id && a.GroupId == previousFocusedGroup.Id).Single();
                modelGroupAssoc.Order = focusedNodeIndex;

                trlModels.SetNodeIndex(trlModels.FocusedNode.PrevNode, trlModels.GetNodeIndex(trlModels.FocusedNode));
            }
        }

        private void MoveGroupDown()
        {
            if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup) && trlModels.FocusedNode.NextNode != null && trlModels.FocusedNode.NextNode.Tag.GetType() == typeof(CashFlowGroup))
            {
                int focusedNodeIndex = trlModels.GetNodeIndex(trlModels.FocusedNode);
                int nextNodeIndex = trlModels.GetNodeIndex(trlModels.FocusedNode.NextNode);

                var focusedCashFlowModel = trlModels.FocusedNode.RootNode.Tag as CashFlowModel;
                var focusedGroup = trlModels.FocusedNode.Tag as CashFlowGroup;
                var nextFocusedGroup = trlModels.FocusedNode.NextNode.Tag as CashFlowGroup;

                CashFlowModelGroup modelGroupAssoc = _modelGroups.Where(a => a.ModelId == focusedCashFlowModel.Id && a.GroupId == focusedGroup.Id).Single();
                modelGroupAssoc.Order = nextNodeIndex;

                modelGroupAssoc = _modelGroups.Where(a => a.ModelId == focusedCashFlowModel.Id && a.GroupId == nextFocusedGroup.Id).Single();
                modelGroupAssoc.Order = focusedNodeIndex;

                trlModels.SetNodeIndex(trlModels.FocusedNode, nextNodeIndex);
            }
        }

        private void MoveItemUp()
        {
            int focusedNodeIndex = trlGroups.GetNodeIndex(trlGroups.FocusedNode);
            int previousNodeIndex = trlGroups.GetNodeIndex(trlGroups.FocusedNode.PrevNode);

            var focusedCashFlowGroup = trlGroups.FocusedNode.RootNode.Tag as CashFlowGroup;
            var focusedItem = trlGroups.FocusedNode.Tag as CashFlowItem;
            var previousFocusedItem = trlGroups.FocusedNode.PrevNode.Tag as CashFlowItem;

            CashFlowGroupItem groupItemAssoc = _groupsItems.Where(a => a.GroupId == focusedCashFlowGroup.Id && a.ItemId == focusedItem.Id).Single();
            groupItemAssoc.Order = previousNodeIndex;

            groupItemAssoc = _groupsItems.Where(a => a.GroupId == focusedCashFlowGroup.Id && a.ItemId == previousFocusedItem.Id).Single();
            groupItemAssoc.Order = focusedNodeIndex;
        }

        private void MoveItemDown()
        {
            int focusedNodeIndex = trlGroups.GetNodeIndex(trlGroups.FocusedNode);
            int nextNodeIndex = trlGroups.GetNodeIndex(trlGroups.FocusedNode.NextNode);

            var focusedCashFlowGroup = trlGroups.FocusedNode.RootNode.Tag as CashFlowGroup;
            var focusedItem = trlGroups.FocusedNode.Tag as CashFlowItem;
            var nextFocusedItem = trlGroups.FocusedNode.NextNode.Tag as CashFlowItem;

            CashFlowGroupItem groupItemAssoc =
                _groupsItems.Where(a => a.GroupId == focusedCashFlowGroup.Id && a.ItemId == focusedItem.Id).Single();
            groupItemAssoc.Order = nextNodeIndex;

            groupItemAssoc =
                _groupsItems.Where(a => a.GroupId == focusedCashFlowGroup.Id && a.ItemId == nextFocusedItem.Id).Single
                    ();
            groupItemAssoc.Order = focusedNodeIndex;
        }

        private void UpdateGroupNameInModels(CashFlowGroup cashFlowGroup, TreeListNodes nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if ((Type)node.Tag.GetType() == typeof(CashFlowGroup))
                {
                    var group = (CashFlowGroup)node.Tag;
                    if (group.Id == cashFlowGroup.Id)
                    {
                        group.Name = cashFlowGroup.Name;
                        node.SetValue("CashFlowObject", group.Name);
                    }
                }
                if ((Type)node.Tag.GetType() == typeof(CashFlowModel) || (Type)node.Tag.GetType() == typeof(CashFlowGroup))
                {
                    UpdateGroupNameInModels(cashFlowGroup, node.Nodes);
                }
            }
        }

        private void SetModelTreeListButtonsEnabled()
        {
            foreach (var button in btnAddRemoveModel.Properties.Buttons)
            {
                string buttonName = ((EditorButton)button).Tag.ToString();

                if(buttonName == "Add")
                    ((EditorButton) button).Enabled = true;

                if (buttonName == "Edit" || buttonName == "Delete" || buttonName == "Status")
                {
                    ((EditorButton)button).Enabled = (trlModels.FocusedNode != null &&
                                                       trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel));
                }
                if (buttonName == "MoveUp")
                {
                    ((EditorButton)button).Enabled = (trlModels.FocusedNode != null &&
                                                       trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup) &&
                                                       trlModels.FocusedNode.PrevNode != null &&
                                                       trlModels.FocusedNode.PrevNode.Tag.GetType() == typeof(CashFlowGroup));
                }
                if (buttonName == "MoveDown")
                {
                    ((EditorButton)button).Enabled = (trlModels.FocusedNode != null &&
                                                       trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup) &&
                                                       trlModels.FocusedNode.NextNode != null &&
                                                       trlModels.FocusedNode.NextNode.Tag.GetType() == typeof(CashFlowGroup));
                }
                if (buttonName == "Close")
                {
                    ((EditorButton)button).Enabled = false;
                }
            }

            btnChangeOperator.Enabled = btnRemoveGroupFromModel.Enabled = (trlModels.FocusedNode != null &&
                                                                           trlModels.FocusedNode.Tag.GetType() ==
                                                                           typeof(CashFlowGroup));

            btnMoveGroupToModel.Enabled = (trlModels.FocusedNode != null &&
                                           (trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel) ||
                                            trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup)));
        }

        private void SetGroupTreeListButtonsEnabled()
        {
            foreach (var button in btnAddRemoveGroup.Properties.Buttons)
            {
                string buttonName = ((EditorButton)button).Tag.ToString();

                if (buttonName == "Add")
                    ((EditorButton)button).Enabled = true;

                if (buttonName == "Edit" || buttonName == "Delete" || buttonName == "Status")
                {
                    ((EditorButton)button).Enabled = (trlGroups.FocusedNode != null &&
                                                       trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowGroup));
                }
                if (buttonName == "MoveUp")
                {
                    ((EditorButton)button).Enabled = (trlGroups.FocusedNode != null &&
                                                       trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowItem) &&
                                                       trlGroups.FocusedNode.PrevNode != null &&
                                                       trlGroups.FocusedNode.PrevNode.Tag.GetType() == typeof(CashFlowItem));
                }
                if (buttonName == "MoveDown")
                {
                    ((EditorButton)button).Enabled = (trlGroups.FocusedNode != null &&
                                                       trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowItem) &&
                                                       trlGroups.FocusedNode.NextNode != null &&
                                                       trlGroups.FocusedNode.NextNode.Tag.GetType() == typeof(CashFlowItem));
                }
                if (buttonName == "Close")
                {
                    ((EditorButton)button).Enabled = false;
                }
            }

            btnMoveGroupToModel.Enabled = (trlModels.FocusedNode != null &&
                                           (trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel) ||
                                            trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup)));

            btnRemoveFromGroup.Enabled = (trlGroups.FocusedNode != null &&
                                              trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowItem));

            btnMoveToGroup.Enabled = (trlGroups.FocusedNode != null &&
                                              trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowGroup));
        }

        #endregion

        #region Proxy Calls

        private void BeginGetInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVCashFlowInitializationData(EndGetInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<CashFlowModel> cashFlowModels;
            List<CashFlowGroup> cashFlowGroups;
            List<CashFlowItem> cashFlowItems;
            List<CashFlowModelGroup> cashFlowModelGroups;
            List<CashFlowGroupItem> cashFlowGroupsItems;
            try
            {
                result = SessionRegistry.Client.EndAEVCashFlowInitializationData(out cashFlowModels, out cashFlowGroups,
                                                                                 out cashFlowItems,
                                                                                 out cashFlowModelGroups, out cashFlowGroupsItems, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                chklItems.Tag = cashFlowItems;

                _modelGroups = cashFlowModelGroups;
                _groupsItems = cashFlowGroupsItems;
                _models = cashFlowModels;
                _groups = cashFlowGroups;

                LoadData();
                
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginGetCashFlowItems()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetCashFlowItems(EndGetCashFlowItems, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetCashFlowItems(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetCashFlowItems;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<CashFlowItem> cashFlowItems;
            try
            {
                result = SessionRegistry.Client.EndGetCashFlowItems(out cashFlowItems, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                chklItems.Tag = cashFlowItems;
                chklItems.Items.Clear();
                chklItems.DataSource = cashFlowItems;

                LoadData();
                
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAECashFlowEntities(List<CashFlowItem> cashFlowItems, List<CashFlowGroup> cashFlowGroups, List<CashFlowModel> cashFlowModels, List<CashFlowGroupItem> cashFlowGroupItems, List<CashFlowModelGroup> cashFlowModelsGroups)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAECashFlowEntities(cashFlowItems, cashFlowGroups, cashFlowModels, cashFlowGroupItems, cashFlowModelsGroups, EndAECashFlowEntities, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAECashFlowEntities(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAECashFlowEntities;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndAECashFlowEntities(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }

            if (result == null) //Exception Occurred
            {
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                if (OnDataSaved != null)
                    OnDataSaved(this, false);
            }
        }

        #endregion

        #region GUI Event Handlers

        private void CashFlowManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetInitializationData();
        }

        private void BtnAddRemoveModelButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var btnModel = sender as ButtonEdit;
            if ((string)e.Button.Tag == "Add")
            {
                foreach (EditorButton button in btnModel.Properties.Buttons)
                {
                    button.Enabled = (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close");
                }
                
                btnAddRemoveModel.Properties.TextEditStyle = TextEditStyles.Standard;
                btnAddRemoveModel.Focus();
                btnAddRemoveModel.Tag = "Add";

                trlModels.Focus();
                trlModels.Enabled = false;
                btnAddRemoveModel.Focus();
            }
            else if ((string)e.Button.Tag == "Delete")
            {
                if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel))
                {
                    DialogResult result = XtraMessageBox.Show(this,
                                                              "Are you sure you want to delete selected model?",
                                                              Strings.Freight_Metrics,
                                                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result != DialogResult.Yes)
                        return;
                    var cashFlowModel = trlModels.FocusedNode.Tag as CashFlowModel;
                    _models.Remove(cashFlowModel);
                    _modelGroups.RemoveAll(a => a.ModelId == cashFlowModel.Id);

                    trlModels.DeleteNode(trlModels.FocusedNode);
                }
            }
            else if((string)e.Button.Tag == "Status")
            {
                if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel))
                {
                    ((CashFlowModel) trlModels.FocusedNode.Tag).Status =
                        ((CashFlowModel) trlModels.FocusedNode.Tag).Status == ActivationStatusEnum.Active
                            ? ActivationStatusEnum.Inactive
                            : ActivationStatusEnum.Active;
                }
            }
            else if((string)e.Button.Tag == "MoveUp")
            {
                MoveGroupUp();
            }
            else if ((string)e.Button.Tag == "MoveDown")
            {
                MoveGroupDown();
            }
            else if ((string)e.Button.Tag == "Edit")
            {
                if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel))
                {
                    btnModel.Tag = "Edit";
                    btnModel.Properties.TextEditStyle = TextEditStyles.Standard;
                    btnModel.EditValue = ((CashFlowModel)trlModels.FocusedNode.Tag).Name;
                    trlModels.Enabled = false;

                    foreach (EditorButton button in btnModel.Properties.Buttons)
                    {
                        button.Enabled = (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close");
                    }
                    btnModel.Focus();
                }
            }
            else if ((string)e.Button.Tag == "Save")
            {
                if (btnModel.Tag == "Add")
                {
                    if (btnModel.EditValue == null || string.IsNullOrEmpty(btnModel.EditValue.ToString().Trim()))
                    {
                        errorProvider.SetError(btnModel, "Please enter the name of the model");
                        return;
                    }

                    TreeListNode newNode = trlModels.AppendNode(new object[] { btnModel.EditValue.ToString() }, null, null);
                    newNode.ImageIndex = newNode.SelectImageIndex = newNode.StateImageIndex = 7;

                    var newCashFlowModel = new CashFlowModel()
                    {
                        Id = counter,
                        Name = btnModel.EditValue.ToString(),
                        Status = ActivationStatusEnum.Active
                    };
                    counter--;

                    _models.Add(newCashFlowModel);

                    newNode.Tag = newCashFlowModel;
                    trlModels.MakeNodeVisible(newNode);
                    btnModel.EditValue = null;


                    foreach (EditorButton button in btnModel.Properties.Buttons)
                    {
                        if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                        {
                            button.Enabled = false;
                        }
                    }
                    SetModelTreeListButtonsEnabled();

                    btnModel.Tag = "";
                    trlModels.Enabled = true;
                }
                else
                {
                    btnAddRemoveModel.Tag = "";
                    if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel))
                    {
                        if (btnModel.EditValue == null || string.IsNullOrEmpty(btnModel.EditValue.ToString()))
                        {
                            errorProvider.SetError(btnAddRemoveModel, "Please enter the name of the model");
                            return;
                        }

                        var focusedCashFlowModel = trlModels.FocusedNode.Tag as CashFlowModel;
                        focusedCashFlowModel.Name = btnModel.EditValue.ToString().Trim();
                        trlModels.FocusedNode.Tag = focusedCashFlowModel;

                        trlModels.FocusedNode.SetValue("CashFlowObject", btnModel.EditValue.ToString().Trim());

                        btnModel.EditValue = null;
                        trlModels.Enabled = true;

                        foreach (EditorButton button in btnModel.Properties.Buttons)
                        {
                            if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                            {
                                button.Enabled = false;
                            }
                        }
                        SetModelTreeListButtonsEnabled();
                    }
                }
                btnModel.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            }
            else if ((string)e.Button.Tag == "Close")
            {
                foreach (EditorButton button in btnModel.Properties.Buttons)
                {
                    if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                    {
                        button.Enabled = false;
                    }
                }
                SetModelTreeListButtonsEnabled();

                btnModel.EditValue = null;
                btnModel.Tag = "";
                btnModel.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                trlModels.Enabled = true;

                errorProvider.ClearErrors();
            }
        }

        private void BtnAddRemoveGroupButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var btnGroup = sender as ButtonEdit;

            if ((string)e.Button.Tag == "Add")
            {
                btnGroup.Tag = "Add";
                btnGroup.Properties.TextEditStyle = TextEditStyles.Standard;
                btnGroup.Focus();
                foreach (EditorButton button in btnGroup.Properties.Buttons)
                {
                    button.Enabled = (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close");
                }

                trlGroups.Focus();
                trlGroups.Enabled = false;
                btnGroup.Focus();
            }
            else if((string) e.Button.Tag == "Delete")
            {
                if (trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
                {
                    DialogResult result = XtraMessageBox.Show(this,
                                                              "Are you sure you want to delete selected group?",
                                                              Strings.Freight_Metrics,
                                                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (result != DialogResult.Yes)
                        return;
                    if (TreeListModelContainsGroup((CashFlowGroup) trlGroups.FocusedNode.Tag, trlModels.Nodes))
                    {
                        XtraMessageBox.Show(this,
                                            "Cannot delete this Cash Flow Group because is used in one or more Cash Flow Models.",
                                            Strings.Freight_Metrics,
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var focusedGroup = trlGroups.FocusedNode.Tag as CashFlowGroup;
                    _groups.Remove(focusedGroup);
                    _groupsItems.RemoveAll(a => a.GroupId == focusedGroup.Id);
                    trlGroups.DeleteNode(trlGroups.FocusedNode);
                }
            }
            else if(e.Button.Tag.ToString() == "MoveUp")
            {
                if(trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowItem) && trlGroups.FocusedNode.PrevNode != null && trlGroups.FocusedNode.PrevNode.Tag.GetType() == typeof(CashFlowItem))
                {
                    MoveItemUp();
                    trlGroups.SetNodeIndex(trlGroups.FocusedNode.PrevNode, trlGroups.GetNodeIndex(trlGroups.FocusedNode));
                }
            }
            else if(e.Button.Tag.ToString()== "MoveDown")
            {
                if(trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowItem) && trlGroups.FocusedNode.NextNode != null && trlGroups.FocusedNode.NextNode.Tag.GetType() == typeof(CashFlowItem))
                {
                    MoveItemDown();
                    trlGroups.SetNodeIndex(trlGroups.FocusedNode, trlGroups.GetNodeIndex(trlGroups.FocusedNode.NextNode));
                }
            }
            else if ((string)e.Button.Tag == "Status")
            {
                if (trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
                {
                    ((CashFlowGroup)trlGroups.FocusedNode.Tag).Status =
                        ((CashFlowGroup)trlGroups.FocusedNode.Tag).Status == ActivationStatusEnum.Active
                            ? ActivationStatusEnum.Inactive
                            : ActivationStatusEnum.Active;
                }
            }
            else if ((string)e.Button.Tag == "Edit")
            {
                if (trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
                {
                    btnGroup.Tag = "Edit";
                    btnGroup.Properties.TextEditStyle = TextEditStyles.Standard;

                    btnGroup.EditValue = ((CashFlowGroup) trlGroups.FocusedNode.Tag).Name;
                    trlGroups.Enabled = false;

                    foreach (EditorButton button in btnGroup.Properties.Buttons)
                    {
                        if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                            button.Enabled = true;
                    }
                }
            }
            else if ((string)e.Button.Tag == "Save")
            {
                if (btnGroup.Tag == "Add")
                {
                    if (btnGroup.EditValue == null || string.IsNullOrEmpty(btnGroup.EditValue.ToString().Trim()))
                    {
                        errorProvider.SetError(btnGroup, "Please enter the name of the group");
                        return;
                    }

                    TreeListNode newNode = trlGroups.AppendNode(new object[] { btnGroup.EditValue.ToString().Trim() }, null, null);
                    newNode.ImageIndex = newNode.SelectImageIndex = newNode.StateImageIndex = 11;

                    var newCashFlowGroup = new CashFlowGroup()
                    {
                        Id = counter,
                        Name = btnGroup.EditValue.ToString(),
                        Status = ActivationStatusEnum.Active
                    };
                    counter--;

                    _groups.Add(newCashFlowGroup);

                    newNode.Tag = newCashFlowGroup;
                    trlGroups.MakeNodeVisible(newNode);
                    btnGroup.EditValue = null;

                    foreach (EditorButton button in btnGroup.Properties.Buttons)
                    {
                        if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                            button.Enabled = false;
                    }
                    SetGroupTreeListButtonsEnabled();

                    btnGroup.Tag = "";
                    trlGroups.Enabled = true;
                }
                else
                {
                    if (trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof (CashFlowGroup))
                    {
                        if (btnGroup.EditValue == null || string.IsNullOrEmpty(btnGroup.EditValue.ToString()))
                        {
                            errorProvider.SetError(btnAddRemoveGroup, "Please enter the name of the group");
                            return;
                        }

                        var focusedCashFlowGroup = trlGroups.FocusedNode.Tag as CashFlowGroup;
                        focusedCashFlowGroup.Name = btnGroup.EditValue.ToString().Trim();
                        trlGroups.FocusedNode.Tag = focusedCashFlowGroup;
                        trlGroups.FocusedNode.SetValue("CashFlowObjectName", btnGroup.EditValue.ToString().Trim());

                        UpdateGroupNameInModels(focusedCashFlowGroup, trlModels.Nodes);

                        btnGroup.EditValue = null;
                        trlGroups.Enabled = true;

                        foreach (EditorButton button in btnGroup.Properties.Buttons)
                        {
                            if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                                button.Enabled = false;
                        }
                        SetGroupTreeListButtonsEnabled();
                        btnGroup.Tag = "";
                    }
                }
                btnGroup.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            }
            else if ((string)e.Button.Tag == "Close")
            {
                foreach (EditorButton button in btnGroup.Properties.Buttons)
                {
                    if (button.Tag.ToString() == "Save" || button.Tag.ToString() == "Close")
                        button.Enabled = false;
                }
                SetGroupTreeListButtonsEnabled();

                btnGroup.EditValue = null;
                btnGroup.Tag = "";
                btnGroup.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                trlGroups.Enabled = true;

                errorProvider.ClearErrors();
            }
        }

        private void BtnItemButtonsButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var btnGroup = sender as ButtonEdit;

            if ((string)e.Button.Tag == "Add")
            {
                if (AddEditViewCashFlowItemEvent != null)
                    AddEditViewCashFlowItemEvent(FormActionTypeEnum.Add, null);
            }
            else if ((string)e.Button.Tag == "Delete")
            {
                if (chklItems.CheckedItems.Count > 0)
                {
                    DialogResult result = XtraMessageBox.Show(this,
                                                              "Are you sure you want to delete selected item(s)?",
                                                              Strings.Freight_Metrics,
                                                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if(result != DialogResult.Yes)
                        return;
                    
                    var cashFlowItems = (List<CashFlowItem>)chklItems.Tag;
                    BaseCheckedListBoxControl.CheckedItemCollection checkedItems = chklItems.CheckedItems;
                    foreach (var checkedItem in checkedItems)
                    {
                        var item = checkedItem as CashFlowItem;

                        if (!item.IsCustom)
                        {
                            XtraMessageBox.Show(this,
                                           "Cannot delete System Cash Flow Item '" + item.Name + "'",
                                           Strings.Freight_Metrics,
                                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                        if (_groupsItems.Where(a => a.ItemId == item.Id).Any())
                        {
                            XtraMessageBox.Show(this,
                                            "Cannot delete this Cash Flow Item '"+ item.Name + "' because is used in a Cash Flow Group.",
                                            Strings.Freight_Metrics,
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                        cashFlowItems.Remove(item);
                    }
                    chklItems.Items.Clear();
                    chklItems.DataSource = cashFlowItems.OrderBy(a=>a.Name).ToList();
                }
            }
            else if ((string)e.Button.Tag == "Edit")
            {
                if (AddEditViewCashFlowItemEvent != null && chklItems.CheckedItems.Count > 0)
                {
                    var cashFlowItem = (CashFlowItem)chklItems.SelectedItem;
                    if (cashFlowItem.SystemType != null)
                    {
                        XtraMessageBox.Show(this,
                                        "System Type Cash Flow Items cannot be edited or viewed.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    AddEditViewCashFlowItemEvent(FormActionTypeEnum.Edit, cashFlowItem);
                }
            }
            else if ((string)e.Button.Tag == "View")
            {
                if (AddEditViewCashFlowItemEvent != null)
                {
                    var cashFlowItem = (CashFlowItem)chklItems.SelectedItem;
                    if (cashFlowItem.SystemType != null)
                    {
                        XtraMessageBox.Show(this,
                                        "System Type Cash Flow Items cannot be edited or viewed.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    AddEditViewCashFlowItemEvent(FormActionTypeEnum.View, cashFlowItem);
                }
            }
        }

        private void BtnMoveItemToGroupClick(object sender, EventArgs e)
        {
            if(chklItems.CheckedItems.Count > 0 && trlGroups.FocusedNode != null && (Type) trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
            {
                foreach (CashFlowItem item in chklItems.CheckedItems)
                {
                    CashFlowGroupItem cfg = _groupsItems.Where(a => a.ItemId == item.Id).SingleOrDefault();

                    if(cfg == null)
                    {
                        TreeListNode newNode = trlGroups.AppendNode(new object[] { item.Name }, trlGroups.FocusedNode, item);
                        newNode.ImageIndex = newNode.SelectImageIndex = newNode.StateImageIndex = 10;

                        _groupsItems.Add(new CashFlowGroupItem()
                                             {
                                                 GroupId = ((CashFlowGroup) trlGroups.FocusedNode.Tag).Id,
                                                 Id = counter,
                                                 ItemId = item.Id,
                                                 Order = trlGroups.GetNodeIndex(newNode)
                                             });
                        counter--;
                        LoadModels();
                    }
                    else
                    {
                        XtraMessageBox.Show(this,
                                    "Cannot add item '"+ item.Name +"' in selected group. The item already exists in group '" + _groups.Where(a => a.Id == cfg.GroupId).Single().Name + "'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                        continue;
                    }
                }
            }
            chklItems.UnCheckAll();
        }

        private void BtnRemoveItemFromGroupClick(object sender, EventArgs e)
        {
            if (trlGroups.FocusedNode != null && (Type)trlGroups.FocusedNode.Tag.GetType() == typeof(CashFlowItem))
            {
                CashFlowGroupItem cashFlowGroupItem =
                    _groupsItems.Where(
                        a =>
                        a.GroupId == ((CashFlowGroup) trlGroups.FocusedNode.ParentNode.Tag).Id &&
                        a.ItemId == ((CashFlowItem) trlGroups.FocusedNode.Tag).Id).Single();
                _groupsItems.Remove(cashFlowGroupItem);

                trlGroups.DeleteNode(trlGroups.FocusedNode);

                LoadModels();
            }
        }

        private void BtnMoveGroupToModelClick(object sender, EventArgs e)
        {
            if (trlGroups.FocusedNode != null && trlGroups.FocusedNode.Tag.GetType() == typeof (CashFlowGroup) &&
                trlModels.FocusedNode != null && (trlModels.FocusedNode.Tag.GetType() == typeof (CashFlowGroup) || trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel)))
            {
                if ((Type)trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel) && trlModels.FocusedNode.Nodes.Count > 0)//It means there is already a root group
                {
                    XtraMessageBox.Show(this,
                                    "Cannot perform action. There is already a root group for the selected model.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var cashFlowGroup = trlGroups.FocusedNode.Tag as CashFlowGroup;

                //Check if the items of the group already exist in the selected model group
                if (TreeListModelContainsGroup(cashFlowGroup, trlModels.FocusedNode.RootNode.Nodes))
                {
                    XtraMessageBox.Show(this,
                                    "Cannot perform action. The group already exists in selected model.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int newNodeIndex = MoveGroupNodeToModel(trlModels.FocusedNode, trlGroups.FocusedNode);

                //Add the new association in List
                
                if(trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowModel))
                {
                    var cashFlowModel = trlModels.FocusedNode.Tag as CashFlowModel;
                    _modelGroups.Add(new CashFlowModelGroup()
                                         {
                                             GroupId = cashFlowGroup.Id,
                                             Id = 0,
                                             ModelId = cashFlowModel.Id,
                                             ParentGroupId = null,
                                             Operator = CashFlowModelGroupOperatorEnum.Addition,
                                             Order = newNodeIndex
                                         });
                }
                else if (trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
                {
                    var cashFlowGroupParent = trlModels.FocusedNode.Tag as CashFlowGroup;
                    var cashFlowModel = trlModels.FocusedNode.RootNode.Tag as CashFlowModel;

                    _modelGroups.Add(new CashFlowModelGroup()
                                         {
                                             GroupId = cashFlowGroup.Id,
                                             Id = 0,
                                             ModelId = cashFlowModel.Id,
                                             ParentGroupId = cashFlowGroupParent.Id,
                                             Operator = CashFlowModelGroupOperatorEnum.Addition,
                                             Order = newNodeIndex
                                         });
                }
            }
        }

        private void BtnRemoveGroupFromModelClick(object sender, EventArgs e)
        {
            if (trlModels.FocusedNode != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
            {
                //Remove the new association from List
                var cashFlowModel = trlModels.FocusedNode.RootNode.Tag as CashFlowModel;

                RemoveGroupFromModel(trlModels.FocusedNode, cashFlowModel.Id);
                
                trlModels.DeleteNode(trlModels.FocusedNode);
            }
        }

        private void BtnSaveClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            #region Get Cash Flow Groups

            //var cashFlowGroups = new List<CashFlowGroup>();
            //var cashFlowGroupsItems = new List<CashFlowGroupItem>();

            //foreach (TreeListNode treeListNode in trlGroups.Nodes)
            //{
            //    if (treeListNode.Tag.GetType() == typeof(CashFlowGroup))
            //    {
            //        var cashFlowGroup = treeListNode.Tag as CashFlowGroup;
            //        cashFlowGroup.Items = new List<CashFlowItem>();

            //        foreach (TreeListNode listNode in treeListNode.Nodes)
            //        {
            //            var cashFlowItem = listNode.Tag as CashFlowItem;
            //            var newCashFlowGroupItem = new CashFlowGroupItem()
            //                                                         {
            //                                                             GroupId = cashFlowGroup.Id,
            //                                                             ItemId = cashFlowItem.Id,
            //                                                             Order = trlGroups.GetNodeIndex(listNode)
            //                                                         };
            //            cashFlowGroupsItems.Add(newCashFlowGroupItem);
            //        }
            //        cashFlowGroups.Add(cashFlowGroup);
            //    }
            //}
            #endregion

            BeginAECashFlowEntities((List<CashFlowItem>)chklItems.DataSource, _groups, _models, _groupsItems, _modelGroups);
        }

        private void BtnCloseClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void TrlModelsCustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e)
        {
            if (e.Node.Tag.GetType() == typeof(CashFlowModel) || e.Node.Tag.GetType() == typeof(CashFlowGroup))
            {
                if(e.Node.Tag.GetType() == typeof(CashFlowModel))
                {
                    var cashFlowModel = e.Node.Tag as CashFlowModel;
                    e.Appearance.Font = cashFlowModel.Status == ActivationStatusEnum.Active
                                            ? new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size,
                                                       FontStyle.Bold)
                                            : new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size, FontStyle.Regular);

                    if (cashFlowModel.Status == ActivationStatusEnum.Inactive)
                        e.Appearance.ForeColor = Color.Gray;
                }
            }
        }

        private void TrlGroupsCustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e)
        {
            if (e.Node.Tag.GetType() == typeof(CashFlowGroup))
            {
                var cashFlowgroup = e.Node.Tag as CashFlowGroup;
                e.Appearance.Font = cashFlowgroup.Status == ActivationStatusEnum.Active
                                        ? new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size,
                                                   FontStyle.Bold)
                                        : new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size,
                                                   FontStyle.Regular);

                if (cashFlowgroup.Status == ActivationStatusEnum.Inactive)
                    e.Appearance.ForeColor = Color.Gray;
            }
        }

        private void BtnChangeOperatorClick(object sender, EventArgs e)
        {
            if(trlModels.FocusedNode != null && trlModels.FocusedNode.Tag != null && trlModels.FocusedNode.Tag.GetType() == typeof(CashFlowGroup))
            {
                trlModels.FocusedNode.StateImageIndex =
                    trlModels.FocusedNode.SelectImageIndex =
                    trlModels.FocusedNode.ImageIndex = (trlModels.FocusedNode.StateImageIndex == 8 ? 9 : 8);

                var cfm = trlModels.FocusedNode.RootNode.Tag as CashFlowModel;
                var cfg = trlModels.FocusedNode.Tag as CashFlowGroup;

                CashFlowModelGroup cfmg = _modelGroups.Where(a => a.ModelId == cfm.Id && a.GroupId == cfg.Id).Single();
                cfmg.Operator =
                    trlModels.FocusedNode.ImageIndex == 8
                        ? CashFlowModelGroupOperatorEnum.Addition
                        : CashFlowModelGroupOperatorEnum.Subtraction;
            }
        }

        private void TrlModelsFocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            SetModelTreeListButtonsEnabled();
        }

        private void TrlGroupsFocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            SetGroupTreeListButtonsEnabled();
        }

        #endregion

        #region Public Methods

        public void RefreshCashFlowItems()
        {
            BeginGetCashFlowItems();
        }

        #endregion

        #region Events

        public event Action<FormActionTypeEnum, CashFlowItem> AddEditViewCashFlowItemEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

    }
}