﻿namespace Exis.WinClient.Controls
{
    partial class VesselPoolsManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VesselPoolsManagementForm));
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridVesselPools = new DevExpress.XtraGrid.GridControl();
            this.gridVesselPoolsMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutGroupVesselPools = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutVesselPools = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnVessels = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnAdd = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnView = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridVesselPools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridVesselPoolsMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupVesselPools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVesselPools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "exit.ico");
            this.imageList24.Images.SetKeyName(4, "viewItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "management_24x24.png");
            // 
            // gridVesselPools
            // 
            this.gridVesselPools.Location = new System.Drawing.Point(14, 33);
            this.gridVesselPools.MainView = this.gridVesselPoolsMainView;
            this.gridVesselPools.Name = "gridVesselPools";
            this.gridVesselPools.Size = new System.Drawing.Size(1016, 534);
            this.gridVesselPools.TabIndex = 9;
            this.gridVesselPools.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridVesselPoolsMainView});
            // 
            // gridVesselPoolsMainView
            // 
            this.gridVesselPoolsMainView.GridControl = this.gridVesselPools;
            this.gridVesselPoolsMainView.Name = "gridVesselPoolsMainView";
            this.gridVesselPoolsMainView.OptionsBehavior.Editable = false;
            this.gridVesselPoolsMainView.OptionsCustomization.AllowGroup = false;
            this.gridVesselPoolsMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridVesselPoolsMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridVesselPoolsMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridVesselPoolsMainView.OptionsView.ShowDetailButtons = false;
            this.gridVesselPoolsMainView.OptionsView.ShowGroupPanel = false;
            this.gridVesselPoolsMainView.OptionsView.ShowIndicator = false;
            // 
            // layoutGroupVesselPools
            // 
            this.layoutGroupVesselPools.CustomizationFormText = "Information";
            this.layoutGroupVesselPools.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutVesselPools});
            this.layoutGroupVesselPools.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupVesselPools.Name = "layoutGroupVesselPools";
            this.layoutGroupVesselPools.Size = new System.Drawing.Size(1044, 581);
            this.layoutGroupVesselPools.Text = "Vessel Pools";
            // 
            // layoutVesselPools
            // 
            this.layoutVesselPools.Control = this.gridVesselPools;
            this.layoutVesselPools.CustomizationFormText = "layoutTraders";
            this.layoutVesselPools.Location = new System.Drawing.Point(0, 0);
            this.layoutVesselPools.Name = "layoutVesselPools";
            this.layoutVesselPools.Size = new System.Drawing.Size(1020, 538);
            this.layoutVesselPools.Text = "layoutVesselPools";
            this.layoutVesselPools.TextSize = new System.Drawing.Size(0, 0);
            this.layoutVesselPools.TextToControlDistance = 0;
            this.layoutVesselPools.TextVisible = false;
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.gridVesselPools);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(569, 275, 250, 350);
            this.layoutControl.Root = this.layoutRoot;
            this.layoutControl.Size = new System.Drawing.Size(1044, 581);
            this.layoutControl.TabIndex = 2;
            this.layoutControl.Text = "layoutControl1";
            // 
            // layoutRoot
            // 
            this.layoutRoot.CustomizationFormText = "layoutRoot";
            this.layoutRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRoot.GroupBordersVisible = false;
            this.layoutRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupVesselPools});
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRoot.Size = new System.Drawing.Size(1044, 581);
            this.layoutRoot.Text = "layoutRoot";
            this.layoutRoot.TextVisible = false;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnView,
            this.btnRefresh,
            this.btnClose,
            this.btnVessels});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 6;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnVessels),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnView),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnVessels
            // 
            this.btnVessels.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnVessels.Caption = "Vessels Management";
            this.btnVessels.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnVessels.Id = 5;
            this.btnVessels.LargeImageIndex = 5;
            this.btnVessels.Name = "btnVessels";
            this.btnVessels.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnVessels_ItemClick);
            // 
            // btnAdd
            // 
            this.btnAdd.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnAdd.Caption = "Add";
            this.btnAdd.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnAdd.Id = 0;
            this.btnAdd.LargeImageIndex = 0;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnEdit.Caption = "Edit";
            this.btnEdit.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnEdit.Id = 1;
            this.btnEdit.LargeImageIndex = 1;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_Click);
            // 
            // btnView
            // 
            this.btnView.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnView.Caption = "View";
            this.btnView.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnView.Id = 2;
            this.btnView.LargeImageIndex = 4;
            this.btnView.Name = "btnView";
            this.btnView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnView_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnRefresh.Caption = "Refresh";
            this.btnRefresh.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnRefresh.Id = 3;
            this.btnRefresh.LargeImageIndex = 2;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 4;
            this.btnClose.LargeImageIndex = 3;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1044, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 581);
            this.barDockControlBottom.Size = new System.Drawing.Size(1044, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 581);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1044, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 581);
            // 
            // VesselPoolsManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 613);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "VesselPoolsManagementForm";
            this.Activated += new System.EventHandler(this.VesselPoolsManagementForm_Activated);
            this.Deactivate += new System.EventHandler(this.VesselPoolsManagementForm_Deactivate);
            this.Load += new System.EventHandler(this.VesselPoolsManagementFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridVesselPools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridVesselPoolsMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupVesselPools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVesselPools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraGrid.GridControl gridVesselPools;
        private DevExpress.XtraGrid.Views.Grid.GridView gridVesselPoolsMainView;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupVesselPools;
        private DevExpress.XtraLayout.LayoutControlItem layoutVesselPools;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRoot;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnAdd;
        private DevExpress.XtraBars.BarLargeButtonItem btnEdit;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarLargeButtonItem btnView;
        private DevExpress.XtraBars.BarLargeButtonItem btnVessels;
    }
}