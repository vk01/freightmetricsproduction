﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class BankAccountAEVForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private readonly object _syncObject = new object();
        private Account _bankAccount;
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public BankAccountAEVForm()
        {
            _formActionType = FormActionTypeEnum.Add;
            InitializeComponent();
            InitializeControls();
        }

        public BankAccountAEVForm(Account bankAccount, FormActionTypeEnum formActionType)
        {
            _formActionType = formActionType;
            _bankAccount = bankAccount;
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_formActionType == FormActionTypeEnum.Add)
            {
                layoutGroupUserInfo.Visibility = LayoutVisibility.Never;
            }

            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Active);
            cmbStatus.Properties.Items.Add(ActivationStatusEnum.Inactive);

            lookupCompany.Properties.DisplayMember = "Name";
            lookupCompany.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
                          {
                              Caption = Strings.Company_Name,
                              FieldName = "Name",
                              Width = 0
                          };
            lookupCompany.Properties.Columns.Add(col);
            lookupCompany.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCompany.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCompany.Properties.NullValuePrompt = Strings.Select_a_Company___;

            lookupBank.Properties.DisplayMember = "Name";
            lookupBank.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Bank_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupBank.Properties.Columns.Add(col);
            lookupBank.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupBank.Properties.SearchMode = SearchMode.AutoFilter;
            lookupBank.Properties.NullValuePrompt = Strings.Select_a_Bank___;

            lookupCurrency.Properties.DisplayMember = "Name";
            lookupCurrency.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
                      {
                          Caption = Strings.Currency_Name,
                          FieldName = "Name",
                          Width = 0
                      };
            lookupCurrency.Properties.Columns.Add(col);
            lookupCurrency.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCurrency.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCurrency.Properties.NullValuePrompt = Strings.Select_a_Currency___;

            if (_formActionType == FormActionTypeEnum.View)
            {
                txtName.Properties.ReadOnly = true;
                txtCode.Properties.ReadOnly = true;
                cmbStatus.Properties.ReadOnly = true;
                lookupCompany.Properties.ReadOnly = true;
                lookupBank.Properties.ReadOnly = true;
                lookupCurrency.Properties.ReadOnly = true;
                txtBalance.Properties.ReadOnly = true;

                btnSave.Visibility = BarItemVisibility.Never;
                //layoutSave.Visibility = LayoutVisibility.Never;
            }
        }

 
        private void InitializeData()
        {
            BeginAEVBankAccountInitializationData();
        }

        private void LoadData()
        {
            lblId.Text = _bankAccount.Id.ToString();
            txtName.Text = _bankAccount.Name;
            txtCode.Text = _bankAccount.Code;
            cmbStatus.SelectedItem = _bankAccount.Status;
            lookupCompany.EditValue = _bankAccount.CompanyId;
            lookupBank.EditValue = _bankAccount.BankId;
            lookupCurrency.EditValue = _bankAccount.CurrencyId;

            txtCreationUser.Text = _bankAccount.Cruser;
            dtpCreationDate.DateTime = _bankAccount.Crd;
            txtUpdateUser.Text = _bankAccount.Chuser;
            dtpUpdateDate.DateTime = _bankAccount.Chd;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
                errorProvider.SetError(txtName, Strings.Field_is_mandatory);
            if (String.IsNullOrEmpty(txtCode.Text.Trim()))
                errorProvider.SetError(txtCode, Strings.Field_is_mandatory);
            if (cmbStatus.SelectedItem == null) errorProvider.SetError(cmbStatus, Strings.Field_is_mandatory);
            if (lookupCompany.EditValue == null) errorProvider.SetError(lookupCompany, Strings.Field_is_mandatory);
            if (lookupBank.EditValue == null) errorProvider.SetError(lookupBank, Strings.Field_is_mandatory);
            if (lookupCurrency.EditValue == null) errorProvider.SetError(lookupCurrency, Strings.Field_is_mandatory);
            if(txtBalance.EditValue == null) errorProvider.SetError(txtBalance, Strings.Field_is_mandatory);
            return !errorProvider.HasErrors;
        }

        #endregion

        #region GUI Events

        private void BankAccountAEVForm_Load(object sender, EventArgs e)
        {
            InitializeData();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                _bankAccount = new Account();
            }

            _bankAccount.Name = txtName.Text.Trim();
            _bankAccount.Code = txtCode.Text.Trim();
            _bankAccount.Status = (ActivationStatusEnum) cmbStatus.SelectedItem;
            _bankAccount.CompanyId = (long) lookupCompany.EditValue;
            _bankAccount.BankId = (long) lookupBank.EditValue;
            _bankAccount.CurrencyId = (long) lookupCurrency.EditValue;

            BeginAEVBankAccount(_formActionType != FormActionTypeEnum.Add);
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void BankAccountAEVForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void BankAccountAEVForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginAEVBankAccountInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVBankAccountInitializationData(
                    EndAEVBankAccountInitializationData, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVBankAccountInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVBankAccountInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Company> companies;
            List<Company> banks;
            List<Currency> currencies;

            try
            {
                result = SessionRegistry.Client.EndAEVBankAccountInitializationData(out companies, out banks,
                                                                                    out currencies, ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                lookupCompany.Tag = companies;
                lookupCompany.Properties.DataSource = companies;

                lookupBank.Tag = banks;
                lookupBank.Properties.DataSource = banks;

                lookupCurrency.Tag = currencies;
                lookupCurrency.Properties.DataSource = currencies;

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                    LoadData();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginAEVBankAccount(bool isEdit)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEEntity(isEdit, _bankAccount, EndAEVAccount, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVAccount(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVAccount;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndAEEntity(ar);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }
            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                // Name already exists
                errorProvider.SetError(txtName, Strings.Entity_name_is_already_present_in_the_system_);
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;

                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        #endregion

        #region Public Properties

        public DialogResult Result { get; set; }

        public Account BankAccount
        {
            get { return _bankAccount; }
        }

        public FormActionTypeEnum FormActionType
        {
            get { return _formActionType; }
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion
    }
}