﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls
{
    public partial class CompanySubtypesManagementForm : XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public CompanySubtypesManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void CompanySubtypesManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetCompanySubtypes();
        }

        private void btnAdd_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (InsertEntityEvent != null) InsertEntityEvent(typeof (CompanySubtype));
        }

        private void btnEdit_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridCompanySubtypesMainView.GetFocusedRow() == null) return;

            if (EditEntityEvent != null) EditEntityEvent((CompanySubtype) gridCompanySubtypesMainView.GetFocusedRow());
        }

        private void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridCompanySubtypesMainView.GetFocusedRow() == null) return;

            if (ViewEntityEvent != null) ViewEntityEvent((CompanySubtype) gridCompanySubtypesMainView.GetFocusedRow());
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetCompanySubtypes();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void CompanySubtypesManagementForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void CompanySubtypesManagementForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridCompanySubtypes.BeginUpdate();

            int gridViewColumnindex = 0;

            gridCompanySubtypesMainView.Columns.Clear();

            gridCompanySubtypesMainView.Columns.Add(new GridColumn
                                                        {
                                                            Caption = Strings.Name,
                                                            FieldName = "Name",
                                                            VisibleIndex = gridViewColumnindex++
                                                        });
            gridCompanySubtypesMainView.Columns.Add(new GridColumn
                                                        {
                                                            Caption = Strings.Status,
                                                            FieldName = "Status",
                                                            VisibleIndex = gridViewColumnindex++
                                                        });
            gridCompanySubtypesMainView.Columns.Add(new GridColumn
                                                        {
                                                            Caption = Strings.Subtype,
                                                            FieldName = "Subtype",
                                                            VisibleIndex = gridViewColumnindex
                                                        });
            gridCompanySubtypes.EndUpdate();
        }

        #endregion

        #region Proxy Calls

        private void BeginGetCompanySubtypes()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetEntities(new CompanySubtype(), EndGetCompanySubtypes, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetCompanySubtypes(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetCompanySubtypes;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<DomainObject> entities;
            try
            {
                result = SessionRegistry.Client.EndGetEntities(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridCompanySubtypes.DataSource = entities.Cast<CompanySubtype>().ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion

        #region Events

        public event Action<Type> InsertEntityEvent;
        public event Action<DomainObject> EditEntityEvent;
        public event Action<DomainObject> ViewEntityEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion
    }
}