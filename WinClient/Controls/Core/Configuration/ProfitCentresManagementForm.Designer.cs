﻿namespace Exis.WinClient.Controls
{
    partial class ProfitCentresManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfitCentresManagementForm));
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridProfitCentres = new DevExpress.XtraGrid.GridControl();
            this.gridProfitCentresMainView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutGroupProfitCentres = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutProfitCentres = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnAdd = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnView = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProfitCentres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProfitCentresMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupProfitCentres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProfitCentres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "exit.ico");
            this.imageList24.Images.SetKeyName(4, "viewItem24x24.ico");
            // 
            // gridProfitCentres
            // 
            this.gridProfitCentres.Location = new System.Drawing.Point(14, 33);
            this.gridProfitCentres.MainView = this.gridProfitCentresMainView;
            this.gridProfitCentres.Name = "gridProfitCentres";
            this.gridProfitCentres.Size = new System.Drawing.Size(928, 499);
            this.gridProfitCentres.TabIndex = 9;
            this.gridProfitCentres.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridProfitCentresMainView});
            // 
            // gridProfitCentresMainView
            // 
            this.gridProfitCentresMainView.GridControl = this.gridProfitCentres;
            this.gridProfitCentresMainView.Name = "gridProfitCentresMainView";
            this.gridProfitCentresMainView.OptionsBehavior.Editable = false;
            this.gridProfitCentresMainView.OptionsCustomization.AllowGroup = false;
            this.gridProfitCentresMainView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridProfitCentresMainView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridProfitCentresMainView.OptionsView.EnableAppearanceOddRow = true;
            this.gridProfitCentresMainView.OptionsView.ShowDetailButtons = false;
            this.gridProfitCentresMainView.OptionsView.ShowGroupPanel = false;
            this.gridProfitCentresMainView.OptionsView.ShowIndicator = false;
            // 
            // layoutGroupProfitCentres
            // 
            this.layoutGroupProfitCentres.CustomizationFormText = "Information";
            this.layoutGroupProfitCentres.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutProfitCentres});
            this.layoutGroupProfitCentres.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupProfitCentres.Name = "layoutGroupProfitCentres";
            this.layoutGroupProfitCentres.Size = new System.Drawing.Size(956, 546);
            this.layoutGroupProfitCentres.Text = "Profit Centres";
            // 
            // layoutProfitCentres
            // 
            this.layoutProfitCentres.Control = this.gridProfitCentres;
            this.layoutProfitCentres.CustomizationFormText = "layoutTraders";
            this.layoutProfitCentres.Location = new System.Drawing.Point(0, 0);
            this.layoutProfitCentres.Name = "layoutProfitCentres";
            this.layoutProfitCentres.Size = new System.Drawing.Size(932, 503);
            this.layoutProfitCentres.Text = "layoutProfitCentres";
            this.layoutProfitCentres.TextSize = new System.Drawing.Size(0, 0);
            this.layoutProfitCentres.TextToControlDistance = 0;
            this.layoutProfitCentres.TextVisible = false;
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.gridProfitCentres);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(569, 275, 250, 350);
            this.layoutControl.Root = this.layoutRoot;
            this.layoutControl.Size = new System.Drawing.Size(956, 546);
            this.layoutControl.TabIndex = 2;
            this.layoutControl.Text = "layoutControl1";
            // 
            // layoutRoot
            // 
            this.layoutRoot.CustomizationFormText = "layoutRoot";
            this.layoutRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRoot.GroupBordersVisible = false;
            this.layoutRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupProfitCentres});
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRoot.Size = new System.Drawing.Size(956, 546);
            this.layoutRoot.Text = "layoutRoot";
            this.layoutRoot.TextVisible = false;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnView,
            this.btnRefresh,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnView),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnAdd
            // 
            this.btnAdd.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnAdd.Caption = "Add";
            this.btnAdd.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnAdd.Id = 0;
            this.btnAdd.LargeImageIndex = 0;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnEdit.Caption = "Edit";
            this.btnEdit.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnEdit.Id = 1;
            this.btnEdit.LargeImageIndex = 1;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_Click);
            // 
            // btnView
            // 
            this.btnView.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnView.Caption = "View";
            this.btnView.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnView.Id = 2;
            this.btnView.LargeImageIndex = 4;
            this.btnView.Name = "btnView";
            this.btnView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnView_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnRefresh.Caption = "Refresh";
            this.btnRefresh.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnRefresh.Id = 3;
            this.btnRefresh.LargeImageIndex = 2;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 4;
            this.btnClose.LargeImageIndex = 3;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 546);
            this.barDockControlBottom.Size = new System.Drawing.Size(956, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 546);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(956, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 546);
            // 
            // ProfitCentresManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 578);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ProfitCentresManagementForm";
            this.Activated += new System.EventHandler(this.ProfitCentresManagementForm_Activated);
            this.Deactivate += new System.EventHandler(this.ProfitCentresManagementForm_Deactivate);
            this.Load += new System.EventHandler(this.ProfitCentresManagementFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProfitCentres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProfitCentresMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupProfitCentres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProfitCentres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraGrid.GridControl gridProfitCentres;
        private DevExpress.XtraGrid.Views.Grid.GridView gridProfitCentresMainView;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupProfitCentres;
        private DevExpress.XtraLayout.LayoutControlItem layoutProfitCentres;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRoot;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnAdd;
        private DevExpress.XtraBars.BarLargeButtonItem btnEdit;
        private DevExpress.XtraBars.BarLargeButtonItem btnView;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
    }
}