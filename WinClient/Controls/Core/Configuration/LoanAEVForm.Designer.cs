﻿namespace Exis.WinClient.Controls
{
    partial class LoanAEVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoanAEVForm));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.cmbBorrower = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.panelAssets = new DevExpress.XtraEditors.PanelControl();
            this.lcAssets = new DevExpress.XtraLayout.LayoutControl();
            this.btnRemoveAsset = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnAddAsset = new DevExpress.XtraEditors.SimpleButton();
            this.txtAssetAmount_1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtAsset_1 = new DevExpress.XtraEditors.TextEdit();
            this.lookupVessel_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbAssetType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lcgRootAssets = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgAssets = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgAsset_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutAssetType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVessel_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAsset_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAssetAmount_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAddAsset = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemRemoveAsset = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelBanks = new DevExpress.XtraEditors.PanelControl();
            this.lcBanks = new DevExpress.XtraLayout.LayoutControl();
            this.txtBankPercentage_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lookupBank_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.chkIsArranger_1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnAddBank = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveBank = new DevExpress.XtraEditors.SimpleButton();
            this.lcgRootBanks = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgBanks = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgBank_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBank_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutArranger_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankPercentage_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAddBank = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemRemoveBank = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelTranches = new DevExpress.XtraEditors.PanelControl();
            this.lcTranches = new DevExpress.XtraLayout.LayoutControl();
            this.dtpFirstInstallmentDate_1 = new DevExpress.XtraEditors.DateEdit();
            this.cmbPeriodType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtCommitmentFee_1 = new DevExpress.XtraEditors.SpinEdit();
            this.btnRemoveDrawdown_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddDrawdown_1 = new DevExpress.XtraEditors.SimpleButton();
            this.txtDrawdownAmount1_1 = new DevExpress.XtraEditors.SpinEdit();
            this.dtpDrawdownDate1_1 = new DevExpress.XtraEditors.DateEdit();
            this.btnRemoveTranch = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddTranch = new DevExpress.XtraEditors.SimpleButton();
            this.txtBalloonAmount_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbPrincipalFrequency_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtRepayPeriod_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbInterestFrequency_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTranchAmount_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbInterestRateType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtInterestRate_1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtInterestCorFactor_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lcgRootTranches = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranches = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranchInfo_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgDrawdowns_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgButtons_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutAddDrawdown_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRemoveDrawdown_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgDrawDownsInfo_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgDrawdown_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutDrawdownAmount1_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDrawdownDate1_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgTranchMainInfo_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTranchMainInfoPart1_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutTranchAmount_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBalloonAmount_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPrincipalFrequency_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterestFrequency_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRepayPeriod_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgTranchMainInfoPart2_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutInterestRateType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterestRate_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterestCorFactor_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCommitmentFee_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgTranchMainInfoPart3_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutFirstInstallmentDate_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAddTranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemRemoveTranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkRetentionAccount = new DevExpress.XtraEditors.CheckEdit();
            this.txtPrepayFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtArrangeFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtAdminFee = new DevExpress.XtraEditors.SpinEdit();
            this.txtVMC = new DevExpress.XtraEditors.SpinEdit();
            this.txtAmount = new DevExpress.XtraEditors.SpinEdit();
            this.lookupCurrency = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpSignDate = new DevExpress.XtraEditors.DateEdit();
            this.lookupGuarantor = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupObligor = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpUpdateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.dtpCreationDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCreationUser = new DevExpress.XtraEditors.TextEdit();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupLoan = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemGuarantor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemObligor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemSignDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemVMC = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAdminFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemArrangeFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPrepayFee = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemRetentionAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCurrency = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBorrower = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutCreationUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUpdateDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPanelTranches = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPanelBanks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPanelAssets = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBorrower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelAssets)).BeginInit();
            this.panelAssets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcAssets)).BeginInit();
            this.lcAssets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssetAmount_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsset_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupVessel_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAssetType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootAssets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAssets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAsset_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAssetType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVessel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAsset_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAssetAmount_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddAsset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveAsset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBanks)).BeginInit();
            this.panelBanks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcBanks)).BeginInit();
            this.lcBanks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankPercentage_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupBank_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsArranger_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBank_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBank_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutArranger_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankPercentage_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTranches)).BeginInit();
            this.panelTranches.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcTranches)).BeginInit();
            this.lcTranches.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFirstInstallmentDate_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFirstInstallmentDate_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommitmentFee_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrawdownAmount1_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDrawdownDate1_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDrawdownDate1_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalloonAmount_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPrincipalFrequency_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepayPeriod_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestFrequency_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTranchAmount_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestRateType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestRate_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestCorFactor_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootTranches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchInfo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDrawdowns_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgButtons_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddDrawdown_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRemoveDrawdown_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDrawDownsInfo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDrawdown_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDrawdownAmount1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDrawdownDate1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTranchAmount_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBalloonAmount_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrincipalFrequency_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestFrequency_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRepayPeriod_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRateType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestCorFactor_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCommitmentFee_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFirstInstallmentDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddTranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveTranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetentionAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepayFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArrangeFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVMC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupGuarantor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupObligor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupLoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGuarantor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemObligor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSignDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemVMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAdminFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemArrangeFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPrepayFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRetentionAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBorrower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelTranches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelAssets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.cmbBorrower);
            this.layoutRootControl.Controls.Add(this.panelAssets);
            this.layoutRootControl.Controls.Add(this.panelBanks);
            this.layoutRootControl.Controls.Add(this.panelTranches);
            this.layoutRootControl.Controls.Add(this.chkRetentionAccount);
            this.layoutRootControl.Controls.Add(this.txtPrepayFee);
            this.layoutRootControl.Controls.Add(this.txtArrangeFee);
            this.layoutRootControl.Controls.Add(this.txtAdminFee);
            this.layoutRootControl.Controls.Add(this.txtVMC);
            this.layoutRootControl.Controls.Add(this.txtAmount);
            this.layoutRootControl.Controls.Add(this.lookupCurrency);
            this.layoutRootControl.Controls.Add(this.dtpSignDate);
            this.layoutRootControl.Controls.Add(this.lookupGuarantor);
            this.layoutRootControl.Controls.Add(this.lookupObligor);
            this.layoutRootControl.Controls.Add(this.dtpUpdateDate);
            this.layoutRootControl.Controls.Add(this.txtUpdateUser);
            this.layoutRootControl.Controls.Add(this.dtpCreationDate);
            this.layoutRootControl.Controls.Add(this.txtCreationUser);
            this.layoutRootControl.Controls.Add(this.lblId);
            this.layoutRootControl.Controls.Add(this.cmbStatus);
            this.layoutRootControl.Controls.Add(this.txtCode);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1511, 348, 521, 452);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutRootGroup;
            this.layoutRootControl.Size = new System.Drawing.Size(1112, 686);
            this.layoutRootControl.TabIndex = 2;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // cmbBorrower
            // 
            this.cmbBorrower.Location = new System.Drawing.Point(660, 57);
            this.cmbBorrower.Name = "cmbBorrower";
            this.cmbBorrower.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBorrower.Size = new System.Drawing.Size(229, 20);
            this.cmbBorrower.StyleController = this.layoutRootControl;
            this.cmbBorrower.TabIndex = 49;
            // 
            // panelAssets
            // 
            this.panelAssets.Controls.Add(this.lcAssets);
            this.panelAssets.Location = new System.Drawing.Point(465, 133);
            this.panelAssets.Name = "panelAssets";
            this.panelAssets.Size = new System.Drawing.Size(645, 67);
            this.panelAssets.TabIndex = 48;
            // 
            // lcAssets
            // 
            this.lcAssets.Controls.Add(this.btnRemoveAsset);
            this.lcAssets.Controls.Add(this.btnAddAsset);
            this.lcAssets.Controls.Add(this.txtAssetAmount_1);
            this.lcAssets.Controls.Add(this.txtAsset_1);
            this.lcAssets.Controls.Add(this.lookupVessel_1);
            this.lcAssets.Controls.Add(this.cmbAssetType_1);
            this.lcAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcAssets.Location = new System.Drawing.Point(2, 2);
            this.lcAssets.Name = "lcAssets";
            this.lcAssets.Root = this.lcgRootAssets;
            this.lcAssets.Size = new System.Drawing.Size(641, 63);
            this.lcAssets.TabIndex = 0;
            this.lcAssets.Text = "layoutControl1";
            // 
            // btnRemoveAsset
            // 
            this.btnRemoveAsset.ImageIndex = 1;
            this.btnRemoveAsset.ImageList = this.imageList1;
            this.btnRemoveAsset.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveAsset.Location = new System.Drawing.Point(592, 36);
            this.btnRemoveAsset.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveAsset.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveAsset.Name = "btnRemoveAsset";
            this.btnRemoveAsset.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveAsset.StyleController = this.lcAssets;
            this.btnRemoveAsset.TabIndex = 13;
            this.btnRemoveAsset.Click += new System.EventHandler(this.BtnRemoveAssetClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList1.Images.SetKeyName(1, "clear16x16.ico");
            // 
            // btnAddAsset
            // 
            this.btnAddAsset.ImageIndex = 0;
            this.btnAddAsset.ImageList = this.imageList1;
            this.btnAddAsset.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddAsset.Location = new System.Drawing.Point(592, 2);
            this.btnAddAsset.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnAddAsset.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnAddAsset.Name = "btnAddAsset";
            this.btnAddAsset.Size = new System.Drawing.Size(30, 30);
            this.btnAddAsset.StyleController = this.lcAssets;
            this.btnAddAsset.TabIndex = 12;
            this.btnAddAsset.Click += new System.EventHandler(this.BtnAddAssetClick);
            // 
            // txtAssetAmount_1
            // 
            this.txtAssetAmount_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAssetAmount_1.Location = new System.Drawing.Point(524, 2);
            this.txtAssetAmount_1.Name = "txtAssetAmount_1";
            this.txtAssetAmount_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAssetAmount_1.Properties.Mask.EditMask = "D";
            this.txtAssetAmount_1.Properties.MaxLength = 9;
            this.txtAssetAmount_1.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtAssetAmount_1.Size = new System.Drawing.Size(64, 20);
            this.txtAssetAmount_1.StyleController = this.lcAssets;
            this.txtAssetAmount_1.TabIndex = 7;
            // 
            // txtAsset_1
            // 
            this.txtAsset_1.Location = new System.Drawing.Point(321, 2);
            this.txtAsset_1.Name = "txtAsset_1";
            this.txtAsset_1.Size = new System.Drawing.Size(153, 20);
            this.txtAsset_1.StyleController = this.lcAssets;
            this.txtAsset_1.TabIndex = 6;
            // 
            // lookupVessel_1
            // 
            this.lookupVessel_1.Location = new System.Drawing.Point(150, 2);
            this.lookupVessel_1.Name = "lookupVessel_1";
            this.lookupVessel_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupVessel_1.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupVessel_1.Size = new System.Drawing.Size(131, 20);
            this.lookupVessel_1.StyleController = this.lcAssets;
            this.lookupVessel_1.TabIndex = 5;
            // 
            // cmbAssetType_1
            // 
            this.cmbAssetType_1.Location = new System.Drawing.Point(35, 2);
            this.cmbAssetType_1.MaximumSize = new System.Drawing.Size(72, 20);
            this.cmbAssetType_1.MinimumSize = new System.Drawing.Size(72, 20);
            this.cmbAssetType_1.Name = "cmbAssetType_1";
            this.cmbAssetType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbAssetType_1.Size = new System.Drawing.Size(72, 20);
            this.cmbAssetType_1.StyleController = this.lcAssets;
            this.cmbAssetType_1.TabIndex = 4;
            this.cmbAssetType_1.SelectedIndexChanged += new System.EventHandler(this.CmbAssetTypeSelectedIndexChanged);
            // 
            // lcgRootAssets
            // 
            this.lcgRootAssets.CustomizationFormText = "lcgAssets";
            this.lcgRootAssets.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRootAssets.GroupBordersVisible = false;
            this.lcgRootAssets.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgAssets,
            this.layoutItemAddAsset,
            this.layoutItemRemoveAsset});
            this.lcgRootAssets.Location = new System.Drawing.Point(0, 0);
            this.lcgRootAssets.Name = "lcgRootAssets";
            this.lcgRootAssets.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRootAssets.Size = new System.Drawing.Size(624, 68);
            this.lcgRootAssets.Text = "lcgRootAssets";
            this.lcgRootAssets.TextVisible = false;
            // 
            // lcgAssets
            // 
            this.lcgAssets.CustomizationFormText = "lcgAsset_1";
            this.lcgAssets.GroupBordersVisible = false;
            this.lcgAssets.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgAsset_1});
            this.lcgAssets.Location = new System.Drawing.Point(0, 0);
            this.lcgAssets.Name = "lcgAssets";
            this.lcgAssets.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgAssets.Size = new System.Drawing.Size(590, 68);
            this.lcgAssets.Tag = global::Exis.WinClient.Strings.Eve;
            this.lcgAssets.Text = "lcgAssets";
            // 
            // lcgAsset_1
            // 
            this.lcgAsset_1.CustomizationFormText = "lcgAssets";
            this.lcgAsset_1.GroupBordersVisible = false;
            this.lcgAsset_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutAssetType_1,
            this.layoutVessel_1,
            this.layoutAsset_1,
            this.layoutAssetAmount_1});
            this.lcgAsset_1.Location = new System.Drawing.Point(0, 0);
            this.lcgAsset_1.Name = "lcgAsset_1";
            this.lcgAsset_1.Size = new System.Drawing.Size(590, 68);
            this.lcgAsset_1.Tag = "1";
            this.lcgAsset_1.Text = "lcgAsset_1";
            this.lcgAsset_1.TextVisible = false;
            // 
            // layoutAssetType_1
            // 
            this.layoutAssetType_1.Control = this.cmbAssetType_1;
            this.layoutAssetType_1.CustomizationFormText = "Type:";
            this.layoutAssetType_1.Location = new System.Drawing.Point(0, 0);
            this.layoutAssetType_1.MaxSize = new System.Drawing.Size(109, 24);
            this.layoutAssetType_1.MinSize = new System.Drawing.Size(109, 24);
            this.layoutAssetType_1.Name = "layoutAssetType_1";
            this.layoutAssetType_1.Size = new System.Drawing.Size(109, 68);
            this.layoutAssetType_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAssetType_1.Text = "Type:";
            this.layoutAssetType_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutAssetType_1.TextSize = new System.Drawing.Size(28, 13);
            this.layoutAssetType_1.TextToControlDistance = 5;
            // 
            // layoutVessel_1
            // 
            this.layoutVessel_1.Control = this.lookupVessel_1;
            this.layoutVessel_1.CustomizationFormText = "Vessel:";
            this.layoutVessel_1.Location = new System.Drawing.Point(109, 0);
            this.layoutVessel_1.Name = "layoutVessel_1";
            this.layoutVessel_1.Size = new System.Drawing.Size(174, 68);
            this.layoutVessel_1.Text = "Vessel:";
            this.layoutVessel_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutVessel_1.TextSize = new System.Drawing.Size(34, 13);
            this.layoutVessel_1.TextToControlDistance = 5;
            // 
            // layoutAsset_1
            // 
            this.layoutAsset_1.Control = this.txtAsset_1;
            this.layoutAsset_1.CustomizationFormText = "Asset:";
            this.layoutAsset_1.Location = new System.Drawing.Point(283, 0);
            this.layoutAsset_1.Name = "layoutAsset_1";
            this.layoutAsset_1.Size = new System.Drawing.Size(193, 68);
            this.layoutAsset_1.Text = "Asset:";
            this.layoutAsset_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutAsset_1.TextSize = new System.Drawing.Size(31, 13);
            this.layoutAsset_1.TextToControlDistance = 5;
            // 
            // layoutAssetAmount_1
            // 
            this.layoutAssetAmount_1.Control = this.txtAssetAmount_1;
            this.layoutAssetAmount_1.CustomizationFormText = "layoutControlItem1";
            this.layoutAssetAmount_1.Location = new System.Drawing.Point(476, 0);
            this.layoutAssetAmount_1.Name = "layoutAssetAmount_1";
            this.layoutAssetAmount_1.Size = new System.Drawing.Size(114, 68);
            this.layoutAssetAmount_1.Text = "Amount:";
            this.layoutAssetAmount_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutAssetAmount_1.TextSize = new System.Drawing.Size(41, 13);
            this.layoutAssetAmount_1.TextToControlDistance = 5;
            // 
            // layoutItemAddAsset
            // 
            this.layoutItemAddAsset.Control = this.btnAddAsset;
            this.layoutItemAddAsset.CustomizationFormText = "layoutItemAddAsset";
            this.layoutItemAddAsset.Location = new System.Drawing.Point(590, 0);
            this.layoutItemAddAsset.Name = "layoutItemAddAsset";
            this.layoutItemAddAsset.Size = new System.Drawing.Size(34, 34);
            this.layoutItemAddAsset.Text = "layoutItemAddAsset";
            this.layoutItemAddAsset.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemAddAsset.TextToControlDistance = 0;
            this.layoutItemAddAsset.TextVisible = false;
            // 
            // layoutItemRemoveAsset
            // 
            this.layoutItemRemoveAsset.Control = this.btnRemoveAsset;
            this.layoutItemRemoveAsset.CustomizationFormText = "layoutItemRemoveAsset";
            this.layoutItemRemoveAsset.Location = new System.Drawing.Point(590, 34);
            this.layoutItemRemoveAsset.Name = "layoutItemRemoveAsset";
            this.layoutItemRemoveAsset.Size = new System.Drawing.Size(34, 34);
            this.layoutItemRemoveAsset.Text = "layoutItemRemoveAsset";
            this.layoutItemRemoveAsset.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemRemoveAsset.TextToControlDistance = 0;
            this.layoutItemRemoveAsset.TextVisible = false;
            // 
            // panelBanks
            // 
            this.panelBanks.Controls.Add(this.lcBanks);
            this.panelBanks.Location = new System.Drawing.Point(2, 133);
            this.panelBanks.Name = "panelBanks";
            this.panelBanks.Size = new System.Drawing.Size(459, 67);
            this.panelBanks.TabIndex = 47;
            // 
            // lcBanks
            // 
            this.lcBanks.Controls.Add(this.txtBankPercentage_1);
            this.lcBanks.Controls.Add(this.lookupBank_1);
            this.lcBanks.Controls.Add(this.chkIsArranger_1);
            this.lcBanks.Controls.Add(this.btnAddBank);
            this.lcBanks.Controls.Add(this.btnRemoveBank);
            this.lcBanks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcBanks.Location = new System.Drawing.Point(2, 2);
            this.lcBanks.Name = "lcBanks";
            this.lcBanks.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1742, 48, 250, 350);
            this.lcBanks.Root = this.lcgRootBanks;
            this.lcBanks.Size = new System.Drawing.Size(455, 63);
            this.lcBanks.TabIndex = 0;
            this.lcBanks.Text = "layoutControl1";
            // 
            // txtBankPercentage_1
            // 
            this.txtBankPercentage_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBankPercentage_1.Location = new System.Drawing.Point(325, 2);
            this.txtBankPercentage_1.Name = "txtBankPercentage_1";
            this.txtBankPercentage_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBankPercentage_1.Properties.Mask.EditMask = "P0";
            this.txtBankPercentage_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtBankPercentage_1.Properties.MaxLength = 3;
            this.txtBankPercentage_1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtBankPercentage_1.Size = new System.Drawing.Size(77, 20);
            this.txtBankPercentage_1.StyleController = this.lcBanks;
            this.txtBankPercentage_1.TabIndex = 8;
            // 
            // lookupBank_1
            // 
            this.lookupBank_1.Location = new System.Drawing.Point(34, 2);
            this.lookupBank_1.Name = "lookupBank_1";
            this.lookupBank_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupBank_1.Properties.NullText = "Select a Bank...";
            this.lookupBank_1.Size = new System.Drawing.Size(156, 20);
            this.lookupBank_1.StyleController = this.lcBanks;
            this.lookupBank_1.TabIndex = 45;
            // 
            // chkIsArranger_1
            // 
            this.chkIsArranger_1.Location = new System.Drawing.Point(246, 2);
            this.chkIsArranger_1.Name = "chkIsArranger_1";
            this.chkIsArranger_1.Properties.Caption = global::Exis.WinClient.Strings.Eve;
            this.chkIsArranger_1.Size = new System.Drawing.Size(43, 19);
            this.chkIsArranger_1.StyleController = this.lcBanks;
            this.chkIsArranger_1.TabIndex = 46;
            // 
            // btnAddBank
            // 
            this.btnAddBank.ImageIndex = 0;
            this.btnAddBank.ImageList = this.imageList1;
            this.btnAddBank.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddBank.Location = new System.Drawing.Point(406, 2);
            this.btnAddBank.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnAddBank.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnAddBank.Name = "btnAddBank";
            this.btnAddBank.Size = new System.Drawing.Size(30, 30);
            this.btnAddBank.StyleController = this.lcBanks;
            this.btnAddBank.TabIndex = 11;
            this.btnAddBank.Click += new System.EventHandler(this.BtnAddBankClick);
            // 
            // btnRemoveBank
            // 
            this.btnRemoveBank.ImageIndex = 1;
            this.btnRemoveBank.ImageList = this.imageList1;
            this.btnRemoveBank.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveBank.Location = new System.Drawing.Point(406, 36);
            this.btnRemoveBank.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveBank.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveBank.Name = "btnRemoveBank";
            this.btnRemoveBank.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveBank.StyleController = this.lcBanks;
            this.btnRemoveBank.TabIndex = 12;
            this.btnRemoveBank.Click += new System.EventHandler(this.BtnRemoveBankClick);
            // 
            // lcgRootBanks
            // 
            this.lcgRootBanks.CustomizationFormText = "lcgBanks";
            this.lcgRootBanks.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRootBanks.GroupBordersVisible = false;
            this.lcgRootBanks.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgBanks,
            this.layoutItemAddBank,
            this.layoutItemRemoveBank});
            this.lcgRootBanks.Location = new System.Drawing.Point(0, 0);
            this.lcgRootBanks.Name = "lcgRootBanks";
            this.lcgRootBanks.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRootBanks.Size = new System.Drawing.Size(438, 68);
            this.lcgRootBanks.Text = "lcgRootBanks";
            this.lcgRootBanks.TextVisible = false;
            // 
            // lcgBanks
            // 
            this.lcgBanks.CustomizationFormText = "lcgBank_1";
            this.lcgBanks.GroupBordersVisible = false;
            this.lcgBanks.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgBank_1});
            this.lcgBanks.Location = new System.Drawing.Point(0, 0);
            this.lcgBanks.Name = "lcgBanks";
            this.lcgBanks.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgBanks.Size = new System.Drawing.Size(404, 68);
            this.lcgBanks.Tag = global::Exis.WinClient.Strings.Eve;
            this.lcgBanks.Text = "lcgBanks";
            this.lcgBanks.TextVisible = false;
            // 
            // lcgBank_1
            // 
            this.lcgBank_1.CustomizationFormText = "layoutControlGroup3";
            this.lcgBank_1.GroupBordersVisible = false;
            this.lcgBank_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBank_1,
            this.layoutArranger_1,
            this.layoutBankPercentage_1});
            this.lcgBank_1.Location = new System.Drawing.Point(0, 0);
            this.lcgBank_1.Name = "lcgBank_1";
            this.lcgBank_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgBank_1.Size = new System.Drawing.Size(404, 68);
            this.lcgBank_1.Tag = "1";
            this.lcgBank_1.Text = "lcgBank_1";
            this.lcgBank_1.TextVisible = false;
            // 
            // layoutBank_1
            // 
            this.layoutBank_1.Control = this.lookupBank_1;
            this.layoutBank_1.CustomizationFormText = "layoutItemBank_1";
            this.layoutBank_1.Location = new System.Drawing.Point(0, 0);
            this.layoutBank_1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutBank_1.MinSize = new System.Drawing.Size(86, 24);
            this.layoutBank_1.Name = "layoutBank_1";
            this.layoutBank_1.Size = new System.Drawing.Size(192, 68);
            this.layoutBank_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutBank_1.Text = "Bank:";
            this.layoutBank_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutBank_1.TextSize = new System.Drawing.Size(27, 13);
            this.layoutBank_1.TextToControlDistance = 5;
            // 
            // layoutArranger_1
            // 
            this.layoutArranger_1.Control = this.chkIsArranger_1;
            this.layoutArranger_1.CustomizationFormText = "Arranger:";
            this.layoutArranger_1.Location = new System.Drawing.Point(192, 0);
            this.layoutArranger_1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutArranger_1.MinSize = new System.Drawing.Size(79, 24);
            this.layoutArranger_1.Name = "layoutArranger_1";
            this.layoutArranger_1.Size = new System.Drawing.Size(99, 68);
            this.layoutArranger_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutArranger_1.Text = "Arranger:";
            this.layoutArranger_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutArranger_1.TextSize = new System.Drawing.Size(47, 13);
            this.layoutArranger_1.TextToControlDistance = 5;
            // 
            // layoutBankPercentage_1
            // 
            this.layoutBankPercentage_1.Control = this.txtBankPercentage_1;
            this.layoutBankPercentage_1.CustomizationFormText = "Percentage:";
            this.layoutBankPercentage_1.Location = new System.Drawing.Point(291, 0);
            this.layoutBankPercentage_1.Name = "layoutBankPercentage_1";
            this.layoutBankPercentage_1.Size = new System.Drawing.Size(113, 68);
            this.layoutBankPercentage_1.Text = "Perc.:";
            this.layoutBankPercentage_1.TextSize = new System.Drawing.Size(29, 13);
            // 
            // layoutItemAddBank
            // 
            this.layoutItemAddBank.Control = this.btnAddBank;
            this.layoutItemAddBank.CustomizationFormText = "layoutItemAddBank";
            this.layoutItemAddBank.Location = new System.Drawing.Point(404, 0);
            this.layoutItemAddBank.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutItemAddBank.MinSize = new System.Drawing.Size(34, 34);
            this.layoutItemAddBank.Name = "layoutItemAddBank";
            this.layoutItemAddBank.Size = new System.Drawing.Size(34, 34);
            this.layoutItemAddBank.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemAddBank.Text = "layoutItemAddBank";
            this.layoutItemAddBank.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutItemAddBank.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemAddBank.TextToControlDistance = 0;
            this.layoutItemAddBank.TextVisible = false;
            // 
            // layoutItemRemoveBank
            // 
            this.layoutItemRemoveBank.Control = this.btnRemoveBank;
            this.layoutItemRemoveBank.CustomizationFormText = "layoutItemRemoveBank";
            this.layoutItemRemoveBank.Location = new System.Drawing.Point(404, 34);
            this.layoutItemRemoveBank.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutItemRemoveBank.MinSize = new System.Drawing.Size(34, 34);
            this.layoutItemRemoveBank.Name = "layoutItemRemoveBank";
            this.layoutItemRemoveBank.Size = new System.Drawing.Size(34, 34);
            this.layoutItemRemoveBank.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemRemoveBank.Text = "layoutItemRemoveBank";
            this.layoutItemRemoveBank.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutItemRemoveBank.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemRemoveBank.TextToControlDistance = 0;
            this.layoutItemRemoveBank.TextVisible = false;
            // 
            // panelTranches
            // 
            this.panelTranches.Controls.Add(this.lcTranches);
            this.panelTranches.Location = new System.Drawing.Point(2, 220);
            this.panelTranches.Name = "panelTranches";
            this.panelTranches.Size = new System.Drawing.Size(1108, 397);
            this.panelTranches.TabIndex = 44;
            // 
            // lcTranches
            // 
            this.lcTranches.Controls.Add(this.dtpFirstInstallmentDate_1);
            this.lcTranches.Controls.Add(this.cmbPeriodType_1);
            this.lcTranches.Controls.Add(this.txtCommitmentFee_1);
            this.lcTranches.Controls.Add(this.btnRemoveDrawdown_1);
            this.lcTranches.Controls.Add(this.btnAddDrawdown_1);
            this.lcTranches.Controls.Add(this.txtDrawdownAmount1_1);
            this.lcTranches.Controls.Add(this.dtpDrawdownDate1_1);
            this.lcTranches.Controls.Add(this.btnRemoveTranch);
            this.lcTranches.Controls.Add(this.btnAddTranch);
            this.lcTranches.Controls.Add(this.txtBalloonAmount_1);
            this.lcTranches.Controls.Add(this.cmbPrincipalFrequency_1);
            this.lcTranches.Controls.Add(this.txtRepayPeriod_1);
            this.lcTranches.Controls.Add(this.cmbInterestFrequency_1);
            this.lcTranches.Controls.Add(this.txtTranchAmount_1);
            this.lcTranches.Controls.Add(this.cmbInterestRateType_1);
            this.lcTranches.Controls.Add(this.txtInterestRate_1);
            this.lcTranches.Controls.Add(this.txtInterestCorFactor_1);
            this.lcTranches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcTranches.Location = new System.Drawing.Point(2, 2);
            this.lcTranches.Name = "lcTranches";
            this.lcTranches.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(252, 286, 609, 350);
            this.lcTranches.Root = this.lcgRootTranches;
            this.lcTranches.Size = new System.Drawing.Size(1104, 393);
            this.lcTranches.TabIndex = 0;
            this.lcTranches.Text = "layoutControl1";
            // 
            // dtpFirstInstallmentDate_1
            // 
            this.dtpFirstInstallmentDate_1.EditValue = null;
            this.dtpFirstInstallmentDate_1.Location = new System.Drawing.Point(114, 75);
            this.dtpFirstInstallmentDate_1.Name = "dtpFirstInstallmentDate_1";
            this.dtpFirstInstallmentDate_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFirstInstallmentDate_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFirstInstallmentDate_1.Size = new System.Drawing.Size(171, 20);
            this.dtpFirstInstallmentDate_1.StyleController = this.lcTranches;
            this.dtpFirstInstallmentDate_1.TabIndex = 47;
            // 
            // cmbPeriodType_1
            // 
            this.cmbPeriodType_1.Location = new System.Drawing.Point(535, 51);
            this.cmbPeriodType_1.Name = "cmbPeriodType_1";
            this.cmbPeriodType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodType_1.Size = new System.Drawing.Size(170, 20);
            this.cmbPeriodType_1.StyleController = this.lcTranches;
            this.cmbPeriodType_1.TabIndex = 46;
            // 
            // txtCommitmentFee_1
            // 
            this.txtCommitmentFee_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCommitmentFee_1.Location = new System.Drawing.Point(998, 51);
            this.txtCommitmentFee_1.Name = "txtCommitmentFee_1";
            this.txtCommitmentFee_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtCommitmentFee_1.Properties.Mask.EditMask = "f4";
            this.txtCommitmentFee_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCommitmentFee_1.Properties.MaxLength = 8;
            this.txtCommitmentFee_1.Size = new System.Drawing.Size(64, 20);
            this.txtCommitmentFee_1.StyleController = this.lcTranches;
            this.txtCommitmentFee_1.TabIndex = 16;
            // 
            // btnRemoveDrawdown_1
            // 
            this.btnRemoveDrawdown_1.ImageIndex = 1;
            this.btnRemoveDrawdown_1.ImageList = this.imageList1;
            this.btnRemoveDrawdown_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveDrawdown_1.Location = new System.Drawing.Point(556, 124);
            this.btnRemoveDrawdown_1.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveDrawdown_1.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveDrawdown_1.Name = "btnRemoveDrawdown_1";
            this.btnRemoveDrawdown_1.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveDrawdown_1.StyleController = this.lcTranches;
            this.btnRemoveDrawdown_1.TabIndex = 15;
            this.btnRemoveDrawdown_1.Click += new System.EventHandler(this.BtnRemoveDrawdownClick);
            // 
            // btnAddDrawdown_1
            // 
            this.btnAddDrawdown_1.ImageIndex = 0;
            this.btnAddDrawdown_1.ImageList = this.imageList1;
            this.btnAddDrawdown_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddDrawdown_1.Location = new System.Drawing.Point(522, 124);
            this.btnAddDrawdown_1.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnAddDrawdown_1.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnAddDrawdown_1.Name = "btnAddDrawdown_1";
            this.btnAddDrawdown_1.Size = new System.Drawing.Size(30, 30);
            this.btnAddDrawdown_1.StyleController = this.lcTranches;
            this.btnAddDrawdown_1.TabIndex = 13;
            this.btnAddDrawdown_1.Click += new System.EventHandler(this.BtnAddDrawdownClick);
            // 
            // txtDrawdownAmount1_1
            // 
            this.txtDrawdownAmount1_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDrawdownAmount1_1.Location = new System.Drawing.Point(295, 124);
            this.txtDrawdownAmount1_1.Name = "txtDrawdownAmount1_1";
            this.txtDrawdownAmount1_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDrawdownAmount1_1.Properties.Mask.EditMask = "D";
            this.txtDrawdownAmount1_1.Properties.MaxLength = 12;
            this.txtDrawdownAmount1_1.Properties.MaxValue = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.txtDrawdownAmount1_1.Size = new System.Drawing.Size(223, 20);
            this.txtDrawdownAmount1_1.StyleController = this.lcTranches;
            this.txtDrawdownAmount1_1.TabIndex = 45;
            // 
            // dtpDrawdownDate1_1
            // 
            this.dtpDrawdownDate1_1.EditValue = null;
            this.dtpDrawdownDate1_1.Location = new System.Drawing.Point(97, 124);
            this.dtpDrawdownDate1_1.Name = "dtpDrawdownDate1_1";
            this.dtpDrawdownDate1_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDrawdownDate1_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDrawdownDate1_1.Size = new System.Drawing.Size(90, 20);
            this.dtpDrawdownDate1_1.StyleController = this.lcTranches;
            this.dtpDrawdownDate1_1.TabIndex = 44;
            // 
            // btnRemoveTranch
            // 
            this.btnRemoveTranch.ImageIndex = 1;
            this.btnRemoveTranch.ImageList = this.imageList1;
            this.btnRemoveTranch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveTranch.Location = new System.Drawing.Point(1072, 36);
            this.btnRemoveTranch.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveTranch.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnRemoveTranch.Name = "btnRemoveTranch";
            this.btnRemoveTranch.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveTranch.StyleController = this.lcTranches;
            this.btnRemoveTranch.TabIndex = 14;
            this.btnRemoveTranch.Click += new System.EventHandler(this.BtnRemoveTranchClick);
            // 
            // btnAddTranch
            // 
            this.btnAddTranch.ImageIndex = 0;
            this.btnAddTranch.ImageList = this.imageList1;
            this.btnAddTranch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddTranch.Location = new System.Drawing.Point(1072, 2);
            this.btnAddTranch.MaximumSize = new System.Drawing.Size(30, 30);
            this.btnAddTranch.MinimumSize = new System.Drawing.Size(30, 30);
            this.btnAddTranch.Name = "btnAddTranch";
            this.btnAddTranch.Size = new System.Drawing.Size(30, 30);
            this.btnAddTranch.StyleController = this.lcTranches;
            this.btnAddTranch.TabIndex = 12;
            this.btnAddTranch.Click += new System.EventHandler(this.BtnAddTranchClick);
            // 
            // txtBalloonAmount_1
            // 
            this.txtBalloonAmount_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBalloonAmount_1.Location = new System.Drawing.Point(313, 27);
            this.txtBalloonAmount_1.Name = "txtBalloonAmount_1";
            this.txtBalloonAmount_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBalloonAmount_1.Properties.Mask.EditMask = "D";
            this.txtBalloonAmount_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtBalloonAmount_1.Properties.MaxLength = 9;
            this.txtBalloonAmount_1.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtBalloonAmount_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtBalloonAmount_1.Size = new System.Drawing.Size(147, 20);
            this.txtBalloonAmount_1.StyleController = this.lcTranches;
            this.txtBalloonAmount_1.TabIndex = 36;
            // 
            // cmbPrincipalFrequency_1
            // 
            this.cmbPrincipalFrequency_1.Location = new System.Drawing.Point(566, 27);
            this.cmbPrincipalFrequency_1.Name = "cmbPrincipalFrequency_1";
            this.cmbPrincipalFrequency_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPrincipalFrequency_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPrincipalFrequency_1.Size = new System.Drawing.Size(136, 20);
            this.cmbPrincipalFrequency_1.StyleController = this.lcTranches;
            this.cmbPrincipalFrequency_1.TabIndex = 40;
            // 
            // txtRepayPeriod_1
            // 
            this.txtRepayPeriod_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRepayPeriod_1.Location = new System.Drawing.Point(1002, 27);
            this.txtRepayPeriod_1.Name = "txtRepayPeriod_1";
            this.txtRepayPeriod_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRepayPeriod_1.Properties.Mask.EditMask = "D";
            this.txtRepayPeriod_1.Properties.MaxLength = 2;
            this.txtRepayPeriod_1.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtRepayPeriod_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtRepayPeriod_1.Size = new System.Drawing.Size(60, 20);
            this.txtRepayPeriod_1.StyleController = this.lcTranches;
            this.txtRepayPeriod_1.TabIndex = 36;
            // 
            // cmbInterestFrequency_1
            // 
            this.cmbInterestFrequency_1.Location = new System.Drawing.Point(808, 27);
            this.cmbInterestFrequency_1.Name = "cmbInterestFrequency_1";
            this.cmbInterestFrequency_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbInterestFrequency_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbInterestFrequency_1.Size = new System.Drawing.Size(117, 20);
            this.cmbInterestFrequency_1.StyleController = this.lcTranches;
            this.cmbInterestFrequency_1.TabIndex = 41;
            // 
            // txtTranchAmount_1
            // 
            this.txtTranchAmount_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTranchAmount_1.Location = new System.Drawing.Point(96, 27);
            this.txtTranchAmount_1.Name = "txtTranchAmount_1";
            this.txtTranchAmount_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtTranchAmount_1.Properties.Mask.EditMask = "D";
            this.txtTranchAmount_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTranchAmount_1.Properties.MaxLength = 12;
            this.txtTranchAmount_1.Properties.MaxValue = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.txtTranchAmount_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtTranchAmount_1.Size = new System.Drawing.Size(130, 20);
            this.txtTranchAmount_1.StyleController = this.lcTranches;
            this.txtTranchAmount_1.TabIndex = 35;
            // 
            // cmbInterestRateType_1
            // 
            this.cmbInterestRateType_1.Location = new System.Drawing.Point(109, 51);
            this.cmbInterestRateType_1.Name = "cmbInterestRateType_1";
            this.cmbInterestRateType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbInterestRateType_1.Size = new System.Drawing.Size(174, 20);
            this.cmbInterestRateType_1.StyleController = this.lcTranches;
            this.cmbInterestRateType_1.TabIndex = 42;
            this.cmbInterestRateType_1.SelectedIndexChanged += new System.EventHandler(this.CmbInterestRateTypeSelectedIndexChanged);
            // 
            // txtInterestRate_1
            // 
            this.txtInterestRate_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtInterestRate_1.Location = new System.Drawing.Point(361, 51);
            this.txtInterestRate_1.Name = "txtInterestRate_1";
            this.txtInterestRate_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtInterestRate_1.Properties.Mask.EditMask = "f6";
            this.txtInterestRate_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInterestRate_1.Properties.MaxLength = 13;
            this.txtInterestRate_1.Size = new System.Drawing.Size(104, 20);
            this.txtInterestRate_1.StyleController = this.lcTranches;
            this.txtInterestRate_1.TabIndex = 15;
            // 
            // txtInterestCorFactor_1
            // 
            this.txtInterestCorFactor_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtInterestCorFactor_1.Location = new System.Drawing.Point(802, 51);
            this.txtInterestCorFactor_1.Name = "txtInterestCorFactor_1";
            this.txtInterestCorFactor_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtInterestCorFactor_1.Properties.Mask.EditMask = "f4";
            this.txtInterestCorFactor_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInterestCorFactor_1.Properties.MaxLength = 8;
            this.txtInterestCorFactor_1.Size = new System.Drawing.Size(86, 20);
            this.txtInterestCorFactor_1.StyleController = this.lcTranches;
            this.txtInterestCorFactor_1.TabIndex = 16;
            // 
            // lcgRootTranches
            // 
            this.lcgRootTranches.CustomizationFormText = "layoutControlGroup1";
            this.lcgRootTranches.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRootTranches.GroupBordersVisible = false;
            this.lcgRootTranches.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranches,
            this.layoutItemAddTranch,
            this.layoutItemRemoveTranch});
            this.lcgRootTranches.Location = new System.Drawing.Point(0, 0);
            this.lcgRootTranches.Name = "lcgRootTranches";
            this.lcgRootTranches.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRootTranches.Size = new System.Drawing.Size(1104, 393);
            this.lcgRootTranches.Text = "lcgRootTranches";
            this.lcgRootTranches.TextVisible = false;
            // 
            // lcgTranches
            // 
            this.lcgTranches.CustomizationFormText = "lcgTranches";
            this.lcgTranches.GroupBordersVisible = false;
            this.lcgTranches.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranchInfo_1});
            this.lcgTranches.Location = new System.Drawing.Point(0, 0);
            this.lcgTranches.Name = "lcgTranches";
            this.lcgTranches.Size = new System.Drawing.Size(1070, 393);
            this.lcgTranches.Text = "lcgTranches";
            this.lcgTranches.TextVisible = false;
            // 
            // lcgTranchInfo_1
            // 
            this.lcgTranchInfo_1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lcgTranchInfo_1.AppearanceGroup.Options.UseFont = true;
            this.lcgTranchInfo_1.CustomizationFormText = "layoutControlGroup1";
            this.lcgTranchInfo_1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgTranchInfo_1.ExpandButtonVisible = true;
            this.lcgTranchInfo_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgDrawdowns_1,
            this.lcgTranchMainInfo_1});
            this.lcgTranchInfo_1.Location = new System.Drawing.Point(0, 0);
            this.lcgTranchInfo_1.Name = "lcgTranchInfo_1";
            this.lcgTranchInfo_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchInfo_1.Size = new System.Drawing.Size(1070, 393);
            this.lcgTranchInfo_1.Tag = "1";
            this.lcgTranchInfo_1.Text = "Tranche 1";
            // 
            // lcgDrawdowns_1
            // 
            this.lcgDrawdowns_1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lcgDrawdowns_1.AppearanceGroup.Options.UseFont = true;
            this.lcgDrawdowns_1.CustomizationFormText = "Draw-downs";
            this.lcgDrawdowns_1.ExpandButtonVisible = true;
            this.lcgDrawdowns_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.lcgButtons_1,
            this.lcgDrawDownsInfo_1});
            this.lcgDrawdowns_1.Location = new System.Drawing.Point(0, 78);
            this.lcgDrawdowns_1.Name = "lcgDrawdowns_1";
            this.lcgDrawdowns_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgDrawdowns_1.Size = new System.Drawing.Size(1064, 290);
            this.lcgDrawdowns_1.Text = "Draw-downs";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(582, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(476, 265);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgButtons_1
            // 
            this.lcgButtons_1.CustomizationFormText = "lcgButtons_1";
            this.lcgButtons_1.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.lcgButtons_1.GroupBordersVisible = false;
            this.lcgButtons_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutAddDrawdown_1,
            this.layoutRemoveDrawdown_1});
            this.lcgButtons_1.Location = new System.Drawing.Point(514, 0);
            this.lcgButtons_1.Name = "lcgButtons_1";
            this.lcgButtons_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgButtons_1.Size = new System.Drawing.Size(68, 265);
            this.lcgButtons_1.Text = "lcgButtons_1";
            // 
            // layoutAddDrawdown_1
            // 
            this.layoutAddDrawdown_1.Control = this.btnAddDrawdown_1;
            this.layoutAddDrawdown_1.CustomizationFormText = "layoutControlItem1";
            this.layoutAddDrawdown_1.Location = new System.Drawing.Point(0, 0);
            this.layoutAddDrawdown_1.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutAddDrawdown_1.MinSize = new System.Drawing.Size(34, 34);
            this.layoutAddDrawdown_1.Name = "layoutAddDrawdown_1";
            this.layoutAddDrawdown_1.Size = new System.Drawing.Size(34, 265);
            this.layoutAddDrawdown_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAddDrawdown_1.Text = "layoutAddDrawdown_1";
            this.layoutAddDrawdown_1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutAddDrawdown_1.TextToControlDistance = 0;
            this.layoutAddDrawdown_1.TextVisible = false;
            // 
            // layoutRemoveDrawdown_1
            // 
            this.layoutRemoveDrawdown_1.Control = this.btnRemoveDrawdown_1;
            this.layoutRemoveDrawdown_1.CustomizationFormText = "layoutControlItem2";
            this.layoutRemoveDrawdown_1.Location = new System.Drawing.Point(34, 0);
            this.layoutRemoveDrawdown_1.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutRemoveDrawdown_1.MinSize = new System.Drawing.Size(34, 34);
            this.layoutRemoveDrawdown_1.Name = "layoutRemoveDrawdown_1";
            this.layoutRemoveDrawdown_1.Size = new System.Drawing.Size(34, 265);
            this.layoutRemoveDrawdown_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRemoveDrawdown_1.Text = "layoutRemoveDrawdown_1";
            this.layoutRemoveDrawdown_1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutRemoveDrawdown_1.TextToControlDistance = 0;
            this.layoutRemoveDrawdown_1.TextVisible = false;
            // 
            // lcgDrawDownsInfo_1
            // 
            this.lcgDrawDownsInfo_1.CustomizationFormText = "lcgDrawDownsInfo_1";
            this.lcgDrawDownsInfo_1.GroupBordersVisible = false;
            this.lcgDrawDownsInfo_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgDrawdown_1});
            this.lcgDrawDownsInfo_1.Location = new System.Drawing.Point(0, 0);
            this.lcgDrawDownsInfo_1.Name = "lcgDrawDownsInfo_1";
            this.lcgDrawDownsInfo_1.Size = new System.Drawing.Size(514, 265);
            this.lcgDrawDownsInfo_1.Text = "lcgDrawDownsInfo_1";
            this.lcgDrawDownsInfo_1.TextVisible = false;
            // 
            // lcgDrawdown_1
            // 
            this.lcgDrawdown_1.CustomizationFormText = "lcgDrawdown_1";
            this.lcgDrawdown_1.GroupBordersVisible = false;
            this.lcgDrawdown_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutDrawdownAmount1_1,
            this.layoutDrawdownDate1_1});
            this.lcgDrawdown_1.Location = new System.Drawing.Point(0, 0);
            this.lcgDrawdown_1.Name = "lcgDrawdown_1";
            this.lcgDrawdown_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgDrawdown_1.Size = new System.Drawing.Size(514, 265);
            this.lcgDrawdown_1.Tag = "1";
            this.lcgDrawdown_1.Text = "lcgDrawdown_1";
            this.lcgDrawdown_1.TextVisible = false;
            // 
            // layoutDrawdownAmount1_1
            // 
            this.layoutDrawdownAmount1_1.Control = this.txtDrawdownAmount1_1;
            this.layoutDrawdownAmount1_1.CustomizationFormText = "Draw-down Amount:";
            this.layoutDrawdownAmount1_1.Location = new System.Drawing.Point(183, 0);
            this.layoutDrawdownAmount1_1.Name = "layoutDrawdownAmount1_1";
            this.layoutDrawdownAmount1_1.Size = new System.Drawing.Size(331, 265);
            this.layoutDrawdownAmount1_1.Text = "Draw-down Amount:";
            this.layoutDrawdownAmount1_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutDrawdownAmount1_1.TextSize = new System.Drawing.Size(99, 13);
            this.layoutDrawdownAmount1_1.TextToControlDistance = 5;
            // 
            // layoutDrawdownDate1_1
            // 
            this.layoutDrawdownDate1_1.Control = this.dtpDrawdownDate1_1;
            this.layoutDrawdownDate1_1.CustomizationFormText = "Draw-down Date:";
            this.layoutDrawdownDate1_1.Location = new System.Drawing.Point(0, 0);
            this.layoutDrawdownDate1_1.MaxSize = new System.Drawing.Size(183, 24);
            this.layoutDrawdownDate1_1.MinSize = new System.Drawing.Size(183, 24);
            this.layoutDrawdownDate1_1.Name = "layoutDrawdownDate1_1";
            this.layoutDrawdownDate1_1.Size = new System.Drawing.Size(183, 265);
            this.layoutDrawdownDate1_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutDrawdownDate1_1.Text = "Draw-down date:";
            this.layoutDrawdownDate1_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutDrawdownDate1_1.TextSize = new System.Drawing.Size(84, 13);
            this.layoutDrawdownDate1_1.TextToControlDistance = 5;
            // 
            // lcgTranchMainInfo_1
            // 
            this.lcgTranchMainInfo_1.CustomizationFormText = "lcgTranchMainInfo_1";
            this.lcgTranchMainInfo_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTranchMainInfoPart1_1,
            this.lcgTranchMainInfoPart2_1,
            this.lcgTranchMainInfoPart3_1});
            this.lcgTranchMainInfo_1.Location = new System.Drawing.Point(0, 0);
            this.lcgTranchMainInfo_1.Name = "lcgTranchMainInfo_1";
            this.lcgTranchMainInfo_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchMainInfo_1.Size = new System.Drawing.Size(1064, 78);
            this.lcgTranchMainInfo_1.Text = "lcgTranchMainInfo_1";
            this.lcgTranchMainInfo_1.TextVisible = false;
            // 
            // lcgTranchMainInfoPart1_1
            // 
            this.lcgTranchMainInfoPart1_1.CustomizationFormText = "lcgTranchMainInfoPart1_1";
            this.lcgTranchMainInfoPart1_1.GroupBordersVisible = false;
            this.lcgTranchMainInfoPart1_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutTranchAmount_1,
            this.layoutBalloonAmount_1,
            this.layoutPrincipalFrequency_1,
            this.layoutInterestFrequency_1,
            this.layoutRepayPeriod_1});
            this.lcgTranchMainInfoPart1_1.Location = new System.Drawing.Point(0, 0);
            this.lcgTranchMainInfoPart1_1.Name = "lcgTranchMainInfoPart1_1";
            this.lcgTranchMainInfoPart1_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchMainInfoPart1_1.Size = new System.Drawing.Size(1058, 24);
            this.lcgTranchMainInfoPart1_1.Text = "lcgTranchMainInfoPart1_1";
            this.lcgTranchMainInfoPart1_1.TextVisible = false;
            // 
            // layoutTranchAmount_1
            // 
            this.layoutTranchAmount_1.Control = this.txtTranchAmount_1;
            this.layoutTranchAmount_1.CustomizationFormText = "Tranche Amount:";
            this.layoutTranchAmount_1.Location = new System.Drawing.Point(0, 0);
            this.layoutTranchAmount_1.Name = "layoutTranchAmount_1";
            this.layoutTranchAmount_1.Size = new System.Drawing.Size(222, 24);
            this.layoutTranchAmount_1.Text = "Tranche Amount:";
            this.layoutTranchAmount_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutTranchAmount_1.TextSize = new System.Drawing.Size(83, 13);
            this.layoutTranchAmount_1.TextToControlDistance = 5;
            // 
            // layoutBalloonAmount_1
            // 
            this.layoutBalloonAmount_1.Control = this.txtBalloonAmount_1;
            this.layoutBalloonAmount_1.CustomizationFormText = "Balloon Amount:";
            this.layoutBalloonAmount_1.Location = new System.Drawing.Point(222, 0);
            this.layoutBalloonAmount_1.Name = "layoutBalloonAmount_1";
            this.layoutBalloonAmount_1.Size = new System.Drawing.Size(234, 24);
            this.layoutBalloonAmount_1.Text = "Balloon Amount:";
            this.layoutBalloonAmount_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutBalloonAmount_1.TextSize = new System.Drawing.Size(78, 13);
            this.layoutBalloonAmount_1.TextToControlDistance = 5;
            // 
            // layoutPrincipalFrequency_1
            // 
            this.layoutPrincipalFrequency_1.Control = this.cmbPrincipalFrequency_1;
            this.layoutPrincipalFrequency_1.CustomizationFormText = "Principal Frequency:";
            this.layoutPrincipalFrequency_1.Location = new System.Drawing.Point(456, 0);
            this.layoutPrincipalFrequency_1.Name = "layoutPrincipalFrequency_1";
            this.layoutPrincipalFrequency_1.Size = new System.Drawing.Size(242, 24);
            this.layoutPrincipalFrequency_1.Text = "Principal Frequency:";
            this.layoutPrincipalFrequency_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutPrincipalFrequency_1.TextSize = new System.Drawing.Size(97, 13);
            this.layoutPrincipalFrequency_1.TextToControlDistance = 5;
            // 
            // layoutInterestFrequency_1
            // 
            this.layoutInterestFrequency_1.Control = this.cmbInterestFrequency_1;
            this.layoutInterestFrequency_1.CustomizationFormText = "Interest Frequency:";
            this.layoutInterestFrequency_1.Location = new System.Drawing.Point(698, 0);
            this.layoutInterestFrequency_1.Name = "layoutInterestFrequency_1";
            this.layoutInterestFrequency_1.Size = new System.Drawing.Size(223, 24);
            this.layoutInterestFrequency_1.Text = "Interest Frequency:";
            this.layoutInterestFrequency_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestFrequency_1.TextSize = new System.Drawing.Size(97, 13);
            this.layoutInterestFrequency_1.TextToControlDistance = 5;
            // 
            // layoutRepayPeriod_1
            // 
            this.layoutRepayPeriod_1.Control = this.txtRepayPeriod_1;
            this.layoutRepayPeriod_1.CustomizationFormText = "Repay Period:";
            this.layoutRepayPeriod_1.Location = new System.Drawing.Point(921, 0);
            this.layoutRepayPeriod_1.Name = "layoutRepayPeriod_1";
            this.layoutRepayPeriod_1.Size = new System.Drawing.Size(137, 24);
            this.layoutRepayPeriod_1.Text = "Repay Period:";
            this.layoutRepayPeriod_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutRepayPeriod_1.TextSize = new System.Drawing.Size(68, 13);
            this.layoutRepayPeriod_1.TextToControlDistance = 5;
            // 
            // lcgTranchMainInfoPart2_1
            // 
            this.lcgTranchMainInfoPart2_1.CustomizationFormText = "lcgTranchMainInfoPart2_1";
            this.lcgTranchMainInfoPart2_1.GroupBordersVisible = false;
            this.lcgTranchMainInfoPart2_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutInterestRateType_1,
            this.layoutInterestRate_1,
            this.layoutInterestCorFactor_1,
            this.layoutCommitmentFee_1,
            this.layoutPeriodType_1});
            this.lcgTranchMainInfoPart2_1.Location = new System.Drawing.Point(0, 24);
            this.lcgTranchMainInfoPart2_1.Name = "lcgTranchMainInfoPart2_1";
            this.lcgTranchMainInfoPart2_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgTranchMainInfoPart2_1.Size = new System.Drawing.Size(1058, 24);
            this.lcgTranchMainInfoPart2_1.Text = "lcgTranchMainInfoPart2_1";
            this.lcgTranchMainInfoPart2_1.TextVisible = false;
            // 
            // layoutInterestRateType_1
            // 
            this.layoutInterestRateType_1.Control = this.cmbInterestRateType_1;
            this.layoutInterestRateType_1.CustomizationFormText = "Interest Rate Type:";
            this.layoutInterestRateType_1.Location = new System.Drawing.Point(0, 0);
            this.layoutInterestRateType_1.Name = "layoutInterestRateType_1";
            this.layoutInterestRateType_1.Size = new System.Drawing.Size(279, 24);
            this.layoutInterestRateType_1.Text = "Interest Rate Type:";
            this.layoutInterestRateType_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestRateType_1.TextSize = new System.Drawing.Size(96, 13);
            this.layoutInterestRateType_1.TextToControlDistance = 5;
            // 
            // layoutInterestRate_1
            // 
            this.layoutInterestRate_1.Control = this.txtInterestRate_1;
            this.layoutInterestRate_1.CustomizationFormText = "Interest Rate:";
            this.layoutInterestRate_1.Location = new System.Drawing.Point(279, 0);
            this.layoutInterestRate_1.Name = "layoutInterestRate_1";
            this.layoutInterestRate_1.Size = new System.Drawing.Size(182, 24);
            this.layoutInterestRate_1.Text = "Interest Rate:";
            this.layoutInterestRate_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestRate_1.TextSize = new System.Drawing.Size(69, 13);
            this.layoutInterestRate_1.TextToControlDistance = 5;
            // 
            // layoutInterestCorFactor_1
            // 
            this.layoutInterestCorFactor_1.Control = this.txtInterestCorFactor_1;
            this.layoutInterestCorFactor_1.CustomizationFormText = "Correction Factor:";
            this.layoutInterestCorFactor_1.Location = new System.Drawing.Point(701, 0);
            this.layoutInterestCorFactor_1.Name = "layoutInterestCorFactor_1";
            this.layoutInterestCorFactor_1.Size = new System.Drawing.Size(183, 24);
            this.layoutInterestCorFactor_1.Text = "Correction Factor:";
            this.layoutInterestCorFactor_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutInterestCorFactor_1.TextSize = new System.Drawing.Size(88, 13);
            this.layoutInterestCorFactor_1.TextToControlDistance = 5;
            // 
            // layoutCommitmentFee_1
            // 
            this.layoutCommitmentFee_1.Control = this.txtCommitmentFee_1;
            this.layoutCommitmentFee_1.CustomizationFormText = "Commitment Fee:";
            this.layoutCommitmentFee_1.Location = new System.Drawing.Point(884, 0);
            this.layoutCommitmentFee_1.Name = "layoutCommitmentFee_1";
            this.layoutCommitmentFee_1.Size = new System.Drawing.Size(174, 24);
            this.layoutCommitmentFee_1.Text = "Commitment Fee:";
            this.layoutCommitmentFee_1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutPeriodType_1
            // 
            this.layoutPeriodType_1.Control = this.cmbPeriodType_1;
            this.layoutPeriodType_1.CustomizationFormText = "Period Type:";
            this.layoutPeriodType_1.Location = new System.Drawing.Point(461, 0);
            this.layoutPeriodType_1.Name = "layoutPeriodType_1";
            this.layoutPeriodType_1.Size = new System.Drawing.Size(240, 24);
            this.layoutPeriodType_1.Text = "Period Type:";
            this.layoutPeriodType_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutPeriodType_1.TextSize = new System.Drawing.Size(61, 13);
            this.layoutPeriodType_1.TextToControlDistance = 5;
            // 
            // lcgTranchMainInfoPart3_1
            // 
            this.lcgTranchMainInfoPart3_1.CustomizationFormText = "lcgTranchMainInfoPart3_1";
            this.lcgTranchMainInfoPart3_1.GroupBordersVisible = false;
            this.lcgTranchMainInfoPart3_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.layoutFirstInstallmentDate_1});
            this.lcgTranchMainInfoPart3_1.Location = new System.Drawing.Point(0, 48);
            this.lcgTranchMainInfoPart3_1.Name = "lcgTranchMainInfoPart3_1";
            this.lcgTranchMainInfoPart3_1.Size = new System.Drawing.Size(1058, 24);
            this.lcgTranchMainInfoPart3_1.Text = "lcgTranchMainInfoPart3_1";
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(281, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(777, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutFirstInstallmentDate_1
            // 
            this.layoutFirstInstallmentDate_1.Control = this.dtpFirstInstallmentDate_1;
            this.layoutFirstInstallmentDate_1.CustomizationFormText = "First Installment Date";
            this.layoutFirstInstallmentDate_1.Location = new System.Drawing.Point(0, 0);
            this.layoutFirstInstallmentDate_1.Name = "layoutFirstInstallmentDate_1";
            this.layoutFirstInstallmentDate_1.Size = new System.Drawing.Size(281, 24);
            this.layoutFirstInstallmentDate_1.Text = "First Installment Date";
            this.layoutFirstInstallmentDate_1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutItemAddTranch
            // 
            this.layoutItemAddTranch.Control = this.btnAddTranch;
            this.layoutItemAddTranch.CustomizationFormText = "layoutItemAddTranch";
            this.layoutItemAddTranch.Location = new System.Drawing.Point(1070, 0);
            this.layoutItemAddTranch.Name = "layoutItemAddTranch";
            this.layoutItemAddTranch.Size = new System.Drawing.Size(34, 34);
            this.layoutItemAddTranch.Text = "layoutItemAddTranch";
            this.layoutItemAddTranch.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemAddTranch.TextToControlDistance = 0;
            this.layoutItemAddTranch.TextVisible = false;
            // 
            // layoutItemRemoveTranch
            // 
            this.layoutItemRemoveTranch.Control = this.btnRemoveTranch;
            this.layoutItemRemoveTranch.CustomizationFormText = "layoutItemRemoveTranch";
            this.layoutItemRemoveTranch.Location = new System.Drawing.Point(1070, 34);
            this.layoutItemRemoveTranch.Name = "layoutItemRemoveTranch";
            this.layoutItemRemoveTranch.Size = new System.Drawing.Size(34, 359);
            this.layoutItemRemoveTranch.Text = "layoutItemRemoveTranch";
            this.layoutItemRemoveTranch.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemRemoveTranch.TextToControlDistance = 0;
            this.layoutItemRemoveTranch.TextVisible = false;
            // 
            // chkRetentionAccount
            // 
            this.chkRetentionAccount.Location = new System.Drawing.Point(926, 81);
            this.chkRetentionAccount.Name = "chkRetentionAccount";
            this.chkRetentionAccount.Properties.Caption = global::Exis.WinClient.Strings.Eve;
            this.chkRetentionAccount.Size = new System.Drawing.Size(172, 19);
            this.chkRetentionAccount.StyleController = this.layoutRootControl;
            this.chkRetentionAccount.TabIndex = 37;
            // 
            // txtPrepayFee
            // 
            this.txtPrepayFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPrepayFee.Location = new System.Drawing.Point(729, 81);
            this.txtPrepayFee.Name = "txtPrepayFee";
            this.txtPrepayFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPrepayFee.Properties.Mask.EditMask = "P0";
            this.txtPrepayFee.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPrepayFee.Properties.MaxLength = 3;
            this.txtPrepayFee.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtPrepayFee.Size = new System.Drawing.Size(97, 20);
            this.txtPrepayFee.StyleController = this.layoutRootControl;
            this.txtPrepayFee.TabIndex = 14;
            // 
            // txtArrangeFee
            // 
            this.txtArrangeFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtArrangeFee.Location = new System.Drawing.Point(542, 81);
            this.txtArrangeFee.Name = "txtArrangeFee";
            this.txtArrangeFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtArrangeFee.Properties.Mask.EditMask = "D";
            this.txtArrangeFee.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtArrangeFee.Properties.MaxLength = 9;
            this.txtArrangeFee.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtArrangeFee.Size = new System.Drawing.Size(97, 20);
            this.txtArrangeFee.StyleController = this.layoutRootControl;
            this.txtArrangeFee.TabIndex = 36;
            // 
            // txtAdminFee
            // 
            this.txtAdminFee.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAdminFee.Location = new System.Drawing.Point(349, 81);
            this.txtAdminFee.Name = "txtAdminFee";
            this.txtAdminFee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAdminFee.Properties.Mask.EditMask = "D";
            this.txtAdminFee.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAdminFee.Properties.MaxLength = 9;
            this.txtAdminFee.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.txtAdminFee.Size = new System.Drawing.Size(98, 20);
            this.txtAdminFee.StyleController = this.layoutRootControl;
            this.txtAdminFee.TabIndex = 35;
            // 
            // txtVMC
            // 
            this.txtVMC.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtVMC.Location = new System.Drawing.Point(187, 81);
            this.txtVMC.Name = "txtVMC";
            this.txtVMC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVMC.Properties.Mask.EditMask = "P0";
            this.txtVMC.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtVMC.Properties.MaxLength = 3;
            this.txtVMC.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtVMC.Size = new System.Drawing.Size(62, 20);
            this.txtVMC.StyleController = this.layoutRootControl;
            this.txtVMC.TabIndex = 13;
            // 
            // txtAmount
            // 
            this.txtAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAmount.Location = new System.Drawing.Point(58, 81);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAmount.Properties.Mask.EditMask = "D";
            this.txtAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAmount.Properties.MaxLength = 12;
            this.txtAmount.Properties.MaxValue = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.txtAmount.Size = new System.Drawing.Size(97, 20);
            this.txtAmount.StyleController = this.layoutRootControl;
            this.txtAmount.TabIndex = 34;
            this.txtAmount.EditValueChanged += new System.EventHandler(this.TxtAmountEditValueChanged);
            // 
            // lookupCurrency
            // 
            this.lookupCurrency.Location = new System.Drawing.Point(944, 57);
            this.lookupCurrency.Name = "lookupCurrency";
            this.lookupCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupCurrency.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupCurrency.Size = new System.Drawing.Size(154, 20);
            this.lookupCurrency.StyleController = this.layoutRootControl;
            this.lookupCurrency.TabIndex = 23;
            // 
            // dtpSignDate
            // 
            this.dtpSignDate.EditValue = null;
            this.dtpSignDate.Location = new System.Drawing.Point(723, 33);
            this.dtpSignDate.Name = "dtpSignDate";
            this.dtpSignDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpSignDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpSignDate.Size = new System.Drawing.Size(375, 20);
            this.dtpSignDate.StyleController = this.layoutRootControl;
            this.dtpSignDate.TabIndex = 33;
            // 
            // lookupGuarantor
            // 
            this.lookupGuarantor.Location = new System.Drawing.Point(366, 57);
            this.lookupGuarantor.Name = "lookupGuarantor";
            this.lookupGuarantor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupGuarantor.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupGuarantor.Size = new System.Drawing.Size(239, 20);
            this.lookupGuarantor.StyleController = this.layoutRootControl;
            this.lookupGuarantor.TabIndex = 32;
            // 
            // lookupObligor
            // 
            this.lookupObligor.Location = new System.Drawing.Point(55, 57);
            this.lookupObligor.Name = "lookupObligor";
            this.lookupObligor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupObligor.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.lookupObligor.Size = new System.Drawing.Size(251, 20);
            this.lookupObligor.StyleController = this.layoutRootControl;
            this.lookupObligor.TabIndex = 31;
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.EditValue = null;
            this.dtpUpdateDate.Location = new System.Drawing.Point(997, 652);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpUpdateDate.Properties.ReadOnly = true;
            this.dtpUpdateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpUpdateDate.Size = new System.Drawing.Size(101, 20);
            this.dtpUpdateDate.StyleController = this.layoutRootControl;
            this.dtpUpdateDate.TabIndex = 8;
            // 
            // txtUpdateUser
            // 
            this.txtUpdateUser.Location = new System.Drawing.Point(620, 652);
            this.txtUpdateUser.Name = "txtUpdateUser";
            this.txtUpdateUser.Properties.MaxLength = 1000;
            this.txtUpdateUser.Properties.ReadOnly = true;
            this.txtUpdateUser.Size = new System.Drawing.Size(305, 20);
            this.txtUpdateUser.StyleController = this.layoutRootControl;
            this.txtUpdateUser.TabIndex = 11;
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.EditValue = null;
            this.dtpCreationDate.Location = new System.Drawing.Point(448, 652);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpCreationDate.Properties.ReadOnly = true;
            this.dtpCreationDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreationDate.Size = new System.Drawing.Size(101, 20);
            this.dtpCreationDate.StyleController = this.layoutRootControl;
            this.dtpCreationDate.TabIndex = 7;
            // 
            // txtCreationUser
            // 
            this.txtCreationUser.Location = new System.Drawing.Point(87, 652);
            this.txtCreationUser.Name = "txtCreationUser";
            this.txtCreationUser.Properties.MaxLength = 1000;
            this.txtCreationUser.Properties.ReadOnly = true;
            this.txtCreationUser.Size = new System.Drawing.Size(283, 20);
            this.txtCreationUser.StyleController = this.layoutRootControl;
            this.txtCreationUser.TabIndex = 10;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(31, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(81, 20);
            this.lblId.StyleController = this.layoutRootControl;
            this.lblId.TabIndex = 16;
            this.lblId.Text = "Id";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(565, 33);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(101, 20);
            this.cmbStatus.StyleController = this.layoutRootControl;
            this.cmbStatus.TabIndex = 30;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(148, 33);
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.MaxLength = 1000;
            this.txtCode.Size = new System.Drawing.Size(375, 20);
            this.txtCode.StyleController = this.layoutRootControl;
            this.txtCode.TabIndex = 9;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutRootGroup";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupLoan,
            this.layoutGroupUserInfo,
            this.layoutItemPanelTranches,
            this.layoutItemPanelBanks,
            this.layoutItemPanelAssets});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(1112, 686);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutGroupLoan
            // 
            this.layoutGroupLoan.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutGroupLoan.AppearanceGroup.Options.UseFont = true;
            this.layoutGroupLoan.CustomizationFormText = "Loan";
            this.layoutGroupLoan.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemCode,
            this.layoutId,
            this.layoutStatus,
            this.layoutItemGuarantor,
            this.layoutItemObligor,
            this.layoutItemSignDate,
            this.layoutItemAmount,
            this.layoutItemVMC,
            this.layoutItemAdminFee,
            this.layoutItemArrangeFee,
            this.layoutItemPrepayFee,
            this.layoutItemRetentionAccount,
            this.layoutCurrency,
            this.layoutBorrower});
            this.layoutGroupLoan.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupLoan.Name = "layoutGroupLoan";
            this.layoutGroupLoan.Size = new System.Drawing.Size(1112, 115);
            this.layoutGroupLoan.Text = "Loan";
            // 
            // layoutItemCode
            // 
            this.layoutItemCode.Control = this.txtCode;
            this.layoutItemCode.CustomizationFormText = "Code:";
            this.layoutItemCode.Location = new System.Drawing.Point(102, 0);
            this.layoutItemCode.Name = "layoutItemCode";
            this.layoutItemCode.Size = new System.Drawing.Size(411, 24);
            this.layoutItemCode.Text = "Code:";
            this.layoutItemCode.TextSize = new System.Drawing.Size(29, 13);
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.MaxSize = new System.Drawing.Size(102, 24);
            this.layoutId.MinSize = new System.Drawing.Size(102, 24);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(102, 24);
            this.layoutId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(14, 13);
            // 
            // layoutStatus
            // 
            this.layoutStatus.Control = this.cmbStatus;
            this.layoutStatus.CustomizationFormText = "Status:";
            this.layoutStatus.Location = new System.Drawing.Point(513, 0);
            this.layoutStatus.MaxSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.MinSize = new System.Drawing.Size(143, 24);
            this.layoutStatus.Name = "layoutStatus";
            this.layoutStatus.Size = new System.Drawing.Size(143, 24);
            this.layoutStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus.Text = "Status:";
            this.layoutStatus.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutItemGuarantor
            // 
            this.layoutItemGuarantor.Control = this.lookupGuarantor;
            this.layoutItemGuarantor.CustomizationFormText = "Guarantor:";
            this.layoutItemGuarantor.Location = new System.Drawing.Point(296, 24);
            this.layoutItemGuarantor.Name = "layoutItemGuarantor";
            this.layoutItemGuarantor.Size = new System.Drawing.Size(299, 24);
            this.layoutItemGuarantor.Text = "Guarantor:";
            this.layoutItemGuarantor.TextSize = new System.Drawing.Size(53, 13);
            // 
            // layoutItemObligor
            // 
            this.layoutItemObligor.Control = this.lookupObligor;
            this.layoutItemObligor.CustomizationFormText = "Obligor:";
            this.layoutItemObligor.Location = new System.Drawing.Point(0, 24);
            this.layoutItemObligor.Name = "layoutItemObligor";
            this.layoutItemObligor.Size = new System.Drawing.Size(296, 24);
            this.layoutItemObligor.Text = "Obligor:";
            this.layoutItemObligor.TextSize = new System.Drawing.Size(38, 13);
            // 
            // layoutItemSignDate
            // 
            this.layoutItemSignDate.Control = this.dtpSignDate;
            this.layoutItemSignDate.CustomizationFormText = "Sign Date:";
            this.layoutItemSignDate.Location = new System.Drawing.Point(656, 0);
            this.layoutItemSignDate.Name = "layoutItemSignDate";
            this.layoutItemSignDate.Size = new System.Drawing.Size(432, 24);
            this.layoutItemSignDate.Text = "Sign Date:";
            this.layoutItemSignDate.TextSize = new System.Drawing.Size(50, 13);
            // 
            // layoutItemAmount
            // 
            this.layoutItemAmount.Control = this.txtAmount;
            this.layoutItemAmount.CustomizationFormText = "Amount:";
            this.layoutItemAmount.Location = new System.Drawing.Point(0, 48);
            this.layoutItemAmount.Name = "layoutItemAmount";
            this.layoutItemAmount.Size = new System.Drawing.Size(145, 24);
            this.layoutItemAmount.Text = "Amount:";
            this.layoutItemAmount.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutItemVMC
            // 
            this.layoutItemVMC.Control = this.txtVMC;
            this.layoutItemVMC.CustomizationFormText = "VMC:";
            this.layoutItemVMC.Location = new System.Drawing.Point(145, 48);
            this.layoutItemVMC.Name = "layoutItemVMC";
            this.layoutItemVMC.Size = new System.Drawing.Size(94, 24);
            this.layoutItemVMC.Text = "VMC:";
            this.layoutItemVMC.TextSize = new System.Drawing.Size(25, 13);
            // 
            // layoutItemAdminFee
            // 
            this.layoutItemAdminFee.Control = this.txtAdminFee;
            this.layoutItemAdminFee.CustomizationFormText = "Administration Fee:";
            this.layoutItemAdminFee.Location = new System.Drawing.Point(239, 48);
            this.layoutItemAdminFee.Name = "layoutItemAdminFee";
            this.layoutItemAdminFee.Size = new System.Drawing.Size(198, 24);
            this.layoutItemAdminFee.Text = "Administration Fee:";
            this.layoutItemAdminFee.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutItemArrangeFee
            // 
            this.layoutItemArrangeFee.Control = this.txtArrangeFee;
            this.layoutItemArrangeFee.CustomizationFormText = "Arrangement Fee:";
            this.layoutItemArrangeFee.Location = new System.Drawing.Point(437, 48);
            this.layoutItemArrangeFee.Name = "layoutItemArrangeFee";
            this.layoutItemArrangeFee.Size = new System.Drawing.Size(192, 24);
            this.layoutItemArrangeFee.Text = "Arrangement Fee:";
            this.layoutItemArrangeFee.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutItemPrepayFee
            // 
            this.layoutItemPrepayFee.Control = this.txtPrepayFee;
            this.layoutItemPrepayFee.CustomizationFormText = "Prepayment Fee:";
            this.layoutItemPrepayFee.Location = new System.Drawing.Point(629, 48);
            this.layoutItemPrepayFee.Name = "layoutItemPrepayFee";
            this.layoutItemPrepayFee.Size = new System.Drawing.Size(187, 24);
            this.layoutItemPrepayFee.Text = "Prepayment Fee:";
            this.layoutItemPrepayFee.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutItemRetentionAccount
            // 
            this.layoutItemRetentionAccount.Control = this.chkRetentionAccount;
            this.layoutItemRetentionAccount.CustomizationFormText = "Retention Account:";
            this.layoutItemRetentionAccount.Location = new System.Drawing.Point(816, 48);
            this.layoutItemRetentionAccount.Name = "layoutItemRetentionAccount";
            this.layoutItemRetentionAccount.Size = new System.Drawing.Size(272, 24);
            this.layoutItemRetentionAccount.Text = "Retention Account:";
            this.layoutItemRetentionAccount.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutCurrency
            // 
            this.layoutCurrency.Control = this.lookupCurrency;
            this.layoutCurrency.CustomizationFormText = "Currency:";
            this.layoutCurrency.Location = new System.Drawing.Point(879, 24);
            this.layoutCurrency.Name = "layoutCurrency";
            this.layoutCurrency.Size = new System.Drawing.Size(209, 24);
            this.layoutCurrency.Text = "Currency:";
            this.layoutCurrency.TextSize = new System.Drawing.Size(48, 13);
            // 
            // layoutBorrower
            // 
            this.layoutBorrower.Control = this.cmbBorrower;
            this.layoutBorrower.CustomizationFormText = "Borrower:";
            this.layoutBorrower.Location = new System.Drawing.Point(595, 24);
            this.layoutBorrower.Name = "layoutBorrower";
            this.layoutBorrower.Size = new System.Drawing.Size(284, 24);
            this.layoutBorrower.Text = "Borrower:";
            this.layoutBorrower.TextSize = new System.Drawing.Size(48, 13);
            // 
            // layoutGroupUserInfo
            // 
            this.layoutGroupUserInfo.CustomizationFormText = "User Info";
            this.layoutGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutCreationUser,
            this.layoutCreationDate,
            this.layoutUpdateUser,
            this.layoutUpdateDate});
            this.layoutGroupUserInfo.Location = new System.Drawing.Point(0, 619);
            this.layoutGroupUserInfo.Name = "layoutGroupUserInfo";
            this.layoutGroupUserInfo.Size = new System.Drawing.Size(1112, 67);
            this.layoutGroupUserInfo.Text = "User Info";
            // 
            // layoutCreationUser
            // 
            this.layoutCreationUser.Control = this.txtCreationUser;
            this.layoutCreationUser.CustomizationFormText = "Creation User:";
            this.layoutCreationUser.Location = new System.Drawing.Point(0, 0);
            this.layoutCreationUser.Name = "layoutCreationUser";
            this.layoutCreationUser.Size = new System.Drawing.Size(360, 24);
            this.layoutCreationUser.Text = "Creation User:";
            this.layoutCreationUser.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutCreationDate
            // 
            this.layoutCreationDate.Control = this.dtpCreationDate;
            this.layoutCreationDate.CustomizationFormText = "Creation Date:";
            this.layoutCreationDate.Location = new System.Drawing.Point(360, 0);
            this.layoutCreationDate.MaxSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.MinSize = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.Name = "layoutCreationDate";
            this.layoutCreationDate.Size = new System.Drawing.Size(179, 24);
            this.layoutCreationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCreationDate.Text = "Creation Date:";
            this.layoutCreationDate.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutUpdateUser
            // 
            this.layoutUpdateUser.Control = this.txtUpdateUser;
            this.layoutUpdateUser.CustomizationFormText = "Update User:";
            this.layoutUpdateUser.Location = new System.Drawing.Point(539, 0);
            this.layoutUpdateUser.Name = "layoutUpdateUser";
            this.layoutUpdateUser.Size = new System.Drawing.Size(376, 24);
            this.layoutUpdateUser.Text = "Update User:";
            this.layoutUpdateUser.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutUpdateDate
            // 
            this.layoutUpdateDate.Control = this.dtpUpdateDate;
            this.layoutUpdateDate.CustomizationFormText = "Update Date:";
            this.layoutUpdateDate.Location = new System.Drawing.Point(915, 0);
            this.layoutUpdateDate.MaxSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.MinSize = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.Name = "layoutUpdateDate";
            this.layoutUpdateDate.Size = new System.Drawing.Size(173, 24);
            this.layoutUpdateDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUpdateDate.Text = "Update Date:";
            this.layoutUpdateDate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutItemPanelTranches
            // 
            this.layoutItemPanelTranches.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutItemPanelTranches.AppearanceItemCaption.Options.UseFont = true;
            this.layoutItemPanelTranches.Control = this.panelTranches;
            this.layoutItemPanelTranches.CustomizationFormText = "layoutItemPanelTranches";
            this.layoutItemPanelTranches.Location = new System.Drawing.Point(0, 202);
            this.layoutItemPanelTranches.Name = "layoutItemPanelTranches";
            this.layoutItemPanelTranches.Size = new System.Drawing.Size(1112, 417);
            this.layoutItemPanelTranches.Text = "Tranches:";
            this.layoutItemPanelTranches.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutItemPanelTranches.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutItemPanelBanks
            // 
            this.layoutItemPanelBanks.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutItemPanelBanks.AppearanceItemCaption.Options.UseFont = true;
            this.layoutItemPanelBanks.Control = this.panelBanks;
            this.layoutItemPanelBanks.CustomizationFormText = "Banks:";
            this.layoutItemPanelBanks.Location = new System.Drawing.Point(0, 115);
            this.layoutItemPanelBanks.Name = "layoutItemPanelBanks";
            this.layoutItemPanelBanks.Size = new System.Drawing.Size(463, 87);
            this.layoutItemPanelBanks.Text = "Banks:";
            this.layoutItemPanelBanks.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutItemPanelBanks.TextSize = new System.Drawing.Size(37, 13);
            // 
            // layoutItemPanelAssets
            // 
            this.layoutItemPanelAssets.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutItemPanelAssets.AppearanceItemCaption.Options.UseFont = true;
            this.layoutItemPanelAssets.Control = this.panelAssets;
            this.layoutItemPanelAssets.CustomizationFormText = "Assets:";
            this.layoutItemPanelAssets.Location = new System.Drawing.Point(463, 115);
            this.layoutItemPanelAssets.Name = "layoutItemPanelAssets";
            this.layoutItemPanelAssets.Size = new System.Drawing.Size(649, 87);
            this.layoutItemPanelAssets.Text = "Collateral Assets:";
            this.layoutItemPanelAssets.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutItemPanelAssets.TextSize = new System.Drawing.Size(98, 13);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 3;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1112, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 686);
            this.barDockControlBottom.Size = new System.Drawing.Size(1112, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 686);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1112, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 686);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSaveClick);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 2;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCloseClick);
            // 
            // LoanAEVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 718);
            this.Controls.Add(this.layoutRootControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "LoanAEVForm";
            this.Activated += new System.EventHandler(this.LoanAevFormActivated);
            this.Deactivate += new System.EventHandler(this.LoanAevFormDeactivate);
            this.Load += new System.EventHandler(this.LoanAevFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbBorrower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelAssets)).EndInit();
            this.panelAssets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcAssets)).EndInit();
            this.lcAssets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAssetAmount_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsset_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupVessel_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAssetType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootAssets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAssets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAsset_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAssetType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVessel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAsset_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAssetAmount_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddAsset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveAsset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBanks)).EndInit();
            this.panelBanks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcBanks)).EndInit();
            this.lcBanks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankPercentage_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupBank_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsArranger_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBank_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBank_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutArranger_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankPercentage_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTranches)).EndInit();
            this.panelTranches.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcTranches)).EndInit();
            this.lcTranches.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpFirstInstallmentDate_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFirstInstallmentDate_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommitmentFee_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrawdownAmount1_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDrawdownDate1_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDrawdownDate1_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalloonAmount_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPrincipalFrequency_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepayPeriod_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestFrequency_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTranchAmount_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInterestRateType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestRate_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestCorFactor_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRootTranches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchInfo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDrawdowns_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgButtons_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddDrawdown_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRemoveDrawdown_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDrawDownsInfo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDrawdown_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDrawdownAmount1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDrawdownDate1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTranchAmount_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBalloonAmount_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrincipalFrequency_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestFrequency_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRepayPeriod_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRateType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestRate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterestCorFactor_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCommitmentFee_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTranchMainInfoPart3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFirstInstallmentDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddTranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveTranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetentionAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepayFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArrangeFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVMC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupGuarantor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupObligor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpUpdateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreationUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupLoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGuarantor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemObligor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSignDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemVMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAdminFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemArrangeFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPrepayFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRetentionAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBorrower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelTranches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPanelAssets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.DateEdit dtpUpdateDate;
        private DevExpress.XtraEditors.TextEdit txtUpdateUser;
        private DevExpress.XtraEditors.DateEdit dtpCreationDate;
        private DevExpress.XtraEditors.TextEdit txtCreationUser;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupLoan;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreationDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutUpdateDate;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.DateEdit dtpSignDate;
        private DevExpress.XtraEditors.LookUpEdit lookupGuarantor;
        private DevExpress.XtraEditors.LookUpEdit lookupObligor;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemGuarantor;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemObligor;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemSignDate;
        private DevExpress.XtraEditors.LookUpEdit lookupCurrency;
        private DevExpress.XtraLayout.LayoutControlItem layoutCurrency;
        private DevExpress.XtraEditors.SpinEdit txtAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAmount;
        private DevExpress.XtraEditors.SpinEdit txtVMC;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemVMC;
        private DevExpress.XtraEditors.CheckEdit chkRetentionAccount;
        private DevExpress.XtraEditors.SpinEdit txtPrepayFee;
        private DevExpress.XtraEditors.SpinEdit txtArrangeFee;
        private DevExpress.XtraEditors.SpinEdit txtAdminFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAdminFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemArrangeFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPrepayFee;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemRetentionAccount;
        private DevExpress.XtraEditors.SpinEdit txtInterestCorFactor_1;
        private DevExpress.XtraEditors.SpinEdit txtInterestRate_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbInterestRateType_1;
        private DevExpress.XtraEditors.SpinEdit txtRepayPeriod_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbInterestFrequency_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPrincipalFrequency_1;
        private DevExpress.XtraEditors.SpinEdit txtBalloonAmount_1;
        private DevExpress.XtraEditors.SpinEdit txtTranchAmount_1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PanelControl panelTranches;
        private DevExpress.XtraLayout.LayoutControl lcTranches;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRootTranches;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPanelTranches;
        private DevExpress.XtraEditors.LookUpEdit lookupBank_1;
        private DevExpress.XtraEditors.CheckEdit chkIsArranger_1;
        private DevExpress.XtraEditors.SimpleButton btnAddBank;
        private DevExpress.XtraEditors.SimpleButton btnRemoveBank;
        private DevExpress.XtraEditors.PanelControl panelBanks;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPanelBanks;
        private DevExpress.XtraLayout.LayoutControl lcBanks;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAddBank;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemRemoveBank;
        private DevExpress.XtraLayout.LayoutControlGroup lcgBanks;
        private DevExpress.XtraLayout.LayoutControlItem layoutArranger_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBank_1;
        private DevExpress.XtraEditors.PanelControl panelAssets;
        private DevExpress.XtraLayout.LayoutControl lcAssets;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRootAssets;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPanelAssets;
        private DevExpress.XtraEditors.SimpleButton btnRemoveAsset;
        private DevExpress.XtraEditors.SimpleButton btnAddAsset;
        private DevExpress.XtraEditors.SpinEdit txtAssetAmount_1;
        private DevExpress.XtraEditors.TextEdit txtAsset_1;
        private DevExpress.XtraEditors.LookUpEdit lookupVessel_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbAssetType_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgAssets;
        private DevExpress.XtraLayout.LayoutControlItem layoutAssetType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutVessel_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutAssetAmount_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutAsset_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAddAsset;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemRemoveAsset;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchInfo_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutTranchAmount_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBalloonAmount_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPrincipalFrequency_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRepayPeriod_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestFrequency_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestRateType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestRate_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterestCorFactor_1;
        private DevExpress.XtraEditors.SimpleButton btnRemoveTranch;
        private DevExpress.XtraEditors.SimpleButton btnAddTranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAddTranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemRemoveTranch;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRootBanks;
        private DevExpress.XtraLayout.LayoutControlGroup lcgBank_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgAsset_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranches;
        private DevExpress.XtraEditors.SpinEdit txtDrawdownAmount1_1;
        private DevExpress.XtraEditors.DateEdit dtpDrawdownDate1_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgDrawdowns_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgDrawdown_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutDrawdownAmount1_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutDrawdownDate1_1;
        private DevExpress.XtraEditors.SimpleButton btnRemoveDrawdown_1;
        private DevExpress.XtraEditors.SimpleButton btnAddDrawdown_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutAddDrawdown_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRemoveDrawdown_1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfo_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgButtons_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgDrawDownsInfo_1;
        private DevExpress.XtraEditors.SpinEdit txtBankPercentage_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankPercentage_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfoPart1_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfoPart2_1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbBorrower;
        private DevExpress.XtraLayout.LayoutControlItem layoutBorrower;
        private DevExpress.XtraEditors.SpinEdit txtCommitmentFee_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutCommitmentFee_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodType_1;
        private DevExpress.XtraEditors.DateEdit dtpFirstInstallmentDate_1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutFirstInstallmentDate_1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTranchMainInfoPart3_1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}