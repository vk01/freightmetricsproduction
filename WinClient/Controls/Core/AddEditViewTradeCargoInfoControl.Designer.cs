﻿namespace Exis.WinClient.Controls.Core
{
    partial class AddEditViewTradeCargoInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditViewTradeCargoInfoControl));
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.txtDischargingPort = new DevExpress.XtraEditors.TextEdit();
            this.txtLoadingPort = new DevExpress.XtraEditors.TextEdit();
            this.lookupVessel = new DevExpress.XtraEditors.LookUpEdit();
            this.txtAddress = new DevExpress.XtraEditors.SpinEdit();
            this.txtVesselIndex = new DevExpress.XtraEditors.SpinEdit();
            this.panelLegs = new System.Windows.Forms.Panel();
            this.layoutLegsControl = new DevExpress.XtraLayout.LayoutControl();
            this.txtTce = new DevExpress.XtraEditors.SpinEdit();
            this.txtQuantityVariation_1 = new DevExpress.XtraEditors.SpinEdit();
            this.chkIsOptional_1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnRemoveLeg = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnAddLeg = new DevExpress.XtraEditors.SimpleButton();
            this.cmbStatus_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtQuantity_1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtIndexPerc_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lookUpIndex_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.txtRate_1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmbRateType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpPeriodTo_1 = new DevExpress.XtraEditors.DateEdit();
            this.dtpPeriodFrom_1 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutLegsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutLeg_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPeriodFrom_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodTo_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRateType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRate_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIndex_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIndexPerc_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutQuantity_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptional_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutQuantityVariation_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTce_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAddTradeLeg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cmbType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTradeLegs = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVesselIndex = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutLoadingPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDischargingPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDischargingPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoadingPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupVessel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVesselIndex.Properties)).BeginInit();
            this.panelLegs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsControl)).BeginInit();
            this.layoutLegsControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTce.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityVariation_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsOptional_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndexPerc_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpIndex_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbRateType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLeg_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodFrom_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodTo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRateType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndex_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndexPerc_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantity_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptional_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityVariation_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTce_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddTradeLeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeLegs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVesselIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLoadingPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDischargingPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.Controls.Add(this.txtDischargingPort);
            this.layoutRoot.Controls.Add(this.txtLoadingPort);
            this.layoutRoot.Controls.Add(this.lookupVessel);
            this.layoutRoot.Controls.Add(this.txtAddress);
            this.layoutRoot.Controls.Add(this.txtVesselIndex);
            this.layoutRoot.Controls.Add(this.panelLegs);
            this.layoutRoot.Controls.Add(this.cmbType);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.Root = this.layoutControlGroup1;
            this.layoutRoot.Size = new System.Drawing.Size(1113, 208);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // txtDischargingPort
            // 
            this.txtDischargingPort.Location = new System.Drawing.Point(567, 26);
            this.txtDischargingPort.Name = "txtDischargingPort";
            this.txtDischargingPort.Properties.MaxLength = 1000;
            this.txtDischargingPort.Size = new System.Drawing.Size(365, 20);
            this.txtDischargingPort.StyleController = this.layoutRoot;
            this.txtDischargingPort.TabIndex = 9;
            // 
            // txtLoadingPort
            // 
            this.txtLoadingPort.Location = new System.Drawing.Point(69, 26);
            this.txtLoadingPort.Name = "txtLoadingPort";
            this.txtLoadingPort.Properties.MaxLength = 1000;
            this.txtLoadingPort.Size = new System.Drawing.Size(409, 20);
            this.txtLoadingPort.StyleController = this.layoutRoot;
            this.txtLoadingPort.TabIndex = 9;
            // 
            // lookupVessel
            // 
            this.lookupVessel.Location = new System.Drawing.Point(195, 2);
            this.lookupVessel.Name = "lookupVessel";
            this.lookupVessel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupVessel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupVessel.Properties.NullText = global::Exis.WinClient.Strings.String2;
            this.lookupVessel.Size = new System.Drawing.Size(783, 20);
            this.lookupVessel.StyleController = this.layoutRoot;
            this.lookupVessel.TabIndex = 14;
            this.lookupVessel.EditValueChanged += new System.EventHandler(this.LookupVesselEditValueChanged);
            // 
            // txtAddress
            // 
            this.txtAddress.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAddress.Location = new System.Drawing.Point(982, 26);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAddress.Properties.Mask.EditMask = "f4";
            this.txtAddress.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAddress.Properties.MaxLength = 8;
            this.txtAddress.Size = new System.Drawing.Size(129, 20);
            this.txtAddress.StyleController = this.layoutRoot;
            this.txtAddress.TabIndex = 12;
            // 
            // txtVesselIndex
            // 
            this.txtVesselIndex.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtVesselIndex.Location = new System.Drawing.Point(1050, 2);
            this.txtVesselIndex.Name = "txtVesselIndex";
            this.txtVesselIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVesselIndex.Size = new System.Drawing.Size(61, 20);
            this.txtVesselIndex.StyleController = this.layoutRoot;
            this.txtVesselIndex.TabIndex = 10;
            // 
            // panelLegs
            // 
            this.panelLegs.Controls.Add(this.layoutLegsControl);
            this.panelLegs.Location = new System.Drawing.Point(31, 50);
            this.panelLegs.MinimumSize = new System.Drawing.Size(0, 100);
            this.panelLegs.Name = "panelLegs";
            this.panelLegs.Size = new System.Drawing.Size(1080, 156);
            this.panelLegs.TabIndex = 9;
            // 
            // layoutLegsControl
            // 
            this.layoutLegsControl.AllowCustomizationMenu = false;
            this.layoutLegsControl.Controls.Add(this.txtTce);
            this.layoutLegsControl.Controls.Add(this.txtQuantityVariation_1);
            this.layoutLegsControl.Controls.Add(this.chkIsOptional_1);
            this.layoutLegsControl.Controls.Add(this.btnRemoveLeg);
            this.layoutLegsControl.Controls.Add(this.btnAddLeg);
            this.layoutLegsControl.Controls.Add(this.cmbStatus_1);
            this.layoutLegsControl.Controls.Add(this.txtQuantity_1);
            this.layoutLegsControl.Controls.Add(this.txtIndexPerc_1);
            this.layoutLegsControl.Controls.Add(this.lookUpIndex_1);
            this.layoutLegsControl.Controls.Add(this.txtRate_1);
            this.layoutLegsControl.Controls.Add(this.cmbRateType_1);
            this.layoutLegsControl.Controls.Add(this.dtpPeriodTo_1);
            this.layoutLegsControl.Controls.Add(this.dtpPeriodFrom_1);
            this.layoutLegsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutLegsControl.Location = new System.Drawing.Point(0, 0);
            this.layoutLegsControl.Name = "layoutLegsControl";
            this.layoutLegsControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(201, 289, 435, 350);
            this.layoutLegsControl.Root = this.layoutControlGroup2;
            this.layoutLegsControl.Size = new System.Drawing.Size(1080, 156);
            this.layoutLegsControl.TabIndex = 0;
            this.layoutLegsControl.Text = "layoutControl1";
            // 
            // txtTce
            // 
            this.txtTce.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTce.Location = new System.Drawing.Point(86, 5);
            this.txtTce.MaximumSize = new System.Drawing.Size(80, 20);
            this.txtTce.MinimumSize = new System.Drawing.Size(80, 20);
            this.txtTce.Name = "txtTce";
            this.txtTce.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtTce.Properties.Mask.EditMask = "f2";
            this.txtTce.Properties.MaxLength = 8;
            this.txtTce.Size = new System.Drawing.Size(80, 20);
            this.txtTce.StyleController = this.layoutLegsControl;
            this.txtTce.TabIndex = 9;
            // 
            // txtQuantityVariation_1
            // 
            this.txtQuantityVariation_1.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtQuantityVariation_1.Location = new System.Drawing.Point(733, 5);
            this.txtQuantityVariation_1.MaximumSize = new System.Drawing.Size(54, 20);
            this.txtQuantityVariation_1.MinimumSize = new System.Drawing.Size(54, 20);
            this.txtQuantityVariation_1.Name = "txtQuantityVariation_1";
            this.txtQuantityVariation_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtQuantityVariation_1.Properties.Mask.EditMask = "P0";
            this.txtQuantityVariation_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtQuantityVariation_1.Properties.MaxLength = 3;
            this.txtQuantityVariation_1.Properties.MaxValue = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.txtQuantityVariation_1.Properties.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtQuantityVariation_1.Size = new System.Drawing.Size(54, 20);
            this.txtQuantityVariation_1.StyleController = this.layoutLegsControl;
            this.txtQuantityVariation_1.TabIndex = 14;
            // 
            // chkIsOptional_1
            // 
            this.chkIsOptional_1.Location = new System.Drawing.Point(843, 5);
            this.chkIsOptional_1.Name = "chkIsOptional_1";
            this.chkIsOptional_1.Properties.Caption = global::Exis.WinClient.Strings.String2;
            this.chkIsOptional_1.Size = new System.Drawing.Size(17, 19);
            this.chkIsOptional_1.StyleController = this.layoutLegsControl;
            this.chkIsOptional_1.TabIndex = 16;
            this.chkIsOptional_1.CheckedChanged += new System.EventHandler(this.ChkIsOptional1CheckedChanged);
            // 
            // btnRemoveLeg
            // 
            this.btnRemoveLeg.ImageIndex = 1;
            this.btnRemoveLeg.ImageList = this.imageList1;
            this.btnRemoveLeg.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveLeg.Location = new System.Drawing.Point(1048, 36);
            this.btnRemoveLeg.Name = "btnRemoveLeg";
            this.btnRemoveLeg.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveLeg.StyleController = this.layoutLegsControl;
            this.btnRemoveLeg.TabIndex = 13;
            this.btnRemoveLeg.Click += new System.EventHandler(this.BtnRemoveLegClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList1.Images.SetKeyName(1, "clear16x16.ico");
            // 
            // btnAddLeg
            // 
            this.btnAddLeg.ImageIndex = 0;
            this.btnAddLeg.ImageList = this.imageList1;
            this.btnAddLeg.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddLeg.Location = new System.Drawing.Point(1048, 2);
            this.btnAddLeg.Name = "btnAddLeg";
            this.btnAddLeg.Size = new System.Drawing.Size(30, 30);
            this.btnAddLeg.StyleController = this.layoutLegsControl;
            this.btnAddLeg.TabIndex = 12;
            this.btnAddLeg.Click += new System.EventHandler(this.BtnAddLegClick);
            // 
            // cmbStatus_1
            // 
            this.cmbStatus_1.Location = new System.Drawing.Point(950, 5);
            this.cmbStatus_1.Name = "cmbStatus_1";
            this.cmbStatus_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus_1.Size = new System.Drawing.Size(91, 20);
            this.cmbStatus_1.StyleController = this.layoutLegsControl;
            this.cmbStatus_1.TabIndex = 10;
            // 
            // txtQuantity_1
            // 
            this.txtQuantity_1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQuantity_1.Location = new System.Drawing.Point(592, 5);
            this.txtQuantity_1.Name = "txtQuantity_1";
            this.txtQuantity_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtQuantity_1.Properties.Mask.EditMask = "D";
            this.txtQuantity_1.Properties.MaxLength = 6;
            this.txtQuantity_1.Properties.MaxValue = new decimal(new int[] {
            500000,
            0,
            0,
            0});
            this.txtQuantity_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQuantity_1.Size = new System.Drawing.Size(51, 20);
            this.txtQuantity_1.StyleController = this.layoutLegsControl;
            this.txtQuantity_1.TabIndex = 9;
            // 
            // txtIndexPerc_1
            // 
            this.txtIndexPerc_1.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtIndexPerc_1.Location = new System.Drawing.Point(447, 5);
            this.txtIndexPerc_1.Name = "txtIndexPerc_1";
            this.txtIndexPerc_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtIndexPerc_1.Properties.Mask.EditMask = "P2";
            this.txtIndexPerc_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtIndexPerc_1.Properties.MaxLength = 6;
            this.txtIndexPerc_1.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtIndexPerc_1.Size = new System.Drawing.Size(55, 20);
            this.txtIndexPerc_1.StyleController = this.layoutLegsControl;
            this.txtIndexPerc_1.TabIndex = 13;
            // 
            // lookUpIndex_1
            // 
            this.lookUpIndex_1.Location = new System.Drawing.Point(256, 5);
            this.lookUpIndex_1.MaximumSize = new System.Drawing.Size(100, 20);
            this.lookUpIndex_1.MinimumSize = new System.Drawing.Size(100, 20);
            this.lookUpIndex_1.Name = "lookUpIndex_1";
            this.lookUpIndex_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpIndex_1.Properties.NullText = global::Exis.WinClient.Strings.String2;
            this.lookUpIndex_1.Size = new System.Drawing.Size(100, 20);
            this.lookUpIndex_1.StyleController = this.layoutLegsControl;
            this.lookUpIndex_1.TabIndex = 15;
            // 
            // txtRate_1
            // 
            this.txtRate_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRate_1.Location = new System.Drawing.Point(-84, 5);
            this.txtRate_1.MaximumSize = new System.Drawing.Size(80, 20);
            this.txtRate_1.MinimumSize = new System.Drawing.Size(80, 20);
            this.txtRate_1.Name = "txtRate_1";
            this.txtRate_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRate_1.Properties.Mask.EditMask = "f2";
            this.txtRate_1.Properties.MaxLength = 8;
            this.txtRate_1.Size = new System.Drawing.Size(80, 20);
            this.txtRate_1.StyleController = this.layoutLegsControl;
            this.txtRate_1.TabIndex = 8;
            // 
            // cmbRateType_1
            // 
            this.cmbRateType_1.Location = new System.Drawing.Point(-274, 5);
            this.cmbRateType_1.Name = "cmbRateType_1";
            this.cmbRateType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbRateType_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbRateType_1.Size = new System.Drawing.Size(100, 20);
            this.cmbRateType_1.StyleController = this.layoutLegsControl;
            this.cmbRateType_1.TabIndex = 8;
            this.cmbRateType_1.SelectedIndexChanged += new System.EventHandler(this.CmbRateType1SelectedIndexChanged);
            // 
            // dtpPeriodTo_1
            // 
            this.dtpPeriodTo_1.EditValue = null;
            this.dtpPeriodTo_1.Location = new System.Drawing.Point(-464, 5);
            this.dtpPeriodTo_1.MaximumSize = new System.Drawing.Size(100, 20);
            this.dtpPeriodTo_1.MinimumSize = new System.Drawing.Size(100, 20);
            this.dtpPeriodTo_1.Name = "dtpPeriodTo_1";
            this.dtpPeriodTo_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpPeriodTo_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpPeriodTo_1.Size = new System.Drawing.Size(100, 20);
            this.dtpPeriodTo_1.StyleController = this.layoutLegsControl;
            this.dtpPeriodTo_1.TabIndex = 6;
            // 
            // dtpPeriodFrom_1
            // 
            this.dtpPeriodFrom_1.EditValue = null;
            this.dtpPeriodFrom_1.Location = new System.Drawing.Point(-654, 5);
            this.dtpPeriodFrom_1.Name = "dtpPeriodFrom_1";
            this.dtpPeriodFrom_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpPeriodFrom_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpPeriodFrom_1.Size = new System.Drawing.Size(100, 20);
            this.dtpPeriodFrom_1.StyleController = this.layoutLegsControl;
            this.dtpPeriodFrom_1.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutLegsGroup,
            this.layoutAddTradeLeg,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(-745, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1825, 139);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutLegsGroup
            // 
            this.layoutLegsGroup.CustomizationFormText = "layoutLegs";
            this.layoutLegsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutLeg_1});
            this.layoutLegsGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutLegsGroup.Name = "layoutLegsGroup";
            this.layoutLegsGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutLegsGroup.Size = new System.Drawing.Size(1791, 139);
            this.layoutLegsGroup.Tag = global::Exis.WinClient.Strings.String2;
            this.layoutLegsGroup.Text = "layoutLegsGroup";
            this.layoutLegsGroup.TextVisible = false;
            // 
            // layoutLeg_1
            // 
            this.layoutLeg_1.CustomizationFormText = "layoutLeg";
            this.layoutLeg_1.GroupBordersVisible = false;
            this.layoutLeg_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPeriodFrom_1,
            this.layoutPeriodTo_1,
            this.layoutRateType_1,
            this.layoutRate_1,
            this.layoutIndex_1,
            this.layoutIndexPerc_1,
            this.layoutQuantity_1,
            this.layoutStatus_1,
            this.layoutOptional_1,
            this.layoutQuantityVariation_1,
            this.layoutTce_1});
            this.layoutLeg_1.Location = new System.Drawing.Point(0, 0);
            this.layoutLeg_1.Name = "layoutLeg_1";
            this.layoutLeg_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutLeg_1.Size = new System.Drawing.Size(1785, 133);
            this.layoutLeg_1.Tag = "1";
            this.layoutLeg_1.Text = "layoutLeg_1";
            this.layoutLeg_1.TextVisible = false;
            // 
            // layoutPeriodFrom_1
            // 
            this.layoutPeriodFrom_1.Control = this.dtpPeriodFrom_1;
            this.layoutPeriodFrom_1.CustomizationFormText = "Period From:";
            this.layoutPeriodFrom_1.Location = new System.Drawing.Point(0, 0);
            this.layoutPeriodFrom_1.MaxSize = new System.Drawing.Size(190, 24);
            this.layoutPeriodFrom_1.MinSize = new System.Drawing.Size(190, 24);
            this.layoutPeriodFrom_1.Name = "layoutPeriodFrom_1";
            this.layoutPeriodFrom_1.Size = new System.Drawing.Size(190, 133);
            this.layoutPeriodFrom_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodFrom_1.Text = "Period From:";
            this.layoutPeriodFrom_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutPeriodTo_1
            // 
            this.layoutPeriodTo_1.Control = this.dtpPeriodTo_1;
            this.layoutPeriodTo_1.CustomizationFormText = "Period To:";
            this.layoutPeriodTo_1.Location = new System.Drawing.Point(190, 0);
            this.layoutPeriodTo_1.MaxSize = new System.Drawing.Size(190, 24);
            this.layoutPeriodTo_1.MinSize = new System.Drawing.Size(190, 24);
            this.layoutPeriodTo_1.Name = "layoutPeriodTo_1";
            this.layoutPeriodTo_1.Size = new System.Drawing.Size(190, 133);
            this.layoutPeriodTo_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodTo_1.Text = "Period To:";
            this.layoutPeriodTo_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutRateType_1
            // 
            this.layoutRateType_1.Control = this.cmbRateType_1;
            this.layoutRateType_1.CustomizationFormText = "Rate Type:";
            this.layoutRateType_1.Location = new System.Drawing.Point(380, 0);
            this.layoutRateType_1.MaxSize = new System.Drawing.Size(190, 24);
            this.layoutRateType_1.MinSize = new System.Drawing.Size(190, 24);
            this.layoutRateType_1.Name = "layoutRateType_1";
            this.layoutRateType_1.Size = new System.Drawing.Size(190, 133);
            this.layoutRateType_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRateType_1.Text = "Rate Type:";
            this.layoutRateType_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutRate_1
            // 
            this.layoutRate_1.Control = this.txtRate_1;
            this.layoutRate_1.CustomizationFormText = "Rate($/ton):";
            this.layoutRate_1.Location = new System.Drawing.Point(570, 0);
            this.layoutRate_1.MaxSize = new System.Drawing.Size(170, 24);
            this.layoutRate_1.MinSize = new System.Drawing.Size(170, 24);
            this.layoutRate_1.Name = "layoutRate_1";
            this.layoutRate_1.Size = new System.Drawing.Size(170, 133);
            this.layoutRate_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRate_1.Text = "Rate($/ton):";
            this.layoutRate_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutIndex_1
            // 
            this.layoutIndex_1.Control = this.lookUpIndex_1;
            this.layoutIndex_1.CustomizationFormText = "Index:";
            this.layoutIndex_1.Location = new System.Drawing.Point(910, 0);
            this.layoutIndex_1.MaxSize = new System.Drawing.Size(191, 24);
            this.layoutIndex_1.MinSize = new System.Drawing.Size(191, 24);
            this.layoutIndex_1.Name = "layoutIndex_1";
            this.layoutIndex_1.Size = new System.Drawing.Size(191, 133);
            this.layoutIndex_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutIndex_1.Text = "Index:";
            this.layoutIndex_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutIndexPerc_1
            // 
            this.layoutIndexPerc_1.Control = this.txtIndexPerc_1;
            this.layoutIndexPerc_1.CustomizationFormText = "Index Perc.:";
            this.layoutIndexPerc_1.Location = new System.Drawing.Point(1101, 0);
            this.layoutIndexPerc_1.MaxSize = new System.Drawing.Size(145, 24);
            this.layoutIndexPerc_1.MinSize = new System.Drawing.Size(145, 24);
            this.layoutIndexPerc_1.Name = "layoutIndexPerc_1";
            this.layoutIndexPerc_1.Size = new System.Drawing.Size(145, 133);
            this.layoutIndexPerc_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutIndexPerc_1.Text = "Index Perc.:";
            this.layoutIndexPerc_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutQuantity_1
            // 
            this.layoutQuantity_1.Control = this.txtQuantity_1;
            this.layoutQuantity_1.CustomizationFormText = "Quantity:";
            this.layoutQuantity_1.Location = new System.Drawing.Point(1246, 0);
            this.layoutQuantity_1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutQuantity_1.MinSize = new System.Drawing.Size(141, 24);
            this.layoutQuantity_1.Name = "layoutQuantity_1";
            this.layoutQuantity_1.Size = new System.Drawing.Size(141, 133);
            this.layoutQuantity_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutQuantity_1.Text = "Quantity (tons):";
            this.layoutQuantity_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutStatus_1
            // 
            this.layoutStatus_1.Control = this.cmbStatus_1;
            this.layoutStatus_1.CustomizationFormText = "Status:";
            this.layoutStatus_1.Location = new System.Drawing.Point(1604, 0);
            this.layoutStatus_1.MaxSize = new System.Drawing.Size(181, 24);
            this.layoutStatus_1.MinSize = new System.Drawing.Size(181, 24);
            this.layoutStatus_1.Name = "layoutStatus_1";
            this.layoutStatus_1.Size = new System.Drawing.Size(181, 133);
            this.layoutStatus_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus_1.Text = "Status:";
            this.layoutStatus_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutOptional_1
            // 
            this.layoutOptional_1.Control = this.chkIsOptional_1;
            this.layoutOptional_1.CustomizationFormText = "Optional:";
            this.layoutOptional_1.Location = new System.Drawing.Point(1532, 0);
            this.layoutOptional_1.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutOptional_1.MinSize = new System.Drawing.Size(72, 23);
            this.layoutOptional_1.Name = "layoutOptional_1";
            this.layoutOptional_1.Size = new System.Drawing.Size(72, 133);
            this.layoutOptional_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptional_1.Text = "Optional:";
            this.layoutOptional_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptional_1.TextSize = new System.Drawing.Size(50, 20);
            this.layoutOptional_1.TextToControlDistance = 1;
            // 
            // layoutQuantityVariation_1
            // 
            this.layoutQuantityVariation_1.Control = this.txtQuantityVariation_1;
            this.layoutQuantityVariation_1.CustomizationFormText = "Quant. Variation:";
            this.layoutQuantityVariation_1.Location = new System.Drawing.Point(1387, 0);
            this.layoutQuantityVariation_1.MaxSize = new System.Drawing.Size(145, 24);
            this.layoutQuantityVariation_1.MinSize = new System.Drawing.Size(145, 24);
            this.layoutQuantityVariation_1.Name = "layoutQuantityVariation_1";
            this.layoutQuantityVariation_1.Size = new System.Drawing.Size(145, 133);
            this.layoutQuantityVariation_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutQuantityVariation_1.Text = "Quant. Variation:";
            this.layoutQuantityVariation_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutTce_1
            // 
            this.layoutTce_1.Control = this.txtTce;
            this.layoutTce_1.CustomizationFormText = "TCE($/day):";
            this.layoutTce_1.Location = new System.Drawing.Point(740, 0);
            this.layoutTce_1.MaxSize = new System.Drawing.Size(170, 24);
            this.layoutTce_1.MinSize = new System.Drawing.Size(170, 24);
            this.layoutTce_1.Name = "layoutTce_1";
            this.layoutTce_1.Size = new System.Drawing.Size(170, 133);
            this.layoutTce_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutTce_1.Text = "TCE($/day):";
            this.layoutTce_1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutAddTradeLeg
            // 
            this.layoutAddTradeLeg.Control = this.btnAddLeg;
            this.layoutAddTradeLeg.CustomizationFormText = "layoutAddTradeLeg";
            this.layoutAddTradeLeg.Location = new System.Drawing.Point(1791, 0);
            this.layoutAddTradeLeg.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutAddTradeLeg.MinSize = new System.Drawing.Size(34, 34);
            this.layoutAddTradeLeg.Name = "layoutAddTradeLeg";
            this.layoutAddTradeLeg.Size = new System.Drawing.Size(34, 34);
            this.layoutAddTradeLeg.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAddTradeLeg.Text = "layoutAddTradeLeg";
            this.layoutAddTradeLeg.TextSize = new System.Drawing.Size(0, 0);
            this.layoutAddTradeLeg.TextToControlDistance = 0;
            this.layoutAddTradeLeg.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnRemoveLeg;
            this.layoutControlItem3.CustomizationFormText = "layoutRemoveTradeLeg";
            this.layoutControlItem3.Location = new System.Drawing.Point(1791, 34);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(34, 105);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // cmbType
            // 
            this.cmbType.Location = new System.Drawing.Point(33, 2);
            this.cmbType.Name = "cmbType";
            this.cmbType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbType.Size = new System.Drawing.Size(121, 20);
            this.cmbType.StyleController = this.layoutRoot;
            this.cmbType.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutType,
            this.layoutTradeLegs,
            this.layoutVesselIndex,
            this.layoutControlItem1,
            this.layoutLoadingPort,
            this.layoutDischargingPort,
            this.layoutAddress});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1113, 208);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutType
            // 
            this.layoutType.Control = this.cmbType;
            this.layoutType.CustomizationFormText = "Type:";
            this.layoutType.Location = new System.Drawing.Point(0, 0);
            this.layoutType.MaxSize = new System.Drawing.Size(156, 24);
            this.layoutType.MinSize = new System.Drawing.Size(156, 24);
            this.layoutType.Name = "layoutType";
            this.layoutType.Size = new System.Drawing.Size(156, 24);
            this.layoutType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutType.Text = "Type:";
            this.layoutType.TextSize = new System.Drawing.Size(28, 13);
            // 
            // layoutTradeLegs
            // 
            this.layoutTradeLegs.Control = this.panelLegs;
            this.layoutTradeLegs.CustomizationFormText = "Trade Legs:";
            this.layoutTradeLegs.Location = new System.Drawing.Point(0, 48);
            this.layoutTradeLegs.Name = "layoutTradeLegs";
            this.layoutTradeLegs.Size = new System.Drawing.Size(1113, 160);
            this.layoutTradeLegs.Text = "Legs:";
            this.layoutTradeLegs.TextSize = new System.Drawing.Size(26, 13);
            // 
            // layoutVesselIndex
            // 
            this.layoutVesselIndex.Control = this.txtVesselIndex;
            this.layoutVesselIndex.CustomizationFormText = "Vessel Index:";
            this.layoutVesselIndex.Location = new System.Drawing.Point(980, 0);
            this.layoutVesselIndex.MaxSize = new System.Drawing.Size(133, 24);
            this.layoutVesselIndex.MinSize = new System.Drawing.Size(133, 24);
            this.layoutVesselIndex.Name = "layoutVesselIndex";
            this.layoutVesselIndex.Size = new System.Drawing.Size(133, 24);
            this.layoutVesselIndex.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutVesselIndex.Text = "Vessel Index:";
            this.layoutVesselIndex.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lookupVessel;
            this.layoutControlItem1.CustomizationFormText = "Vessel:";
            this.layoutControlItem1.Location = new System.Drawing.Point(156, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(824, 24);
            this.layoutControlItem1.Text = "Vessel:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(34, 13);
            // 
            // layoutLoadingPort
            // 
            this.layoutLoadingPort.Control = this.txtLoadingPort;
            this.layoutLoadingPort.CustomizationFormText = "Loading Port:";
            this.layoutLoadingPort.Location = new System.Drawing.Point(0, 24);
            this.layoutLoadingPort.Name = "layoutLoadingPort";
            this.layoutLoadingPort.Size = new System.Drawing.Size(480, 24);
            this.layoutLoadingPort.Text = "Loading Port:";
            this.layoutLoadingPort.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutDischargingPort
            // 
            this.layoutDischargingPort.Control = this.txtDischargingPort;
            this.layoutDischargingPort.CustomizationFormText = "Discharging Port:";
            this.layoutDischargingPort.Location = new System.Drawing.Point(480, 24);
            this.layoutDischargingPort.Name = "layoutDischargingPort";
            this.layoutDischargingPort.Size = new System.Drawing.Size(454, 24);
            this.layoutDischargingPort.Text = "Discharging Port:";
            this.layoutDischargingPort.TextSize = new System.Drawing.Size(82, 13);
            // 
            // layoutAddress
            // 
            this.layoutAddress.Control = this.txtAddress;
            this.layoutAddress.CustomizationFormText = "Address:";
            this.layoutAddress.Location = new System.Drawing.Point(934, 24);
            this.layoutAddress.Name = "layoutAddress";
            this.layoutAddress.Size = new System.Drawing.Size(179, 24);
            this.layoutAddress.Text = "Address:";
            this.layoutAddress.TextSize = new System.Drawing.Size(43, 13);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // AddEditViewTradeCargoInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutRoot);
            this.Name = "AddEditViewTradeCargoInfoControl";
            this.Size = new System.Drawing.Size(1113, 208);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDischargingPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoadingPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupVessel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVesselIndex.Properties)).EndInit();
            this.panelLegs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsControl)).EndInit();
            this.layoutLegsControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTce.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityVariation_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsOptional_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndexPerc_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpIndex_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbRateType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLeg_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodFrom_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodTo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRateType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndex_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndexPerc_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantity_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptional_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityVariation_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTce_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddTradeLeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeLegs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVesselIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLoadingPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDischargingPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbType;
        private DevExpress.XtraLayout.LayoutControlItem layoutType;
        private System.Windows.Forms.Panel panelLegs;
        private DevExpress.XtraLayout.LayoutControlItem layoutTradeLegs;
        private DevExpress.XtraLayout.LayoutControl layoutLegsControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutLegsGroup;
        private DevExpress.XtraEditors.SpinEdit txtVesselIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutVesselIndex;
        private DevExpress.XtraEditors.SpinEdit txtAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutAddress;
        private DevExpress.XtraEditors.DateEdit dtpPeriodFrom_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodFrom_1;
        private DevExpress.XtraEditors.DateEdit dtpPeriodTo_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodTo_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbRateType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRateType_1;
        private DevExpress.XtraEditors.SpinEdit txtRate_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRate_1;
        private DevExpress.XtraEditors.LookUpEdit lookUpIndex_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutIndex_1;
        private DevExpress.XtraEditors.SpinEdit txtIndexPerc_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutIndexPerc_1;
        private DevExpress.XtraEditors.SpinEdit txtQuantity_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutQuantity_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus_1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.LookUpEdit lookupVessel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.TextEdit txtDischargingPort;
        private DevExpress.XtraEditors.TextEdit txtLoadingPort;
        private DevExpress.XtraLayout.LayoutControlItem layoutLoadingPort;
        private DevExpress.XtraLayout.LayoutControlItem layoutDischargingPort;
        private DevExpress.XtraLayout.LayoutControlGroup layoutLeg_1;
        private DevExpress.XtraEditors.SimpleButton btnAddLeg;
        private DevExpress.XtraLayout.LayoutControlItem layoutAddTradeLeg;
        private DevExpress.XtraEditors.SimpleButton btnRemoveLeg;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit chkIsOptional_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptional_1;
        private DevExpress.XtraEditors.SpinEdit txtQuantityVariation_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutQuantityVariation_1;
        private DevExpress.XtraEditors.SpinEdit txtTce;
        private DevExpress.XtraLayout.LayoutControlItem layoutTce_1;
    }
}
