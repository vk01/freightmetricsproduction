﻿namespace Exis.WinClient.Controls.Core
{
    partial class MCCriteriaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MCCriteriaForm));
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtSimulationRuns = new DevExpress.XtraEditors.SpinEdit();
            this.rdgMTMResultsType = new DevExpress.XtraEditors.RadioGroup();
            this.chkUseSpotValues = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionPremium = new DevExpress.XtraEditors.CheckEdit();
            this.txtMarketSensitivityValue = new DevExpress.XtraEditors.SpinEdit();
            this.cmbMarketSensitivityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPositionMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpFilterCurveDate = new DevExpress.XtraEditors.DateEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnRun = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFilterCurveDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutOptionPremium = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUseSpotValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMTMResultsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemSimulationRuns = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSimulationRuns.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgMTMResultsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMTMResultsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSimulationRuns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.StyleController = this.layoutControl1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtSimulationRuns);
            this.layoutControl1.Controls.Add(this.rdgMTMResultsType);
            this.layoutControl1.Controls.Add(this.chkUseSpotValues);
            this.layoutControl1.Controls.Add(this.chkOptionPremium);
            this.layoutControl1.Controls.Add(this.txtMarketSensitivityValue);
            this.layoutControl1.Controls.Add(this.cmbMarketSensitivityType);
            this.layoutControl1.Controls.Add(this.cmbPositionMethod);
            this.layoutControl1.Controls.Add(this.dtpFilterCurveDate);
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.btnRun);
            this.layoutControl1.Controls.Add(this.txtName);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup;
            // 
            // txtSimulationRuns
            // 
            resources.ApplyResources(this.txtSimulationRuns, "txtSimulationRuns");
            this.txtSimulationRuns.Name = "txtSimulationRuns";
            this.txtSimulationRuns.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtSimulationRuns.StyleController = this.layoutControl1;
            // 
            // rdgMTMResultsType
            // 
            resources.ApplyResources(this.rdgMTMResultsType, "rdgMTMResultsType");
            this.rdgMTMResultsType.Name = "rdgMTMResultsType";
            this.rdgMTMResultsType.Properties.Columns = 2;
            this.rdgMTMResultsType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(resources.GetString("rdgMTMResultsType.Properties.Items"), resources.GetString("rdgMTMResultsType.Properties.Items1")),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(resources.GetString("rdgMTMResultsType.Properties.Items2"), resources.GetString("rdgMTMResultsType.Properties.Items3"))});
            this.rdgMTMResultsType.StyleController = this.layoutControl1;
            // 
            // chkUseSpotValues
            // 
            resources.ApplyResources(this.chkUseSpotValues, "chkUseSpotValues");
            this.chkUseSpotValues.Name = "chkUseSpotValues";
            this.chkUseSpotValues.Properties.Caption = global::Exis.WinClient.Strings.String2;
            this.chkUseSpotValues.StyleController = this.layoutControl1;
            // 
            // chkOptionPremium
            // 
            resources.ApplyResources(this.chkOptionPremium, "chkOptionPremium");
            this.chkOptionPremium.Name = "chkOptionPremium";
            this.chkOptionPremium.Properties.Caption = global::Exis.WinClient.Strings.String2;
            this.chkOptionPremium.StyleController = this.layoutControl1;
            // 
            // txtMarketSensitivityValue
            // 
            resources.ApplyResources(this.txtMarketSensitivityValue, "txtMarketSensitivityValue");
            this.txtMarketSensitivityValue.Name = "txtMarketSensitivityValue";
            this.txtMarketSensitivityValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMarketSensitivityValue.StyleController = this.layoutControl1;
            // 
            // cmbMarketSensitivityType
            // 
            resources.ApplyResources(this.cmbMarketSensitivityType, "cmbMarketSensitivityType");
            this.cmbMarketSensitivityType.Name = "cmbMarketSensitivityType";
            this.cmbMarketSensitivityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbMarketSensitivityType.Properties.Buttons"))))});
            this.cmbMarketSensitivityType.Properties.Items.AddRange(new object[] {
            resources.GetString("cmbMarketSensitivityType.Properties.Items"),
            resources.GetString("cmbMarketSensitivityType.Properties.Items1"),
            resources.GetString("cmbMarketSensitivityType.Properties.Items2"),
            resources.GetString("cmbMarketSensitivityType.Properties.Items3")});
            this.cmbMarketSensitivityType.StyleController = this.layoutControl1;
            this.cmbMarketSensitivityType.SelectedValueChanged += new System.EventHandler(this.cmbMarketSensitivityType_SelectedValueChanged);
            // 
            // cmbPositionMethod
            // 
            resources.ApplyResources(this.cmbPositionMethod, "cmbPositionMethod");
            this.cmbPositionMethod.Name = "cmbPositionMethod";
            this.cmbPositionMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbPositionMethod.Properties.Buttons"))))});
            this.cmbPositionMethod.Properties.Items.AddRange(new object[] {
            resources.GetString("cmbPositionMethod.Properties.Items"),
            resources.GetString("cmbPositionMethod.Properties.Items1"),
            resources.GetString("cmbPositionMethod.Properties.Items2")});
            this.cmbPositionMethod.StyleController = this.layoutControl1;
            // 
            // dtpFilterCurveDate
            // 
            resources.ApplyResources(this.dtpFilterCurveDate, "dtpFilterCurveDate");
            this.dtpFilterCurveDate.Name = "dtpFilterCurveDate";
            this.dtpFilterCurveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dtpFilterCurveDate.Properties.Buttons"))))});
            this.dtpFilterCurveDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterCurveDate.StyleController = this.layoutControl1;
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnRun
            // 
            resources.ApplyResources(this.btnRun, "btnRun");
            this.btnRun.Name = "btnRun";
            this.btnRun.StyleController = this.layoutControl1;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // layoutControlGroup
            // 
            resources.ApplyResources(this.layoutControlGroup, "layoutControlGroup");
            this.layoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup.GroupBordersVisible = false;
            this.layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutFilterCurveDate,
            this.layoutMarketSensitivityType,
            this.emptySpaceItem1,
            this.layoutOptionPremium,
            this.layoutUseSpotValues,
            this.layoutMTMResultsType,
            this.layoutPositionMethod,
            this.layoutMarketSensitivityValue,
            this.layoutItemSimulationRuns});
            this.layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup.Name = "layoutControlGroup";
            this.layoutControlGroup.Size = new System.Drawing.Size(383, 271);
            this.layoutControlGroup.TextVisible = false;
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.txtName;
            resources.ApplyResources(this.layoutControlItemName, "layoutControlItemName");
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(363, 24);
            this.layoutControlItemName.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnRun;
            resources.ApplyResources(this.layoutControlItem1, "layoutControlItem1");
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 225);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(199, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCancel;
            resources.ApplyResources(this.layoutControlItem2, "layoutControlItem2");
            this.layoutControlItem2.Location = new System.Drawing.Point(199, 225);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(164, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutFilterCurveDate
            // 
            this.layoutFilterCurveDate.Control = this.dtpFilterCurveDate;
            resources.ApplyResources(this.layoutFilterCurveDate, "layoutFilterCurveDate");
            this.layoutFilterCurveDate.Location = new System.Drawing.Point(0, 48);
            this.layoutFilterCurveDate.Name = "layoutFilterCurveDate";
            this.layoutFilterCurveDate.Size = new System.Drawing.Size(363, 24);
            this.layoutFilterCurveDate.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutMarketSensitivityType
            // 
            this.layoutMarketSensitivityType.Control = this.cmbMarketSensitivityType;
            resources.ApplyResources(this.layoutMarketSensitivityType, "layoutMarketSensitivityType");
            this.layoutMarketSensitivityType.Location = new System.Drawing.Point(0, 125);
            this.layoutMarketSensitivityType.Name = "layoutMarketSensitivityType";
            this.layoutMarketSensitivityType.Size = new System.Drawing.Size(363, 24);
            this.layoutMarketSensitivityType.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem1, "emptySpaceItem1");
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 196);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(363, 29);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutOptionPremium
            // 
            this.layoutOptionPremium.Control = this.chkOptionPremium;
            resources.ApplyResources(this.layoutOptionPremium, "layoutOptionPremium");
            this.layoutOptionPremium.Location = new System.Drawing.Point(0, 173);
            this.layoutOptionPremium.Name = "layoutOptionPremium";
            this.layoutOptionPremium.Size = new System.Drawing.Size(181, 23);
            this.layoutOptionPremium.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutUseSpotValues
            // 
            this.layoutUseSpotValues.Control = this.chkUseSpotValues;
            resources.ApplyResources(this.layoutUseSpotValues, "layoutUseSpotValues");
            this.layoutUseSpotValues.Location = new System.Drawing.Point(181, 173);
            this.layoutUseSpotValues.Name = "layoutUseSpotValues";
            this.layoutUseSpotValues.Size = new System.Drawing.Size(182, 23);
            this.layoutUseSpotValues.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutUseSpotValues.TextSize = new System.Drawing.Size(43, 13);
            this.layoutUseSpotValues.TextToControlDistance = 5;
            // 
            // layoutMTMResultsType
            // 
            this.layoutMTMResultsType.Control = this.rdgMTMResultsType;
            resources.ApplyResources(this.layoutMTMResultsType, "layoutMTMResultsType");
            this.layoutMTMResultsType.Location = new System.Drawing.Point(0, 96);
            this.layoutMTMResultsType.MinSize = new System.Drawing.Size(175, 29);
            this.layoutMTMResultsType.Name = "layoutMTMResultsType";
            this.layoutMTMResultsType.Size = new System.Drawing.Size(363, 29);
            this.layoutMTMResultsType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMTMResultsType.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutPositionMethod
            // 
            this.layoutPositionMethod.Control = this.cmbPositionMethod;
            resources.ApplyResources(this.layoutPositionMethod, "layoutPositionMethod");
            this.layoutPositionMethod.Location = new System.Drawing.Point(0, 72);
            this.layoutPositionMethod.Name = "layoutPositionMethod";
            this.layoutPositionMethod.Size = new System.Drawing.Size(363, 24);
            this.layoutPositionMethod.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutMarketSensitivityValue
            // 
            this.layoutMarketSensitivityValue.Control = this.txtMarketSensitivityValue;
            resources.ApplyResources(this.layoutMarketSensitivityValue, "layoutMarketSensitivityValue");
            this.layoutMarketSensitivityValue.Location = new System.Drawing.Point(0, 149);
            this.layoutMarketSensitivityValue.Name = "layoutMarketSensitivityValue";
            this.layoutMarketSensitivityValue.Size = new System.Drawing.Size(363, 24);
            this.layoutMarketSensitivityValue.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutItemSimulationRuns
            // 
            this.layoutItemSimulationRuns.Control = this.txtSimulationRuns;
            resources.ApplyResources(this.layoutItemSimulationRuns, "layoutItemSimulationRuns");
            this.layoutItemSimulationRuns.Location = new System.Drawing.Point(0, 24);
            this.layoutItemSimulationRuns.Name = "layoutItemSimulationRuns";
            this.layoutItemSimulationRuns.Size = new System.Drawing.Size(363, 24);
            this.layoutItemSimulationRuns.TextSize = new System.Drawing.Size(118, 13);
            // 
            // imageList24
            // 
            resources.ApplyResources(this.imageList24, "imageList24");
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "event32x32.ico");
            this.imageList24.Images.SetKeyName(1, "exit.ico");
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // MCCriteriaForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "MCCriteriaForm";
            this.ShowInTaskbar = false;
            this.Activated += new System.EventHandler(this.MCGetNameForm_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSimulationRuns.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgMTMResultsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMTMResultsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemSimulationRuns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnRun;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbMarketSensitivityType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPositionMethod;
        private DevExpress.XtraEditors.DateEdit dtpFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityType;
        private DevExpress.XtraEditors.RadioGroup rdgMTMResultsType;
        private DevExpress.XtraEditors.CheckEdit chkUseSpotValues;
        private DevExpress.XtraEditors.CheckEdit chkOptionPremium;
        private DevExpress.XtraEditors.SpinEdit txtMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutUseSpotValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutMTMResultsType;
        private DevExpress.XtraEditors.SpinEdit txtSimulationRuns;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemSimulationRuns;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
    }
}