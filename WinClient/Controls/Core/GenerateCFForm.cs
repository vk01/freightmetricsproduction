﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Itenso.TimePeriod;
using SummaryItemType = DevExpress.Data.SummaryItemType;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;
using DevExpress.XtraEditors.Controls;

namespace Exis.WinClient.Controls.Core
{
    public partial class GenerateCFForm : DevExpress.XtraEditors.XtraForm
    {
        #region Nested type: FormStateEnum

        private enum FormStateEnum
        {
            BeforeExecution,
            WhileExecuting,
            AfterExecution
        }

        #endregion

        #region Private Properties

        private AppParameter _appParameter;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm1;
        private WaitDialogForm _dialogForm2;
 
        private TreeListNode _expandedNode;
        private FormStateEnum _formState = FormStateEnum.BeforeExecution;
        private bool isLoad;

        private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<DateTime, decimal>> _tradeMTMResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string,Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMRatiosResults = new Dictionary<string,Dictionary<string, Dictionary<DateTime, decimal>>>();
        private Dictionary<string, decimal> _tradeEmbeddedValues = new Dictionary<string, decimal>();
        private Dictionary<DateTime, Dictionary<string, decimal>> _indexesValues = new Dictionary<DateTime, Dictionary<string, decimal>>(); 

        private bool RatioReportPL;
        private bool RatioReportCashFlow;
        private bool InSelection;


        //params for query 
        private DateTime _positionDate;
        private DateTime _periodFrom;
        private DateTime _periodTo;
        private List<string> _tradeIdentifiers;
        private List<TradeInformation> _tradeInformations;
        private List<Book> _books;
        private HierarchyNodeInfo _selectedHierarchyNode;

        //params for info
        private ViewTradeGeneralInfo _viewTradesGeneralInfo;

        #endregion

        #region Constructors

        public GenerateCFForm(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            _viewTradesGeneralInfo = viewTradesGeneralInfo;

            InitializeComponent();
            SetGuiControls();
            InitializeControls();
            GetCFModels();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {

            if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject == null)
                lblEntity.Text = _viewTradesGeneralInfo.selectedHierarchyNode.StrObject;
            else
            {
                if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Company))
                    lblEntity.Text = "Company" + "-" +
                                     ((Company)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Book))
                    lblEntity.Text = "Book" + "-" +
                                     ((Book)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Market))
                    lblEntity.Text = "Market" + "-" +
                                     ((Market)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Trader))
                    lblEntity.Text = "Trader" + "-" +
                                     ((Trader)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Vessel))
                    lblEntity.Text = "Vessel" + "-" +
                                     ((Vessel)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
            }


            lblPeriodFrom.Text = _viewTradesGeneralInfo.periodFrom.ToShortDateString();
            lblPeriodTo.Text = _viewTradesGeneralInfo.periodTo.ToShortDateString();
            lblIsDraft.Text = _viewTradesGeneralInfo.IsDraft.ToString();
            lblIsMinimumPeriod.Text = _viewTradesGeneralInfo.IsMinimumPeriod.ToString();
            lblIsOptionalPeriod.Text = _viewTradesGeneralInfo.IsOptionalPeriod.ToString();
            lblOnlyNonZeroTotalDays.Text = _viewTradesGeneralInfo.OnlyNonZeroTotalDays.ToString();


            int gridViewColumnindex = 0;
            GridColumn column = AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                          UnboundColumnType.Bound, true, null);
            column.SummaryItem.SummaryType = SummaryItemType.Count;
            column.SummaryItem.DisplayFormat = "Count: {0:N0}";
    
            gridTradeActionResults.BeginUpdate();
            gridTradeActionResultsView.Columns.Clear();
            gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;

            gridTradeActionResultsView.Columns.Add(AddColumn("Type", Strings.Type,  gridViewColumnindex++,
                                                             UnboundColumnType.Bound, true,null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Books", Strings.Books,  gridViewColumnindex++,
                                                             UnboundColumnType.String, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, true,null));
            gridTradeActionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false,null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                                             UnboundColumnType.String, false, null));

            try
            {
                gridTradeActionResultsView.RestoreLayoutFromXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }

            gridTradeActionResults.EndUpdate();


            lookupCFModel.Properties.DisplayMember = "Name";
            lookupCFModel.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Model_name,
                FieldName = "Name",
                Width = 0
            };
            lookupCFModel.Properties.Columns.Add(col);
            lookupCFModel.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCFModel.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCFModel.Properties.NullValuePrompt = Strings.Select_a_Cash_flow_model;

            //dtpFilterCurveDate.Properties.MaxValue = DateTime.Today.AddDays(-1);
            //dtpFilterCurveDate.DateTime = DateTime.Today.AddDays(-1);
            dtpFilterCurveDate.DateTime = _viewTradesGeneralInfo.lastImportDate;
            dtpFilterCurveDate.Properties.MaxValue = _viewTradesGeneralInfo.lastImportDate;

            cmbPositionMethod.SelectedItem = "Dynamic";
            cmbMarketSensitivityType.SelectedIndex = 0;

            rdgResultsAggregationType.SelectedIndex = 0;
            rdgPositionResultsType.SelectedIndex = 0;
            //rdgMTMResultsType.SelectedIndex = 0;
        }

        private void SetGuiControls()
        {
            if (_formState == FormStateEnum.BeforeExecution)
            {
                btnGeneratePosition.Enabled = true;
                gridTradeActionResults.DataSource = null;
                SetEnabledForWorkingStatus(true);
                SetEnabledResultsToolbarStatus(false);
            }
            else if (_formState == FormStateEnum.WhileExecuting)
            {
              
                btnGeneratePosition.Enabled = false;
 
                SetEnabledForWorkingStatus(true);
                SetEnabledResultsToolbarStatus(false);
            }
            else if (_formState == FormStateEnum.AfterExecution)
            {
                btnGeneratePosition.Enabled = true;
                SetEnabledForWorkingStatus(true);
                if (gridTradeActionResultsView.DataRowCount > 0) SetEnabledResultsToolbarStatus(true);
                
            }
        }

        private void SetEnabledResultsToolbarStatus(bool status)
        {
            rdgResultsAggregationType.Enabled = status;
            rdgPositionResultsType.Enabled = status;
            chkCalculateSums.Enabled = status;
            chkCalculateCommissions.Enabled = status;
            btnExportResults.Enabled = status;
        }
        
        private GridColumn AddColumn(string name, string caption, int columnIndex,UnboundColumnType columnType, bool isFixedLeft, object Tag)
        {
            var column = new GridColumn();
            column.Visible = true;
            column.AppearanceCell.Options.UseTextOptions = true;
            column.OptionsColumn.FixedWidth = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsColumn.AllowEdit = false;
            column.FieldName = name;
            column.Caption = caption;
            column.Tag = Tag;
            column.VisibleIndex = columnIndex;
            column.Fixed = isFixedLeft ? FixedStyle.Left : FixedStyle.None;
            if (columnType != UnboundColumnType.Bound)
                column.UnboundType = columnType;

            switch (columnType)
            {
                case UnboundColumnType.String:
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.DateTime:
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.DisplayFormat.FormatType = FormatType.DateTime;
                    column.DisplayFormat.FormatString = "G";
                    break;
                case UnboundColumnType.Decimal:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.UnboundType = UnboundColumnType.Decimal;
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "N3";
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Integer:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.UnboundType = UnboundColumnType.Integer;
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Boolean:
                    column.UnboundType = UnboundColumnType.Boolean;
                    column.OptionsColumn.AllowEdit = false;
                    break;
            }

            return column;
        }

        private void CreateResultGridColumns(Dictionary<string, Dictionary<DateTime, decimal>> tradeResults, Dictionary<string, decimal> tradeEmbeddedValues, string type)
        {
            var groupedColumns = gridTradeActionResultsView.GroupedColumns;

            gridTradeActionResults.BeginUpdate();
            gridTradeActionResults.DataSource = null;
            gridTradeActionResultsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
            gridTradeActionResultsView.Columns.Clear();
            gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;
            int gridViewColumnindex = 0;

            if (type == "MTM" || type == "POSITION")
            {
                gridTradeActionResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Books", Strings.Books,gridViewColumnindex++,
                                                                UnboundColumnType.String,true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound,true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound,false,null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                GridColumn lastColumn = AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++, UnboundColumnType.String, false, null);
                gridTradeActionResultsView.Columns.Add(lastColumn);

                if (type == "MTM")
                {
                    GridColumn column = AddColumn(
                        "EmbeddedValue",
                        "Embedded Value", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, tradeEmbeddedValues);
                    gridTradeActionResultsView.Columns.Add(column);

                    if (chkCalculateSums.Checked)
                    {
                        //Created only to make a void summary item, so that no custom summary is diplayed in that cell
                        var dummySummarytem = new GridColumnSummaryItem();
                        dummySummarytem.DisplayFormat = "Totals:";
                        dummySummarytem.FieldName = "dummie";
                        dummySummarytem.SummaryType = SummaryItemType.Custom;
                        column.Summary.Add(dummySummarytem);
                    }
                    lastColumn = column;
                }

                List<DateTime> dates = tradeResults.First().Value.Select(a => a.Key).ToList();

                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    foreach (DateTime date in dates)
                    {
                        GridColumn column = AddColumn(
                            date.ToString("MMM-yyyy", new CultureInfo("en-GB")),
                            date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                            UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                        gridTradeActionResultsView.Columns.Add(column);
                        if (chkCalculateSums.Checked)
                        {
                            var item = new GridGroupSummaryItem();
                            item.FieldName = column.FieldName;
                            item.ShowInGroupColumnFooter = column;
                            item.SummaryType = SummaryItemType.Sum;
                            item.DisplayFormat = "{0:N3}";
                            gridTradeActionResultsView.GroupSummary.Add(item);

                            column.SummaryItem.SummaryType = SummaryItemType.Sum;
                            column.SummaryItem.DisplayFormat = "{0:N3}";
                        }
                        if (type == "MTM")
                        {
                            DateTime dt = (DateTime)column.Tag;
                            foreach (var pair in _indexesValues[dt])
                            {
                                if (string.IsNullOrEmpty(pair.Key)) continue;

                                var item = new GridColumnSummaryItem();
                                item.FieldName = pair.Key;
                                item.SummaryType = SummaryItemType.Custom;
                                item.DisplayFormat = "{0:N3}";
                                item.Tag = dt.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                                column.Summary.Add(item);
                            }
                        }
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                {
                    int quarterAdded = 0;
                    foreach (DateTime date in dates)
                    {
                        int intQuarter = ((date.Month - 1) / 3) + 1;
                        if (intQuarter != quarterAdded)
                        {
                            quarterAdded = intQuarter;
                            string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                            GridColumn column = AddColumn(
                                strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")),
                                strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                                UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                            gridTradeActionResultsView.Columns.Add(column);
                            if (chkCalculateSums.Checked)
                            {
                                var item = new GridGroupSummaryItem();
                                item.FieldName = column.FieldName;
                                item.ShowInGroupColumnFooter = column;
                                item.SummaryType = SummaryItemType.Sum;
                                item.DisplayFormat = "{0:N3}";
                                gridTradeActionResultsView.GroupSummary.Add(item);

                                column.SummaryItem.SummaryType = SummaryItemType.Sum;
                                column.SummaryItem.DisplayFormat = "{0:N3}";
                            }
                            if (type == "MTM")
                            {
                                var dt = (DateTime)column.Tag;
                                foreach (var pair in _indexesValues[dt])
                                {
                                    if (string.IsNullOrEmpty(pair.Key)) continue;

                                    var item = new GridColumnSummaryItem();
                                    item.FieldName = pair.Key;
                                    item.SummaryType = SummaryItemType.Custom;
                                    item.DisplayFormat = "{0:N3}";
                                    item.Tag = column.FieldName;
                                    column.Summary.Add(item);
                                }
                            }
                        }
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                {
                    int calendarAdded = 0;
                    foreach (DateTime date in dates)
                    {
                        int intCalendar = date.Year;
                        if (intCalendar != calendarAdded)
                        {
                            calendarAdded = intCalendar;

                            GridColumn column = AddColumn(
                                "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")),
                                "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                                UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                            if (chkCalculateSums.Checked)
                            {
                                var item = new GridGroupSummaryItem();
                                item.FieldName = column.FieldName;
                                item.ShowInGroupColumnFooter = column;
                                item.SummaryType = SummaryItemType.Sum;
                                item.DisplayFormat = "{0:N3}";
                                gridTradeActionResultsView.GroupSummary.Add(item);

                                column.SummaryItem.SummaryType = SummaryItemType.Sum;
                                column.SummaryItem.DisplayFormat = "{0:N3}";
                            }
                            gridTradeActionResultsView.Columns.Add(column);
                            if (type == "MTM")
                            {
                                var dt = (DateTime)column.Tag;
                                foreach (var pair in _indexesValues[dt])
                                {
                                    if (string.IsNullOrEmpty(pair.Key)) continue;

                                    var item = new GridColumnSummaryItem();
                                    item.FieldName = pair.Key;
                                    item.SummaryType = SummaryItemType.Custom;
                                    item.DisplayFormat = "{0:N3}";
                                    item.Tag = column.FieldName;
                                    column.Summary.Add(item);
                                }
                            }
                        }
                    }
                }

                GridColumn columnCommission = null;
                if (type == "MTM" && chkCalculateCommissions.Checked)
                {
                    columnCommission = AddColumn("Commission", "Commission", gridViewColumnindex++, UnboundColumnType.Decimal, false, "Commission");
                    gridTradeActionResultsView.Columns.Add(columnCommission);
                }


                if (chkCalculateSums.Checked)
                {
                    GridColumn columnTotal = AddColumn(
                        "Total",
                        "Total", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, "Total");
                    gridTradeActionResultsView.Columns.Add(columnTotal);

                    var item = new GridGroupSummaryItem();
                    item.FieldName = columnTotal.FieldName;
                    item.ShowInGroupColumnFooter = columnTotal;
                    item.SummaryType = SummaryItemType.Sum;
                    item.DisplayFormat = "{0:N3}";
                    gridTradeActionResultsView.GroupSummary.Add(item);
                    columnTotal.SummaryItem.SummaryType = SummaryItemType.Sum;
                    columnTotal.SummaryItem.DisplayFormat = "{0:N3}";

                    if (type == "MTM" && columnCommission != null)
                    {
                        var commissionSumItem = new GridGroupSummaryItem();
                        commissionSumItem.FieldName = columnCommission.FieldName;
                        commissionSumItem.ShowInGroupColumnFooter = columnCommission;
                        commissionSumItem.SummaryType = SummaryItemType.Sum;
                        commissionSumItem.DisplayFormat = "{0:N3}";
                        gridTradeActionResultsView.GroupSummary.Add(commissionSumItem);
                        columnCommission.SummaryItem.SummaryType = SummaryItemType.Sum;
                        columnCommission.SummaryItem.DisplayFormat = "{0:N3}";
                    }
                }

                if (type == "MTM")
                {
                    var index = _indexesValues.First(a => a.Value.Count > 0).Value;
                    foreach (var pair in index)
                    {
                        var item = new GridColumnSummaryItem();
                        item.FieldName = pair.Key;
                        item.SummaryType = SummaryItemType.Custom;
                        item.DisplayFormat = "{0:N3}";
                        item.Tag = "Index";
                        lastColumn.Summary.Add(item);
                    }
                }
                gridTradeActionResults.Tag = type;
                gridTradeActionResultsView.Tag = tradeResults;

                gridTradeActionResults.DataSource = _viewTradesGeneralInfo.tradeInformations;

            }

            if (groupedColumns.Count > 0)
            {
                foreach (GridColumn groupedColumn in groupedColumns)
                {
                    if (gridTradeActionResultsView.Columns.ColumnByFieldName(groupedColumn.FieldName) != null)
                        gridTradeActionResultsView.Columns[groupedColumn.FieldName].Group();
                }
            }

            gridTradeActionResults.EndUpdate();

            _formState = FormStateEnum.AfterExecution;
            SetGuiControls();
        }

        private void ProcessCashFlowGroups(long? parentId, CashFlowInformation parentGroupInfo, Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults, List<CashFlowItem> cashFlowItems, List<CashFlowGroupItem> cashFlowItemGroups, List<CashFlowGroup> cashFlowGroups, List<CashFlowModelGroup> cashFlowModelGroups, ref Dictionary<int, CashFlowInformation> cashFlowInfos, ref int index, int level)
        {
            string strLevel = "";
            for (int i = 1; i <= level; i++)
            {
                strLevel = strLevel + "--";
            }
            List<CashFlowModelGroup> modelGroups = cashFlowModelGroups.Where(a => a.ParentGroupId == parentId).OrderBy(a=> a.Order).ToList();
            if (parentGroupInfo != null) parentGroupInfo.ChildrenGroupInfo = new List<CashFlowInformation>();
            foreach (CashFlowModelGroup modelGroup in modelGroups)
            {
                List<CashFlowItem> items = (from objCashFlowItem in cashFlowItems
                                            from objCashFlowItemGroup in cashFlowItemGroups
                                            where objCashFlowItem.Id == objCashFlowItemGroup.ItemId
                                                  && objCashFlowItemGroup.GroupId == modelGroup.GroupId
                                            orderby objCashFlowItemGroup.Order
                                            select objCashFlowItem).ToList();

                CashFlowInformation cashFlowInfo = new CashFlowInformation {Name = strLevel + cashFlowGroups.Single(a => a.Id == modelGroup.GroupId).Name, Type = CashFlowInformationTypeEnum.GroupHeader, Results = cashFlowItemResults.Where(a => items.Select(b => b.Id).Contains(a.Key)).Select(a => a.Value).ToList(), GroupOperator = modelGroup.Operator};
                cashFlowInfos.Add(index++, cashFlowInfo);
                if (parentGroupInfo != null) parentGroupInfo.ChildrenGroupInfo.Add(cashFlowInfo);
                foreach (CashFlowItem item in items)
                {
                    cashFlowInfos.Add(index++, new CashFlowInformation { Id = item.Id, Name = strLevel + item.Name, Type = CashFlowInformationTypeEnum.Item, Results = cashFlowItemResults.Where(a => a.Key == item.Id).Select(a => a.Value).ToList() });
                }
                ProcessCashFlowGroups(modelGroup.GroupId, cashFlowInfo, cashFlowItemResults, cashFlowItems, cashFlowItemGroups, cashFlowGroups, cashFlowModelGroups, ref cashFlowInfos, ref index, level + 1);
                CashFlowInformation cashFlowInfo2 = new CashFlowInformation { Id = modelGroup.GroupId, Name = strLevel + cashFlowGroups.Single(a => a.Id == modelGroup.GroupId).Name, Type = CashFlowInformationTypeEnum.GroupFooter, GroupItems = items, Results = cashFlowItemResults.Where(a => items.Select(b => b.Id).Contains(a.Key)).Select(a => a.Value).ToList(), GroupHeaderInfo = cashFlowInfo, GroupOperator = modelGroup.Operator };
                cashFlowInfos.Add(index++, cashFlowInfo2);
                // if (parentGroupInfo != null) parentGroupInfo.ChildrenGroupInfo.Add(cashFlowInfo2);
            }
        }

        private void SetEnabledForWorkingStatus(bool status)
        {
           
        }

        void GetSelectionBooks(TreeListNode parent, List<HierarchyNodeInfo> selectedNodes)
        {
            if (parent == null) return;
            IEnumerator en = parent.Nodes.GetEnumerator();
            while (en.MoveNext())
            {
                var child = (TreeListNode)en.Current;
                selectedNodes.Add((HierarchyNodeInfo)child.Tag);
                child.Selected = true;
                if (child.HasChildren) GetSelectionBooks(child, selectedNodes);
            }
        }

        private void GetCFModels()
        {
            BeginGetCFModels();
        }
        #endregion

        #region GUI Events

        private void MainControl_Load(object sender, EventArgs e)
        {
            isLoad = true;
            
        }

        private void ViewTradesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           
            try
            {
                gridTradeActionResultsView.SaveLayoutToXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }
        }

        private void cmbPositionMethod_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedPositionMethod = (string) cmbPositionMethod.SelectedItem;
            //if (selectedPositionMethod == "Static")
            //{
            //    chkOptionNullify.Enabled = true;
            //}
            //else
            //{
            //    chkOptionNullify.Enabled = false;
            //    chkOptionNullify.EditValue = false;
            //}
        }

        private void gridTradeActionResultsView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                if (e.Row is TradeInformation)
                {
                    bool isMTMAndcommissionCalculation = chkCalculateCommissions.Checked &&
                                            (gridTradeActionResults.Tag != null &&
                                             gridTradeActionResults.Tag.ToString() == "MTM");

                    var tradeInformation = (TradeInformation)e.Row;

                    decimal commissionPerMonth = 0;
                    decimal totalCommission = 0;

                    var numOfTradeMotnhsInSelectedPeriod = 0;
                    var firstDay = _periodFrom;
                    var lastDay = _periodTo;

                    DateTime tradeDateInSelectedPeriod = tradeInformation.PeriodFrom.Date;
                    while (tradeDateInSelectedPeriod < firstDay)
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddDays(1);

                    while (tradeDateInSelectedPeriod >= firstDay && tradeDateInSelectedPeriod <= tradeInformation.PeriodTo.Date)
                    {
                        numOfTradeMotnhsInSelectedPeriod++;
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddMonths(1);
                    }

                    if (isMTMAndcommissionCalculation)
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.Option)
                        {
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                  tradeInformation.TradeInfo.OptionInfo.Premium *
                                                  tradeInformation.TradeInfo.OptionInfo.QuantityDays);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                        else if (tradeInformation.Trade.Type == TradeTypeEnum.FFA)
                        {
                            //JIRA:FREIG-11
                            //commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                            //                      tradeInformation.TradeInfo.FFAInfo.Price *
                            //                      tradeInformation.TradeInfo.FFAInfo.QuantityDays);
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                 tradeInformation.TradeInfo.FFAInfo.Price *
                                                 tradeInformation.TradeInfo.FFAInfo.TotalDaysOfTrade);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                    }

                    if (e.Column.FieldName == "Type")
                    {
                        if (tradeInformation.Type == "Option")
                            e.Value = tradeInformation.TradeInfo.OptionInfo.Type.ToString("g");
                    }
                    else if (e.Column.FieldName == "Books")
                    {
                        string books = tradeInformation.TradeInfo.TradeInfoBooks.Aggregate("", (current, tradeInfoBook) =>
                                                                                           current + _viewTradesGeneralInfo.books.Single(a => a.Id == tradeInfoBook.BookId).Name + ", ");
                        e.Value = books.Substring(0, books.LastIndexOf(','));
                    }
                    else if (e.Column.FieldName == "EmbeddedValue")
                    {
                        var embeddedValues = (Dictionary<string, decimal>)e.Column.Tag;
                        e.Value = (embeddedValues == null || embeddedValues.Count == 0 || !embeddedValues.ContainsKey(tradeInformation.Identifier))
                                      ? 0
                                      : embeddedValues[tradeInformation.Identifier];
                    }
                    else if (e.Column.FieldName == "Total")
                    {
                        var tradePositions = (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                        if (isMTMAndcommissionCalculation)
                        {
                            if (rdgPositionResultsType.SelectedIndex == 0) // Days
                            {
                                e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value) - totalCommission;
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        a.Value /
                                        (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                          new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum() - totalCommission;
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        (a.Value /
                                         (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                           new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                        tradeInformation.MarketTonnes).Sum() - totalCommission;
                            }
                        }
                        else
                        {
                            if (rdgPositionResultsType.SelectedIndex == 0) // Days
                            {
                                e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value);
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        a.Value /
                                        (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                          new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        (a.Value /
                                         (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                           new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                        tradeInformation.MarketTonnes).Sum();
                            }
                        }
                    }
                    else if (e.Column.FieldName == "Commission" && isMTMAndcommissionCalculation)
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.TC || tradeInformation.Trade.Type == TradeTypeEnum.Cargo
                            || tradeInformation.TradeInfo.TradeBrokerInfos == null || tradeInformation.TradeInfo.TradeBrokerInfos.Count == 0)
                            return;

                        if (tradeInformation.TradeInfo.TradeBrokerInfos.Count > 1)
                        {
                            e.Value = "More than 1 brokers";
                            return;
                        }
                        e.Value = totalCommission;
                    }
                    else
                    {
                        #region POSITION Calculation
                        if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
                        {
                            var tradePositions =
                                (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = tradePositions[tradeInformation.Identifier][date];
                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => a.Value).Single();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = tradePositions[tradeInformation.Identifier][date] /
                                              (((new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)) -
                                                new DateTime(date.Year, date.Month, 1)).Days + 1);

                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => a.Value/(((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) - new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Single();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = (tradePositions[tradeInformation.Identifier][date] /
                                               (((new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)) -
                                                 new DateTime(date.Year, date.Month, 1)).Days + 1)) *
                                              tradeInformation.MarketTonnes;

                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => (a.Value / (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) - new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) * tradeInformation.MarketTonnes).Single();
                                }
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName;
                                        }).Select(
                                                a => a.Value).Sum();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                                    ? "Q1"
                                                                    : intQuarter == 2
                                                                          ? "Q2"
                                                                          : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                                a =>
                                                a.Value /
                                                (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                                  new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                                    ? "Q1"
                                                                    : intQuarter == 2
                                                                          ? "Q2"
                                                                          : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                                a =>
                                                (a.Value /
                                                 (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                                   new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                                tradeInformation.MarketTonnes).Sum();
                                }
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                            a => a.Value).Sum();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName)
                                                  .Select(
                                                      a =>
                                                      a.Value /
                                                      (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(
                                                          -1)) -
                                                        new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName)
                                                  .Select(
                                                      a =>
                                                      (a.Value /
                                                       (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(
                                                           -1)) -
                                                         new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                                      tradeInformation.MarketTonnes).Sum();
                                }
                            }
                        }
                        #endregion

                        else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM" && e.Column.Tag != "Commission")
                        {
                            var tradeMTMs = (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;
                            decimal comm = 0;
                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                var currentDateColumn = (DateTime)e.Column.Tag;

                                var firstDayInCurrMonth = new DateTime(currentDateColumn.Year, currentDateColumn.Month, 1);
                                var lastDayInCurrMonth = firstDayInCurrMonth.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom, tradeInformation.PeriodTo);
                                ITimePeriod monthPeriod = new TimeRange(firstDayInCurrMonth, lastDayInCurrMonth);
                                bool intersects = tradePeriod.IntersectsWith(monthPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    comm = commissionPerMonth;
                                }
                                e.Value = tradeMTMs[tradeInformation.Identifier][(DateTime)e.Column.Tag] - comm;
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                var currentDateColumn = (DateTime)e.Column.Tag;
                                var currentQuarter = new Quarter(currentDateColumn);

                                var firstDayInQuarter = new DateTime(currentQuarter.FirstMonthStart.Year,
                                                                     currentQuarter.FirstMonthStart.Month, 1);
                                var lastDayInQuarter = currentQuarter.LastMonthStart.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom, tradeInformation.PeriodTo);
                                ITimePeriod quarterPeriod = new TimeRange(firstDayInQuarter, lastDayInQuarter);
                                bool intersects = tradePeriod.IntersectsWith(quarterPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    var tradeRange = new TimeRange(tradePeriod);
                                    var calendarRange = new TimeRange(quarterPeriod);
                                    var selectedDatesRange = new TimeRange(_periodFrom, _periodTo);

                                    var s = selectedDatesRange.GetIntersection(calendarRange).GetIntersection(tradeRange);
                                    DateDiff a = new DateDiff(s.Start, s.End);
                                    var numOfTradeMonthsInCurrQuarter = a.ElapsedDays > 0 ? a.Months + 1 : a.Months;
                                    comm = commissionPerMonth * numOfTradeMonthsInCurrQuarter;
                                }

                                e.Value = tradeMTMs[tradeInformation.Identifier].Where(
                                   a =>
                                   {
                                       int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                       string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                       return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName;
                                   }).Select(
                                           a => a.Value).Sum() - comm;
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                var currentDate = (DateTime)e.Column.Tag;
                                var currentYear = new Year(currentDate);

                                var firstDayInYear = new DateTime(currentYear.FirstMonthStart.Year,
                                                                     currentYear.FirstMonthStart.Month, 1);
                                var lastDayInYear = currentYear.LastMonthStart.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom, tradeInformation.PeriodTo);
                                ITimePeriod calendarPeriod = new TimeRange(firstDayInYear, lastDayInYear);
                                bool intersects = tradePeriod.IntersectsWith(calendarPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    var tradeRange = new TimeRange(tradePeriod);
                                    var calendarRange = new TimeRange(calendarPeriod);
                                    var selectedDatesRange = new TimeRange(_periodFrom, _periodTo);

                                    var s = selectedDatesRange.GetIntersection(calendarRange).GetIntersection(tradeRange);
                                    DateDiff a = new DateDiff(s.Start, s.End);
                                    var numOfTradeMonthsInCurrYear = a.ElapsedDays > 0 ? a.Months + 1 : a.Months;
                                    comm = commissionPerMonth * numOfTradeMonthsInCurrYear;
                                }

                                e.Value =
                                    tradeMTMs[tradeInformation.Identifier].Where(
                                        a =>
                                        "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName)
                                        .Select(a => a.Value).Sum() - comm;
                            }
                        }
                    }
                }
                else
                {
                    var cashFlowInfo = (CashFlowInformation)e.Row;
                    if (e.Column.Tag.GetType() == typeof(DateTime))
                    {
                        var date = (DateTime)e.Column.Tag;

                        if (cashFlowInfo.Type == CashFlowInformationTypeEnum.Item)
                        {
                            e.Value = cashFlowInfo.Results.First()[date];
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.GroupHeader)
                        {
                            decimal value = 0;
                            //ProcessCashFlowInfo(cashFlowInfo, ref value, date, cashFlowInfo.Type);
                            e.Value = value;
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.GroupFooter)
                        {
                            decimal value = 0;
                           // ProcessCashFlowInfo(cashFlowInfo, ref value, date, cashFlowInfo.Type);
                            e.Value = value;
                        }
                    }
                    else if (e.Column.FieldName == "Totals" && cashFlowInfo.Type == CashFlowInformationTypeEnum.Item)
                    {
                        if (cashFlowInfo.Name == "Break Even All" || cashFlowInfo.Name == "Break Even Spot" || cashFlowInfo.Name == "Break Even Balloon"
                            || cashFlowInfo.Name == "Break Even Equity" || cashFlowInfo.Name == "Valuation" || cashFlowInfo.Name == "Discount Factor"
                            || cashFlowInfo.Name == "Present Value" || cashFlowInfo.Name == "Break Even Scrap")
                        {
                            e.Value = cashFlowInfo.Results.Aggregate<Dictionary<DateTime, decimal>, decimal>(0,
                                                                                                             (current,
                                                                                                              dictionary)
                                                                                                             =>
                                                                                                             current +
                                                                                                             dictionary.
                                                                                                                 Values.
                                                                                                                 Sum());
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.Item && cashFlowInfo.Name == "IRR")
                        {
                            List<double> values = new List<double>();
                            foreach (Dictionary<DateTime, decimal> dictionary in cashFlowInfo.Results)
                            {
                                foreach (KeyValuePair<DateTime, decimal> keyValuePair in dictionary)
                                {
                                    values.Add(Convert.ToDouble(keyValuePair.Value));
                                }

                            }

                            if (values[0] != 0 && values.Any(a => a != 0))
                            {
                                var calculator = new NewtonRaphsonIRRCalculator(values.ToArray());
                                try
                                {
                                    e.Value = calculator.Execute();
                                }
                                catch (Exception exc)
                                {
                                    e.Value = "There was an error while computing IRR item. " + exc.Message;
                                }
                            }
                            else
                            {
                                e.Value = 0;
                            }
                        }
                    }
                }
            }
        }

        private void btnCalculateClick(object sender, EventArgs e)
        {
            if (lookupCFModel.EditValue == null) return;
            BeginGenerateCashFlow();
        }

        private void btnExportResults_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            //string strDateFrom = dtpFilterPeriodFrom.DateTime.Year + "_" + dtpFilterPeriodFrom.DateTime.Month + "_" + dtpFilterPeriodFrom.DateTime.Day;
            //string strDateTo = dtpFilterPeriodTo.DateTime.Year + "_" + dtpFilterPeriodTo.DateTime.Month + "_" + dtpFilterPeriodTo.DateTime.Day;

            string strDateFrom = "2014_12_01";
            string strDateTo = "2014_12_31";

            gridTradeActionResultsView.OptionsPrint.PrintHorzLines = false;
            gridTradeActionResultsView.OptionsPrint.PrintVertLines = false;
            gridTradeActionResultsView.OptionsPrint.AutoWidth = false;

            string fileName = "File";
            string sheetName = "Sheet";

            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                fileName = "FreightMetrics_PositionReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "Position Report";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                fileName = "FreightMetrics_MTMReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "MTM Report";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "CASHFLOW")
            {
                fileName = "FreightMetrics_CashFlowReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "Cash Flow Report";
            }

            try
            {
                if (gridTradeActionResultsView.DataRowCount > 60000)
                {
                    gridTradeActionResultsView.ExportToXlsx(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xlsx", new XlsxExportOptions {ExportHyperlinks = false, ExportMode = XlsxExportMode.SingleFile, SheetName = sheetName, ShowGridLines = false, TextExportMode = TextExportMode.Value});
                }
                else
                {
                    gridTradeActionResultsView.ExportToXls(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xls", new XlsExportOptions {ExportHyperlinks = false, ExportMode = XlsExportMode.SingleFile, SheetName = sheetName, ShowGridLines = false, TextExportMode = TextExportMode.Value});
                }
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this, "File with name: '" + fileName + "' exported to Desktop successfully."
                                    ,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this, string.Format(Strings.Failed_to_write_file_to_Desktop__Details___0_, exc.Message)
                                    ,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGraphResults_Click(object sender, EventArgs e)
        {
            //string strDateFrom = dtpFilterPeriodFrom.DateTime.Year + "-" + dtpFilterPeriodFrom.DateTime.Month + "-" + dtpFilterPeriodFrom.DateTime.Day;
            //string strDateTo = dtpFilterPeriodTo.DateTime.Year + "-" + dtpFilterPeriodTo.DateTime.Month + "-" + dtpFilterPeriodTo.DateTime.Day;

            string strDateFrom = "2014_12_01";
            string strDateTo = "2014_12_31";

            var chart = new ChartControl();
            var seriesTotal = new Series("Net", ViewType.Bar);

            string title = "Title";
            string columnName = "Column";

            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                title = "Position Chart From: " + strDateFrom + " To: " + strDateTo;
                columnName = "PositionData";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                title = "MTM Chart From: " + strDateFrom + " To: " + strDateTo;
                columnName = "MTMData";
            }

            foreach (GridColumn column in gridTradeActionResultsView.Columns)
            {
                if (column.Tag != null && column.Tag.ToString() == columnName)
                {
                    seriesTotal.Points.Add(new SeriesPoint(column.FieldName, column.SummaryItem.SummaryValue));
                }
            }

            chart.Series.AddRange(new[] {seriesTotal});

            seriesTotal.Label.Visible = true;
            seriesTotal.Label.ResolveOverlappingMode = ResolveOverlappingMode.Default;

            seriesTotal.PointOptions.PointView = PointView.ArgumentAndValues;

            var myView = (BarSeriesView) seriesTotal.View;

            var chartTitle = new ChartTitle();
            chartTitle.Text = title;
            chart.Titles.Add(chartTitle);
            chart.Legend.Visible = true;

            var memoryStream = new MemoryStream();
            chart.SaveToStream(memoryStream);

            if (ViewChartEvent != null) ViewChartEvent(memoryStream, title);
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void RdgResultsAggregationTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                btnGeneratePosition.PerformClick();
            }
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                CreateResultGridColumns(_tradeMTMResults, _tradeEmbeddedValues, "MTM");
            }
        }

        private void RdgPositionResultsTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                btnGeneratePosition.PerformClick();
            }
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                CreateResultGridColumns(_tradeMTMResults, _tradeEmbeddedValues, "MTM");
            }
        }

        private void GridTradeActionResultsViewCustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            if (((GridSummaryItem)e.Item).Tag == null || (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() != "MTM"))
                return;

            if ((string)((GridSummaryItem)e.Item).Tag == "Index")
            {
                e.TotalValue = ((GridSummaryItem)e.Item).FieldName;
            }
            else
            {
                string indexName = ((GridSummaryItem)e.Item).FieldName;
                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    DateTime dt = Convert.ToDateTime(((GridSummaryItem)e.Item).Tag);
                    e.TotalValue = _indexesValues[dt][indexName];
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1)//Quarter
                {
                    string quarter = ((GridSummaryItem)e.Item).Tag.ToString();
                    e.TotalValue = _indexesValues.Where(
                        a =>
                        {
                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                            string strQuarter = intQuarter == 1
                                                    ? "Q1"
                                                    : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;
                        }).Any(a => a.Value.Count > 0)
                                       ? _indexesValues.Where(
                                           a =>
                                           {
                                               int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                               string strQuarter = intQuarter == 1
                                                                       ? "Q1"
                                                                       : intQuarter == 2
                                                                             ? "Q2"
                                                                             : intQuarter == 3 ? "Q3" : "Q4";
                                               return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                      quarter;
                                           }).Where(a => a.Value.Count > 0 && a.Value.ContainsKey(indexName)).Select(a => a.Value[indexName]).Average()
                                       : 0;
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2)//Calendar
                {
                    string calendar = ((GridSummaryItem)e.Item).Tag.ToString();

                    if (_indexesValues.Any(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar && a.Value.Count > 0 && a.Value.ContainsKey(indexName)))
                    {
                        e.TotalValue =
                            _indexesValues.Where(
                                a =>
                                "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar &&
                                a.Value.Count > 0 && a.Value.ContainsKey(indexName)).Select(a => a.Value[indexName]).
                                Average();
                    }
                    else
                        e.TotalValue = 0;
                }
            }
        }
  
        private void CmbMarketSensitivityTypeSelectedValueChanged(object sender, EventArgs e)
        {
            txtMarketSensitivityValue.Properties.ReadOnly = (cmbMarketSensitivityType.SelectedItem != null &&
                                                             (cmbMarketSensitivityType.SelectedItem.ToString() ==
                                                              "Stress" ||
                                                              cmbMarketSensitivityType.SelectedItem.ToString() == "None"));
        }

        #endregion

        #region Proxy Calls


        private void BeginGetCFModels()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm2 = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetCFModels(EndGetCFModels, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetCFModels(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetCFModels;
                Invoke(action, ar);
                return;
            }

            int? result;

            List<CashFlowModel> models;
            try
            {
                result = SessionRegistry.Client.EndGetCFModels(out models, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {

                lookupCFModel.Tag = models;
                lookupCFModel.Properties.DataSource = models;

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
            
            }
        }

        private void BeginGenerateCashFlow()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm1 = new WaitDialogForm(this);
         
            try
            {
                SessionRegistry.Client.BeginGenerateCashFlow(_viewTradesGeneralInfo.positionDate,
                                                             dtpFilterCurveDate.DateTime.Date,
                                                             _viewTradesGeneralInfo.periodFrom,
                                                             _viewTradesGeneralInfo.periodTo,
                                                             (string)cmbPositionMethod.SelectedItem,
                                                             (string)cmbMarketSensitivityType.SelectedItem,
                                                             txtMarketSensitivityValue.Value,
                                                             chkUseSpotValues.Checked, chkOptionPremium.Checked,
                                                             _viewTradesGeneralInfo.tradeIdentifiers, Convert.ToInt64(lookupCFModel.EditValue),
                                                             chkCFBreakEven.Checked, chkCFValuation.Checked, chkCFIRR.Checked,
                                                             chkCFBreakEvenBalloon.Checked, chkCFBreakEvenEquity.Checked, chkCFBreakEvenScrap.Checked, txtCFValuationDiscountRate.Value,
                                                             EndGenerateCashFlow, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGenerateCashFlow(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGenerateCashFlow;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults;
            List<CashFlowGroup> cashFlowGroups;
            List<CashFlowModelGroup> cashFlowModelGroups;
            List<CashFlowItem> cashFlowItems;
            List<CashFlowGroupItem> cashFlowGroupItems;
            Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsAll;
            Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsSpot;
            Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsBalloon;
            Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsEquity;
            Dictionary<string, Dictionary<DateTime, decimal>> breakEvenResultsScrappings;
            Dictionary<string, Dictionary<DateTime, decimal>> valuationResults;
            Dictionary<string, Dictionary<DateTime, decimal>> valuationDiscountFactor;
            Dictionary<string, Dictionary<DateTime, decimal>> valuationPresentValue;
            Dictionary<string, Dictionary<DateTime, decimal>> irrResults;

            try
            {
                result = SessionRegistry.Client.EndGenerateCashFlow(out cashFlowItemResults, out cashFlowGroups, out cashFlowModelGroups, out cashFlowItems, out cashFlowGroupItems, out breakEvenResultsAll, out breakEvenResultsSpot, out breakEvenResultsBalloon, out breakEvenResultsEquity, out breakEvenResultsScrappings, out valuationResults, out valuationDiscountFactor, out valuationPresentValue, out irrResults, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //_formState = FormStateEnum.AfterSearch;
            //SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                gridTradeActionResults.BeginUpdate();
                gridTradeActionResults.DataSource = null;
                gridTradeActionResultsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
                gridTradeActionResultsView.Columns.Clear();
                gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;
                int gridViewColumnindex = 0;

                gridTradeActionResultsView.Columns.Add(AddColumn("Name", Strings.CF_Item_Name, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));

                List<DateTime> dates = cashFlowItemResults.First().Value.Select(a => a.Key).ToList();

                foreach (DateTime date in dates)
                {
                    GridColumn column = AddColumn(
                        date.ToString("MMM-yyyy", new CultureInfo("en-GB")),
                        date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                    gridTradeActionResultsView.Columns.Add(column);

                    if (chkCalculateSums.Checked)
                    {
                        var item = new GridGroupSummaryItem();
                        item.FieldName = column.FieldName;
                        item.ShowInGroupColumnFooter = column;
                        item.SummaryType = SummaryItemType.Sum;
                        item.DisplayFormat = "{0:N3}";
                        gridTradeActionResultsView.GroupSummary.Add(item);

                        column.SummaryItem.SummaryType = SummaryItemType.Sum;
                        column.SummaryItem.DisplayFormat = "Total={0:N3}";
                    }
                }
                if (chkCalculateSums.Checked)
                    gridTradeActionResultsView.Columns.Add(AddColumn("Totals", "Totals", gridViewColumnindex++, UnboundColumnType.Decimal, false, "Totals"));

                gridTradeActionResults.EndUpdate();

                Dictionary<int, CashFlowInformation> cashFlowInfos = new Dictionary<int, CashFlowInformation>();
                int index = 0;
                int level = 1;
                ProcessCashFlowGroups(null, null, cashFlowItemResults, cashFlowItems, cashFlowGroupItems, cashFlowGroups, cashFlowModelGroups, ref cashFlowInfos, ref index, level);



                if (chkCFBreakEven.Checked)
                {
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Blank });
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Break Even All", Results = breakEvenResultsAll.Where(a => a.Key == "BreakEvenAll").Select(a => a.Value).ToList() });
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Break Even Spot", Results = breakEvenResultsSpot.Where(a => a.Key == "BreakEvenSpot").Select(a => a.Value).ToList() });

                    if (chkCFBreakEvenBalloon.Checked)
                    {
                        cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Break Even Balloon", Results = breakEvenResultsBalloon.Where(a => a.Key == "BreakEvenBalloon").Select(a => a.Value).ToList() });
                    }
                    if (chkCFBreakEvenEquity.Checked)
                    {
                        cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Break Even Equity", Results = breakEvenResultsEquity.Where(a => a.Key == "BreakEvenEquity").Select(a => a.Value).ToList() });
                    }
                    if (chkCFBreakEvenScrap.Checked)
                    {
                        cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Break Even Scrap", Results = breakEvenResultsScrappings.Where(a => a.Key == "BreakEvenScrappings").Select(a => a.Value).ToList() });
                    }
                }

                if (chkCFValuation.Checked)
                {
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Blank });
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Valuation", Results = valuationResults.Where(a => a.Key == "ValuationResults").Select(a => a.Value).ToList() });
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Discount Factor", Results = valuationDiscountFactor.Where(a => a.Key == "ValuationDiscountFactor").Select(a => a.Value).ToList() });
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "Present Value", Results = valuationPresentValue.Where(a => a.Key == "ValuationPresentValue").Select(a => a.Value).ToList() });
                }

                if (chkCFIRR.Checked)
                {
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Blank });
                    cashFlowInfos.Add(index++, new CashFlowInformation { Type = CashFlowInformationTypeEnum.Item, Name = "IRR", Results = irrResults.Where(a => a.Key == "IRRResults").Select(a => a.Value).ToList() });
                }

                gridTradeActionResults.DataSource = cashFlowInfos.OrderBy(a => a.Key).Select(a => a.Value).ToList();
                gridTradeActionResults.Tag = "CASHFLOW";
                //_formState = FormStateEnum.AfterResults;
                //SetGuiControls();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
            }
        }


      
        #endregion

        #region Events

        public event Action<MemoryStream, string> ViewChartEvent;
        public event Action<long, long, string, TradeTypeEnum> OnViewTrade;
        public event Action<long, long, string, TradeTypeEnum> OnEditTrade;
        public event Action<Dictionary<DateTime, List<double>>, List<double>, List<Dictionary<DateTime, decimal>>> MonteCarloSimulationEvent;
        public event Action<List<TradeInformation>, Dictionary<string, Dictionary<DateTime, decimal>>, Dictionary<string,Dictionary<string, Dictionary<DateTime, decimal>>>, DateTime, DateTime, DateTime> RatiosReportEvent;
        public event Action<object, bool> OnDataSaved;
        public event Action OnOpenActivateTradesForm;
        public event Action OnTradeDeactivated;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            //if (treeNavigation.FocusedNode != null)
            //{
            //    var filters = new List<HierarchyNodeInfo>();
            //    TreeListNode node = treeNavigation.FocusedNode;
            //    var entity = (HierarchyNodeInfo) node.Tag;
            //    filters.Add(entity);

            //    TreeListNode parentNode = node.ParentNode;
            //    while (parentNode != null)
            //    {
            //        filters.Add((HierarchyNodeInfo) parentNode.Tag);
            //        parentNode = parentNode.ParentNode;
            //    }

            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesByFilters(filters);
            //}
            //else
            //{
            //    if (treeBookNavigation.FocusedNode == null)
            //        return;
            //    var filters = new List<HierarchyNodeInfo>();
            //    TreeListNode node = treeBookNavigation.FocusedNode;
            //    var entity = (HierarchyNodeInfo) node.Tag;
            //    filters.Add(entity);

            //    TreeListNode parentNode = node.ParentNode;
            //    while (parentNode != null)
            //    {
            //        filters.Add((HierarchyNodeInfo) parentNode.Tag);
            //        parentNode = parentNode.ParentNode;
            //    }

            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesByFilters(filters);
            //}
        }

        #endregion

       
    }

}