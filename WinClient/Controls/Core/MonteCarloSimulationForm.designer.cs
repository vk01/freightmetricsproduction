﻿namespace Exis.WinClient.Controls.Core
{
    partial class MonteCarloSimulationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonteCarloSimulationForm));
            DevExpress.XtraCharts.XYDiagram3D xyDiagram3D2 = new DevExpress.XtraCharts.XYDiagram3D();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Area3DSeriesLabel area3DSeriesLabel4 = new DevExpress.XtraCharts.Area3DSeriesLabel();
            DevExpress.XtraCharts.Area3DSeriesView area3DSeriesView4 = new DevExpress.XtraCharts.Area3DSeriesView();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Area3DSeriesLabel area3DSeriesLabel5 = new DevExpress.XtraCharts.Area3DSeriesLabel();
            DevExpress.XtraCharts.Area3DSeriesView area3DSeriesView5 = new DevExpress.XtraCharts.Area3DSeriesView();
            DevExpress.XtraCharts.Area3DSeriesLabel area3DSeriesLabel6 = new DevExpress.XtraCharts.Area3DSeriesLabel();
            DevExpress.XtraCharts.Area3DSeriesView area3DSeriesView6 = new DevExpress.XtraCharts.Area3DSeriesView();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.lblTrades = new DevExpress.XtraEditors.LabelControl();
            this.lblUseSpot = new DevExpress.XtraEditors.LabelControl();
            this.lblIsOptionPremium = new DevExpress.XtraEditors.LabelControl();
            this.lblMakSensValue = new DevExpress.XtraEditors.LabelControl();
            this.lblMarkSensType = new DevExpress.XtraEditors.LabelControl();
            this.lblMTMType = new DevExpress.XtraEditors.LabelControl();
            this.lblPosMethod = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodTo = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodFrom = new DevExpress.XtraEditors.LabelControl();
            this.lblNoOfRuns = new DevExpress.XtraEditors.LabelControl();
            this.lblEndTime = new DevExpress.XtraEditors.LabelControl();
            this.lblStartTime = new DevExpress.XtraEditors.LabelControl();
            this.lblUser = new DevExpress.XtraEditors.LabelControl();
            this.lblCurveDate = new DevExpress.XtraEditors.LabelControl();
            this.cmbPercentilePeriods = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbPercentagePeriods = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbPeriodType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtProbability = new DevExpress.XtraEditors.SpinEdit();
            this.btnComputePercentile = new DevExpress.XtraEditors.SimpleButton();
            this.imageList16 = new DevExpress.Utils.ImageCollection(this.components);
            this.lblPercentile = new DevExpress.XtraEditors.LabelControl();
            this.txtTo = new DevExpress.XtraEditors.SpinEdit();
            this.txtFrom = new DevExpress.XtraEditors.SpinEdit();
            this.btnComputeProbability = new DevExpress.XtraEditors.SimpleButton();
            this.lblDescr = new DevExpress.XtraEditors.LabelControl();
            this.lblProbability = new DevExpress.XtraEditors.LabelControl();
            this.chart = new DevExpress.XtraCharts.ChartControl();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgProbability = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutProbability = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBtnComputeProbability = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutValueFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutValueTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemPercentagePeriods = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgPercentile = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemProbability = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemPercentilePeriods = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblSimulationName = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupGraph = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.layCtlItemPeriodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnExport = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPercentilePeriods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPercentagePeriods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProbability.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram3D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgProbability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProbability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnComputeProbability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValueFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValueTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemPercentagePeriods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgPercentile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemProbability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemPercentilePeriods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSimulationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "export24x24.ico");
            this.imageList24.Images.SetKeyName(1, "print24x24.ico");
            this.imageList24.Images.SetKeyName(2, "exit.ico");
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.lblTrades);
            this.layoutControl.Controls.Add(this.lblUseSpot);
            this.layoutControl.Controls.Add(this.lblIsOptionPremium);
            this.layoutControl.Controls.Add(this.lblMakSensValue);
            this.layoutControl.Controls.Add(this.lblMarkSensType);
            this.layoutControl.Controls.Add(this.lblMTMType);
            this.layoutControl.Controls.Add(this.lblPosMethod);
            this.layoutControl.Controls.Add(this.lblPeriodTo);
            this.layoutControl.Controls.Add(this.lblPeriodFrom);
            this.layoutControl.Controls.Add(this.lblNoOfRuns);
            this.layoutControl.Controls.Add(this.lblEndTime);
            this.layoutControl.Controls.Add(this.lblStartTime);
            this.layoutControl.Controls.Add(this.lblUser);
            this.layoutControl.Controls.Add(this.lblCurveDate);
            this.layoutControl.Controls.Add(this.cmbPercentilePeriods);
            this.layoutControl.Controls.Add(this.cmbPercentagePeriods);
            this.layoutControl.Controls.Add(this.cmbPeriodType);
            this.layoutControl.Controls.Add(this.txtProbability);
            this.layoutControl.Controls.Add(this.btnComputePercentile);
            this.layoutControl.Controls.Add(this.lblPercentile);
            this.layoutControl.Controls.Add(this.txtTo);
            this.layoutControl.Controls.Add(this.txtFrom);
            this.layoutControl.Controls.Add(this.btnComputeProbability);
            this.layoutControl.Controls.Add(this.lblDescr);
            this.layoutControl.Controls.Add(this.lblProbability);
            this.layoutControl.Controls.Add(this.chart);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1769, 343, 250, 350);
            this.layoutControl.Root = this.layoutRoot;
            this.layoutControl.Size = new System.Drawing.Size(1111, 644);
            this.layoutControl.TabIndex = 4;
            this.layoutControl.Text = "layoutControl1";
            // 
            // lblTrades
            // 
            this.lblTrades.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTrades.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTrades.Location = new System.Drawing.Point(108, 132);
            this.lblTrades.Name = "lblTrades";
            this.lblTrades.Size = new System.Drawing.Size(980, 21);
            this.lblTrades.StyleController = this.layoutControl;
            this.lblTrades.TabIndex = 50;
            // 
            // lblUseSpot
            // 
            this.lblUseSpot.Location = new System.Drawing.Point(913, 78);
            this.lblUseSpot.Name = "lblUseSpot";
            this.lblUseSpot.Size = new System.Drawing.Size(102, 13);
            this.lblUseSpot.StyleController = this.layoutControl;
            this.lblUseSpot.TabIndex = 49;
            // 
            // lblIsOptionPremium
            // 
            this.lblIsOptionPremium.Location = new System.Drawing.Point(913, 61);
            this.lblIsOptionPremium.Name = "lblIsOptionPremium";
            this.lblIsOptionPremium.Size = new System.Drawing.Size(102, 13);
            this.lblIsOptionPremium.StyleController = this.layoutControl;
            this.lblIsOptionPremium.TabIndex = 48;
            // 
            // lblMakSensValue
            // 
            this.lblMakSensValue.Location = new System.Drawing.Point(674, 61);
            this.lblMakSensValue.Name = "lblMakSensValue";
            this.lblMakSensValue.Size = new System.Drawing.Size(141, 13);
            this.lblMakSensValue.StyleController = this.layoutControl;
            this.lblMakSensValue.TabIndex = 47;
            // 
            // lblMarkSensType
            // 
            this.lblMarkSensType.Location = new System.Drawing.Point(674, 44);
            this.lblMarkSensType.Name = "lblMarkSensType";
            this.lblMarkSensType.Size = new System.Drawing.Size(141, 13);
            this.lblMarkSensType.StyleController = this.layoutControl;
            this.lblMarkSensType.TabIndex = 46;
            // 
            // lblMTMType
            // 
            this.lblMTMType.Location = new System.Drawing.Point(913, 44);
            this.lblMTMType.Name = "lblMTMType";
            this.lblMTMType.Size = new System.Drawing.Size(102, 13);
            this.lblMTMType.StyleController = this.layoutControl;
            this.lblMTMType.TabIndex = 45;
            // 
            // lblPosMethod
            // 
            this.lblPosMethod.Location = new System.Drawing.Point(674, 78);
            this.lblPosMethod.Name = "lblPosMethod";
            this.lblPosMethod.Size = new System.Drawing.Size(141, 13);
            this.lblPosMethod.StyleController = this.layoutControl;
            this.lblPosMethod.TabIndex = 44;
            // 
            // lblPeriodTo
            // 
            this.lblPeriodTo.Location = new System.Drawing.Point(364, 78);
            this.lblPeriodTo.Name = "lblPeriodTo";
            this.lblPeriodTo.Size = new System.Drawing.Size(151, 13);
            this.lblPeriodTo.StyleController = this.layoutControl;
            this.lblPeriodTo.TabIndex = 43;
            // 
            // lblPeriodFrom
            // 
            this.lblPeriodFrom.Location = new System.Drawing.Point(364, 61);
            this.lblPeriodFrom.Name = "lblPeriodFrom";
            this.lblPeriodFrom.Size = new System.Drawing.Size(151, 13);
            this.lblPeriodFrom.StyleController = this.layoutControl;
            this.lblPeriodFrom.TabIndex = 42;
            // 
            // lblNoOfRuns
            // 
            this.lblNoOfRuns.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblNoOfRuns.Location = new System.Drawing.Point(150, 78);
            this.lblNoOfRuns.Name = "lblNoOfRuns";
            this.lblNoOfRuns.Size = new System.Drawing.Size(103, 13);
            this.lblNoOfRuns.StyleController = this.layoutControl;
            this.lblNoOfRuns.TabIndex = 41;
            // 
            // lblEndTime
            // 
            this.lblEndTime.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblEndTime.Location = new System.Drawing.Point(150, 61);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(103, 13);
            this.lblEndTime.StyleController = this.layoutControl;
            this.lblEndTime.TabIndex = 40;
            // 
            // lblStartTime
            // 
            this.lblStartTime.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblStartTime.Location = new System.Drawing.Point(150, 44);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(103, 13);
            this.lblStartTime.StyleController = this.layoutControl;
            this.lblStartTime.TabIndex = 39;
            // 
            // lblUser
            // 
            this.lblUser.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblUser.Location = new System.Drawing.Point(150, 95);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(103, 13);
            this.lblUser.StyleController = this.layoutControl;
            this.lblUser.TabIndex = 38;
            // 
            // lblCurveDate
            // 
            this.lblCurveDate.Location = new System.Drawing.Point(364, 44);
            this.lblCurveDate.Name = "lblCurveDate";
            this.lblCurveDate.Size = new System.Drawing.Size(151, 13);
            this.lblCurveDate.StyleController = this.layoutControl;
            this.lblCurveDate.TabIndex = 37;
            // 
            // cmbPercentilePeriods
            // 
            this.cmbPercentilePeriods.Location = new System.Drawing.Point(108, 608);
            this.cmbPercentilePeriods.Name = "cmbPercentilePeriods";
            this.cmbPercentilePeriods.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cmbPercentilePeriods.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPercentilePeriods.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.cmbPercentilePeriods.Size = new System.Drawing.Size(146, 20);
            this.cmbPercentilePeriods.StyleController = this.layoutControl;
            this.cmbPercentilePeriods.TabIndex = 21;
            // 
            // cmbPercentagePeriods
            // 
            this.cmbPercentagePeriods.Location = new System.Drawing.Point(108, 539);
            this.cmbPercentagePeriods.Name = "cmbPercentagePeriods";
            this.cmbPercentagePeriods.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cmbPercentagePeriods.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPercentagePeriods.Properties.NullText = global::Exis.WinClient.Strings.Eve;
            this.cmbPercentagePeriods.Size = new System.Drawing.Size(146, 20);
            this.cmbPercentagePeriods.StyleController = this.layoutControl;
            this.cmbPercentagePeriods.TabIndex = 20;
            // 
            // cmbPeriodType
            // 
            this.cmbPeriodType.Location = new System.Drawing.Point(108, 472);
            this.cmbPeriodType.Name = "cmbPeriodType";
            this.cmbPeriodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriodType.Size = new System.Drawing.Size(172, 20);
            this.cmbPeriodType.StyleController = this.layoutControl;
            this.cmbPeriodType.TabIndex = 19;
            this.cmbPeriodType.SelectedIndexChanged += new System.EventHandler(this.CmbPeriodTypeSelectedIndexChanged);
            // 
            // txtProbability
            // 
            this.txtProbability.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtProbability.Location = new System.Drawing.Point(352, 608);
            this.txtProbability.Name = "txtProbability";
            this.txtProbability.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtProbability.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtProbability.Properties.Mask.EditMask = "P0";
            this.txtProbability.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtProbability.Properties.MaxLength = 3;
            this.txtProbability.Size = new System.Drawing.Size(214, 20);
            this.txtProbability.StyleController = this.layoutControl;
            this.txtProbability.TabIndex = 18;
            // 
            // btnComputePercentile
            // 
            this.btnComputePercentile.ImageIndex = 0;
            this.btnComputePercentile.ImageList = this.imageList16;
            this.btnComputePercentile.Location = new System.Drawing.Point(918, 608);
            this.btnComputePercentile.Name = "btnComputePercentile";
            this.btnComputePercentile.Size = new System.Drawing.Size(179, 22);
            this.btnComputePercentile.StyleController = this.layoutControl;
            this.btnComputePercentile.TabIndex = 16;
            this.btnComputePercentile.Text = "Compute";
            this.btnComputePercentile.Click += new System.EventHandler(this.BtnComputePercentileClick);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.Images.SetKeyName(0, "calculator.ico");
            // 
            // lblPercentile
            // 
            this.lblPercentile.Location = new System.Drawing.Point(635, 608);
            this.lblPercentile.Name = "lblPercentile";
            this.lblPercentile.Size = new System.Drawing.Size(279, 22);
            this.lblPercentile.StyleController = this.layoutControl;
            this.lblPercentile.TabIndex = 14;
            // 
            // txtTo
            // 
            this.txtTo.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTo.Location = new System.Drawing.Point(661, 539);
            this.txtTo.Name = "txtTo";
            this.txtTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtTo.Size = new System.Drawing.Size(104, 20);
            this.txtTo.StyleController = this.layoutControl;
            this.txtTo.TabIndex = 17;
            // 
            // txtFrom
            // 
            this.txtFrom.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFrom.Location = new System.Drawing.Point(525, 539);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtFrom.Size = new System.Drawing.Size(111, 20);
            this.txtFrom.StyleController = this.layoutControl;
            this.txtFrom.TabIndex = 16;
            // 
            // btnComputeProbability
            // 
            this.btnComputeProbability.ImageIndex = 0;
            this.btnComputeProbability.ImageList = this.imageList16;
            this.btnComputeProbability.Location = new System.Drawing.Point(925, 539);
            this.btnComputeProbability.Name = "btnComputeProbability";
            this.btnComputeProbability.Size = new System.Drawing.Size(172, 22);
            this.btnComputeProbability.StyleController = this.layoutControl;
            this.btnComputeProbability.TabIndex = 15;
            this.btnComputeProbability.Text = "Compute";
            this.btnComputeProbability.Click += new System.EventHandler(this.BtnComputeProbabilityClick);
            // 
            // lblDescr
            // 
            this.lblDescr.Location = new System.Drawing.Point(258, 539);
            this.lblDescr.Name = "lblDescr";
            this.lblDescr.Size = new System.Drawing.Size(230, 22);
            this.lblDescr.StyleController = this.layoutControl;
            this.lblDescr.TabIndex = 14;
            this.lblDescr.Text = "% Probality of value (CF/P&&L) in:";
            // 
            // lblProbability
            // 
            this.lblProbability.Location = new System.Drawing.Point(838, 539);
            this.lblProbability.Name = "lblProbability";
            this.lblProbability.Size = new System.Drawing.Size(83, 22);
            this.lblProbability.StyleController = this.layoutControl;
            this.lblProbability.TabIndex = 13;
            // 
            // chart
            // 
            xyDiagram3D2.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram3D2.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram3D2.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram3D2.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram3D2.HorizontalScrollPercent = -5.555555555555526D;
            xyDiagram3D2.RotationMatrixSerializable = "0.872794341907058;-0.15703181986568;0.462137473361476;0;-0.0235198139376687;0.932" +
    "200845210289;0.361176414708637;0;-0.487521132965123;-0.326102118575034;0.8099262" +
    "64034923;0;0;0;0;1";
            xyDiagram3D2.VerticalScrollPercent = 9.59725792630677D;
            xyDiagram3D2.ZoomPercent = 150;
            this.chart.Diagram = xyDiagram3D2;
            this.chart.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Right;
            this.chart.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Center;
            this.chart.Location = new System.Drawing.Point(14, 209);
            this.chart.Name = "chart";
            area3DSeriesLabel4.LineVisible = true;
            area3DSeriesLabel4.Visible = false;
            series3.Label = area3DSeriesLabel4;
            series3.Name = "Series 1";
            series3.View = area3DSeriesView4;
            area3DSeriesLabel5.LineVisible = true;
            area3DSeriesLabel5.Visible = false;
            series4.Label = area3DSeriesLabel5;
            series4.Name = "Series 2";
            series4.View = area3DSeriesView5;
            this.chart.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series3,
        series4};
            area3DSeriesLabel6.LineVisible = true;
            area3DSeriesLabel6.Visible = true;
            this.chart.SeriesTemplate.Label = area3DSeriesLabel6;
            area3DSeriesView6.Transparency = ((byte)(0));
            this.chart.SeriesTemplate.View = area3DSeriesView6;
            this.chart.Size = new System.Drawing.Size(1083, 259);
            this.chart.TabIndex = 9;
            // 
            // layoutRoot
            // 
            this.layoutRoot.CustomizationFormText = "layoutRoot";
            this.layoutRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRoot.GroupBordersVisible = false;
            this.layoutRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgProbability,
            this.lcgPercentile,
            this.lblSimulationName,
            this.layoutGroupGraph});
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "Root";
            this.layoutRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRoot.Size = new System.Drawing.Size(1111, 644);
            this.layoutRoot.Text = "Root";
            this.layoutRoot.TextVisible = false;
            // 
            // lcgProbability
            // 
            this.lcgProbability.CustomizationFormText = "layoutControlGroup1";
            this.lcgProbability.ExpandButtonVisible = true;
            this.lcgProbability.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutProbability,
            this.layoutControlItem1,
            this.layoutBtnComputeProbability,
            this.layoutValueFrom,
            this.layoutValueTo,
            this.layCtlItemPercentagePeriods});
            this.lcgProbability.Location = new System.Drawing.Point(0, 506);
            this.lcgProbability.Name = "lcgProbability";
            this.lcgProbability.Size = new System.Drawing.Size(1111, 69);
            this.lcgProbability.Text = "Probability";
            // 
            // layoutProbability
            // 
            this.layoutProbability.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutProbability.AppearanceItemCaption.Options.UseFont = true;
            this.layoutProbability.Control = this.lblProbability;
            this.layoutProbability.CustomizationFormText = "Probability:";
            this.layoutProbability.Location = new System.Drawing.Point(755, 0);
            this.layoutProbability.MinSize = new System.Drawing.Size(73, 17);
            this.layoutProbability.Name = "layoutProbability";
            this.layoutProbability.Size = new System.Drawing.Size(156, 26);
            this.layoutProbability.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutProbability.Text = "Probability:";
            this.layoutProbability.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutProbability.TextSize = new System.Drawing.Size(64, 13);
            this.layoutProbability.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblDescr;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(244, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(135, 17);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(234, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutBtnComputeProbability
            // 
            this.layoutBtnComputeProbability.Control = this.btnComputeProbability;
            this.layoutBtnComputeProbability.CustomizationFormText = "layoutBtnComputeProbability";
            this.layoutBtnComputeProbability.Location = new System.Drawing.Point(911, 0);
            this.layoutBtnComputeProbability.Name = "layoutBtnComputeProbability";
            this.layoutBtnComputeProbability.Size = new System.Drawing.Size(176, 26);
            this.layoutBtnComputeProbability.Text = "layoutBtnComputeProbability";
            this.layoutBtnComputeProbability.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBtnComputeProbability.TextToControlDistance = 0;
            this.layoutBtnComputeProbability.TextVisible = false;
            // 
            // layoutValueFrom
            // 
            this.layoutValueFrom.Control = this.txtFrom;
            this.layoutValueFrom.CustomizationFormText = "From:";
            this.layoutValueFrom.Location = new System.Drawing.Point(478, 0);
            this.layoutValueFrom.Name = "layoutValueFrom";
            this.layoutValueFrom.Size = new System.Drawing.Size(148, 26);
            this.layoutValueFrom.Text = "From:";
            this.layoutValueFrom.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutValueFrom.TextSize = new System.Drawing.Size(28, 13);
            this.layoutValueFrom.TextToControlDistance = 5;
            // 
            // layoutValueTo
            // 
            this.layoutValueTo.Control = this.txtTo;
            this.layoutValueTo.CustomizationFormText = "To:";
            this.layoutValueTo.Location = new System.Drawing.Point(626, 0);
            this.layoutValueTo.Name = "layoutValueTo";
            this.layoutValueTo.Size = new System.Drawing.Size(129, 26);
            this.layoutValueTo.Text = "To:";
            this.layoutValueTo.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutValueTo.TextSize = new System.Drawing.Size(16, 13);
            this.layoutValueTo.TextToControlDistance = 5;
            // 
            // layCtlItemPercentagePeriods
            // 
            this.layCtlItemPercentagePeriods.Control = this.cmbPercentagePeriods;
            this.layCtlItemPercentagePeriods.CustomizationFormText = "Period Type:";
            this.layCtlItemPercentagePeriods.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemPercentagePeriods.Name = "layCtlItemPercentagePeriods";
            this.layCtlItemPercentagePeriods.Size = new System.Drawing.Size(244, 26);
            this.layCtlItemPercentagePeriods.Text = "Period Type:";
            this.layCtlItemPercentagePeriods.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lcgPercentile
            // 
            this.lcgPercentile.CustomizationFormText = "Percentile";
            this.lcgPercentile.ExpandButtonVisible = true;
            this.lcgPercentile.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layCtlItemProbability,
            this.layCtlItemPercentilePeriods});
            this.lcgPercentile.Location = new System.Drawing.Point(0, 575);
            this.lcgPercentile.Name = "lcgPercentile";
            this.lcgPercentile.Size = new System.Drawing.Size(1111, 69);
            this.lcgPercentile.Text = "Percentile";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.lblPercentile;
            this.layoutControlItem5.CustomizationFormText = "Percentile:";
            this.layoutControlItem5.Location = new System.Drawing.Point(556, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(69, 17);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(348, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Percentile:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnComputePercentile;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(904, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(183, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layCtlItemProbability
            // 
            this.layCtlItemProbability.Control = this.txtProbability;
            this.layCtlItemProbability.CustomizationFormText = "Probability:";
            this.layCtlItemProbability.Location = new System.Drawing.Point(244, 0);
            this.layCtlItemProbability.Name = "layCtlItemProbability";
            this.layCtlItemProbability.Size = new System.Drawing.Size(312, 26);
            this.layCtlItemProbability.Text = "Probability:";
            this.layCtlItemProbability.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layCtlItemPercentilePeriods
            // 
            this.layCtlItemPercentilePeriods.Control = this.cmbPercentilePeriods;
            this.layCtlItemPercentilePeriods.CustomizationFormText = "Period Type:";
            this.layCtlItemPercentilePeriods.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemPercentilePeriods.Name = "layCtlItemPercentilePeriods";
            this.layCtlItemPercentilePeriods.Size = new System.Drawing.Size(244, 26);
            this.layCtlItemPercentilePeriods.Text = "Period Type:";
            this.layCtlItemPercentilePeriods.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lblSimulationName
            // 
            this.lblSimulationName.CustomizationFormText = "Criteria";
            this.lblSimulationName.ExpandButtonVisible = true;
            this.lblSimulationName.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup7,
            this.layoutControlGroup1});
            this.lblSimulationName.Location = new System.Drawing.Point(0, 0);
            this.lblSimulationName.Name = "lblSimulationName";
            this.lblSimulationName.Size = new System.Drawing.Size(1111, 176);
            this.lblSimulationName.Text = "lblSimulationName";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem22,
            this.layoutControlItem28,
            this.layoutControlItem21,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.emptySpaceItem3,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(254, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 8, 8);
            this.layoutControlGroup2.Size = new System.Drawing.Size(833, 90);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.lblCurveDate;
            this.layoutControlItem17.CustomizationFormText = "Curve Date:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(240, 17);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(240, 17);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(240, 17);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "Curve Date:";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 13);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.lblPeriodTo;
            this.layoutControlItem22.CustomizationFormText = "Period To:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(240, 17);
            this.layoutControlItem22.Text = "Period To:";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 13);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.lblUseSpot;
            this.layoutControlItem28.CustomizationFormText = "Used Spot Values:";
            this.layoutControlItem28.Location = new System.Drawing.Point(540, 34);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(200, 17);
            this.layoutControlItem28.Text = "Used Spot Values:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.lblPeriodFrom;
            this.layoutControlItem21.CustomizationFormText = "Period From:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(240, 17);
            this.layoutControlItem21.Text = "Period From:";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(80, 13);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.lblPosMethod;
            this.layoutControlItem23.CustomizationFormText = "Position Method";
            this.layoutControlItem23.Location = new System.Drawing.Point(240, 34);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(300, 17);
            this.layoutControlItem23.Text = "Position Method:";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(150, 13);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.lblMarkSensType;
            this.layoutControlItem25.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutControlItem25.Location = new System.Drawing.Point(240, 0);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(300, 17);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(260, 17);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(300, 17);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "Market Sensitivity Type:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(150, 13);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.lblMTMType;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(540, 0);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(200, 17);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(200, 17);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(200, 17);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "MTM Type:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.lblMakSensValue;
            this.layoutControlItem26.CustomizationFormText = "Marketing Sensitivity Value:";
            this.layoutControlItem26.Location = new System.Drawing.Point(240, 17);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(300, 17);
            this.layoutControlItem26.Text = "Marketing Sensitivity Value:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(150, 13);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.lblIsOptionPremium;
            this.layoutControlItem27.CustomizationFormText = "Is Option Premium:";
            this.layoutControlItem27.Location = new System.Drawing.Point(540, 17);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(200, 17);
            this.layoutControlItem27.Text = "Is Option Premium:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(91, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 5);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(740, 17);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(740, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(71, 68);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem2,
            this.layoutControlItem19,
            this.layoutControlItem18});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 8, 8);
            this.layoutControlGroup7.Size = new System.Drawing.Size(254, 90);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.lblNoOfRuns;
            this.layoutControlItem20.CustomizationFormText = "Number Of Runs:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(232, 17);
            this.layoutControlItem20.Text = "Number Of Runs:";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(120, 13);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblStartTime;
            this.layoutControlItem2.CustomizationFormText = "Execution Start Time:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(232, 17);
            this.layoutControlItem2.Text = "Execution Start Time:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(120, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.lblEndTime;
            this.layoutControlItem19.CustomizationFormText = "Execution End Time:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(232, 17);
            this.layoutControlItem19.Text = "Execution End Time:";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(120, 13);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.lblUser;
            this.layoutControlItem18.CustomizationFormText = "User";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(260, 17);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(200, 17);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(232, 17);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "User:";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(120, 13);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 90);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 8, 8);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1087, 43);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lblTrades;
            this.layoutControlItem4.CustomizationFormText = "Trades:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(1, 20);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem4.Size = new System.Drawing.Size(1065, 21);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Trades:";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutGroupGraph
            // 
            this.layoutGroupGraph.CustomizationFormText = "Information";
            this.layoutGroupGraph.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutChart,
            this.layCtlItemPeriodType,
            this.emptySpaceItem1});
            this.layoutGroupGraph.Location = new System.Drawing.Point(0, 176);
            this.layoutGroupGraph.Name = "layoutGroupGraph";
            this.layoutGroupGraph.Size = new System.Drawing.Size(1111, 330);
            this.layoutGroupGraph.Text = "Chart";
            // 
            // layoutChart
            // 
            this.layoutChart.Control = this.chart;
            this.layoutChart.CustomizationFormText = "layoutChart";
            this.layoutChart.Location = new System.Drawing.Point(0, 0);
            this.layoutChart.Name = "layoutChart";
            this.layoutChart.Size = new System.Drawing.Size(1087, 263);
            this.layoutChart.Text = "layoutChart";
            this.layoutChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutChart.TextToControlDistance = 0;
            this.layoutChart.TextVisible = false;
            // 
            // layCtlItemPeriodType
            // 
            this.layCtlItemPeriodType.Control = this.cmbPeriodType;
            this.layCtlItemPeriodType.CustomizationFormText = "Period Type:";
            this.layCtlItemPeriodType.Location = new System.Drawing.Point(0, 263);
            this.layCtlItemPeriodType.Name = "layCtlItemPeriodType";
            this.layCtlItemPeriodType.Size = new System.Drawing.Size(270, 24);
            this.layCtlItemPeriodType.Text = "Period Type:";
            this.layCtlItemPeriodType.TextSize = new System.Drawing.Size(91, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(270, 263);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(817, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExport,
            this.btnPrint,
            this.btnClose});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1111, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 644);
            this.barDockControlBottom.Size = new System.Drawing.Size(1111, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 644);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1111, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 644);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnExport
            // 
            this.btnExport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnExport.Caption = "Export";
            this.btnExport.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnExport.Id = 0;
            this.btnExport.LargeImageIndex = 0;
            this.btnExport.Name = "btnExport";
            // 
            // btnPrint
            // 
            this.btnPrint.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnPrint.Caption = "Print";
            this.btnPrint.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnPrint.Id = 1;
            this.btnPrint.LargeImageIndex = 1;
            this.btnPrint.Name = "btnPrint";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 2;
            this.btnClose.LargeImageIndex = 2;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // MonteCarloSimulationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 676);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MonteCarloSimulationForm";
            this.Load += new System.EventHandler(this.MonteCarloSimulationFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPercentilePeriods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPercentagePeriods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProbability.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram3D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(area3DSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgProbability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProbability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBtnComputeProbability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValueFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutValueTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemPercentagePeriods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgPercentile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemProbability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemPercentilePeriods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSimulationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraCharts.ChartControl chart;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupGraph;
        private DevExpress.XtraLayout.LayoutControlItem layoutChart;
        private DevExpress.XtraLayout.LayoutControlGroup lcgProbability;
        private DevExpress.XtraEditors.LabelControl lblProbability;
        private DevExpress.XtraLayout.LayoutControlItem layoutProbability;
        private DevExpress.XtraEditors.LabelControl lblDescr;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnComputeProbability;
        private DevExpress.XtraLayout.LayoutControlItem layoutBtnComputeProbability;
        private DevExpress.Utils.ImageCollection imageList16;
        private DevExpress.XtraEditors.SpinEdit txtTo;
        private DevExpress.XtraEditors.SpinEdit txtFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutValueFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutValueTo;
        private DevExpress.XtraLayout.LayoutControlGroup lcgPercentile;
        private DevExpress.XtraEditors.SpinEdit txtProbability;
        private DevExpress.XtraEditors.SimpleButton btnComputePercentile;
        private DevExpress.XtraEditors.LabelControl lblPercentile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemProbability;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodType;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemPeriodType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.LookUpEdit cmbPercentilePeriods;
        private DevExpress.XtraEditors.LookUpEdit cmbPercentagePeriods;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemPercentagePeriods;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemPercentilePeriods;
        private DevExpress.XtraEditors.LabelControl lblMTMType;
        private DevExpress.XtraEditors.LabelControl lblPosMethod;
        private DevExpress.XtraEditors.LabelControl lblPeriodTo;
        private DevExpress.XtraEditors.LabelControl lblPeriodFrom;
        private DevExpress.XtraEditors.LabelControl lblNoOfRuns;
        private DevExpress.XtraEditors.LabelControl lblEndTime;
        private DevExpress.XtraEditors.LabelControl lblStartTime;
        private DevExpress.XtraEditors.LabelControl lblUser;
        private DevExpress.XtraEditors.LabelControl lblCurveDate;
        private DevExpress.XtraLayout.LayoutControlGroup lblSimulationName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.LabelControl lblUseSpot;
        private DevExpress.XtraEditors.LabelControl lblIsOptionPremium;
        private DevExpress.XtraEditors.LabelControl lblMakSensValue;
        private DevExpress.XtraEditors.LabelControl lblMarkSensType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.LabelControl lblTrades;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnExport;
        private DevExpress.XtraBars.BarLargeButtonItem btnPrint;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}