﻿namespace Exis.WinClient.Controls.Core
{
    partial class GeneratePOSForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneratePOSForm));
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.lblOnlyNonZeroTotalDays = new DevExpress.XtraEditors.LabelControl();
            this.tbResults = new DevExpress.XtraTab.XtraTabControl();
            this.tbpgPosition = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chkReverseNumberSign = new DevExpress.XtraEditors.CheckEdit();
            this.btnExportToCSV = new DevExpress.XtraEditors.SimpleButton();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridTradeActionResults = new DevExpress.XtraGrid.GridControl();
            this.gridTradeActionResultsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnExportResults = new DevExpress.XtraEditors.SimpleButton();
            this.rdgPositionResultsType = new DevExpress.XtraEditors.RadioGroup();
            this.rdgResultsAggregationType = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciGridPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutResultsAggregationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionResultsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcibtnExport = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tbpgInitialMargin = new DevExpress.XtraTab.XtraTabPage();
            this.lcInitialMargin = new DevExpress.XtraLayout.LayoutControl();
            this.lcgInitialMargin = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tbpgIMNasdaq = new DevExpress.XtraTab.XtraTabPage();
            this.lcIMNasdaq = new DevExpress.XtraLayout.LayoutControl();
            this.lcgIMNasdaq = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblIsOptionalPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblIsMinimumPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblIsDraft = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodTo = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodFrom = new DevExpress.XtraEditors.LabelControl();
            this.lblEntity = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkCalculateCommissions = new DevExpress.XtraEditors.CheckEdit();
            this.chkCalculateSums = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseSpotValues = new DevExpress.XtraEditors.CheckEdit();
            this.dtpFilterCurveDate = new DevExpress.XtraEditors.DateEdit();
            this.txtMarketSensitivityValue = new DevExpress.XtraEditors.SpinEdit();
            this.cmbMarketSensitivityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPositionMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnGeneratePosition = new DevExpress.XtraEditors.SimpleButton();
            this.layoutGroupRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemGeneratePosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutFilterCurveDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUseSpotValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCalculateSums = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCalculateCommissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupTradeResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeActionResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTabs = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList16 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlRoot = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbResults)).BeginInit();
            this.tbResults.SuspendLayout();
            this.tbpgPosition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReverseNumberSign.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResultsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgPositionResultsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGridPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsAggregationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionResultsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcibtnExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.tbpgInitialMargin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInitialMargin)).BeginInit();
            this.tbpgIMNasdaq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcIMNasdaq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgIMNasdaq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGeneratePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateCommissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTradeResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTabs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.AllowCustomizationMenu = false;
            this.layoutRoot.Controls.Add(this.lblOnlyNonZeroTotalDays);
            this.layoutRoot.Controls.Add(this.tbResults);
            this.layoutRoot.Controls.Add(this.lblIsOptionalPeriod);
            this.layoutRoot.Controls.Add(this.lblIsMinimumPeriod);
            this.layoutRoot.Controls.Add(this.lblIsDraft);
            this.layoutRoot.Controls.Add(this.lblPeriodTo);
            this.layoutRoot.Controls.Add(this.lblPeriodFrom);
            this.layoutRoot.Controls.Add(this.lblEntity);
            this.layoutRoot.Controls.Add(this.labelControl1);
            this.layoutRoot.Controls.Add(this.chkCalculateCommissions);
            this.layoutRoot.Controls.Add(this.chkCalculateSums);
            this.layoutRoot.Controls.Add(this.chkUseSpotValues);
            this.layoutRoot.Controls.Add(this.dtpFilterCurveDate);
            this.layoutRoot.Controls.Add(this.txtMarketSensitivityValue);
            this.layoutRoot.Controls.Add(this.cmbMarketSensitivityType);
            this.layoutRoot.Controls.Add(this.cmbPositionMethod);
            this.layoutRoot.Controls.Add(this.btnGeneratePosition);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1643, 0, 426, 731);
            this.layoutRoot.Root = this.layoutGroupRoot;
            this.layoutRoot.Size = new System.Drawing.Size(1219, 645);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // lblOnlyNonZeroTotalDays
            // 
            this.lblOnlyNonZeroTotalDays.Location = new System.Drawing.Point(1300, 31);
            this.lblOnlyNonZeroTotalDays.Name = "lblOnlyNonZeroTotalDays";
            this.lblOnlyNonZeroTotalDays.Size = new System.Drawing.Size(61, 14);
            this.lblOnlyNonZeroTotalDays.StyleController = this.layoutRoot;
            this.lblOnlyNonZeroTotalDays.TabIndex = 46;
            // 
            // tbResults
            // 
            this.tbResults.Location = new System.Drawing.Point(9, 117);
            this.tbResults.Name = "tbResults";
            this.tbResults.SelectedTabPage = this.tbpgPosition;
            this.tbResults.Size = new System.Drawing.Size(1365, 502);
            this.tbResults.TabIndex = 36;
            this.tbResults.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbpgPosition,
            this.tbpgInitialMargin,
            this.tbpgIMNasdaq});
            // 
            // tbpgPosition
            // 
            this.tbpgPosition.Controls.Add(this.layoutControl1);
            this.tbpgPosition.Name = "tbpgPosition";
            this.tbpgPosition.Size = new System.Drawing.Size(1359, 474);
            this.tbpgPosition.Text = "Position";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkReverseNumberSign);
            this.layoutControl1.Controls.Add(this.btnExportToCSV);
            this.layoutControl1.Controls.Add(this.gridTradeActionResults);
            this.layoutControl1.Controls.Add(this.btnExportResults);
            this.layoutControl1.Controls.Add(this.rdgPositionResultsType);
            this.layoutControl1.Controls.Add(this.rdgResultsAggregationType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup6;
            this.layoutControl1.Size = new System.Drawing.Size(1359, 474);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // chkReverseNumberSign
            // 
            this.chkReverseNumberSign.Location = new System.Drawing.Point(1308, 77);
            this.chkReverseNumberSign.Name = "chkReverseNumberSign";
            this.chkReverseNumberSign.Properties.AutoHeight = false;
            this.chkReverseNumberSign.Properties.Caption = "";
            this.chkReverseNumberSign.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkReverseNumberSign.Size = new System.Drawing.Size(17, 34);
            this.chkReverseNumberSign.StyleController = this.layoutControl1;
            this.chkReverseNumberSign.TabIndex = 23;
            // 
            // btnExportToCSV
            // 
            this.btnExportToCSV.ImageIndex = 16;
            this.btnExportToCSV.ImageList = this.imageList24;
            this.btnExportToCSV.Location = new System.Drawing.Point(951, 77);
            this.btnExportToCSV.MinimumSize = new System.Drawing.Size(0, 28);
            this.btnExportToCSV.Name = "btnExportToCSV";
            this.btnExportToCSV.Size = new System.Drawing.Size(224, 30);
            this.btnExportToCSV.StyleController = this.layoutControl1;
            this.btnExportToCSV.TabIndex = 8;
            this.btnExportToCSV.Text = "Export to CSV and Convert (SPAN)";
            this.btnExportToCSV.Click += new System.EventHandler(this.btnExportToCSV_Click);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            this.imageList24.Images.SetKeyName(6, "gantt-chart.ico");
            this.imageList24.Images.SetKeyName(7, "line-chart.ico");
            this.imageList24.Images.SetKeyName(8, "export24x24.ico");
            this.imageList24.Images.SetKeyName(9, "view24x24.ico");
            this.imageList24.Images.SetKeyName(10, "activate.ico");
            this.imageList24.Images.SetKeyName(11, "deactivate.ico");
            this.imageList24.Images.SetKeyName(12, "text_sum.ico");
            this.imageList24.Images.SetKeyName(15, "ExportToXML_24x24.png");
            this.imageList24.Images.SetKeyName(16, "ExportToCSV_24x24.png");
            // 
            // gridTradeActionResults
            // 
            this.gridTradeActionResults.Location = new System.Drawing.Point(12, 127);
            this.gridTradeActionResults.MainView = this.gridTradeActionResultsView;
            this.gridTradeActionResults.MenuManager = this.barManager1;
            this.gridTradeActionResults.Name = "gridTradeActionResults";
            this.gridTradeActionResults.Size = new System.Drawing.Size(1335, 335);
            this.gridTradeActionResults.TabIndex = 8;
            this.gridTradeActionResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridTradeActionResultsView});
            // 
            // gridTradeActionResultsView
            // 
            this.gridTradeActionResultsView.GridControl = this.gridTradeActionResults;
            this.gridTradeActionResultsView.Name = "gridTradeActionResultsView";
            this.gridTradeActionResultsView.OptionsView.ShowFooter = true;
            this.gridTradeActionResultsView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.GridTradeActionResultsViewCustomSummaryCalculate);
            this.gridTradeActionResultsView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridTradeActionResultsView_CustomUnboundColumnData);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnClose});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.FloatLocation = new System.Drawing.Point(2322, 344);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 0;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1219, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 645);
            this.barDockControlBottom.Size = new System.Drawing.Size(1219, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 645);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1219, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 645);
            // 
            // btnExportResults
            // 
            this.btnExportResults.ImageIndex = 8;
            this.btnExportResults.ImageList = this.imageList24;
            this.btnExportResults.Location = new System.Drawing.Point(951, 43);
            this.btnExportResults.Name = "btnExportResults";
            this.btnExportResults.Size = new System.Drawing.Size(374, 30);
            this.btnExportResults.StyleController = this.layoutControl1;
            this.btnExportResults.TabIndex = 7;
            this.btnExportResults.Text = "Export";
            this.btnExportResults.Click += new System.EventHandler(this.btnExportResults_Click);
            // 
            // rdgPositionResultsType
            // 
            this.rdgPositionResultsType.Location = new System.Drawing.Point(660, 43);
            this.rdgPositionResultsType.MenuManager = this.barManager1;
            this.rdgPositionResultsType.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdgPositionResultsType.Name = "rdgPositionResultsType";
            this.rdgPositionResultsType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Days"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Vessels"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Tonnes")});
            this.rdgPositionResultsType.Size = new System.Drawing.Size(287, 68);
            this.rdgPositionResultsType.StyleController = this.layoutControl1;
            this.rdgPositionResultsType.TabIndex = 6;
            this.rdgPositionResultsType.SelectedIndexChanged += new System.EventHandler(this.RdgPositionResultsTypeSelectedIndexChanged);
            // 
            // rdgResultsAggregationType
            // 
            this.rdgResultsAggregationType.Location = new System.Drawing.Point(129, 43);
            this.rdgResultsAggregationType.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdgResultsAggregationType.Name = "rdgResultsAggregationType";
            this.rdgResultsAggregationType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Month"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Quarter"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Calendar")});
            this.rdgResultsAggregationType.Size = new System.Drawing.Size(422, 68);
            this.rdgResultsAggregationType.StyleController = this.layoutControl1;
            this.rdgResultsAggregationType.TabIndex = 5;
            this.rdgResultsAggregationType.SelectedIndexChanged += new System.EventHandler(this.RdgResultsAggregationTypeSelectedIndexChanged);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciGridPosition,
            this.layoutControlGroup7});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1359, 474);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // lciGridPosition
            // 
            this.lciGridPosition.Control = this.gridTradeActionResults;
            this.lciGridPosition.CustomizationFormText = "lciGridPosition";
            this.lciGridPosition.Location = new System.Drawing.Point(0, 115);
            this.lciGridPosition.Name = "lciGridPosition";
            this.lciGridPosition.Size = new System.Drawing.Size(1339, 339);
            this.lciGridPosition.Text = "lciGridPosition";
            this.lciGridPosition.TextSize = new System.Drawing.Size(0, 0);
            this.lciGridPosition.TextToControlDistance = 0;
            this.lciGridPosition.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Filters";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutResultsAggregationType,
            this.layoutPositionResultsType,
            this.lcibtnExport,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem10});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1339, 115);
            this.layoutControlGroup7.Text = "Filters";
            // 
            // layoutResultsAggregationType
            // 
            this.layoutResultsAggregationType.Control = this.rdgResultsAggregationType;
            this.layoutResultsAggregationType.CustomizationFormText = "Aggregation:";
            this.layoutResultsAggregationType.Location = new System.Drawing.Point(0, 0);
            this.layoutResultsAggregationType.Name = "layoutResultsAggregationType";
            this.layoutResultsAggregationType.Size = new System.Drawing.Size(531, 72);
            this.layoutResultsAggregationType.Text = "Aggregation:";
            this.layoutResultsAggregationType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutResultsAggregationType.TextSize = new System.Drawing.Size(100, 20);
            this.layoutResultsAggregationType.TextToControlDistance = 5;
            // 
            // layoutPositionResultsType
            // 
            this.layoutPositionResultsType.Control = this.rdgPositionResultsType;
            this.layoutPositionResultsType.CustomizationFormText = "Results Type:";
            this.layoutPositionResultsType.Location = new System.Drawing.Point(531, 0);
            this.layoutPositionResultsType.Name = "layoutPositionResultsType";
            this.layoutPositionResultsType.Size = new System.Drawing.Size(396, 72);
            this.layoutPositionResultsType.Text = "Results Type:";
            this.layoutPositionResultsType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutPositionResultsType.TextSize = new System.Drawing.Size(100, 20);
            this.layoutPositionResultsType.TextToControlDistance = 5;
            // 
            // lcibtnExport
            // 
            this.lcibtnExport.Control = this.btnExportResults;
            this.lcibtnExport.CustomizationFormText = "lcibtnExport";
            this.lcibtnExport.Location = new System.Drawing.Point(927, 0);
            this.lcibtnExport.Name = "lcibtnExport";
            this.lcibtnExport.Size = new System.Drawing.Size(378, 34);
            this.lcibtnExport.Text = "lcibtnExport";
            this.lcibtnExport.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcibtnExport.TextSize = new System.Drawing.Size(0, 0);
            this.lcibtnExport.TextToControlDistance = 0;
            this.lcibtnExport.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnExportToCSV;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(927, 34);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(228, 38);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(1305, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 72);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.chkReverseNumberSign;
            this.layoutControlItem10.CustomizationFormText = "Reverse Number Sign";
            this.layoutControlItem10.Location = new System.Drawing.Point(1155, 34);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 38);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(150, 38);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(150, 38);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "Reverse Number Sign:";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(124, 16);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // tbpgInitialMargin
            // 
            this.tbpgInitialMargin.Controls.Add(this.lcInitialMargin);
            this.tbpgInitialMargin.Name = "tbpgInitialMargin";
            this.tbpgInitialMargin.Size = new System.Drawing.Size(1359, 475);
            this.tbpgInitialMargin.Text = "Initial Margin";
            // 
            // lcInitialMargin
            // 
            this.lcInitialMargin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcInitialMargin.Location = new System.Drawing.Point(0, 0);
            this.lcInitialMargin.Name = "lcInitialMargin";
            this.lcInitialMargin.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(522, 33, 512, 350);
            this.lcInitialMargin.Root = this.lcgInitialMargin;
            this.lcInitialMargin.Size = new System.Drawing.Size(1359, 475);
            this.lcInitialMargin.TabIndex = 0;
            this.lcInitialMargin.Text = "layoutControl2";
            // 
            // lcgInitialMargin
            // 
            this.lcgInitialMargin.CustomizationFormText = "Root";
            this.lcgInitialMargin.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgInitialMargin.GroupBordersVisible = false;
            this.lcgInitialMargin.Location = new System.Drawing.Point(0, 0);
            this.lcgInitialMargin.Name = "Root";
            this.lcgInitialMargin.Size = new System.Drawing.Size(1359, 475);
            this.lcgInitialMargin.Text = "Root";
            this.lcgInitialMargin.TextVisible = false;
            // 
            // tbpgIMNasdaq
            // 
            this.tbpgIMNasdaq.Controls.Add(this.lcIMNasdaq);
            this.tbpgIMNasdaq.Name = "tbpgIMNasdaq";
            this.tbpgIMNasdaq.Size = new System.Drawing.Size(1359, 475);
            this.tbpgIMNasdaq.Text = "Initial Margin (Nasdaq)";
            // 
            // lcIMNasdaq
            // 
            this.lcIMNasdaq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcIMNasdaq.Location = new System.Drawing.Point(0, 0);
            this.lcIMNasdaq.Name = "lcIMNasdaq";
            this.lcIMNasdaq.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(522, 33, 512, 350);
            this.lcIMNasdaq.Root = this.lcgIMNasdaq;
            this.lcIMNasdaq.Size = new System.Drawing.Size(1359, 475);
            this.lcIMNasdaq.TabIndex = 0;
            this.lcIMNasdaq.Text = "layoutControl2";
            // 
            // lcgIMNasdaq
            // 
            this.lcgIMNasdaq.CustomizationFormText = "Root";
            this.lcgIMNasdaq.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgIMNasdaq.GroupBordersVisible = false;
            this.lcgIMNasdaq.Location = new System.Drawing.Point(0, 0);
            this.lcgIMNasdaq.Name = "Root";
            this.lcgIMNasdaq.Size = new System.Drawing.Size(1359, 475);
            this.lcgIMNasdaq.Text = "Root";
            this.lcgIMNasdaq.TextVisible = false;
            // 
            // lblIsOptionalPeriod
            // 
            this.lblIsOptionalPeriod.Location = new System.Drawing.Point(1039, 31);
            this.lblIsOptionalPeriod.Name = "lblIsOptionalPeriod";
            this.lblIsOptionalPeriod.Size = new System.Drawing.Size(61, 14);
            this.lblIsOptionalPeriod.StyleController = this.layoutRoot;
            this.lblIsOptionalPeriod.TabIndex = 34;
            // 
            // lblIsMinimumPeriod
            // 
            this.lblIsMinimumPeriod.Location = new System.Drawing.Point(864, 31);
            this.lblIsMinimumPeriod.Name = "lblIsMinimumPeriod";
            this.lblIsMinimumPeriod.Size = new System.Drawing.Size(61, 14);
            this.lblIsMinimumPeriod.StyleController = this.layoutRoot;
            this.lblIsMinimumPeriod.TabIndex = 33;
            // 
            // lblIsDraft
            // 
            this.lblIsDraft.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblIsDraft.Location = new System.Drawing.Point(699, 31);
            this.lblIsDraft.Name = "lblIsDraft";
            this.lblIsDraft.Size = new System.Drawing.Size(61, 14);
            this.lblIsDraft.StyleController = this.layoutRoot;
            this.lblIsDraft.TabIndex = 32;
            // 
            // lblPeriodTo
            // 
            this.lblPeriodTo.Location = new System.Drawing.Point(564, 31);
            this.lblPeriodTo.Name = "lblPeriodTo";
            this.lblPeriodTo.Size = new System.Drawing.Size(76, 14);
            this.lblPeriodTo.StyleController = this.layoutRoot;
            this.lblPeriodTo.TabIndex = 31;
            // 
            // lblPeriodFrom
            // 
            this.lblPeriodFrom.Location = new System.Drawing.Point(409, 31);
            this.lblPeriodFrom.Name = "lblPeriodFrom";
            this.lblPeriodFrom.Size = new System.Drawing.Size(76, 14);
            this.lblPeriodFrom.StyleController = this.layoutRoot;
            this.lblPeriodFrom.TabIndex = 30;
            // 
            // lblEntity
            // 
            this.lblEntity.Location = new System.Drawing.Point(238, 31);
            this.lblEntity.Name = "lblEntity";
            this.lblEntity.Size = new System.Drawing.Size(92, 14);
            this.lblEntity.StyleController = this.layoutRoot;
            this.lblEntity.TabIndex = 29;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.labelControl1.Location = new System.Drawing.Point(12, 31);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.labelControl1.Size = new System.Drawing.Size(177, 14);
            this.labelControl1.StyleController = this.layoutRoot;
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = "Filter Defined in View Trades:";
            // 
            // chkCalculateCommissions
            // 
            this.chkCalculateCommissions.Location = new System.Drawing.Point(1230, 58);
            this.chkCalculateCommissions.Name = "chkCalculateCommissions";
            this.chkCalculateCommissions.Properties.Caption = "";
            this.chkCalculateCommissions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCalculateCommissions.Size = new System.Drawing.Size(38, 19);
            this.chkCalculateCommissions.StyleController = this.layoutRoot;
            this.chkCalculateCommissions.TabIndex = 22;
            // 
            // chkCalculateSums
            // 
            this.chkCalculateSums.Location = new System.Drawing.Point(1108, 58);
            this.chkCalculateSums.Name = "chkCalculateSums";
            this.chkCalculateSums.Properties.Caption = "";
            this.chkCalculateSums.Size = new System.Drawing.Size(48, 19);
            this.chkCalculateSums.StyleController = this.layoutRoot;
            this.chkCalculateSums.TabIndex = 21;
            // 
            // chkUseSpotValues
            // 
            this.chkUseSpotValues.Location = new System.Drawing.Point(1030, 58);
            this.chkUseSpotValues.Name = "chkUseSpotValues";
            this.chkUseSpotValues.Properties.Caption = "";
            this.chkUseSpotValues.Size = new System.Drawing.Size(31, 19);
            this.chkUseSpotValues.StyleController = this.layoutRoot;
            this.chkUseSpotValues.TabIndex = 20;
            // 
            // dtpFilterCurveDate
            // 
            this.dtpFilterCurveDate.EditValue = null;
            this.dtpFilterCurveDate.Location = new System.Drawing.Point(79, 58);
            this.dtpFilterCurveDate.Name = "dtpFilterCurveDate";
            this.dtpFilterCurveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterCurveDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterCurveDate.Size = new System.Drawing.Size(177, 20);
            this.dtpFilterCurveDate.StyleController = this.layoutRoot;
            this.dtpFilterCurveDate.TabIndex = 10;
            // 
            // txtMarketSensitivityValue
            // 
            this.txtMarketSensitivityValue.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtMarketSensitivityValue.Location = new System.Drawing.Point(885, 58);
            this.txtMarketSensitivityValue.Name = "txtMarketSensitivityValue";
            this.txtMarketSensitivityValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMarketSensitivityValue.Properties.Mask.EditMask = "D";
            this.txtMarketSensitivityValue.Properties.MaxLength = 7;
            this.txtMarketSensitivityValue.Size = new System.Drawing.Size(89, 20);
            this.txtMarketSensitivityValue.StyleController = this.layoutRoot;
            this.txtMarketSensitivityValue.TabIndex = 9;
            // 
            // cmbMarketSensitivityType
            // 
            this.cmbMarketSensitivityType.Location = new System.Drawing.Point(670, 58);
            this.cmbMarketSensitivityType.Name = "cmbMarketSensitivityType";
            this.cmbMarketSensitivityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbMarketSensitivityType.Properties.Items.AddRange(new object[] {
            "None",
            "Absolute",
            "Percentage",
            "Stress"});
            this.cmbMarketSensitivityType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbMarketSensitivityType.Size = new System.Drawing.Size(88, 20);
            this.cmbMarketSensitivityType.StyleController = this.layoutRoot;
            this.cmbMarketSensitivityType.TabIndex = 8;
            this.cmbMarketSensitivityType.SelectedValueChanged += new System.EventHandler(this.CmbMarketSensitivityTypeSelectedValueChanged);
            // 
            // cmbPositionMethod
            // 
            this.cmbPositionMethod.Location = new System.Drawing.Point(345, 58);
            this.cmbPositionMethod.Name = "cmbPositionMethod";
            this.cmbPositionMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPositionMethod.Properties.Items.AddRange(new object[] {
            "Static",
            "Dynamic",
            "Delta"});
            this.cmbPositionMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPositionMethod.Size = new System.Drawing.Size(198, 20);
            this.cmbPositionMethod.StyleController = this.layoutRoot;
            this.cmbPositionMethod.TabIndex = 22;
            this.cmbPositionMethod.SelectedValueChanged += new System.EventHandler(this.cmbPositionMethod_SelectedValueChanged);
            // 
            // btnGeneratePosition
            // 
            this.btnGeneratePosition.ImageIndex = 7;
            this.btnGeneratePosition.ImageList = this.imageList24;
            this.btnGeneratePosition.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGeneratePosition.Location = new System.Drawing.Point(1277, 53);
            this.btnGeneratePosition.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.btnGeneratePosition.Name = "btnGeneratePosition";
            this.btnGeneratePosition.Size = new System.Drawing.Size(96, 32);
            this.btnGeneratePosition.StyleController = this.layoutRoot;
            this.btnGeneratePosition.TabIndex = 8;
            this.btnGeneratePosition.Text = "Calculate";
            this.btnGeneratePosition.Click += new System.EventHandler(this.btnGeneratePosition_Click);
            // 
            // layoutGroupRoot
            // 
            this.layoutGroupRoot.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutGroupRoot.GroupBordersVisible = false;
            this.layoutGroupRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTrades});
            this.layoutGroupRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupRoot.Name = "Root";
            this.layoutGroupRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupRoot.Size = new System.Drawing.Size(1383, 628);
            this.layoutGroupRoot.Text = "Root";
            this.layoutGroupRoot.TextVisible = false;
            // 
            // layoutGroupTrades
            // 
            this.layoutGroupTrades.CustomizationFormText = "Trades";
            this.layoutGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupTrades,
            this.layoutControlGroupTradeResults});
            this.layoutGroupTrades.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTrades.Name = "layoutGroupTrades";
            this.layoutGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTrades.Size = new System.Drawing.Size(1383, 628);
            this.layoutGroupTrades.Text = "Trades";
            this.layoutGroupTrades.TextVisible = false;
            // 
            // layoutControlGroupTrades
            // 
            this.layoutControlGroupTrades.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupTrades.ExpandButtonVisible = true;
            this.layoutControlGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeDetails});
            this.layoutControlGroupTrades.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTrades.Name = "layoutControlGroupTrades";
            this.layoutControlGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Size = new System.Drawing.Size(1377, 89);
            this.layoutControlGroupTrades.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Text = "layoutControlGroupTrades";
            this.layoutControlGroupTrades.TextVisible = false;
            // 
            // layoutGroupTradeDetails
            // 
            this.layoutGroupTradeDetails.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupTradeDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup2});
            this.layoutGroupTradeDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeDetails.Name = "layoutGroupTradeDetails";
            this.layoutGroupTradeDetails.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeDetails.Size = new System.Drawing.Size(1375, 68);
            this.layoutGroupTradeDetails.Text = "Details";
            this.layoutGroupTradeDetails.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemGeneratePosition,
            this.layoutControlGroup5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1369, 38);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutItemGeneratePosition
            // 
            this.layoutItemGeneratePosition.Control = this.btnGeneratePosition;
            this.layoutItemGeneratePosition.CustomizationFormText = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.Location = new System.Drawing.Point(1267, 0);
            this.layoutItemGeneratePosition.MaxSize = new System.Drawing.Size(100, 60);
            this.layoutItemGeneratePosition.MinSize = new System.Drawing.Size(100, 36);
            this.layoutItemGeneratePosition.Name = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.Size = new System.Drawing.Size(100, 36);
            this.layoutItemGeneratePosition.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemGeneratePosition.Text = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutItemGeneratePosition.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemGeneratePosition.TextToControlDistance = 0;
            this.layoutItemGeneratePosition.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutFilterCurveDate,
            this.layoutPositionMethod,
            this.layoutMarketSensitivityType,
            this.layoutMarketSensitivityValue,
            this.layoutUseSpotValues,
            this.layoutItemCalculateSums,
            this.layoutItemCalculateCommissions});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1267, 36);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutFilterCurveDate
            // 
            this.layoutFilterCurveDate.Control = this.dtpFilterCurveDate;
            this.layoutFilterCurveDate.CustomizationFormText = "Curve Date:";
            this.layoutFilterCurveDate.Location = new System.Drawing.Point(0, 0);
            this.layoutFilterCurveDate.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutFilterCurveDate.MinSize = new System.Drawing.Size(118, 24);
            this.layoutFilterCurveDate.Name = "layoutFilterCurveDate";
            this.layoutFilterCurveDate.Size = new System.Drawing.Size(245, 26);
            this.layoutFilterCurveDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutFilterCurveDate.Text = "Curve Date:";
            this.layoutFilterCurveDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutFilterCurveDate.TextSize = new System.Drawing.Size(59, 13);
            this.layoutFilterCurveDate.TextToControlDistance = 5;
            // 
            // layoutPositionMethod
            // 
            this.layoutPositionMethod.Control = this.cmbPositionMethod;
            this.layoutPositionMethod.CustomizationFormText = "Position Method:";
            this.layoutPositionMethod.Location = new System.Drawing.Point(245, 0);
            this.layoutPositionMethod.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutPositionMethod.MinSize = new System.Drawing.Size(139, 24);
            this.layoutPositionMethod.Name = "layoutPositionMethod";
            this.layoutPositionMethod.Size = new System.Drawing.Size(287, 26);
            this.layoutPositionMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPositionMethod.Text = "Position Method:";
            this.layoutPositionMethod.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutPositionMethod.TextSize = new System.Drawing.Size(80, 13);
            this.layoutPositionMethod.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityType
            // 
            this.layoutMarketSensitivityType.Control = this.cmbMarketSensitivityType;
            this.layoutMarketSensitivityType.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.Location = new System.Drawing.Point(532, 0);
            this.layoutMarketSensitivityType.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityType.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityType.Name = "layoutMarketSensitivityType";
            this.layoutMarketSensitivityType.Size = new System.Drawing.Size(215, 26);
            this.layoutMarketSensitivityType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityType.Text = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityType.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityType.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityValue
            // 
            this.layoutMarketSensitivityValue.Control = this.txtMarketSensitivityValue;
            this.layoutMarketSensitivityValue.CustomizationFormText = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.Location = new System.Drawing.Point(747, 0);
            this.layoutMarketSensitivityValue.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityValue.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityValue.Name = "layoutMarketSensitivityValue";
            this.layoutMarketSensitivityValue.Size = new System.Drawing.Size(216, 26);
            this.layoutMarketSensitivityValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityValue.Text = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityValue.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityValue.TextToControlDistance = 5;
            // 
            // layoutUseSpotValues
            // 
            this.layoutUseSpotValues.Control = this.chkUseSpotValues;
            this.layoutUseSpotValues.CustomizationFormText = "Use Spot:";
            this.layoutUseSpotValues.Location = new System.Drawing.Point(963, 0);
            this.layoutUseSpotValues.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutUseSpotValues.MinSize = new System.Drawing.Size(76, 24);
            this.layoutUseSpotValues.Name = "layoutUseSpotValues";
            this.layoutUseSpotValues.Size = new System.Drawing.Size(87, 26);
            this.layoutUseSpotValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUseSpotValues.Text = "Use Spot:";
            this.layoutUseSpotValues.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutUseSpotValues.TextSize = new System.Drawing.Size(47, 13);
            this.layoutUseSpotValues.TextToControlDistance = 5;
            // 
            // layoutItemCalculateSums
            // 
            this.layoutItemCalculateSums.Control = this.chkCalculateSums;
            this.layoutItemCalculateSums.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutItemCalculateSums.CustomizationFormText = "Calculate Sums:";
            this.layoutItemCalculateSums.Location = new System.Drawing.Point(1050, 0);
            this.layoutItemCalculateSums.Name = "layoutItemCalculateSums";
            this.layoutItemCalculateSums.Size = new System.Drawing.Size(95, 26);
            this.layoutItemCalculateSums.Text = "Totals:";
            this.layoutItemCalculateSums.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateSums.TextSize = new System.Drawing.Size(33, 13);
            this.layoutItemCalculateSums.TextToControlDistance = 10;
            // 
            // layoutItemCalculateCommissions
            // 
            this.layoutItemCalculateCommissions.Control = this.chkCalculateCommissions;
            this.layoutItemCalculateCommissions.CustomizationFormText = "Commissions";
            this.layoutItemCalculateCommissions.Location = new System.Drawing.Point(1145, 0);
            this.layoutItemCalculateCommissions.Name = "layoutItemCalculateCommissions";
            this.layoutItemCalculateCommissions.Size = new System.Drawing.Size(112, 26);
            this.layoutItemCalculateCommissions.Text = "Commissions";
            this.layoutItemCalculateCommissions.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateCommissions.TextSize = new System.Drawing.Size(60, 13);
            this.layoutItemCalculateCommissions.TextToControlDistance = 10;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "POS Filters:";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.layoutControlItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1369, 24);
            this.layoutControlGroup2.Text = "POS Filters:";
            this.layoutControlGroup2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(181, 18);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(181, 18);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            this.layoutControlItem4.TrimClientAreaToControl = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblEntity;
            this.layoutControlItem1.CustomizationFormText = "Book/XX Selected:";
            this.layoutControlItem1.Location = new System.Drawing.Point(181, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(141, 17);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(141, 18);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Enitity:";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(35, 13);
            this.layoutControlItem1.TextToControlDistance = 10;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblPeriodFrom;
            this.layoutControlItem2.CustomizationFormText = "Period From:";
            this.layoutControlItem2.Location = new System.Drawing.Point(322, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(155, 17);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(155, 18);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Period From:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem2.TextToControlDistance = 10;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lblIsDraft;
            this.layoutControlItem7.CustomizationFormText = "Is Draft:";
            this.layoutControlItem7.Location = new System.Drawing.Point(632, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(120, 17);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(120, 18);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Is Draft:";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(45, 13);
            this.layoutControlItem7.TextToControlDistance = 10;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lblPeriodTo;
            this.layoutControlItem6.CustomizationFormText = "Period To:";
            this.layoutControlItem6.Location = new System.Drawing.Point(477, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(155, 17);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(155, 18);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Period To:";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem6.TextToControlDistance = 10;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lblIsMinimumPeriod;
            this.layoutControlItem8.CustomizationFormText = "Is Minimum Period:";
            this.layoutControlItem8.Location = new System.Drawing.Point(752, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(165, 17);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(165, 18);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Is Minimum Period:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            this.layoutControlItem8.TextToControlDistance = 10;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.lblIsOptionalPeriod;
            this.layoutControlItem9.CustomizationFormText = "Is Optional Period:";
            this.layoutControlItem9.Location = new System.Drawing.Point(917, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(175, 17);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(175, 18);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "Is Optional Period:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 13);
            this.layoutControlItem9.TextToControlDistance = 10;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(1353, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 18);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lblOnlyNonZeroTotalDays;
            this.layoutControlItem5.CustomizationFormText = "Only trades with Total Days > 0:";
            this.layoutControlItem5.Location = new System.Drawing.Point(1092, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(261, 17);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(261, 18);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Only trades with Total Days > 0:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(186, 13);
            this.layoutControlItem5.TextToControlDistance = 10;
            // 
            // layoutControlGroupTradeResults
            // 
            this.layoutControlGroupTradeResults.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroupTradeResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeActionResults});
            this.layoutControlGroupTradeResults.Location = new System.Drawing.Point(0, 89);
            this.layoutControlGroupTradeResults.Name = "layoutControlGroupTradeResults";
            this.layoutControlGroupTradeResults.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTradeResults.Size = new System.Drawing.Size(1377, 533);
            this.layoutControlGroupTradeResults.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTradeResults.Text = "layoutControlGroupTradeResults";
            this.layoutControlGroupTradeResults.TextVisible = false;
            // 
            // layoutGroupTradeActionResults
            // 
            this.layoutGroupTradeActionResults.CustomizationFormText = "Action Results";
            this.layoutGroupTradeActionResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTabs});
            this.layoutGroupTradeActionResults.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeActionResults.Name = "layoutGroupTradeActionResults";
            this.layoutGroupTradeActionResults.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeActionResults.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeActionResults.Size = new System.Drawing.Size(1375, 531);
            this.layoutGroupTradeActionResults.Text = "Results";
            // 
            // lciTabs
            // 
            this.lciTabs.Control = this.tbResults;
            this.lciTabs.CustomizationFormText = "lciTabs";
            this.lciTabs.Location = new System.Drawing.Point(0, 0);
            this.lciTabs.Name = "lciTabs";
            this.lciTabs.Size = new System.Drawing.Size(1369, 506);
            this.lciTabs.Text = "lciTabs";
            this.lciTabs.TextSize = new System.Drawing.Size(0, 0);
            this.lciTabs.TextToControlDistance = 0;
            this.lciTabs.TextVisible = false;
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.Images.SetKeyName(0, "company.ico");
            this.imageList16.Images.SetKeyName(1, "counterparty.ico");
            this.imageList16.Images.SetKeyName(2, "market.ico");
            this.imageList16.Images.SetKeyName(3, "trader.ico");
            this.imageList16.Images.SetKeyName(4, "vessel.ico");
            this.imageList16.Images.SetKeyName(5, "book.ico");
            this.imageList16.Images.SetKeyName(6, "pool.ico");
            this.imageList16.Images.SetKeyName(7, "vesselpool.ico");
            this.imageList16.Images.SetKeyName(8, "broker.ico");
            this.imageList16.Images.SetKeyName(9, "bulletblue.ico");
            this.imageList16.Images.SetKeyName(10, "house.ico");
            this.imageList16.Images.SetKeyName(11, "bullet_ball_glass_grey.ico");
            this.imageList16.Images.SetKeyName(12, "bullet_ball_glass_yellow.ico");
            this.imageList16.Images.SetKeyName(13, "bullet_ball_glass_green.ico");
            this.imageList16.Images.SetKeyName(14, "bullet_ball_glass_red.ico");
            this.imageList16.Images.SetKeyName(15, "calendar.ico");
            this.imageList16.Images.SetKeyName(16, "briefcase.ico");
            this.imageList16.Images.SetKeyName(17, "businessmen.ico");
            // 
            // layoutControlRoot
            // 
            this.layoutControlRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRoot.Name = "layoutControlRoot";
            this.layoutControlRoot.Root = this.layoutControlGroup1;
            this.layoutControlRoot.Size = new System.Drawing.Size(310, 97);
            this.layoutControlRoot.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(310, 97);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem1";
            this.splitterItem2.Location = new System.Drawing.Point(0, 0);
            this.splitterItem2.Name = "splitterItem1";
            this.splitterItem2.Size = new System.Drawing.Size(1378, 606);
            // 
            // GeneratePOSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1219, 677);
            this.Controls.Add(this.layoutRoot);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "GeneratePOSForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewTradesForm_FormClosing);
            this.Load += new System.EventHandler(this.MainControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbResults)).EndInit();
            this.tbResults.ResumeLayout(false);
            this.tbpgPosition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkReverseNumberSign.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResultsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgPositionResultsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGridPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsAggregationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionResultsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcibtnExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.tbpgInitialMargin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInitialMargin)).EndInit();
            this.tbpgIMNasdaq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcIMNasdaq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgIMNasdaq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGeneratePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateCommissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTradeResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTabs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTrades;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeDetails;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeActionResults;
        private DevExpress.XtraEditors.SimpleButton btnGeneratePosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemGeneratePosition;
        private DevExpress.Utils.ImageCollection imageList16;
        private DevExpress.XtraEditors.SpinEdit txtMarketSensitivityValue;
        private DevExpress.XtraEditors.ComboBoxEdit cmbMarketSensitivityType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControl layoutControlRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit dtpFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutFilterCurveDate;
        private DevExpress.XtraEditors.CheckEdit chkUseSpotValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutUseSpotValues;
        private DevExpress.XtraEditors.CheckEdit chkCalculateSums;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateSums;
        private DevExpress.XtraEditors.CheckEdit chkCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTradeResults;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrades;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LabelControl lblIsOptionalPeriod;
        private DevExpress.XtraEditors.LabelControl lblIsMinimumPeriod;
        private DevExpress.XtraEditors.LabelControl lblIsDraft;
        private DevExpress.XtraEditors.LabelControl lblPeriodTo;
        private DevExpress.XtraEditors.LabelControl lblPeriodFrom;
        private DevExpress.XtraEditors.LabelControl lblEntity;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraTab.XtraTabControl tbResults;
        private DevExpress.XtraTab.XtraTabPage tbpgPosition;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.RadioGroup rdgPositionResultsType;
        private DevExpress.XtraEditors.RadioGroup rdgResultsAggregationType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutResultsAggregationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionResultsType;
        private DevExpress.XtraLayout.LayoutControlItem lciTabs;
        private DevExpress.XtraEditors.SimpleButton btnExportResults;
        private DevExpress.XtraLayout.LayoutControlItem lcibtnExport;
        private DevExpress.XtraGrid.GridControl gridTradeActionResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridTradeActionResultsView;
        private DevExpress.XtraLayout.LayoutControlItem lciGridPosition;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraTab.XtraTabPage tbpgInitialMargin;
        private DevExpress.XtraLayout.LayoutControl lcInitialMargin;
        private DevExpress.XtraLayout.LayoutControlGroup lcgInitialMargin;
        private DevExpress.XtraLayout.LayoutControl lcIMNasdaq;
        private DevExpress.XtraLayout.LayoutControlGroup lcgIMNasdaq;
        private DevExpress.XtraTab.XtraTabPage tbpgIMNasdaq;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraEditors.SimpleButton btnExportToCSV;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.LabelControl lblOnlyNonZeroTotalDays;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.CheckEdit chkReverseNumberSign;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}
