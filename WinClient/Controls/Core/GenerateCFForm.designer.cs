﻿namespace Exis.WinClient.Controls.Core
{
    partial class GenerateCFForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenerateCFForm));
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.chkCFIRR = new DevExpress.XtraEditors.CheckEdit();
            this.txtCFValuationDiscountRate = new DevExpress.XtraEditors.SpinEdit();
            this.chkCFValuation = new DevExpress.XtraEditors.CheckEdit();
            this.chkCFBreakEvenScrap = new DevExpress.XtraEditors.CheckEdit();
            this.chkCFBreakEvenEquity = new DevExpress.XtraEditors.CheckEdit();
            this.chkCFBreakEvenBalloon = new DevExpress.XtraEditors.CheckEdit();
            this.chkCFBreakEven = new DevExpress.XtraEditors.CheckEdit();
            this.lookupCFModel = new DevExpress.XtraEditors.LookUpEdit();
            this.rdgPositionResultsType = new DevExpress.XtraEditors.RadioGroup();
            this.lblIsOptionalPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblIsMinimumPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblIsDraft = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodTo = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodFrom = new DevExpress.XtraEditors.LabelControl();
            this.lblEntity = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkCalculateCommissions = new DevExpress.XtraEditors.CheckEdit();
            this.chkCalculateSums = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionPremium = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseSpotValues = new DevExpress.XtraEditors.CheckEdit();
            this.dtpFilterCurveDate = new DevExpress.XtraEditors.DateEdit();
            this.rdgResultsAggregationType = new DevExpress.XtraEditors.RadioGroup();
            this.txtMarketSensitivityValue = new DevExpress.XtraEditors.SpinEdit();
            this.cmbMarketSensitivityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPositionMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnExportResults = new DevExpress.XtraEditors.SimpleButton();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnGeneratePosition = new DevExpress.XtraEditors.SimpleButton();
            this.gridTradeActionResults = new DevExpress.XtraGrid.GridControl();
            this.gridTradeActionResultsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutGroupRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemGeneratePosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutFilterCurveDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionPremium = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUseSpotValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupTradeResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeActionResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemTradeActionResults = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutResultsActions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutExportResults = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemCalculateCommissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCalculateSums = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutResultsAggregationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutPositionResultsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList16 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlRoot = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblOnlyNonZeroTotalDays = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFIRR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCFValuationDiscountRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFValuation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEvenScrap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEvenEquity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEvenBalloon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEven.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCFModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgPositionResultsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResultsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGeneratePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTradeResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExportResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateCommissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsAggregationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionResultsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.AllowCustomizationMenu = false;
            this.layoutRoot.Controls.Add(this.lblOnlyNonZeroTotalDays);
            this.layoutRoot.Controls.Add(this.chkCFIRR);
            this.layoutRoot.Controls.Add(this.txtCFValuationDiscountRate);
            this.layoutRoot.Controls.Add(this.chkCFValuation);
            this.layoutRoot.Controls.Add(this.chkCFBreakEvenScrap);
            this.layoutRoot.Controls.Add(this.chkCFBreakEvenEquity);
            this.layoutRoot.Controls.Add(this.chkCFBreakEvenBalloon);
            this.layoutRoot.Controls.Add(this.chkCFBreakEven);
            this.layoutRoot.Controls.Add(this.lookupCFModel);
            this.layoutRoot.Controls.Add(this.rdgPositionResultsType);
            this.layoutRoot.Controls.Add(this.lblIsOptionalPeriod);
            this.layoutRoot.Controls.Add(this.lblIsMinimumPeriod);
            this.layoutRoot.Controls.Add(this.lblIsDraft);
            this.layoutRoot.Controls.Add(this.lblPeriodTo);
            this.layoutRoot.Controls.Add(this.lblPeriodFrom);
            this.layoutRoot.Controls.Add(this.lblEntity);
            this.layoutRoot.Controls.Add(this.labelControl2);
            this.layoutRoot.Controls.Add(this.labelControl1);
            this.layoutRoot.Controls.Add(this.chkCalculateCommissions);
            this.layoutRoot.Controls.Add(this.chkCalculateSums);
            this.layoutRoot.Controls.Add(this.chkOptionPremium);
            this.layoutRoot.Controls.Add(this.chkUseSpotValues);
            this.layoutRoot.Controls.Add(this.dtpFilterCurveDate);
            this.layoutRoot.Controls.Add(this.rdgResultsAggregationType);
            this.layoutRoot.Controls.Add(this.txtMarketSensitivityValue);
            this.layoutRoot.Controls.Add(this.cmbMarketSensitivityType);
            this.layoutRoot.Controls.Add(this.cmbPositionMethod);
            this.layoutRoot.Controls.Add(this.btnExportResults);
            this.layoutRoot.Controls.Add(this.btnGeneratePosition);
            this.layoutRoot.Controls.Add(this.gridTradeActionResults);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1643, 0, 426, 731);
            this.layoutRoot.Root = this.layoutGroupRoot;
            this.layoutRoot.Size = new System.Drawing.Size(1422, 795);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // chkCFIRR
            // 
            this.chkCFIRR.Location = new System.Drawing.Point(1168, 92);
            this.chkCFIRR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCFIRR.Name = "chkCFIRR";
            this.chkCFIRR.Properties.Caption = "";
            this.chkCFIRR.Size = new System.Drawing.Size(43, 19);
            this.chkCFIRR.StyleController = this.layoutRoot;
            this.chkCFIRR.TabIndex = 43;
            // 
            // txtCFValuationDiscountRate
            // 
            this.txtCFValuationDiscountRate.EditValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtCFValuationDiscountRate.Location = new System.Drawing.Point(171, 118);
            this.txtCFValuationDiscountRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCFValuationDiscountRate.Name = "txtCFValuationDiscountRate";
            this.txtCFValuationDiscountRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtCFValuationDiscountRate.Size = new System.Drawing.Size(123, 22);
            this.txtCFValuationDiscountRate.StyleController = this.layoutRoot;
            this.txtCFValuationDiscountRate.TabIndex = 42;
            // 
            // chkCFValuation
            // 
            this.chkCFValuation.Location = new System.Drawing.Point(1074, 92);
            this.chkCFValuation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCFValuation.Name = "chkCFValuation";
            this.chkCFValuation.Properties.Caption = "";
            this.chkCFValuation.Size = new System.Drawing.Size(41, 19);
            this.chkCFValuation.StyleController = this.layoutRoot;
            this.chkCFValuation.TabIndex = 41;
            // 
            // chkCFBreakEvenScrap
            // 
            this.chkCFBreakEvenScrap.Location = new System.Drawing.Point(988, 92);
            this.chkCFBreakEvenScrap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCFBreakEvenScrap.Name = "chkCFBreakEvenScrap";
            this.chkCFBreakEvenScrap.Properties.Caption = "";
            this.chkCFBreakEvenScrap.Size = new System.Drawing.Size(31, 19);
            this.chkCFBreakEvenScrap.StyleController = this.layoutRoot;
            this.chkCFBreakEvenScrap.TabIndex = 40;
            // 
            // chkCFBreakEvenEquity
            // 
            this.chkCFBreakEvenEquity.Location = new System.Drawing.Point(440, 92);
            this.chkCFBreakEvenEquity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCFBreakEvenEquity.Name = "chkCFBreakEvenEquity";
            this.chkCFBreakEvenEquity.Properties.Caption = "";
            this.chkCFBreakEvenEquity.Size = new System.Drawing.Size(52, 19);
            this.chkCFBreakEvenEquity.StyleController = this.layoutRoot;
            this.chkCFBreakEvenEquity.TabIndex = 39;
            // 
            // chkCFBreakEvenBalloon
            // 
            this.chkCFBreakEvenBalloon.Location = new System.Drawing.Point(859, 92);
            this.chkCFBreakEvenBalloon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCFBreakEvenBalloon.Name = "chkCFBreakEvenBalloon";
            this.chkCFBreakEvenBalloon.Properties.Caption = "";
            this.chkCFBreakEvenBalloon.Size = new System.Drawing.Size(40, 19);
            this.chkCFBreakEvenBalloon.StyleController = this.layoutRoot;
            this.chkCFBreakEvenBalloon.TabIndex = 38;
            // 
            // chkCFBreakEven
            // 
            this.chkCFBreakEven.Location = new System.Drawing.Point(693, 92);
            this.chkCFBreakEven.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCFBreakEven.Name = "chkCFBreakEven";
            this.chkCFBreakEven.Properties.Caption = "";
            this.chkCFBreakEven.Size = new System.Drawing.Size(69, 19);
            this.chkCFBreakEven.StyleController = this.layoutRoot;
            this.chkCFBreakEven.TabIndex = 37;
            // 
            // lookupCFModel
            // 
            this.lookupCFModel.Location = new System.Drawing.Point(79, 92);
            this.lookupCFModel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCFModel.Name = "lookupCFModel";
            this.lookupCFModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupCFModel.Size = new System.Drawing.Size(271, 22);
            this.lookupCFModel.StyleController = this.layoutRoot;
            this.lookupCFModel.TabIndex = 36;
            // 
            // rdgPositionResultsType
            // 
            this.rdgPositionResultsType.Location = new System.Drawing.Point(604, 186);
            this.rdgPositionResultsType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdgPositionResultsType.Name = "rdgPositionResultsType";
            this.rdgPositionResultsType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Days"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Vessels"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Tonnes")});
            this.rdgPositionResultsType.Size = new System.Drawing.Size(419, 30);
            this.rdgPositionResultsType.StyleController = this.layoutRoot;
            this.rdgPositionResultsType.TabIndex = 35;
            this.rdgPositionResultsType.SelectedIndexChanged += new System.EventHandler(this.RdgPositionResultsTypeSelectedIndexChanged);
            // 
            // lblIsOptionalPeriod
            // 
            this.lblIsOptionalPeriod.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblIsOptionalPeriod.Location = new System.Drawing.Point(1113, 34);
            this.lblIsOptionalPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblIsOptionalPeriod.Name = "lblIsOptionalPeriod";
            this.lblIsOptionalPeriod.Size = new System.Drawing.Size(61, 21);
            this.lblIsOptionalPeriod.StyleController = this.layoutRoot;
            this.lblIsOptionalPeriod.TabIndex = 34;
            // 
            // lblIsMinimumPeriod
            // 
            this.lblIsMinimumPeriod.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblIsMinimumPeriod.Location = new System.Drawing.Point(938, 34);
            this.lblIsMinimumPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblIsMinimumPeriod.Name = "lblIsMinimumPeriod";
            this.lblIsMinimumPeriod.Size = new System.Drawing.Size(61, 21);
            this.lblIsMinimumPeriod.StyleController = this.layoutRoot;
            this.lblIsMinimumPeriod.TabIndex = 33;
            // 
            // lblIsDraft
            // 
            this.lblIsDraft.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblIsDraft.Location = new System.Drawing.Point(773, 34);
            this.lblIsDraft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblIsDraft.Name = "lblIsDraft";
            this.lblIsDraft.Size = new System.Drawing.Size(61, 21);
            this.lblIsDraft.StyleController = this.layoutRoot;
            this.lblIsDraft.TabIndex = 32;
            // 
            // lblPeriodTo
            // 
            this.lblPeriodTo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblPeriodTo.Location = new System.Drawing.Point(638, 34);
            this.lblPeriodTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblPeriodTo.Name = "lblPeriodTo";
            this.lblPeriodTo.Size = new System.Drawing.Size(76, 21);
            this.lblPeriodTo.StyleController = this.layoutRoot;
            this.lblPeriodTo.TabIndex = 31;
            // 
            // lblPeriodFrom
            // 
            this.lblPeriodFrom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblPeriodFrom.Location = new System.Drawing.Point(466, 34);
            this.lblPeriodFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblPeriodFrom.Name = "lblPeriodFrom";
            this.lblPeriodFrom.Size = new System.Drawing.Size(93, 21);
            this.lblPeriodFrom.StyleController = this.layoutRoot;
            this.lblPeriodFrom.TabIndex = 30;
            // 
            // lblEntity
            // 
            this.lblEntity.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblEntity.Location = new System.Drawing.Point(246, 34);
            this.lblEntity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblEntity.Name = "lblEntity";
            this.lblEntity.Size = new System.Drawing.Size(141, 21);
            this.lblEntity.StyleController = this.layoutRoot;
            this.lblEntity.TabIndex = 29;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.Location = new System.Drawing.Point(12, 65);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(0, 60);
            this.labelControl2.StyleController = this.layoutRoot;
            this.labelControl2.TabIndex = 28;
            this.labelControl2.Text = "CF Filters:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.labelControl1.Location = new System.Drawing.Point(12, 34);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(0, 0, 17, 0);
            this.labelControl1.Size = new System.Drawing.Size(185, 20);
            this.labelControl1.StyleController = this.layoutRoot;
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = "Filter Defined in View Trades:";
            // 
            // chkCalculateCommissions
            // 
            this.chkCalculateCommissions.Location = new System.Drawing.Point(1212, 191);
            this.chkCalculateCommissions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCalculateCommissions.Name = "chkCalculateCommissions";
            this.chkCalculateCommissions.Properties.Caption = "";
            this.chkCalculateCommissions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCalculateCommissions.Size = new System.Drawing.Size(44, 19);
            this.chkCalculateCommissions.StyleController = this.layoutRoot;
            this.chkCalculateCommissions.TabIndex = 22;
            // 
            // chkCalculateSums
            // 
            this.chkCalculateSums.Location = new System.Drawing.Point(1077, 191);
            this.chkCalculateSums.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCalculateSums.Name = "chkCalculateSums";
            this.chkCalculateSums.Properties.Caption = "";
            this.chkCalculateSums.Size = new System.Drawing.Size(46, 19);
            this.chkCalculateSums.StyleController = this.layoutRoot;
            this.chkCalculateSums.TabIndex = 21;
            // 
            // chkOptionPremium
            // 
            this.chkOptionPremium.Location = new System.Drawing.Point(580, 92);
            this.chkOptionPremium.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkOptionPremium.Name = "chkOptionPremium";
            this.chkOptionPremium.Properties.Caption = "";
            this.chkOptionPremium.Size = new System.Drawing.Size(61, 19);
            this.chkOptionPremium.StyleController = this.layoutRoot;
            this.chkOptionPremium.TabIndex = 21;
            // 
            // chkUseSpotValues
            // 
            this.chkUseSpotValues.Location = new System.Drawing.Point(1267, 92);
            this.chkUseSpotValues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkUseSpotValues.Name = "chkUseSpotValues";
            this.chkUseSpotValues.Properties.Caption = "";
            this.chkUseSpotValues.Size = new System.Drawing.Size(40, 19);
            this.chkUseSpotValues.StyleController = this.layoutRoot;
            this.chkUseSpotValues.TabIndex = 20;
            // 
            // dtpFilterCurveDate
            // 
            this.dtpFilterCurveDate.EditValue = null;
            this.dtpFilterCurveDate.Location = new System.Drawing.Point(80, 68);
            this.dtpFilterCurveDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpFilterCurveDate.Name = "dtpFilterCurveDate";
            this.dtpFilterCurveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterCurveDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterCurveDate.Size = new System.Drawing.Size(230, 22);
            this.dtpFilterCurveDate.StyleController = this.layoutRoot;
            this.dtpFilterCurveDate.TabIndex = 10;
            // 
            // rdgResultsAggregationType
            // 
            this.rdgResultsAggregationType.Location = new System.Drawing.Point(94, 186);
            this.rdgResultsAggregationType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdgResultsAggregationType.Name = "rdgResultsAggregationType";
            this.rdgResultsAggregationType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Month"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Quarter"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Calendar")});
            this.rdgResultsAggregationType.Size = new System.Drawing.Size(430, 30);
            this.rdgResultsAggregationType.StyleController = this.layoutRoot;
            this.rdgResultsAggregationType.TabIndex = 23;
            this.rdgResultsAggregationType.SelectedIndexChanged += new System.EventHandler(this.RdgResultsAggregationTypeSelectedIndexChanged);
            // 
            // txtMarketSensitivityValue
            // 
            this.txtMarketSensitivityValue.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtMarketSensitivityValue.Location = new System.Drawing.Point(954, 68);
            this.txtMarketSensitivityValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMarketSensitivityValue.Name = "txtMarketSensitivityValue";
            this.txtMarketSensitivityValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMarketSensitivityValue.Properties.Mask.EditMask = "D";
            this.txtMarketSensitivityValue.Properties.MaxLength = 7;
            this.txtMarketSensitivityValue.Size = new System.Drawing.Size(353, 22);
            this.txtMarketSensitivityValue.StyleController = this.layoutRoot;
            this.txtMarketSensitivityValue.TabIndex = 9;
            // 
            // cmbMarketSensitivityType
            // 
            this.cmbMarketSensitivityType.Location = new System.Drawing.Point(732, 68);
            this.cmbMarketSensitivityType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbMarketSensitivityType.Name = "cmbMarketSensitivityType";
            this.cmbMarketSensitivityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbMarketSensitivityType.Properties.Items.AddRange(new object[] {
            "None",
            "Absolute",
            "Percentage",
            "Stress"});
            this.cmbMarketSensitivityType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbMarketSensitivityType.Size = new System.Drawing.Size(95, 22);
            this.cmbMarketSensitivityType.StyleController = this.layoutRoot;
            this.cmbMarketSensitivityType.TabIndex = 8;
            this.cmbMarketSensitivityType.SelectedValueChanged += new System.EventHandler(this.CmbMarketSensitivityTypeSelectedValueChanged);
            // 
            // cmbPositionMethod
            // 
            this.cmbPositionMethod.Location = new System.Drawing.Point(399, 68);
            this.cmbPositionMethod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPositionMethod.Name = "cmbPositionMethod";
            this.cmbPositionMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPositionMethod.Properties.Items.AddRange(new object[] {
            "Static",
            "Dynamic",
            "Delta"});
            this.cmbPositionMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPositionMethod.Size = new System.Drawing.Size(206, 22);
            this.cmbPositionMethod.StyleController = this.layoutRoot;
            this.cmbPositionMethod.TabIndex = 22;
            this.cmbPositionMethod.SelectedValueChanged += new System.EventHandler(this.cmbPositionMethod_SelectedValueChanged);
            // 
            // btnExportResults
            // 
            this.btnExportResults.ImageIndex = 8;
            this.btnExportResults.ImageList = this.imageList24;
            this.btnExportResults.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnExportResults.Location = new System.Drawing.Point(1327, 185);
            this.btnExportResults.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExportResults.Name = "btnExportResults";
            this.btnExportResults.Size = new System.Drawing.Size(81, 32);
            this.btnExportResults.StyleController = this.layoutRoot;
            this.btnExportResults.TabIndex = 13;
            this.btnExportResults.Text = "Export";
            this.btnExportResults.Click += new System.EventHandler(this.btnExportResults_Click);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            this.imageList24.Images.SetKeyName(6, "gantt-chart.ico");
            this.imageList24.Images.SetKeyName(7, "line-chart.ico");
            this.imageList24.Images.SetKeyName(8, "export24x24.ico");
            this.imageList24.Images.SetKeyName(9, "view24x24.ico");
            this.imageList24.Images.SetKeyName(10, "activate.ico");
            this.imageList24.Images.SetKeyName(11, "deactivate.ico");
            this.imageList24.Images.SetKeyName(12, "text_sum.ico");
            // 
            // btnGeneratePosition
            // 
            this.btnGeneratePosition.ImageIndex = 7;
            this.btnGeneratePosition.ImageList = this.imageList24;
            this.btnGeneratePosition.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGeneratePosition.Location = new System.Drawing.Point(1316, 63);
            this.btnGeneratePosition.Margin = new System.Windows.Forms.Padding(3, 12, 3, 4);
            this.btnGeneratePosition.Name = "btnGeneratePosition";
            this.btnGeneratePosition.Size = new System.Drawing.Size(96, 82);
            this.btnGeneratePosition.StyleController = this.layoutRoot;
            this.btnGeneratePosition.TabIndex = 8;
            this.btnGeneratePosition.Text = "Calculate";
            this.btnGeneratePosition.Click += new System.EventHandler(this.btnCalculateClick);
            // 
            // gridTradeActionResults
            // 
            this.gridTradeActionResults.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridTradeActionResults.Location = new System.Drawing.Point(9, 226);
            this.gridTradeActionResults.MainView = this.gridTradeActionResultsView;
            this.gridTradeActionResults.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridTradeActionResults.Name = "gridTradeActionResults";
            this.gridTradeActionResults.Size = new System.Drawing.Size(1404, 560);
            this.gridTradeActionResults.TabIndex = 6;
            this.gridTradeActionResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridTradeActionResultsView});
            // 
            // gridTradeActionResultsView
            // 
            this.gridTradeActionResultsView.GridControl = this.gridTradeActionResults;
            this.gridTradeActionResultsView.Name = "gridTradeActionResultsView";
            this.gridTradeActionResultsView.OptionsBehavior.Editable = false;
            this.gridTradeActionResultsView.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridTradeActionResultsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridTradeActionResultsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridTradeActionResultsView.OptionsView.ShowFooter = true;
            this.gridTradeActionResultsView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.GridTradeActionResultsViewCustomSummaryCalculate);
            this.gridTradeActionResultsView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridTradeActionResultsView_CustomUnboundColumnData);
            // 
            // layoutGroupRoot
            // 
            this.layoutGroupRoot.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutGroupRoot.GroupBordersVisible = false;
            this.layoutGroupRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTrades});
            this.layoutGroupRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupRoot.Name = "Root";
            this.layoutGroupRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupRoot.Size = new System.Drawing.Size(1422, 795);
            this.layoutGroupRoot.Text = "Root";
            this.layoutGroupRoot.TextVisible = false;
            // 
            // layoutGroupTrades
            // 
            this.layoutGroupTrades.CustomizationFormText = "Trades";
            this.layoutGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupTrades,
            this.layoutControlGroupTradeResults});
            this.layoutGroupTrades.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTrades.Name = "layoutGroupTrades";
            this.layoutGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTrades.Size = new System.Drawing.Size(1422, 795);
            this.layoutGroupTrades.Text = "Trades";
            this.layoutGroupTrades.TextVisible = false;
            // 
            // layoutControlGroupTrades
            // 
            this.layoutControlGroupTrades.CustomizationFormText = "Filters";
            this.layoutControlGroupTrades.ExpandButtonVisible = true;
            this.layoutControlGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeDetails});
            this.layoutControlGroupTrades.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTrades.Name = "layoutControlGroupTrades";
            this.layoutControlGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Size = new System.Drawing.Size(1416, 149);
            this.layoutControlGroupTrades.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Text = "Filters";
            // 
            // layoutGroupTradeDetails
            // 
            this.layoutGroupTradeDetails.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupTradeDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlGroup3,
            this.layoutControlGroup2});
            this.layoutGroupTradeDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeDetails.Name = "layoutGroupTradeDetails";
            this.layoutGroupTradeDetails.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeDetails.Size = new System.Drawing.Size(1414, 125);
            this.layoutGroupTradeDetails.Text = "Details";
            this.layoutGroupTradeDetails.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControl2;
            this.layoutControlItem5.CustomizationFormText = "MTM Filters:";
            this.layoutControlItem5.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 70);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(1, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(1, 88);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "MTM Filters:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemGeneratePosition,
            this.layoutControlGroup5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(1, 31);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1407, 88);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutItemGeneratePosition
            // 
            this.layoutItemGeneratePosition.Control = this.btnGeneratePosition;
            this.layoutItemGeneratePosition.CustomizationFormText = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.Location = new System.Drawing.Point(1305, 0);
            this.layoutItemGeneratePosition.MaxSize = new System.Drawing.Size(100, 100);
            this.layoutItemGeneratePosition.MinSize = new System.Drawing.Size(100, 36);
            this.layoutItemGeneratePosition.Name = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.Size = new System.Drawing.Size(100, 86);
            this.layoutItemGeneratePosition.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemGeneratePosition.Text = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutItemGeneratePosition.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemGeneratePosition.TextToControlDistance = 0;
            this.layoutItemGeneratePosition.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutFilterCurveDate,
            this.layoutPositionMethod,
            this.layoutMarketSensitivityType,
            this.layoutMarketSensitivityValue,
            this.layoutOptionPremium,
            this.layoutControlItem3,
            this.layoutControlItem12,
            this.layoutControlItem15,
            this.layoutControlItem14,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem16,
            this.layoutControlItem13,
            this.layoutUseSpotValues,
            this.emptySpaceItem3});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1305, 86);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutFilterCurveDate
            // 
            this.layoutFilterCurveDate.Control = this.dtpFilterCurveDate;
            this.layoutFilterCurveDate.CustomizationFormText = "Curve Date:";
            this.layoutFilterCurveDate.Location = new System.Drawing.Point(0, 0);
            this.layoutFilterCurveDate.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutFilterCurveDate.MinSize = new System.Drawing.Size(118, 24);
            this.layoutFilterCurveDate.Name = "layoutFilterCurveDate";
            this.layoutFilterCurveDate.Size = new System.Drawing.Size(298, 24);
            this.layoutFilterCurveDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutFilterCurveDate.Text = "Curve Date:";
            this.layoutFilterCurveDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutFilterCurveDate.TextSize = new System.Drawing.Size(59, 13);
            this.layoutFilterCurveDate.TextToControlDistance = 5;
            // 
            // layoutPositionMethod
            // 
            this.layoutPositionMethod.Control = this.cmbPositionMethod;
            this.layoutPositionMethod.CustomizationFormText = "Position Method:";
            this.layoutPositionMethod.Location = new System.Drawing.Point(298, 0);
            this.layoutPositionMethod.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutPositionMethod.MinSize = new System.Drawing.Size(139, 24);
            this.layoutPositionMethod.Name = "layoutPositionMethod";
            this.layoutPositionMethod.Size = new System.Drawing.Size(295, 24);
            this.layoutPositionMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPositionMethod.Text = "Position Method:";
            this.layoutPositionMethod.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutPositionMethod.TextSize = new System.Drawing.Size(80, 13);
            this.layoutPositionMethod.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityType
            // 
            this.layoutMarketSensitivityType.Control = this.cmbMarketSensitivityType;
            this.layoutMarketSensitivityType.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.Location = new System.Drawing.Point(593, 0);
            this.layoutMarketSensitivityType.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityType.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityType.Name = "layoutMarketSensitivityType";
            this.layoutMarketSensitivityType.Size = new System.Drawing.Size(222, 24);
            this.layoutMarketSensitivityType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityType.Text = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityType.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityType.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityValue
            // 
            this.layoutMarketSensitivityValue.Control = this.txtMarketSensitivityValue;
            this.layoutMarketSensitivityValue.CustomizationFormText = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.Location = new System.Drawing.Point(815, 0);
            this.layoutMarketSensitivityValue.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityValue.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityValue.Name = "layoutMarketSensitivityValue";
            this.layoutMarketSensitivityValue.Size = new System.Drawing.Size(480, 24);
            this.layoutMarketSensitivityValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityValue.Text = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityValue.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityValue.TextToControlDistance = 5;
            // 
            // layoutOptionPremium
            // 
            this.layoutOptionPremium.Control = this.chkOptionPremium;
            this.layoutOptionPremium.CustomizationFormText = "Option Premium:";
            this.layoutOptionPremium.Location = new System.Drawing.Point(480, 24);
            this.layoutOptionPremium.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionPremium.MinSize = new System.Drawing.Size(108, 24);
            this.layoutOptionPremium.Name = "layoutOptionPremium";
            this.layoutOptionPremium.Size = new System.Drawing.Size(149, 26);
            this.layoutOptionPremium.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionPremium.Text = "Option Premium:";
            this.layoutOptionPremium.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionPremium.TextSize = new System.Drawing.Size(79, 13);
            this.layoutOptionPremium.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lookupCFModel;
            this.layoutControlItem3.CustomizationFormText = "CF Model:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(338, 26);
            this.layoutControlItem3.Text = "CF Model:";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(58, 16);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.chkCFBreakEvenEquity;
            this.layoutControlItem12.CustomizationFormText = "CF B/E Equity:";
            this.layoutControlItem12.Location = new System.Drawing.Point(338, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(142, 26);
            this.layoutControlItem12.Text = "CF B/E Equity:";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(81, 16);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtCFValuationDiscountRate;
            this.layoutControlItem15.CustomizationFormText = "CFV Discount Rate % p.a.:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(282, 26);
            this.layoutControlItem15.Text = "CFV Discount Rate % p.a.:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(152, 16);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.chkCFBreakEven;
            this.layoutControlItem14.CustomizationFormText = "CF B/E:";
            this.layoutControlItem14.Location = new System.Drawing.Point(629, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(121, 26);
            this.layoutControlItem14.Text = "CF B/E:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(43, 16);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.chkCFValuation;
            this.layoutControlItem10.CustomizationFormText = "CF Val.:";
            this.layoutControlItem10.Location = new System.Drawing.Point(1007, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem10.Text = "CF Val.:";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(46, 16);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.chkCFBreakEvenBalloon;
            this.layoutControlItem11.CustomizationFormText = "CF B/E Balloon:";
            this.layoutControlItem11.Location = new System.Drawing.Point(750, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem11.Text = "CF B/E Balloon:";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(88, 16);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.chkCFBreakEvenScrap;
            this.layoutControlItem16.CustomizationFormText = "CF B/E Scrap:";
            this.layoutControlItem16.Location = new System.Drawing.Point(887, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(120, 26);
            this.layoutControlItem16.Text = "CF B/E Scrap:";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 16);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.chkCFIRR;
            this.layoutControlItem13.CustomizationFormText = "CF IRR:";
            this.layoutControlItem13.Location = new System.Drawing.Point(1103, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem13.Text = "CF IRR:";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(44, 16);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutUseSpotValues
            // 
            this.layoutUseSpotValues.Control = this.chkUseSpotValues;
            this.layoutUseSpotValues.CustomizationFormText = "Use Spot:";
            this.layoutUseSpotValues.Location = new System.Drawing.Point(1199, 24);
            this.layoutUseSpotValues.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutUseSpotValues.MinSize = new System.Drawing.Size(76, 24);
            this.layoutUseSpotValues.Name = "layoutUseSpotValues";
            this.layoutUseSpotValues.Size = new System.Drawing.Size(96, 26);
            this.layoutUseSpotValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUseSpotValues.Text = "Use Spot:";
            this.layoutUseSpotValues.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutUseSpotValues.TextSize = new System.Drawing.Size(47, 13);
            this.layoutUseSpotValues.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(282, 50);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1013, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "POS Filters:";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.layoutControlItem17});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1408, 31);
            this.layoutControlGroup2.Text = "POS Filters:";
            this.layoutControlGroup2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(181, 18);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(189, 25);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            this.layoutControlItem4.TrimClientAreaToControl = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.lblEntity;
            this.layoutControlItem1.CustomizationFormText = "Book/XX Selected:";
            this.layoutControlItem1.Location = new System.Drawing.Point(189, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(141, 17);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(190, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Enitity:";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(35, 13);
            this.layoutControlItem1.TextToControlDistance = 10;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.lblPeriodFrom;
            this.layoutControlItem2.CustomizationFormText = "Period From:";
            this.layoutControlItem2.Location = new System.Drawing.Point(379, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(155, 17);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(172, 25);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Period From:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem2.TextToControlDistance = 10;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.lblIsDraft;
            this.layoutControlItem7.CustomizationFormText = "Is Draft:";
            this.layoutControlItem7.Location = new System.Drawing.Point(706, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(120, 17);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(120, 25);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Is Draft:";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(45, 13);
            this.layoutControlItem7.TextToControlDistance = 10;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.lblPeriodTo;
            this.layoutControlItem6.CustomizationFormText = "Period To:";
            this.layoutControlItem6.Location = new System.Drawing.Point(551, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(155, 17);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(155, 25);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Period To:";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem6.TextToControlDistance = 10;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.lblIsMinimumPeriod;
            this.layoutControlItem8.CustomizationFormText = "Is Minimum Period:";
            this.layoutControlItem8.Location = new System.Drawing.Point(826, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(165, 17);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(165, 25);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Is Minimum Period:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            this.layoutControlItem8.TextToControlDistance = 10;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.lblIsOptionalPeriod;
            this.layoutControlItem9.CustomizationFormText = "Is Optional Period:";
            this.layoutControlItem9.Location = new System.Drawing.Point(991, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(175, 25);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "Is Optional Period:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem9.TextToControlDistance = 10;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(1401, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1, 25);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupTradeResults
            // 
            this.layoutControlGroupTradeResults.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroupTradeResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeActionResults});
            this.layoutControlGroupTradeResults.Location = new System.Drawing.Point(0, 149);
            this.layoutControlGroupTradeResults.Name = "layoutControlGroupTradeResults";
            this.layoutControlGroupTradeResults.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTradeResults.Size = new System.Drawing.Size(1416, 640);
            this.layoutControlGroupTradeResults.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTradeResults.Text = "layoutControlGroupTradeResults";
            this.layoutControlGroupTradeResults.TextVisible = false;
            // 
            // layoutGroupTradeActionResults
            // 
            this.layoutGroupTradeActionResults.CustomizationFormText = "Action Results";
            this.layoutGroupTradeActionResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemTradeActionResults,
            this.layoutResultsActions,
            this.layoutControlGroup4});
            this.layoutGroupTradeActionResults.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeActionResults.Name = "layoutGroupTradeActionResults";
            this.layoutGroupTradeActionResults.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeActionResults.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeActionResults.Size = new System.Drawing.Size(1414, 638);
            this.layoutGroupTradeActionResults.Text = "Results";
            // 
            // layoutItemTradeActionResults
            // 
            this.layoutItemTradeActionResults.Control = this.gridTradeActionResults;
            this.layoutItemTradeActionResults.CustomizationFormText = "layoutItemTradeActionResults";
            this.layoutItemTradeActionResults.Location = new System.Drawing.Point(0, 46);
            this.layoutItemTradeActionResults.MinSize = new System.Drawing.Size(50, 204);
            this.layoutItemTradeActionResults.Name = "layoutItemTradeActionResults";
            this.layoutItemTradeActionResults.Size = new System.Drawing.Size(1408, 564);
            this.layoutItemTradeActionResults.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemTradeActionResults.Text = "layoutItemTradeActionResults";
            this.layoutItemTradeActionResults.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemTradeActionResults.TextToControlDistance = 0;
            this.layoutItemTradeActionResults.TextVisible = false;
            // 
            // layoutResultsActions
            // 
            this.layoutResultsActions.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutResultsActions.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutResultsActions.AppearanceGroup.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutResultsActions.CustomizationFormText = "Actions";
            this.layoutResultsActions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutExportResults});
            this.layoutResultsActions.Location = new System.Drawing.Point(1313, 0);
            this.layoutResultsActions.Name = "layoutResultsActions";
            this.layoutResultsActions.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutResultsActions.Size = new System.Drawing.Size(95, 46);
            this.layoutResultsActions.Text = "Actions";
            this.layoutResultsActions.TextVisible = false;
            // 
            // layoutExportResults
            // 
            this.layoutExportResults.Control = this.btnExportResults;
            this.layoutExportResults.CustomizationFormText = "layoutExportResults";
            this.layoutExportResults.Location = new System.Drawing.Point(0, 0);
            this.layoutExportResults.MaxSize = new System.Drawing.Size(85, 40);
            this.layoutExportResults.MinSize = new System.Drawing.Size(80, 36);
            this.layoutExportResults.Name = "layoutExportResults";
            this.layoutExportResults.Size = new System.Drawing.Size(85, 36);
            this.layoutExportResults.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutExportResults.Text = "layoutExportResults";
            this.layoutExportResults.TextSize = new System.Drawing.Size(0, 0);
            this.layoutExportResults.TextToControlDistance = 0;
            this.layoutExportResults.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemCalculateCommissions,
            this.layoutItemCalculateSums,
            this.layoutResultsAggregationType,
            this.emptySpaceItem1,
            this.layoutPositionResultsType});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1313, 46);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutItemCalculateCommissions
            // 
            this.layoutItemCalculateCommissions.Control = this.chkCalculateCommissions;
            this.layoutItemCalculateCommissions.CustomizationFormText = "Commissions";
            this.layoutItemCalculateCommissions.Location = new System.Drawing.Point(1112, 0);
            this.layoutItemCalculateCommissions.MaxSize = new System.Drawing.Size(0, 34);
            this.layoutItemCalculateCommissions.MinSize = new System.Drawing.Size(80, 34);
            this.layoutItemCalculateCommissions.Name = "layoutItemCalculateCommissions";
            this.layoutItemCalculateCommissions.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 7, 2);
            this.layoutItemCalculateCommissions.Size = new System.Drawing.Size(133, 34);
            this.layoutItemCalculateCommissions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemCalculateCommissions.Text = "Commissions";
            this.layoutItemCalculateCommissions.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateCommissions.TextSize = new System.Drawing.Size(75, 16);
            this.layoutItemCalculateCommissions.TextToControlDistance = 10;
            // 
            // layoutItemCalculateSums
            // 
            this.layoutItemCalculateSums.Control = this.chkCalculateSums;
            this.layoutItemCalculateSums.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutItemCalculateSums.CustomizationFormText = "Calculate Sums:";
            this.layoutItemCalculateSums.Location = new System.Drawing.Point(1012, 0);
            this.layoutItemCalculateSums.MaxSize = new System.Drawing.Size(100, 34);
            this.layoutItemCalculateSums.MinSize = new System.Drawing.Size(100, 34);
            this.layoutItemCalculateSums.Name = "layoutItemCalculateSums";
            this.layoutItemCalculateSums.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 7, 2);
            this.layoutItemCalculateSums.Size = new System.Drawing.Size(100, 34);
            this.layoutItemCalculateSums.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemCalculateSums.Text = "Totals:";
            this.layoutItemCalculateSums.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateSums.TextSize = new System.Drawing.Size(40, 16);
            this.layoutItemCalculateSums.TextToControlDistance = 10;
            // 
            // layoutResultsAggregationType
            // 
            this.layoutResultsAggregationType.Control = this.rdgResultsAggregationType;
            this.layoutResultsAggregationType.CustomizationFormText = "Aggregation Type:";
            this.layoutResultsAggregationType.Location = new System.Drawing.Point(0, 0);
            this.layoutResultsAggregationType.MinSize = new System.Drawing.Size(120, 24);
            this.layoutResultsAggregationType.Name = "layoutResultsAggregationType";
            this.layoutResultsAggregationType.Size = new System.Drawing.Size(513, 34);
            this.layoutResultsAggregationType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutResultsAggregationType.Text = "Aggregation:";
            this.layoutResultsAggregationType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutResultsAggregationType.TextSize = new System.Drawing.Size(74, 16);
            this.layoutResultsAggregationType.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(1245, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(56, 34);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutPositionResultsType
            // 
            this.layoutPositionResultsType.Control = this.rdgPositionResultsType;
            this.layoutPositionResultsType.CustomizationFormText = "Type:";
            this.layoutPositionResultsType.Location = new System.Drawing.Point(513, 0);
            this.layoutPositionResultsType.Name = "layoutPositionResultsType";
            this.layoutPositionResultsType.Size = new System.Drawing.Size(499, 34);
            this.layoutPositionResultsType.Text = "Results Type";
            this.layoutPositionResultsType.TextSize = new System.Drawing.Size(73, 16);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.Images.SetKeyName(0, "company.ico");
            this.imageList16.Images.SetKeyName(1, "counterparty.ico");
            this.imageList16.Images.SetKeyName(2, "market.ico");
            this.imageList16.Images.SetKeyName(3, "trader.ico");
            this.imageList16.Images.SetKeyName(4, "vessel.ico");
            this.imageList16.Images.SetKeyName(5, "book.ico");
            this.imageList16.Images.SetKeyName(6, "pool.ico");
            this.imageList16.Images.SetKeyName(7, "vesselpool.ico");
            this.imageList16.Images.SetKeyName(8, "broker.ico");
            this.imageList16.Images.SetKeyName(9, "bulletblue.ico");
            this.imageList16.Images.SetKeyName(10, "house.ico");
            this.imageList16.Images.SetKeyName(11, "bullet_ball_glass_grey.ico");
            this.imageList16.Images.SetKeyName(12, "bullet_ball_glass_yellow.ico");
            this.imageList16.Images.SetKeyName(13, "bullet_ball_glass_green.ico");
            this.imageList16.Images.SetKeyName(14, "bullet_ball_glass_red.ico");
            this.imageList16.Images.SetKeyName(15, "calendar.ico");
            this.imageList16.Images.SetKeyName(16, "briefcase.ico");
            this.imageList16.Images.SetKeyName(17, "businessmen.ico");
            // 
            // layoutControlRoot
            // 
            this.layoutControlRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRoot.Name = "layoutControlRoot";
            this.layoutControlRoot.Root = this.layoutControlGroup1;
            this.layoutControlRoot.Size = new System.Drawing.Size(310, 97);
            this.layoutControlRoot.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(310, 97);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 0;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1422, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 795);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1422, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 795);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1422, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 795);
            // 
            // lblOnlyNonZeroTotalDays
            // 
            this.lblOnlyNonZeroTotalDays.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.lblOnlyNonZeroTotalDays.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.lblOnlyNonZeroTotalDays.Location = new System.Drawing.Point(1348, 34);
            this.lblOnlyNonZeroTotalDays.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblOnlyNonZeroTotalDays.Name = "lblOnlyNonZeroTotalDays";
            this.lblOnlyNonZeroTotalDays.Size = new System.Drawing.Size(61, 21);
            this.lblOnlyNonZeroTotalDays.StyleController = this.layoutRoot;
            this.lblOnlyNonZeroTotalDays.TabIndex = 44;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem17.Control = this.lblOnlyNonZeroTotalDays;
            this.layoutControlItem17.CustomizationFormText = "Only trades with Total Days > 0:";
            this.layoutControlItem17.Location = new System.Drawing.Point(1166, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(235, 25);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(235, 25);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "Only trades with Total Days > 0:";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(160, 20);
            this.layoutControlItem17.TextToControlDistance = 10;
            // 
            // GenerateCFForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1422, 833);
            this.Controls.Add(this.layoutRoot);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "GenerateCFForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewTradesForm_FormClosing);
            this.Load += new System.EventHandler(this.MainControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkCFIRR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCFValuationDiscountRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFValuation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEvenScrap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEvenEquity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEvenBalloon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCFBreakEven.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCFModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgPositionResultsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResultsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGeneratePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTradeResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExportResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateCommissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsAggregationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionResultsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTrades;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeDetails;
        private DevExpress.XtraGrid.GridControl gridTradeActionResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridTradeActionResultsView;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeActionResults;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTradeActionResults;
        private DevExpress.XtraEditors.SimpleButton btnGeneratePosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemGeneratePosition;
        private DevExpress.Utils.ImageCollection imageList16;
        private DevExpress.XtraEditors.SimpleButton btnExportResults;
        private DevExpress.XtraLayout.LayoutControlItem layoutExportResults;
        private DevExpress.XtraLayout.LayoutControlGroup layoutResultsActions;
        private DevExpress.XtraEditors.SpinEdit txtMarketSensitivityValue;
        private DevExpress.XtraEditors.ComboBoxEdit cmbMarketSensitivityType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControl layoutControlRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.RadioGroup rdgResultsAggregationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutResultsAggregationType;
        private DevExpress.XtraEditors.DateEdit dtpFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutFilterCurveDate;
        private DevExpress.XtraEditors.CheckEdit chkUseSpotValues;
        private DevExpress.XtraEditors.CheckEdit chkOptionPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionPremium;
        private DevExpress.XtraEditors.CheckEdit chkCalculateSums;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateSums;
        private DevExpress.XtraEditors.CheckEdit chkCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTradeResults;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrades;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LabelControl lblIsOptionalPeriod;
        private DevExpress.XtraEditors.LabelControl lblIsMinimumPeriod;
        private DevExpress.XtraEditors.LabelControl lblIsDraft;
        private DevExpress.XtraEditors.LabelControl lblPeriodTo;
        private DevExpress.XtraEditors.LabelControl lblPeriodFrom;
        private DevExpress.XtraEditors.LabelControl lblEntity;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.RadioGroup rdgPositionResultsType;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionResultsType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.CheckEdit chkCFIRR;
        private DevExpress.XtraEditors.SpinEdit txtCFValuationDiscountRate;
        private DevExpress.XtraEditors.CheckEdit chkCFValuation;
        private DevExpress.XtraEditors.CheckEdit chkCFBreakEvenScrap;
        private DevExpress.XtraEditors.CheckEdit chkCFBreakEvenEquity;
        private DevExpress.XtraEditors.CheckEdit chkCFBreakEvenBalloon;
        private DevExpress.XtraEditors.CheckEdit chkCFBreakEven;
        private DevExpress.XtraEditors.LookUpEdit lookupCFModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutUseSpotValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.LabelControl lblOnlyNonZeroTotalDays;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}
