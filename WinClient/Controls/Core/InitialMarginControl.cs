﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.IO;
using System.Globalization;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPrinting;
using DTG.Spreadsheet;
using Exis.Domain;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraPrintingLinks;

namespace Exis.WinClient.Controls.Core
{
    public partial class InitialMarginControl : XtraUserControl
    {
        #region Private Properties

        private const string licenceCode = "SOZT-CSWE-M2SN-CXMT";

        public struct CorrVal
        {
            int x;
            int y;
            decimal val;
        }


        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm1;
        private WaitDialogForm _dialogForm2;


        private readonly ViewTradeGeneralInfo _viewTradesGeneralInfo;
        private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<DateTime, decimal>> _indexPositionSummaryValues;

        #region SpanCodes
        private readonly Dictionary<string, string> _spanCodes = new Dictionary<string, string>
        {
            {"P", "PTOO"},
            {"S", "STOO"},
            {"C", "CTOO"},
            {"H", "HTOO"}
        };
        #endregion

        #region InitialMarginCodes
        private readonly Dictionary<string, string> _initMarginCodes = new Dictionary<string, string>
        {
            {"P", "PTC"},
            {"S", "STC"},
            {"C", "CTC"},
            {"H", "HTC"}
        };
        #endregion

        private List<TmpScanningRange> _tmpScanningRates;
        private List<TmpIntercommodityCredits> _tmpIntercommCredits;
        private List<TmpEnclear> _tmpEnclears;

        private List<string> _availableCodes;
        private List<DateTime> _dates;
        private Dictionary<string, Dictionary<DateTime, decimal>> _indexPosValues;
        private List<OptionPositionValues> _optPosValues;
        private List<DeltaValues> _delta;
        private List<RiskArrayParameter> _rAparam;
        private List<Contract> _contracts;

        private Dictionary<string, decimal[]> _scanningScenario;
        private Dictionary<string, decimal> _intermonthSpreadCharge;
        private Dictionary<string, decimal> _spotCharge;
        private Dictionary<string, decimal> _interCommoditySpreadCredit;

        private List<InitialMargin> _initialMargins;

        private bool imTabLoaded;
        private bool _optionParametersLoaded;

        #endregion

        #region Public Properties

        #endregion

        #region Constructors

        public InitialMarginControl(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            _viewTradesGeneralInfo = viewTradesGeneralInfo;
            InitializeComponent();
            InitializeControl();
            //InitializeData(tradePositionResults);
        }

        #endregion

        #region Proxy
        #endregion

        #region Gui Events

        private void beDATfile_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var openFileDialogDat = new OpenFileDialog
            {
                Filter = @"Dat files (*.dat)|*.dat",
                RestoreDirectory = true,
                Title = Strings.Open_File
            };
            if (openFileDialogDat.ShowDialog() == DialogResult.OK)
            {
                ((ButtonEdit)sender).EditValue = openFileDialogDat.FileName;
                GeneralSettings.Default.DefaultPosOptionParameterFile = openFileDialogDat.FileName.Trim();
                GeneralSettings.Default.Save();
            }
        }

        private void btnLoadOptionsParameters_Click(object sender, EventArgs e)
        {
            //Parse()
            if (beDATfile.Text == "" || beDATfile.EditValue == null)
            {
                XtraMessageBox.Show(this,
                   "Please insert a Dat file.",
                   Strings.Freight_Metrics,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ParseDatFile();
            }
        }

        #endregion

        #region Public Methods

        public void LoadPositionValues(Dictionary<string, Dictionary<DateTime, decimal>> tradePositionResults)
        {
            _tradePositionResults = tradePositionResults;
            InitializeData(tradePositionResults);
            SetButtons(true);
        }

        public void GetGrids(out List<GridControl> grids)
        {
            var gridsToExport = new List<GridControl>();

            if (gridViewSpanOptionParameters.RowCount > 0)
            {
                gridViewSpanOptionParameters.OptionsPrint.PrintHorzLines = false;
                gridViewSpanOptionParameters.OptionsPrint.PrintVertLines = false;
                gridViewSpanOptionParameters.OptionsPrint.AutoWidth = false;
                gridsToExport.Add(gridSpanOptionParameters);
            }

            gridViewInitialMargin.OptionsPrint.PrintHorzLines = false;
            gridViewInitialMargin.OptionsPrint.PrintVertLines = false;
            gridViewInitialMargin.OptionsPrint.AutoWidth = false;
            gridsToExport.Add(gridInitialMargin);

            grids = gridsToExport;
        }

        #endregion

        #region Private Methods

        private void InitializeControl()
        {
            InitInitialMarginGrid();
            beDATfile.EditValue = GeneralSettings.Default.DefaultPosOptionParameterFile;
            beSelectExcel.EditValue = GeneralSettings.Default.DefaultInitialMarginXls;

            SetButtons(false);
        }

        private void SetButtons(bool value)
        {
            btnLoadOptionsParameters.Enabled = value;
            btnExportInitialMargin.Enabled = value;
            btnCalculateInitialMargin.Enabled = value;
        }

        private void InitializeData(Dictionary<string, Dictionary<DateTime, decimal>> tradePositionResults)
        {
            _availableCodes =_viewTradesGeneralInfo.tradeInformations.GroupBy(n => n.ForwardCurve)
                            .Select(n => n.First())
                            .Select(n => n.ForwardCurve)
                            .ToList();

            _dates = tradePositionResults.First().Value.Select(a => a.Key).ToList();

            if (_viewTradesGeneralInfo.tradeInformations.Any(n => n.Type == "FFA" || n.Type == "OPTION" || n.Type == "Put" || n.Type == "Call"))
            {
                //Position Summaries Per Index
                _indexPosValues = new Dictionary<string, Dictionary<DateTime, decimal>>();

                //var groups = _viewTradesGeneralInfo.tradeInformations.GroupBy(n => n.ForwardCurve).ToList();
                foreach (var c in _availableCodes)
                {
                    
                    if (!_indexPosValues.ContainsKey(c))
                        _indexPosValues.Add(c, new Dictionary<DateTime, decimal>() { });

                    foreach (var date in _dates)
                    {
                        if (!_indexPosValues[c].ContainsKey(date))
                            _indexPosValues[c].Add(date, new decimal());
                    }

                    //find trades with same c
                    var tradesWithSameCode =
                        _viewTradesGeneralInfo.tradeInformations.Where(
                            n => n.ForwardCurve == c && (n.Type == "FFA" || n.Type == "OPTION"))
                            .Select(n => n.Identifier)
                            .ToList();

                    foreach (var t in tradePositionResults)
                    {
                        if (tradesWithSameCode.Contains(t.Key))
                        {
                            foreach (var date in _dates)
                            {
                                //if (!_indexPosValues[c].ContainsKey(date))
                                //    _indexPosValues[c].Add(date, new decimal());

                                var trade = _viewTradesGeneralInfo.tradeInformations.Single(n => n.Identifier == t.Key);

                                decimal tradeDateValue;
                                if (trade.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                    tradeDateValue = Math.Abs(tradePositionResults[t.Key][date]);
                                else
                                    tradeDateValue = Math.Abs(tradePositionResults[t.Key][date]) * (-1);
                                _indexPosValues[c][date] = _indexPosValues[c][date] + tradeDateValue;
                            }
                        }
                    }
                }

                //Option Position Summaries
                var optPos =
                    _viewTradesGeneralInfo.tradeInformations.GroupBy(
                        x => new { x.ForwardCurve, x.StrikePrice, x.Type })
                        .Select(
                            x =>
                                new
                                {
                                    ForwardCurve = x.Key.ForwardCurve,
                                    StrikePrice = x.Key.StrikePrice,
                                    CallOrPut = x.Key.Type
                                }).ToList();

                _optPosValues = new List<OptionPositionValues>();
                foreach (var c in optPos)
                {
                    if (c.StrikePrice != 0)
                    {
                        var optPosValue = new OptionPositionValues
                        {
                            Index = c.ForwardCurve,
                            StrikePrice = c.StrikePrice,
                            CallOrPut = c.CallOrPut
                        };

                        var optionInTrades =
                            _viewTradesGeneralInfo.tradeInformations.Where(
                                n => n.ForwardCurve == optPosValue.Index
                                     && n.Type == optPosValue.CallOrPut &&
                                     n.StrikePrice ==
                                     optPosValue.StrikePrice).ToList();

                        optPosValue.PosValues = new Dictionary<DateTime, decimal>();
                        foreach (var date in _dates)
                        {
                            var sum = new decimal();
                            foreach (var option in optionInTrades)
                            {
                                var tradeDateValue = tradePositionResults[option.Identifier][date];
                                if (option.Type == "Put")
                                {
                                    if (option.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                        tradeDateValue = Math.Abs(tradeDateValue);
                                    else
                                        tradeDateValue = Math.Abs(tradeDateValue) * (-1);
                                }
                                sum = sum + tradeDateValue;
                            }
                            optPosValue.PosValues.Add(date, sum);
                        }

                        _optPosValues.Add(optPosValue);
                    }
                }

                if (_optPosValues.Count <= 0)
                {
                    lcgSpanOptionsParameters.Visibility = LayoutVisibility.Never;
                }
            }
        }

        private void ParseDatFile()
        {
            try
            {
                if (!File.Exists(beDATfile.Text))
                {
                    XtraMessageBox.Show(this,
                        "Dat File does not exist. Please select another one.",
                        Strings.Freight_Metrics,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    beDATfile.EditValue = null;
                    GeneralSettings.Default.DefaultPosOptionParameterFile = string.Empty;
                    GeneralSettings.Default.Save();
                    return;
                }

                #region Init Contracts

                var ffaOptionTrades = _optPosValues;

                if (ffaOptionTrades.Count == 0)
                {
                    XtraMessageBox.Show("No or FFAs or Options in trade list. Cannot operate this function");
                    return;
                }

                //Load Contracts
                _contracts = new List<Contract>();

                foreach (var trade in ffaOptionTrades)
                {
                    var spanCode = GetSpanCode(trade.Index);
                    if (spanCode != null)
                    {
                        var contract = new Contract();
                        contract.Index = trade.Index;
                        contract.Code = GetInitialMarginCode(trade.Index);
                        contract.SpanCode = spanCode;
                        contract.CallPut = trade.CallOrPut.Substring(0, 1);
                        contract.Strike = trade.StrikePrice;

                        foreach (var dt in _dates)
                        {
                            var ced = new ContractExpiryDetail();
                            ced.Date = dt;
                            ced.Expiry = dt.ToString();
                            ced.SpanExpiry = GetSpanExpiry(dt);

                            contract.ExpiryDetails.Add(ced);
                        }

                        _contracts.Add(contract);
                    }
                }

                var spanCodes = _contracts.Select(n => n.SpanCode).ToList();

                #endregion

                #region Read File & Fill Extra Info to contracts

                //Open File
                Stream myStream = null;

                string fileName = beDATfile.Text;
                string textLine = "";

                try
                {
                    System.IO.StreamReader objReader;
                    objReader = new StreamReader(fileName);

                    using (objReader)
                    {
                        var fileRow = 0;
                        string fileIndex = string.Empty;
                        decimal fileStrikeDenom = new decimal();
                        decimal fileStrike = new decimal();
                        string fileExpiry = string.Empty;
                        string fileCP;

                        do
                        {
                            fileRow = fileRow + 1;
                            textLine = objReader.ReadLine() + "\r\n";

                            var recordType = textLine.Substring(0, 2);

                            //Identify current index
                            if (recordType == "40")
                            {
                                foreach (var code in spanCodes)
                                {
                                    //If Mid(textLine, 3, 4) = codes(i) & "O" Then?????
                                    var codeInFile = textLine.Substring(2, 4);
                                    if (codeInFile == code)
                                    {
                                        fileIndex = code;
                                        fileStrikeDenom = Convert.ToDecimal(textLine.Substring(31, 4),
                                            CultureInfo.GetCultureInfo("en"));
                                        break;
                                    }
                                    else
                                        fileIndex = string.Empty;

                                    fileExpiry = string.Empty;
                                }
                            }

                            //Identify current expiry
                            if (!string.IsNullOrEmpty(fileIndex) && recordType == "50")
                            {
                                fileExpiry = textLine.Substring(2, 8);
                            }

                            //Identify current strike and option type
                            if (!string.IsNullOrEmpty(fileExpiry) && recordType == "60")
                            {
                                fileStrike =
                                    Convert.ToDecimal(textLine.Substring(2, 8), CultureInfo.GetCultureInfo("en")) /
                                    fileStrikeDenom;
                                fileCP = textLine.Substring(10, 1);

                                //Find Contract If Exists
                                var ctr = _contracts.FirstOrDefault(n => n.SpanCode == fileIndex && n.Strike == fileStrike && n.CallPut == fileCP);

                                if (ctr != null)
                                {
                                    //Find Expiry Detail for specific date If exists
                                    var edt = ctr.ExpiryDetails.FirstOrDefault(n => n.SpanExpiry == fileExpiry);

                                    if (edt != null)
                                    {
                                        edt.SettlementPrice = Convert.ToDecimal(textLine.Substring(17, 8),
                                            CultureInfo.GetCultureInfo("en"));
                                        edt.CompositeData = Convert.ToDecimal(textLine.Substring(25, 9),
                                            CultureInfo.GetCultureInfo("en"));

                                        for (var i = 1; i < 17; i++)
                                        {
                                            edt.LossScenarios[i - 1] = Convert.ToDecimal(
                                                textLine.Substring(28 + 7 * i, 7), CultureInfo.GetCultureInfo("en"));
                                        }

                                        edt.FileRow = fileRow;
                                    }
                                }
                            }



                        } while (objReader.Peek() != -1);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

                #endregion

                //TODO

                #region Create Grid

                var lossScenarios = new List<IndexLossScenarios>();

                foreach (var c in _contracts)
                {
                    foreach (var edt in c.ExpiryDetails)
                    {
                        var ls = new IndexLossScenarios()
                        {
                            Index = c.Index,
                            Expiry = edt.Expiry,
                            CallPut = "Call Put",
                            Strike = c.Strike,
                            FileRow = edt.FileRow,
                            SettlementPrice = edt.SettlementPrice,
                            CompositeDelta = edt.CompositeData,
                            LossScenario1 = edt.LossScenarios[0],
                            LossScenario2 = edt.LossScenarios[1],
                            LossScenario3 = edt.LossScenarios[2],
                            LossScenario4 = edt.LossScenarios[3],
                            LossScenario5 = edt.LossScenarios[4],
                            LossScenario6 = edt.LossScenarios[5],
                            LossScenario7 = edt.LossScenarios[6],
                            LossScenario8 = edt.LossScenarios[7],
                            LossScenario9 = edt.LossScenarios[8],
                            LossScenario10 = edt.LossScenarios[9],
                            LossScenario11 = edt.LossScenarios[10],
                            LossScenario12 = edt.LossScenarios[11],
                            LossScenario13 = edt.LossScenarios[12],
                            LossScenario14 = edt.LossScenarios[13],
                            LossScenario15 = edt.LossScenarios[14],
                            LossScenario16 = edt.LossScenarios[15]
                        };

                        lossScenarios.Add(ls);
                    }

                }

                LoadOptionsRiskParameters(lossScenarios);

                #endregion

                _optionParametersLoaded = true;
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                    exc.Message,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                _optionParametersLoaded = false;
            }
        }

        private string GetSpanCode(string code)
        {
            if (code.Contains('_'))
            {
                //get last two chars
                var chars = code.Substring(code.Length - 2, 2);
                if (chars[0].ToString() == "_")
                {
                    return _spanCodes[chars[1].ToString()];
                }
            }
            else
            {
                return code;
            }

            return null;
        }

        private string GetInitialMarginCode(string code)
        {
            if (code.Contains('_'))
            {
                //get last two chars
                var chars = code.Substring(code.Length - 2, 2);
                if (chars[0].ToString() == "_")
                {
                    return _initMarginCodes[chars[1].ToString()];
                }
            }
            else
            {
                return code;
            }

            return null;
        }

        private string GetSpanExpiry(DateTime date)
        {
            return date.Year.ToString() + date.ToString("MM") + "00";

        }

        private void beSelectExcel_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = @"Excel files (*.xls)|*.xls;*.xlsx;*.xlsm",
                RestoreDirectory = true,
                Title = Strings.Open_File
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                ((ButtonEdit)sender).EditValue = ofd.FileName;
                GeneralSettings.Default.DefaultInitialMarginXls = ofd.FileName.Trim();
                GeneralSettings.Default.Save();
            }
        }

        private void LoadExcelObjects()
        {
            //ConvertToSpanCodes

            var codesToLoad = new List<string>();

            foreach (var c in _availableCodes)
            {
                var codeToLoad = GetInitialMarginCode(c);
                if (codeToLoad != null)
                    codesToLoad.Add(codeToLoad);
            }

           
                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbook;
                string fileName = beSelectExcel.EditValue.ToString();
                string extension = Path.GetExtension(fileName);

                Cursor = Cursors.WaitCursor;
                //_dialogForm1 = new WaitDialogForm(this);

                switch (extension.ToLower())
                {
                    case ".xlsx":
                        {
                            wbook = ExcelWorkbook.ReadXLSX(fileName);
                            break;
                        }
                    case ".xls":
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                    case ".csv":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName);
                            break;
                        }
                    case ".txt":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                            break;
                        }
                    default:
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                }

                #region Workbook - A EnClear Values

                var columnNamesEnclear = new List<string>()
                {
                    "Contract Code",
                    "Contract",
                    "Scanning Range",
                    "Intermonth Spread Charge",
                    "Volatility Range %",
                    "Short Option Minimum",
                    "Spot Month Charge Method",
                    "Margin Top Up Rate***",
                    "Per Day / Per MWH / Per Lot",
                    "Spot Margin Levied From**",
                    "Date Effective From*"
                    //,
                    //"Front Months for Application of Spot Month Charge"
                };

                ExcelCellCollection cells = wbook.Worksheets[0].Cells;

                //Create a dictionary which holds column name and index

                var columnIndexes = new Dictionary<string, int>();
                int dataFirstRow = 0;
                bool found = false;
                int k, l = 0;

                for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) &&
                            columnNamesEnclear.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) &&
                            columnNamesEnclear.Contains(cell.Value.ToString().Trim()))
                        {
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                        }
                    }
                    if (found)
                        break;
                }

                string missingNames =
                    columnNamesEnclear.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                        (current,
                            columnName) =>
                            current +
                            columnName +
                            ", ");

                if (!String.IsNullOrEmpty(missingNames))
                {
                    throw new Exception("Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                        "' do(es) not exist in the selected file."); 

                    //XtraMessageBox.Show(this,
                    //    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                    //    "' do(es) not exist in the selected file.",
                    //    Strings.Freight_Metrics,
                    //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //return;
                }

                _tmpEnclears = new List<TmpEnclear>();

                for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                {
                    if (wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value == null
                        ||
                        string.IsNullOrEmpty(
                            wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value.ToString()))
                        continue;

                    var code = wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value.ToString();
                    if (!codesToLoad.Contains(code)) continue;

                    var tmpEnclear = new TmpEnclear()
                    {
                        ContractCode = wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value != null
                            ? wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value.
                                ToString()
                            : null,
                        Contract = wbook.Worksheets[0].Cells[i, columnIndexes["Contract"]].Value != null
                            ? wbook.Worksheets[0].Cells[i, columnIndexes["Contract"]].Value.
                                ToString()
                            : null,
                        ScanningRange = wbook.Worksheets[0].Cells[i, columnIndexes["Scanning Range"]].Value != null
                            ? wbook.Worksheets[0].Cells[i, columnIndexes["Scanning Range"]].Value.
                                ToString()
                            : null,
                        IntermonthSpreadCharge =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Intermonth Spread Charge"]].Value != null
                                ? Convert.ToDecimal(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Intermonth Spread Charge"]].Value
                                        .ToString(), CultureInfo.GetCultureInfo("en"))
                                : 0,
                        VolatitilyRange =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Volatility Range %"]].Value != null &&
                            wbook.Worksheets[0].Cells[i, columnIndexes["Volatility Range %"]].Value.ToString() != "-"
                                ? Convert.ToDecimal(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Volatility Range %"]].Value.ToString(), CultureInfo.GetCultureInfo("en"))
                                : 0,
                        ShortOptionMinimum =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Short Option Minimum"]].Value != null &&
                            wbook.Worksheets[0].Cells[i, columnIndexes["Short Option Minimum"]].Value.ToString() != "-"
                                ? Convert.ToInt32(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Short Option Minimum"]].Value.ToString(), CultureInfo.GetCultureInfo("en"))
                                : 0,
                        SpotMonthChargeMethod =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Spot Month Charge Method"]].Value != null &&
                            wbook.Worksheets[0].Cells[i, columnIndexes["Spot Month Charge Method"]].Value.ToString() !=
                            "-"
                                ? Convert.ToInt32(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Spot Month Charge Method"]].Value
                                        .ToString(), CultureInfo.GetCultureInfo("en"))
                                : 0,
                        MarginTopUpRate =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Margin Top Up Rate***"]].Value != null &&
                            wbook.Worksheets[0].Cells[i, columnIndexes["Margin Top Up Rate***"]].Value.ToString() != "-"
                                ? Convert.ToInt32(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Margin Top Up Rate***"]].Value.ToString(), CultureInfo.GetCultureInfo("en"))
                                : 0,
                        SpotMarginleviedFrom =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Spot Margin Levied From**"]].Value != null &&
                            wbook.Worksheets[0].Cells[i, columnIndexes["Spot Margin Levied From**"]].Value.ToString() !=
                            "-"
                                ? Convert.ToInt32(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Spot Margin Levied From**"]].Value
                                        .ToString(), CultureInfo.GetCultureInfo("en"))
                                : 0,
                        DateEffectiveFrom =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Date Effective From*"]].Value != null
                                ? Convert.ToDateTime(
                                    wbook.Worksheets[0].Cells[i, columnIndexes["Date Effective From*"]].Value.ToString())
                                : new DateTime(1999, 1, 1)
                        //,
                        //FrontMonthsForApplicationOfSpotMonthCharge = 
                        //    wbook.Worksheets[0].Cells[i, columnIndexes["Front Months for Application of Spot Month Charge"]].Value != null &&
                        //    wbook.Worksheets[0].Cells[i, columnIndexes["Front Months for Application of Spot Month Charge"]].Value.ToString() !=
                        //    "-"
                        //        ? Convert.ToInt32(
                        //            wbook.Worksheets[0].Cells[i, columnIndexes["Front Months for Application of Spot Month Charge"]].Value
                        //                .ToString(), CultureInfo.GetCultureInfo("en"))
                        //        : 0
                    };

                    _tmpEnclears.Add(tmpEnclear);

                }

                #endregion

                #region Workbook - B Scanning Ranges

                var columnNamesScanningRanges = new List<string>()
                {
                    "Contract Code",
                    "Contract",
                    "Scanning Range",
                    "Discount Rate / Tier Structure*",
                    "Per Day / Per MWH / Per Lot / Per Contract Month"
                };

                cells = wbook.Worksheets[1].Cells;

                columnIndexes = new Dictionary<string, int>();
                dataFirstRow = 0;
                found = false;
                k = 0;
                l = 0;

                for (k = 0; k < wbook.Worksheets[1].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[1].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) &&
                            columnNamesScanningRanges.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) &&
                            columnNamesScanningRanges.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                        }
                    }
                    if (found)
                        break;
                }

                missingNames =
                    columnNamesScanningRanges.Where(columnName => !columnIndexes.Keys.Contains(columnName))
                        .Aggregate("",
                            (current,
                                columnName) =>
                                current +
                                columnName +
                                ", ");

                if (!String.IsNullOrEmpty(missingNames))
                {
                    throw new Exception("Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                        "' do(es) not exist in the selected file."); 

                    //XtraMessageBox.Show(this,
                    //    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                    //    "' do(es) not exist in the selected file.",
                    //    Strings.Freight_Metrics,
                    //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //return;
                }


                _tmpScanningRates = new List<TmpScanningRange>();

                for (int i = dataFirstRow; i < wbook.Worksheets[1].Rows.Count; i += 2)
                {
                    if (wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value == null
                        ||
                        string.IsNullOrEmpty(
                            wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value.ToString()))
                        continue;

                    var code = wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value.ToString();
                    if (!codesToLoad.Contains(code)) continue;

                    var tmpScanningRange = new TmpScanningRange()
                    {
                        ContractCode = wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value != null
                            ? wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value.
                                ToString()
                            : null,
                        Contract = wbook.Worksheets[1].Cells[i, columnIndexes["Contract"]].Value != null
                            ? wbook.Worksheets[1].Cells[i, columnIndexes["Contract"]].Value.
                                ToString()
                            : null,
                        ScanningRange = wbook.Worksheets[1].Cells[i, columnIndexes["Scanning Range"]].Value != null
                            ? Convert.ToDecimal(wbook.Worksheets[1].Cells[i, columnIndexes["Scanning Range"]].Value.
                                ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0

                    };

                    for (var tier = 0; tier < 5; tier++)
                    {
                        if (wbook.Worksheets[1].Cells[i, 4].Value != null &&
                            wbook.Worksheets[1].Cells[i, 4].Value.ToString() != "")
                        {
                            var rate = Convert.ToDecimal(wbook.Worksheets[1].Cells[i - 1, 4 + tier].Value.ToString());

                            var dateRange = wbook.Worksheets[1].Cells[i, 4 + tier].Value.ToString();

                            var tierFrom = new DateTime();
                            var tierTo = new DateTime();

                            if (tier == 0 && DefineDateRangeIsDate(dateRange))
                            {
                                tierFrom = Convert.ToDateTime(dateRange);
                                tierTo = tierFrom;
                            }
                            else if (tier == 0 && !dateRange.Contains('-'))
                            {
                                var month = GetMonth(dateRange.Substring(0, 3));
                                var year = Convert.ToInt32(dateRange.Substring(dateRange.Count()-2, 2));
                                tierFrom = new DateTime(year, month, 1);
                                tierTo = tierFrom;
                            }
                            else if (dateRange.Trim().Substring(dateRange.Trim().Length - 7, 7) == "onwards")
                            {
                                var month = GetMonth(dateRange.Substring(0, 3));
                                var year = Convert.ToInt32("20" + dateRange.Substring(4, 2));

                                tierFrom = new DateTime(year, month, 1);


                                //
                                var monthTo = _viewTradesGeneralInfo.periodTo.AddMonths(1).Month;
                                var yearTo = _viewTradesGeneralInfo.periodTo.AddMonths(1).Year;
                                tierTo = new DateTime(yearTo, monthTo, 1);
                            }
                            else
                            {
                                string[] dates = dateRange.Split('-').ToArray();

                                if (dates.Count() > 1) //Υπάρχει range
                                {
                                    var monthFrom = GetMonth(dates[0].Trim().Substring(0, 3));
                                    var yearFrom =
                                        Convert.ToInt32("20" + dates[0].Trim().Substring(dates[0].Trim().Length - 2, 2));
                                    tierFrom = new DateTime(yearFrom, monthFrom, 1);

                                    var monthTo = GetMonth(dates[1].Trim().Substring(0, 3));
                                    var yearTo =
                                        Convert.ToInt32("20" + dates[1].Trim().Substring(dates[1].Trim().Length - 2, 2));
                                    tierTo = new DateTime(yearTo, monthTo, 1);
                                }
                                else
                                {
                                    throw new Exception("Error occurred while parsing excel file. Error: There is no \'-\' between ranges in Scanning Ranges Tab for Tiers 2 to 4. Correct format Mon YY - Mon YY."); 
                                }
                            }

                            tmpScanningRange.TierDiscounts.Add(
                                new TierDiscount { Rate = rate, DateRange = dateRange, TierFrom = tierFrom, TierTo = tierTo });
                        }
                        else
                        {
                            break;
                        }
                    }

                    _tmpScanningRates.Add(tmpScanningRange);

                }

                #endregion

                #region Workbook - C Intercommodity Credits

                var columnNamesIntercommodityCredits = new List<string>()
                {
                    "Contract / Spread",
                    "Contract Codes",
                    "Priorities",
                    "Delta Ratio",
                    "% Credit per leg",
                    "Date effective from*"
                };

                cells = wbook.Worksheets[2].Cells;

                columnIndexes = new Dictionary<string, int>();
                dataFirstRow = 0;
                found = false;
                k = 0;
                l = 0;

                for (k = 0; k < wbook.Worksheets[2].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[2].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) &&
                            columnNamesIntercommodityCredits.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) &&
                            columnNamesIntercommodityCredits.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                        }
                    }
                    if (found)
                        break;
                }

                missingNames =
                    columnNamesIntercommodityCredits.Where(columnName => !columnIndexes.Keys.Contains(columnName))
                        .Aggregate("",
                            (current,
                                columnName) =>
                                current +
                                columnName +
                                ", ");

                if (!String.IsNullOrEmpty(missingNames))
                {
                    throw new Exception("Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                        "' do(es) not exist in the selected file."); 
                    
                    //XtraMessageBox.Show(this,
                    //    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                    //    "' do(es) not exist in the selected file.",
                    //    Strings.Freight_Metrics,
                    //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //return;
                }


                _tmpIntercommCredits = new List<TmpIntercommodityCredits>();

            for (int i = dataFirstRow; i < wbook.Worksheets[2].Rows.Count; i += 2)
            {
                if (wbook.Worksheets[2].Cells[i, columnIndexes["Contract Codes"]].Value == null
                    ||
                    string.IsNullOrEmpty(
                        wbook.Worksheets[2].Cells[i, columnIndexes["Contract Codes"]].Value.ToString()))
                    continue;

                //char[] delimiterChars = { '-',':' };
                string[] contractCodes =
                    wbook.Worksheets[2].Cells[i, columnIndexes["Contract Codes"]].Value.ToString()
                        .Split('-')
                        .ToArray();
                var code1 = contractCodes[0].Trim();
                var code2 = contractCodes[1].Trim();

                if (codesToLoad.Contains(code1) && codesToLoad.Contains(code2))
                {

                    string[] deltaRations =
                        wbook.Worksheets[2].Cells[i, columnIndexes["Delta Ratio"]].Value.ToString()
                            .Split(':')
                            .ToArray();

                    var tmpIntercommCredit = new TmpIntercommodityCredits()
                    {
                        Code1 = code1,
                        DeltaRatioCode1 = Convert.ToInt32(deltaRations[0].Trim()),
                        Code2 = code2,
                        DeltaRatioCode2 = Convert.ToInt32(deltaRations[1].Trim()),
                        Properties = wbook.Worksheets[2].Cells[i, columnIndexes["Priorities"]].Value != null
                            ? Convert.ToInt32(wbook.Worksheets[2].Cells[i, columnIndexes["Priorities"]].Value.
                                ToString())
                            : 0,
                        CreditPerLeg = wbook.Worksheets[2].Cells[i, columnIndexes["% Credit per leg"]].Value != null
                            ? Convert.ToInt32(wbook.Worksheets[2].Cells[i, columnIndexes["% Credit per leg"]].Value.
                                ToString())
                            : 0,
                        DateEffectiveForm =
                            wbook.Worksheets[2].Cells[i, columnIndexes["Date effective from*"]].Value != null
                                ? Convert.ToDateTime(
                                    wbook.Worksheets[2].Cells[i, columnIndexes["Date effective from*"]].Value
                                        .ToString())
                                : new DateTime(1999, 1, 1)
                    };

                    _tmpIntercommCredits.Add(tmpIntercommCredit);
                }
            }

            #endregion

            Cursor = Cursors.Default;
            //lock (_syncObject)
            //{
            //    _dialogForm1.Close();
            //    _dialogForm1 = null;
            //}

            
            //catch (Exception exc)
            //{

            //    Cursor = Cursors.Default;
            //    //lock (_syncObject)
            //    //{
            //    //    _dialogForm1.Close();
            //    //    _dialogForm1 = null;
            //    //}

            //    //XtraMessageBox.Show(this,
            //    //    "Error occurred while parsing excel file. Error: '" + exc.Message + "'",
            //    //    Strings.Freight_Metrics,
            //    //    MessageBoxButtons.OK, MessageBoxIcon.Error);

            //    throw new Exception("There is no \'-\' between ranges in Scanning Ranges Tab for Tiers 2 to 4. Correct format Mon YY - Mon YY."); 
            //}
        }

        private bool DefineDateRangeIsDate(string value)
        {
            bool isDate;

            try
            {
                var date = Convert.ToDateTime(value);
                isDate = true;
            }
            catch (Exception exc)
            {
                isDate = false;
            }

            return isDate;
        }

        private int GetMonth(string p)
        {
            if (p == "Jan" || p == "Ιαν") return 1;
            if (p == "Feb" || p == "Φεβρ") return 2;
            if (p == "Mar" || p == "Μαρ") return 3;
            if (p == "Apr" || p == "Απρ") return 4;
            if (p == "May" || p == "Μαι") return 5;
            if (p == "Jun" || p == "Ιουν") return 6;
            if (p == "Jul" || p == "Ιουλ") return 7;
            if (p == "Aug" || p == "Αυγ") return 8;
            if (p == "Sep" || p == "Σεπτ") return 9;
            if (p == "Oct" || p == "Οκτ") return 10;
            if (p == "Nov" || p == "Νοε") return 11;
            if (p == "Dec" || p == "Δεκ") return 12;

            return 0;
        }

        private void btnCalculateInitialMargin_Click(object sender, EventArgs e)
        {
            if (_optPosValues.Count > 0 && !_optionParametersLoaded)
            {
                XtraMessageBox.Show(this,
                  "Please Load Options Parameters.",
                  Strings.Freight_Metrics,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (beSelectExcel.Text == string.Empty || beSelectExcel.EditValue == null)
            {
                XtraMessageBox.Show(this,
                   "Please insert the Excel File.",
                   Strings.Freight_Metrics,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!File.Exists(beSelectExcel.Text))
            {
                XtraMessageBox.Show(this,
                   "The Excel File does not exist. Please select another one.",
                   Strings.Freight_Metrics,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                beSelectExcel.EditValue = null;
                GeneralSettings.Default.DefaultInitialMarginXls = string.Empty;
                GeneralSettings.Default.Save();
                return;
            }

            if (_indexPosValues == null)
            {
                XtraMessageBox.Show(this,
                    "Please calculate position first.",
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            //Risk Array Parameters
            _rAparam = LoadRiskArrayParameters();

            try
            {
                LoadExcelObjects();

                _delta = new List<DeltaValues>();
                _scanningScenario = new Dictionary<string, decimal[]>();
                _intermonthSpreadCharge = new Dictionary<string, decimal>();
                _spotCharge = new Dictionary<string, decimal>();
                _interCommoditySpreadCredit = new Dictionary<string, decimal>();

                foreach (var code in _availableCodes)
                {
                    var delta = new DeltaValues { Code = code };

                    foreach (var date in _dates)
                    {
                        var contract = _tmpScanningRates.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));
                        if (contract != null)
                        {
                            // check if date range not included in enclear.xlsx
                            var tier = contract.TierDiscounts[0].TierFrom > date
                                ? null
                                : contract.TierDiscounts.FirstOrDefault(n => n.TierFrom <= date && n.TierTo >= date);

                            var deltaValue = tier == null ? 0 : tier.Rate * _indexPosValues[code][date];
                            delta.DateValues.Add(date, deltaValue);
                        }
                    }

                    _delta.Add(delta);

                    _scanningScenario.Add(code, CalculationOfScanningRange(code));
                    _intermonthSpreadCharge.Add(code, CalculateIntermonthSpreadCharge(code));
                    _spotCharge.Add(code, CalculateSpotCharge(code));
                }

                _interCommoditySpreadCredit = CalculateInterCommoditySpreadCreditForAllIndexes();

                var initalMargin = CalculateInitialMargin();
                LoadInitialMargin(initalMargin);

                if (gridViewInitialMargin.DataRowCount > 0)
                    btnExportInitialMargin.Enabled = true;
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                    exc.Message,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<InitialMargin> CalculateInitialMargin()
        {
            //var initalMarginDict = new Dictionary<string, decimal>();
            var initialMargins = new List<InitialMargin>();

            foreach (var c in _availableCodes)
            {
                var code = GetInitialMarginCode(c);
                var im = new InitialMargin
                {
                    SpanCode = code,
                    ScanningRisk = _scanningScenario[c][0],
                    IntermonthSpreadCharge = _intermonthSpreadCharge[c],
                    SpotMonthCharge = _spotCharge[c],
                    InterContractCredit = _interCommoditySpreadCredit[code],
                    ShortOptionMinimun = 0
                };

                if (im.ScanningRisk + im.IntermonthSpreadCharge + im.SpotMonthCharge - im.InterContractCredit < im.ShortOptionMinimun)
                    im.InitialMarginValue = 0;
                else
                    im.InitialMarginValue = im.ScanningRisk + im.IntermonthSpreadCharge + im.SpotMonthCharge - im.InterContractCredit;

                initialMargins.Add(im);
            }

            return initialMargins;
        }

        private Dictionary<string, decimal> CalculateInterCommoditySpreadCreditForAllIndexes()
        {
            //net Delta code
            var netDeltaDict = new Dictionary<string, decimal>();
            var WPFRDict = new Dictionary<string, decimal>();
            var interCommSpreadCreditDict = new Dictionary<string, decimal>();

            foreach (var av in _availableCodes)
            {
                var code = GetInitialMarginCode(av);

                decimal netDelta = 0;
                decimal wpfr = 0;
                decimal codePriceRisk = 0;
                decimal interCommSpreadCredit = 0;

                var interComm = _tmpIntercommCredits.FirstOrDefault(n => n.Code1 == code);
                var delta = _delta.FirstOrDefault(n => n.Code == av);

                foreach (var date in _dates)
                {
                    netDelta = netDelta + delta.DateValues[date];

                    //Add Options
                    var options = _optPosValues.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                    foreach (var option in options)
                    {
                        //Ean yparxei Option Value gia ton index
                        var contract =
                            _contracts.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice && n.CallPut == option.CallOrPut.Substring(0, 1));
                        if (contract != null)
                        {
                            var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                            netDelta = netDelta + Math.Abs(option.PosValues[date]) * exd.CompositeData;
                        }
                    }
                }
                netDeltaDict.Add(code, netDelta);

                //priceRisk per Code
                codePriceRisk = _scanningScenario[av][1];

                //WPFR
                wpfr = netDelta != 0 ? Decimal.Round(codePriceRisk / Math.Abs(netDelta), 0) : 0;
                WPFRDict.Add(code, wpfr);

                //InterCommodity Spread Credit
                interCommSpreadCreditDict.Add(code, 0);
            }

            //Run through priority list
            foreach (var pair in _tmpIntercommCredits)
            {
                if ((netDeltaDict[pair.Code1] < 0 && netDeltaDict[pair.Code2] > 0) ||
                    (netDeltaDict[pair.Code1] > 0 && netDeltaDict[pair.Code2] < 0))
                {
                    var numSpreads1 = pair.DeltaRatioCode1 != 0 ? Decimal.Round(Math.Abs(netDeltaDict[pair.Code1] / pair.DeltaRatioCode1), 0) : 0;
                    var numSpreads2 = pair.DeltaRatioCode2 != 0 ? Decimal.Round(Math.Abs(netDeltaDict[pair.Code2] / pair.DeltaRatioCode2), 0) : 0;

                    decimal numSpreads = 0;
                    if (numSpreads1 < numSpreads2)
                        numSpreads = numSpreads1;
                    else
                        numSpreads = numSpreads2;

                    interCommSpreadCreditDict[pair.Code1] = interCommSpreadCreditDict[pair.Code1] +
                                                            WPFRDict[pair.Code1] * numSpreads * pair.DeltaRatioCode1 *
                                                            pair.CreditPerLeg / 100;

                    interCommSpreadCreditDict[pair.Code2] = interCommSpreadCreditDict[pair.Code2] +
                                                            WPFRDict[pair.Code1] * numSpreads * pair.DeltaRatioCode2 *
                                                            pair.CreditPerLeg / 100;

                    int code1sign = netDeltaDict[pair.Code1] < 0 ? -1 : 1;
                    int code2sign = netDeltaDict[pair.Code2] < 0 ? -1 : 1;

                    interCommSpreadCreditDict[pair.Code1] = interCommSpreadCreditDict[pair.Code1] -
                                                            (code1sign * numSpreads * pair.DeltaRatioCode1);

                    interCommSpreadCreditDict[pair.Code2] = interCommSpreadCreditDict[pair.Code2] -
                                                            (code2sign * numSpreads * pair.DeltaRatioCode2);
                }
            }

            return interCommSpreadCreditDict;
        }

        private decimal CalculateSpotCharge(string code)
        {
            var enclear = _tmpEnclears.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));
            var delta = _delta.FirstOrDefault(n => n.Code == code);

            var chargeAmount = enclear.MarginTopUpRate;
            decimal totalDelta = 0;

            foreach (var date in _dates)
            {
                totalDelta = totalDelta + Math.Abs(delta.DateValues[date]);

                //Add Options
                var options = _optPosValues.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                foreach (var option in options)
                {
                    //Ean yparxei Option Value gia ton index
                    var contract =
                        _contracts.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice && n.CallPut == option.CallOrPut.Substring(0, 1));
                    if (contract != null)
                    {
                        var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                        totalDelta = totalDelta + Math.Abs(option.PosValues[date] * exd.CompositeData);
                    }
                }
            }

            return Decimal.Round(totalDelta * chargeAmount, 0);
        }

        private decimal CalculateIntermonthSpreadCharge(string code)
        {
            decimal totalShort = 0;
            decimal totalLong = 0;

            var deltaCode = _delta.First(n => n.Code == code);
            foreach (var date in _dates)
            {
                if (deltaCode.DateValues[date] < 0)
                    totalShort = totalShort - deltaCode.DateValues[date];

                if (deltaCode.DateValues[date] > 0)
                    totalLong = totalLong + deltaCode.DateValues[date];

                //Add Options
                var options = _optPosValues.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                foreach (var option in options)
                {
                    //Ean yparxei Option Value gia ton index
                    var contract = _contracts.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice
                        && n.CallPut == option.CallOrPut.Substring(0, 1));
                    if (contract != null)
                    {
                        var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                        if (option.CallOrPut.Substring(0, 1) == "C")
                        {
                            if (option.PosValues[date] < 0)
                                totalShort = totalShort - option.PosValues[date] * Math.Abs(exd.CompositeData);

                            if (option.PosValues[date] > 0)
                                totalLong = totalLong + option.PosValues[date] * Math.Abs(exd.CompositeData);
                        }
                        else
                        {
                            if (option.PosValues[date] > 0)
                                totalShort = totalShort + option.PosValues[date] * Math.Abs(exd.CompositeData);

                            if (option.PosValues[date] < 0)
                                totalLong = totalLong - option.PosValues[date] * Math.Abs(exd.CompositeData);
                        }
                    }
                }
            }

            var enclear = _tmpEnclears.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));

            if (totalLong < totalShort)
                return Math.Round(totalLong * enclear.IntermonthSpreadCharge, 0);
            else
                return Math.Round(totalShort * enclear.IntermonthSpreadCharge, 0);
        }

        private decimal[] CalculationOfScanningRange(string code)
        {
            var scanRange = new Dictionary<int, decimal>();

            var scanningRange = _tmpScanningRates.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));

            var deltaCode = _delta.First(n => n.Code == code);
            //Scanning WorstScenario
            for (var i = 0; i < 16; i++)
            {
                var sr = new decimal(0);

                var riskArrayParam = _rAparam.FirstOrDefault(n => n.RiskScenario == i + 1);

                foreach (var date in _dates)
                {
                    sr = sr + deltaCode.DateValues[date] * scanningRange.ScanningRange * riskArrayParam.PriceVariation *
                         riskArrayParam.WeightFraction;

                    //Options

                    var options = _optPosValues.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                    foreach (var option in options)
                    {
                        //Ean yparxei Option Value gia ton index
                        var contract =
                            _contracts.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice && n.CallPut == option.CallOrPut.Substring(0, 1));
                        if (contract != null)
                        {
                            var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                            var optSr = Decimal.Round((option.PosValues[date] * exd.LossScenarios[i] / 10),
                                MidpointRounding.AwayFromZero);
                            sr = sr - optSr;
                        }
                    }

                }

                scanRange.Add(i, Decimal.Round(sr, MidpointRounding.AwayFromZero));
            }

            var worstCase = scanRange[0];
            var activeScenario = 1;
            int pairedScenario = 0;

            for (var i = 1; i < 17; i++)
            {
                if (scanRange[i - 1] < worstCase)
                {
                    worstCase = scanRange[i - 1];
                    activeScenario = i;
                }
            }
            if (activeScenario == 15 || activeScenario == 16)
                pairedScenario = activeScenario;
            else
            {
                if (activeScenario % 2 == 0)
                    pairedScenario = activeScenario - 1;
                else
                    pairedScenario = activeScenario + 1;
            }

            var volRisk = -(Math.Round((scanRange[activeScenario - 1] + scanRange[pairedScenario - 1]) / 2, 0));
            var timeRisk = -(Math.Round((scanRange[0] + scanRange[1]) / 2, 0));
            var priceRisk = volRisk - timeRisk;

            if (priceRisk < 0)
                priceRisk = 0;

            var scanningScenario = new decimal[2];
            scanningScenario[0] = -(Math.Round(worstCase, 0));
            scanningScenario[1] = priceRisk;

            return scanningScenario;
        }

        private List<RiskArrayParameter> LoadRiskArrayParameters()
        {
            var raParams = new List<RiskArrayParameter>()
            {
                new RiskArrayParameter()
                {
                    RiskScenario = 1,
                    PriceVariation = 0,
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 2,
                    PriceVariation = 0,
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 3,
                    PriceVariation = new decimal(0.33333333),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 4,
                    PriceVariation = new decimal(0.33333333),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 5,
                    PriceVariation = -new decimal(0.33333333),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 6,
                    PriceVariation = -new decimal(0.33333333),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 7,
                    PriceVariation = new decimal(0.66666667),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 8,
                    PriceVariation = new decimal(0.66666667),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 9,
                    PriceVariation = -new decimal(0.66666667),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 10,
                    PriceVariation = -new decimal(0.66666667),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 11,
                    PriceVariation = 1,
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 12,
                    PriceVariation = 1,
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 13,
                    PriceVariation = -1,
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 14,
                    PriceVariation = -1,
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 15,
                    PriceVariation = 2,
                    VolatilityVariation = 0,
                    WeightFraction = new decimal(0.35)
                },
                new RiskArrayParameter()
                {
                    RiskScenario = 16,
                    PriceVariation = -2,
                    VolatilityVariation = 0,
                    WeightFraction = new decimal(0.35)
                },
            };

            return raParams;
        }

        private void LoadOptionsRiskParameters(List<IndexLossScenarios> lossScenarios)
        {
            #region Grid Loss Scenarios

            gridSpanOptionParameters.ForceInitialize();
            gridViewSpanOptionParameters.Columns.Clear();

            var gcIndex = new GridColumn { Name = "Index", FieldName = "Index", Caption = "Index", Visible = true };
            gridViewSpanOptionParameters.Columns.Add(gcIndex);

            var gcExpiry = new GridColumn
            {
                Name = "Expiry",
                FieldName = "Expiry",
                Caption = "Expiry",
                Visible = true
            };
            gridViewSpanOptionParameters.Columns.Add(gcExpiry);

            var gcCallPut = new GridColumn
            {
                Name = "CallPut",
                FieldName = "CallPut",
                Caption = "Call/Put",
                Visible = true
            };
            gridViewSpanOptionParameters.Columns.Add(gcCallPut);

            var gcStrike = new GridColumn
            {
                Name = "Strike",
                FieldName = "Strike",
                Caption = "Strike",
                Visible = true
            };
            gcStrike.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcStrike.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcStrike);

            var gcFileRow = new GridColumn { Name = "FileRow", FieldName = "FileRow", Caption = "File Row", Visible = true };
            gcFileRow.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridViewSpanOptionParameters.Columns.Add(gcFileRow);

            var gcLSettlementPrice = new GridColumn
            {
                Name = "SettlementPrice",
                FieldName = "SettlementPrice",
                Caption = "Settlement Price",
                Visible = true
            };
            gcLSettlementPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLSettlementPrice.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLSettlementPrice);

            var gcCompositeDelta = new GridColumn
            {
                Name = "CompositeDelta",
                FieldName = "CompositeDelta",
                Caption = "Composite Delta",
                Visible = true
            };
            gcCompositeDelta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcCompositeDelta.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcCompositeDelta);

            #region Loss Scenarios

            var gcLossScenario1 = new GridColumn
            {
                Name = "LossScenario1",
                FieldName = "LossScenario1",
                Caption = "LS 1",
                Visible = true
            };
            gcLossScenario1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario1.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario1);

            var gcLossScenario2 = new GridColumn
            {
                Name = "LossScenario2",
                FieldName = "LossScenario2",
                Caption = "LS 2",
                Visible = true
            };
            gcLossScenario2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario2.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario2);

            var gcLossScenario3 = new GridColumn
            {
                Name = "LossScenario3",
                FieldName = "LossScenario3",
                Caption = "LS 3",
                Visible = true
            };
            gcLossScenario3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario3.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario3);

            var gcLossScenario4 = new GridColumn
            {
                Name = "LossScenario4",
                FieldName = "LossScenario4",
                Caption = "LS 4",
                Visible = true
            };
            gcLossScenario4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario4.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario4);

            var gcLossScenario5 = new GridColumn
            {
                Name = "LossScenario5",
                FieldName = "LossScenario5",
                Caption = "LS 5",
                Visible = true
            };
            gcLossScenario5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario5.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario5);

            var gcLossScenario6 = new GridColumn
            {
                Name = "LossScenario6",
                FieldName = "LossScenario6",
                Caption = "LS 6",
                Visible = true
            };
            gcLossScenario6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario6.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario6);

            var gcLossScenario7 = new GridColumn
            {
                Name = "LossScenario7",
                FieldName = "LossScenario7",
                Caption = "LS 7",
                Visible = true
            };
            gcLossScenario7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario7.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario7);

            var gcLossScenario8 = new GridColumn
            {
                Name = "LossScenario8",
                FieldName = "LossScenario8",
                Caption = "LS 8",
                Visible = true
            };
            gcLossScenario8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario8.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario8);

            var gcLossScenario9 = new GridColumn
            {
                Name = "LossScenario9",
                FieldName = "LossScenario9",
                Caption = "LS 9",
                Visible = true
            };
            gcLossScenario9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario9.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario9);

            var gcLossScenario10 = new GridColumn
            {
                Name = "LossScenario10",
                FieldName = "LossScenario10",
                Caption = "LS 10",
                Visible = true
            };
            gcLossScenario10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario10.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario10);

            var gcLossScenario11 = new GridColumn
            {
                Name = "LossScenario11",
                FieldName = "LossScenario11",
                Caption = "LS 11",
                Visible = true
            };
            gcLossScenario11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario11.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario11);

            var gcLossScenario12 = new GridColumn
            {
                Name = "LossScenario12",
                FieldName = "LossScenario12",
                Caption = "LS 12",
                Visible = true
            };
            gcLossScenario12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario12.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario12);

            var gcLossScenario13 = new GridColumn
            {
                Name = "LossScenario13",
                FieldName = "LossScenario13",
                Caption = "LS 13",
                Visible = true
            };
            gcLossScenario13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario13.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario13);

            var gcLossScenario14 = new GridColumn
            {
                Name = "LossScenario14",
                FieldName = "LossScenario14",
                Caption = "LS 14",
                Visible = true
            };
            gcLossScenario14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario14.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario14);

            var gcLossScenario15 = new GridColumn
            {
                Name = "LossScenario15",
                FieldName = "LossScenario15",
                Caption = "LS 15",
                Visible = true
            };
            gcLossScenario15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario15.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario15);

            var gcLossScenario16 = new GridColumn
            {
                Name = "LossScenario16",
                FieldName = "LossScenario16",
                Caption = "LS 16",
                Visible = true
            };
            gcLossScenario16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcLossScenario16.DisplayFormat.FormatString = "d2";
            gridViewSpanOptionParameters.Columns.Add(gcLossScenario16);

            #endregion

            gridViewSpanOptionParameters.ColumnPanelRowHeight = 10;
            gridSpanOptionParameters.BeginUpdate();
            gridSpanOptionParameters.DataSource = lossScenarios;
            gridViewSpanOptionParameters.BestFitColumns();
            gridSpanOptionParameters.EndUpdate();

            #endregion
        }

        private void LoadInitialMargin(List<InitialMargin> initialMargins)
        {
            gridInitialMargin.DataSource = initialMargins;
            gridInitialMargin.RefreshDataSource();
        }

        private void InitInitialMarginGrid()
        {
            gridInitialMargin.ForceInitialize();

            var gcIndex = new GridColumn { Name = "Contract Code", FieldName = "SpanCode", Caption = "Contract Code", Visible = true };
            gridViewInitialMargin.Columns.Add(gcIndex);

            var gcScanningRisk = new GridColumn
            {
                Name = "ScanningRisk",
                FieldName = "ScanningRisk",
                Caption = "Scanning Risk",
                Visible = true
            };
            gcScanningRisk.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcScanningRisk.DisplayFormat.FormatString = "n2";
            gcScanningRisk.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcScanningRisk);

            var gcIntermonthSpreadCharge = new GridColumn
            {
                Name = "IntermonthSpreadCharge",
                FieldName = "IntermonthSpreadCharge",
                Caption = "Intermonth Spread Charge",
                Visible = true
            };
            gcIntermonthSpreadCharge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcIntermonthSpreadCharge.DisplayFormat.FormatString = "n2";
            gcIntermonthSpreadCharge.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcIntermonthSpreadCharge);

            var gcSpotMonthCharge = new GridColumn
            {
                Name = "SpotMonthCharge",
                FieldName = "SpotMonthCharge",
                Caption = "Spot Month Charge",
                Visible = true
            };
            gcSpotMonthCharge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcSpotMonthCharge.DisplayFormat.FormatString = "n2";
            gcSpotMonthCharge.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcSpotMonthCharge);

            var gcInterContractCredit = new GridColumn { Name = "InterContractCredit", FieldName = "InterContractCredit", Caption = "Inter - Contract Credit", Visible = true };
            gcInterContractCredit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcInterContractCredit.DisplayFormat.FormatString = "n2";
            gcInterContractCredit.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcInterContractCredit);

            var gcShortOptionMinimun = new GridColumn
            {
                Name = "ShortOptionMinimun",
                FieldName = "ShortOptionMinimun",
                Caption = "Short Option Minimun",
                Visible = true
            };
            gcShortOptionMinimun.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcShortOptionMinimun.DisplayFormat.FormatString = "n2";
            gcShortOptionMinimun.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcShortOptionMinimun);

            var gcInitialMargin = new GridColumn
            {
                Name = "InitialMargin",
                FieldName = "InitialMarginValue",
                Caption = "Initial Margin",
                Visible = true
            };
            gcInitialMargin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcInitialMargin.DisplayFormat.FormatString = "n2";
            gcInitialMargin.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcInitialMargin);

            gridViewInitialMargin.ColumnPanelRowHeight = 10;
            gridInitialMargin.BeginUpdate();
            gridInitialMargin.EndUpdate();

        }

        //private void tbResults_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        //{
        //    if (e.Page == tbpgInitialMargin && !imTabLoaded)
        //    {
        //        InitInitialMarginGrid();
        //        imTabLoaded = true;
        //        beDATfile.EditValue = GeneralSettings.Default.DefaultPosOptionParameterFile;
        //        beSelectExcel.EditValue = GeneralSettings.Default.DefaultInitialMarginXls;
        //    }
        //}

        private void btnExportInitialMargin_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            gridViewInitialMargin.OptionsPrint.PrintHorzLines = false;
            gridViewInitialMargin.OptionsPrint.PrintVertLines = false;
            gridViewInitialMargin.OptionsPrint.AutoWidth = false;

            try
            {
                string fileName = "Initial_Margin";
                string sheetNameInitialMargin = "Initial_Margin";
                string sheetNameParameters = "Options Parameters";

                if (gridViewSpanOptionParameters.DataRowCount > 0)
                {
                    var printingSystem = new PrintingSystemBase();
                    var compositeLink = new CompositeLinkBase { PrintingSystemBase = printingSystem };

                    var link1 = new PrintableComponentLinkBase
                    {
                        Component = gridInitialMargin,
                        PaperName = sheetNameInitialMargin,
                    };
                    var link2 = new PrintableComponentLinkBase
                    {
                        Component = gridSpanOptionParameters,
                        PaperName = sheetNameParameters,
                    };

                    compositeLink.Links.Add(link1);
                    compositeLink.Links.Add(link2);

                    var options = new XlsxExportOptions()
                    {
                        ExportHyperlinks = false,
                        ExportMode = XlsxExportMode.SingleFilePageByPage,
                        ShowGridLines = false,
                        TextExportMode = TextExportMode.Value

                    };

                    compositeLink.CreatePageForEachLink();
                    compositeLink.ExportToXlsx(
                        Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName +
                        ".xlsx", options);
                }
                else
                {
                    gridViewInitialMargin.ExportToXlsx(
                        Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName +
                        ".xlsx",
                        new XlsxExportOptions
                        {
                            ExportHyperlinks = false,
                            ExportMode = XlsxExportMode.SingleFile,
                            SheetName = sheetNameInitialMargin,
                            ShowGridLines = false,
                            TextExportMode = TextExportMode.Value
                        });
                }

                //gridViewInitialMargin.ExportToXlsx(
                //    Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName +
                //    ".xlsx",
                //    new XlsxExportOptions
                //    {
                //        ExportHyperlinks = false,
                //        ExportMode = XlsxExportMode.SingleFile,
                //        SheetName = sheetNameInitialMargin,
                //        ShowGridLines = false,
                //        TextExportMode = TextExportMode.Value
                //    });

                //if (gridViewSpanOptionParameters.DataRowCount > 0)
                //{
                //    gridSpanOptionParameters.ExportToXlsx(
                //    Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName +
                //    ".xlsx",
                //    new XlsxExportOptions
                //    {
                //        ExportHyperlinks = false,
                //        ExportMode = XlsxExportMode.SingleFile,
                //        SheetName = sheetNameParameters,
                //        ShowGridLines = false,
                //        TextExportMode = TextExportMode.Value
                //    });
                //}

                //else
                //{
                //    gridViewInitialMargin.ExportToXls(
                //        Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xls",
                //        new XlsExportOptions
                //        {
                //            ExportHyperlinks = false,
                //            ExportMode = XlsExportMode.SingleFile,
                //            SheetName = sheetNameInitialMargin,
                //            ShowGridLines = false,
                //            TextExportMode = TextExportMode.Value
                //        });

                //    if (gridViewSpanOptionParameters.DataRowCount > 0)
                //    {
                //        gridViewSpanOptionParameters.ExportToXls(
                //         Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xls",
                //         new XlsExportOptions
                //         {
                //             ExportHyperlinks = false,
                //             ExportMode = XlsExportMode.SingleFile,
                //             SheetName = sheetNameParameters,
                //             ShowGridLines = false,
                //             TextExportMode = TextExportMode.Value
                //         });
                //    }
                //}

                Cursor = Cursors.Default;
                XtraMessageBox.Show(this, "File with name: '" + fileName + "' exported to Desktop successfully."
                    ,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this,
                    string.Format(Strings.Failed_to_write_file_to_Desktop__Details___0_, exc.Message)
                    ,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

    }

    #region Classes

    internal class Contract
    {
        public string Index { get; set; }
        public string Code { get; set; }
        public string SpanCode { get; set; }
        public string CallPut { get; set; }
        public decimal Strike { get; set; }

        public List<ContractExpiryDetail> ExpiryDetails { get; set; }

        public Contract()
        {
            ExpiryDetails = new List<ContractExpiryDetail>();
        }
    }

    internal class ContractExpiryDetail
    {
        public DateTime Date { get; set; }
        public string Expiry { get; set; }
        public string SpanExpiry { get; set; }
        public decimal SettlementPrice { get; set; }
        public decimal CompositeData { get; set; }
        public int FileRow { get; set; }

        public decimal[] LossScenarios { get; set; }

        public ContractExpiryDetail()
        {
            LossScenarios = new decimal[16];

        }
    }







    internal class TmpEnclear
    {
        public string ContractCode { get; set; }
        public string Contract { get; set; }
        public string ScanningRange { get; set; }
        public decimal IntermonthSpreadCharge { get; set; }
        public decimal VolatitilyRange { get; set; }
        public int ShortOptionMinimum { get; set; }
        public int SpotMonthChargeMethod { get; set; }
        public int MarginTopUpRate { get; set; }
        public int SpotMarginleviedFrom { get; set; }
        public DateTime DateEffectiveFrom { get; set; }
        public int FrontMonthsForApplicationOfSpotMonthCharge { get; set; }
    }

    internal class TmpScanningRange
    {
        public string ContractCode { get; set; }
        public string Contract { get; set; }
        public decimal ScanningRange { get; set; }
        public List<TierDiscount> TierDiscounts { get; set; }

        public TmpScanningRange()
        {
            TierDiscounts = new List<TierDiscount>();
        }

    }

    internal class TierDiscount
    {
        public decimal Rate { get; set; }
        public string DateRange { get; set; }
        public DateTime TierFrom { get; set; }
        public DateTime TierTo { get; set; }
    }

    internal class TmpIntercommodityCredits
    {
        public string Code1 { get; set; }
        public int DeltaRatioCode1 { get; set; }

        public string Code2 { get; set; }
        public int DeltaRatioCode2 { get; set; }

        public int Properties { get; set; }
        public int CreditPerLeg { get; set; }
        public DateTime DateEffectiveForm { get; set; }
    }

    internal class RiskArrayParameter
    {
        public short RiskScenario { get; set; }
        public decimal PriceVariation { get; set; }
        public int VolatilityVariation { get; set; }
        public decimal WeightFraction { get; set; }
    }

    internal class IndexLossScenarios
    {
        public string Index { get; set; }
        public string Expiry { get; set; }
        public string CallPut { get; set; }
        public decimal Strike { get; set; }
        public int FileRow { get; set; }
        public decimal SettlementPrice { get; set; }
        public decimal CompositeDelta { get; set; }

        public decimal LossScenario1 { get; set; }
        public decimal LossScenario2 { get; set; }
        public decimal LossScenario3 { get; set; }
        public decimal LossScenario4 { get; set; }
        public decimal LossScenario5 { get; set; }
        public decimal LossScenario6 { get; set; }
        public decimal LossScenario7 { get; set; }
        public decimal LossScenario8 { get; set; }
        public decimal LossScenario9 { get; set; }
        public decimal LossScenario10 { get; set; }
        public decimal LossScenario11 { get; set; }
        public decimal LossScenario12 { get; set; }
        public decimal LossScenario13 { get; set; }
        public decimal LossScenario14 { get; set; }
        public decimal LossScenario15 { get; set; }
        public decimal LossScenario16 { get; set; }
    }

    internal class InitialMargin
    {
        public string Index { get; set; }
        public string SpanCode { get; set; }
        public decimal ScanningRisk { get; set; }
        public decimal IntermonthSpreadCharge { get; set; }
        public decimal SpotMonthCharge { get; set; }
        public decimal InterContractCredit { get; set; }
        public decimal ShortOptionMinimun { get; set; }
        public decimal InitialMarginValue { get; set; }
    }

    public class OptionPositionValues
    {
        public string Index { get; set; }
        public string CallOrPut { get; set; }
        public decimal StrikePrice { get; set; }

        public Dictionary<DateTime, decimal> PosValues;
    }

    internal class DeltaValues
    {
        public string Code { get; set; }

        public Dictionary<DateTime, decimal> DateValues { get; set; }

        public DeltaValues()
        {
            DateValues = new Dictionary<DateTime, decimal>();
        }
    }

    #endregion
}
