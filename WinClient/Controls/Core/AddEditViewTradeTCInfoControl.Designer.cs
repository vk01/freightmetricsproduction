﻿namespace Exis.WinClient.Controls.Core
{
    partial class AddEditViewTradeTCInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditViewTradeTCInfoControl));
            this.layoutRootControl = new DevExpress.XtraLayout.LayoutControl();
            this.chkIsBareboat = new DevExpress.XtraEditors.CheckEdit();
            this.txtBallastBonus = new DevExpress.XtraEditors.SpinEdit();
            this.txtDelivery = new DevExpress.XtraEditors.TextEdit();
            this.lookupVessel = new DevExpress.XtraEditors.LookUpEdit();
            this.txtAddress = new DevExpress.XtraEditors.SpinEdit();
            this.panelLegs = new DevExpress.XtraEditors.PanelControl();
            this.layoutLegsControl = new DevExpress.XtraLayout.LayoutControl();
            this.chkIsOptional_1 = new DevExpress.XtraEditors.CheckEdit();
            this.txtRedelivDays_1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtIndexPerc_1 = new DevExpress.XtraEditors.SpinEdit();
            this.lookUpIndex_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.btnRemoveBroker = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnAddLeg = new DevExpress.XtraEditors.SimpleButton();
            this.cmbRateType_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbStatus_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpPeriodTo_1 = new DevExpress.XtraEditors.DateEdit();
            this.dtpPeriodFrom_1 = new DevExpress.XtraEditors.DateEdit();
            this.txtRate_1 = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutLegsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutLeg_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPeriodFrom_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodTo_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRate_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStatus_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRateType_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIndex_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIndexPerc_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRedelivDays_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptional_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAddTradeLeg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRemoveTradeLeg = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtVesselIndex = new DevExpress.XtraEditors.SpinEdit();
            this.txtRedelivery = new DevExpress.XtraEditors.TextEdit();
            this.cmbType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRedelivery = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTradeLegs = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVesselIndex = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVessel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDelivery = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBallastBonus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIsBareboat = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).BeginInit();
            this.layoutRootControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsBareboat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBallastBonus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupVessel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelLegs)).BeginInit();
            this.panelLegs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsControl)).BeginInit();
            this.layoutLegsControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsOptional_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRedelivDays_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndexPerc_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpIndex_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbRateType_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLeg_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodFrom_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodTo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRateType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndex_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndexPerc_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRedelivDays_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptional_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddTradeLeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRemoveTradeLeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVesselIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRedelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRedelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeLegs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVesselIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVessel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBallastBonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIsBareboat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRootControl
            // 
            this.layoutRootControl.AllowCustomizationMenu = false;
            this.layoutRootControl.Controls.Add(this.chkIsBareboat);
            this.layoutRootControl.Controls.Add(this.txtBallastBonus);
            this.layoutRootControl.Controls.Add(this.txtDelivery);
            this.layoutRootControl.Controls.Add(this.lookupVessel);
            this.layoutRootControl.Controls.Add(this.txtAddress);
            this.layoutRootControl.Controls.Add(this.panelLegs);
            this.layoutRootControl.Controls.Add(this.txtVesselIndex);
            this.layoutRootControl.Controls.Add(this.txtRedelivery);
            this.layoutRootControl.Controls.Add(this.cmbType);
            this.layoutRootControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRootControl.Location = new System.Drawing.Point(0, 0);
            this.layoutRootControl.Name = "layoutRootControl";
            this.layoutRootControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(400, 332, 250, 350);
            this.layoutRootControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRootControl.Root = this.layoutControlGroup1;
            this.layoutRootControl.Size = new System.Drawing.Size(1232, 204);
            this.layoutRootControl.TabIndex = 0;
            this.layoutRootControl.Text = "layoutControl1";
            // 
            // chkIsBareboat
            // 
            this.chkIsBareboat.Location = new System.Drawing.Point(1124, 2);
            this.chkIsBareboat.Name = "chkIsBareboat";
            this.chkIsBareboat.Properties.Caption = global::Exis.WinClient.Strings.sf;
            this.chkIsBareboat.Size = new System.Drawing.Size(106, 19);
            this.chkIsBareboat.StyleController = this.layoutRootControl;
            this.chkIsBareboat.TabIndex = 15;
            // 
            // txtBallastBonus
            // 
            this.txtBallastBonus.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBallastBonus.Location = new System.Drawing.Point(1169, 26);
            this.txtBallastBonus.Name = "txtBallastBonus";
            this.txtBallastBonus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBallastBonus.Size = new System.Drawing.Size(61, 20);
            this.txtBallastBonus.StyleController = this.layoutRootControl;
            this.txtBallastBonus.TabIndex = 14;
            // 
            // txtDelivery
            // 
            this.txtDelivery.Location = new System.Drawing.Point(48, 26);
            this.txtDelivery.Name = "txtDelivery";
            this.txtDelivery.Properties.MaxLength = 1000;
            this.txtDelivery.Size = new System.Drawing.Size(412, 20);
            this.txtDelivery.StyleController = this.layoutRootControl;
            this.txtDelivery.TabIndex = 8;
            // 
            // lookupVessel
            // 
            this.lookupVessel.Location = new System.Drawing.Point(195, 2);
            this.lookupVessel.Name = "lookupVessel";
            this.lookupVessel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupVessel.Size = new System.Drawing.Size(727, 20);
            this.lookupVessel.StyleController = this.layoutRootControl;
            this.lookupVessel.TabIndex = 13;
            this.lookupVessel.EditValueChanged += new System.EventHandler(this.lookupVessel_EditValueChanged);
            // 
            // txtAddress
            // 
            this.txtAddress.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAddress.Location = new System.Drawing.Point(1034, 26);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAddress.Properties.Mask.EditMask = "f4";
            this.txtAddress.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAddress.Properties.MaxLength = 8;
            this.txtAddress.Size = new System.Drawing.Size(61, 20);
            this.txtAddress.StyleController = this.layoutRootControl;
            this.txtAddress.TabIndex = 11;
            // 
            // panelLegs
            // 
            this.panelLegs.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.panelLegs.Controls.Add(this.layoutLegsControl);
            this.panelLegs.Location = new System.Drawing.Point(31, 50);
            this.panelLegs.MinimumSize = new System.Drawing.Size(0, 120);
            this.panelLegs.Name = "panelLegs";
            this.panelLegs.Size = new System.Drawing.Size(1199, 152);
            this.panelLegs.TabIndex = 10;
            // 
            // layoutLegsControl
            // 
            this.layoutLegsControl.AllowCustomizationMenu = false;
            this.layoutLegsControl.Controls.Add(this.chkIsOptional_1);
            this.layoutLegsControl.Controls.Add(this.txtRedelivDays_1);
            this.layoutLegsControl.Controls.Add(this.txtIndexPerc_1);
            this.layoutLegsControl.Controls.Add(this.lookUpIndex_1);
            this.layoutLegsControl.Controls.Add(this.btnRemoveBroker);
            this.layoutLegsControl.Controls.Add(this.btnAddLeg);
            this.layoutLegsControl.Controls.Add(this.cmbRateType_1);
            this.layoutLegsControl.Controls.Add(this.cmbStatus_1);
            this.layoutLegsControl.Controls.Add(this.dtpPeriodTo_1);
            this.layoutLegsControl.Controls.Add(this.dtpPeriodFrom_1);
            this.layoutLegsControl.Controls.Add(this.txtRate_1);
            this.layoutLegsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutLegsControl.Location = new System.Drawing.Point(2, 2);
            this.layoutLegsControl.Name = "layoutLegsControl";
            this.layoutLegsControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(469, 306, 250, 350);
            this.layoutLegsControl.Root = this.layoutControlGroup2;
            this.layoutLegsControl.Size = new System.Drawing.Size(1195, 148);
            this.layoutLegsControl.TabIndex = 0;
            this.layoutLegsControl.Text = "layoutControl2";
            // 
            // chkIsOptional_1
            // 
            this.chkIsOptional_1.Location = new System.Drawing.Point(991, 2);
            this.chkIsOptional_1.Name = "chkIsOptional_1";
            this.chkIsOptional_1.Properties.Caption = global::Exis.WinClient.Strings.sf;
            this.chkIsOptional_1.Size = new System.Drawing.Size(23, 19);
            this.chkIsOptional_1.StyleController = this.layoutLegsControl;
            this.chkIsOptional_1.TabIndex = 15;
            this.chkIsOptional_1.CheckedChanged += new System.EventHandler(this.chkIsOptional_1_CheckedChanged);
            // 
            // txtRedelivDays_1
            // 
            this.txtRedelivDays_1.EditValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.txtRedelivDays_1.Location = new System.Drawing.Point(900, 2);
            this.txtRedelivDays_1.Name = "txtRedelivDays_1";
            this.txtRedelivDays_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRedelivDays_1.Properties.Mask.EditMask = "D";
            this.txtRedelivDays_1.Properties.MaxLength = 2;
            this.txtRedelivDays_1.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtRedelivDays_1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtRedelivDays_1.Size = new System.Drawing.Size(36, 20);
            this.txtRedelivDays_1.StyleController = this.layoutLegsControl;
            this.txtRedelivDays_1.TabIndex = 8;
            // 
            // txtIndexPerc_1
            // 
            this.txtIndexPerc_1.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtIndexPerc_1.Location = new System.Drawing.Point(755, 2);
            this.txtIndexPerc_1.Name = "txtIndexPerc_1";
            this.txtIndexPerc_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtIndexPerc_1.Properties.Mask.EditMask = "P2";
            this.txtIndexPerc_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtIndexPerc_1.Properties.MaxLength = 6;
            this.txtIndexPerc_1.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtIndexPerc_1.Size = new System.Drawing.Size(68, 20);
            this.txtIndexPerc_1.StyleController = this.layoutLegsControl;
            this.txtIndexPerc_1.TabIndex = 12;
            // 
            // lookUpIndex_1
            // 
            this.lookUpIndex_1.Location = new System.Drawing.Point(580, 2);
            this.lookUpIndex_1.Name = "lookUpIndex_1";
            this.lookUpIndex_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpIndex_1.Properties.NullText = global::Exis.WinClient.Strings.sf;
            this.lookUpIndex_1.Size = new System.Drawing.Size(98, 20);
            this.lookUpIndex_1.StyleController = this.layoutLegsControl;
            this.lookUpIndex_1.TabIndex = 14;
            // 
            // btnRemoveBroker
            // 
            this.btnRemoveBroker.ImageIndex = 1;
            this.btnRemoveBroker.ImageList = this.imageList1;
            this.btnRemoveBroker.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveBroker.Location = new System.Drawing.Point(1176, 36);
            this.btnRemoveBroker.Name = "btnRemoveBroker";
            this.btnRemoveBroker.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveBroker.StyleController = this.layoutLegsControl;
            this.btnRemoveBroker.TabIndex = 12;
            this.btnRemoveBroker.Click += new System.EventHandler(this.btnRemoveLeg_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList1.Images.SetKeyName(1, "clear16x16.ico");
            // 
            // btnAddLeg
            // 
            this.btnAddLeg.ImageIndex = 0;
            this.btnAddLeg.ImageList = this.imageList1;
            this.btnAddLeg.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddLeg.Location = new System.Drawing.Point(1176, 2);
            this.btnAddLeg.Name = "btnAddLeg";
            this.btnAddLeg.Size = new System.Drawing.Size(30, 30);
            this.btnAddLeg.StyleController = this.layoutLegsControl;
            this.btnAddLeg.TabIndex = 11;
            this.btnAddLeg.Click += new System.EventHandler(this.btnAddLeg_Click);
            // 
            // cmbRateType_1
            // 
            this.cmbRateType_1.Location = new System.Drawing.Point(244, 2);
            this.cmbRateType_1.Name = "cmbRateType_1";
            this.cmbRateType_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbRateType_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbRateType_1.Size = new System.Drawing.Size(101, 20);
            this.cmbRateType_1.StyleController = this.layoutLegsControl;
            this.cmbRateType_1.TabIndex = 7;
            this.cmbRateType_1.SelectedIndexChanged += new System.EventHandler(this.cmbRateType_SelectedIndexChanged);
            // 
            // cmbStatus_1
            // 
            this.cmbStatus_1.Location = new System.Drawing.Point(1091, 2);
            this.cmbStatus_1.Name = "cmbStatus_1";
            this.cmbStatus_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus_1.Size = new System.Drawing.Size(81, 20);
            this.cmbStatus_1.StyleController = this.layoutLegsControl;
            this.cmbStatus_1.TabIndex = 9;
            // 
            // dtpPeriodTo_1
            // 
            this.dtpPeriodTo_1.EditValue = null;
            this.dtpPeriodTo_1.Location = new System.Drawing.Point(66, 2);
            this.dtpPeriodTo_1.Name = "dtpPeriodTo_1";
            this.dtpPeriodTo_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpPeriodTo_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpPeriodTo_1.Size = new System.Drawing.Size(101, 20);
            this.dtpPeriodTo_1.StyleController = this.layoutLegsControl;
            this.dtpPeriodTo_1.TabIndex = 5;
            // 
            // dtpPeriodFrom_1
            // 
            this.dtpPeriodFrom_1.EditValue = null;
            this.dtpPeriodFrom_1.Location = new System.Drawing.Point(-112, 2);
            this.dtpPeriodFrom_1.Name = "dtpPeriodFrom_1";
            this.dtpPeriodFrom_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpPeriodFrom_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpPeriodFrom_1.Size = new System.Drawing.Size(101, 20);
            this.dtpPeriodFrom_1.StyleController = this.layoutLegsControl;
            this.dtpPeriodFrom_1.TabIndex = 4;
            // 
            // txtRate_1
            // 
            this.txtRate_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRate_1.Location = new System.Drawing.Point(422, 2);
            this.txtRate_1.Name = "txtRate_1";
            this.txtRate_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRate_1.Properties.Mask.EditMask = "f2";
            this.txtRate_1.Properties.MaxLength = 9;
            this.txtRate_1.Size = new System.Drawing.Size(81, 20);
            this.txtRate_1.StyleController = this.layoutLegsControl;
            this.txtRate_1.TabIndex = 7;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutLegsGroup,
            this.layoutAddTradeLeg,
            this.layoutRemoveTradeLeg});
            this.layoutControlGroup2.Location = new System.Drawing.Point(-187, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1395, 131);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutLegsGroup
            // 
            this.layoutLegsGroup.CustomizationFormText = "layoutLegs";
            this.layoutLegsGroup.GroupBordersVisible = false;
            this.layoutLegsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutLeg_1});
            this.layoutLegsGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutLegsGroup.Name = "layoutLegsGroup";
            this.layoutLegsGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutLegsGroup.Size = new System.Drawing.Size(1361, 131);
            this.layoutLegsGroup.Text = "layoutLegsGroup";
            this.layoutLegsGroup.TextVisible = false;
            // 
            // layoutLeg_1
            // 
            this.layoutLeg_1.CustomizationFormText = "layoutLeg";
            this.layoutLeg_1.GroupBordersVisible = false;
            this.layoutLeg_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPeriodFrom_1,
            this.layoutPeriodTo_1,
            this.layoutRate_1,
            this.layoutStatus_1,
            this.layoutRateType_1,
            this.layoutIndex_1,
            this.layoutIndexPerc_1,
            this.layoutRedelivDays_1,
            this.layoutOptional_1});
            this.layoutLeg_1.Location = new System.Drawing.Point(0, 0);
            this.layoutLeg_1.Name = "layoutLeg_1";
            this.layoutLeg_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutLeg_1.Size = new System.Drawing.Size(1361, 131);
            this.layoutLeg_1.Tag = "1";
            this.layoutLeg_1.Text = "layoutLeg_1";
            this.layoutLeg_1.TextVisible = false;
            // 
            // layoutPeriodFrom_1
            // 
            this.layoutPeriodFrom_1.Control = this.dtpPeriodFrom_1;
            this.layoutPeriodFrom_1.CustomizationFormText = "Period From:";
            this.layoutPeriodFrom_1.Location = new System.Drawing.Point(0, 0);
            this.layoutPeriodFrom_1.MaxSize = new System.Drawing.Size(178, 24);
            this.layoutPeriodFrom_1.MinSize = new System.Drawing.Size(178, 24);
            this.layoutPeriodFrom_1.Name = "layoutPeriodFrom_1";
            this.layoutPeriodFrom_1.Size = new System.Drawing.Size(178, 131);
            this.layoutPeriodFrom_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodFrom_1.Text = "Period From:";
            this.layoutPeriodFrom_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutPeriodTo_1
            // 
            this.layoutPeriodTo_1.Control = this.dtpPeriodTo_1;
            this.layoutPeriodTo_1.CustomizationFormText = "Period To:";
            this.layoutPeriodTo_1.Location = new System.Drawing.Point(178, 0);
            this.layoutPeriodTo_1.MaxSize = new System.Drawing.Size(178, 24);
            this.layoutPeriodTo_1.MinSize = new System.Drawing.Size(178, 24);
            this.layoutPeriodTo_1.Name = "layoutPeriodTo_1";
            this.layoutPeriodTo_1.Size = new System.Drawing.Size(178, 131);
            this.layoutPeriodTo_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodTo_1.Text = "Period To:";
            this.layoutPeriodTo_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutRate_1
            // 
            this.layoutRate_1.Control = this.txtRate_1;
            this.layoutRate_1.CustomizationFormText = "Rate($/day):";
            this.layoutRate_1.Location = new System.Drawing.Point(534, 0);
            this.layoutRate_1.MaxSize = new System.Drawing.Size(158, 24);
            this.layoutRate_1.MinSize = new System.Drawing.Size(158, 24);
            this.layoutRate_1.Name = "layoutRate_1";
            this.layoutRate_1.Size = new System.Drawing.Size(158, 131);
            this.layoutRate_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRate_1.Text = "Rate($/day):";
            this.layoutRate_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutStatus_1
            // 
            this.layoutStatus_1.Control = this.cmbStatus_1;
            this.layoutStatus_1.CustomizationFormText = "Status:";
            this.layoutStatus_1.Location = new System.Drawing.Point(1203, 0);
            this.layoutStatus_1.MaxSize = new System.Drawing.Size(158, 24);
            this.layoutStatus_1.MinSize = new System.Drawing.Size(158, 24);
            this.layoutStatus_1.Name = "layoutStatus_1";
            this.layoutStatus_1.Size = new System.Drawing.Size(158, 131);
            this.layoutStatus_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutStatus_1.Text = "Status:";
            this.layoutStatus_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutRateType_1
            // 
            this.layoutRateType_1.Control = this.cmbRateType_1;
            this.layoutRateType_1.CustomizationFormText = "Rate Type:";
            this.layoutRateType_1.Location = new System.Drawing.Point(356, 0);
            this.layoutRateType_1.MaxSize = new System.Drawing.Size(178, 24);
            this.layoutRateType_1.MinSize = new System.Drawing.Size(178, 24);
            this.layoutRateType_1.Name = "layoutRateType_1";
            this.layoutRateType_1.Size = new System.Drawing.Size(178, 131);
            this.layoutRateType_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRateType_1.Text = "Rate Type:";
            this.layoutRateType_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutIndex_1
            // 
            this.layoutIndex_1.Control = this.lookUpIndex_1;
            this.layoutIndex_1.CustomizationFormText = "Index:";
            this.layoutIndex_1.Location = new System.Drawing.Point(692, 0);
            this.layoutIndex_1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutIndex_1.MinSize = new System.Drawing.Size(175, 24);
            this.layoutIndex_1.Name = "layoutIndex_1";
            this.layoutIndex_1.Size = new System.Drawing.Size(175, 131);
            this.layoutIndex_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutIndex_1.Text = "Index:";
            this.layoutIndex_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutIndexPerc_1
            // 
            this.layoutIndexPerc_1.Control = this.txtIndexPerc_1;
            this.layoutIndexPerc_1.CustomizationFormText = "Index Perc.:";
            this.layoutIndexPerc_1.Location = new System.Drawing.Point(867, 0);
            this.layoutIndexPerc_1.MaxSize = new System.Drawing.Size(145, 24);
            this.layoutIndexPerc_1.MinSize = new System.Drawing.Size(145, 24);
            this.layoutIndexPerc_1.Name = "layoutIndexPerc_1";
            this.layoutIndexPerc_1.Size = new System.Drawing.Size(145, 131);
            this.layoutIndexPerc_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutIndexPerc_1.Text = "Index Perc.:";
            this.layoutIndexPerc_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutRedelivDays_1
            // 
            this.layoutRedelivDays_1.Control = this.txtRedelivDays_1;
            this.layoutRedelivDays_1.CustomizationFormText = "Redeliv. Days:";
            this.layoutRedelivDays_1.Location = new System.Drawing.Point(1012, 0);
            this.layoutRedelivDays_1.MaxSize = new System.Drawing.Size(113, 24);
            this.layoutRedelivDays_1.MinSize = new System.Drawing.Size(113, 24);
            this.layoutRedelivDays_1.Name = "layoutRedelivDays_1";
            this.layoutRedelivDays_1.Size = new System.Drawing.Size(113, 131);
            this.layoutRedelivDays_1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRedelivDays_1.Text = "Redeliv. Days:";
            this.layoutRedelivDays_1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutOptional_1
            // 
            this.layoutOptional_1.Control = this.chkIsOptional_1;
            this.layoutOptional_1.CustomizationFormText = "Optional:";
            this.layoutOptional_1.Location = new System.Drawing.Point(1125, 0);
            this.layoutOptional_1.Name = "layoutOptional_1";
            this.layoutOptional_1.Size = new System.Drawing.Size(78, 131);
            this.layoutOptional_1.Text = "Optional:";
            this.layoutOptional_1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptional_1.TextSize = new System.Drawing.Size(50, 20);
            this.layoutOptional_1.TextToControlDistance = 1;
            // 
            // layoutAddTradeLeg
            // 
            this.layoutAddTradeLeg.Control = this.btnAddLeg;
            this.layoutAddTradeLeg.CustomizationFormText = "layoutAddTradeLeg";
            this.layoutAddTradeLeg.Location = new System.Drawing.Point(1361, 0);
            this.layoutAddTradeLeg.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutAddTradeLeg.MinSize = new System.Drawing.Size(34, 34);
            this.layoutAddTradeLeg.Name = "layoutAddTradeLeg";
            this.layoutAddTradeLeg.Size = new System.Drawing.Size(34, 34);
            this.layoutAddTradeLeg.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAddTradeLeg.Text = "layoutAddTradeLeg";
            this.layoutAddTradeLeg.TextSize = new System.Drawing.Size(0, 0);
            this.layoutAddTradeLeg.TextToControlDistance = 0;
            this.layoutAddTradeLeg.TextVisible = false;
            // 
            // layoutRemoveTradeLeg
            // 
            this.layoutRemoveTradeLeg.Control = this.btnRemoveBroker;
            this.layoutRemoveTradeLeg.CustomizationFormText = "layoutRemoveTradeLeg";
            this.layoutRemoveTradeLeg.Location = new System.Drawing.Point(1361, 34);
            this.layoutRemoveTradeLeg.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutRemoveTradeLeg.MinSize = new System.Drawing.Size(34, 34);
            this.layoutRemoveTradeLeg.Name = "layoutRemoveTradeLeg";
            this.layoutRemoveTradeLeg.Size = new System.Drawing.Size(34, 97);
            this.layoutRemoveTradeLeg.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutRemoveTradeLeg.Text = "layoutRemoveTradeLeg";
            this.layoutRemoveTradeLeg.TextSize = new System.Drawing.Size(0, 0);
            this.layoutRemoveTradeLeg.TextToControlDistance = 0;
            this.layoutRemoveTradeLeg.TextVisible = false;
            // 
            // txtVesselIndex
            // 
            this.txtVesselIndex.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtVesselIndex.Location = new System.Drawing.Point(994, 2);
            this.txtVesselIndex.Name = "txtVesselIndex";
            this.txtVesselIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVesselIndex.Size = new System.Drawing.Size(63, 20);
            this.txtVesselIndex.StyleController = this.layoutRootControl;
            this.txtVesselIndex.TabIndex = 8;
            // 
            // txtRedelivery
            // 
            this.txtRedelivery.Location = new System.Drawing.Point(522, 26);
            this.txtRedelivery.Name = "txtRedelivery";
            this.txtRedelivery.Properties.MaxLength = 1000;
            this.txtRedelivery.Size = new System.Drawing.Size(462, 20);
            this.txtRedelivery.StyleController = this.layoutRootControl;
            this.txtRedelivery.TabIndex = 7;
            // 
            // cmbType
            // 
            this.cmbType.Location = new System.Drawing.Point(33, 2);
            this.cmbType.Name = "cmbType";
            this.cmbType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbType.Size = new System.Drawing.Size(121, 20);
            this.cmbType.StyleController = this.layoutRootControl;
            this.cmbType.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutType,
            this.layoutRedelivery,
            this.layoutTradeLegs,
            this.layoutAddress,
            this.layoutVesselIndex,
            this.layoutVessel,
            this.layoutDelivery,
            this.layoutBallastBonus,
            this.layoutIsBareboat});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1232, 204);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutType
            // 
            this.layoutType.Control = this.cmbType;
            this.layoutType.CustomizationFormText = "Type:";
            this.layoutType.Location = new System.Drawing.Point(0, 0);
            this.layoutType.MaxSize = new System.Drawing.Size(156, 24);
            this.layoutType.MinSize = new System.Drawing.Size(156, 24);
            this.layoutType.Name = "layoutType";
            this.layoutType.Size = new System.Drawing.Size(156, 24);
            this.layoutType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutType.Text = "Type:";
            this.layoutType.TextSize = new System.Drawing.Size(28, 13);
            // 
            // layoutRedelivery
            // 
            this.layoutRedelivery.Control = this.txtRedelivery;
            this.layoutRedelivery.CustomizationFormText = "Redelivery:";
            this.layoutRedelivery.Location = new System.Drawing.Point(462, 24);
            this.layoutRedelivery.Name = "layoutRedelivery";
            this.layoutRedelivery.Size = new System.Drawing.Size(524, 24);
            this.layoutRedelivery.Text = "Redelivery:";
            this.layoutRedelivery.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutTradeLegs
            // 
            this.layoutTradeLegs.Control = this.panelLegs;
            this.layoutTradeLegs.CustomizationFormText = "layoutControlItem1";
            this.layoutTradeLegs.Location = new System.Drawing.Point(0, 48);
            this.layoutTradeLegs.Name = "layoutTradeLegs";
            this.layoutTradeLegs.Size = new System.Drawing.Size(1232, 156);
            this.layoutTradeLegs.Text = "Legs:";
            this.layoutTradeLegs.TextSize = new System.Drawing.Size(26, 13);
            // 
            // layoutAddress
            // 
            this.layoutAddress.Control = this.txtAddress;
            this.layoutAddress.CustomizationFormText = "Address(%):";
            this.layoutAddress.Location = new System.Drawing.Point(986, 24);
            this.layoutAddress.MaxSize = new System.Drawing.Size(111, 24);
            this.layoutAddress.MinSize = new System.Drawing.Size(111, 24);
            this.layoutAddress.Name = "layoutAddress";
            this.layoutAddress.Size = new System.Drawing.Size(111, 24);
            this.layoutAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAddress.Text = "Address:";
            this.layoutAddress.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutVesselIndex
            // 
            this.layoutVesselIndex.Control = this.txtVesselIndex;
            this.layoutVesselIndex.CustomizationFormText = "Vessel Index:";
            this.layoutVesselIndex.Location = new System.Drawing.Point(924, 0);
            this.layoutVesselIndex.MaxSize = new System.Drawing.Size(135, 24);
            this.layoutVesselIndex.MinSize = new System.Drawing.Size(133, 24);
            this.layoutVesselIndex.Name = "layoutVesselIndex";
            this.layoutVesselIndex.Size = new System.Drawing.Size(135, 24);
            this.layoutVesselIndex.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutVesselIndex.Text = "Vessel Index:";
            this.layoutVesselIndex.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutVessel
            // 
            this.layoutVessel.Control = this.lookupVessel;
            this.layoutVessel.CustomizationFormText = "Vessel:";
            this.layoutVessel.Location = new System.Drawing.Point(156, 0);
            this.layoutVessel.Name = "layoutVessel";
            this.layoutVessel.Size = new System.Drawing.Size(768, 24);
            this.layoutVessel.Text = "Vessel:";
            this.layoutVessel.TextSize = new System.Drawing.Size(34, 13);
            // 
            // layoutDelivery
            // 
            this.layoutDelivery.Control = this.txtDelivery;
            this.layoutDelivery.CustomizationFormText = "Delivery:";
            this.layoutDelivery.Location = new System.Drawing.Point(0, 24);
            this.layoutDelivery.Name = "layoutDelivery";
            this.layoutDelivery.Size = new System.Drawing.Size(462, 24);
            this.layoutDelivery.Text = "Delivery:";
            this.layoutDelivery.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutBallastBonus
            // 
            this.layoutBallastBonus.Control = this.txtBallastBonus;
            this.layoutBallastBonus.CustomizationFormText = "Ballast Bonus:";
            this.layoutBallastBonus.Location = new System.Drawing.Point(1097, 24);
            this.layoutBallastBonus.MaxSize = new System.Drawing.Size(135, 24);
            this.layoutBallastBonus.MinSize = new System.Drawing.Size(135, 24);
            this.layoutBallastBonus.Name = "layoutBallastBonus";
            this.layoutBallastBonus.Size = new System.Drawing.Size(135, 24);
            this.layoutBallastBonus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutBallastBonus.Text = "Ballast Bonus:";
            this.layoutBallastBonus.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutIsBareboat
            // 
            this.layoutIsBareboat.Control = this.chkIsBareboat;
            this.layoutIsBareboat.CustomizationFormText = "Is Bareboat:";
            this.layoutIsBareboat.Location = new System.Drawing.Point(1059, 0);
            this.layoutIsBareboat.Name = "layoutIsBareboat";
            this.layoutIsBareboat.Size = new System.Drawing.Size(173, 24);
            this.layoutIsBareboat.Text = "Is Bareboat:";
            this.layoutIsBareboat.TextSize = new System.Drawing.Size(60, 13);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // AddEditViewTradeTCInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutRootControl);
            this.Name = "AddEditViewTradeTCInfoControl";
            this.Size = new System.Drawing.Size(1232, 204);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootControl)).EndInit();
            this.layoutRootControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsBareboat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBallastBonus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupVessel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelLegs)).EndInit();
            this.panelLegs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsControl)).EndInit();
            this.layoutLegsControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsOptional_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRedelivDays_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndexPerc_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpIndex_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbRateType_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodTo_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPeriodFrom_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLegsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLeg_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodFrom_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodTo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStatus_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRateType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndex_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIndexPerc_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRedelivDays_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptional_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddTradeLeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRemoveTradeLeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVesselIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRedelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRedelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeLegs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVesselIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVessel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBallastBonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIsBareboat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRootControl;
        private DevExpress.XtraEditors.SpinEdit txtVesselIndex;
        private DevExpress.XtraEditors.TextEdit txtRedelivery;
        private DevExpress.XtraEditors.ComboBoxEdit cmbType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutType;
        private DevExpress.XtraLayout.LayoutControlItem layoutRedelivery;
        private DevExpress.XtraLayout.LayoutControlItem layoutVesselIndex;
        private DevExpress.XtraEditors.PanelControl panelLegs;
        private DevExpress.XtraLayout.LayoutControlItem layoutTradeLegs;
        private DevExpress.XtraEditors.SpinEdit txtAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutAddress;
        private DevExpress.XtraLayout.LayoutControl layoutLegsControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DateEdit dtpPeriodFrom_1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutLegsGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodFrom_1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutLeg_1;
        private DevExpress.XtraEditors.DateEdit dtpPeriodTo_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodTo_1;
        private DevExpress.XtraEditors.SpinEdit txtRate_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRate_1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutStatus_1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.LookUpEdit lookupVessel;
        private DevExpress.XtraLayout.LayoutControlItem layoutVessel;
        private DevExpress.XtraEditors.ComboBoxEdit cmbRateType_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRateType_1;
        private DevExpress.XtraEditors.TextEdit txtDelivery;
        private DevExpress.XtraLayout.LayoutControlItem layoutDelivery;
        private DevExpress.XtraEditors.SimpleButton btnAddLeg;
        private DevExpress.XtraLayout.LayoutControlItem layoutAddTradeLeg;
        private DevExpress.XtraEditors.SimpleButton btnRemoveBroker;
        private DevExpress.XtraLayout.LayoutControlItem layoutRemoveTradeLeg;
        private DevExpress.XtraEditors.LookUpEdit lookUpIndex_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutIndex_1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.SpinEdit txtBallastBonus;
        private DevExpress.XtraLayout.LayoutControlItem layoutBallastBonus;
        private DevExpress.XtraEditors.CheckEdit chkIsBareboat;
        private DevExpress.XtraLayout.LayoutControlItem layoutIsBareboat;
        private DevExpress.XtraEditors.SpinEdit txtIndexPerc_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutIndexPerc_1;
        private DevExpress.XtraEditors.SpinEdit txtRedelivDays_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutRedelivDays_1;
        private DevExpress.XtraEditors.CheckEdit chkIsOptional_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptional_1;

    }
}
