
namespace Exis.WinClient.Controls.Core
{
    partial class ViewTradesForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewTradesForm));
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.gridTradeDetails = new DevExpress.XtraGrid.GridControl();
            this.gridTradeDetailsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutGroupRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitterVertical = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemTradeDetails = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.tbEntities = new DevExpress.XtraTab.XtraTabControl();
            this.tbpCompanies = new DevExpress.XtraTab.XtraTabPage();
            this.treeNavigation = new DevExpress.XtraTreeList.TreeList();
            this.imageList16 = new DevExpress.Utils.ImageCollection(this.components);
            this.tbpBooks = new DevExpress.XtraTab.XtraTabPage();
            this.treeBookNavigation = new DevExpress.XtraTreeList.TreeList();
            this.chkFilterOptional = new DevExpress.XtraEditors.CheckEdit();
            this.chkFilterMinimum = new DevExpress.XtraEditors.CheckEdit();
            this.chkFilterDraft = new DevExpress.XtraEditors.CheckEdit();
            this.btnFilterTrades = new DevExpress.XtraEditors.SimpleButton();
            this.dtpFilterPeriodTo = new DevExpress.XtraEditors.DateEdit();
            this.dtpFilterPeriodFrom = new DevExpress.XtraEditors.DateEdit();
            this.dtpFilterPositionDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlRoot = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnGeneratePosition = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnMarkToMarket = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnGenerateMonteCarlo = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnRatiosReport = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnGenerateCashFlow = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnViewTrade = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnEditTrade = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnDeleteTrade = new DevExpress.XtraBars.BarLargeButtonItem();
            this.BtnActivateTrades = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnExport = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.chkOnlyNonZeroTotalDays = new DevExpress.XtraEditors.CheckEdit();
            this.btnCpmRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetailsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterVertical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEntities)).BeginInit();
            this.tbEntities.SuspendLayout();
            this.tbpCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeNavigation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).BeginInit();
            this.tbpBooks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeBookNavigation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterOptional.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterMinimum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterDraft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPositionDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPositionDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyNonZeroTotalDays.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.AllowCustomizationMenu = false;
            this.layoutRoot.Controls.Add(this.gridTradeDetails);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(267, 0);
            this.layoutRoot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1598, 126, 426, 731);
            this.layoutRoot.Root = this.layoutGroupRoot;
            this.layoutRoot.Size = new System.Drawing.Size(1155, 795);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // gridTradeDetails
            // 
            this.gridTradeDetails.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridTradeDetails.Location = new System.Drawing.Point(11, 28);
            this.gridTradeDetails.MainView = this.gridTradeDetailsView;
            this.gridTradeDetails.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridTradeDetails.Name = "gridTradeDetails";
            this.gridTradeDetails.Size = new System.Drawing.Size(1138, 761);
            this.gridTradeDetails.TabIndex = 5;
            this.gridTradeDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridTradeDetailsView});
            // 
            // gridTradeDetailsView
            // 
            this.gridTradeDetailsView.GridControl = this.gridTradeDetails;
            this.gridTradeDetailsView.Name = "gridTradeDetailsView";
            this.gridTradeDetailsView.OptionsBehavior.Editable = false;
            this.gridTradeDetailsView.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridTradeDetailsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridTradeDetailsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridTradeDetailsView.OptionsView.ShowFooter = true;
            this.gridTradeDetailsView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridTradeDetailsViewFocusedRowChanged);
            this.gridTradeDetailsView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridTradeDetailsView_CustomUnboundColumnData);
            // 
            // layoutGroupRoot
            // 
            this.layoutGroupRoot.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutGroupRoot.GroupBordersVisible = false;
            this.layoutGroupRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.splitterVertical,
            this.layoutControlGroupTrades});
            this.layoutGroupRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupRoot.Name = "Root";
            this.layoutGroupRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupRoot.Size = new System.Drawing.Size(1155, 795);
            this.layoutGroupRoot.Text = "Root";
            this.layoutGroupRoot.TextVisible = false;
            // 
            // splitterVertical
            // 
            this.splitterVertical.AllowHotTrack = true;
            this.splitterVertical.CustomizationFormText = "splitter";
            this.splitterVertical.Location = new System.Drawing.Point(0, 0);
            this.splitterVertical.Name = "splitterVertical";
            this.splitterVertical.Size = new System.Drawing.Size(5, 795);
            // 
            // layoutControlGroupTrades
            // 
            this.layoutControlGroupTrades.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeDetails});
            this.layoutControlGroupTrades.Location = new System.Drawing.Point(5, 0);
            this.layoutControlGroupTrades.Name = "layoutControlGroupTrades";
            this.layoutControlGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Size = new System.Drawing.Size(1150, 795);
            this.layoutControlGroupTrades.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Text = "layoutControlGroupTrades";
            this.layoutControlGroupTrades.TextVisible = false;
            // 
            // layoutGroupTradeDetails
            // 
            this.layoutGroupTradeDetails.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupTradeDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemTradeDetails});
            this.layoutGroupTradeDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeDetails.Name = "layoutGroupTradeDetails";
            this.layoutGroupTradeDetails.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeDetails.Size = new System.Drawing.Size(1148, 793);
            this.layoutGroupTradeDetails.Text = "Trades";
            // 
            // layoutItemTradeDetails
            // 
            this.layoutItemTradeDetails.Control = this.gridTradeDetails;
            this.layoutItemTradeDetails.CustomizationFormText = "layoutItemTrades";
            this.layoutItemTradeDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutItemTradeDetails.MinSize = new System.Drawing.Size(50, 24);
            this.layoutItemTradeDetails.Name = "layoutItemTradeDetails";
            this.layoutItemTradeDetails.Size = new System.Drawing.Size(1142, 765);
            this.layoutItemTradeDetails.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemTradeDetails.Text = "Trades";
            this.layoutItemTradeDetails.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemTradeDetails.TextToControlDistance = 0;
            this.layoutItemTradeDetails.TextVisible = false;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            this.imageList24.Images.SetKeyName(6, "gantt-chart.ico");
            this.imageList24.Images.SetKeyName(7, "line-chart.ico");
            this.imageList24.Images.SetKeyName(8, "export24x24.ico");
            this.imageList24.Images.SetKeyName(9, "view24x24.ico");
            this.imageList24.Images.SetKeyName(10, "activate.ico");
            this.imageList24.Images.SetKeyName(11, "deactivate.ico");
            this.imageList24.Images.SetKeyName(12, "text_sum.ico");
            // 
            // tbEntities
            // 
            this.tbEntities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEntities.Location = new System.Drawing.Point(6, 250);
            this.tbEntities.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbEntities.Name = "tbEntities";
            this.tbEntities.SelectedTabPage = this.tbpCompanies;
            this.tbEntities.Size = new System.Drawing.Size(248, 509);
            this.tbEntities.TabIndex = 27;
            this.tbEntities.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbpCompanies,
            this.tbpBooks});
            this.tbEntities.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.tbEntities_SelectedPageChanging);
            // 
            // tbpCompanies
            // 
            this.tbpCompanies.AutoScroll = true;
            this.tbpCompanies.Controls.Add(this.treeNavigation);
            this.tbpCompanies.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbpCompanies.Name = "tbpCompanies";
            this.tbpCompanies.Size = new System.Drawing.Size(242, 478);
            this.tbpCompanies.Text = "Companies";
            // 
            // treeNavigation
            // 
            this.treeNavigation.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.treeNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeNavigation.Location = new System.Drawing.Point(0, 0);
            this.treeNavigation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeNavigation.Name = "treeNavigation";
            this.treeNavigation.OptionsBehavior.Editable = false;
            this.treeNavigation.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeNavigation.OptionsSelection.MultiSelect = true;
            this.treeNavigation.OptionsView.ShowCheckBoxes = true;
            this.treeNavigation.OptionsView.ShowColumns = false;
            this.treeNavigation.OptionsView.ShowHorzLines = false;
            this.treeNavigation.OptionsView.ShowIndicator = false;
            this.treeNavigation.OptionsView.ShowVertLines = false;
            this.treeNavigation.Size = new System.Drawing.Size(242, 478);
            this.treeNavigation.StateImageList = this.imageList16;
            this.treeNavigation.TabIndex = 1;
            this.treeNavigation.BeforeExpand += new DevExpress.XtraTreeList.BeforeExpandEventHandler(this.treeNavigation_BeforeExpand);
            this.treeNavigation.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.treeNavigation_AfterExpand);
            this.treeNavigation.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeNavigation_BeforeCheckNode);
            this.treeNavigation.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeNavigation_AfterCheckNode);
            this.treeNavigation.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeNavigation_FocusedNodeChanged);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.Images.SetKeyName(0, "company.ico");
            this.imageList16.Images.SetKeyName(1, "counterparty.ico");
            this.imageList16.Images.SetKeyName(2, "market.ico");
            this.imageList16.Images.SetKeyName(3, "trader.ico");
            this.imageList16.Images.SetKeyName(4, "vessel.ico");
            this.imageList16.Images.SetKeyName(5, "book.ico");
            this.imageList16.Images.SetKeyName(6, "pool.ico");
            this.imageList16.Images.SetKeyName(7, "vesselpool.ico");
            this.imageList16.Images.SetKeyName(8, "broker.ico");
            this.imageList16.Images.SetKeyName(9, "bulletblue.ico");
            this.imageList16.Images.SetKeyName(10, "house.ico");
            this.imageList16.Images.SetKeyName(11, "bullet_ball_glass_grey.ico");
            this.imageList16.Images.SetKeyName(12, "bullet_ball_glass_yellow.ico");
            this.imageList16.Images.SetKeyName(13, "bullet_ball_glass_green.ico");
            this.imageList16.Images.SetKeyName(14, "bullet_ball_glass_red.ico");
            this.imageList16.Images.SetKeyName(15, "calendar.ico");
            this.imageList16.Images.SetKeyName(16, "briefcase.ico");
            this.imageList16.Images.SetKeyName(17, "businessmen.ico");
            // 
            // tbpBooks
            // 
            this.tbpBooks.AutoScroll = true;
            this.tbpBooks.Controls.Add(this.treeBookNavigation);
            this.tbpBooks.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbpBooks.Name = "tbpBooks";
            this.tbpBooks.Size = new System.Drawing.Size(202, 472);
            this.tbpBooks.Text = "Books";
            // 
            // treeBookNavigation
            // 
            this.treeBookNavigation.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.treeBookNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeBookNavigation.Location = new System.Drawing.Point(0, 0);
            this.treeBookNavigation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeBookNavigation.Name = "treeBookNavigation";
            this.treeBookNavigation.OptionsBehavior.Editable = false;
            this.treeBookNavigation.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeBookNavigation.OptionsView.ShowCheckBoxes = true;
            this.treeBookNavigation.OptionsView.ShowColumns = false;
            this.treeBookNavigation.OptionsView.ShowHorzLines = false;
            this.treeBookNavigation.OptionsView.ShowIndicator = false;
            this.treeBookNavigation.OptionsView.ShowVertLines = false;
            this.treeBookNavigation.Size = new System.Drawing.Size(202, 472);
            this.treeBookNavigation.StateImageList = this.imageList16;
            this.treeBookNavigation.TabIndex = 0;
            this.treeBookNavigation.BeforeExpand += new DevExpress.XtraTreeList.BeforeExpandEventHandler(this.treeNavigation_BeforeExpand);
            this.treeBookNavigation.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeBookNavigation_BeforeCheckNode);
            this.treeBookNavigation.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeBookNavigation_AfterCheckNode);
            this.treeBookNavigation.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeBookNavigation_FocusedNodeChanged);
            this.treeBookNavigation.SelectionChanged += new System.EventHandler(this.treeBookNavigation_SelectionChanged);
            // 
            // chkFilterOptional
            // 
            this.chkFilterOptional.Location = new System.Drawing.Point(103, 135);
            this.chkFilterOptional.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkFilterOptional.Name = "chkFilterOptional";
            this.chkFilterOptional.Properties.Caption = "";
            this.chkFilterOptional.Size = new System.Drawing.Size(117, 19);
            this.chkFilterOptional.TabIndex = 21;
            this.chkFilterOptional.EditValueChanged += new System.EventHandler(this.chkFilterOptional_EditValueChanged);
            // 
            // chkFilterMinimum
            // 
            this.chkFilterMinimum.EditValue = true;
            this.chkFilterMinimum.Location = new System.Drawing.Point(103, 111);
            this.chkFilterMinimum.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkFilterMinimum.Name = "chkFilterMinimum";
            this.chkFilterMinimum.Properties.Caption = "";
            this.chkFilterMinimum.Size = new System.Drawing.Size(117, 19);
            this.chkFilterMinimum.TabIndex = 20;
            this.chkFilterMinimum.EditValueChanged += new System.EventHandler(this.chkFilterMinimum_EditValueChanged);
            // 
            // chkFilterDraft
            // 
            this.chkFilterDraft.EditValue = true;
            this.chkFilterDraft.Location = new System.Drawing.Point(103, 86);
            this.chkFilterDraft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkFilterDraft.Name = "chkFilterDraft";
            this.chkFilterDraft.Properties.Caption = "";
            this.chkFilterDraft.Size = new System.Drawing.Size(117, 19);
            this.chkFilterDraft.TabIndex = 19;
            this.chkFilterDraft.EditValueChanged += new System.EventHandler(this.chkFilterDraft_EditValueChanged);
            // 
            // btnFilterTrades
            // 
            this.btnFilterTrades.Location = new System.Drawing.Point(6, 212);
            this.btnFilterTrades.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFilterTrades.Name = "btnFilterTrades";
            this.btnFilterTrades.Size = new System.Drawing.Size(100, 30);
            this.btnFilterTrades.TabIndex = 15;
            this.btnFilterTrades.Text = "Filter Entities";
            this.btnFilterTrades.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // dtpFilterPeriodTo
            // 
            this.dtpFilterPeriodTo.EditValue = null;
            this.dtpFilterPeriodTo.Location = new System.Drawing.Point(103, 59);
            this.dtpFilterPeriodTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpFilterPeriodTo.Name = "dtpFilterPeriodTo";
            this.dtpFilterPeriodTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterPeriodTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterPeriodTo.Size = new System.Drawing.Size(117, 22);
            this.dtpFilterPeriodTo.TabIndex = 11;
            this.dtpFilterPeriodTo.EditValueChanged += new System.EventHandler(this.dtpFilterPeriodTo_EditValueChanged);
            this.dtpFilterPeriodTo.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.FilterEditors_EditValueChanging);
            // 
            // dtpFilterPeriodFrom
            // 
            this.dtpFilterPeriodFrom.EditValue = null;
            this.dtpFilterPeriodFrom.Location = new System.Drawing.Point(103, 32);
            this.dtpFilterPeriodFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpFilterPeriodFrom.Name = "dtpFilterPeriodFrom";
            this.dtpFilterPeriodFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterPeriodFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterPeriodFrom.Size = new System.Drawing.Size(117, 22);
            this.dtpFilterPeriodFrom.TabIndex = 10;
            this.dtpFilterPeriodFrom.EditValueChanged += new System.EventHandler(this.dtpFilterPeriodFrom_EditValueChanged);
            this.dtpFilterPeriodFrom.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.FilterEditors_EditValueChanging);
            // 
            // dtpFilterPositionDateFrom
            // 
            this.dtpFilterPositionDateFrom.EditValue = null;
            this.dtpFilterPositionDateFrom.Location = new System.Drawing.Point(103, 6);
            this.dtpFilterPositionDateFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpFilterPositionDateFrom.Name = "dtpFilterPositionDateFrom";
            this.dtpFilterPositionDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterPositionDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterPositionDateFrom.Size = new System.Drawing.Size(117, 22);
            this.dtpFilterPositionDateFrom.TabIndex = 9;
            this.dtpFilterPositionDateFrom.EditValueChanged += new System.EventHandler(this.dtpFilterPositionDate_EditValueChanged);
            this.dtpFilterPositionDateFrom.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.FilterEditors_EditValueChanging);
            // 
            // layoutControlRoot
            // 
            this.layoutControlRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRoot.Name = "layoutControlRoot";
            this.layoutControlRoot.Root = this.layoutControlGroup1;
            this.layoutControlRoot.Size = new System.Drawing.Size(310, 97);
            this.layoutControlRoot.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(310, 97);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnGeneratePosition,
            this.btnGenerateMonteCarlo,
            this.btnRatiosReport,
            this.btnGenerateCashFlow,
            this.btnViewTrade,
            this.btnEditTrade,
            this.btnDeleteTrade,
            this.BtnActivateTrades,
            this.btnClose,
            this.btnMarkToMarket,
            this.btnExport});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 13;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.FloatLocation = new System.Drawing.Point(2369, 509);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnGeneratePosition),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMarkToMarket),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnGenerateMonteCarlo),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRatiosReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnGenerateCashFlow),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnViewTrade, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEditTrade),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeleteTrade),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnActivateTrades, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExport, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnGeneratePosition
            // 
            this.btnGeneratePosition.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnGeneratePosition.Caption = "Position";
            this.btnGeneratePosition.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnGeneratePosition.Id = 0;
            this.btnGeneratePosition.LargeImageIndex = 7;
            this.btnGeneratePosition.Name = "btnGeneratePosition";
            this.btnGeneratePosition.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGeneratePosition_Click);
            // 
            // btnMarkToMarket
            // 
            this.btnMarkToMarket.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnMarkToMarket.Caption = "Mark To Market";
            this.btnMarkToMarket.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnMarkToMarket.Id = 10;
            this.btnMarkToMarket.LargeImageIndex = 6;
            this.btnMarkToMarket.Name = "btnMarkToMarket";
            this.btnMarkToMarket.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGenerateMTM_Click);
            // 
            // btnGenerateMonteCarlo
            // 
            this.btnGenerateMonteCarlo.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnGenerateMonteCarlo.Caption = "Monte Carlo";
            this.btnGenerateMonteCarlo.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnGenerateMonteCarlo.Id = 2;
            this.btnGenerateMonteCarlo.LargeImageIndex = 7;
            this.btnGenerateMonteCarlo.Name = "btnGenerateMonteCarlo";
            this.btnGenerateMonteCarlo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGenerateMonteCarlo_Click);
            // 
            // btnRatiosReport
            // 
            this.btnRatiosReport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnRatiosReport.Caption = "Ratios Report";
            this.btnRatiosReport.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnRatiosReport.Id = 3;
            this.btnRatiosReport.LargeImageIndex = 12;
            this.btnRatiosReport.Name = "btnRatiosReport";
            this.btnRatiosReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnRatiosReportClick);
            // 
            // btnGenerateCashFlow
            // 
            this.btnGenerateCashFlow.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnGenerateCashFlow.Caption = "Cash Flow";
            this.btnGenerateCashFlow.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnGenerateCashFlow.Id = 4;
            this.btnGenerateCashFlow.LargeImageIndex = 6;
            this.btnGenerateCashFlow.Name = "btnGenerateCashFlow";
            this.btnGenerateCashFlow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGenerateCashFlow_Click);
            // 
            // btnViewTrade
            // 
            this.btnViewTrade.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnViewTrade.Caption = "View";
            this.btnViewTrade.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnViewTrade.Id = 5;
            this.btnViewTrade.LargeImageIndex = 9;
            this.btnViewTrade.Name = "btnViewTrade";
            this.btnViewTrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnViewTrade_Click);
            // 
            // btnEditTrade
            // 
            this.btnEditTrade.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnEditTrade.Caption = "Edit";
            this.btnEditTrade.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnEditTrade.Id = 6;
            this.btnEditTrade.LargeImageIndex = 1;
            this.btnEditTrade.Name = "btnEditTrade";
            this.btnEditTrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEditTrade_Click);
            // 
            // btnDeleteTrade
            // 
            this.btnDeleteTrade.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnDeleteTrade.Caption = "Deactivate";
            this.btnDeleteTrade.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnDeleteTrade.Id = 7;
            this.btnDeleteTrade.LargeImageIndex = 13;
            this.btnDeleteTrade.Name = "btnDeleteTrade";
            this.btnDeleteTrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnDeleteTradeClick);
            // 
            // BtnActivateTrades
            // 
            this.BtnActivateTrades.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.BtnActivateTrades.Caption = "Inactive Trades";
            this.BtnActivateTrades.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.BtnActivateTrades.Id = 8;
            this.BtnActivateTrades.LargeImageIndex = 14;
            this.BtnActivateTrades.Name = "BtnActivateTrades";
            this.BtnActivateTrades.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnActivateTradesClick);
            // 
            // btnExport
            // 
            this.btnExport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnExport.Caption = "Export";
            this.btnExport.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnExport.Id = 12;
            this.btnExport.LargeImageIndex = 8;
            this.btnExport.Name = "btnExport";
            this.btnExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExport_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 9;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1422, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 795);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1422, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 795);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1422, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 795);
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("d46f4306-af7d-4fed-a9b5-5a0216ec3476");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockBottom = false;
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.FloatOnDblClick = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(229, 200);
            this.dockPanel1.Size = new System.Drawing.Size(267, 795);
            this.dockPanel1.Text = "Apply Filters";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.groupControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 24);
            this.dockPanel1_Container.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(259, 767);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.chkOnlyNonZeroTotalDays);
            this.groupControl1.Controls.Add(this.btnCpmRefresh);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dtpFilterPositionDateFrom);
            this.groupControl1.Controls.Add(this.btnFilterTrades);
            this.groupControl1.Controls.Add(this.dtpFilterPeriodFrom);
            this.groupControl1.Controls.Add(this.dtpFilterPeriodTo);
            this.groupControl1.Controls.Add(this.chkFilterDraft);
            this.groupControl1.Controls.Add(this.chkFilterMinimum);
            this.groupControl1.Controls.Add(this.chkFilterOptional);
            this.groupControl1.Controls.Add(this.tbEntities);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(259, 767);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Apply Filters";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(6, 164);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(97, 39);
            this.labelControl2.TabIndex = 34;
            this.labelControl2.Text = "Only trades with Total Days > 0:";
            // 
            // chkOnlyNonZeroTotalDays
            // 
            this.chkOnlyNonZeroTotalDays.Location = new System.Drawing.Point(103, 166);
            this.chkOnlyNonZeroTotalDays.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkOnlyNonZeroTotalDays.Name = "chkOnlyNonZeroTotalDays";
            this.chkOnlyNonZeroTotalDays.Properties.Caption = "";
            this.chkOnlyNonZeroTotalDays.Size = new System.Drawing.Size(117, 19);
            this.chkOnlyNonZeroTotalDays.TabIndex = 33;
            this.chkOnlyNonZeroTotalDays.EditValueChanged += new System.EventHandler(this.chkOnlyNonZeroTotalDays_EditValueChanged);
            // 
            // btnCpmRefresh
            // 
            this.btnCpmRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnCpmRefresh.Image")));
            this.btnCpmRefresh.Location = new System.Drawing.Point(115, 212);
            this.btnCpmRefresh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCpmRefresh.Name = "btnCpmRefresh";
            this.btnCpmRefresh.Size = new System.Drawing.Size(94, 30);
            this.btnCpmRefresh.TabIndex = 32;
            this.btnCpmRefresh.Text = "Refresh";
            this.btnCpmRefresh.ToolTip = "Refresh Tabs";
            this.btnCpmRefresh.Click += new System.EventHandler(this.btnCpmRefresh_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(6, 138);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(92, 16);
            this.labelControl7.TabIndex = 31;
            this.labelControl7.Text = "Optional Period:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(6, 113);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(97, 16);
            this.labelControl6.TabIndex = 30;
            this.labelControl6.Text = "Minimum Period:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(6, 89);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(33, 16);
            this.labelControl5.TabIndex = 29;
            this.labelControl5.Text = "Draft:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 62);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 16);
            this.labelControl4.TabIndex = 28;
            this.labelControl4.Text = "Period To:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(6, 36);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 16);
            this.labelControl3.TabIndex = 28;
            this.labelControl3.Text = "Period From:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 10);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(93, 16);
            this.labelControl1.TabIndex = 28;
            this.labelControl1.Text = "Pos. Date From:";
            // 
            // ViewTradesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1422, 833);
            this.Controls.Add(this.layoutRoot);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ViewTradesForm";
            this.Activated += new System.EventHandler(this.ViewTradesForm_Activated);
            this.Deactivate += new System.EventHandler(this.ViewTradesForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewTradesForm_FormClosing);
            this.Load += new System.EventHandler(this.MainControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeDetailsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterVertical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEntities)).EndInit();
            this.tbEntities.ResumeLayout(false);
            this.tbpCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeNavigation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).EndInit();
            this.tbpBooks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeBookNavigation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterOptional.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterMinimum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterDraft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPeriodFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPositionDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterPositionDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyNonZeroTotalDays.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupRoot;
        private DevExpress.XtraGrid.GridControl gridTradeDetails;
        private DevExpress.XtraGrid.Views.Grid.GridView gridTradeDetailsView;
        private DevExpress.XtraLayout.SplitterItem splitterVertical;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.Utils.ImageCollection imageList16;
        private DevExpress.XtraEditors.DateEdit dtpFilterPeriodTo;
        private DevExpress.XtraEditors.DateEdit dtpFilterPeriodFrom;
        private DevExpress.XtraEditors.DateEdit dtpFilterPositionDateFrom;
        private DevExpress.XtraEditors.SimpleButton btnFilterTrades;
        private DevExpress.XtraEditors.CheckEdit chkFilterDraft;
        private DevExpress.XtraEditors.CheckEdit chkFilterMinimum;
        private DevExpress.XtraEditors.CheckEdit chkFilterOptional;
        private DevExpress.XtraLayout.LayoutControl layoutControlRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraTab.XtraTabControl tbEntities;
        private DevExpress.XtraTab.XtraTabPage tbpCompanies;
        private DevExpress.XtraTab.XtraTabPage tbpBooks;
        private DevExpress.XtraTreeList.TreeList treeBookNavigation;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrades;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeDetails;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTradeDetails;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTreeList.TreeList treeNavigation;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnGeneratePosition;
        private DevExpress.XtraBars.BarLargeButtonItem btnGenerateMonteCarlo;
        private DevExpress.XtraBars.BarLargeButtonItem btnRatiosReport;
        private DevExpress.XtraBars.BarLargeButtonItem btnGenerateCashFlow;
        private DevExpress.XtraBars.BarLargeButtonItem btnViewTrade;
        private DevExpress.XtraBars.BarLargeButtonItem btnEditTrade;
        private DevExpress.XtraBars.BarLargeButtonItem btnDeleteTrade;
        private DevExpress.XtraBars.BarLargeButtonItem BtnActivateTrades;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarLargeButtonItem btnMarkToMarket;
        private DevExpress.XtraEditors.SimpleButton btnCpmRefresh;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit chkOnlyNonZeroTotalDays;
        private DevExpress.XtraBars.BarLargeButtonItem btnExport;
    }
}
