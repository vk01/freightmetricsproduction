﻿namespace Exis.WinClient.Controls.Core
{
    partial class InitialMarginControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lcInitialMargin = new DevExpress.XtraLayout.LayoutControl();
            this.btnExportInitialMargin = new DevExpress.XtraEditors.SimpleButton();
            this.gridInitialMargin = new DevExpress.XtraGrid.GridControl();
            this.gridViewInitialMargin = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridSpanOptionParameters = new DevExpress.XtraGrid.GridControl();
            this.gridViewSpanOptionParameters = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnCalculateInitialMargin = new DevExpress.XtraEditors.SimpleButton();
            this.beSelectExcel = new DevExpress.XtraEditors.ButtonEdit();
            this.btnLoadOptionsParameters = new DevExpress.XtraEditors.SimpleButton();
            this.beDATfile = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciSelectExcel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcigridInitialMargin = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgSpanOptionsParameters = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciLoadDatFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSpanOptionsParameters = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.lciBtnExportInitialMargin = new DevExpress.XtraLayout.LayoutControlItem();
            this.bciBtnInitialMargin = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).BeginInit();
            this.lcInitialMargin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpanOptionParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSpanOptionParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beSelectExcel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beDATfile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSelectExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcigridInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSpanOptionsParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLoadDatFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSpanOptionsParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBtnExportInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bciBtnInitialMargin)).BeginInit();
            this.SuspendLayout();
            // 
            // lcInitialMargin
            // 
            this.lcInitialMargin.Controls.Add(this.btnExportInitialMargin);
            this.lcInitialMargin.Controls.Add(this.gridInitialMargin);
            this.lcInitialMargin.Controls.Add(this.gridSpanOptionParameters);
            this.lcInitialMargin.Controls.Add(this.btnCalculateInitialMargin);
            this.lcInitialMargin.Controls.Add(this.beSelectExcel);
            this.lcInitialMargin.Controls.Add(this.btnLoadOptionsParameters);
            this.lcInitialMargin.Controls.Add(this.beDATfile);
            this.lcInitialMargin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcInitialMargin.Location = new System.Drawing.Point(0, 0);
            this.lcInitialMargin.Name = "lcInitialMargin";
            this.lcInitialMargin.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1771, 450, 386, 350);
            this.lcInitialMargin.Root = this.layoutControlGroup4;
            this.lcInitialMargin.Size = new System.Drawing.Size(900, 600);
            this.lcInitialMargin.TabIndex = 1;
            this.lcInitialMargin.Text = "layoutControl2";
            // 
            // btnExportInitialMargin
            // 
            this.btnExportInitialMargin.ImageIndex = 8;
            this.btnExportInitialMargin.Location = new System.Drawing.Point(811, 180);
            this.btnExportInitialMargin.Name = "btnExportInitialMargin";
            this.btnExportInitialMargin.Size = new System.Drawing.Size(75, 22);
            this.btnExportInitialMargin.StyleController = this.lcInitialMargin;
            this.btnExportInitialMargin.TabIndex = 10;
            this.btnExportInitialMargin.Text = "Export";
            this.btnExportInitialMargin.Click += new System.EventHandler(this.btnExportInitialMargin_Click);
            // 
            // gridInitialMargin
            // 
            this.gridInitialMargin.Location = new System.Drawing.Point(14, 206);
            this.gridInitialMargin.MainView = this.gridViewInitialMargin;
            this.gridInitialMargin.Name = "gridInitialMargin";
            this.gridInitialMargin.Size = new System.Drawing.Size(872, 380);
            this.gridInitialMargin.TabIndex = 9;
            this.gridInitialMargin.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInitialMargin});
            // 
            // gridViewInitialMargin
            // 
            this.gridViewInitialMargin.GridControl = this.gridInitialMargin;
            this.gridViewInitialMargin.Name = "gridViewInitialMargin";
            this.gridViewInitialMargin.OptionsBehavior.Editable = false;
            this.gridViewInitialMargin.OptionsView.ShowFooter = true;
            this.gridViewInitialMargin.OptionsView.ShowGroupPanel = false;
            // 
            // gridSpanOptionParameters
            // 
            this.gridSpanOptionParameters.Location = new System.Drawing.Point(26, 71);
            this.gridSpanOptionParameters.MainView = this.gridViewSpanOptionParameters;
            this.gridSpanOptionParameters.MinimumSize = new System.Drawing.Size(0, 50);
            this.gridSpanOptionParameters.Name = "gridSpanOptionParameters";
            this.gridSpanOptionParameters.Size = new System.Drawing.Size(848, 88);
            this.gridSpanOptionParameters.TabIndex = 8;
            this.gridSpanOptionParameters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSpanOptionParameters});
            // 
            // gridViewSpanOptionParameters
            // 
            this.gridViewSpanOptionParameters.GridControl = this.gridSpanOptionParameters;
            this.gridViewSpanOptionParameters.Name = "gridViewSpanOptionParameters";
            this.gridViewSpanOptionParameters.OptionsView.ShowGroupPanel = false;
            // 
            // btnCalculateInitialMargin
            // 
            this.btnCalculateInitialMargin.ImageIndex = 7;
            this.btnCalculateInitialMargin.Location = new System.Drawing.Point(688, 180);
            this.btnCalculateInitialMargin.Name = "btnCalculateInitialMargin";
            this.btnCalculateInitialMargin.Size = new System.Drawing.Size(119, 22);
            this.btnCalculateInitialMargin.StyleController = this.lcInitialMargin;
            this.btnCalculateInitialMargin.TabIndex = 7;
            this.btnCalculateInitialMargin.Text = "Calculate Initial Margin";
            this.btnCalculateInitialMargin.Click += new System.EventHandler(this.btnCalculateInitialMargin_Click);
            // 
            // beSelectExcel
            // 
            this.beSelectExcel.Location = new System.Drawing.Point(97, 180);
            this.beSelectExcel.Name = "beSelectExcel";
            this.beSelectExcel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.beSelectExcel.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.beSelectExcel_ButtonClick);
            this.beSelectExcel.Size = new System.Drawing.Size(587, 20);
            this.beSelectExcel.StyleController = this.lcInitialMargin;
            this.beSelectExcel.TabIndex = 6;
            // 
            // btnLoadOptionsParameters
            // 
            this.btnLoadOptionsParameters.Location = new System.Drawing.Point(742, 45);
            this.btnLoadOptionsParameters.Name = "btnLoadOptionsParameters";
            this.btnLoadOptionsParameters.Size = new System.Drawing.Size(132, 22);
            this.btnLoadOptionsParameters.StyleController = this.lcInitialMargin;
            this.btnLoadOptionsParameters.TabIndex = 5;
            this.btnLoadOptionsParameters.Text = "Load Options Parameters";
            this.btnLoadOptionsParameters.Click += new System.EventHandler(this.btnLoadOptionsParameters_Click);
            // 
            // beDATfile
            // 
            this.beDATfile.Location = new System.Drawing.Point(109, 45);
            this.beDATfile.Name = "beDATfile";
            this.beDATfile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.beDATfile.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.beDATfile_ButtonClick);
            this.beDATfile.Size = new System.Drawing.Size(629, 20);
            this.beDATfile.StyleController = this.lcInitialMargin;
            this.beDATfile.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(900, 600);
            this.layoutControlGroup4.Text = "Root";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciSelectExcel,
            this.lcigridInitialMargin,
            this.lcgSpanOptionsParameters,
            this.splitterItem1,
            this.lciBtnExportInitialMargin,
            this.bciBtnInitialMargin});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(900, 600);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            this.layoutControlGroup9.TextVisible = false;
            // 
            // lciSelectExcel
            // 
            this.lciSelectExcel.Control = this.beSelectExcel;
            this.lciSelectExcel.CustomizationFormText = "Select Excel File:";
            this.lciSelectExcel.Location = new System.Drawing.Point(0, 166);
            this.lciSelectExcel.Name = "lciSelectExcel";
            this.lciSelectExcel.Size = new System.Drawing.Size(674, 26);
            this.lciSelectExcel.Text = "Select Excel File:";
            this.lciSelectExcel.TextSize = new System.Drawing.Size(80, 13);
            // 
            // lcigridInitialMargin
            // 
            this.lcigridInitialMargin.Control = this.gridInitialMargin;
            this.lcigridInitialMargin.CustomizationFormText = "lcigridInitialMargin";
            this.lcigridInitialMargin.Location = new System.Drawing.Point(0, 192);
            this.lcigridInitialMargin.Name = "lcigridInitialMargin";
            this.lcigridInitialMargin.Size = new System.Drawing.Size(876, 384);
            this.lcigridInitialMargin.Text = "lcigridInitialMargin";
            this.lcigridInitialMargin.TextSize = new System.Drawing.Size(0, 0);
            this.lcigridInitialMargin.TextToControlDistance = 0;
            this.lcigridInitialMargin.TextVisible = false;
            // 
            // lcgSpanOptionsParameters
            // 
            this.lcgSpanOptionsParameters.CustomizationFormText = "Options Parameters";
            this.lcgSpanOptionsParameters.ExpandButtonVisible = true;
            this.lcgSpanOptionsParameters.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciLoadDatFile,
            this.layoutControlItem3,
            this.lciSpanOptionsParameters});
            this.lcgSpanOptionsParameters.Location = new System.Drawing.Point(0, 0);
            this.lcgSpanOptionsParameters.Name = "lcgSpanOptionsParameters";
            this.lcgSpanOptionsParameters.Size = new System.Drawing.Size(876, 161);
            this.lcgSpanOptionsParameters.Text = "Options Parameters";
            // 
            // lciLoadDatFile
            // 
            this.lciLoadDatFile.Control = this.beDATfile;
            this.lciLoadDatFile.CustomizationFormText = "Import DAT:";
            this.lciLoadDatFile.Location = new System.Drawing.Point(0, 0);
            this.lciLoadDatFile.Name = "lciLoadDatFile";
            this.lciLoadDatFile.Size = new System.Drawing.Size(716, 26);
            this.lciLoadDatFile.Text = "Import DAT:";
            this.lciLoadDatFile.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoadOptionsParameters;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(716, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(136, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // lciSpanOptionsParameters
            // 
            this.lciSpanOptionsParameters.Control = this.gridSpanOptionParameters;
            this.lciSpanOptionsParameters.CustomizationFormText = "lciSpanOptionsParameters";
            this.lciSpanOptionsParameters.Location = new System.Drawing.Point(0, 26);
            this.lciSpanOptionsParameters.Name = "lciSpanOptionsParameters";
            this.lciSpanOptionsParameters.Size = new System.Drawing.Size(852, 92);
            this.lciSpanOptionsParameters.Text = "lciSpanOptionsParameters";
            this.lciSpanOptionsParameters.TextSize = new System.Drawing.Size(0, 0);
            this.lciSpanOptionsParameters.TextToControlDistance = 0;
            this.lciSpanOptionsParameters.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 161);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(876, 5);
            // 
            // lciBtnExportInitialMargin
            // 
            this.lciBtnExportInitialMargin.Control = this.btnExportInitialMargin;
            this.lciBtnExportInitialMargin.CustomizationFormText = "lciBtnExportInitialMargin";
            this.lciBtnExportInitialMargin.Location = new System.Drawing.Point(797, 166);
            this.lciBtnExportInitialMargin.Name = "lciBtnExportInitialMargin";
            this.lciBtnExportInitialMargin.Size = new System.Drawing.Size(79, 26);
            this.lciBtnExportInitialMargin.Text = "lciBtnExportInitialMargin";
            this.lciBtnExportInitialMargin.TextSize = new System.Drawing.Size(0, 0);
            this.lciBtnExportInitialMargin.TextToControlDistance = 0;
            this.lciBtnExportInitialMargin.TextVisible = false;
            // 
            // bciBtnInitialMargin
            // 
            this.bciBtnInitialMargin.Control = this.btnCalculateInitialMargin;
            this.bciBtnInitialMargin.CustomizationFormText = "bciBtnInitialMargin";
            this.bciBtnInitialMargin.Location = new System.Drawing.Point(674, 166);
            this.bciBtnInitialMargin.Name = "bciBtnInitialMargin";
            this.bciBtnInitialMargin.Size = new System.Drawing.Size(123, 26);
            this.bciBtnInitialMargin.Text = "bciBtnInitialMargin";
            this.bciBtnInitialMargin.TextSize = new System.Drawing.Size(0, 0);
            this.bciBtnInitialMargin.TextToControlDistance = 0;
            this.bciBtnInitialMargin.TextVisible = false;
            // 
            // InitialMarginControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcInitialMargin);
            this.Name = "InitialMarginControl";
            this.Size = new System.Drawing.Size(900, 600);
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).EndInit();
            this.lcInitialMargin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpanOptionParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSpanOptionParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beSelectExcel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beDATfile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSelectExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcigridInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSpanOptionsParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLoadDatFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSpanOptionsParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBtnExportInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bciBtnInitialMargin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcInitialMargin;
        private DevExpress.XtraEditors.SimpleButton btnExportInitialMargin;
        private DevExpress.XtraGrid.GridControl gridInitialMargin;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInitialMargin;
        private DevExpress.XtraGrid.GridControl gridSpanOptionParameters;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSpanOptionParameters;
        private DevExpress.XtraEditors.SimpleButton btnCalculateInitialMargin;
        private DevExpress.XtraEditors.ButtonEdit beSelectExcel;
        private DevExpress.XtraEditors.SimpleButton btnLoadOptionsParameters;
        private DevExpress.XtraEditors.ButtonEdit beDATfile;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem lciSelectExcel;
        private DevExpress.XtraLayout.LayoutControlItem lcigridInitialMargin;
        private DevExpress.XtraLayout.LayoutControlGroup lcgSpanOptionsParameters;
        private DevExpress.XtraLayout.LayoutControlItem lciLoadDatFile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem lciSpanOptionsParameters;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlItem lciBtnExportInitialMargin;
        private DevExpress.XtraLayout.LayoutControlItem bciBtnInitialMargin;
    }
}
