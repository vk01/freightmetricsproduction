using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using Exis.Domain;
using Exis.WinClient.General;

using Itenso.TimePeriod;
using SummaryItemType = DevExpress.Data.SummaryItemType;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;
using DevExpress.XtraEditors.Controls;
using System.Data;
using System.Threading;

namespace Exis.WinClient.Controls.Core
{
    public partial class ViewTradesForm : XtraForm
    {
        #region Nested type: FormStateEnum

        private enum FormStateEnum
        {
            BeforeSearchDates,
            BeforeSearchTree,
            BeforeAction,
            AfterSearch,
            AfterResults,
            BeforeFiltering
        }

        #endregion

        #region Private Properties

        private AppParameter _appParameter;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm0;
        private WaitDialogForm _dialogForm1;
        private WaitDialogForm _dialogForm2;

        private TreeListNode _expandedNode;
        private FormStateEnum _formState = FormStateEnum.BeforeSearchDates;
        private bool isLoad;

        //private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        //private Dictionary<string, Dictionary<DateTime, decimal>> _tradeMTMResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        //private Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMRatiosResults = new Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>();
        //private Dictionary<string, decimal> _tradeEmbeddedValues = new Dictionary<string, decimal>();
        //private Dictionary<DateTime, Dictionary<string, decimal>> _indexesValues = new Dictionary<DateTime, Dictionary<string, decimal>>();

        private bool RatioReportPL;
        private bool RatioReportCashFlow;
        private bool InSelection;

        private List<Book> Books;
        private HierarchyNodeInfo _selectedHierarchyNode;
        private ArrayList _selectedNodes;

        private DateTime _lastImportDate;

        private string _mcName;

        private Dictionary<long, List<TradeInformation>> _companiesLoadedTrades;
        private Dictionary<long, List<TradeInformation>> _booksLoadedTrades;

        private List<Index> indexes;

        #endregion

        #region Constructors

        public ViewTradesForm()
        {
            InitializeComponent();

            InitializeControls();
            SetGuiControls();

            //btnMarkToMarket.Enabled = true;
            //btnGeneratePosition.Enabled = true;
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridTradeDetails.BeginUpdate();

            int gridViewColumnindex = 0;
            gridTradeDetailsView.Columns.Clear();
            gridTradeDetailsView.OptionsView.ColumnAutoWidth = false;
            GridColumn column = AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                          UnboundColumnType.Bound, null);
            column.SummaryItem.SummaryType = SummaryItemType.Count;
            column.SummaryItem.DisplayFormat = "Count: {0:N0}";
            gridTradeDetailsView.Columns.Add(column);
            gridTradeDetailsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Books", Strings.Books, gridViewColumnindex++,
                                                       UnboundColumnType.String, null));
            gridTradeDetailsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Ownership", Strings.Ownership, gridViewColumnindex++,
                                                       UnboundColumnType.Decimal, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                       UnboundColumnType.Bound, null));
            gridTradeDetailsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                                       UnboundColumnType.String, null));
            gridTradeDetailsView.Columns.Add(AddColumn("MarketID", "Market ID", gridViewColumnindex++,
                                                       UnboundColumnType.String, null));
            gridTradeDetailsView.Columns.Add(AddColumn("VesselId", "Vessel ID", gridViewColumnindex++,
                                                       UnboundColumnType.String, null));

            //only used for filtering data - NOT VISIBLE
            gridTradeDetailsView.Columns["MarketID"].Visible = false;
            gridTradeDetailsView.Columns["VesselId"].Visible = false;



            gridTradeDetailsView.OptionsLayout.StoreDataSettings = true;
            gridTradeDetailsView.OptionsLayout.StoreVisualOptions = true;

            try
            {
                // gridTradeDetailsView.RestoreLayoutFromXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Details.Layout.xml");
            }
            catch (Exception)
            {
            }

            gridTradeDetails.EndUpdate();

            treeNavigation.BeginUpdate();
            treeNavigation.Columns.Add();
            treeNavigation.Columns[0].Caption = Strings.Entities;
            treeNavigation.Columns[0].VisibleIndex = 0;
            treeNavigation.Columns[0].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            //treeNavigation.OptionsBehavior.AllowRecursiveNodeChecking = true;
            //treeNavigation.OptionsBehavior.AllowIndeterminateCheckState = false;
            treeNavigation.EndUpdate();

            treeBookNavigation.BeginUpdate();
            treeBookNavigation.Columns.Add();
            treeBookNavigation.Columns[0].Caption = Strings.Entities;
            treeBookNavigation.Columns[0].VisibleIndex = 0;
            treeBookNavigation.Columns[0].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            treeBookNavigation.FocusedNode = null;
            //treeBookNavigation.OptionsBehavior.AllowRecursiveNodeChecking = true;
            treeBookNavigation.EndUpdate();

            gridTradeDetailsView.OptionsLayout.StoreDataSettings = true;
            gridTradeDetailsView.OptionsLayout.StoreVisualOptions = true;

            try
            {
                //gridTradeActionResultsView.RestoreLayoutFromXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }

            dtpFilterPositionDateFrom.DateTime = DateTime.Today;
            dtpFilterPeriodFrom.DateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            dtpFilterPeriodTo.DateTime = new DateTime(DateTime.Today.Year + 1, 1, 1).AddDays(-1);//Last day of current year    


            _companiesLoadedTrades = new Dictionary<long, List<TradeInformation>>();
            _booksLoadedTrades = new Dictionary<long, List<TradeInformation>>();

            _formState = FormStateEnum.BeforeSearchDates;
        }

        private void SetGuiControls()
        {
            if (_formState == FormStateEnum.BeforeSearchDates)
            {
                //Button FilterTrades
                btnFilterTrades.Enabled = true;

                //Clear Trees
                treeNavigation.ClearNodes();
                //treeBookNavigation.SelectionChanged -= treeBookNavigation_SelectionChanged;
                treeBookNavigation.FocusedNodeChanged -= treeBookNavigation_FocusedNodeChanged;
                treeBookNavigation.ClearNodes();
                //treeBookNavigation.SelectionChanged += treeBookNavigation_SelectionChanged;
                treeBookNavigation.FocusedNodeChanged += treeBookNavigation_FocusedNodeChanged;
                _expandedNode = null;
                _companiesLoadedTrades = new Dictionary<long, List<TradeInformation>>();
                _booksLoadedTrades = new Dictionary<long, List<TradeInformation>>();

                //Calculations Buttons Disabled
                btnGeneratePosition.Enabled = false;
                btnMarkToMarket.Enabled = false;
                btnGenerateMonteCarlo.Enabled = false;
                btnGenerateCashFlow.Enabled = false;
                btnRatiosReport.Enabled = false;

                //Action Buttons Disabled
                btnDeleteTrade.Enabled = false;
                btnEditTrade.Enabled = false;
                btnViewTrade.Enabled = false;

                BtnActivateTrades.Enabled = false;
                btnClose.Enabled = false;

                btnCpmRefresh.Enabled = false;

                //Clear Grid
                gridTradeDetails.DataSource = null;

                SetEnabledForWorkingStatus(false);
            }
            else if (_formState == FormStateEnum.BeforeSearchTree)
            {
                //Button FilterTrades
                btnFilterTrades.Enabled = true;

                //Calculations Buttons Disabled
                btnGeneratePosition.Enabled = false;
                btnMarkToMarket.Enabled = false;
                btnGenerateMonteCarlo.Enabled = false;
                btnGenerateCashFlow.Enabled = false;
                btnRatiosReport.Enabled = false;

                //Action Buttons Disabled
                btnDeleteTrade.Enabled = false;
                btnEditTrade.Enabled = false;
                btnViewTrade.Enabled = false;

                BtnActivateTrades.Enabled = false;
                btnExport.Enabled = false;
                btnClose.Enabled = false;

                btnCpmRefresh.Enabled = false;

                //Clear Grid
                gridTradeDetails.DataSource = null;

                SetEnabledForWorkingStatus(false);
            }
            else if (_formState == FormStateEnum.BeforeAction)
            {
                //Button FilterTrades
                btnFilterTrades.Enabled = true;

                //Calculations Buttons Disabled
                btnGeneratePosition.Enabled = false;
                btnMarkToMarket.Enabled = false;
                btnGenerateMonteCarlo.Enabled = false;
                btnGenerateCashFlow.Enabled = false;
                btnRatiosReport.Enabled = false;

                //Action Buttons Disabled
                btnDeleteTrade.Enabled = false;
                btnEditTrade.Enabled = false;
                btnViewTrade.Enabled = false;

                BtnActivateTrades.Enabled = false;
                btnExport.Enabled = false;
                btnClose.Enabled = false;

                btnCpmRefresh.Enabled = false;

                //Clear Grid
                gridTradeDetails.DataSource = null;

                SetEnabledForWorkingStatus(false);
            }
            else if (_formState == FormStateEnum.AfterSearch)
            {

                //Button FilterTrades
                btnFilterTrades.Enabled = false;

                //Calculations Buttons Disabled
                btnGeneratePosition.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnMarkToMarket.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnGenerateMonteCarlo.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnGenerateCashFlow.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnRatiosReport.Enabled = gridTradeDetailsView.DataRowCount > 0;

                //Action Buttons Disabled
                btnDeleteTrade.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnEditTrade.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnViewTrade.Enabled = gridTradeDetailsView.DataRowCount > 0;

                BtnActivateTrades.Enabled = true;
                btnExport.Enabled = gridTradeDetailsView.DataRowCount > 0;
                btnClose.Enabled = true;

                btnCpmRefresh.Enabled = true;



                SetEnabledForWorkingStatus(true);
            }
            else if (_formState == FormStateEnum.BeforeFiltering)
            {
                //Button FilterTrades
                btnFilterTrades.Enabled = true;

                //Clear Trees
                treeNavigation.ClearNodes();
                //treeBookNavigation.SelectionChanged -= treeBookNavigation_SelectionChanged;
                treeBookNavigation.FocusedNodeChanged -= treeBookNavigation_FocusedNodeChanged;
                treeBookNavigation.ClearNodes();
                //treeBookNavigation.SelectionChanged += treeBookNavigation_SelectionChanged;
                treeBookNavigation.FocusedNodeChanged += treeBookNavigation_FocusedNodeChanged;
                _expandedNode = null;
                _companiesLoadedTrades = new Dictionary<long, List<TradeInformation>>();
                _booksLoadedTrades = new Dictionary<long, List<TradeInformation>>();

                //Calculations Buttons Disabled
                btnGeneratePosition.Enabled = false;
                btnMarkToMarket.Enabled = false;
                btnGenerateMonteCarlo.Enabled = false;
                btnGenerateCashFlow.Enabled = false;
                btnRatiosReport.Enabled = false;

                //Action Buttons Disabled
                btnDeleteTrade.Enabled = false;
                btnEditTrade.Enabled = false;
                btnViewTrade.Enabled = false;

                BtnActivateTrades.Enabled = false;
                btnExport.Enabled = false;
                btnClose.Enabled = true;

                btnCpmRefresh.Enabled = false;

                //Clear Grid
                gridTradeDetails.DataSource = null;

                SetEnabledForWorkingStatus(true);
            }
            else if (_formState == FormStateEnum.AfterResults)
            {
                SetEnabledForWorkingStatus(true);
            }
        }

        private void GetAppParameters()
        {
            BeginApplicationParametersInitializationData();
        }

        private GridColumn AddColumn(string name, string caption, int columnIndex, UnboundColumnType columnType, object Tag)
        {
            var column = new GridColumn();
            column.Visible = true;
            column.AppearanceCell.Options.UseTextOptions = true;
            column.OptionsColumn.FixedWidth = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsColumn.AllowEdit = false;
            column.FieldName = name;
            column.Caption = caption;
            column.Tag = Tag;
            column.VisibleIndex = columnIndex;
            if (columnType != UnboundColumnType.Bound)
                column.UnboundType = columnType;

            switch (columnType)
            {
                case UnboundColumnType.String:
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.DateTime:
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.DisplayFormat.FormatType = FormatType.DateTime;
                    column.DisplayFormat.FormatString = "G";
                    break;
                case UnboundColumnType.Decimal:
                    if (name != "Ownership")
                    {
                        column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                        column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                        column.UnboundType = UnboundColumnType.Decimal;
                        column.DisplayFormat.FormatType = FormatType.Numeric;
                        column.DisplayFormat.FormatString = "N3";
                        column.OptionsColumn.AllowEdit = false;
                    }
                    else
                    {
                        column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                        column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo; //System.Globalization.CultureInfo.GetCultureInfo("no");//SessionRegistry.ClientCultureInfo;
                        column.UnboundType = UnboundColumnType.Decimal;
                        column.DisplayFormat.FormatType = FormatType.Numeric;
                        column.DisplayFormat.FormatString = "p2";
                        column.OptionsColumn.AllowEdit = false;
                    }
                    break;
                case UnboundColumnType.Integer:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.UnboundType = UnboundColumnType.Integer;
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Boolean:
                    column.UnboundType = UnboundColumnType.Boolean;
                    column.OptionsColumn.AllowEdit = false;
                    break;

            }

            return column;
        }

        private void SetEnabledForWorkingStatus(bool status)
        {
            treeNavigation.Enabled = status;
            treeBookNavigation.Enabled = status;
            dtpFilterPositionDateFrom.Enabled = status;
            dtpFilterPeriodFrom.Enabled = status;
            dtpFilterPeriodTo.Enabled = status;
            chkFilterOptional.Enabled = status;
            chkFilterDraft.Enabled = status;
            chkFilterMinimum.Enabled = status;
            chkOnlyNonZeroTotalDays.Enabled = status;
        }

        private void SelectNodeAndChildren()
        {
            InSelection = true;
            var selectedNodes = new ArrayList();
            selectChildren(treeBookNavigation.FocusedNode, selectedNodes);
            treeBookNavigation.Selection.Add(selectedNodes);
            _selectedNodes = selectedNodes;
            InSelection = false;
        }

        void selectChildren(TreeListNode parent, ArrayList selectedNodes)
        {
            IEnumerator en = parent.Nodes.GetEnumerator();
            while (en.MoveNext())
            {
                var child = (TreeListNode)en.Current;
                selectedNodes.Add(child);
                if (child.HasChildren) selectChildren(child, selectedNodes);
            }
        }

        #endregion

        #region GUI Events

        private void MainControl_Load(object sender, EventArgs e)
        {
            isLoad = true;
            BeginGetEntitiesThatHaveTradesByFilters(null, new List<HierarchyNodeInfo>());
        }

        private void ViewTradesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                gridTradeDetailsView.SaveLayoutToXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Details.Layout.xml");
            }
            catch (Exception)
            {
            }

            try
            {
                //gridTradeActionResultsView.SaveLayoutToXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }
        }

        private void treeNavigation_BeforeExpand(object sender, BeforeExpandEventArgs e)
        {
            _expandedNode = e.Node;

            if (e.Node.Nodes.Count > 0)
            {
                return;
            }

            var filters = new List<HierarchyNodeInfo>();
            var entity = (HierarchyNodeInfo)e.Node.Tag;
            filters.Add(entity);

            TreeListNode node = e.Node.ParentNode;
            while (node != null)
            {
                filters.Add((HierarchyNodeInfo)node.Tag);
                node = node.ParentNode;
            }

            if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Company) &&
                    entity.HierarchyNodeType.Code == "CMP" && e.Node.ParentNode == null)
            {
                _formState = FormStateEnum.BeforeSearchTree;
                SetGuiControls();
                BeginGetCompanyHierarchyThatHaveTradesByFilters(((HierarchyNodeInfo)e.Node.Tag).HierarchyNodeType.Id, filters);
            }

        }

        private void treeNavigation_AfterExpand(object sender, NodeEventArgs e)
        {

            TreeListNode node = e.Node;
            if (node.Checked)
            {
                node.CheckAll();
            }
            else
            {
                node.UncheckAll();
            }
        }

        private void treeNavigation_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeListNode node = e.Node;
            if (node.Checked)
            {
                node.UncheckAll();
            }
            else
            {
                node.CheckAll();
            }
            bool isCompany = false;
            var entity = (HierarchyNodeInfo)node.Tag;
            if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Company) &&
                entity.HierarchyNodeType.Code == "CMP")
                isCompany = true;
            if (!isCompany)
            {
                while (node.ParentNode != null)
                {
                    node = node.ParentNode;
                    bool oneOfChildIsChecked = OneOfChildsIsChecked(node);
                    if (oneOfChildIsChecked)
                    {
                        node.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        node.CheckState = CheckState.Unchecked;
                    }
                }
            }
            else
            {

                while (node.ParentNode != null)
                {
                    node = node.ParentNode;
                    bool oneOfChildIsChecked = OneOfChildsIsChecked(node);
                    if (!oneOfChildIsChecked)
                    {
                        node.CheckState = CheckState.Unchecked;
                    }
                }

            }
        }
        

        private void treeNavigation_AfterCheckNode(object sender, NodeEventArgs e)
        {
            e.Node.ExpandAll();
            GetCheckedNodesOperation op = new GetCheckedNodesOperation();
            treeNavigation.NodesIterator.DoOperation(op);

            var filters = new List<HierarchyNodeInfo>();


            gridTradeDetailsView.ActiveFilterString = "";
            var navigationGridfilter = "";


            List<TradeInformation> datasource = new List<TradeInformation>();

            foreach (TreeListNode checkNode in op.CheckedNodes)
            {
                var entity = (HierarchyNodeInfo)checkNode.Tag;

                _selectedHierarchyNode = entity;
                if (checkNode.Nodes.Count > 0 && _selectedHierarchyNode.DomainObject != null && _selectedHierarchyNode.DomainObject.GetType() == typeof(Company) &&
                    _selectedHierarchyNode.HierarchyNodeType.Code == "CMP" && !_companiesLoadedTrades.ContainsKey(((Company)_selectedHierarchyNode.DomainObject).Id))
                {
                    filters.Add(entity);

                }
                else
                {
                    if (_selectedHierarchyNode.DomainObject != null && _selectedHierarchyNode.DomainObject.GetType() == typeof(Company) &&
                    _selectedHierarchyNode.HierarchyNodeType.Code == "CMP" && _companiesLoadedTrades.ContainsKey(((Company)_selectedHierarchyNode.DomainObject).Id))
                    {
                        datasource.AddRange(_companiesLoadedTrades[((Company)_selectedHierarchyNode.DomainObject).Id]);
                    }
                    if (!nodeHasCheckedChildren(checkNode))
                    {
                        navigationGridfilter = filterQuery(navigationGridfilter, checkNode);
                    }

                }
            }

            if (filters.Count > 0)
            {
                _formState = FormStateEnum.BeforeSearchTree;
                SetGuiControls();
                BeginGetTradesByFilters(filters);
            }
            else
            {
                gridTradeDetails.DataSource = datasource;
                gridTradeDetailsView.ActiveFilterString = navigationGridfilter; //set gridview filter string
                navigationGridfilter = ""; //reset gridview filter string
                gridTradeDetailsView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never; //Hide gridview filterpanel

            }

        }

        private bool nodeHasCheckedChildren(TreeListNode node)
        {
            foreach (TreeListNode childNode in node.Nodes)
            {
                if (childNode.CheckState == CheckState.Checked)
                    return true;
            }
            return false;
        }

        private string filterQuery(string inFilter, TreeListNode nodeInfo)
        {
            if (nodeInfo.ParentNode == null) return inFilter;
            string loFilter = "";
            loFilter = inFilter;
            var checkParentNode = nodeInfo.ParentNode;
            var selectedNodeParents = new ArrayList();
            selectedNodeParents.Add(nodeInfo);
            selectParents(nodeInfo, selectedNodeParents);

            if (inFilter == "") inFilter = "(";
            else inFilter = inFilter + " OR (";
            int cnt = selectedNodeParents.Count;
            int cnt2 = 0;
            foreach (TreeListNode filterNode in selectedNodeParents)
            {
                cnt2++;
                _selectedHierarchyNode = (HierarchyNodeInfo)filterNode.Tag;
                if (_selectedHierarchyNode.HierarchyNodeType.Code == "CMP") inFilter = inFilter + "[Company] = '" + ((Company)_selectedHierarchyNode.DomainObject).Name + "'";
                if (_selectedHierarchyNode.HierarchyNodeType.Code == "MRK") inFilter = inFilter + "[MarketID] = " + ((Market)_selectedHierarchyNode.DomainObject).Id;
                //   if (_selectedHierarchyNode.HierarchyNodeType.Code == "TRD") inFilter = inFilter + "[TraderID] = " + ((Trader)_selectedHierarchyNode.DomainObject).Id;
                if (_selectedHierarchyNode.HierarchyNodeType.Code == "DLT")
                {
                    if (_selectedHierarchyNode.StrObject == "Option") inFilter = inFilter + "([Type] like 'Put' OR [Type] like 'Call')";
                    else inFilter = inFilter + "[Type] like '" + (_selectedHierarchyNode.StrObject) + "'";
                }
                if (_selectedHierarchyNode.HierarchyNodeType.Code == "VSL") inFilter = inFilter + "[VesselId] = " + ((Vessel)_selectedHierarchyNode.DomainObject).Id;
                if (cnt2 < cnt)
                {
                    if (inFilter.Length >= 5)
                    {
                        if (inFilter.Substring(inFilter.Length - 5) != " AND ")
                        {
                            inFilter = inFilter + " AND ";
                        }
                    }
                    else
                    {
                        inFilter = inFilter + " AND ";
                    }
                }

            }

            if (inFilter.Substring(inFilter.Length - 5) == " AND ")
            {
                inFilter.Remove(inFilter.Length - 5);
            }

            inFilter = inFilter + ")";
            return inFilter;
        }

        void selectParents(TreeListNode inNode, ArrayList selectedNodeParents)
        {
            if (inNode.ParentNode != null)
            {
                var parentData = (HierarchyNodeInfo)inNode.ParentNode.Tag;
                if (parentData.HierarchyNodeType.Code != null)
                {
                    var parent = inNode.ParentNode;
                    selectedNodeParents.Add(parent);
                    if (parentData.HierarchyNodeType.Code != "CMP")
                        selectParents(parent, selectedNodeParents);
                }
            }
        }

        private void treeNavigation_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {
            //if (e.Node.CheckState == CheckState.Checked) e.Node.CheckState = CheckState.Unchecked;
            //else e.Node.CheckState = CheckState.Checked;

            //treeNavigation_AfterCheckNode(sender, e);

        }







        private void treeBookNavigation_SelectionChanged(object sender, EventArgs e)
        {
            //if (treeBookNavigation.FocusedNode == null || InSelection) return;


            ////if (filters.Count > 0) filters.Clear();
            //var filters = new List<HierarchyNodeInfo>();
            //var entity = (HierarchyNodeInfo)treeBookNavigation.FocusedNode.Tag;
            //_selectedHierarchyNode = entity;

            ////treeNavigation.FocusedNode = null;
            //_formState = FormStateEnum.BeforeSearchTree;
            //SetGuiControls();

            //SelectNodeAndChildren();

            //if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Book))//If it is a book selection
            //{
            //    //filters = (from TreeListNode listNode in treeBookNavigation.Selection select (HierarchyNodeInfo)listNode.Tag).ToList();
            //    filters = (from TreeListNode listNode in _selectedNodes select (HierarchyNodeInfo)listNode.Tag).ToList();
            //    BeginGetTradesBySelectionBooks(filters);
            //}
            //else
            //{
            //    filters.Add(entity);
            //    BeginGetTradesByFilters(filters);
            //}
        }

        private void treeBookNavigation_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {
            //if (treeBookNavigation.FocusedNode == null || InSelection) return;
            //treeNavigation.FocusedNode = null;

            //string gridfilter = "";
            //gridTradeDetailsView.ActiveFilterString = gridfilter;

            //var filters = new List<HierarchyNodeInfo>();
            //if (filters.Count > 0) filters.Clear();
            //var entity = (HierarchyNodeInfo)treeBookNavigation.FocusedNode.Tag;

            //_selectedHierarchyNode = entity;

            //_formState = FormStateEnum.BeforeSearchTree;
            //SetGuiControls();

            //SelectNodeAndChildren();

            //if (_selectedNodes != null)
            //    _selectedNodes.Add(treeBookNavigation.FocusedNode);

            //if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Book))//If it is a book selection
            //{
            //    filters = (from TreeListNode listNode in _selectedNodes select (HierarchyNodeInfo)listNode.Tag).ToList();
            //    BeginGetTradesBySelectionBooks(filters);
            //}
            //else
            //{
            //    filters.Add(entity);
            //    BeginGetTradesByFilters(filters);
            //}


        }

        private void treeBookNavigation_AfterCheckNode(object sender, NodeEventArgs e)
        {
            //GetCheckedNodesOperation op = new GetCheckedNodesOperation();
            //treeBookNavigation.NodesIterator.DoOperation(op);

            //var filters = new List<HierarchyNodeInfo>();

            //gridTradeDetailsView.ActiveFilterString = "";

            //foreach (TreeListNode checkNode in op.CheckedNodes)
            //{

            //    var entity = (HierarchyNodeInfo)checkNode.Tag;
            //    var checkParentNode = checkNode.ParentNode;
            //    _selectedHierarchyNode = entity;
            //    if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Book))
            //        filters.Add(entity);
            //}

            //if(filters.Count > 0)
            //{
            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesBySelectionBooks(filters);
            //}
            e.Node.ExpandAll();
            GetCheckedNodesOperation op = new GetCheckedNodesOperation();
            treeBookNavigation.NodesIterator.DoOperation(op);

            var filters = new List<HierarchyNodeInfo>();


            gridTradeDetailsView.ActiveFilterString = "";
            var navigationGridfilter = "";


            List<TradeInformation> datasource = new List<TradeInformation>();
            foreach (TreeListNode checkNode in op.CheckedNodes)
            {

                var entity = (HierarchyNodeInfo)checkNode.Tag;
                var checkParentNode = checkNode.ParentNode;
                _selectedHierarchyNode = entity;
                if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Book) &&
                    !_booksLoadedTrades.ContainsKey(((Book)_selectedHierarchyNode.DomainObject).Id))
                {
                    filters.Add(entity);

                }
                else
                {
                    if (entity.DomainObject != null && entity.DomainObject.GetType() == typeof(Book) &&
                    _booksLoadedTrades.ContainsKey(((Book)_selectedHierarchyNode.DomainObject).Id))
                    {
                        foreach (TradeInformation ti in _booksLoadedTrades[((Book)_selectedHierarchyNode.DomainObject).Id])
                        {
                            if (!datasource.Contains(ti))
                            {
                                datasource.Add(ti);
                            }
                        }
                    }

                    navigationGridfilter = filterBooksQuery(navigationGridfilter, checkNode);
                }
            }

            if (filters.Count > 0)
            {
                _formState = FormStateEnum.BeforeSearchTree;
                SetGuiControls();
                BeginGetTradesBySelectionBooks(filters);
            }
            else
            {
                gridTradeDetails.DataSource = datasource;
                gridTradeDetailsView.ActiveFilterString = navigationGridfilter; //set gridview filter string
                navigationGridfilter = ""; //reset gridview filter string
                gridTradeDetailsView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never; //Hide gridview filterpanel

            }
        }

        void treeBookNavigation_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeListNode node = e.Node;
            if (node.Checked)
            {
                node.UncheckAll();
            }
            else
            {
                node.CheckAll();
            }
            //while (node.ParentNode != null)
            //{
            //    node = node.ParentNode;
            //    bool oneOfChildIsChecked = OneOfChildsIsChecked(node);
            //    if (oneOfChildIsChecked)
            //    {
            //        node.CheckState = CheckState.Checked;
            //    }
            //    else
            //    {
            //        node.CheckState = CheckState.Unchecked;
            //    }
            //}

           


        }

        private string filterBooksQuery(string inFilter, TreeListNode nodeInfo)
        {
            if (inFilter == "") inFilter = "(";
            else inFilter = inFilter + " OR (";
            _selectedHierarchyNode = (HierarchyNodeInfo)nodeInfo.Tag;
            if (nodeInfo.ParentNode == null) inFilter = inFilter + "[Books] = '" + ((Book)_selectedHierarchyNode.DomainObject).Name + "'";
            else
            {
                inFilter = inFilter + "Contains([Books], '" + ((Book)_selectedHierarchyNode.DomainObject).Name + "')";
            }



            inFilter = inFilter + ")";
            return inFilter;
        }

        private bool OneOfChildsIsChecked(TreeListNode node)
        {
            bool result = false;
            foreach (TreeListNode item in node.Nodes)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    result = true;
                }
            }
            return result;
        }

        private void ViewTradesForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm1 != null) _dialogForm1.Show();
                if (_dialogForm2 != null) _dialogForm2.Show();
            }
        }

        private void ViewTradesForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm1 != null) _dialogForm1.Hide();
                if (_dialogForm2 != null) _dialogForm2.Hide();
            }
        }

        private void FilterEditors_EditValueChanging(object sender, ChangingEventArgs e)
        {
            //_formState = FormStateEnum.BeforeSearchDates;
            //SetGuiControls();
        }

        private void dtpFilterPositionDate_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void dtpFilterPeriodFrom_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void dtpFilterPeriodTo_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void chkFilterDraft_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void chkFilterMinimum_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void chkFilterOptional_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void chkOnlyNonZeroTotalDays_EditValueChanged(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeFiltering;
            SetGuiControls();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            _formState = FormStateEnum.BeforeSearchDates;
            SetGuiControls();

            isLoad = true;
            BeginGetEntitiesThatHaveTradesByFilters(null, new List<HierarchyNodeInfo>());
        }

        private void gridTradeDetailsView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                var tradeInformation = e.Row as TradeInformation;
                if (tradeInformation != null)
                {
                    if (e.Column.FieldName == "Books")
                    {
                        string books = tradeInformation.TradeInfo.TradeInfoBooks.Aggregate("", (current, tradeInfoBook) =>
                                                                                           current + Books.Single(a => a.Id == tradeInfoBook.BookId).Name + ", ");
                        e.Value = books.Substring(0, books.LastIndexOf(','));
                    }
                }
            }
        }

        private void btnGeneratePosition_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnGeneratePOS != null)
                OnGeneratePOS(GetViewTradesGeneralInfo());
        }

        private void btnGenerateMTM_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnGenerateMTM != null)
                OnGenerateMTM(GetViewTradesGeneralInfo());
        }

        private void btnGenerateMonteCarlo_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var form = new MCCriteriaForm(_lastImportDate);
            form.OnGetMCCriteria += OnGetMCName;
            form.ShowDialog();
        }

        private void OnGetMCName(MCSimulationCriteria criteria)
        {
            //_mcName = mcName;
            GenerateMonteCarloSimulation(criteria);
        }

        private ViewTradeGeneralInfo GetViewTradesGeneralInfo()
        {
            var filteredDataSource = GetFilteredData<TradeInformation>(gridTradeDetailsView);

            var viewTradesGeneralInfo = new ViewTradeGeneralInfo()
            {
                positionDate = dtpFilterPositionDateFrom.DateTime.Date,
                periodFrom = dtpFilterPeriodFrom.DateTime.Date,
                periodTo = dtpFilterPeriodTo.DateTime.Date,
                IsDraft = chkFilterDraft.Checked,
                IsMinimumPeriod = chkFilterMinimum.Checked,
                IsOptionalPeriod = chkFilterOptional.Checked,
                OnlyNonZeroTotalDays = chkOnlyNonZeroTotalDays.Checked,
                books = Books,
                selectedHierarchyNode = _selectedHierarchyNode,
                //tradeIdentifiers = ((List<TradeInformation>)gridTradeDetails.DataSource).Select(a => a.Identifier).ToList(),
                tradeIdentifiers = filteredDataSource.Select(a => a.Identifier).ToList(),
                //tradeInformations = ((List<TradeInformation>)gridTradeDetails.DataSource),
                tradeInformations = filteredDataSource,
                lastImportDate = _lastImportDate
            };

            return viewTradesGeneralInfo;
        }

        public static List<T> GetFilteredData<T>(ColumnView view)
        {
            List<T> resp = new List<T>();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                int rowHandle = view.GetVisibleRowHandle(i);
                resp.Add((T)view.GetRow(rowHandle));
            }
            return resp;
        }

        private void btnGenerateCashFlow_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnGenerateCF != null)
            {
                OnGenerateCF(GetViewTradesGeneralInfo());
            }
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void btnEditTrade_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var tradeInformation = (TradeInformation)gridTradeDetailsView.GetFocusedRow();
            if (tradeInformation == null) return;

            //Check for user permission in trade's trader.If user does not have the permission, show message box. Else open trade for edit.
            BeginCheckUserHasPermissionOnTrader(tradeInformation.TradeInfo.TraderId);

        }

        private void btnViewTrade_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var tradeInformation = (TradeInformation)gridTradeDetailsView.GetFocusedRow();
            if (tradeInformation == null) return;
            if (OnViewTrade != null)
                OnViewTrade(tradeInformation.Trade.Id, tradeInformation.TradeInfo.Id, tradeInformation.ExternalCode,
                            tradeInformation.Trade.Type);
        }

        private void GridTradeDetailsViewFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {

        }

        private void BtnDeleteTradeClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var tradeInformation = (TradeInformation)gridTradeDetailsView.GetFocusedRow();
            if (tradeInformation == null) return;

            DialogResult result = XtraMessageBox.Show(this,
                                                          "The selected trade will be deactivated. Are you sure you want to procceed?",
                                                          Strings.Freight_Metrics,
                                                          MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.No) return;

            long identifierId = Convert.ToInt32(tradeInformation.Identifier.Split('|')[0]);

            TradeTypeEnum typeEnum = (tradeInformation.Type == "Call" || tradeInformation.Type == "Put")
                                         ? TradeTypeEnum.Option
                                         : (TradeTypeEnum)Enum.Parse(typeof(TradeTypeEnum), tradeInformation.Type);

            BeginUpdateTradeStatus(typeEnum,
                                   identifierId,
                                   ActivationStatusEnum.Inactive);
        }

        private void BtnRatiosReportClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (RatiosReportEvent != null)
            {
                RatiosReportEvent(GetViewTradesGeneralInfo());
            }
        }

        private void BtnActivateTradesClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (OnOpenActivateTradesForm != null)
                OnOpenActivateTradesForm();
        }

        private void btnExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (SaveFileDialog saveDialog = new SaveFileDialog())
            {
                saveDialog.FileName = "Trades_" + DateTime.Now.ToString("yyyyMMdd");
                saveDialog.Filter = "Excel (2003-2007)(.xls)|*.xls|Excel (2010-2013) (.xlsx)|*.xlsx |Comma Separated Values (.csv)|*.csv |PDF (.pdf)|*.pdf ";
                if (saveDialog.ShowDialog() != DialogResult.Cancel)
                {
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;
                    switch (fileExtenstion)
                    {
                        case ".xls":
                            gridTradeDetailsView.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            gridTradeDetailsView.ExportToXlsx(exportFilePath);
                            break;
                        case ".pdf":
                            gridTradeDetailsView.ExportToPdf(exportFilePath);
                            break;
                        case ".csv":
                            gridTradeDetailsView.ExportToCsv(exportFilePath);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        #endregion

        #region Proxy Calls

        private void BeginGetTradesByFilters(List<HierarchyNodeInfo> entityTypeFilters)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm1 = new WaitDialogForm(this);
            btnFilterTrades.Enabled = false;
            _formState = FormStateEnum.BeforeSearchTree;
            SetGuiControls();
            //SetEnabledForWorkingStatus(false);
            try
            {
                SessionRegistry.Client.BeginGetTradesByFilters2(dtpFilterPositionDateFrom.DateTime,
                                                                dtpFilterPeriodFrom.DateTime, dtpFilterPeriodTo.DateTime,
                                                                chkFilterDraft.Checked, chkFilterMinimum.Checked,
                                                                chkFilterOptional.Checked, chkOnlyNonZeroTotalDays.Checked, entityTypeFilters,
                                                                EndGetTradesByFilters, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnFilterTrades.Enabled = true;
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
            }
        }

        private void EndGetTradesByFilters(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetTradesByFilters;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Trade> trades;
            List<Company> companies;

            try
            {
                result = SessionRegistry.Client.EndGetTradesByFilters2(out trades, out companies, ar);
            }
            catch (Exception)
            {
                btnFilterTrades.Enabled = true;
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (result == null) //Exception Occurred
            {
                btnFilterTrades.Enabled = true;
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                var tradeInformations = new List<TradeInformation>();

                foreach (Trade tradeTC in trades.Where(a => a.Type == TradeTypeEnum.TC))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeTC.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeTC.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    
                    foreach (TradeTcInfoLeg tradeTCleg in tradeTC.Info.TCInfo.Legs)
                    {
                        tradeInformations.Add(new TradeInformation
                        {
                            Identifier = tradeTCleg.Id + "|TC",
                            Brokers = tradeTC.Info.BrokersName,
                            Company = tradeTC.Info.CompanyName,
                            Ownership = Convert.ToDecimal(tradeTC.Info.Ownership),// System.Globalization.CultureInfo.GetCultureInfo("no")), //companies.Count>1 ? companies.Where(n=>n.Name ==tradeTC.Info.CompanyName).Select(n=>n.Ownership).ToString():"100",
                            Counterparty = tradeTC.Info.CounterpartyName,
                            MarketID = tradeTC.Info.MarketId,
                            Direction = tradeTC.Info.Direction.ToString("g"),
                            ExternalCode = tradeTC.Info.ExternalCode,
                            LegNumber = tradeTCleg.Identifier,
                            Status = tradeTC.Status.ToString("g"),
                            Type = tradeTC.Type.ToString("g"),
                            PhysicalFinancial =
                                tradeTC.Type == TradeTypeEnum.TC ||
                                tradeTC.Type == TradeTypeEnum.Cargo
                                    ? "Physical"
                                    : "Financial",
                            Price = tradeTCleg.Rate.HasValue ? (tradeTCleg.Rate.Value) : 0, // * Convert.ToDecimal(tradeTC.Info.Ownership):0,// System.Globalization.CultureInfo.GetCultureInfo("no")) : 0,
                            SignDate = tradeTC.Info.SignDate,
                            PeriodFrom = tradeTCleg.PeriodFrom,
                            PeriodTo = tradeTCleg.PeriodTo,
                            Vessel = tradeTC.Info.TCInfo.VesselName,
                            VesselId = tradeTC.Info.TCInfo.VesselId,
                            ForwardCurve = tradeTC.Info.MTMFwdIndexName,
                            ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeTC.Info.MTMFwdIndexName,
                            FCoverAssocIndexName = coverAssocIndexName,
                            FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                            StrikePrice = 0,
                            MarketTonnes = tradeTC.Info.MarketTonnes,
                            Trade = tradeTC,
                            TradeInfo = tradeTC.Info
                        });
                    }
                }


                foreach (Trade tradeFFA in trades.Where(a => a.Type == TradeTypeEnum.FFA))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeFFA.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeFFA.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    tradeInformations.Add(new TradeInformation
                    {
                        Identifier = tradeFFA.Info.FFAInfo.Id + "|FFA",
                        Brokers = tradeFFA.Info.BrokersName,
                        Company = tradeFFA.Info.CompanyName,
                        Ownership = Convert.ToDecimal(tradeFFA.Info.Ownership), //, System.Globalization.CultureInfo.GetCultureInfo("no")), //companies.Count > 1 ? companies.Where(n => n.Name == tradeFFA.Info.CompanyName).Select(n => n.Ownership).ToString() : "100",
                        Counterparty = tradeFFA.Info.CounterpartyName,
                        MarketID = tradeFFA.Info.MarketId,
                        Direction = tradeFFA.Info.Direction.ToString("g"),
                        ExternalCode = tradeFFA.Info.ExternalCode,
                        LegNumber = 0,
                        Status = tradeFFA.Status.ToString("g"),
                        Type = tradeFFA.Type.ToString("g"),
                        PhysicalFinancial =
                            tradeFFA.Type == TradeTypeEnum.TC ||
                            tradeFFA.Type == TradeTypeEnum.Cargo
                                ? "Physical"
                                : "Financial",
                        Price = tradeFFA.Info.FFAInfo.Price, // * Convert.ToDecimal(tradeFFA.Info.Ownership), //, System.Globalization.CultureInfo.GetCultureInfo("no")),
                        SignDate = tradeFFA.Info.SignDate,
                        PeriodFrom = tradeFFA.Info.PeriodFrom,
                        PeriodTo = tradeFFA.Info.PeriodTo,
                        Vessel = tradeFFA.Info.FFAInfo.VesselName,
                        VesselId = tradeFFA.Info.FFAInfo.VesselId,
                        ForwardCurve = tradeFFA.Info.MTMFwdIndexName,
                        ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeFFA.Info.MTMFwdIndexName,
                        FCoverAssocIndexName = coverAssocIndexName,
                        FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                        StrikePrice = 0,
                        MarketTonnes = tradeFFA.Info.MarketTonnes,
                        Trade = tradeFFA,
                        TradeInfo = tradeFFA.Info
                    });
                }

                foreach (Trade tradeCargo in trades.Where(a => a.Type == TradeTypeEnum.Cargo))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeCargo.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeCargo.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    foreach (TradeCargoInfoLeg tradeCargoLeg in tradeCargo.Info.CargoInfo.Legs)
                    {
                        tradeInformations.Add(new TradeInformation
                        {
                            Identifier = tradeCargoLeg.Id + "|CARGO",
                            Brokers = tradeCargo.Info.BrokersName,
                            Company = tradeCargo.Info.CompanyName,
                            Ownership = Convert.ToDecimal(tradeCargo.Info.Ownership),// System.Globalization.CultureInfo.GetCultureInfo("no")),//companies.Count > 1 ? companies.Where(n => n.Name == tradeCargo.Info.CompanyName).Select(n => n.Ownership).ToString() : "100",
                            Counterparty = tradeCargo.Info.CounterpartyName,
                            MarketID = tradeCargo.Info.MarketId,
                            Direction = tradeCargo.Info.Direction.ToString("g"),
                            ExternalCode = tradeCargo.Info.ExternalCode,
                            LegNumber = tradeCargoLeg.Identifier,
                            Status = tradeCargo.Status.ToString("g"),
                            Type = tradeCargo.Type.ToString("g"),
                            PhysicalFinancial = "Physical",
                            Price = tradeCargoLeg.Tce.HasValue ? tradeCargoLeg.Tce.Value : 0, // * Convert.ToDecimal(tradeCargo.Info.Ownership):0,// System.Globalization.CultureInfo.GetCultureInfo("no")) : 0,
                            SignDate = tradeCargo.Info.SignDate,
                            PeriodFrom = tradeCargoLeg.PeriodFrom,
                            PeriodTo = tradeCargoLeg.PeriodTo,
                            Vessel = tradeCargo.Info.CargoInfo.VesselName,
                            VesselId = tradeCargo.Info.CargoInfo.VesselId,
                            ForwardCurve = tradeCargo.Info.MTMFwdIndexName,
                            ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeCargo.Info.MTMFwdIndexName,
                            FCoverAssocIndexName = coverAssocIndexName,
                            FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                            StrikePrice = 0,
                            MarketTonnes = tradeCargo.Info.MarketTonnes,
                            Trade = tradeCargo,
                            TradeInfo = tradeCargo.Info
                        });
                    }
                }

                foreach (Trade tradeOption in trades.Where(a => a.Type == TradeTypeEnum.Option))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeOption.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeOption.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    tradeInformations.Add(new TradeInformation
                    {
                        Identifier = tradeOption.Info.OptionInfo.Id + "|OPTION",
                        Brokers = tradeOption.Info.BrokersName,
                        Company = tradeOption.Info.CompanyName,
                        Ownership = Convert.ToDecimal(tradeOption.Info.Ownership), //, System.Globalization.CultureInfo.GetCultureInfo("no")),//companies.Count > 1 ? companies.Where(n => n.Name == tradeOption.Info.CompanyName).Select(n => n.Ownership).ToString() : "100",
                        Counterparty = tradeOption.Info.CounterpartyName,
                        MarketID = tradeOption.Info.MarketId,
                        Direction = tradeOption.Info.Direction.ToString("g"),
                        ExternalCode = tradeOption.Info.ExternalCode,
                        LegNumber = 0,
                        Status = tradeOption.Status.ToString("g"),
                        Type = tradeOption.Info.OptionInfo.Type.ToString("g"),
                        PhysicalFinancial =
                            tradeOption.Type == TradeTypeEnum.TC ||
                            tradeOption.Type == TradeTypeEnum.Cargo
                                ? "Physical"
                                : "Financial",
                        Price = tradeOption.Info.OptionInfo.Premium, // * Convert.ToDecimal(tradeOption.Info.Ownership),// System.Globalization.CultureInfo.GetCultureInfo("no")),
                        SignDate = tradeOption.Info.SignDate,
                        PeriodFrom = tradeOption.Info.PeriodFrom,
                        PeriodTo = tradeOption.Info.PeriodTo,
                        Vessel = tradeOption.Info.OptionInfo.VesselName,
                        VesselId = tradeOption.Info.OptionInfo.VesselId,
                        ForwardCurve = tradeOption.Info.MTMFwdIndexName,
                        ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeOption.Info.MTMFwdIndexName,
                        FCoverAssocIndexName = coverAssocIndexName,
                        FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                        StrikePrice = tradeOption.Info.OptionInfo.Strike, // * Convert.ToDecimal(tradeOption.Info.Ownership), //, System.Globalization.CultureInfo.GetCultureInfo("no")),
                        MarketTonnes = tradeOption.Info.MarketTonnes,
                        Trade = tradeOption,
                        TradeInfo = tradeOption.Info
                    });
                }




                //gridTradeDetails.DataSource = tradeInformations;
                var datasource = new List<TradeInformation>();


                var navigationGridfilter = "";
                GetCheckedNodesOperation op = new GetCheckedNodesOperation();
                treeNavigation.NodesIterator.DoOperation(op);

                var filters = new List<HierarchyNodeInfo>();
                var tempCompanyIds = new List<long>();

                foreach (TreeListNode checkNode in op.CheckedNodes)
                {
                    var entity = (HierarchyNodeInfo)checkNode.Tag;

                    _selectedHierarchyNode = entity;
                    if (_selectedHierarchyNode.DomainObject != null && _selectedHierarchyNode.DomainObject.GetType() == typeof(Company) &&
                        _selectedHierarchyNode.HierarchyNodeType.Code == "CMP" && !_companiesLoadedTrades.ContainsKey(((Company)_selectedHierarchyNode.DomainObject).Id)
                        && (((Company)_selectedHierarchyNode.DomainObject).ParentId == null || !tempCompanyIds.Contains(((Company)_selectedHierarchyNode.DomainObject).ParentId.Value)))
                    {
                        tempCompanyIds.Add(((Company)_selectedHierarchyNode.DomainObject).Id);
                        _companiesLoadedTrades.Add(((Company)_selectedHierarchyNode.DomainObject).Id, tradeInformations.Where(t => t.Company == ((Company)_selectedHierarchyNode.DomainObject).Name).ToList());

                    }
                    else
                    {
                        if (_selectedHierarchyNode.DomainObject != null && _selectedHierarchyNode.DomainObject.GetType() == typeof(Company) &&
                        _selectedHierarchyNode.HierarchyNodeType.Code == "CMP" && _companiesLoadedTrades.ContainsKey(((Company)_selectedHierarchyNode.DomainObject).Id))
                        {
                            datasource.AddRange(_companiesLoadedTrades[((Company)_selectedHierarchyNode.DomainObject).Id]);
                        }
                        if (!nodeHasCheckedChildren(checkNode))
                        {
                            navigationGridfilter = filterQuery(navigationGridfilter, checkNode);
                        }

                    }

                }

                datasource.AddRange(tradeInformations);

                gridTradeDetails.DataSource = datasource;

                gridTradeDetailsView.ActiveFilterString = navigationGridfilter; //set gridview filter string
                navigationGridfilter = ""; //reset gridview filter string
                gridTradeDetailsView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never; //Hide gridview filterpanel



                _formState = FormStateEnum.AfterSearch;

                SetGuiControls();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    if (_dialogForm1 != null)
                    {
                        _dialogForm1.Close();
                        _dialogForm1 = null;
                    }
                }

                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
            }
        }

        private void BeginGetEntitiesThatHaveTradesByFilters(long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters)
        {
            Cursor = Cursors.WaitCursor;
            //_dialogForm2 = new WaitDialogForm(this);
            btnFilterTrades.Enabled = false;
            //SetEnabledForWorkingStatus(false);
            try
            {
                SessionRegistry.Client.BeginGetEntitiesThatHaveTradesByFilters2(dtpFilterPositionDateFrom.DateTime,
                                                                                dtpFilterPeriodFrom.DateTime,
                                                                                dtpFilterPeriodTo.DateTime,
                                                                                chkFilterDraft.Checked,
                                                                                hierarchyNodeTypeId,
                                                                                entityTypeFilters,
                                                                                EndGetEntitiesThatHaveTradesByFilters,
                                                                                null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                    //{
                    //    _dialogForm2.Close();
                    //    _dialogForm2 = null;
                    //}
                    _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void EndGetEntitiesThatHaveTradesByFilters(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetEntitiesThatHaveTradesByFilters;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<HierarchyNodeInfo> entities;
            TreeListNode companyNode = null;
            try
            {
                result = SessionRegistry.Client.EndGetEntitiesThatHaveTradesByFilters2(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                //lock (_syncObject)
                //{
                //    _dialogForm2.Close();
                //    _dialogForm2 = null;
                //}                //{
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                //lock (_syncObject)
                //    _dialogForm2.Close();
                //    _dialogForm2 = null;
                //}
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                treeNavigation.FocusedNodeChanged -= treeNavigation_FocusedNodeChanged;

                treeNavigation.BeginUnboundLoad();

                TreeListNode parentNode = _expandedNode;

                //TreeListNode parentNode = null;


                //if (isLoad)
                //{
                //    parentNode = _expandedNode;
                //}
                //else
                //{

                //    if (currentTypeFilters.Count > 0)
                //    {
                //        var parent = currentTypeFilters[currentTypeFilters.Count - 1];


                //        foreach (TreeListNode node in treeNavigation.Nodes)
                //        {

                //            if (parent.DomainObject.GetType() == typeof(Market))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Market)nodeObject.DomainObject).Id == ((Market)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }

                //            else if (parent.DomainObject.GetType() == typeof(Company) && parent.HierarchyNodeType.Code == "CMP")
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Company)nodeObject.DomainObject).Name == ((Company)parent.DomainObject).Name)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }

                //            else if (parent.DomainObject.GetType() == typeof(Trader))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Trader)nodeObject.DomainObject).Id == ((Trader)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(Company) && parent.HierarchyNodeType.Code == "BRK")
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Company)nodeObject.DomainObject).Id == ((Company)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(Vessel))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Vessel)nodeObject.DomainObject).Id == ((Vessel)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(VesselPool))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((VesselPool)nodeObject.DomainObject).Id == ((VesselPool)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(Desk))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Desk)nodeObject.DomainObject).Id == ((Desk)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(ProfitCentre))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((ProfitCentre)nodeObject.DomainObject).Id == ((ProfitCentre)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(Index))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Index)nodeObject.DomainObject).Id == ((Index)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //            else if (parent.DomainObject.GetType() == typeof(ClearingHouse))
                //            {
                //                var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((ClearingHouse)nodeObject.DomainObject).Id == ((ClearingHouse)parent.DomainObject).Id)
                //                {
                //                    parentNode = node;
                //                    break;
                //                }
                //            }
                //        }


                //        if (currentTypeFilters.Count > 1)
                //        {
                //            for (int i = currentTypeFilters.Count - 2; i >= 0; i--)
                //            {
                //                foreach (TreeListNode node in parentNode.Nodes)
                //                {
                //                    parent = currentTypeFilters[i];

                //                    if (parent.DomainObject != null)
                //                    {
                //                        if (parent.DomainObject.GetType() == typeof(Market))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Market)nodeObject.DomainObject).Id == ((Market)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }

                //                        else if (parent.DomainObject.GetType() == typeof(Company) && currentTypeFilters[i].HierarchyNodeType.Code == "CMP")
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Company)nodeObject.DomainObject).Name == ((Company)parent.DomainObject).Name)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }

                //                        else if (parent.DomainObject.GetType() == typeof(Trader))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Trader)nodeObject.DomainObject).Id == ((Trader)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (parent.DomainObject.GetType() == typeof(Company) && currentTypeFilters[i].HierarchyNodeType.Code == "BRK")
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Company)nodeObject.DomainObject).Id == ((Company)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (parent.DomainObject.GetType() == typeof(Vessel))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Vessel)nodeObject.DomainObject).Id == ((Vessel)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (parent.DomainObject.GetType() == typeof(VesselPool))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((VesselPool)nodeObject.DomainObject).Id == ((VesselPool)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (parent.DomainObject.GetType() == typeof(Desk))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Desk)nodeObject.DomainObject).Id == ((Desk)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (parent.DomainObject.GetType() == typeof(ProfitCentre))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((ProfitCentre)nodeObject.DomainObject).Id == ((ProfitCentre)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (currentTypeFilters[i].DomainObject.GetType() == typeof(Index))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((Index)nodeObject.DomainObject).Id == ((Index)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                        else if (parent.DomainObject.GetType() == typeof(ClearingHouse))
                //                        {
                //                            var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                            if (nodeObject.DomainObject.GetType() == parent.DomainObject.GetType() && ((ClearingHouse)nodeObject.DomainObject).Id == ((ClearingHouse)parent.DomainObject).Id)
                //                            {
                //                                parentNode = node;
                //                                break;
                //                            }
                //                        }
                //                    }
                //                    else
                //                    {
                //                        var nodeObject = ((HierarchyNodeInfo)node.Tag);
                //                        if (nodeObject.StrObject == parent.StrObject)
                //                        {
                //                            parentNode = node;
                //                            break;
                //                        }

                //                    }
                //                }
                //            }
                //        }
                //    }
                //}



                foreach (HierarchyNodeInfo entity in entities)
                {
                    TreeListNode newNode = null;

                    if (entity.DomainObject != null)
                    {
                        if (entity.DomainObject.GetType() == typeof(Market))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Market)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 2;
                        }

                        else if (entity.DomainObject.GetType() == typeof(Company) && entity.HierarchyNodeType.Code == "CMP")
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Company)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 0;
                        }

                        else if (entity.DomainObject.GetType() == typeof(Trader))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Trader)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 3;
                        }
                        else if (entity.DomainObject.GetType() == typeof(Company) && entity.HierarchyNodeType.Code == "BRK")
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Company)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 8;
                        }
                        else if (entity.DomainObject.GetType() == typeof(Vessel))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Vessel)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 6;
                        }
                        else if (entity.DomainObject.GetType() == typeof(VesselPool))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((VesselPool)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 7;
                        }
                        else if (entity.DomainObject.GetType() == typeof(Desk))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Desk)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 10;
                        }
                        else if (entity.DomainObject.GetType() == typeof(ProfitCentre))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((ProfitCentre)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 10;
                        }
                        else if (entity.DomainObject.GetType() == typeof(Index))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((Index)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 12;
                        }
                        else if (entity.DomainObject.GetType() == typeof(ClearingHouse))
                        {
                            newNode = treeNavigation.AppendNode(new[] { ((ClearingHouse)entity.DomainObject).Name }, parentNode, entity);
                            newNode.StateImageIndex = 12;
                        }

                        if (!isLoad)
                        {
                            //treeNavigation.BeforeExpand += treeNavigation_BeforeExpand;
                            newNode.ExpandAll();
                        }

                    }
                    else
                    {
                        newNode = treeNavigation.AppendNode(new[] { entity.StrObject }, parentNode, entity);
                        if (entity.HierarchyNodeType.Code == "DLT")
                            newNode.StateImageIndex = 13;
                        else if (entity.HierarchyNodeType.Code == "PFT")
                            newNode.StateImageIndex = 11;
                        else if (entity.HierarchyNodeType.Code == "LRT")
                            newNode.StateImageIndex = 11;
                        else if (entity.HierarchyNodeType.Code == "OPT")
                            newNode.StateImageIndex = 12;
                        else if (entity.HierarchyNodeType.Code == "SETPERIOD")
                            newNode.StateImageIndex = 15;
                        else if (entity.HierarchyNodeType.Code == "DIR")
                            newNode.StateImageIndex = 9;
                        else if (entity.HierarchyNodeType.Code == "STA")
                            newNode.StateImageIndex = 14;
                        else if (entity.HierarchyNodeType.Code == "CMPTYPE")
                            newNode.StateImageIndex = 16;
                        else if (entity.HierarchyNodeType.Code == "CMPSUBTYPE")
                            newNode.StateImageIndex = 17;
                        else if (entity.HierarchyNodeType.Code == "OTC")
                            newNode.StateImageIndex = 11;
                    }
                    if (newNode != null)
                    {
                        newNode.HasChildren = true;
                        if (!isLoad)
                        {
                            //treeNavigation.BeforeExpand += treeNavigation_BeforeExpand;
                            newNode.ExpandAll();
                        }
                    }

                }


                treeNavigation.EndUnboundLoad();
                treeNavigation.FocusedNode = null;
                treeNavigation.FocusedNodeChanged += treeNavigation_FocusedNodeChanged;





                //lock (_syncObject)
                //{
                //    _dialogForm2.Close();
                //    _dialogForm2 = null;
                //}
                if (isLoad)
                {
                    BeginGeInitializationData();
                    GetAppParameters();

                }
                else
                {
                    Cursor = Cursors.Default;
                    _formState = FormStateEnum.AfterSearch;
                    SetGuiControls();
                }

            }


        }

        private void BeginGetCompanyHierarchyThatHaveTradesByFilters(long? hierarchyNodeTypeId, List<HierarchyNodeInfo> entityTypeFilters)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm2 = new WaitDialogForm(this);
            btnFilterTrades.Enabled = false;
            //SetEnabledForWorkingStatus(false);
            try
            {
                SessionRegistry.Client.BeginGetCompanyHierarchyThatHaveTradesByFilters(dtpFilterPositionDateFrom.DateTime,
                                                                                dtpFilterPeriodFrom.DateTime,
                                                                                dtpFilterPeriodTo.DateTime,
                                                                                chkFilterDraft.Checked,
                                                                                hierarchyNodeTypeId,
                                                                                entityTypeFilters[0],
                                                                                EndGetCompanyHierarchyThatHaveTradesByFilters,
                                                                                null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    if (_dialogForm2 != null)
                    {
                        _dialogForm2.Close();
                        _dialogForm2 = null;

                    }
                }
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void EndGetCompanyHierarchyThatHaveTradesByFilters(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetCompanyHierarchyThatHaveTradesByFilters;
                Invoke(action, ar);
                return;
            }

            int? result;
            TreeListNode companyNode = null;
            Dictionary<HierarchyNodeInfo, List<HierarchyNodeInfo>> parentEntities;
            try
            {
                result = SessionRegistry.Client.EndGetCompanyHierarchyThatHaveTradesByFilters(out parentEntities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    if (_dialogForm2 != null)
                    {
                        _dialogForm2.Close();
                        _dialogForm2 = null;

                    }
                }
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    if (_dialogForm2 != null)
                    {
                        _dialogForm2.Close();
                        _dialogForm2 = null;

                    }
                }
                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                treeNavigation.FocusedNodeChanged -= treeNavigation_FocusedNodeChanged;

                treeNavigation.BeginUnboundLoad();

                List<TreeListNode> newNodes = new List<TreeListNode>();
                newNodes.Add(_expandedNode);

                foreach (var parentEntity in parentEntities)
                {
                    TreeListNode parentNode = null;
                    TreeListNode lastCompanyNode = null;
                    var parentsCount = parentEntity.Value.Count;
                    foreach (TreeListNode checkParentNode in newNodes)
                    {
                        var rightHierarchy = false;
                        if (parentsCount > 1)
                        {
                            TreeListNode nn = checkParentNode;
                            while (nn != null)
                            {
                                var checkNN = (HierarchyNodeInfo)nn.Tag;
                                if (checkNN.DomainObject != null && checkNN.DomainObject.GetType() == typeof(Company) && checkNN.HierarchyNodeType.Code == "CMP")
                                {
                                    if (parentEntity.Value[parentsCount - 1].DomainObject.GetType() == checkNN.DomainObject.GetType() && ((Company)parentEntity.Value[parentsCount - 1].DomainObject).Name == ((Company)checkNN.DomainObject).Name)
                                    {
                                        rightHierarchy = true;
                                    }
                                    break;
                                }
                                nn = nn.ParentNode;
                            }
                        }
                        else
                        {
                            rightHierarchy = true;
                        }

                        if (!rightHierarchy) continue;

                        var checkParentEntity = (HierarchyNodeInfo)checkParentNode.Tag;
                        //get last company in hierarchy

                        if (checkParentEntity.DomainObject != null && parentEntity.Value[0].DomainObject != null)
                        {
                            if (checkParentEntity.DomainObject.GetType() == typeof(Market))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Market)parentEntity.Value[0].DomainObject).Id == ((Market)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }

                            else if (checkParentEntity.DomainObject.GetType() == typeof(Company) && checkParentEntity.HierarchyNodeType.Code == "CMP")
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Company)parentEntity.Value[0].DomainObject).Name == ((Company)checkParentEntity.DomainObject).Name)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }

                            else if (checkParentEntity.DomainObject.GetType() == typeof(Trader))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Trader)parentEntity.Value[0].DomainObject).Id == ((Trader)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(Company) && checkParentEntity.HierarchyNodeType.Code == "BRK")
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Company)parentEntity.Value[0].DomainObject).Id == ((Company)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(Vessel))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Vessel)parentEntity.Value[0].DomainObject).Id == ((Vessel)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(VesselPool))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((VesselPool)parentEntity.Value[0].DomainObject).Id == ((VesselPool)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(Desk))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Desk)parentEntity.Value[0].DomainObject).Id == ((Desk)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(ProfitCentre))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((ProfitCentre)parentEntity.Value[0].DomainObject).Id == ((ProfitCentre)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(Index))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((Index)parentEntity.Value[0].DomainObject).Id == ((Index)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                            else if (checkParentEntity.DomainObject.GetType() == typeof(ClearingHouse))
                            {
                                if (parentEntity.Value[0].DomainObject.GetType() == checkParentEntity.DomainObject.GetType() && ((ClearingHouse)parentEntity.Value[0].DomainObject).Id == ((ClearingHouse)checkParentEntity.DomainObject).Id)
                                {
                                    parentNode = checkParentNode;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (checkParentEntity.StrObject == parentEntity.Value[0].StrObject)
                            {
                                parentNode = checkParentNode;
                            }
                        }
                    }
                    if (parentNode != null)
                    {
                        TreeListNode newNode = null;
                        var entity = parentEntity.Key;
                        if (entity.DomainObject != null)
                        {
                            if (entity.DomainObject.GetType() == typeof(Market))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Market)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 2;
                            }

                            else if (entity.DomainObject.GetType() == typeof(Company) && entity.HierarchyNodeType.Code == "CMP")
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Company)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 0;
                            }

                            else if (entity.DomainObject.GetType() == typeof(Trader))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Trader)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 3;
                            }
                            else if (entity.DomainObject.GetType() == typeof(Company) && entity.HierarchyNodeType.Code == "BRK")
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Company)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 8;
                            }
                            else if (entity.DomainObject.GetType() == typeof(Vessel))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Vessel)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 6;
                            }
                            else if (entity.DomainObject.GetType() == typeof(VesselPool))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((VesselPool)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 7;
                            }
                            else if (entity.DomainObject.GetType() == typeof(Desk))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Desk)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 10;
                            }
                            else if (entity.DomainObject.GetType() == typeof(ProfitCentre))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((ProfitCentre)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 10;
                            }
                            else if (entity.DomainObject.GetType() == typeof(Index))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((Index)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 12;
                            }
                            else if (entity.DomainObject.GetType() == typeof(ClearingHouse))
                            {
                                newNode = treeNavigation.AppendNode(new[] { ((ClearingHouse)entity.DomainObject).Name }, parentNode, entity);
                                newNode.StateImageIndex = 12;
                            }
                            newNodes.Add(newNode);
                        }
                        else
                        {
                            newNode = treeNavigation.AppendNode(new[] { entity.StrObject }, parentNode, entity);
                            if (entity.HierarchyNodeType.Code == "DLT")
                                newNode.StateImageIndex = 13;
                            else if (entity.HierarchyNodeType.Code == "PFT")
                                newNode.StateImageIndex = 11;
                            else if (entity.HierarchyNodeType.Code == "LRT")
                                newNode.StateImageIndex = 11;
                            else if (entity.HierarchyNodeType.Code == "OPT")
                                newNode.StateImageIndex = 12;
                            else if (entity.HierarchyNodeType.Code == "SETPERIOD")
                                newNode.StateImageIndex = 15;
                            else if (entity.HierarchyNodeType.Code == "DIR")
                                newNode.StateImageIndex = 9;
                            else if (entity.HierarchyNodeType.Code == "STA")
                                newNode.StateImageIndex = 14;
                            else if (entity.HierarchyNodeType.Code == "CMPTYPE")
                                newNode.StateImageIndex = 16;
                            else if (entity.HierarchyNodeType.Code == "CMPSUBTYPE")
                                newNode.StateImageIndex = 17;
                            else if (entity.HierarchyNodeType.Code == "OTC")
                                newNode.StateImageIndex = 11;
                        }
                        if (newNode != null)
                        {
                            newNode.HasChildren = true;
                            newNodes.Add(newNode);
                        }
                    }






                }


                treeNavigation.EndUnboundLoad();
                treeNavigation.FocusedNode = null;
                treeNavigation.FocusedNodeChanged += treeNavigation_FocusedNodeChanged;




                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    if (_dialogForm2 != null)
                    {
                        _dialogForm2.Close();
                        _dialogForm2 = null;

                    }

                }
                if (isLoad)
                {
                    BeginGeInitializationData();
                    GetAppParameters();

                }
                else
                {
                    if (_expandedNode.CheckState == CheckState.Checked)
                    {
                        CheckAllChildren(_expandedNode);
                        _expandedNode.ExpandAll();

                        GetCheckedNodesOperation op = new GetCheckedNodesOperation();
                        treeNavigation.NodesIterator.DoOperation(op);

                        var filters = new List<HierarchyNodeInfo>();


                        foreach (TreeListNode checkNode in op.CheckedNodes)
                        {
                            var entity = (HierarchyNodeInfo)checkNode.Tag;

                            _selectedHierarchyNode = entity;
                            if (_selectedHierarchyNode.DomainObject != null && _selectedHierarchyNode.DomainObject.GetType() == typeof(Company) &&
                                _selectedHierarchyNode.HierarchyNodeType.Code == "CMP" && !_companiesLoadedTrades.ContainsKey(((Company)_selectedHierarchyNode.DomainObject).Id))
                            {
                                filters.Add(entity);

                            }

                        }

                        if (filters.Count > 0)
                        {
                            BeginGetTradesByFilters(filters);
                            return;
                        }




                    }
                    else
                    {
                        _expandedNode.ExpandAll();
                    }
                }

                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
            }


        }

        private void CheckAllChildren(TreeListNode parentNode)
        {
            foreach (TreeListNode childNode in parentNode.Nodes)
            {
                childNode.CheckState = CheckState.Checked;

                CheckAllChildren(childNode);
            }
        }

        private void BeginGetTradesBySelectionBooks(List<HierarchyNodeInfo> entityTypeFilters)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm1 = new WaitDialogForm(this);
            btnFilterTrades.Enabled = false;
            //SetEnabledForWorkingStatus(false);
            try
            {
                SessionRegistry.Client.BeginGetTradesBySelectionBooks(dtpFilterPositionDateFrom.DateTime,
                                                                dtpFilterPeriodFrom.DateTime, dtpFilterPeriodTo.DateTime,
                                                                chkFilterDraft.Checked, chkFilterMinimum.Checked,
                                                                chkFilterOptional.Checked, chkOnlyNonZeroTotalDays.Checked, entityTypeFilters,
                                                                EndGetTradesBySelectionBooks, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnFilterTrades.Enabled = true;
                //SetEnabledForWorkingStatus(true);
            }
        }

        private void EndGetTradesBySelectionBooks(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetTradesBySelectionBooks;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Trade> trades;

            try
            {
                result = SessionRegistry.Client.EndGetTradesBySelectionBooks(out trades, ar);
            }
            catch (Exception)
            {
                btnFilterTrades.Enabled = true;
                //SetEnabledForWorkingStatus(true);
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (result == null) //Exception Occurred
            {
                btnFilterTrades.Enabled = true;
                //SetEnabledForWorkingStatus(true);
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                var tradeInformations = new List<TradeInformation>();
                foreach (Trade tradeTC in trades.Where(a => a.Type == TradeTypeEnum.TC))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeTC.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeTC.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    foreach (TradeTcInfoLeg tradeTCleg in tradeTC.Info.TCInfo.Legs)
                    {
                        tradeInformations.Add(new TradeInformation
                        {
                            Identifier = tradeTCleg.Id + "|TC",
                            Brokers = tradeTC.Info.BrokersName,
                            Company = tradeTC.Info.CompanyName,
                            Ownership = Convert.ToDecimal(tradeTC.Info.Ownership),
                            Counterparty = tradeTC.Info.CounterpartyName,
                            Direction = tradeTC.Info.Direction.ToString("g"),
                            ExternalCode = tradeTC.Info.ExternalCode,
                            LegNumber = tradeTCleg.Identifier,
                            Status = tradeTC.Status.ToString("g"),
                            Type = tradeTC.Type.ToString("g"),
                            PhysicalFinancial =
                                tradeTC.Type == TradeTypeEnum.TC ||
                                tradeTC.Type == TradeTypeEnum.Cargo
                                    ? "Physical"
                                    : "Financial",
                            Price = tradeTCleg.Rate.HasValue ? tradeTCleg.Rate.Value : 0,
                            SignDate = tradeTC.Info.SignDate,
                            PeriodFrom = tradeTCleg.PeriodFrom,
                            PeriodTo = tradeTCleg.PeriodTo,
                            Vessel = tradeTC.Info.TCInfo.VesselName,
                            ForwardCurve = tradeTC.Info.MTMFwdIndexName,
                            ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeTC.Info.MTMFwdIndexName,
                            FCoverAssocIndexName = coverAssocIndexName,
                            FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                            StrikePrice = 0,
                            MarketTonnes = tradeTC.Info.MarketTonnes,
                            Trade = tradeTC,
                            TradeInfo = tradeTC.Info
                        });
                    }
                }


                foreach (Trade tradeFFA in trades.Where(a => a.Type == TradeTypeEnum.FFA))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeFFA.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeFFA.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    tradeInformations.Add(new TradeInformation
                    {
                        Identifier = tradeFFA.Info.FFAInfo.Id + "|FFA",
                        Brokers = tradeFFA.Info.BrokersName,
                        Company = tradeFFA.Info.CompanyName,
                        Ownership = Convert.ToDecimal(tradeFFA.Info.Ownership),
                        Counterparty = tradeFFA.Info.CounterpartyName,
                        Direction = tradeFFA.Info.Direction.ToString("g"),
                        ExternalCode = tradeFFA.Info.ExternalCode,
                        LegNumber = 0,
                        Status = tradeFFA.Status.ToString("g"),
                        Type = tradeFFA.Type.ToString("g"),
                        PhysicalFinancial =
                            tradeFFA.Type == TradeTypeEnum.TC ||
                            tradeFFA.Type == TradeTypeEnum.Cargo
                                ? "Physical"
                                : "Financial",
                        Price = tradeFFA.Info.FFAInfo.Price,
                        SignDate = tradeFFA.Info.SignDate,
                        PeriodFrom = tradeFFA.Info.PeriodFrom,
                        PeriodTo = tradeFFA.Info.PeriodTo,
                        Vessel = tradeFFA.Info.FFAInfo.VesselName,
                        ForwardCurve = tradeFFA.Info.MTMFwdIndexName,
                        ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeFFA.Info.MTMFwdIndexName,
                        FCoverAssocIndexName = coverAssocIndexName,
                        FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                        StrikePrice = 0,
                        MarketTonnes = tradeFFA.Info.MarketTonnes,
                        Trade = tradeFFA,
                        TradeInfo = tradeFFA.Info
                    });
                }

                foreach (Trade tradeCargo in trades.Where(a => a.Type == TradeTypeEnum.Cargo))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeCargo.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeCargo.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    foreach (TradeCargoInfoLeg tradeCargoLeg in tradeCargo.Info.CargoInfo.Legs)
                    {
                        tradeInformations.Add(new TradeInformation
                        {
                            Identifier = tradeCargoLeg.Id + "|CARGO",
                            Brokers = tradeCargo.Info.BrokersName,
                            Company = tradeCargo.Info.CompanyName,
                            Ownership = Convert.ToDecimal(tradeCargo.Info.Ownership),
                            Counterparty = tradeCargo.Info.CounterpartyName,
                            Direction = tradeCargo.Info.Direction.ToString("g"),
                            ExternalCode = tradeCargo.Info.ExternalCode,
                            LegNumber = tradeCargoLeg.Identifier,
                            Status = tradeCargo.Status.ToString("g"),
                            Type = tradeCargo.Type.ToString("g"),
                            PhysicalFinancial = "Physical",
                            Price = tradeCargoLeg.Tce.HasValue ? tradeCargoLeg.Tce.Value : 0,
                            SignDate = tradeCargo.Info.SignDate,
                            PeriodFrom = tradeCargoLeg.PeriodFrom,
                            PeriodTo = tradeCargoLeg.PeriodTo,
                            Vessel = tradeCargo.Info.CargoInfo.VesselName,
                            ForwardCurve = tradeCargo.Info.MTMFwdIndexName,
                            ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeCargo.Info.MTMFwdIndexName,
                            FCoverAssocIndexName = coverAssocIndexName,
                            FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                            StrikePrice = 0,
                            MarketTonnes = tradeCargo.Info.MarketTonnes,
                            Trade = tradeCargo,
                            TradeInfo = tradeCargo.Info
                        });
                    }
                }

                foreach (Trade tradeOption in trades.Where(a => a.Type == TradeTypeEnum.Option))
                {
                    var tradeIndex = indexes.SingleOrDefault(a => a.Name == tradeOption.Info.MTMFwdIndexName);
                    string reportsMarketDefautIndexName = null;
                    string coverAssocIndexName = null;
                    if (tradeIndex != null)
                    {
                        if (tradeIndex.CoverAssocIndexId != null)
                        {
                            var coverAssocIndex = indexes.SingleOrDefault(a => a.Id == tradeIndex.CoverAssocIndexId.Value);
                            if (coverAssocIndex != null)
                            {
                                coverAssocIndexName = coverAssocIndex.Name;
                            }
                        }
                        if (tradeIndex.IsReportsMarketDefault)
                            reportsMarketDefautIndexName = tradeIndex.Name;
                        else
                        {
                            var reportsMarketDefault = indexes.SingleOrDefault(a => a.IsReportsMarketDefault && a.MarketId == tradeOption.Info.MarketId);
                            if (reportsMarketDefault != null)
                            {
                                reportsMarketDefautIndexName = reportsMarketDefault.Name;
                            }
                        }
                    }
                    tradeInformations.Add(new TradeInformation
                    {
                        Identifier = tradeOption.Info.OptionInfo.Id + "|OPTION",
                        Brokers = tradeOption.Info.BrokersName,
                        Company = tradeOption.Info.CompanyName,
                        Ownership = Convert.ToDecimal(tradeOption.Info.Ownership),
                        Counterparty = tradeOption.Info.CounterpartyName,
                        Direction = tradeOption.Info.Direction.ToString("g"),
                        ExternalCode = tradeOption.Info.ExternalCode,
                        LegNumber = 0,
                        Status = tradeOption.Status.ToString("g"),
                        Type = tradeOption.Info.OptionInfo.Type.ToString("g"),
                        PhysicalFinancial =
                            tradeOption.Type == TradeTypeEnum.TC ||
                            tradeOption.Type == TradeTypeEnum.Cargo
                                ? "Physical"
                                : "Financial",
                        Price = tradeOption.Info.OptionInfo.Premium,
                        SignDate = tradeOption.Info.SignDate,
                        PeriodFrom = tradeOption.Info.PeriodFrom,
                        PeriodTo = tradeOption.Info.PeriodTo,
                        Vessel = tradeOption.Info.OptionInfo.VesselName,
                        ForwardCurve = tradeOption.Info.MTMFwdIndexName,
                        ForwardCurveForReports = reportsMarketDefautIndexName ?? tradeOption.Info.MTMFwdIndexName,
                        FCoverAssocIndexName = coverAssocIndexName,
                        FCCoverAssocOffset = tradeIndex == null ? null : tradeIndex.CoverAssocOffset,
                        StrikePrice = tradeOption.Info.OptionInfo.Strike,// * Convert.ToDecimal(tradeOption.Info.Ownership),
                        MarketTonnes = tradeOption.Info.MarketTonnes,
                        Trade = tradeOption,
                        TradeInfo = tradeOption.Info
                    });
                }


                //gridTradeDetails.DataSource = tradeInformations;
                var datasource = new List<TradeInformation>();


                var navigationGridfilter = "";
                GetCheckedNodesOperation op = new GetCheckedNodesOperation();
                treeBookNavigation.NodesIterator.DoOperation(op);

                var filters = new List<HierarchyNodeInfo>();

                foreach (TreeListNode checkNode in op.CheckedNodes)
                {
                    var entity = (HierarchyNodeInfo)checkNode.Tag;

                    _selectedHierarchyNode = entity;
                    if (!_booksLoadedTrades.ContainsKey(((Book)_selectedHierarchyNode.DomainObject).Id))
                    {
                        var bookTrades = new List<TradeInformation>();
                        if (checkNode.ParentNode == null)
                        {
                            bookTrades = tradeInformations.Where(t => t.TradeInfo.TradeInfoBooks.Select(tib => tib.BookId).ToList().Contains(((Book)_selectedHierarchyNode.DomainObject).Id)
                                && t.TradeInfo.TradeInfoBooks.ToList().Count == 1).ToList();
                        }
                        else
                        {
                            bookTrades = tradeInformations.Where(t => t.TradeInfo.TradeInfoBooks.Select(tib => tib.BookId).ToList().Contains(((Book)_selectedHierarchyNode.DomainObject).Id)).ToList();
                        }
                        _booksLoadedTrades.Add(((Book)_selectedHierarchyNode.DomainObject).Id, bookTrades);
                    }
                    else
                    {
                        if (_booksLoadedTrades.ContainsKey(((Book)_selectedHierarchyNode.DomainObject).Id))
                        {
                            foreach (TradeInformation ti in _booksLoadedTrades[((Book)_selectedHierarchyNode.DomainObject).Id])
                            {
                                if (!datasource.Contains(ti))
                                {
                                    datasource.Add(ti);
                                }
                            }
                        }

                    }
                    navigationGridfilter = filterBooksQuery(navigationGridfilter, checkNode);
                }

                foreach (TradeInformation ti in tradeInformations)
                {
                    if (!datasource.Contains(ti))
                    {
                        datasource.Add(ti);
                    }
                }

                gridTradeDetails.DataSource = datasource;

                gridTradeDetailsView.ActiveFilterString = navigationGridfilter; //set gridview filter string
                navigationGridfilter = ""; //reset gridview filter string
                gridTradeDetailsView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never; //Hide gridview filterpanel



                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }

                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
            }
        }

        private void EndGetUserAssociatedBooks(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetUserAssociatedBooks;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Book> entities;

            try
            {
                result = SessionRegistry.Client.EndGetUserAssociatedBooks(out entities, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                isLoad = false;
                treeBookNavigation.BeginUnboundLoad();

                Books = entities;
                var books = Books;

                treeBookNavigation.FocusedNodeChanged -= treeBookNavigation_FocusedNodeChanged;
                PopulateTree(null, books.Where(a => a.ParentBookId == null).ToList(), ref books);
                treeBookNavigation.FocusedNodeChanged += treeBookNavigation_FocusedNodeChanged;




                treeBookNavigation.EndUnboundLoad();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }

                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
            }
        }

        private void BeginGeInitializationData()
        {
            Cursor = Cursors.WaitCursor;
            //_dialogForm0 = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetViewTradesInitializationData(EndTradesInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                //lock (_syncObject)
                //{
                //    _dialogForm0.Close();
                //    _dialogForm0 = null;
                //}
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndTradesInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndTradesInitializationData;
                Invoke(action, ar);
                return;
            }

            int? result;
            List<Book> userAssocBooks;
            List<CashFlowModel> models;
            try
            {
                result = SessionRegistry.Client.EndGetViewTradesInitializationData(out Books, out userAssocBooks, out models, out indexes, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                //lock (_syncObject)
                //{
                //    _dialogForm0.Close();
                //    _dialogForm0 = null;
                //}
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                //lock (_syncObject)
                //{
                //    _dialogForm0.Close();
                //    _dialogForm0 = null;
                //}
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                isLoad = false;
                treeBookNavigation.BeginUnboundLoad();

                List<Book> books = userAssocBooks;

                treeBookNavigation.FocusedNodeChanged -= treeBookNavigation_FocusedNodeChanged;
                PopulateTree(null, books.Where(a => a.ParentBookId == null).ToList(), ref books);
                treeBookNavigation.FocusedNodeChanged += treeBookNavigation_FocusedNodeChanged;


                treeBookNavigation.EndUnboundLoad();

                treeBookNavigation.FocusedNode = null;


                Cursor = Cursors.Default;

                //lock (_syncObject)
                //{
                //    _dialogForm0.Close();
                //    _dialogForm0 = null;
                //}


                _formState = FormStateEnum.AfterSearch;
                SetGuiControls();
            }
        }

        private void PopulateTree(TreeListNode parentNode, List<Book> childrenBooks,
                                  ref List<Book> allBooks)
        {
            foreach (Book book in childrenBooks)
            {
                TreeListNode childNode =
                    treeBookNavigation.AppendNode(
                        new object[] { book.Name }, parentNode, new HierarchyNodeInfo { DomainObject = book }
                        );
                childNode.StateImageIndex = 5;
                PopulateTree(childNode, allBooks.Where(a => a.ParentBookId == book.Id).ToList(), ref allBooks);
            }
        }

        private void GenerateMonteCarloSimulation(MCSimulationCriteria criteria)
        {
            try
            {
                SessionRegistry.Client.GenerateMonteCarloSimulation(criteria.Name, dtpFilterPositionDateFrom.DateTime.Date,
                                                                         criteria.CurveDate,
                                                                         dtpFilterPeriodFrom.DateTime.Date,
                                                                         dtpFilterPeriodTo.DateTime.Date,
                                                                         criteria.PositionMethod,
                                                                         criteria.MarketSensitivityType,
                                                                         criteria.MarketSensitivityValue,
                                                                         criteria.UseSpotValue,
                                                                         criteria.IsOptionPremium,
                                                                         criteria.MTMType,
                                                                         ((List<TradeInformation>)gridTradeDetails.DataSource).
                                                                             Select(a => a.Identifier).ToList(), criteria.SimulationRuns
                                                                         );
                //Message 

                XtraMessageBox.Show(this,
                                    "Monte Carlo Simulation will run on background. You may view the results on Monte Carlo Simulations Archive Option in main menu.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void BeginUpdateTradeStatus(TradeTypeEnum tradeTypeEnum, long identifierId, ActivationStatusEnum statusEnum)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm2 = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginUpdateTradeStatus(tradeTypeEnum, identifierId, statusEnum, EndUpdateTradeStatus, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndUpdateTradeStatus(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndUpdateTradeStatus;
                Invoke(action, ar);
                return;
            }

            int? result;
            try
            {
                result = SessionRegistry.Client.EndUpdateTradeStatus(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                if (OnTradeDeactivated != null)
                    OnTradeDeactivated();
                RefreshData();
            }
        }

        private void BeginCheckUserHasPermissionOnTrader(long traderId)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm2 = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginCheckUserHasPermissionOnTrader(SessionRegistry.AppUser.Id, traderId, EndCheckUserHasPermissionOnTrader, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndCheckUserHasPermissionOnTrader(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndCheckUserHasPermissionOnTrader;
                Invoke(action, ar);
                return;
            }

            int? result;
            bool hasPermission = false;
            try
            {
                result = SessionRegistry.Client.EndCheckUserHasPermissionOnTrader(out hasPermission, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm2.Close();
                    _dialogForm2 = null;
                }

                if (!hasPermission)
                {
                    XtraMessageBox.Show(this,
                                        "You do not have the permission to edit the selected trade. The reason is that currently logged user does not have permission on selected trade's trader.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                var tradeInformation = (TradeInformation)gridTradeDetailsView.GetFocusedRow();
                if (tradeInformation.TradeInfo.DateTo != null)
                {
                    DialogResult result3 = XtraMessageBox.Show(this,
                                                              Strings.This_is_not_the_latest_version_of_the_trade__Would_you_like_to_edit_the_latest_one_,
                                                              Strings.Freight_Metrics,
                                                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (result3 == DialogResult.Yes)
                    {
                        if (OnEditTrade != null)
                            OnEditTrade(tradeInformation.Trade.Id, tradeInformation.TradeInfo.Id, tradeInformation.ExternalCode,
                                        tradeInformation.Trade.Type);
                    }
                }
                else if (OnEditTrade != null)
                    OnEditTrade(tradeInformation.Trade.Id, tradeInformation.TradeInfo.Id, tradeInformation.ExternalCode,
                                tradeInformation.Trade.Type);
            }
        }

        private void BeginApplicationParametersInitializationData()
        {
            Cursor = Cursors.WaitCursor;

            try
            {
                SessionRegistry.Client.BeginApplicationParametersInitializationData(EndApplicationParametersInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;

                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndApplicationParametersInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndApplicationParametersInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<AppParameter> appParameters;
            try
            {
                result = SessionRegistry.Client.EndApplicationParametersInitializationData(out appParameters, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;

                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                _appParameter = appParameters.FirstOrDefault(n => n.Code.Equals("LAST_IMPORT_INDEXES_VALUES_DATE"));
                Cursor = Cursors.Default;

                if (_appParameter.Value.Length == 8)
                    _lastImportDate = DateTime.ParseExact(_appParameter.Value, "d/M/yyyy", CultureInfo.InvariantCulture);
                else if (_appParameter.Value.Length == 10)
                    _lastImportDate = DateTime.ParseExact(_appParameter.Value, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                else if (_appParameter.Value.IndexOf('/') == 2)
                    _lastImportDate = DateTime.ParseExact(_appParameter.Value, "dd/M/yyyy", CultureInfo.InvariantCulture);
                else
                    _lastImportDate = DateTime.ParseExact(_appParameter.Value, "d/MM/yyyy", CultureInfo.InvariantCulture);


            }
        }

        #endregion

        #region Events

        public event Action<MemoryStream, string> ViewChartEvent;
        public event Action<long, long, string, TradeTypeEnum> OnViewTrade;
        public event Action<long, long, string, TradeTypeEnum> OnEditTrade;
        public event Action<Dictionary<DateTime, List<double>>, List<double>, List<Dictionary<DateTime, decimal>>> MonteCarloSimulationEvent;
        public event Action<object, bool> OnDataSaved;
        public event Action OnOpenActivateTradesForm;
        public event Action OnTradeDeactivated;
        public event Action<ViewTradeGeneralInfo> OnGenerateMTM;
        public event Action<ViewTradeGeneralInfo> OnGeneratePOS;
        public event Action<ViewTradeGeneralInfo> RatiosReportEvent;
        public event Action<ViewTradeGeneralInfo> OnGenerateCF;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            if (treeNavigation.GetAllCheckedNodes().Count > 0 && tbEntities.SelectedTabPageIndex == 0)
            {
                _companiesLoadedTrades = new Dictionary<long, List<TradeInformation>>();
                GetCheckedNodesOperation op = new GetCheckedNodesOperation();
                treeNavigation.NodesIterator.DoOperation(op);

                var filters = new List<HierarchyNodeInfo>();

                string gridfilter = "";

                gridTradeDetailsView.ActiveFilterString = "";

                foreach (TreeListNode checkNode in op.CheckedNodes)
                {
                    var entity = (HierarchyNodeInfo)checkNode.Tag;

                    _selectedHierarchyNode = entity;
                    if (checkNode.Nodes.Count > 0 && _selectedHierarchyNode.DomainObject != null && _selectedHierarchyNode.DomainObject.GetType() == typeof(Company) &&
                        _selectedHierarchyNode.HierarchyNodeType.Code == "CMP")
                    {
                        filters.Add(entity);

                    }
                }

                if (filters.Count > 0)
                {
                    _formState = FormStateEnum.BeforeSearchTree;
                    SetGuiControls();
                    BeginGetTradesByFilters(filters);
                }
            }
            else
            {

                if (treeBookNavigation.GetAllCheckedNodes().Count == 0 || tbEntities.SelectedTabPageIndex == 0)
                {
                    return;
                }

                _booksLoadedTrades = new Dictionary<long, List<TradeInformation>>();

                GetCheckedNodesOperation op = new GetCheckedNodesOperation();
                treeBookNavigation.NodesIterator.DoOperation(op);

                var filters = new List<HierarchyNodeInfo>();

                string gridfilter = "";

                gridTradeDetailsView.ActiveFilterString = "";

                foreach (TreeListNode checkNode in op.CheckedNodes)
                {
                    var entity = (HierarchyNodeInfo)checkNode.Tag;
                    var checkParentNode = checkNode.ParentNode;
                    _selectedHierarchyNode = entity;
                    filters.Add(entity);
                }

                if (filters.Count > 0)
                {
                    _formState = FormStateEnum.BeforeSearchTree;
                    SetGuiControls();
                    BeginGetTradesBySelectionBooks(filters);
                }
            }
        }
        #region treelistoperations 
        //The operation class that collects checked nodes
        public class GetCheckedNodesOperation : TreeListOperation
        {
            public List<TreeListNode> CheckedNodes = new List<TreeListNode>();
            public GetCheckedNodesOperation() : base() { }
            public override void Execute(TreeListNode node)
            {
                if (node.CheckState != CheckState.Unchecked)
                    CheckedNodes.Add(node);
            }
        }

        class GetChildNodeOperation : TreeListOperation
        {
            int level;
            int nodeCount;
            public GetChildNodeOperation(int level)
                : base()
            {
                this.level = level;
                this.nodeCount = 0;
            }
            public override void Execute(TreeListNode node)
            {
                if (node.Level == level)
                    nodeCount++;
            }
            public int NodeCount
            {
                get { return nodeCount; }
            }
        }

        #endregion

        #endregion

        //private void treeNavigation_CustomDrawNodeCheckBox(object sender, CustomDrawNodeCheckBoxEventArgs e)
        //{
        //    var chk_node = (HierarchyNodeInfo)e.Node.Tag;

        //    /* if ((chk_node.HierarchyNodeType.Code == "CMP") || (chk_node.HierarchyNodeType.Code == "MRK"))
        //     {
        //         //return;
        //     }
        //     else e.Handled = true;*/
        //}


        private void btnCpmRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void tbEntities_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            if (e.Page == null)
                return;

            gridTradeDetails.BeginInit();
            gridTradeDetails.DataSource = null;
            gridTradeDetails.EndInit();

            treeNavigation.UncheckAll();
            treeNavigation.CollapseAll();
            treeBookNavigation.UncheckAll();
            treeBookNavigation.CollapseAll();

            treeNavigation.FocusedNode = null;
            treeBookNavigation.FocusedNode = null;
        }

       
    }
    public class TradeInformation
    {
        public string Identifier { get; set; }
        public string Type { get; set; }
        public string ExternalCode { get; set; }
        public int LegNumber { get; set; }
        public string Company { get; set; }
        public decimal Ownership { get; set; } //VK: changed type from string to decimal
        public decimal SubType { get; set; }
        public string Counterparty { get; set; }
        public string Status { get; set; }
        public DateTime SignDate { get; set; }
        public string Direction { get; set; }
        public decimal Price { get; set; }
        public decimal StrikePrice { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public string Vessel { get; set; }
        public string Brokers { get; set; }
        public string ForwardCurve { get; set; }
        public string ForwardCurveForReports { get; set; }
        public string FCoverAssocIndexName { get; set; }
        public decimal? FCCoverAssocOffset { get; set; }
        public string PhysicalFinancial { get; set; }
        public int MarketTonnes { get; set; }
        public Trade Trade { get; set; }
        public TradeInfo TradeInfo { get; set; }
        public long MarketID { get; set; } //VK: added to support fitlering, not displayed
        public long? VesselId { get; set; } //VK: added to support fitlering, not displayed
        public string GroupedForwardCurve { get; set; }
    }

    public class ViewTradeGeneralInfo
    {
        //filters Data
        public DateTime positionDate { get; set; }
        public DateTime periodFrom { get; set; }
        public DateTime periodTo { get; set; }
        public Boolean IsDraft { get; set; }
        public Boolean IsMinimumPeriod { get; set; }
        public Boolean IsOptionalPeriod { get; set; }
        public Boolean OnlyNonZeroTotalDays { get; set; }

        //Selected Book/Entity
        public List<Book> books { get; set; }
        public HierarchyNodeInfo selectedHierarchyNode { get; set; }

        //Trades
        public List<string> tradeIdentifiers { get; set; }
        public List<TradeInformation> tradeInformations { get; set; }

        //AppParameters Data
        public DateTime lastImportDate { get; set; }


    }

    public class CashFlowInformation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public CashFlowInformationTypeEnum Type { get; set; }
        internal List<CashFlowItem> GroupItems { get; set; }
        internal List<Dictionary<DateTime, decimal>> Results { get; set; }
        internal List<CashFlowInformation> ChildrenGroupInfo { get; set; }
        internal CashFlowInformation GroupHeaderInfo { get; set; }
        internal CashFlowModelGroupOperatorEnum GroupOperator { get; set; }
    }

    public enum CashFlowInformationTypeEnum
    {
        Item,
        GroupHeader,
        GroupFooter,
        Blank
    }

    public class NewtonRaphsonIRRCalculator
    {
        private const int _maxNumberOfIterations = 50000;
        private const double _tolerance = 0.00000001;
        private readonly double[] _cashFlows;
        private int _numberOfIterations;

        private double _result;

        public NewtonRaphsonIRRCalculator(double[] cashFlows)
        {
            _cashFlows = cashFlows;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid cash flows.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is valid cash flows; otherwise, <c>false</c>.
        /// </value>
        private bool IsValidCashFlows
        {
            //Cash flows for the first period must be positive
            //There should be at least two cash flow periods         
            get
            {
                const int MIN_NO_CASH_FLOW_PERIODS = 2;

                if (_cashFlows.Length < MIN_NO_CASH_FLOW_PERIODS || (_cashFlows[0] > 0))
                {
                    throw new ArgumentOutOfRangeException(
                        "Cash flow for the first period  must be negative and there should");
                }
                return true;
            }
        }

        /// <summary>
        /// Gets the initial guess.
        /// </summary>
        /// <value>The initial guess.</value>
        private double InitialGuess
        {
            get
            {
                double initialGuess = -1 * (1 + (_cashFlows[1] / _cashFlows[0]));
                return initialGuess;
            }
        }

        #region ICalculator Members

        public double Execute()
        {
            if (IsValidCashFlows)
            {
                DoNewtonRapshonCalculation(InitialGuess);

                if (_result > 1)
                    throw new ApplicationException(
                        "Failed to calculate the IRR for the cash flow series. Please provide a valid cash flow sequence");
            }
            return _result;
        }

        #endregion

        /// <summary>
        /// Does the newton rapshon calculation.
        /// </summary>
        /// <param name="estimatedReturn">The estimated return.</param>
        /// <returns></returns>
        private void DoNewtonRapshonCalculation(double estimatedReturn)
        {
            _numberOfIterations++;
            _result = estimatedReturn - SumOfIRRPolynomial(estimatedReturn) / IRRDerivativeSum(estimatedReturn);
            while (!HasConverged(_result) && _maxNumberOfIterations != _numberOfIterations)
            {
                DoNewtonRapshonCalculation(_result);
            }
        }


        /// <summary>
        /// Sums the of IRR polynomial.
        /// </summary>
        /// <param name="estimatedReturnRate">The estimated return rate.</param>
        /// <returns></returns>
        private double SumOfIRRPolynomial(double estimatedReturnRate)
        {
            double sumOfPolynomial = 0;
            if (IsValidIterationBounds(estimatedReturnRate))
                for (int j = 0; j < _cashFlows.Length; j++)
                {
                    sumOfPolynomial += _cashFlows[j] / (Math.Pow((1 + estimatedReturnRate), j));
                }
            return sumOfPolynomial;
        }

        /// <summary>
        /// Determines whether the specified estimated return rate has converged.
        /// </summary>
        /// <param name="estimatedReturnRate">The estimated return rate.</param>
        /// <returns>
        /// 	<c>true</c> if the specified estimated return rate has converged; otherwise, <c>false</c>.
        /// </returns>
        private bool HasConverged(double estimatedReturnRate)
        {
            //Check that the calculated value makes the IRR polynomial zero.
            bool isWithinTolerance = Math.Abs(SumOfIRRPolynomial(estimatedReturnRate)) <= _tolerance;
            return (isWithinTolerance) ? true : false;
        }

        /// <summary>
        /// IRRs the derivative sum.
        /// </summary>
        /// <param name="estimatedReturnRate">The estimated return rate.</param>
        /// <returns></returns>
        private double IRRDerivativeSum(double estimatedReturnRate)
        {
            double sumOfDerivative = 0;
            if (IsValidIterationBounds(estimatedReturnRate))
                for (int i = 1; i < _cashFlows.Length; i++)
                {
                    sumOfDerivative += _cashFlows[i] * (i) / Math.Pow((1 + estimatedReturnRate), i);
                }
            return sumOfDerivative * -1;
        }

        /// <summary>
        /// Determines whether [is valid iteration bounds] [the specified estimated return rate].
        /// </summary>
        /// <param name="estimatedReturnRate">The estimated return rate.</param>
        /// <returns>
        /// 	<c>true</c> if [is valid iteration bounds] [the specified estimated return rate]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsValidIterationBounds(double estimatedReturnRate)
        {
            return estimatedReturnRate != -1 && (estimatedReturnRate < int.MaxValue) &&
                   (estimatedReturnRate > int.MinValue);
        }
    }
    //The operation class that collects checked nodes
    public class GetCheckedNodesOperation : TreeListOperation
    {
        public List<TreeListNode> CheckedNodes = new List<TreeListNode>();
        public GetCheckedNodesOperation() : base() { }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState != CheckState.Unchecked)
                CheckedNodes.Add(node);
        }
    }


}