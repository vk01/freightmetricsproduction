﻿namespace Exis.WinClient.Controls.Core
{
    partial class InitialMarginNasdaqControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lcInitialMargin = new DevExpress.XtraLayout.LayoutControl();
            this.btnExportInitialMargin = new DevExpress.XtraEditors.SimpleButton();
            this.gridInitialMargin = new DevExpress.XtraGrid.GridControl();
            this.gridViewInitialMargin = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnCalculateInitialMargin = new DevExpress.XtraEditors.SimpleButton();
            this.beSelectExcel = new DevExpress.XtraEditors.ButtonEdit();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciSelectExcel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcigridInitialMargin = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.lciBtnExportInitialMargin = new DevExpress.XtraLayout.LayoutControlItem();
            this.bciBtnInitialMargin = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).BeginInit();
            this.lcInitialMargin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beSelectExcel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSelectExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcigridInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBtnExportInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bciBtnInitialMargin)).BeginInit();
            this.SuspendLayout();
            // 
            // lcInitialMargin
            // 
            this.lcInitialMargin.Controls.Add(this.btnExportInitialMargin);
            this.lcInitialMargin.Controls.Add(this.gridInitialMargin);
            this.lcInitialMargin.Controls.Add(this.btnCalculateInitialMargin);
            this.lcInitialMargin.Controls.Add(this.beSelectExcel);
            this.lcInitialMargin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcInitialMargin.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.lcInitialMargin.Location = new System.Drawing.Point(0, 0);
            this.lcInitialMargin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lcInitialMargin.Name = "lcInitialMargin";
            this.lcInitialMargin.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(852, 234, 386, 350);
            this.lcInitialMargin.Root = this.layoutControlGroup4;
            this.lcInitialMargin.Size = new System.Drawing.Size(1050, 738);
            this.lcInitialMargin.TabIndex = 1;
            this.lcInitialMargin.Text = "layoutControl2";
            // 
            // btnExportInitialMargin
            // 
            this.btnExportInitialMargin.ImageIndex = 8;
            this.btnExportInitialMargin.Location = new System.Drawing.Point(948, 19);
            this.btnExportInitialMargin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExportInitialMargin.Name = "btnExportInitialMargin";
            this.btnExportInitialMargin.Size = new System.Drawing.Size(88, 23);
            this.btnExportInitialMargin.StyleController = this.lcInitialMargin;
            this.btnExportInitialMargin.TabIndex = 10;
            this.btnExportInitialMargin.Text = "Export";
            // 
            // gridInitialMargin
            // 
            this.gridInitialMargin.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridInitialMargin.Location = new System.Drawing.Point(14, 46);
            this.gridInitialMargin.MainView = this.gridViewInitialMargin;
            this.gridInitialMargin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridInitialMargin.Name = "gridInitialMargin";
            this.gridInitialMargin.Size = new System.Drawing.Size(1022, 678);
            this.gridInitialMargin.TabIndex = 9;
            this.gridInitialMargin.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInitialMargin});
            // 
            // gridViewInitialMargin
            // 
            this.gridViewInitialMargin.GridControl = this.gridInitialMargin;
            this.gridViewInitialMargin.Name = "gridViewInitialMargin";
            this.gridViewInitialMargin.OptionsBehavior.Editable = false;
            this.gridViewInitialMargin.OptionsView.ShowFooter = true;
            this.gridViewInitialMargin.OptionsView.ShowGroupPanel = false;
            // 
            // btnCalculateInitialMargin
            // 
            this.btnCalculateInitialMargin.ImageIndex = 7;
            this.btnCalculateInitialMargin.Location = new System.Drawing.Point(803, 19);
            this.btnCalculateInitialMargin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCalculateInitialMargin.Name = "btnCalculateInitialMargin";
            this.btnCalculateInitialMargin.Size = new System.Drawing.Size(141, 23);
            this.btnCalculateInitialMargin.StyleController = this.lcInitialMargin;
            this.btnCalculateInitialMargin.TabIndex = 7;
            this.btnCalculateInitialMargin.Text = "Calculate Initial Margin";
            this.btnCalculateInitialMargin.Click += new System.EventHandler(this.btnCalculateInitialMargin_Click);
            // 
            // beSelectExcel
            // 
            this.beSelectExcel.Location = new System.Drawing.Point(162, 19);
            this.beSelectExcel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.beSelectExcel.Name = "beSelectExcel";
            this.beSelectExcel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.beSelectExcel.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.beSelectExcel_ButtonClick);
            this.beSelectExcel.Size = new System.Drawing.Size(637, 22);
            this.beSelectExcel.StyleController = this.lcInitialMargin;
            this.beSelectExcel.TabIndex = 6;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = null;
            this.tabbedControlGroup1.SelectedTabPageIndex = -1;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1050, 738);
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1050, 738);
            this.layoutControlGroup4.Text = "Root";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Baltic";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciSelectExcel,
            this.lcigridInitialMargin,
            this.splitterItem1,
            this.lciBtnExportInitialMargin,
            this.bciBtnInitialMargin});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1050, 738);
            this.layoutControlGroup9.Text = "Baltic";
            this.layoutControlGroup9.TextVisible = false;
            // 
            // lciSelectExcel
            // 
            this.lciSelectExcel.Control = this.beSelectExcel;
            this.lciSelectExcel.CustomizationFormText = "Select Excel File:";
            this.lciSelectExcel.Location = new System.Drawing.Point(0, 5);
            this.lciSelectExcel.Name = "lciSelectExcel";
            this.lciSelectExcel.Size = new System.Drawing.Size(789, 27);
            this.lciSelectExcel.Text = "Volatility/Correlation File:";
            this.lciSelectExcel.TextSize = new System.Drawing.Size(145, 16);
            // 
            // lcigridInitialMargin
            // 
            this.lcigridInitialMargin.Control = this.gridInitialMargin;
            this.lcigridInitialMargin.CustomizationFormText = "lcigridInitialMargin";
            this.lcigridInitialMargin.Location = new System.Drawing.Point(0, 32);
            this.lcigridInitialMargin.Name = "lcigridInitialMargin";
            this.lcigridInitialMargin.Size = new System.Drawing.Size(1026, 682);
            this.lcigridInitialMargin.Text = "lcigridInitialMargin";
            this.lcigridInitialMargin.TextSize = new System.Drawing.Size(0, 0);
            this.lcigridInitialMargin.TextToControlDistance = 0;
            this.lcigridInitialMargin.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1026, 5);
            // 
            // lciBtnExportInitialMargin
            // 
            this.lciBtnExportInitialMargin.Control = this.btnExportInitialMargin;
            this.lciBtnExportInitialMargin.CustomizationFormText = "lciBtnExportInitialMargin";
            this.lciBtnExportInitialMargin.Location = new System.Drawing.Point(934, 5);
            this.lciBtnExportInitialMargin.Name = "lciBtnExportInitialMargin";
            this.lciBtnExportInitialMargin.Size = new System.Drawing.Size(92, 27);
            this.lciBtnExportInitialMargin.Text = "lciBtnExportInitialMargin";
            this.lciBtnExportInitialMargin.TextSize = new System.Drawing.Size(0, 0);
            this.lciBtnExportInitialMargin.TextToControlDistance = 0;
            this.lciBtnExportInitialMargin.TextVisible = false;
            // 
            // bciBtnInitialMargin
            // 
            this.bciBtnInitialMargin.Control = this.btnCalculateInitialMargin;
            this.bciBtnInitialMargin.CustomizationFormText = "bciBtnInitialMargin";
            this.bciBtnInitialMargin.Location = new System.Drawing.Point(789, 5);
            this.bciBtnInitialMargin.Name = "bciBtnInitialMargin";
            this.bciBtnInitialMargin.Size = new System.Drawing.Size(145, 27);
            this.bciBtnInitialMargin.Text = "bciBtnInitialMargin";
            this.bciBtnInitialMargin.TextSize = new System.Drawing.Size(0, 0);
            this.bciBtnInitialMargin.TextToControlDistance = 0;
            this.bciBtnInitialMargin.TextVisible = false;
            // 
            // InitialMarginNasdaqControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcInitialMargin);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "InitialMarginNasdaqControl";
            this.Size = new System.Drawing.Size(1050, 738);
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).EndInit();
            this.lcInitialMargin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beSelectExcel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSelectExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcigridInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBtnExportInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bciBtnInitialMargin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcInitialMargin;
        private DevExpress.XtraEditors.SimpleButton btnExportInitialMargin;
        private DevExpress.XtraGrid.GridControl gridInitialMargin;
        private DevExpress.XtraGrid.GridControl gridVariationMargin;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInitialMargin;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVariationMargin;
        private DevExpress.XtraEditors.SimpleButton btnCalculateInitialMargin;
        private DevExpress.XtraEditors.ButtonEdit beSelectExcel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem lciSelectExcel;
        private DevExpress.XtraLayout.LayoutControlItem lcigridInitialMargin;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlItem lciBtnExportInitialMargin;
        private DevExpress.XtraLayout.LayoutControlItem bciBtnInitialMargin;
    }
}
