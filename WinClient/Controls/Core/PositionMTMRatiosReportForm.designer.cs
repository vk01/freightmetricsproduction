﻿namespace Exis.WinClient.Controls.Core
{
    partial class PositionMTMRatiosReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PositionMTMRatiosReportForm));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.rdgResultsAggregationType = new DevExpress.XtraEditors.RadioGroup();
            this.chkCmbBxEdtTradeType = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnExport = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.chkCalculateCommissions = new DevExpress.XtraEditors.CheckEdit();
            this.chkCalculateSums = new DevExpress.XtraEditors.CheckEdit();
            this.cmbMarketSensitivityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnCalculate = new DevExpress.XtraEditors.SimpleButton();
            this.chkUseSpotValues = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionEmbeddedValue = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionNullify = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionPremium = new DevExpress.XtraEditors.CheckEdit();
            this.rdgMTMResultsType = new DevExpress.XtraEditors.RadioGroup();
            this.txtMarketSensitivityValue = new DevExpress.XtraEditors.SpinEdit();
            this.cmbPositionMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpFilterCurveDate = new DevExpress.XtraEditors.DateEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCalculateCommissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCalculateSums = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTradeType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tbControl = new DevExpress.XtraTab.XtraTabControl();
            this.tbpgMTMResults = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.tbMTMIndexSummaries = new DevExpress.XtraTab.XtraTabControl();
            this.gridTradeMTMResults = new DevExpress.XtraGrid.GridControl();
            this.gridMTMResultsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.itemTbMTMIndexSummaries = new DevExpress.XtraLayout.LayoutControlItem();
            this.tbpgPositionResults = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.tbPosIndexSummaries = new DevExpress.XtraTab.XtraTabControl();
            this.gridTradePositionResults = new DevExpress.XtraGrid.GridControl();
            this.gridPositionResultsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.itemTabPosIndexSummaries = new DevExpress.XtraLayout.LayoutControlItem();
            this.tbpgInitialMargin = new DevExpress.XtraTab.XtraTabPage();
            this.lcInitialMargin = new DevExpress.XtraLayout.LayoutControl();
            this.lcgInitialMargin = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTabControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageListFooter = new System.Windows.Forms.ImageList(this.components);
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutFilterCurveDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMTMResultsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionPremium = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionNullify = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionEmbeddedValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUseSpotValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbBxEdtTradeType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionEmbeddedValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionNullify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgMTMResultsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCalculateCommissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTradeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).BeginInit();
            this.tbControl.SuspendLayout();
            this.tbpgMTMResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMTMIndexSummaries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeMTMResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMTMResultsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemTbMTMIndexSummaries)).BeginInit();
            this.tbpgPositionResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPosIndexSummaries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradePositionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPositionResultsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemTabPosIndexSummaries)).BeginInit();
            this.tbpgInitialMargin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInitialMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMTMResultsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionNullify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionEmbeddedValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.layoutControl3);
            this.layoutControl.Controls.Add(this.tbControl);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(869, 338, 250, 350);
            this.layoutControl.Root = this.lcgMain;
            this.layoutControl.Size = new System.Drawing.Size(1051, 547);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.rdgResultsAggregationType);
            this.layoutControl3.Controls.Add(this.chkCmbBxEdtTradeType);
            this.layoutControl3.Controls.Add(this.chkCalculateCommissions);
            this.layoutControl3.Controls.Add(this.chkCalculateSums);
            this.layoutControl3.Controls.Add(this.cmbMarketSensitivityType);
            this.layoutControl3.Controls.Add(this.btnCalculate);
            this.layoutControl3.Controls.Add(this.chkUseSpotValues);
            this.layoutControl3.Controls.Add(this.chkOptionEmbeddedValue);
            this.layoutControl3.Controls.Add(this.chkOptionNullify);
            this.layoutControl3.Controls.Add(this.chkOptionPremium);
            this.layoutControl3.Controls.Add(this.rdgMTMResultsType);
            this.layoutControl3.Controls.Add(this.txtMarketSensitivityValue);
            this.layoutControl3.Controls.Add(this.cmbPositionMethod);
            this.layoutControl3.Controls.Add(this.dtpFilterCurveDate);
            this.layoutControl3.Location = new System.Drawing.Point(5, 24);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.Root;
            this.layoutControl3.Size = new System.Drawing.Size(1041, 97);
            this.layoutControl3.TabIndex = 19;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // rdgResultsAggregationType
            // 
            this.rdgResultsAggregationType.Location = new System.Drawing.Point(777, 28);
            this.rdgResultsAggregationType.Name = "rdgResultsAggregationType";
            this.rdgResultsAggregationType.Properties.Columns = 10;
            this.rdgResultsAggregationType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Month"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Quarter"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Calendar")});
            this.rdgResultsAggregationType.Size = new System.Drawing.Size(262, 67);
            this.rdgResultsAggregationType.StyleController = this.layoutControl3;
            this.rdgResultsAggregationType.TabIndex = 24;
            this.rdgResultsAggregationType.SelectedIndexChanged += new System.EventHandler(this.RdgResultsAggregationTypeSelectedIndexChanged);
            // 
            // chkCmbBxEdtTradeType
            // 
            this.chkCmbBxEdtTradeType.Location = new System.Drawing.Point(769, 2);
            this.chkCmbBxEdtTradeType.MenuManager = this.barManager1;
            this.chkCmbBxEdtTradeType.Name = "chkCmbBxEdtTradeType";
            this.chkCmbBxEdtTradeType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkCmbBxEdtTradeType.Size = new System.Drawing.Size(130, 20);
            this.chkCmbBxEdtTradeType.StyleController = this.layoutControl3;
            this.chkCmbBxEdtTradeType.TabIndex = 16;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExport,
            this.btnClose});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnExport
            // 
            this.btnExport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnExport.Caption = "Export";
            this.btnExport.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnExport.Id = 0;
            this.btnExport.LargeImageIndex = 0;
            this.btnExport.Name = "btnExport";
            this.btnExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExportResultsClick);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 1;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1051, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 547);
            this.barDockControlBottom.Size = new System.Drawing.Size(1051, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 547);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1051, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 547);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "export24x24.ico");
            this.imageList24.Images.SetKeyName(1, "exit.ico");
            // 
            // chkCalculateCommissions
            // 
            this.chkCalculateCommissions.Location = new System.Drawing.Point(686, 26);
            this.chkCalculateCommissions.MenuManager = this.barManager1;
            this.chkCalculateCommissions.Name = "chkCalculateCommissions";
            this.chkCalculateCommissions.Properties.Caption = "";
            this.chkCalculateCommissions.Size = new System.Drawing.Size(19, 19);
            this.chkCalculateCommissions.StyleController = this.layoutControl3;
            this.chkCalculateCommissions.TabIndex = 15;
            // 
            // chkCalculateSums
            // 
            this.chkCalculateSums.Location = new System.Drawing.Point(659, 49);
            this.chkCalculateSums.MenuManager = this.barManager1;
            this.chkCalculateSums.Name = "chkCalculateSums";
            this.chkCalculateSums.Properties.Caption = " ";
            this.chkCalculateSums.Size = new System.Drawing.Size(46, 19);
            this.chkCalculateSums.StyleController = this.layoutControl3;
            this.chkCalculateSums.TabIndex = 14;
            // 
            // cmbMarketSensitivityType
            // 
            this.cmbMarketSensitivityType.Location = new System.Drawing.Point(378, 2);
            this.cmbMarketSensitivityType.Name = "cmbMarketSensitivityType";
            this.cmbMarketSensitivityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbMarketSensitivityType.Properties.Items.AddRange(new object[] {
            "None",
            "Absolute",
            "Percentage",
            "Stress"});
            this.cmbMarketSensitivityType.Size = new System.Drawing.Size(50, 20);
            this.cmbMarketSensitivityType.StyleController = this.layoutControl3;
            this.cmbMarketSensitivityType.TabIndex = 13;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(903, 2);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(136, 22);
            this.btnCalculate.StyleController = this.layoutControl3;
            this.btnCalculate.TabIndex = 12;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculateClick);
            // 
            // chkUseSpotValues
            // 
            this.chkUseSpotValues.Location = new System.Drawing.Point(467, 49);
            this.chkUseSpotValues.Name = "chkUseSpotValues";
            this.chkUseSpotValues.Properties.Caption = "Use Spot";
            this.chkUseSpotValues.Size = new System.Drawing.Size(150, 19);
            this.chkUseSpotValues.StyleController = this.layoutControl3;
            this.chkUseSpotValues.TabIndex = 11;
            // 
            // chkOptionEmbeddedValue
            // 
            this.chkOptionEmbeddedValue.Location = new System.Drawing.Point(467, 26);
            this.chkOptionEmbeddedValue.Name = "chkOptionEmbeddedValue";
            this.chkOptionEmbeddedValue.Properties.Caption = "Option Embedded Value";
            this.chkOptionEmbeddedValue.Size = new System.Drawing.Size(150, 19);
            this.chkOptionEmbeddedValue.StyleController = this.layoutControl3;
            this.chkOptionEmbeddedValue.TabIndex = 10;
            // 
            // chkOptionNullify
            // 
            this.chkOptionNullify.Location = new System.Drawing.Point(365, 49);
            this.chkOptionNullify.Name = "chkOptionNullify";
            this.chkOptionNullify.Properties.Caption = "Option Nullify";
            this.chkOptionNullify.Size = new System.Drawing.Size(98, 19);
            this.chkOptionNullify.StyleController = this.layoutControl3;
            this.chkOptionNullify.TabIndex = 9;
            // 
            // chkOptionPremium
            // 
            this.chkOptionPremium.Location = new System.Drawing.Point(365, 26);
            this.chkOptionPremium.Name = "chkOptionPremium";
            this.chkOptionPremium.Properties.Caption = "Option Premium";
            this.chkOptionPremium.Size = new System.Drawing.Size(98, 19);
            this.chkOptionPremium.StyleController = this.layoutControl3;
            this.chkOptionPremium.TabIndex = 8;
            this.chkOptionPremium.CheckedChanged += new System.EventHandler(this.chkOptionPremium_CheckedChanged);
            // 
            // rdgMTMResultsType
            // 
            this.rdgMTMResultsType.Location = new System.Drawing.Point(93, 26);
            this.rdgMTMResultsType.Name = "rdgMTMResultsType";
            this.rdgMTMResultsType.Properties.Columns = 10;
            this.rdgMTMResultsType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Cash Flow", "Cash Flow"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("P&L", "P&&L"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("COMBINED", "Combined")});
            this.rdgMTMResultsType.Size = new System.Drawing.Size(268, 69);
            this.rdgMTMResultsType.StyleController = this.layoutControl3;
            this.rdgMTMResultsType.TabIndex = 7;
            // 
            // txtMarketSensitivityValue
            // 
            this.txtMarketSensitivityValue.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtMarketSensitivityValue.Location = new System.Drawing.Point(555, 2);
            this.txtMarketSensitivityValue.Name = "txtMarketSensitivityValue";
            this.txtMarketSensitivityValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMarketSensitivityValue.Size = new System.Drawing.Size(150, 20);
            this.txtMarketSensitivityValue.StyleController = this.layoutControl3;
            this.txtMarketSensitivityValue.TabIndex = 6;
            // 
            // cmbPositionMethod
            // 
            this.cmbPositionMethod.Location = new System.Drawing.Point(205, 2);
            this.cmbPositionMethod.Name = "cmbPositionMethod";
            this.cmbPositionMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPositionMethod.Properties.Items.AddRange(new object[] {
            "Static",
            "Dynamic",
            "Delta"});
            this.cmbPositionMethod.Size = new System.Drawing.Size(50, 20);
            this.cmbPositionMethod.StyleController = this.layoutControl3;
            this.cmbPositionMethod.TabIndex = 5;
            // 
            // dtpFilterCurveDate
            // 
            this.dtpFilterCurveDate.EditValue = null;
            this.dtpFilterCurveDate.Location = new System.Drawing.Point(66, 2);
            this.dtpFilterCurveDate.Name = "dtpFilterCurveDate";
            this.dtpFilterCurveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterCurveDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterCurveDate.Size = new System.Drawing.Size(50, 20);
            this.dtpFilterCurveDate.StyleController = this.layoutControl3;
            this.dtpFilterCurveDate.TabIndex = 4;
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem19,
            this.layoutControlItem25,
            this.layoutControlItem17,
            this.lciCalculateCommissions,
            this.layoutControlItem21,
            this.layoutControlItem23,
            this.layoutItemCalculateSums,
            this.layoutControlItem2,
            this.layoutControlItemTradeType,
            this.layoutControlItem24});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1041, 97);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.dtpFilterCurveDate;
            this.layoutControlItem16.CustomizationFormText = "Curve Date:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(118, 24);
            this.layoutControlItem16.Text = "Curve Date:";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(59, 13);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtMarketSensitivityValue;
            this.layoutControlItem18.CustomizationFormText = "Market Sensitivity Value:";
            this.layoutControlItem18.Location = new System.Drawing.Point(430, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem18.Text = "Market Sensitivity Value:";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(118, 13);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.chkOptionPremium;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(363, 24);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(102, 23);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(102, 23);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.chkOptionEmbeddedValue;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(465, 24);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(141, 23);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(154, 23);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.rdgMTMResultsType;
            this.layoutControlItem19.CustomizationFormText = "MTM Result Type:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(145, 29);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(363, 73);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "MTM Result Type:";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(86, 13);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.cmbMarketSensitivityType;
            this.layoutControlItem25.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutControlItem25.Location = new System.Drawing.Point(257, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(173, 24);
            this.layoutControlItem25.Text = "Market Sensitivity Type:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.cmbPositionMethod;
            this.layoutControlItem17.CustomizationFormText = "Position Method:";
            this.layoutControlItem17.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(139, 24);
            this.layoutControlItem17.Text = "Position Method:";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 13);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // lciCalculateCommissions
            // 
            this.lciCalculateCommissions.Control = this.chkCalculateCommissions;
            this.lciCalculateCommissions.CustomizationFormText = "Commissions";
            this.lciCalculateCommissions.Location = new System.Drawing.Point(619, 24);
            this.lciCalculateCommissions.MaxSize = new System.Drawing.Size(0, 23);
            this.lciCalculateCommissions.MinSize = new System.Drawing.Size(88, 23);
            this.lciCalculateCommissions.Name = "lciCalculateCommissions";
            this.lciCalculateCommissions.Size = new System.Drawing.Size(88, 23);
            this.lciCalculateCommissions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCalculateCommissions.Text = "Commissions";
            this.lciCalculateCommissions.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciCalculateCommissions.TextSize = new System.Drawing.Size(60, 13);
            this.lciCalculateCommissions.TextToControlDistance = 5;
            this.lciCalculateCommissions.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.chkOptionNullify;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(363, 47);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(91, 23);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(102, 50);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.chkUseSpotValues;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(465, 47);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(154, 50);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutItemCalculateSums
            // 
            this.layoutItemCalculateSums.Control = this.chkCalculateSums;
            this.layoutItemCalculateSums.CustomizationFormText = "Totals:";
            this.layoutItemCalculateSums.Location = new System.Drawing.Point(619, 47);
            this.layoutItemCalculateSums.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutItemCalculateSums.MinSize = new System.Drawing.Size(67, 23);
            this.layoutItemCalculateSums.Name = "layoutItemCalculateSums";
            this.layoutItemCalculateSums.Size = new System.Drawing.Size(88, 50);
            this.layoutItemCalculateSums.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemCalculateSums.Text = "Totals:";
            this.layoutItemCalculateSums.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateSums.TextSize = new System.Drawing.Size(33, 13);
            this.layoutItemCalculateSums.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.rdgResultsAggregationType;
            this.layoutControlItem2.CustomizationFormText = "Aggregation:";
            this.layoutControlItem2.Location = new System.Drawing.Point(707, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(122, 29);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(334, 71);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Aggregation:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(63, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItemTradeType
            // 
            this.layoutControlItemTradeType.Control = this.chkCmbBxEdtTradeType;
            this.layoutControlItemTradeType.CustomizationFormText = "Trade Type";
            this.layoutControlItemTradeType.Location = new System.Drawing.Point(707, 0);
            this.layoutControlItemTradeType.Name = "layoutControlItemTradeType";
            this.layoutControlItemTradeType.Size = new System.Drawing.Size(194, 26);
            this.layoutControlItemTradeType.Text = "Trade Type";
            this.layoutControlItemTradeType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemTradeType.TextSize = new System.Drawing.Size(55, 13);
            this.layoutControlItemTradeType.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.btnCalculate;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(901, 0);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(140, 26);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // tbControl
            // 
            this.tbControl.Location = new System.Drawing.Point(4, 130);
            this.tbControl.Name = "tbControl";
            this.tbControl.Padding = new System.Windows.Forms.Padding(4);
            this.tbControl.SelectedTabPage = this.tbpgPositionResults;
            this.tbControl.Size = new System.Drawing.Size(1043, 413);
            this.tbControl.TabIndex = 16;
            this.tbControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbpgPositionResults,
            this.tbpgMTMResults,
            this.tbpgInitialMargin});
            this.tbControl.Visible = false;
            this.tbControl.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.tbControl_SelectedPageChanging);
            // 
            // tbpgMTMResults
            // 
            this.tbpgMTMResults.Controls.Add(this.layoutControl2);
            this.tbpgMTMResults.Name = "tbpgMTMResults";
            this.tbpgMTMResults.Padding = new System.Windows.Forms.Padding(4);
            this.tbpgMTMResults.Size = new System.Drawing.Size(1037, 385);
            this.tbpgMTMResults.Text = "MTM Results";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.tbMTMIndexSummaries);
            this.layoutControl2.Controls.Add(this.gridTradeMTMResults);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(4, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(364, 323, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1029, 377);
            this.layoutControl2.TabIndex = 8;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // tbMTMIndexSummaries
            // 
            this.tbMTMIndexSummaries.Location = new System.Drawing.Point(4, 193);
            this.tbMTMIndexSummaries.Name = "tbMTMIndexSummaries";
            this.tbMTMIndexSummaries.Size = new System.Drawing.Size(1021, 180);
            this.tbMTMIndexSummaries.TabIndex = 9;
            // 
            // gridTradeMTMResults
            // 
            this.gridTradeMTMResults.Location = new System.Drawing.Point(4, 4);
            this.gridTradeMTMResults.MainView = this.gridMTMResultsView;
            this.gridTradeMTMResults.Name = "gridTradeMTMResults";
            this.gridTradeMTMResults.Size = new System.Drawing.Size(1021, 180);
            this.gridTradeMTMResults.TabIndex = 7;
            this.gridTradeMTMResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridMTMResultsView});
            // 
            // gridMTMResultsView
            // 
            this.gridMTMResultsView.GridControl = this.gridTradeMTMResults;
            this.gridMTMResultsView.Name = "gridMTMResultsView";
            this.gridMTMResultsView.OptionsBehavior.Editable = false;
            this.gridMTMResultsView.OptionsCustomization.AllowColumnMoving = false;
            this.gridMTMResultsView.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridMTMResultsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridMTMResultsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridMTMResultsView.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridMTMResultsView.OptionsView.ShowFooter = true;
            this.gridMTMResultsView.OptionsView.ShowGroupPanel = false;
            this.gridMTMResultsView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridMTMResultsView.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.GridMTMResultsViewCustomDrawFooterCell);
            this.gridMTMResultsView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.GridTradeMtmResultsViewCustomSummaryCalculate);
            this.gridMTMResultsView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.GridTradeMtmResultsViewCustomUnboundColumnData);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.splitterItem2,
            this.itemTbMTMIndexSummaries});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1029, 377);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridTradeMTMResults;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1025, 184);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(0, 184);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(1025, 5);
            // 
            // itemTbMTMIndexSummaries
            // 
            this.itemTbMTMIndexSummaries.Control = this.tbMTMIndexSummaries;
            this.itemTbMTMIndexSummaries.CustomizationFormText = "itemTbMTMIndexSummaries";
            this.itemTbMTMIndexSummaries.Location = new System.Drawing.Point(0, 189);
            this.itemTbMTMIndexSummaries.Name = "itemTbMTMIndexSummaries";
            this.itemTbMTMIndexSummaries.Size = new System.Drawing.Size(1025, 184);
            this.itemTbMTMIndexSummaries.Text = "itemTbMTMIndexSummaries";
            this.itemTbMTMIndexSummaries.TextSize = new System.Drawing.Size(0, 0);
            this.itemTbMTMIndexSummaries.TextToControlDistance = 0;
            this.itemTbMTMIndexSummaries.TextVisible = false;
            // 
            // tbpgPositionResults
            // 
            this.tbpgPositionResults.Controls.Add(this.layoutControl1);
            this.tbpgPositionResults.Name = "tbpgPositionResults";
            this.tbpgPositionResults.Padding = new System.Windows.Forms.Padding(4);
            this.tbpgPositionResults.Size = new System.Drawing.Size(1037, 385);
            this.tbpgPositionResults.Text = "Position Results";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.tbPosIndexSummaries);
            this.layoutControl1.Controls.Add(this.gridTradePositionResults);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(4, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(364, 302, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1029, 377);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // tbPosIndexSummaries
            // 
            this.tbPosIndexSummaries.Location = new System.Drawing.Point(4, 189);
            this.tbPosIndexSummaries.Name = "tbPosIndexSummaries";
            this.tbPosIndexSummaries.Size = new System.Drawing.Size(1021, 184);
            this.tbPosIndexSummaries.TabIndex = 9;
            // 
            // gridTradePositionResults
            // 
            this.gridTradePositionResults.Location = new System.Drawing.Point(4, 4);
            this.gridTradePositionResults.MainView = this.gridPositionResultsView;
            this.gridTradePositionResults.Name = "gridTradePositionResults";
            this.gridTradePositionResults.Size = new System.Drawing.Size(1021, 176);
            this.gridTradePositionResults.TabIndex = 7;
            this.gridTradePositionResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridPositionResultsView});
            // 
            // gridPositionResultsView
            // 
            this.gridPositionResultsView.GridControl = this.gridTradePositionResults;
            this.gridPositionResultsView.Name = "gridPositionResultsView";
            this.gridPositionResultsView.OptionsBehavior.Editable = false;
            this.gridPositionResultsView.OptionsCustomization.AllowColumnMoving = false;
            this.gridPositionResultsView.OptionsCustomization.AllowFilter = false;
            this.gridPositionResultsView.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridPositionResultsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridPositionResultsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridPositionResultsView.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gridPositionResultsView.OptionsView.ShowFooter = true;
            this.gridPositionResultsView.OptionsView.ShowGroupedColumns = true;
            this.gridPositionResultsView.OptionsView.ShowGroupPanel = false;
            this.gridPositionResultsView.Tag = "";
            this.gridPositionResultsView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridPositionResultsView.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.GridPositionResultsViewCustomDrawFooterCell);
            this.gridPositionResultsView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.GridTradePositionResultsViewCustomSummaryCalculate);
            this.gridPositionResultsView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.GridTradePositionResultsViewCustomUnboundColumnData);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.splitterItem1,
            this.itemTabPosIndexSummaries});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1029, 377);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridTradePositionResults;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1025, 180);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 180);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1025, 5);
            // 
            // itemTabPosIndexSummaries
            // 
            this.itemTabPosIndexSummaries.Control = this.tbPosIndexSummaries;
            this.itemTabPosIndexSummaries.CustomizationFormText = "itemTabPosIndexSummaries";
            this.itemTabPosIndexSummaries.Location = new System.Drawing.Point(0, 185);
            this.itemTabPosIndexSummaries.Name = "itemTabPosIndexSummaries";
            this.itemTabPosIndexSummaries.Size = new System.Drawing.Size(1025, 188);
            this.itemTabPosIndexSummaries.Text = "itemTabPosIndexSummaries";
            this.itemTabPosIndexSummaries.TextSize = new System.Drawing.Size(0, 0);
            this.itemTabPosIndexSummaries.TextToControlDistance = 0;
            this.itemTabPosIndexSummaries.TextVisible = false;
            // 
            // tbpgInitialMargin
            // 
            this.tbpgInitialMargin.Controls.Add(this.lcInitialMargin);
            this.tbpgInitialMargin.Name = "tbpgInitialMargin";
            this.tbpgInitialMargin.PageVisible = false;
            this.tbpgInitialMargin.Size = new System.Drawing.Size(1037, 385);
            this.tbpgInitialMargin.Text = "Initial Margin";
            // 
            // lcInitialMargin
            // 
            this.lcInitialMargin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcInitialMargin.Location = new System.Drawing.Point(0, 0);
            this.lcInitialMargin.Name = "lcInitialMargin";
            this.lcInitialMargin.Root = this.lcgInitialMargin;
            this.lcInitialMargin.Size = new System.Drawing.Size(1037, 385);
            this.lcInitialMargin.TabIndex = 0;
            this.lcInitialMargin.Text = "layoutControl4";
            // 
            // lcgInitialMargin
            // 
            this.lcgInitialMargin.CustomizationFormText = "lcgInitialMargin";
            this.lcgInitialMargin.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgInitialMargin.GroupBordersVisible = false;
            this.lcgInitialMargin.Location = new System.Drawing.Point(0, 0);
            this.lcgInitialMargin.Name = "lcgInitialMargin";
            this.lcgInitialMargin.Size = new System.Drawing.Size(1037, 385);
            this.lcgInitialMargin.Text = "lcgInitialMargin";
            this.lcgInitialMargin.TextVisible = false;
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "layoutControlGroup1";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTabControl,
            this.layoutControlGroup3});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "Root";
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgMain.Size = new System.Drawing.Size(1051, 547);
            this.lcgMain.Text = "Root";
            this.lcgMain.TextVisible = false;
            // 
            // lciTabControl
            // 
            this.lciTabControl.Control = this.tbControl;
            this.lciTabControl.CustomizationFormText = "lciTabControl";
            this.lciTabControl.Location = new System.Drawing.Point(0, 126);
            this.lciTabControl.Name = "lciTabControl";
            this.lciTabControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.lciTabControl.Size = new System.Drawing.Size(1051, 421);
            this.lciTabControl.Text = "lciTabControl";
            this.lciTabControl.TextSize = new System.Drawing.Size(0, 0);
            this.lciTabControl.TextToControlDistance = 0;
            this.lciTabControl.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Filters";
            this.layoutControlGroup3.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1051, 126);
            this.layoutControlGroup3.Text = "Filters";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.layoutControl3;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 101);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(995, 101);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1045, 101);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // imageListFooter
            // 
            this.imageListFooter.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListFooter.ImageStream")));
            this.imageListFooter.TransparentColor = System.Drawing.Color.Red;
            this.imageListFooter.Images.SetKeyName(0, "Footer.png");
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1009, 58);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutFilterCurveDate
            // 
            this.layoutFilterCurveDate.CustomizationFormText = "Curve Date:";
            this.layoutFilterCurveDate.Location = new System.Drawing.Point(0, 0);
            this.layoutFilterCurveDate.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutFilterCurveDate.MinSize = new System.Drawing.Size(118, 24);
            this.layoutFilterCurveDate.Name = "layoutFilterCurveDate";
            this.layoutFilterCurveDate.Size = new System.Drawing.Size(221, 24);
            this.layoutFilterCurveDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutFilterCurveDate.Text = "Curve Date:";
            this.layoutFilterCurveDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutFilterCurveDate.TextSize = new System.Drawing.Size(59, 13);
            this.layoutFilterCurveDate.TextToControlDistance = 5;
            // 
            // layoutPositionMethod
            // 
            this.layoutPositionMethod.CustomizationFormText = "Position Method:";
            this.layoutPositionMethod.Location = new System.Drawing.Point(221, 0);
            this.layoutPositionMethod.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutPositionMethod.MinSize = new System.Drawing.Size(139, 24);
            this.layoutPositionMethod.Name = "layoutPositionMethod";
            this.layoutPositionMethod.Size = new System.Drawing.Size(218, 24);
            this.layoutPositionMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPositionMethod.Text = "Position Method:";
            this.layoutPositionMethod.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutPositionMethod.TextSize = new System.Drawing.Size(80, 13);
            this.layoutPositionMethod.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityType
            // 
            this.layoutMarketSensitivityType.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.Location = new System.Drawing.Point(439, 0);
            this.layoutMarketSensitivityType.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityType.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityType.Name = "layoutMarketSensitivityType";
            this.layoutMarketSensitivityType.Size = new System.Drawing.Size(231, 24);
            this.layoutMarketSensitivityType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityType.Text = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityType.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityType.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityValue
            // 
            this.layoutMarketSensitivityValue.CustomizationFormText = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.Location = new System.Drawing.Point(670, 0);
            this.layoutMarketSensitivityValue.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityValue.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityValue.Name = "layoutMarketSensitivityValue";
            this.layoutMarketSensitivityValue.Size = new System.Drawing.Size(329, 24);
            this.layoutMarketSensitivityValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityValue.Text = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityValue.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityValue.TextToControlDistance = 5;
            // 
            // layoutMTMResultsType
            // 
            this.layoutMTMResultsType.CustomizationFormText = "MTM Result Type:";
            this.layoutMTMResultsType.Location = new System.Drawing.Point(0, 24);
            this.layoutMTMResultsType.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMTMResultsType.MinSize = new System.Drawing.Size(284, 24);
            this.layoutMTMResultsType.Name = "layoutMTMResultsType";
            this.layoutMTMResultsType.Size = new System.Drawing.Size(312, 24);
            this.layoutMTMResultsType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMTMResultsType.Text = "MTM Type:";
            this.layoutMTMResultsType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMTMResultsType.TextSize = new System.Drawing.Size(53, 13);
            this.layoutMTMResultsType.TextToControlDistance = 5;
            // 
            // layoutOptionPremium
            // 
            this.layoutOptionPremium.CustomizationFormText = "Option Premium:";
            this.layoutOptionPremium.Location = new System.Drawing.Point(312, 24);
            this.layoutOptionPremium.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionPremium.MinSize = new System.Drawing.Size(108, 24);
            this.layoutOptionPremium.Name = "layoutOptionPremium";
            this.layoutOptionPremium.Size = new System.Drawing.Size(156, 24);
            this.layoutOptionPremium.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionPremium.Text = "Option Premium:";
            this.layoutOptionPremium.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionPremium.TextSize = new System.Drawing.Size(79, 13);
            this.layoutOptionPremium.TextToControlDistance = 5;
            // 
            // layoutOptionNullify
            // 
            this.layoutOptionNullify.CustomizationFormText = "Option Nullify:";
            this.layoutOptionNullify.Location = new System.Drawing.Point(468, 24);
            this.layoutOptionNullify.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionNullify.MinSize = new System.Drawing.Size(97, 24);
            this.layoutOptionNullify.Name = "layoutOptionNullify";
            this.layoutOptionNullify.Size = new System.Drawing.Size(125, 24);
            this.layoutOptionNullify.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionNullify.Text = "Option Nullify:";
            this.layoutOptionNullify.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionNullify.TextSize = new System.Drawing.Size(68, 13);
            this.layoutOptionNullify.TextToControlDistance = 5;
            // 
            // layoutOptionEmbeddedValue
            // 
            this.layoutOptionEmbeddedValue.CustomizationFormText = "Option Embedded Value:";
            this.layoutOptionEmbeddedValue.Location = new System.Drawing.Point(593, 24);
            this.layoutOptionEmbeddedValue.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionEmbeddedValue.MinSize = new System.Drawing.Size(121, 24);
            this.layoutOptionEmbeddedValue.Name = "layoutOptionEmbeddedValue";
            this.layoutOptionEmbeddedValue.Size = new System.Drawing.Size(163, 24);
            this.layoutOptionEmbeddedValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionEmbeddedValue.Text = "Option Emb. Value:";
            this.layoutOptionEmbeddedValue.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionEmbeddedValue.TextSize = new System.Drawing.Size(92, 13);
            this.layoutOptionEmbeddedValue.TextToControlDistance = 5;
            // 
            // layoutUseSpotValues
            // 
            this.layoutUseSpotValues.CustomizationFormText = "Use Spot:";
            this.layoutUseSpotValues.Location = new System.Drawing.Point(756, 24);
            this.layoutUseSpotValues.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutUseSpotValues.MinSize = new System.Drawing.Size(76, 24);
            this.layoutUseSpotValues.Name = "layoutUseSpotValues";
            this.layoutUseSpotValues.Size = new System.Drawing.Size(243, 24);
            this.layoutUseSpotValues.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutUseSpotValues.Text = "Use Spot:";
            this.layoutUseSpotValues.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutUseSpotValues.TextSize = new System.Drawing.Size(47, 13);
            this.layoutUseSpotValues.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.CustomizationFormText = "Curve Date:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(118, 24);
            this.layoutControlItem5.Name = "layoutFilterCurveDate";
            this.layoutControlItem5.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Curve Date:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(59, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.CustomizationFormText = "Position Method:";
            this.layoutControlItem7.Location = new System.Drawing.Point(221, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(139, 24);
            this.layoutControlItem7.Name = "layoutPositionMethod";
            this.layoutControlItem7.Size = new System.Drawing.Size(218, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Position Method:";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 13);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutControlItem8.Location = new System.Drawing.Point(439, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(187, 24);
            this.layoutControlItem8.Name = "layoutMarketSensitivityType";
            this.layoutControlItem8.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Market Sensitivity Type:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(118, 13);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.CustomizationFormText = "Market Sensitivity Value:";
            this.layoutControlItem9.Location = new System.Drawing.Point(670, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(187, 24);
            this.layoutControlItem9.Name = "layoutMarketSensitivityValue";
            this.layoutControlItem9.Size = new System.Drawing.Size(329, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "Market Sensitivity Value:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(118, 13);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.CustomizationFormText = "MTM Result Type:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(284, 24);
            this.layoutControlItem10.Name = "layoutMTMResultsType";
            this.layoutControlItem10.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "MTM Type:";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(53, 13);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.CustomizationFormText = "Option Premium:";
            this.layoutControlItem11.Location = new System.Drawing.Point(312, 24);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(108, 24);
            this.layoutControlItem11.Name = "layoutOptionPremium";
            this.layoutControlItem11.Size = new System.Drawing.Size(156, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "Option Premium:";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(79, 13);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.CustomizationFormText = "Option Nullify:";
            this.layoutControlItem12.Location = new System.Drawing.Point(468, 24);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(97, 24);
            this.layoutControlItem12.Name = "layoutOptionNullify";
            this.layoutControlItem12.Size = new System.Drawing.Size(125, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "Option Nullify:";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(68, 13);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.CustomizationFormText = "Option Embedded Value:";
            this.layoutControlItem13.Location = new System.Drawing.Point(593, 24);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(121, 24);
            this.layoutControlItem13.Name = "layoutOptionEmbeddedValue";
            this.layoutControlItem13.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "Option Emb. Value:";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(92, 13);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.CustomizationFormText = "Use Spot:";
            this.layoutControlItem14.Location = new System.Drawing.Point(756, 24);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(76, 24);
            this.layoutControlItem14.Name = "layoutUseSpotValues";
            this.layoutControlItem14.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "Use Spot:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(47, 13);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.CustomizationFormText = "Curve Date:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(118, 24);
            this.layoutControlItem15.Name = "layoutFilterCurveDate";
            this.layoutControlItem15.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "Curve Date:";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(59, 13);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Location = new System.Drawing.Point(786, 4);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(173, 43);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // PositionMTMRatiosReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1051, 579);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PositionMTMRatiosReportForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbBxEdtTradeType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionEmbeddedValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionNullify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgMTMResultsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCalculateCommissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTradeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).EndInit();
            this.tbControl.ResumeLayout(false);
            this.tbpgMTMResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbMTMIndexSummaries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeMTMResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMTMResultsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemTbMTMIndexSummaries)).EndInit();
            this.tbpgPositionResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbPosIndexSummaries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradePositionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPositionResultsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemTabPosIndexSummaries)).EndInit();
            this.tbpgInitialMargin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInitialMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMTMResultsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionNullify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionEmbeddedValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraGrid.GridControl gridTradePositionResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridPositionResultsView;
        private DevExpress.XtraGrid.GridControl gridTradeMTMResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridMTMResultsView;
        private DevExpress.Utils.ImageCollection imageList24;
        private System.Windows.Forms.ImageList imageListFooter;
        private DevExpress.XtraTab.XtraTabControl tbControl;
        private DevExpress.XtraTab.XtraTabPage tbpgPositionResults;
        private DevExpress.XtraTab.XtraTabPage tbpgMTMResults;
        private DevExpress.XtraLayout.LayoutControlItem lciTabControl;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutMTMResultsType;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionNullify;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionEmbeddedValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutUseSpotValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.DateEdit dtpFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SpinEdit txtMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.RadioGroup rdgMTMResultsType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.CheckEdit chkOptionPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.CheckEdit chkOptionNullify;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.CheckEdit chkOptionEmbeddedValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit chkUseSpotValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.SimpleButton btnCalculate;
        private DevExpress.XtraEditors.ComboBoxEdit cmbMarketSensitivityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnExport;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraTab.XtraTabControl tbMTMIndexSummaries;
        private DevExpress.XtraLayout.LayoutControlItem itemTbMTMIndexSummaries;
        private DevExpress.XtraTab.XtraTabControl tbPosIndexSummaries;
        private DevExpress.XtraLayout.LayoutControlItem itemTabPosIndexSummaries;
        private DevExpress.XtraTab.XtraTabPage tbpgInitialMargin;
        private DevExpress.XtraLayout.LayoutControl lcInitialMargin;
        private DevExpress.XtraLayout.LayoutControlGroup lcgInitialMargin;
        private DevExpress.XtraEditors.CheckEdit chkCalculateSums;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateSums;
        private DevExpress.XtraEditors.CheckEdit chkCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlItem lciCalculateCommissions;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkCmbBxEdtTradeType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.RadioGroup rdgResultsAggregationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTradeType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
    }
}