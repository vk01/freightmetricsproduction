﻿namespace Exis.WinClient.Controls.Core
{
    partial class ImportTradesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportTradesForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.btnImportCargoTrades = new DevExpress.XtraEditors.SimpleButton();
            this.imageList16x16 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnSelectCargoTradesFile = new DevExpress.XtraEditors.ButtonEdit();
            this.btnImportTCTrades = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectTCTradesFolder = new DevExpress.XtraEditors.ButtonEdit();
            this.btnImportFFAOptionTrades = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectFFAOptionTradesFile = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgImportFFAOptionTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemBtnSelectFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgImportTCTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemBtnSelectFolder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgImportCargoTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layCtlItemBtnSelectCargoFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectCargoTradesFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectTCTradesFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectFFAOptionTradesFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgImportFFAOptionTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnSelectFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgImportTCTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnSelectFolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgImportCargoTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnSelectCargoFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.btnImportCargoTrades);
            this.layoutControl.Controls.Add(this.btnSelectCargoTradesFile);
            this.layoutControl.Controls.Add(this.btnImportTCTrades);
            this.layoutControl.Controls.Add(this.btnSelectTCTradesFolder);
            this.layoutControl.Controls.Add(this.btnImportFFAOptionTrades);
            this.layoutControl.Controls.Add(this.btnSelectFFAOptionTradesFile);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(737, 392);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // btnImportCargoTrades
            // 
            this.btnImportCargoTrades.ImageIndex = 0;
            this.btnImportCargoTrades.ImageList = this.imageList16x16;
            this.btnImportCargoTrades.Location = new System.Drawing.Point(629, 104);
            this.btnImportCargoTrades.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportCargoTrades.MaximumSize = new System.Drawing.Size(91, 27);
            this.btnImportCargoTrades.MinimumSize = new System.Drawing.Size(91, 27);
            this.btnImportCargoTrades.Name = "btnImportCargoTrades";
            this.btnImportCargoTrades.Size = new System.Drawing.Size(91, 27);
            this.btnImportCargoTrades.StyleController = this.layoutControl;
            this.btnImportCargoTrades.TabIndex = 15;
            this.btnImportCargoTrades.Text = "Import";
            this.btnImportCargoTrades.Click += new System.EventHandler(this.BtnImportCargoTradesClick);
            // 
            // imageList16x16
            // 
            this.imageList16x16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16x16.ImageStream")));
            this.imageList16x16.Images.SetKeyName(0, "import.ico");
            this.imageList16x16.Images.SetKeyName(1, "error2.ico");
            this.imageList16x16.Images.SetKeyName(2, "warning.ico");
            this.imageList16x16.Images.SetKeyName(3, "ok.ico");
            // 
            // btnSelectCargoTradesFile
            // 
            this.btnSelectCargoTradesFile.Location = new System.Drawing.Point(111, 104);
            this.btnSelectCargoTradesFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelectCargoTradesFile.Name = "btnSelectCargoTradesFile";
            this.btnSelectCargoTradesFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnSelectCargoTradesFile.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, global::Exis.WinClient.Strings.Eve, null, null, true)});
            this.btnSelectCargoTradesFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSelectCargoTradesFile.Size = new System.Drawing.Size(514, 22);
            this.btnSelectCargoTradesFile.StyleController = this.layoutControl;
            this.btnSelectCargoTradesFile.TabIndex = 5;
            this.btnSelectCargoTradesFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSelectCargoTradesFile_ButtonClick);
            // 
            // btnImportTCTrades
            // 
            this.btnImportTCTrades.ImageIndex = 0;
            this.btnImportTCTrades.ImageList = this.imageList16x16;
            this.btnImportTCTrades.Location = new System.Drawing.Point(629, 168);
            this.btnImportTCTrades.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportTCTrades.MaximumSize = new System.Drawing.Size(91, 27);
            this.btnImportTCTrades.MinimumSize = new System.Drawing.Size(91, 27);
            this.btnImportTCTrades.Name = "btnImportTCTrades";
            this.btnImportTCTrades.Size = new System.Drawing.Size(91, 27);
            this.btnImportTCTrades.StyleController = this.layoutControl;
            this.btnImportTCTrades.TabIndex = 15;
            this.btnImportTCTrades.Text = "Import";
            this.btnImportTCTrades.Click += new System.EventHandler(this.BtnImportTcTradesClick);
            // 
            // btnSelectTCTradesFolder
            // 
            this.btnSelectTCTradesFolder.Location = new System.Drawing.Point(111, 168);
            this.btnSelectTCTradesFolder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelectTCTradesFolder.Name = "btnSelectTCTradesFolder";
            this.btnSelectTCTradesFolder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnSelectTCTradesFolder.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, global::Exis.WinClient.Strings.Eve, null, null, true)});
            this.btnSelectTCTradesFolder.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSelectTCTradesFolder.Size = new System.Drawing.Size(514, 22);
            this.btnSelectTCTradesFolder.StyleController = this.layoutControl;
            this.btnSelectTCTradesFolder.TabIndex = 5;
            this.btnSelectTCTradesFolder.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSelectTCTradesFile_ButtonClick);
            // 
            // btnImportFFAOptionTrades
            // 
            this.btnImportFFAOptionTrades.ImageIndex = 0;
            this.btnImportFFAOptionTrades.ImageList = this.imageList16x16;
            this.btnImportFFAOptionTrades.Location = new System.Drawing.Point(629, 40);
            this.btnImportFFAOptionTrades.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportFFAOptionTrades.MaximumSize = new System.Drawing.Size(91, 27);
            this.btnImportFFAOptionTrades.MinimumSize = new System.Drawing.Size(91, 27);
            this.btnImportFFAOptionTrades.Name = "btnImportFFAOptionTrades";
            this.btnImportFFAOptionTrades.Size = new System.Drawing.Size(91, 27);
            this.btnImportFFAOptionTrades.StyleController = this.layoutControl;
            this.btnImportFFAOptionTrades.TabIndex = 14;
            this.btnImportFFAOptionTrades.Text = "Import";
            this.btnImportFFAOptionTrades.Click += new System.EventHandler(this.BtnImportFfaOptionTradesClick);
            // 
            // btnSelectFFAOptionTradesFile
            // 
            this.btnSelectFFAOptionTradesFile.Location = new System.Drawing.Point(111, 40);
            this.btnSelectFFAOptionTradesFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelectFFAOptionTradesFile.Name = "btnSelectFFAOptionTradesFile";
            this.btnSelectFFAOptionTradesFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.Eve, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnSelectFFAOptionTradesFile.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, global::Exis.WinClient.Strings.Eve, null, null, true)});
            this.btnSelectFFAOptionTradesFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSelectFFAOptionTradesFile.Size = new System.Drawing.Size(514, 22);
            this.btnSelectFFAOptionTradesFile.StyleController = this.layoutControl;
            this.btnSelectFFAOptionTradesFile.TabIndex = 4;
            this.btnSelectFFAOptionTradesFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnImportFfaOptionTradesButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgImportFFAOptionTrades,
            this.lcgImportTCTrades,
            this.emptySpaceItem1,
            this.lcgImportCargoTrades});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(737, 392);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lcgImportFFAOptionTrades
            // 
            this.lcgImportFFAOptionTrades.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lcgImportFFAOptionTrades.AppearanceGroup.Options.UseFont = true;
            this.lcgImportFFAOptionTrades.CustomizationFormText = "Import FFA/Option Trades";
            this.lcgImportFFAOptionTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemBtnSelectFile,
            this.layoutControlItem1});
            this.lcgImportFFAOptionTrades.Location = new System.Drawing.Point(0, 0);
            this.lcgImportFFAOptionTrades.Name = "lcgImportFFAOptionTrades";
            this.lcgImportFFAOptionTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgImportFFAOptionTrades.Size = new System.Drawing.Size(717, 64);
            this.lcgImportFFAOptionTrades.Text = "Import FFA/Option Trades";
            // 
            // layCtlItemBtnSelectFile
            // 
            this.layCtlItemBtnSelectFile.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layCtlItemBtnSelectFile.AppearanceItemCaption.Options.UseFont = true;
            this.layCtlItemBtnSelectFile.Control = this.btnSelectFFAOptionTradesFile;
            this.layCtlItemBtnSelectFile.CustomizationFormText = "Select file to import from:";
            this.layCtlItemBtnSelectFile.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemBtnSelectFile.Name = "layCtlItemBtnSelectFile";
            this.layCtlItemBtnSelectFile.Size = new System.Drawing.Size(612, 31);
            this.layCtlItemBtnSelectFile.Text = "Select a file:";
            this.layCtlItemBtnSelectFile.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnImportFFAOptionTrades;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(612, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(95, 31);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // lcgImportTCTrades
            // 
            this.lcgImportTCTrades.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lcgImportTCTrades.AppearanceGroup.Options.UseFont = true;
            this.lcgImportTCTrades.CustomizationFormText = "Import TC Trades";
            this.lcgImportTCTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemBtnSelectFolder,
            this.layoutControlItem3});
            this.lcgImportTCTrades.Location = new System.Drawing.Point(0, 128);
            this.lcgImportTCTrades.Name = "lcgImportTCTrades";
            this.lcgImportTCTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgImportTCTrades.Size = new System.Drawing.Size(717, 64);
            this.lcgImportTCTrades.Text = "Import TC Trades";
            // 
            // layCtlItemBtnSelectFolder
            // 
            this.layCtlItemBtnSelectFolder.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layCtlItemBtnSelectFolder.AppearanceItemCaption.Options.UseFont = true;
            this.layCtlItemBtnSelectFolder.Control = this.btnSelectTCTradesFolder;
            this.layCtlItemBtnSelectFolder.CustomizationFormText = "Select folder to import from:";
            this.layCtlItemBtnSelectFolder.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemBtnSelectFolder.Name = "layCtlItemBtnSelectFolder";
            this.layCtlItemBtnSelectFolder.Size = new System.Drawing.Size(612, 31);
            this.layCtlItemBtnSelectFolder.Text = "Select a folder:";
            this.layCtlItemBtnSelectFolder.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnImportTCTrades;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(612, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(95, 31);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 192);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(717, 180);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgImportCargoTrades
            // 
            this.lcgImportCargoTrades.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lcgImportCargoTrades.AppearanceGroup.Options.UseFont = true;
            this.lcgImportCargoTrades.CustomizationFormText = "Import Cargo Trades";
            this.lcgImportCargoTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layCtlItemBtnSelectCargoFile,
            this.layoutControlItem5});
            this.lcgImportCargoTrades.Location = new System.Drawing.Point(0, 64);
            this.lcgImportCargoTrades.Name = "lcgImportCargoTrades";
            this.lcgImportCargoTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgImportCargoTrades.Size = new System.Drawing.Size(717, 64);
            this.lcgImportCargoTrades.Text = "Import Cargo Trades";
            // 
            // layCtlItemBtnSelectCargoFile
            // 
            this.layCtlItemBtnSelectCargoFile.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.layCtlItemBtnSelectCargoFile.AppearanceItemCaption.Options.UseFont = true;
            this.layCtlItemBtnSelectCargoFile.Control = this.btnSelectCargoTradesFile;
            this.layCtlItemBtnSelectCargoFile.CustomizationFormText = "Select file to import from:";
            this.layCtlItemBtnSelectCargoFile.Location = new System.Drawing.Point(0, 0);
            this.layCtlItemBtnSelectCargoFile.Name = "layCtlItemBtnSelectCargoFile";
            this.layCtlItemBtnSelectCargoFile.Size = new System.Drawing.Size(612, 31);
            this.layCtlItemBtnSelectCargoFile.Text = "Select a file:";
            this.layCtlItemBtnSelectCargoFile.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnImportCargoTrades;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(612, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(95, 31);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "exit.ico");
            this.imageList24.Images.SetKeyName(1, "import.ico");
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnClose});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 0;
            this.btnClose.LargeImageIndex = 0;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(737, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 392);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(737, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 392);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(737, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 392);
            // 
            // ImportTradesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 430);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ImportTradesForm";
            this.Text = "Import Trades";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageList16x16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectCargoTradesFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectTCTradesFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectFFAOptionTradesFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgImportFFAOptionTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnSelectFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgImportTCTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnSelectFolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgImportCargoTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layCtlItemBtnSelectCargoFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ButtonEdit btnSelectFFAOptionTradesFile;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemBtnSelectFile;
        private DevExpress.Utils.ImageCollection imageList16x16;
        private DevExpress.XtraEditors.SimpleButton btnImportTCTrades;
        private DevExpress.XtraEditors.ButtonEdit btnSelectTCTradesFolder;
        private DevExpress.XtraEditors.SimpleButton btnImportFFAOptionTrades;
        private DevExpress.XtraLayout.LayoutControlGroup lcgImportFFAOptionTrades;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgImportTCTrades;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemBtnSelectFolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.SimpleButton btnImportCargoTrades;
        private DevExpress.XtraEditors.ButtonEdit btnSelectCargoTradesFile;
        private DevExpress.XtraLayout.LayoutControlGroup lcgImportCargoTrades;
        private DevExpress.XtraLayout.LayoutControlItem layCtlItemBtnSelectCargoFile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}