﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Description;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.Domain;
using Exis.WinClient.ClientsEngineService;
using Exis.WinClient.General;
using Exis.WinClient.Resources;

namespace Exis.WinClient.Controls.Core
{
    public partial class MCCriteriaForm : XtraForm
    {
        #region Private Properties

        private MCSimulationCriteria _criteria;
        private DateTime _lastImportDate;

        #endregion

        #region Constructors

        public MCCriteriaForm(DateTime lastImportDate)
        {
            _lastImportDate = lastImportDate;
            _criteria = new MCSimulationCriteria();
            InitializeComponent();
            InitializeControls();
            

            dxErrorProvider1.DataSource = typeof (MCSimulationCriteria);
        }

        private void InitializeControls()
        {
            dtpFilterCurveDate.DateTime = _lastImportDate;
            dtpFilterCurveDate.Properties.MaxValue = _lastImportDate;
        }

        #endregion

        #region GUI Events

        private void MCGetNameForm_Activated(object sender, EventArgs e)
        {
            txtName.Focus();
        }

        private void MCGetNameForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnRun.PerformClick();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
                e.Handled = true;
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {

            if ((int) txtSimulationRuns.Value == 0)
            {
                XtraMessageBox.Show(this,
                   "Simulations Number cannot be 0.",
                   Strings.Freight_Metrics,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            //var mcName = txtName.Text;
            if (ValidateData())
            {
                GetCriteriaFromControls();
                Close();
                if (OnGetMCCriteria != null && txtName.Text != "") OnGetMCCriteria(_criteria);
            }
            else
            {
                XtraMessageBox.Show(this,
                    "Please fill all the fields in form.",
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidateData()
        {
            bool validation = !(string.IsNullOrEmpty(txtName.Text) ||
                                string.IsNullOrEmpty(dtpFilterCurveDate.DateTime.Date.ToString()) ||
                                string.IsNullOrEmpty((string)cmbPositionMethod.SelectedItem) ||
                                string.IsNullOrEmpty((string)cmbMarketSensitivityType.SelectedItem) ||
                                string.IsNullOrEmpty(txtMarketSensitivityValue.Value.ToString()) ||
                                string.IsNullOrEmpty((string)rdgMTMResultsType.EditValue) || (int)txtSimulationRuns.Value==0);

            return validation;
        }

        private void GetCriteriaFromControls()
        {
            _criteria.Name = txtName.Text;
            _criteria.CurveDate = dtpFilterCurveDate.DateTime.Date;
            _criteria.PositionMethod = (string)cmbPositionMethod.SelectedItem;
            _criteria.MarketSensitivityType = (string)cmbMarketSensitivityType.SelectedItem;
            _criteria.MarketSensitivityValue = txtMarketSensitivityValue.Value;
            _criteria.UseSpotValue = chkUseSpotValues.Checked;
            _criteria.IsOptionPremium = chkOptionPremium.Checked;
            _criteria.MTMType = (string)rdgMTMResultsType.EditValue;
            _criteria.SimulationRuns = (int) txtSimulationRuns.Value;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        #endregion

        #region Form Controls Handlers

        #endregion

        #region Proxy Calls

        #endregion

        #region Events
        public event Action<MCSimulationCriteria> OnGetMCCriteria;
        #endregion

        private void cmbMarketSensitivityType_SelectedValueChanged(object sender, EventArgs e)
        {
            txtMarketSensitivityValue.Properties.ReadOnly = (cmbMarketSensitivityType.SelectedItem != null &&
                                                            (cmbMarketSensitivityType.SelectedItem.ToString() ==
                                                             "Stress" ||
                                                             cmbMarketSensitivityType.SelectedItem.ToString() == "None"));
        }

    }

    public class MCSimulationCriteria
    {
        public string Name { get; set; }
        public DateTime CurveDate { get; set; }
        public string PositionMethod { get; set; }
        public string MTMType { get; set; }
        public string MarketSensitivityType { get; set; }
        public decimal MarketSensitivityValue { get; set; }
        public bool UseSpotValue { get; set; }
        public bool IsOptionPremium { get; set; }
        public int SimulationRuns {get;set;}
    }
}