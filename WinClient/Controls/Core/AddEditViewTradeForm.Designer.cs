﻿namespace Exis.WinClient.Controls.Core
{
    partial class AddEditViewTradeForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditViewTradeForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpVersions = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbState = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtVentureShare = new DevExpress.XtraEditors.SpinEdit();
            this.chkIsJointVenture = new DevExpress.XtraEditors.CheckEdit();
            this.txtExternalCode = new DevExpress.XtraEditors.TextEdit();
            this.lblCode = new DevExpress.XtraEditors.LabelControl();
            this.lookupMTMStressCurve = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupMTMForwardCurve = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupMarketRoute = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupMarketRegion = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupMarketSegment = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupTrader = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupCounterparty = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupCompany = new DevExpress.XtraEditors.LookUpEdit();
            this.txtComments = new DevExpress.XtraEditors.MemoEdit();
            this.lblId = new DevExpress.XtraEditors.LabelControl();
            this.txtStrategySet = new DevExpress.XtraEditors.TextEdit();
            this.treeBook = new DevExpress.XtraTreeList.TreeList();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnRemoveBroker = new DevExpress.XtraEditors.SimpleButton();
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.btnAddBroker = new DevExpress.XtraEditors.SimpleButton();
            this.lookupBrokerInfo_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.txtBrokerCommission_1 = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBrokers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBroker_1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBrokerCommission_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBrokerInfo_1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemAddBroker = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemRemoveBroker = new DevExpress.XtraLayout.LayoutControlItem();
            this.dtpSignDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGeneral = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSignDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStrategySet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCompany = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCounterparty = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTrader = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExternalCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVentureShare = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutState = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemTradeVersions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutIsJointVenture = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBook = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgBrokers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemBrokerCommissionInfo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPortfolio = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutMarketSegment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketRegion = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketRoute = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarkToMarket = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutForwardCurve = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutStressTestingCurve = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTradeExtraInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpVersions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVentureShare.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsJointVenture.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExternalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMTMStressCurve.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMTMForwardCurve.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarketRoute.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarketRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarketSegment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupTrader.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCounterparty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStrategySet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookupBrokerInfo_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrokerCommission_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBrokers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBroker_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBrokerCommission_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBrokerInfo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddBroker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveBroker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSignDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStrategySet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCounterparty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExternalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVentureShare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeVersions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIsJointVenture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBrokers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBrokerCommissionInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPortfolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSegment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarkToMarket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutForwardCurve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStressTestingCurve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeExtraInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.lookUpVersions);
            this.layoutControl1.Controls.Add(this.cmbState);
            this.layoutControl1.Controls.Add(this.txtVentureShare);
            this.layoutControl1.Controls.Add(this.chkIsJointVenture);
            this.layoutControl1.Controls.Add(this.txtExternalCode);
            this.layoutControl1.Controls.Add(this.lblCode);
            this.layoutControl1.Controls.Add(this.lookupMTMStressCurve);
            this.layoutControl1.Controls.Add(this.lookupMTMForwardCurve);
            this.layoutControl1.Controls.Add(this.lookupMarketRoute);
            this.layoutControl1.Controls.Add(this.lookupMarketRegion);
            this.layoutControl1.Controls.Add(this.lookupMarketSegment);
            this.layoutControl1.Controls.Add(this.lookupTrader);
            this.layoutControl1.Controls.Add(this.lookupCounterparty);
            this.layoutControl1.Controls.Add(this.lookupCompany);
            this.layoutControl1.Controls.Add(this.txtComments);
            this.layoutControl1.Controls.Add(this.lblId);
            this.layoutControl1.Controls.Add(this.txtStrategySet);
            this.layoutControl1.Controls.Add(this.treeBook);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.dtpSignDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(617, 377, 250, 350);
            this.layoutControl1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl1.Root = this.layoutRoot;
            this.layoutControl1.Size = new System.Drawing.Size(1137, 710);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lookUpVersions
            // 
            this.lookUpVersions.Location = new System.Drawing.Point(769, 27);
            this.lookUpVersions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookUpVersions.Name = "lookUpVersions";
            this.lookUpVersions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lookUpVersions.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.lookUpVersions.Size = new System.Drawing.Size(363, 22);
            this.lookUpVersions.StyleController = this.layoutControl1;
            this.lookUpVersions.TabIndex = 30;
            this.lookUpVersions.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpVersions_ButtonClick);
            // 
            // cmbState
            // 
            this.cmbState.Location = new System.Drawing.Point(165, 27);
            this.cmbState.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbState.Name = "cmbState";
            this.cmbState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbState.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbState.Size = new System.Drawing.Size(186, 22);
            this.cmbState.StyleController = this.layoutControl1;
            this.cmbState.TabIndex = 29;
            // 
            // txtVentureShare
            // 
            this.txtVentureShare.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtVentureShare.Location = new System.Drawing.Point(1052, 79);
            this.txtVentureShare.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVentureShare.Name = "txtVentureShare";
            this.txtVentureShare.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVentureShare.Properties.Mask.EditMask = "P0";
            this.txtVentureShare.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtVentureShare.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtVentureShare.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtVentureShare.Size = new System.Drawing.Size(80, 22);
            this.txtVentureShare.StyleController = this.layoutControl1;
            this.txtVentureShare.TabIndex = 8;
            // 
            // chkIsJointVenture
            // 
            this.chkIsJointVenture.Location = new System.Drawing.Point(919, 79);
            this.chkIsJointVenture.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkIsJointVenture.Name = "chkIsJointVenture";
            this.chkIsJointVenture.Properties.Caption = "";
            this.chkIsJointVenture.Size = new System.Drawing.Size(38, 19);
            this.chkIsJointVenture.StyleController = this.layoutControl1;
            this.chkIsJointVenture.TabIndex = 28;
            this.chkIsJointVenture.CheckedChanged += new System.EventHandler(this.chkIsJointVenture_CheckedChanged);
            // 
            // txtExternalCode
            // 
            this.txtExternalCode.Location = new System.Drawing.Point(442, 27);
            this.txtExternalCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtExternalCode.Name = "txtExternalCode";
            this.txtExternalCode.Properties.MaxLength = 1000;
            this.txtExternalCode.Size = new System.Drawing.Size(266, 22);
            this.txtExternalCode.StyleController = this.layoutControl1;
            this.txtExternalCode.TabIndex = 8;
            // 
            // lblCode
            // 
            this.lblCode.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblCode.Location = new System.Drawing.Point(82, 27);
            this.lblCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(41, 19);
            this.lblCode.StyleController = this.layoutControl1;
            this.lblCode.TabIndex = 16;
            this.lblCode.Text = "Code";
            // 
            // lookupMTMStressCurve
            // 
            this.lookupMTMStressCurve.Location = new System.Drawing.Point(132, 659);
            this.lookupMTMStressCurve.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupMTMStressCurve.Name = "lookupMTMStressCurve";
            this.lookupMTMStressCurve.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMTMStressCurve.Size = new System.Drawing.Size(387, 22);
            this.lookupMTMStressCurve.StyleController = this.layoutControl1;
            this.lookupMTMStressCurve.TabIndex = 26;
            // 
            // lookupMTMForwardCurve
            // 
            this.lookupMTMForwardCurve.Location = new System.Drawing.Point(98, 633);
            this.lookupMTMForwardCurve.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupMTMForwardCurve.Name = "lookupMTMForwardCurve";
            this.lookupMTMForwardCurve.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMTMForwardCurve.Size = new System.Drawing.Size(421, 22);
            this.lookupMTMForwardCurve.StyleController = this.layoutControl1;
            this.lookupMTMForwardCurve.TabIndex = 25;
            // 
            // lookupMarketRoute
            // 
            this.lookupMarketRoute.Location = new System.Drawing.Point(725, 329);
            this.lookupMarketRoute.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupMarketRoute.Name = "lookupMarketRoute";
            this.lookupMarketRoute.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMarketRoute.Size = new System.Drawing.Size(407, 22);
            this.lookupMarketRoute.StyleController = this.layoutControl1;
            this.lookupMarketRoute.TabIndex = 24;
            // 
            // lookupMarketRegion
            // 
            this.lookupMarketRegion.Location = new System.Drawing.Point(320, 329);
            this.lookupMarketRegion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupMarketRegion.Name = "lookupMarketRegion";
            this.lookupMarketRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMarketRegion.Size = new System.Drawing.Size(360, 22);
            this.lookupMarketRegion.StyleController = this.layoutControl1;
            this.lookupMarketRegion.TabIndex = 23;
            // 
            // lookupMarketSegment
            // 
            this.lookupMarketSegment.Location = new System.Drawing.Point(52, 329);
            this.lookupMarketSegment.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupMarketSegment.Name = "lookupMarketSegment";
            this.lookupMarketSegment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupMarketSegment.Size = new System.Drawing.Size(179, 22);
            this.lookupMarketSegment.StyleController = this.layoutControl1;
            this.lookupMarketSegment.TabIndex = 22;
            this.lookupMarketSegment.EditValueChanged += new System.EventHandler(this.lookupMarketSegment_EditValueChanged);
            // 
            // lookupTrader
            // 
            this.lookupTrader.Location = new System.Drawing.Point(52, 79);
            this.lookupTrader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupTrader.Name = "lookupTrader";
            this.lookupTrader.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupTrader.Size = new System.Drawing.Size(273, 22);
            this.lookupTrader.StyleController = this.layoutControl1;
            this.lookupTrader.TabIndex = 21;
            this.lookupTrader.EditValueChanged += new System.EventHandler(this.lookupTrader_EditValueChanged);
            // 
            // lookupCounterparty
            // 
            this.lookupCounterparty.Location = new System.Drawing.Point(573, 53);
            this.lookupCounterparty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCounterparty.Name = "lookupCounterparty";
            this.lookupCounterparty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupCounterparty.Size = new System.Drawing.Size(384, 22);
            this.lookupCounterparty.StyleController = this.layoutControl1;
            this.lookupCounterparty.TabIndex = 20;
            // 
            // lookupCompany
            // 
            this.lookupCompany.Location = new System.Drawing.Point(66, 53);
            this.lookupCompany.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCompany.Name = "lookupCompany";
            this.lookupCompany.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupCompany.Size = new System.Drawing.Size(421, 22);
            this.lookupCompany.StyleController = this.layoutControl1;
            this.lookupCompany.TabIndex = 19;
            this.lookupCompany.EditValueChanged += new System.EventHandler(this.lookupCompany_EditValueChanged);
            // 
            // txtComments
            // 
            this.txtComments.Location = new System.Drawing.Point(595, 608);
            this.txtComments.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtComments.Name = "txtComments";
            this.txtComments.Properties.MaxLength = 1000;
            this.txtComments.Size = new System.Drawing.Size(540, 100);
            this.txtComments.StyleController = this.layoutControl1;
            this.txtComments.TabIndex = 8;
            this.txtComments.UseOptimizedRendering = true;
            // 
            // lblId
            // 
            this.lblId.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblId.Location = new System.Drawing.Point(24, 27);
            this.lblId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(17, 19);
            this.lblId.StyleController = this.layoutControl1;
            this.lblId.TabIndex = 15;
            this.lblId.Text = "Id";
            // 
            // txtStrategySet
            // 
            this.txtStrategySet.Location = new System.Drawing.Point(408, 79);
            this.txtStrategySet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStrategySet.Name = "txtStrategySet";
            this.txtStrategySet.Properties.MaxLength = 1000;
            this.txtStrategySet.Size = new System.Drawing.Size(424, 22);
            this.txtStrategySet.StyleController = this.layoutControl1;
            this.txtStrategySet.TabIndex = 14;
            // 
            // treeBook
            // 
            this.treeBook.Location = new System.Drawing.Point(687, 129);
            this.treeBook.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeBook.Name = "treeBook";
            this.treeBook.OptionsBehavior.Editable = false;
            this.treeBook.OptionsView.ShowCheckBoxes = true;
            this.treeBook.OptionsView.ShowColumns = false;
            this.treeBook.OptionsView.ShowHorzLines = false;
            this.treeBook.OptionsView.ShowIndicator = false;
            this.treeBook.OptionsView.ShowVertLines = false;
            this.treeBook.Size = new System.Drawing.Size(442, 165);
            this.treeBook.TabIndex = 13;
            this.treeBook.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeBook_BeforeCheckNode);
            this.treeBook.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeBook_AfterCheckNode);
            this.treeBook.NodeChanged += new DevExpress.XtraTreeList.NodeChangedEventHandler(this.treeBook_NodeChanged);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.layoutControl2);
            this.panelControl1.Location = new System.Drawing.Point(8, 127);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.MaximumSize = new System.Drawing.Size(0, 148);
            this.panelControl1.MinimumSize = new System.Drawing.Size(0, 148);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(669, 148);
            this.panelControl1.TabIndex = 12;
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomizationMenu = false;
            this.layoutControl2.Controls.Add(this.btnRemoveBroker);
            this.layoutControl2.Controls.Add(this.btnAddBroker);
            this.layoutControl2.Controls.Add(this.lookupBrokerInfo_1);
            this.layoutControl2.Controls.Add(this.txtBrokerCommission_1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(974, 156, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup3;
            this.layoutControl2.Size = new System.Drawing.Size(669, 148);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnRemoveBroker
            // 
            this.btnRemoveBroker.ImageIndex = 1;
            this.btnRemoveBroker.ImageList = this.imageList16;
            this.btnRemoveBroker.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveBroker.Location = new System.Drawing.Point(637, 36);
            this.btnRemoveBroker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRemoveBroker.Name = "btnRemoveBroker";
            this.btnRemoveBroker.Size = new System.Drawing.Size(30, 30);
            this.btnRemoveBroker.StyleController = this.layoutControl2;
            this.btnRemoveBroker.TabIndex = 11;
            this.btnRemoveBroker.Click += new System.EventHandler(this.btnRemoveBroker_Click);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "addItem16x16.ico");
            this.imageList16.Images.SetKeyName(1, "clear16x16.ico");
            // 
            // btnAddBroker
            // 
            this.btnAddBroker.ImageIndex = 0;
            this.btnAddBroker.ImageList = this.imageList16;
            this.btnAddBroker.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddBroker.Location = new System.Drawing.Point(637, 2);
            this.btnAddBroker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddBroker.Name = "btnAddBroker";
            this.btnAddBroker.Size = new System.Drawing.Size(30, 30);
            this.btnAddBroker.StyleController = this.layoutControl2;
            this.btnAddBroker.TabIndex = 10;
            this.btnAddBroker.Click += new System.EventHandler(this.btnAddBroker_Click);
            // 
            // lookupBrokerInfo_1
            // 
            this.lookupBrokerInfo_1.Location = new System.Drawing.Point(79, 2);
            this.lookupBrokerInfo_1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupBrokerInfo_1.Name = "lookupBrokerInfo_1";
            this.lookupBrokerInfo_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupBrokerInfo_1.Size = new System.Drawing.Size(313, 22);
            this.lookupBrokerInfo_1.StyleController = this.layoutControl2;
            this.lookupBrokerInfo_1.TabIndex = 9;
            this.lookupBrokerInfo_1.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.lookupBrokerInfo_1_QueryPopUp);
            // 
            // txtBrokerCommission_1
            // 
            this.txtBrokerCommission_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBrokerCommission_1.Location = new System.Drawing.Point(473, 2);
            this.txtBrokerCommission_1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBrokerCommission_1.Name = "txtBrokerCommission_1";
            this.txtBrokerCommission_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtBrokerCommission_1.Properties.Mask.EditMask = "P2";
            this.txtBrokerCommission_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtBrokerCommission_1.Properties.MaxLength = 6;
            this.txtBrokerCommission_1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtBrokerCommission_1.Size = new System.Drawing.Size(160, 22);
            this.txtBrokerCommission_1.StyleController = this.layoutControl2;
            this.txtBrokerCommission_1.TabIndex = 7;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.False;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBrokers,
            this.layoutItemAddBroker,
            this.layoutItemRemoveBroker});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(669, 148);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutBrokers
            // 
            this.layoutBrokers.CustomizationFormText = "layoutBrokers";
            this.layoutBrokers.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.False;
            this.layoutBrokers.GroupBordersVisible = false;
            this.layoutBrokers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBroker_1});
            this.layoutBrokers.Location = new System.Drawing.Point(0, 0);
            this.layoutBrokers.Name = "layoutBrokers";
            this.layoutBrokers.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutBrokers.Size = new System.Drawing.Size(635, 148);
            this.layoutBrokers.Text = "layoutBrokers";
            this.layoutBrokers.TextVisible = false;
            // 
            // layoutBroker_1
            // 
            this.layoutBroker_1.CustomizationFormText = "layoutBroker";
            this.layoutBroker_1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.False;
            this.layoutBroker_1.GroupBordersVisible = false;
            this.layoutBroker_1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBrokerCommission_1,
            this.layoutBrokerInfo_1});
            this.layoutBroker_1.Location = new System.Drawing.Point(0, 0);
            this.layoutBroker_1.Name = "layoutBroker_1";
            this.layoutBroker_1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutBroker_1.Size = new System.Drawing.Size(635, 148);
            this.layoutBroker_1.Tag = "1";
            this.layoutBroker_1.Text = "layoutBroker_1";
            this.layoutBroker_1.TextVisible = false;
            // 
            // layoutBrokerCommission_1
            // 
            this.layoutBrokerCommission_1.Control = this.txtBrokerCommission_1;
            this.layoutBrokerCommission_1.CustomizationFormText = "Commission(%):";
            this.layoutBrokerCommission_1.Location = new System.Drawing.Point(394, 0);
            this.layoutBrokerCommission_1.Name = "layoutBrokerCommission_1";
            this.layoutBrokerCommission_1.Size = new System.Drawing.Size(241, 148);
            this.layoutBrokerCommission_1.Text = "Commission:";
            this.layoutBrokerCommission_1.TextSize = new System.Drawing.Size(74, 16);
            // 
            // layoutBrokerInfo_1
            // 
            this.layoutBrokerInfo_1.Control = this.lookupBrokerInfo_1;
            this.layoutBrokerInfo_1.CustomizationFormText = "Broker:";
            this.layoutBrokerInfo_1.Location = new System.Drawing.Point(0, 0);
            this.layoutBrokerInfo_1.Name = "layoutBrokerInfo_1";
            this.layoutBrokerInfo_1.Size = new System.Drawing.Size(394, 148);
            this.layoutBrokerInfo_1.Text = "Broker:";
            this.layoutBrokerInfo_1.TextSize = new System.Drawing.Size(74, 16);
            // 
            // layoutItemAddBroker
            // 
            this.layoutItemAddBroker.Control = this.btnAddBroker;
            this.layoutItemAddBroker.CustomizationFormText = "layoutItemAddBroker";
            this.layoutItemAddBroker.Location = new System.Drawing.Point(635, 0);
            this.layoutItemAddBroker.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutItemAddBroker.MinSize = new System.Drawing.Size(34, 34);
            this.layoutItemAddBroker.Name = "layoutItemAddBroker";
            this.layoutItemAddBroker.Size = new System.Drawing.Size(34, 34);
            this.layoutItemAddBroker.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemAddBroker.Text = "layoutItemAddBroker";
            this.layoutItemAddBroker.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemAddBroker.TextToControlDistance = 0;
            this.layoutItemAddBroker.TextVisible = false;
            // 
            // layoutItemRemoveBroker
            // 
            this.layoutItemRemoveBroker.Control = this.btnRemoveBroker;
            this.layoutItemRemoveBroker.CustomizationFormText = "layoutItemRemoveBroker";
            this.layoutItemRemoveBroker.Location = new System.Drawing.Point(635, 34);
            this.layoutItemRemoveBroker.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutItemRemoveBroker.MinSize = new System.Drawing.Size(34, 34);
            this.layoutItemRemoveBroker.Name = "layoutItemRemoveBroker";
            this.layoutItemRemoveBroker.Size = new System.Drawing.Size(34, 114);
            this.layoutItemRemoveBroker.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemRemoveBroker.Text = "layoutItemRemoveBroker";
            this.layoutItemRemoveBroker.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemRemoveBroker.TextToControlDistance = 0;
            this.layoutItemRemoveBroker.TextVisible = false;
            // 
            // dtpSignDate
            // 
            this.dtpSignDate.EditValue = null;
            this.dtpSignDate.Location = new System.Drawing.Point(1024, 53);
            this.dtpSignDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpSignDate.Name = "dtpSignDate";
            this.dtpSignDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpSignDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpSignDate.Size = new System.Drawing.Size(108, 22);
            this.dtpSignDate.StyleController = this.layoutControl1;
            this.dtpSignDate.TabIndex = 6;
            this.dtpSignDate.DateTimeChanged += new System.EventHandler(this.dtpSignDate_DateTimeChanged);
            // 
            // layoutRoot
            // 
            this.layoutRoot.CustomizationFormText = "layoutRoot";
            this.layoutRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRoot.GroupBordersVisible = false;
            this.layoutRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGeneral,
            this.layoutPortfolio,
            this.layoutMarkToMarket,
            this.layoutComments,
            this.layoutTradeExtraInfo});
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRoot.Size = new System.Drawing.Size(1137, 710);
            this.layoutRoot.Text = "layoutRoot";
            this.layoutRoot.TextVisible = false;
            // 
            // layoutGeneral
            // 
            this.layoutGeneral.CustomizationFormText = "General";
            this.layoutGeneral.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutGeneral.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutId,
            this.layoutSignDate,
            this.layoutStrategySet,
            this.layoutCompany,
            this.layoutCounterparty,
            this.layoutTrader,
            this.layoutCode,
            this.layoutExternalCode,
            this.layoutVentureShare,
            this.layoutState,
            this.layoutItemTradeVersions,
            this.layoutIsJointVenture,
            this.layoutControlGroup1,
            this.lcgBrokers});
            this.layoutGeneral.Location = new System.Drawing.Point(0, 0);
            this.layoutGeneral.Name = "layoutGeneral";
            this.layoutGeneral.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGeneral.Size = new System.Drawing.Size(1137, 302);
            this.layoutGeneral.Text = "General";
            // 
            // layoutId
            // 
            this.layoutId.Control = this.lblId;
            this.layoutId.CustomizationFormText = "Trade Id:";
            this.layoutId.Location = new System.Drawing.Point(0, 0);
            this.layoutId.Name = "layoutId";
            this.layoutId.Size = new System.Drawing.Size(40, 26);
            this.layoutId.Text = "Id:";
            this.layoutId.TextSize = new System.Drawing.Size(16, 16);
            // 
            // layoutSignDate
            // 
            this.layoutSignDate.Control = this.dtpSignDate;
            this.layoutSignDate.CustomizationFormText = "Sign Date:";
            this.layoutSignDate.Location = new System.Drawing.Point(956, 26);
            this.layoutSignDate.Name = "layoutSignDate";
            this.layoutSignDate.Size = new System.Drawing.Size(175, 26);
            this.layoutSignDate.Text = "Sign Date:";
            this.layoutSignDate.TextSize = new System.Drawing.Size(60, 16);
            // 
            // layoutStrategySet
            // 
            this.layoutStrategySet.Control = this.txtStrategySet;
            this.layoutStrategySet.CustomizationFormText = "Hedging/Strategy Set:";
            this.layoutStrategySet.Location = new System.Drawing.Point(324, 52);
            this.layoutStrategySet.Name = "layoutStrategySet";
            this.layoutStrategySet.Size = new System.Drawing.Size(507, 26);
            this.layoutStrategySet.Text = "Strategy Set:";
            this.layoutStrategySet.TextSize = new System.Drawing.Size(76, 16);
            // 
            // layoutCompany
            // 
            this.layoutCompany.Control = this.lookupCompany;
            this.layoutCompany.CustomizationFormText = "Company:";
            this.layoutCompany.Location = new System.Drawing.Point(0, 26);
            this.layoutCompany.Name = "layoutCompany";
            this.layoutCompany.Size = new System.Drawing.Size(486, 26);
            this.layoutCompany.Text = "Company:";
            this.layoutCompany.TextSize = new System.Drawing.Size(58, 16);
            // 
            // layoutCounterparty
            // 
            this.layoutCounterparty.Control = this.lookupCounterparty;
            this.layoutCounterparty.CustomizationFormText = "Counterparty:";
            this.layoutCounterparty.Location = new System.Drawing.Point(486, 26);
            this.layoutCounterparty.Name = "layoutCounterparty";
            this.layoutCounterparty.Size = new System.Drawing.Size(470, 26);
            this.layoutCounterparty.Text = "Counterparty:";
            this.layoutCounterparty.TextSize = new System.Drawing.Size(79, 16);
            // 
            // layoutTrader
            // 
            this.layoutTrader.Control = this.lookupTrader;
            this.layoutTrader.CustomizationFormText = "Trader:";
            this.layoutTrader.Location = new System.Drawing.Point(0, 52);
            this.layoutTrader.Name = "layoutTrader";
            this.layoutTrader.Size = new System.Drawing.Size(324, 26);
            this.layoutTrader.Text = "Trader:";
            this.layoutTrader.TextSize = new System.Drawing.Size(44, 16);
            // 
            // layoutCode
            // 
            this.layoutCode.Control = this.lblCode;
            this.layoutCode.CustomizationFormText = "Code:";
            this.layoutCode.Location = new System.Drawing.Point(40, 0);
            this.layoutCode.Name = "layoutCode";
            this.layoutCode.Size = new System.Drawing.Size(82, 26);
            this.layoutCode.Text = "Code:";
            this.layoutCode.TextSize = new System.Drawing.Size(34, 16);
            // 
            // layoutExternalCode
            // 
            this.layoutExternalCode.Control = this.txtExternalCode;
            this.layoutExternalCode.CustomizationFormText = "External Code:";
            this.layoutExternalCode.Location = new System.Drawing.Point(350, 0);
            this.layoutExternalCode.Name = "layoutExternalCode";
            this.layoutExternalCode.Size = new System.Drawing.Size(357, 26);
            this.layoutExternalCode.Text = "External Code:";
            this.layoutExternalCode.TextSize = new System.Drawing.Size(84, 16);
            // 
            // layoutVentureShare
            // 
            this.layoutVentureShare.Control = this.txtVentureShare;
            this.layoutVentureShare.CustomizationFormText = "Venture Share:";
            this.layoutVentureShare.Location = new System.Drawing.Point(956, 52);
            this.layoutVentureShare.Name = "layoutVentureShare";
            this.layoutVentureShare.Size = new System.Drawing.Size(175, 26);
            this.layoutVentureShare.Text = "Venture Share:";
            this.layoutVentureShare.TextSize = new System.Drawing.Size(88, 16);
            // 
            // layoutState
            // 
            this.layoutState.Control = this.cmbState;
            this.layoutState.CustomizationFormText = "State:";
            this.layoutState.Location = new System.Drawing.Point(122, 0);
            this.layoutState.Name = "layoutState";
            this.layoutState.Size = new System.Drawing.Size(228, 26);
            this.layoutState.Text = "State:";
            this.layoutState.TextSize = new System.Drawing.Size(35, 16);
            // 
            // layoutItemTradeVersions
            // 
            this.layoutItemTradeVersions.Control = this.lookUpVersions;
            this.layoutItemTradeVersions.CustomizationFormText = "Versions:";
            this.layoutItemTradeVersions.Location = new System.Drawing.Point(707, 0);
            this.layoutItemTradeVersions.Name = "layoutItemTradeVersions";
            this.layoutItemTradeVersions.Size = new System.Drawing.Size(424, 26);
            this.layoutItemTradeVersions.Text = "Versions:";
            this.layoutItemTradeVersions.TextSize = new System.Drawing.Size(54, 16);
            // 
            // layoutIsJointVenture
            // 
            this.layoutIsJointVenture.Control = this.chkIsJointVenture;
            this.layoutIsJointVenture.CustomizationFormText = "Joint Venture:";
            this.layoutIsJointVenture.Location = new System.Drawing.Point(831, 52);
            this.layoutIsJointVenture.Name = "layoutIsJointVenture";
            this.layoutIsJointVenture.Size = new System.Drawing.Size(125, 26);
            this.layoutIsJointVenture.Text = "Joint Venture:";
            this.layoutIsJointVenture.TextSize = new System.Drawing.Size(80, 16);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "lcgBooks";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBook});
            this.layoutControlGroup1.Location = new System.Drawing.Point(679, 78);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(452, 196);
            this.layoutControlGroup1.Text = "Books";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutBook
            // 
            this.layoutBook.Control = this.treeBook;
            this.layoutBook.CustomizationFormText = "Book:";
            this.layoutBook.Location = new System.Drawing.Point(0, 0);
            this.layoutBook.Name = "layoutBook";
            this.layoutBook.Size = new System.Drawing.Size(446, 190);
            this.layoutBook.Text = "Book:";
            this.layoutBook.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutBook.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutBook.TextSize = new System.Drawing.Size(32, 16);
            this.layoutBook.TextToControlDistance = 5;
            // 
            // lcgBrokers
            // 
            this.lcgBrokers.CustomizationFormText = "lcgBrokers";
            this.lcgBrokers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemBrokerCommissionInfo});
            this.lcgBrokers.Location = new System.Drawing.Point(0, 78);
            this.lcgBrokers.Name = "lcgBrokers";
            this.lcgBrokers.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgBrokers.Size = new System.Drawing.Size(679, 196);
            this.lcgBrokers.Text = "lcgBrokers";
            this.lcgBrokers.TextVisible = false;
            // 
            // layoutItemBrokerCommissionInfo
            // 
            this.layoutItemBrokerCommissionInfo.Control = this.panelControl1;
            this.layoutItemBrokerCommissionInfo.CustomizationFormText = "layoutControlItem4";
            this.layoutItemBrokerCommissionInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutItemBrokerCommissionInfo.Name = "layoutItemBrokerCommissionInfo";
            this.layoutItemBrokerCommissionInfo.Size = new System.Drawing.Size(673, 190);
            this.layoutItemBrokerCommissionInfo.Text = "Broker/Commission:";
            this.layoutItemBrokerCommissionInfo.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutItemBrokerCommissionInfo.TextSize = new System.Drawing.Size(116, 16);
            // 
            // layoutPortfolio
            // 
            this.layoutPortfolio.CustomizationFormText = "Portfolio";
            this.layoutPortfolio.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutMarketSegment,
            this.layoutMarketRegion,
            this.layoutMarketRoute});
            this.layoutPortfolio.Location = new System.Drawing.Point(0, 302);
            this.layoutPortfolio.Name = "layoutPortfolio";
            this.layoutPortfolio.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutPortfolio.Size = new System.Drawing.Size(1137, 54);
            this.layoutPortfolio.Text = "Portfolio";
            // 
            // layoutMarketSegment
            // 
            this.layoutMarketSegment.Control = this.lookupMarketSegment;
            this.layoutMarketSegment.CustomizationFormText = "Market:";
            this.layoutMarketSegment.Location = new System.Drawing.Point(0, 0);
            this.layoutMarketSegment.Name = "layoutMarketSegment";
            this.layoutMarketSegment.Size = new System.Drawing.Size(230, 26);
            this.layoutMarketSegment.Text = "Market:";
            this.layoutMarketSegment.TextSize = new System.Drawing.Size(44, 16);
            // 
            // layoutMarketRegion
            // 
            this.layoutMarketRegion.Control = this.lookupMarketRegion;
            this.layoutMarketRegion.CustomizationFormText = "Trade Region:";
            this.layoutMarketRegion.Location = new System.Drawing.Point(230, 0);
            this.layoutMarketRegion.Name = "layoutMarketRegion";
            this.layoutMarketRegion.Size = new System.Drawing.Size(449, 26);
            this.layoutMarketRegion.Text = "Trade Region:";
            this.layoutMarketRegion.TextSize = new System.Drawing.Size(82, 16);
            // 
            // layoutMarketRoute
            // 
            this.layoutMarketRoute.Control = this.lookupMarketRoute;
            this.layoutMarketRoute.CustomizationFormText = "Route:";
            this.layoutMarketRoute.Location = new System.Drawing.Point(679, 0);
            this.layoutMarketRoute.Name = "layoutMarketRoute";
            this.layoutMarketRoute.Size = new System.Drawing.Size(452, 26);
            this.layoutMarketRoute.Text = "Route:";
            this.layoutMarketRoute.TextSize = new System.Drawing.Size(38, 16);
            // 
            // layoutMarkToMarket
            // 
            this.layoutMarkToMarket.CustomizationFormText = "Mark-To-Market";
            this.layoutMarkToMarket.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutForwardCurve,
            this.layoutStressTestingCurve});
            this.layoutMarkToMarket.Location = new System.Drawing.Point(0, 606);
            this.layoutMarkToMarket.Name = "layoutMarkToMarket";
            this.layoutMarkToMarket.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutMarkToMarket.Size = new System.Drawing.Size(524, 104);
            this.layoutMarkToMarket.Text = "Mark-To-Market";
            // 
            // layoutForwardCurve
            // 
            this.layoutForwardCurve.Control = this.lookupMTMForwardCurve;
            this.layoutForwardCurve.CustomizationFormText = "Forward Curve:";
            this.layoutForwardCurve.Location = new System.Drawing.Point(0, 0);
            this.layoutForwardCurve.Name = "layoutForwardCurve";
            this.layoutForwardCurve.Size = new System.Drawing.Size(518, 26);
            this.layoutForwardCurve.Text = "Forward Curve:";
            this.layoutForwardCurve.TextSize = new System.Drawing.Size(90, 16);
            // 
            // layoutStressTestingCurve
            // 
            this.layoutStressTestingCurve.Control = this.lookupMTMStressCurve;
            this.layoutStressTestingCurve.CustomizationFormText = "Stress Testing Curve:";
            this.layoutStressTestingCurve.Location = new System.Drawing.Point(0, 26);
            this.layoutStressTestingCurve.Name = "layoutStressTestingCurve";
            this.layoutStressTestingCurve.Size = new System.Drawing.Size(518, 50);
            this.layoutStressTestingCurve.Text = "Stress Testing Curve:";
            this.layoutStressTestingCurve.TextSize = new System.Drawing.Size(124, 16);
            // 
            // layoutComments
            // 
            this.layoutComments.Control = this.txtComments;
            this.layoutComments.CustomizationFormText = "Comments:";
            this.layoutComments.Location = new System.Drawing.Point(524, 606);
            this.layoutComments.MaxSize = new System.Drawing.Size(0, 104);
            this.layoutComments.MinSize = new System.Drawing.Size(1, 104);
            this.layoutComments.Name = "layoutComments";
            this.layoutComments.Size = new System.Drawing.Size(613, 104);
            this.layoutComments.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutComments.Text = "Comments:";
            this.layoutComments.TextSize = new System.Drawing.Size(66, 16);
            // 
            // layoutTradeExtraInfo
            // 
            this.layoutTradeExtraInfo.CustomizationFormText = "Extra Information";
            this.layoutTradeExtraInfo.Location = new System.Drawing.Point(0, 356);
            this.layoutTradeExtraInfo.Name = "layoutTradeExtraInfo";
            this.layoutTradeExtraInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutTradeExtraInfo.Size = new System.Drawing.Size(1137, 250);
            this.layoutTradeExtraInfo.Text = "Extra Information";
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblId;
            this.layoutControlItem1.CustomizationFormText = "Trade Id:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutTradeId";
            this.layoutControlItem1.Size = new System.Drawing.Size(107, 20);
            this.layoutControlItem1.Text = "Trade Id:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(45, 13);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSave,
            this.btnClose});
            this.barManager.LargeImages = this.imageList24;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnSave
            // 
            this.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnSave.Caption = "Save";
            this.btnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnSave.Id = 0;
            this.btnSave.LargeImageIndex = 4;
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 1;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_Click);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1137, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 710);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1137, 38);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 710);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1137, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 710);
            // 
            // AddEditViewTradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1137, 748);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AddEditViewTradeForm";
            this.Activated += new System.EventHandler(this.AddEditViewTradeForm_Activated);
            this.Deactivate += new System.EventHandler(this.AddEditViewTradeForm_Deactivate);
            this.Load += new System.EventHandler(this.AddEditViewTradeControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpVersions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVentureShare.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsJointVenture.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExternalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMTMStressCurve.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMTMForwardCurve.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarketRoute.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarketRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupMarketSegment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupTrader.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCounterparty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStrategySet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookupBrokerInfo_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrokerCommission_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBrokers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBroker_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBrokerCommission_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBrokerInfo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemAddBroker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemRemoveBroker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpSignDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSignDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStrategySet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCounterparty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExternalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVentureShare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeVersions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutIsJointVenture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBrokers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemBrokerCommissionInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPortfolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSegment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarkToMarket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutForwardCurve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutStressTestingCurve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTradeExtraInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGeneral;
        private DevExpress.XtraEditors.DateEdit dtpSignDate;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SpinEdit txtBrokerCommission_1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutBrokers;
        private DevExpress.XtraLayout.LayoutControlGroup layoutBroker_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBrokerCommission_1;
        private DevExpress.XtraTreeList.TreeList treeBook;
        private DevExpress.XtraEditors.TextEdit txtStrategySet;
        private DevExpress.XtraEditors.LabelControl lblId;
        private DevExpress.XtraLayout.LayoutControlItem layoutSignDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutBook;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemBrokerCommissionInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutStrategySet;
        private DevExpress.XtraLayout.LayoutControlItem layoutId;
        private DevExpress.XtraLayout.LayoutControlGroup layoutPortfolio;
        private DevExpress.XtraLayout.LayoutControlGroup layoutMarkToMarket;
        private DevExpress.XtraEditors.MemoEdit txtComments;
        private DevExpress.XtraLayout.LayoutControlItem layoutComments;
        private DevExpress.XtraLayout.LayoutControlGroup layoutTradeExtraInfo;
        private DevExpress.XtraEditors.LookUpEdit lookupCompany;
        private DevExpress.XtraLayout.LayoutControlItem layoutCompany;
        private DevExpress.XtraEditors.LookUpEdit lookupCounterparty;
        private DevExpress.XtraLayout.LayoutControlItem layoutCounterparty;
        private DevExpress.XtraEditors.LookUpEdit lookupTrader;
        private DevExpress.XtraLayout.LayoutControlItem layoutTrader;
        private DevExpress.XtraEditors.LookUpEdit lookupBrokerInfo_1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBrokerInfo_1;
        private DevExpress.XtraEditors.LookUpEdit lookupMarketRegion;
        private DevExpress.XtraEditors.LookUpEdit lookupMarketSegment;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSegment;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketRegion;
        private DevExpress.XtraEditors.LookUpEdit lookupMarketRoute;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketRoute;
        private DevExpress.XtraEditors.LookUpEdit lookupMTMForwardCurve;
        private DevExpress.XtraLayout.LayoutControlItem layoutForwardCurve;
        private DevExpress.XtraEditors.LookUpEdit lookupMTMStressCurve;
        private DevExpress.XtraLayout.LayoutControlItem layoutStressTestingCurve;
        private DevExpress.XtraEditors.SimpleButton btnAddBroker;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemAddBroker;
        private DevExpress.XtraEditors.SimpleButton btnRemoveBroker;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemRemoveBroker;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.LabelControl lblCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtExternalCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutExternalCode;
        private DevExpress.XtraEditors.CheckEdit chkIsJointVenture;
        private DevExpress.XtraLayout.LayoutControlItem layoutIsJointVenture;
        private DevExpress.XtraEditors.SpinEdit txtVentureShare;
        private DevExpress.XtraLayout.LayoutControlItem layoutVentureShare;
        private DevExpress.XtraEditors.ComboBoxEdit cmbState;
        private DevExpress.XtraLayout.LayoutControlItem layoutState;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.LookUpEdit lookUpVersions;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTradeVersions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgBrokers;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}
