﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.IO;
using System.Globalization;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPrinting;
using DTG.Spreadsheet;
using Exis.Domain;
using Exis.WinClient.SpecialForms;
using DevExpress.XtraPrintingLinks;
//using Excel;
using ExcelDataReader;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Exis.WinClient.General;

namespace Exis.WinClient.Controls.Core
{
    public partial class InitialMarginNasdaqControl : XtraUserControl
    {
        #region Private Properties

        private const string licenceCode = "SOZT-CSWE-M2SN-CXMT";

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm1;
        private WaitDialogForm _dialogForm2;
        private List<decimal> _cases = new List<decimal>();
        private Dictionary<string, decimal[]> _worstscenarios = new Dictionary<string, decimal[]>();

        private readonly ViewTradeGeneralInfo _viewTradesGeneralInfo;
        private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<DateTime, decimal>> _indexPositionSummaryValues;
        Dictionary<string, decimal> _dailyfixes = new Dictionary<string, decimal>();
        List<Correlation> corlist = new List<Correlation>();
        List<List<int>> startendList = new List<List<int>>();
        List<tradelist> resultList = new List<tradelist>();
        private bool[] hasoption;


        #region SpanCodes
        private readonly Dictionary<string, string> _spanCodesNasdaq = new Dictionary<string, string>
        {
            {"P", "PTOO"},
            {"S", "STOO"},
            {"C", "CTOO"},
            {"H", "HTOO"}
        };
        #endregion

        #region MapCodesNasdaq
        private readonly Dictionary<string, string> _mapCodesNasdaq = new Dictionary<string, string>
        {
            {"C3E", "C3_AVG"},
            {"C4" ,  "C4"},
            {"C4E", "C4_AVG"},
            {"C5E", "C5_AVG"},
            {"C7" ,  "C7"},
            {"C7E", "C7_AVG"},
            {"CTC", "CS5TC"},
            {"CPT", "CS5TC"},
            {"HTC", "HS6TC"},
            {"P1A", "P1A"},
            {"P1E", "P1A_AVG"},
            {"P2A", "P2A"},
            {"P2E", "P2A_AVG"},
            {"P3A", "P3A"},
            {"P3E", "P3A_AVG"},
            {"PTC", "PM4TC"},
            {"S7" ,  "S7"},
            {"STC", "SM6TC"},
            {"CTO", "CS4TC"},
            {"CPO", "CS5TC"},
            {"HTO", "HS6TC"},
            {"PTO", "PM4TC"},
            {"STO", "SM6TC"},
            {"SPT", "SM10TC"},
            {"SPO", "SM10TC"}
        };






        #endregion

        #region InitialMarginCodes
        private readonly Dictionary<string, string> _initMarginCodesNasdaq = new Dictionary<string, string>
        {
            {"P", "PTC"},
            {"S", "STC"},
            {"C", "CTC"},
            {"H", "HTC"}
        };
        #endregion

        private List<TmpScanningRangeNasdaq> _tmpScanningRatesNasdaq;
        private List<TmpIntercommodityCreditsNasdaq> _tmpIntercommCreditsNasdaq;
        private List<TmpEnclearNasdaq> _tmpEnclearsNasdaq;
        private List<volcor> _VolCorrList;
        private List<string> _availableCodes;
        private List<TradeInformation> _availableTrades;
        private List<DateTime> _dates;
        private Dictionary<string, Dictionary<DateTime, decimal>> _indexPosValuesNasdaq;
        private List<OptionPositionValuesNasdaq> _optPosValuesNasdaq;
        private List<DeltaValuesNasdaq> _deltaNasdaq;
        private List<RiskArrayParameterNasdaq> _rAparamNasdaq;
        private List<ContractNasdaq> _contractsNasdaq;
        private List<ContractDailyFix> _condaily;
        private List<InstrumentNasdaq> _instrumentNasdaq = new List<InstrumentNasdaq>();

        private Dictionary<string, decimal> _scanningRanges;
        private Dictionary<string, decimal[]> _riskScenarios;
        private Dictionary<string, decimal> _intermonthSpreadCharge;
        private Dictionary<string, decimal> _spotCharge;
        private Dictionary<string, decimal> _interCommoditySpreadCredit;

        private List<InitialMarginNasdaq> _initialMarginNasdaq;

        private bool imTabLoaded;
        private bool _optionParametersLoaded;

        #endregion

        #region Public Properties

        #endregion

        #region Constructors

        public InitialMarginNasdaqControl(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            _viewTradesGeneralInfo = viewTradesGeneralInfo;
            InitializeComponent();
            InitializeControl();
            //InitializeData(tradePositionResults);
        }

        #endregion

        #region Proxy
        #endregion

        #region Gui Events

        private void beDATfile_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var openFileDialogDat = new OpenFileDialog
            {
                Filter = @"Span files (*.txt)|*.txt",
                RestoreDirectory = true,
                Title = Strings.Open_File
            };
            if (openFileDialogDat.ShowDialog() == DialogResult.OK)
            {
                ((ButtonEdit)sender).EditValue = openFileDialogDat.FileName;
                GeneralSettings.Default.DefaultPosOptionParameterFile = openFileDialogDat.FileName.Trim();
                GeneralSettings.Default.Save();
            }
        }


        #endregion

        #region Nasdaq
        List<tradelist> listA = new List<tradelist>();

        private void loadCorrVol()
        {
            //string filePath = "freight.xlsx";
            string filePath = string.Empty;
            string fileExt = string.Empty;

            IExcelDataReader excelReader = null;
            if (beSelectExcel.Text == string.Empty || beSelectExcel.EditValue == null)
            {
                XtraMessageBox.Show(this,
                   "Please insert the Excel File.",
                   Strings.Freight_Metrics,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                OpenFileDialog file = new OpenFileDialog();
                file.InitialDirectory = @"c:\FreightMetrics\Data";
                if (file.ShowDialog() == System.Windows.Forms.DialogResult.OK) //if there is a file choosen by the user  
                {
                    filePath = file.FileName; //get the path of the file  
                    fileExt = Path.GetExtension(filePath); //get the file extension  
                }
                //return;
                //TODO: use shared path on server with setting in DB to access downloaded files.            
            }
            else { filePath = beSelectExcel.Text; fileExt = Path.GetExtension(filePath); }
            //open dialog to choose file

            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            if (fileExt.CompareTo(".xlsx") == 0)
            { /*IExcelDataReader*/ excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream); }
            else if (fileExt.CompareTo(".xls") == 0)
            { /*IExcelDataReader*/ excelReader = ExcelReaderFactory.CreateBinaryReader(stream); }
            else
            {
                MessageBox.Show("Please choose .xls or .xlsx file only.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error); //custom messageBox to show error  
            }

            _VolCorrList = new List<volcor>();
            //excelReader.IsFirstRowAsColumnNames = true;
            DataSet result = excelReader.AsDataSet();

            foreach (DataTable table in result.Tables)
            {
                var excelsheetname = table.TableName.ToString();
                int start = excelsheetname.IndexOf("(") + 1;
                int last = excelsheetname.LastIndexOf(")");
                var cor_index = excelsheetname.Substring(start, last - start);
                var t_type = excelsheetname.Contains("Volatility") ? "Volatility" : "Correlation";
                var vc = new volcor
                {
                    nindex = cor_index,
                    table_type = t_type,
                    NasdaqVolCorrTable = table
                };

                _VolCorrList.Add(vc);
            };
            excelReader.Close();
        }




        private tradelist LoadSpanfile2(string filePath, string pattern)
        {

            string tradeprice = "";
            string traderisk = "";
            string tradeticker = "";

            var value = new tradelist();





            using (var reader = new StreamReader(filePath))
            {

                while (!reader.EndOfStream)
                {



                    var line = reader.ReadLine();
                    var contractid = string.Empty;
                    bool iscontained = false;

                    if (line.Contains(pattern))
                    {
                        if (line.Substring(0, 4) == "0201")
                        {
                            tradeticker = line.Substring(18, 24);
                            traderisk = line.Substring(42, 6);
                            value.ticker = tradeticker.Trim();
                            value.risk_group = traderisk.Trim();
                            value.contractid = line.Substring(6, 12);
                            line = reader.ReadLine();
                            if (line != null) tradeprice = line.Substring(21, 12);
                            value.price = tradeprice.Trim();
                            listA.Add(value);
                            tradeticker = "";
                            traderisk = "";
                            tradeprice = "";
                        }
                    }


                    if (line.Substring(0, 4) == "0301" && line.Substring(6, 12) == value.contractid)
                    {


                        if (value != null)
                        {
                            value.risk.val1 = line.Substring(48, 9);
                            value.risk.val2 = line.Substring(57, 9);
                            value.risk.val3 = line.Substring(66, 9);
                            value.risk.val4 = line.Substring(75, 5);
                            line = reader.ReadLine();
                            if (!string.IsNullOrEmpty(line))
                            {
                                value.risk.val4 += line.Substring(6, 4);
                                value.risk.val5 = line.Substring(10, 9);
                                value.risk.val6 = line.Substring(19, 9);
                                value.risk.val7 = line.Substring(28, 9);
                                value.risk.val8 = line.Substring(37, 9);
                                value.risk.val9 = line.Substring(46, 9);
                                value.risk.val10 = line.Substring(55, 9);
                                value.risk.val11 = line.Substring(64, 9);
                                value.risk.val12 = line.Substring(73, 7);
                                line = reader.ReadLine();
                                if (!string.IsNullOrEmpty(line))
                                {
                                    value.risk.val12 += line.Substring(6, 2);
                                    value.risk.val13 = line.Substring(8, 9);
                                    value.risk.val14 = line.Substring(17, 9);
                                    value.risk.val15 = line.Substring(26, 9);
                                    value.risk.val16 = line.Substring(35, 9);
                                    value.risk.delta = line.Substring(44, 10);
                                    value.risk.dvc = line.Substring(54, 1);
                                }
                            }
                        }
                    }

                }
            }
            return value;

        }



        private void LoadSpanfile(string filePath)
        {

            string tradeprice = "";
            string traderisk = "";
            string tradeticker = "";







            using (var reader = new StreamReader(filePath))
            {

                while (!reader.EndOfStream)
                {
                    var value = new tradelist();
                    var line = reader.ReadLine();
                    var contractid = string.Empty;



                    if (line.Substring(0, 4) == "0101")
                    {
                        Correlation cor = new Correlation();
                        string key = line.Substring(6, 6);
                        string val = line.Substring(16, 4);
                        cor.key = key.Trim();
                        cor.tablename = val;
                        corlist.Add(cor);
                    }





                    if (line.Substring(0, 4) == "0201")
                    {
                        tradeticker = line.Substring(18, 24);
                        traderisk = line.Substring(42, 6);
                        value.ticker = tradeticker.Trim();
                        value.risk_group = traderisk.Trim();
                        value.contractid = line.Substring(6, 12);
                        line = reader.ReadLine();
                        if (line != null) tradeprice = line.Substring(21, 12);
                        value.price = tradeprice.Trim();
                        listA.Add(value);
                        tradeticker = "";
                        traderisk = "";
                        tradeprice = "";
                    }




                    if (line.Substring(0, 4) == "0301")
                    {
                        string cntid = string.Empty;
                        cntid = line.Substring(6, 12);
                        tradelist test = listA.First(n => n.contractid == cntid);
                        if (test != null)
                        {
                            test.risk.val1 = line.Substring(48, 9);
                            test.risk.val2 = line.Substring(57, 9);
                            test.risk.val3 = line.Substring(66, 9);
                            test.risk.val4 = line.Substring(75, 5);
                            line = reader.ReadLine();
                            if (!string.IsNullOrEmpty(line))
                            {
                                test.risk.val4 += line.Substring(6, 4);
                                test.risk.val5 = line.Substring(10, 9);
                                test.risk.val6 = line.Substring(19, 9);
                                test.risk.val7 = line.Substring(28, 9);
                                test.risk.val8 = line.Substring(37, 9);
                                test.risk.val9 = line.Substring(46, 9);
                                test.risk.val10 = line.Substring(55, 9);
                                test.risk.val11 = line.Substring(64, 9);
                                test.risk.val12 = line.Substring(73, 7);
                                line = reader.ReadLine();
                                if (!string.IsNullOrEmpty(line))
                                {
                                    test.risk.val12 += line.Substring(6, 2);
                                    test.risk.val13 = line.Substring(8, 9);
                                    test.risk.val14 = line.Substring(17, 9);
                                    test.risk.val15 = line.Substring(26, 9);
                                    test.risk.val16 = line.Substring(35, 9);
                                    test.risk.delta = line.Substring(44, 10);
                                    test.risk.dvc = line.Substring(54, 1);
                                }
                            }
                        }


                    }


                    if (line.Substring(0, 4) == "0401")
                    {
                        string tablename = line.Substring(6, 4);
                        int x = 0;
                        int y = 0;
                        decimal fval = 0;
                        string xval = line.Substring(11, 4);
                        string yval = line.Substring(16, 4);
                        string val = line.Substring(21, 4);
                        int.TryParse(xval, out x);
                        int.TryParse(yval, out y);
                        decimal.TryParse(val, out fval);
                        fval = fval / 1000;
                        if (corlist.FirstOrDefault(n => n.tablename == tablename) != null)
                        {
                            corlist.FirstOrDefault(n => n.tablename == tablename).AddVal(x, y, fval);
                            corlist.FirstOrDefault(n => n.tablename == tablename).key =
                                corlist.FirstOrDefault(n => n.tablename == tablename).key;
                        }

                    }








                }
            }
        }




        public static decimal getDecimal(string val)
        {
            try
            {
                return (decimal.Parse(val,
                    System.Globalization.NumberStyles.AllowParentheses |
                    System.Globalization.NumberStyles.AllowLeadingWhite |
                    System.Globalization.NumberStyles.AllowTrailingWhite |
                    System.Globalization.NumberStyles.AllowThousands |
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowLeadingSign));
            }
            catch (Exception ex)
            {
                return -1;
            }
        }




        private decimal getdailyfix(string instrument)
        {
            try
            {
                string dailyfix = "";

                var firstOrDefault =
                    listA.Where(c => c.ticker.Contains(instrument)).Select(c => c.price).FirstOrDefault();
                if (firstOrDefault != null)
                    dailyfix = firstOrDefault.ToString();

                var dailyFix = Convert.ToDecimal(dailyfix) / 10000;

                return dailyFix;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }



        private decimal[] getworstcase(string instrument)
        {
            string dailyfix = "";
            List<decimal> res = new List<decimal>();
            decimal dvc = 0;
            var firstOrDefault = listA.FirstOrDefault(c => c.ticker.Contains(instrument));
            if (firstOrDefault != null)
            {
                dvc = getDecimal(firstOrDefault.risk.dvc);
                res.Add(getDecimal(firstOrDefault.risk.val1));
                res.Add(getDecimal(firstOrDefault.risk.val2));
                res.Add(getDecimal(firstOrDefault.risk.val3));
                res.Add(getDecimal(firstOrDefault.risk.val4));
                res.Add(getDecimal(firstOrDefault.risk.val5));
                res.Add(getDecimal(firstOrDefault.risk.val6));
                res.Add(getDecimal(firstOrDefault.risk.val7));
                res.Add(getDecimal(firstOrDefault.risk.val8));
                res.Add(getDecimal(firstOrDefault.risk.val9));
                res.Add(getDecimal(firstOrDefault.risk.val10));
                res.Add(getDecimal(firstOrDefault.risk.val11));
                res.Add(getDecimal(firstOrDefault.risk.val12));
                res.Add(getDecimal(firstOrDefault.risk.val13));
                res.Add(getDecimal(firstOrDefault.risk.val14));
                decimal[] res1 = new decimal[2];

                res1[0] = (decimal)((double)res.Min() / Math.Pow(10, (double)dvc));

                res1[1] = (decimal)((double)res.Max() / Math.Pow(10, (double)dvc));
                return res1;
            }
            return new decimal[2] { 0, 0 };
        }






        private decimal[] getworstcase2(tradelist a)
        {
            string dailyfix = "";
            List<decimal> res = new List<decimal>();
            decimal dvc = 0;

            if (a != null)
            {
                dvc = getDecimal(a.risk.dvc);
                res.Add(getDecimal(a.risk.val1));
                res.Add(getDecimal(a.risk.val2));
                res.Add(getDecimal(a.risk.val3));
                res.Add(getDecimal(a.risk.val4));
                res.Add(getDecimal(a.risk.val5));
                res.Add(getDecimal(a.risk.val6));
                res.Add(getDecimal(a.risk.val7));
                res.Add(getDecimal(a.risk.val8));
                res.Add(getDecimal(a.risk.val9));
                res.Add(getDecimal(a.risk.val10));
                res.Add(getDecimal(a.risk.val11));
                res.Add(getDecimal(a.risk.val12));
                res.Add(getDecimal(a.risk.val13));
                res.Add(getDecimal(a.risk.val14));
                decimal[] res1 = new decimal[2];

                res1[0] = (decimal)((double)res.Min() / Math.Pow(10, (double)dvc));

                res1[1] = (decimal)((double)res.Max() / Math.Pow(10, (double)dvc));
                return res1;
            }
            return new decimal[2] { 0, 0 };
        }




        private List<decimal> getsumworstcase(List<tradelist> lista)
        {
            List<decimal> res = new List<decimal>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int i = 0;
            foreach (tradelist x in lista)
            {
                res[0] = res[0] + getDecimal(x.risk.val1)/100;
                res[1] = res[1] + getDecimal(x.risk.val2) / 100;
                res[2] = res[2] + getDecimal(x.risk.val3) / 100;
                res[3] = res[3] + getDecimal(x.risk.val4) / 100;
                res[4] = res[4] + getDecimal(x.risk.val5) / 100;
                res[5] = res[5] + getDecimal(x.risk.val6) / 100;
                res[6] = res[6] + getDecimal(x.risk.val7) / 100;
                res[7] = res[7] + getDecimal(x.risk.val8) / 100;
                res[8] = res[8] + getDecimal(x.risk.val9) / 100;
                res[9] = res[9] + getDecimal(x.risk.val10) / 100;
                res[10] = res[10] + getDecimal(x.risk.val11) / 100;
                res[11] = res[11] + getDecimal(x.risk.val12) / 100;
                res[12] = res[12] + getDecimal(x.risk.val13) / 100;
                res[13] = res[13] + getDecimal(x.risk.val14) / 100;
                res[14] = res[14] + getDecimal(x.risk.val15)/100;
                res[15] = res[15] + getDecimal(x.risk.val16) / 100;
                i++;
            }
            return res;
        }






        #endregion

        #region Public Methods

        public void LoadPositionValues(Dictionary<string, Dictionary<DateTime, decimal>> tradePositionResults)
        {
            _tradePositionResults = tradePositionResults;
            InitializeData(tradePositionResults);
            SetButtons(true);
        }



        #endregion

        #region Private Methods

        private void InitializeControl()
        {
            InitInitialMarginGrid();
            // beDATfile.EditValue = GeneralSettings.Default.DefaultPosOptionParameterFile;
            beSelectExcel.EditValue = GeneralSettings.Default.DefaultInitialMarginXls;

            SetButtons(false);
        }

        private void SetButtons(bool value)
        {
            //btnLoadOptionsParameters.Enabled = value;
            btnExportInitialMargin.Enabled = value;
            btnCalculateInitialMargin.Enabled = value;
        }

        private void InitializeData(Dictionary<string, Dictionary<DateTime, decimal>> tradePositionResults)
        {
            _availableCodes = _viewTradesGeneralInfo.tradeInformations.GroupBy(n => n.ForwardCurve)
                            .Select(n => n.First())
                            .Select(n => n.ForwardCurve)
                            .ToList();

            _availableTrades = _viewTradesGeneralInfo.tradeInformations.ToList();

            _dates = tradePositionResults.First().Value.Select(a => a.Key).ToList();

            if (_viewTradesGeneralInfo.tradeInformations.Any(n => n.Type == "FFA" || n.Type == "OPTION" || n.Type == "Put" || n.Type == "Call"))
            {
                //Position Summaries Per Index
                _indexPosValuesNasdaq = new Dictionary<string, Dictionary<DateTime, decimal>>();

                //var groups = _viewTradesGeneralInfo.tradeInformations.GroupBy(n => n.ForwardCurve).ToList();
                foreach (var c in _availableCodes)
                {

                    if (!_indexPosValuesNasdaq.ContainsKey(c))
                        _indexPosValuesNasdaq.Add(c, new Dictionary<DateTime, decimal>() { });

                    foreach (var date in _dates)
                    {
                        if (!_indexPosValuesNasdaq[c].ContainsKey(date))
                            _indexPosValuesNasdaq[c].Add(date, new decimal());
                    }

                    //find trades with same c
                    var tradesWithSameCode =
                        _viewTradesGeneralInfo.tradeInformations.Where(
                            n => n.ForwardCurve == c && (n.Type == "FFA" || n.Type == "OPTION"))
                            .Select(n => n.Identifier)
                            .ToList();

                    foreach (var t in tradePositionResults)
                    {
                        if (tradesWithSameCode.Contains(t.Key))
                        {
                            foreach (var date in _dates)
                            {
                                //if (!_indexPosValues[c].ContainsKey(date))
                                //    _indexPosValues[c].Add(date, new decimal());

                                var trade = _viewTradesGeneralInfo.tradeInformations.Single(n => n.Identifier == t.Key);

                                decimal tradeDateValue;
                                if (trade.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                    tradeDateValue = Math.Abs(tradePositionResults[t.Key][date]);
                                else
                                    tradeDateValue = Math.Abs(tradePositionResults[t.Key][date]) * (-1);
                                _indexPosValuesNasdaq[c][date] = _indexPosValuesNasdaq[c][date] + tradeDateValue;
                            }
                        }
                    }
                }

                //Option Position Summaries
                var optPosNasdaq =
                    _viewTradesGeneralInfo.tradeInformations.GroupBy(
                        x => new { x.ForwardCurve, x.StrikePrice, x.Type })
                        .Select(
                            x =>
                                new
                                {
                                    ForwardCurve = x.Key.ForwardCurve,
                                    StrikePrice = x.Key.StrikePrice,
                                    CallOrPut = x.Key.Type
                                }).ToList();

                _optPosValuesNasdaq = new List<OptionPositionValuesNasdaq>();
                foreach (var c in optPosNasdaq)
                {
                    if (c.StrikePrice != 0)
                    {
                        var optPosValueNasdaq = new OptionPositionValuesNasdaq
                        {
                            Index = c.ForwardCurve,
                            StrikePrice = c.StrikePrice,
                            CallOrPut = c.CallOrPut
                        };

                        var optionInTradesNasdaq =
                            _viewTradesGeneralInfo.tradeInformations.Where(
                                n => n.ForwardCurve == optPosValueNasdaq.Index
                                     && n.Type == optPosValueNasdaq.CallOrPut &&
                                     n.StrikePrice ==
                                     optPosValueNasdaq.StrikePrice).ToList();

                        optPosValueNasdaq.PosValues = new Dictionary<DateTime, decimal>();
                        foreach (var date in _dates)
                        {
                            var sum = new decimal();
                            foreach (var option in optionInTradesNasdaq)
                            {
                                var tradeDateValue = tradePositionResults[option.Identifier][date];
                                if (option.Type == "Put")
                                {
                                    if (option.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                        tradeDateValue = Math.Abs(tradeDateValue);
                                    else
                                        tradeDateValue = Math.Abs(tradeDateValue) * (-1);
                                }
                                sum = sum + tradeDateValue;
                            }
                            optPosValueNasdaq.PosValues.Add(date, sum);
                        }

                        _optPosValuesNasdaq.Add(optPosValueNasdaq);
                    }
                }

                if (_optPosValuesNasdaq.Count <= 0)
                {
                    //lcgSpanOptionsParameters.Visibility = LayoutVisibility.Never;
                }
            }
        }

        //private void ParseDatFile()
        //{
        //    try
        //    {
        //        if (!File.Exists(beDATfile.Text))
        //        {
        //            XtraMessageBox.Show(this,
        //                "Dat File does not exist. Please select another one.",
        //                Strings.Freight_Metrics,
        //                MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            beDATfile.EditValue = null;
        //            GeneralSettings.Default.DefaultPosOptionParameterFile = string.Empty;
        //            GeneralSettings.Default.Save();
        //            return;
        //        }

        //        #region Init Contracts

        //        var ffaOptionTrades = _optPosValuesNasdaq;

        //        if (ffaOptionTrades.Count == 0)
        //        {
        //            XtraMessageBox.Show("No or FFAs or Options in trade list. Cannot operate this function");
        //            return;
        //        }

        //        //Load Contracts
        //        _contractsNasdaq = new List<ContractNasdaq>();

        //        foreach (var trade in ffaOptionTrades)
        //        {
        //            var spanCode = GetSpanCode(trade.Index);
        //            if (spanCode != null)
        //            {
        //                var contract = new ContractNasdaq();
        //                contract.Index = trade.Index;
        //                contract.Code = GetInitialMarginCode(trade.Index);
        //                contract.SpanCode = spanCode;
        //                contract.CallPut = trade.CallOrPut.Substring(0, 1);
        //                contract.Strike = trade.StrikePrice;

        //                foreach (var dt in _dates)
        //                {
        //                    var ced = new ContractExpiryDetail();
        //                    ced.Date = dt;
        //                    ced.Expiry = dt.ToString();
        //                    ced.SpanExpiry = GetSpanExpiry(dt);

        //                    contract.ExpiryDetails.Add(ced);
        //                }

        //                _contractsNasdaq.Add(contract);
        //            }
        //        }

        //        var spanCodes = _contractsNasdaq.Select(n => n.SpanCode).ToList();

        //        #endregion

        //        #region Read File & Fill Extra Info to contracts

        //        //Open File
        //        Stream myStream = null;

        //        string fileName = beDATfile.Text;
        //        string textLine = "";

        //        try
        //        {
        //            System.IO.StreamReader objReader;
        //            objReader = new StreamReader(fileName);

        //            using (objReader)
        //            {
        //                var fileRow = 0;
        //                string fileIndex = string.Empty;
        //                decimal fileStrikeDenom = new decimal();
        //                decimal fileStrike = new decimal();
        //                string fileExpiry = string.Empty;
        //                string fileCP;

        //                do
        //                {
        //                    fileRow = fileRow + 1;
        //                    textLine = objReader.ReadLine() + "\r\n";

        //                    var recordType = textLine.Substring(0, 2);

        //                    //Identify current index
        //                    if (recordType == "40")
        //                    {
        //                        foreach (var code in spanCodes)
        //                        {
        //                            //If Mid(textLine, 3, 4) = codes(i) & "O" Then?????
        //                            var codeInFile = textLine.Substring(2, 4);
        //                            if (codeInFile == code)
        //                            {
        //                                fileIndex = code;
        //                                fileStrikeDenom = Convert.ToDecimal(textLine.Substring(31, 4),
        //                                    CultureInfo.GetCultureInfo("en"));
        //                                break;
        //                            }
        //                            else
        //                                fileIndex = string.Empty;

        //                            fileExpiry = string.Empty;
        //                        }
        //                    }

        //                    //Identify current expiry
        //                    if (!string.IsNullOrEmpty(fileIndex) && recordType == "50")
        //                    {
        //                        fileExpiry = textLine.Substring(2, 8);
        //                    }

        //                    //Identify current strike and option type
        //                    if (!string.IsNullOrEmpty(fileExpiry) && recordType == "60")
        //                    {
        //                        fileStrike =
        //                            Convert.ToDecimal(textLine.Substring(2, 8), CultureInfo.GetCultureInfo("en")) /
        //                            fileStrikeDenom;
        //                        fileCP = textLine.Substring(10, 1);

        //                        //Find Contract If Exists
        //                        var ctr = _contractsNasdaq.FirstOrDefault(n => n.SpanCode == fileIndex && n.Strike == fileStrike && n.CallPut == fileCP);

        //                        if (ctr != null)
        //                        {
        //                            //Find Expiry Detail for specific date If exists
        //                            var edt = ctr.ExpiryDetails.FirstOrDefault(n => n.SpanExpiry == fileExpiry);

        //                            if (edt != null)
        //                            {
        //                                edt.SettlementPrice = Convert.ToDecimal(textLine.Substring(17, 8),
        //                                    CultureInfo.GetCultureInfo("en"));
        //                                edt.CompositeData = Convert.ToDecimal(textLine.Substring(25, 9),
        //                                    CultureInfo.GetCultureInfo("en"));

        //                                for (var i = 1; i < 17; i++)
        //                                {
        //                                    edt.LossScenarios[i - 1] = Convert.ToDecimal(
        //                                        textLine.Substring(28 + 7 * i, 7), CultureInfo.GetCultureInfo("en"));
        //                                }

        //                                edt.FileRow = fileRow;
        //                            }
        //                        }
        //                    }



        //                } while (objReader.Peek() != -1);
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
        //        }

        //        #endregion

        //        //TODO

        //        #region Create Grid

        //        // Loss senarios
        //        var lossScenarios = new List<IndexLossScenarios>();

        //        foreach (var c in _contractsNasdaq)
        //        {
        //            foreach (var edt in c.ExpiryDetails)
        //            {
        //                var ls = new IndexLossScenarios()
        //                {
        //                    Index = c.Index,
        //                    Expiry = edt.Expiry,
        //                    CallPut = "Call Put",
        //                    Strike = c.Strike,
        //                    FileRow = edt.FileRow,
        //                    SettlementPrice = edt.SettlementPrice,
        //                    CompositeDelta = edt.CompositeData,
        //                    LossScenario1 = edt.LossScenarios[0],
        //                    LossScenario2 = edt.LossScenarios[1],
        //                    LossScenario3 = edt.LossScenarios[2],
        //                    LossScenario4 = edt.LossScenarios[3],
        //                    LossScenario5 = edt.LossScenarios[4],
        //                    LossScenario6 = edt.LossScenarios[5],
        //                    LossScenario7 = edt.LossScenarios[6],
        //                    LossScenario8 = edt.LossScenarios[7],
        //                    LossScenario9 = edt.LossScenarios[8],
        //                    LossScenario10 = edt.LossScenarios[9],
        //                    LossScenario11 = edt.LossScenarios[10],
        //                    LossScenario12 = edt.LossScenarios[11],
        //                    LossScenario13 = edt.LossScenarios[12],
        //                    LossScenario14 = edt.LossScenarios[13],
        //                    LossScenario15 = edt.LossScenarios[14],
        //                    LossScenario16 = edt.LossScenarios[15]
        //                };

        //                lossScenarios.Add(ls);
        //            }

        //        }

        //        //LoadOptionsRiskParameters(lossScenarios);

        //        #endregion

        //        _optionParametersLoaded = true;
        //    }
        //    catch (Exception exc)
        //    {
        //        Cursor = Cursors.Default;
        //        XtraMessageBox.Show(this,
        //            exc.Message,
        //            Strings.Freight_Metrics,
        //            MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        _optionParametersLoaded = false;
        //    }
        //}

        //private void ParseSpanFile(string filepa)
        //{
        //    try
        //    {
        //        if (!File.Exists(beDATfile.Text))
        //        {
        //            XtraMessageBox.Show(this,
        //                "Dat File does not exist. Please select another one.",
        //                Strings.Freight_Metrics,
        //                MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            beDATfile.EditValue = null;
        //            GeneralSettings.Default.DefaultPosOptionParameterFile = string.Empty;
        //            GeneralSettings.Default.Save();
        //            return;
        //        }

        //        #region Init Contracts

        //        var ffaOptionTrades = _optPosValuesNasdaq;

        //        if (ffaOptionTrades.Count == 0)
        //        {
        //            XtraMessageBox.Show("No or FFAs or Options in trade list. Cannot operate this function");
        //            return;
        //        }

        //        //Load Contracts
        //        _contractsNasdaq = new List<ContractNasdaq>();

        //        foreach (var trade in ffaOptionTrades)
        //        {
        //            var spanCode = GetSpanCode(trade.Index);
        //            if (spanCode != null)
        //            {
        //                var contract = new ContractNasdaq();
        //                contract.Index = trade.Index;
        //                contract.Code = GetInitialMarginCode(trade.Index);
        //                contract.SpanCode = spanCode;
        //                contract.DailyFix = get_dailyfix(spanCode);
        //                contract.CallPut = trade.CallOrPut.Substring(0, 1);
        //                contract.Strike = trade.StrikePrice;

        //                foreach (var dt in _dates)
        //                {
        //                    var ced = new ContractExpiryDetail();
        //                    ced.Date = dt;
        //                    ced.Expiry = dt.ToString();
        //                    ced.SpanExpiry = GetSpanExpiry(dt);

        //                    contract.ExpiryDetails.Add(ced);
        //                }

        //                _contractsNasdaq.Add(contract);
        //            }
        //        }

        //        var spanCodes = _contractsNasdaq.Select(n => n.SpanCode).ToList();

        //        #endregion

        //        #region Read File & Fill Extra Info to contracts

        //        //Open File


        //        #endregion

        //        //TODO

        //        #region Create Grid

        //        // Loss senarios
        //        var lossScenarios = new List<IndexLossScenarios>();

        //        foreach (var c in _contractsNasdaq)
        //        {
        //            foreach (var edt in c.ExpiryDetails)
        //            {
        //                var ls = new IndexLossScenarios()
        //                {
        //                    Index = c.Index,
        //                    Expiry = edt.Expiry,
        //                    CallPut = "Call Put",
        //                    Strike = c.Strike,
        //                    FileRow = edt.FileRow,
        //                    SettlementPrice = edt.SettlementPrice,
        //                    CompositeDelta = edt.CompositeData,
        //                    LossScenario1 = edt.LossScenarios[0],
        //                    LossScenario2 = edt.LossScenarios[1],
        //                    LossScenario3 = edt.LossScenarios[2],
        //                    LossScenario4 = edt.LossScenarios[3],
        //                    LossScenario5 = edt.LossScenarios[4],
        //                    LossScenario6 = edt.LossScenarios[5],
        //                    LossScenario7 = edt.LossScenarios[6],
        //                    LossScenario8 = edt.LossScenarios[7],
        //                    LossScenario9 = edt.LossScenarios[8],
        //                    LossScenario10 = edt.LossScenarios[9],
        //                    LossScenario11 = edt.LossScenarios[10],
        //                    LossScenario12 = edt.LossScenarios[11],
        //                    LossScenario13 = edt.LossScenarios[12],
        //                    LossScenario14 = edt.LossScenarios[13],
        //                    LossScenario15 = edt.LossScenarios[14],
        //                    LossScenario16 = edt.LossScenarios[15]
        //                };

        //                lossScenarios.Add(ls);
        //            }

        //        }

        //        //LoadOptionsRiskParameters(lossScenarios);

        //        #endregion

        //        _optionParametersLoaded = true;
        //    }
        //    catch (Exception exc)
        //    {
        //        Cursor = Cursors.Default;
        //        XtraMessageBox.Show(this,
        //            exc.Message,
        //            Strings.Freight_Metrics,
        //            MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        _optionParametersLoaded = false;
        //    }
        //}

        private string GetSpanCode(string code)
        {
            if (code.Contains('_'))
            {
                //get last two chars
                var chars = code.Substring(0, code.Length - 2);
                return chars;

            }
            else
            {
                return code;
            }

            //return null;
        }

        private string GetInitialMarginCode(string code)
        {
            if (code.Contains('_'))
            {
                //get last two chars
                var chars = code.Substring(code.Length - 2, 2);
                if (chars[0].ToString() == "_")
                {
                    string a = _initMarginCodesNasdaq[chars[1].ToString()];
                    return a;
                }
            }
            else
            {
                return code;
            }
            return null;
        }

        private string GetSpanExpiry(DateTime date)
        {
            return date.Year.ToString() + date.ToString("MM") + "00";

        }

        private void beSelectExcel_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = @"Excel files (*.xls)|*.xls;*.xlsx;*.xlsm",
                //RestoreDirectory = true,
                InitialDirectory = @"c:\FreightMetrics\data",
                Title = Strings.Open_File
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                ((ButtonEdit)sender).EditValue = ofd.FileName;
                GeneralSettings.Default.DefaultInitialMarginXls = ofd.FileName.Trim();
                GeneralSettings.Default.Save();
            }
        }

        private void LoadExcelObjects()
        {
            //ConvertToSpanCodes

            var codesToLoad = new List<string>();

            foreach (var c in _availableCodes)
            {
                var codeToLoad = GetInitialMarginCode(c);
                if (codeToLoad != null)
                    codesToLoad.Add(codeToLoad);
            }


            ExcelWorkbook.SetLicenseCode(licenceCode);

            ExcelWorkbook wbook;
            string fileName = beSelectExcel.EditValue.ToString();
            string extension = Path.GetExtension(fileName);

            Cursor = Cursors.WaitCursor;
            //_dialogForm1 = new WaitDialogForm(this);

            switch (extension.ToLower())
            {
                case ".xlsx":
                    {
                        wbook = ExcelWorkbook.ReadXLSX(fileName);
                        break;
                    }
                case ".xls":
                    {
                        wbook = ExcelWorkbook.ReadXLS(fileName);
                        break;
                    }
                case ".csv":
                    {
                        wbook = ExcelWorkbook.ReadCSV(fileName);
                        break;
                    }
                case ".txt":
                    {
                        wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                        break;
                    }
                default:
                    {
                        wbook = ExcelWorkbook.ReadXLS(fileName);
                        break;
                    }
            }

            #region Workbook - A EnClear Values

            var columnNamesEnclear = new List<string>()
                {
                    "Contract Code",
                    "Contract",
                    "Scanning Range",
                    "Intermonth Spread Charge",
                    "Volatility Range %",
                    "Short Option Minimum",
                    "Spot Month Charge Method",
                    "Margin Top Up Rate***",
                    "Per Day / Per MWH / Per Lot",
                    "Spot Margin Levied From**",
                    "Date Effective From*"
                    //,
                    //"Front Months for Application of Spot Month Charge"
                };

            ExcelCellCollection cells = wbook.Worksheets[0].Cells;

            //Create a dictionary which holds column name and index

            var columnIndexes = new Dictionary<string, int>();
            int dataFirstRow = 0;
            bool found = false;
            int k, l = 0;

            for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
            {
                for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                {
                    ExcelCell cell = cells[k, l];
                    if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) &&
                        columnNamesEnclear.Contains(cell.Value.ToString().Trim()))
                    {
                        if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                        columnIndexes.Add(cell.Value.ToString().Trim(), l);

                        dataFirstRow = k + 1;
                        found = true;
                        continue;
                    }
                    if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) &&
                        columnNamesEnclear.Contains(cell.Value.ToString().Trim()))
                    {
                        columnIndexes.Add(cell.Value.ToString().Trim(), l);
                    }
                }
                if (found)
                    break;
            }

            string missingNames =
                columnNamesEnclear.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                    (current,
                        columnName) =>
                        current +
                        columnName +
                        ", ");

            if (!String.IsNullOrEmpty(missingNames))
            {
                throw new Exception("Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                    "' do(es) not exist in the selected file.");

                //XtraMessageBox.Show(this,
                //    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                //    "' do(es) not exist in the selected file.",
                //    Strings.Freight_Metrics,
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return;
            }

            _tmpEnclearsNasdaq = new List<TmpEnclearNasdaq>();

            for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
            {
                if (wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value == null
                    ||
                    string.IsNullOrEmpty(
                        wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value.ToString()))
                    continue;

                var code = wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value.ToString();
                if (!codesToLoad.Contains(code)) continue;

                var tmpEnclear = new TmpEnclearNasdaq()
                {
                    ContractCode = wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value != null
                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Contract Code"]].Value.
                            ToString()
                        : null,
                    Contract = wbook.Worksheets[0].Cells[i, columnIndexes["Contract"]].Value != null
                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Contract"]].Value.
                            ToString()
                        : null,
                    ScanningRange = wbook.Worksheets[0].Cells[i, columnIndexes["Scanning Range"]].Value != null
                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Scanning Range"]].Value.
                            ToString()
                        : null,
                    IntermonthSpreadCharge =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Intermonth Spread Charge"]].Value != null
                            ? Convert.ToDecimal(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Intermonth Spread Charge"]].Value
                                    .ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0,
                    VolatitilyRange =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Volatility Range %"]].Value != null &&
                        wbook.Worksheets[0].Cells[i, columnIndexes["Volatility Range %"]].Value.ToString() != "-"
                            ? Convert.ToDecimal(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Volatility Range %"]].Value.ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0,
                    ShortOptionMinimum =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Short Option Minimum"]].Value != null &&
                        wbook.Worksheets[0].Cells[i, columnIndexes["Short Option Minimum"]].Value.ToString() != "-"
                            ? Convert.ToInt32(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Short Option Minimum"]].Value.ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0,
                    SpotMonthChargeMethod =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Spot Month Charge Method"]].Value != null &&
                        wbook.Worksheets[0].Cells[i, columnIndexes["Spot Month Charge Method"]].Value.ToString() !=
                        "-"
                            ? Convert.ToInt32(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Spot Month Charge Method"]].Value
                                    .ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0,
                    MarginTopUpRate =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Margin Top Up Rate***"]].Value != null &&
                        wbook.Worksheets[0].Cells[i, columnIndexes["Margin Top Up Rate***"]].Value.ToString() != "-"
                            ? Convert.ToInt32(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Margin Top Up Rate***"]].Value.ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0,
                    SpotMarginleviedFrom =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Spot Margin Levied From**"]].Value != null &&
                        wbook.Worksheets[0].Cells[i, columnIndexes["Spot Margin Levied From**"]].Value.ToString() !=
                        "-"
                            ? Convert.ToInt32(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Spot Margin Levied From**"]].Value
                                    .ToString(), CultureInfo.GetCultureInfo("en"))
                            : 0,
                    DateEffectiveFrom =
                        wbook.Worksheets[0].Cells[i, columnIndexes["Date Effective From*"]].Value != null
                            ? Convert.ToDateTime(
                                wbook.Worksheets[0].Cells[i, columnIndexes["Date Effective From*"]].Value.ToString())
                            : new DateTime(1999, 1, 1)
                    //,
                    //FrontMonthsForApplicationOfSpotMonthCharge = 
                    //    wbook.Worksheets[0].Cells[i, columnIndexes["Front Months for Application of Spot Month Charge"]].Value != null &&
                    //    wbook.Worksheets[0].Cells[i, columnIndexes["Front Months for Application of Spot Month Charge"]].Value.ToString() !=
                    //    "-"
                    //        ? Convert.ToInt32(
                    //            wbook.Worksheets[0].Cells[i, columnIndexes["Front Months for Application of Spot Month Charge"]].Value
                    //                .ToString(), CultureInfo.GetCultureInfo("en"))
                    //        : 0
                };

                _tmpEnclearsNasdaq.Add(tmpEnclear);

            }

            #endregion

            #region Workbook - B Scanning Ranges

            var columnNamesScanningRanges = new List<string>()
                {
                    "Contract Code",
                    "Contract",
                    "Scanning Range",
                    "Discount Rate / Tier Structure*",
                    "Per Day / Per MWH / Per Lot / Per Contract Month"
                };

            cells = wbook.Worksheets[1].Cells;

            columnIndexes = new Dictionary<string, int>();
            dataFirstRow = 0;
            found = false;
            k = 0;
            l = 0;

            for (k = 0; k < wbook.Worksheets[1].Rows.Count; k++)
            {
                for (l = 0; l < wbook.Worksheets[1].Columns.Count; l++)
                {
                    ExcelCell cell = cells[k, l];
                    if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) &&
                        columnNamesScanningRanges.Contains(cell.Value.ToString().Trim()))
                    {
                        if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                        columnIndexes.Add(cell.Value.ToString().Trim(), l);

                        dataFirstRow = k + 1;
                        found = true;
                        continue;
                    }
                    if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) &&
                        columnNamesScanningRanges.Contains(cell.Value.ToString().Trim()))
                    {
                        if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                        columnIndexes.Add(cell.Value.ToString().Trim(), l);
                    }
                }
                if (found)
                    break;
            }

            missingNames =
                columnNamesScanningRanges.Where(columnName => !columnIndexes.Keys.Contains(columnName))
                    .Aggregate("",
                        (current,
                            columnName) =>
                            current +
                            columnName +
                            ", ");

            if (!String.IsNullOrEmpty(missingNames))
            {
                throw new Exception("Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                    "' do(es) not exist in the selected file.");

                //XtraMessageBox.Show(this,
                //    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                //    "' do(es) not exist in the selected file.",
                //    Strings.Freight_Metrics,
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return;
            }


            _tmpScanningRatesNasdaq = new List<TmpScanningRangeNasdaq>();

            for (int i = dataFirstRow; i < wbook.Worksheets[1].Rows.Count; i += 2)
            {
                if (wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value == null
                    ||
                    string.IsNullOrEmpty(
                        wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value.ToString()))
                    continue;

                var code = wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value.ToString();
                if (!codesToLoad.Contains(code)) continue;

                var tmpScanningRange = new TmpScanningRangeNasdaq()
                {
                    ContractCode = wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value != null
                        ? wbook.Worksheets[1].Cells[i, columnIndexes["Contract Code"]].Value.
                            ToString()
                        : null,
                    Contract = wbook.Worksheets[1].Cells[i, columnIndexes["Contract"]].Value != null
                        ? wbook.Worksheets[1].Cells[i, columnIndexes["Contract"]].Value.
                            ToString()
                        : null,
                    ScanningRange = wbook.Worksheets[1].Cells[i, columnIndexes["Scanning Range"]].Value != null
                        ? Convert.ToDecimal(wbook.Worksheets[1].Cells[i, columnIndexes["Scanning Range"]].Value.
                            ToString(), CultureInfo.GetCultureInfo("en"))
                        : 0

                };

                for (var tier = 0; tier < 5; tier++)
                {
                    if (wbook.Worksheets[1].Cells[i, 4].Value != null &&
                        wbook.Worksheets[1].Cells[i, 4].Value.ToString() != "")
                    {
                        var rate = Convert.ToDecimal(wbook.Worksheets[1].Cells[i - 1, 4 + tier].Value.ToString());

                        var dateRange = wbook.Worksheets[1].Cells[i, 4 + tier].Value.ToString();

                        var tierFrom = new DateTime();
                        var tierTo = new DateTime();

                        if (tier == 0 && DefineDateRangeIsDate(dateRange))
                        {
                            tierFrom = Convert.ToDateTime(dateRange);
                            tierTo = tierFrom;
                        }
                        else if (tier == 0 && !dateRange.Contains('-'))
                        {
                            var month = GetMonth(dateRange.Substring(0, 3));
                            var year = Convert.ToInt32(dateRange.Substring(dateRange.Count() - 2, 2));
                            tierFrom = new DateTime(year, month, 1);
                            tierTo = tierFrom;
                        }
                        else if (dateRange.Trim().Substring(dateRange.Trim().Length - 7, 7) == "onwards")
                        {
                            var month = GetMonth(dateRange.Substring(0, 3));
                            var year = Convert.ToInt32("20" + dateRange.Substring(4, 2));

                            tierFrom = new DateTime(year, month, 1);


                            //
                            var monthTo = _viewTradesGeneralInfo.periodTo.AddMonths(1).Month;
                            var yearTo = _viewTradesGeneralInfo.periodTo.AddMonths(1).Year;
                            tierTo = new DateTime(yearTo, monthTo, 1);
                        }
                        else
                        {
                            string[] dates = dateRange.Split('-').ToArray();

                            if (dates.Count() > 1) //Υπάρχει range
                            {
                                var monthFrom = GetMonth(dates[0].Trim().Substring(0, 3));
                                var yearFrom =
                                    Convert.ToInt32("20" + dates[0].Trim().Substring(dates[0].Trim().Length - 2, 2));
                                tierFrom = new DateTime(yearFrom, monthFrom, 1);

                                var monthTo = GetMonth(dates[1].Trim().Substring(0, 3));
                                var yearTo =
                                    Convert.ToInt32("20" + dates[1].Trim().Substring(dates[1].Trim().Length - 2, 2));
                                tierTo = new DateTime(yearTo, monthTo, 1);
                            }
                            else
                            {
                                throw new Exception("Error occurred while parsing excel file. Error: There is no \'-\' between ranges in Scanning Ranges Tab for Tiers 2 to 4. Correct format Mon YY - Mon YY.");
                            }
                        }

                        tmpScanningRange.TierDiscounts.Add(
                            new TierDiscount { Rate = rate, DateRange = dateRange, TierFrom = tierFrom, TierTo = tierTo });
                    }
                    else
                    {
                        break;
                    }
                }

                _tmpScanningRatesNasdaq.Add(tmpScanningRange);

            }

            #endregion

            #region Workbook - C Intercommodity Credits

            var columnNamesIntercommodityCredits = new List<string>()
                {
                    "Contract / Spread",
                    "Contract Codes",
                    "Priorities",
                    "Delta Ratio",
                    "% Credit per leg",
                    "Date effective from*"
                };

            cells = wbook.Worksheets[2].Cells;

            columnIndexes = new Dictionary<string, int>();
            dataFirstRow = 0;
            found = false;
            k = 0;
            l = 0;

            for (k = 0; k < wbook.Worksheets[2].Rows.Count; k++)
            {
                for (l = 0; l < wbook.Worksheets[2].Columns.Count; l++)
                {
                    ExcelCell cell = cells[k, l];
                    if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) &&
                        columnNamesIntercommodityCredits.Contains(cell.Value.ToString().Trim()))
                    {
                        if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                        columnIndexes.Add(cell.Value.ToString().Trim(), l);

                        dataFirstRow = k + 1;
                        found = true;
                        continue;
                    }
                    if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) &&
                        columnNamesIntercommodityCredits.Contains(cell.Value.ToString().Trim()))
                    {
                        if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                        columnIndexes.Add(cell.Value.ToString().Trim(), l);
                    }
                }
                if (found)
                    break;
            }

            missingNames =
                columnNamesIntercommodityCredits.Where(columnName => !columnIndexes.Keys.Contains(columnName))
                    .Aggregate("",
                        (current,
                            columnName) =>
                            current +
                            columnName +
                            ", ");

            if (!String.IsNullOrEmpty(missingNames))
            {
                throw new Exception("Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                    "' do(es) not exist in the selected file.");

                //XtraMessageBox.Show(this,
                //    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                //    "' do(es) not exist in the selected file.",
                //    Strings.Freight_Metrics,
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return;
            }


            _tmpIntercommCreditsNasdaq = new List<TmpIntercommodityCreditsNasdaq>();

            for (int i = dataFirstRow; i < wbook.Worksheets[2].Rows.Count; i += 2)
            {
                if (wbook.Worksheets[2].Cells[i, columnIndexes["Contract Codes"]].Value == null
                    ||
                    string.IsNullOrEmpty(
                        wbook.Worksheets[2].Cells[i, columnIndexes["Contract Codes"]].Value.ToString()))
                    continue;

                //char[] delimiterChars = { '-',':' };
                string[] contractCodes =
                    wbook.Worksheets[2].Cells[i, columnIndexes["Contract Codes"]].Value.ToString()
                        .Split('-')
                        .ToArray();
                var code1 = contractCodes[0].Trim();
                var code2 = contractCodes[1].Trim();

                if (codesToLoad.Contains(code1) && codesToLoad.Contains(code2))
                {

                    string[] deltaRations =
                        wbook.Worksheets[2].Cells[i, columnIndexes["Delta Ratio"]].Value.ToString()
                            .Split(':')
                            .ToArray();

                    var tmpIntercommCredit = new TmpIntercommodityCreditsNasdaq()
                    {
                        Code1 = code1,
                        DeltaRatioCode1 = Convert.ToInt32(deltaRations[0].Trim()),
                        Code2 = code2,
                        DeltaRatioCode2 = Convert.ToInt32(deltaRations[1].Trim()),
                        Properties = wbook.Worksheets[2].Cells[i, columnIndexes["Priorities"]].Value != null
                            ? Convert.ToInt32(wbook.Worksheets[2].Cells[i, columnIndexes["Priorities"]].Value.
                                ToString())
                            : 0,
                        CreditPerLeg = wbook.Worksheets[2].Cells[i, columnIndexes["% Credit per leg"]].Value != null
                            ? Convert.ToInt32(wbook.Worksheets[2].Cells[i, columnIndexes["% Credit per leg"]].Value.
                                ToString())
                            : 0,
                        DateEffectiveForm =
                            wbook.Worksheets[2].Cells[i, columnIndexes["Date effective from*"]].Value != null
                                ? Convert.ToDateTime(
                                    wbook.Worksheets[2].Cells[i, columnIndexes["Date effective from*"]].Value
                                        .ToString())
                                : new DateTime(1999, 1, 1)
                    };

                    _tmpIntercommCreditsNasdaq.Add(tmpIntercommCredit);
                }
            }

            #endregion

            Cursor = Cursors.Default;
            //lock (_syncObject)
            //{
            //    _dialogForm1.Close();
            //    _dialogForm1 = null;
            //}


            //catch (Exception exc)
            //{

            //    Cursor = Cursors.Default;
            //    //lock (_syncObject)
            //    //{
            //    //    _dialogForm1.Close();
            //    //    _dialogForm1 = null;
            //    //}

            //    //XtraMessageBox.Show(this,
            //    //    "Error occurred while parsing excel file. Error: '" + exc.Message + "'",
            //    //    Strings.Freight_Metrics,
            //    //    MessageBoxButtons.OK, MessageBoxIcon.Error);

            //    throw new Exception("There is no \'-\' between ranges in Scanning Ranges Tab for Tiers 2 to 4. Correct format Mon YY - Mon YY."); 
            //}
        }

        private bool DefineDateRangeIsDate(string value)
        {
            bool isDate;

            try
            {
                var date = Convert.ToDateTime(value);
                isDate = true;
            }
            catch (Exception exc)
            {
                isDate = false;
            }

            return isDate;
        }

        private int GetMonth(string p)
        {
            if (p == "Jan" || p == "Ιαν") return 1;
            if (p == "Feb" || p == "Φεβρ") return 2;
            if (p == "Mar" || p == "Μαρ") return 3;
            if (p == "Apr" || p == "Απρ") return 4;
            if (p == "May" || p == "Μαι") return 5;
            if (p == "Jun" || p == "Ιουν") return 6;
            if (p == "Jul" || p == "Ιουλ") return 7;
            if (p == "Aug" || p == "Αυγ") return 8;
            if (p == "Sep" || p == "Σεπτ") return 9;
            if (p == "Oct" || p == "Οκτ") return 10;
            if (p == "Nov" || p == "Νοε") return 11;
            if (p == "Dec" || p == "Δεκ") return 12;

            return 0;
        }

        private string GetMon(string p)
        {
            if (p == "Ιαν") return "JAN";
            if (p == "Φεβρ") return "FEB";
            if (p == "Μαρ") return "MAR";
            if (p == "Απρ") return "APR";
            if (p == "Μαι") return "MAY";
            if (p == "Ιουν") return "JUN";
            if (p == "Ιουλ") return "JUL";
            if (p == "Αυγ") return "AUG";
            if (p == "Σεπτ") return "SEP";
            if (p == "Οκτ") return "OCT";
            if (p == "Νοε") return "NOV";
            if (p == "Δεκ") return "DEC";

            return p;
        }

        private void btnCalculateInitialMargin_Click(object sender, EventArgs e)
        {
            resultList.Clear();
            hasoption = new bool[_availableCodes.Count];;
            corlist.Clear();
            startendList.Clear();
            _scanningRanges = new Dictionary<string, decimal>();
            _dailyfixes = new Dictionary<string, decimal>();
            _worstscenarios = new Dictionary<string, decimal[]>();
            //Load span file data
            _rAparamNasdaq = LoadRiskArrayParameters();

            try
            {
                string filePath = string.Empty;
                string fileExt = string.Empty;
                var spantoday = DateTime.Today;
                if (spantoday.DayOfWeek == DayOfWeek.Monday)
                {
                    DateTime lastFriday = DateTime.Now.AddDays(-3);
                    spantoday = lastFriday;
                }
                if (spantoday.DayOfWeek == DayOfWeek.Sunday)
                {
                    DateTime lastFriday = DateTime.Now.AddDays(-2);
                    spantoday = lastFriday;
                }

                if (spantoday.DayOfWeek == DayOfWeek.Saturday)
                {
                    DateTime lastFriday = DateTime.Now.AddDays(-1);
                    spantoday = lastFriday;
                }
                var spanname = spantoday.AddDays(-1).ToString("yyMMdd");
                filePath = @"c:\freightmetrics\Data\NSPANPAR_-001__-SE-_____-" + spanname + "-001.txt";
                if (!File.Exists(filePath)) filePath = @"192.168.12.165\Freightmetrics\Data\NSPANPAR_-001__-SE-_____-" + spanname + "-001.txt";


                if (File.Exists(filePath))
                {
                    LoadSpanfile(filePath);
                }
                else
                {
                    XtraMessageBox.Show(this,
                       "Please insert a Span file.",
                        Strings.Freight_Metrics,
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                loadCorrVol();

                _deltaNasdaq = new List<DeltaValuesNasdaq>();
                _scanningRanges = new Dictionary<string, decimal>();
                _riskScenarios = new Dictionary<string, decimal[]>();

                //_intermonthSpreadCharge = new Dictionary<string, decimal>();
                //_spotCharge = new Dictionary<string, decimal>();
                //_interCommoditySpreadCredit = new Dictionary<string, decimal>();


                int i = 0;
                foreach (var code in _availableCodes)
                {

                    var contr = _mapCodesNasdaq[GetInitialMarginCode(code)];// GetInitialMarginCode(code);
                    var delta = new DeltaValuesNasdaq { Code = code };

                    foreach (var date in _dates)
                    {
                        //var contract = _tmpScanningRatesNasdaq.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));
                        //if (contract != null)
                        //{
                        // check if date range not included in enclear.xlsx                            
                        var s = _indexPosValuesNasdaq[code][date];
                        var deltaValue = _indexPosValuesNasdaq[code][date] * 297 / 1000;
                        delta.DateValuesNasdaq.Add(date, deltaValue);
                        //}
                    }

                    _deltaNasdaq.Add(delta);
                    //string cont = "";
                    //cont = (contr == "CS4TC")?"CS5TC" : contr;
                    //_scanningRanges.Add(contr, CalcOfScanningRange(contr));

                    CalcOfScanningRange(contr, i);
                    i++;



                    //_scanningRanges.Add(contr, 15000);
                    foreach (var a in _scanningRanges)
                    {
                        if (!_riskScenarios.ContainsKey(a.Key))
                            _riskScenarios.Add(a.Key, CalculationOfRiskScenarios(a.Key));
                    }

                    //    _intermonthSpreadCharge.Add(code, CalculateIntermonthSpreadCharge(code));
                    //    _spotCharge.Add(code, CalculateSpotCharge(code));
                }

                //_interCommoditySpreadCredit = CalculateInterCommoditySpreadCreditForAllIndexes(); //last step

                var initialMargin = CalculateInitialMargin();
                LoadInitialMargin(initialMargin);

                if (gridViewInitialMargin.DataRowCount > 0)
                    btnExportInitialMargin.Enabled = true;
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                    exc.Message,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private List<InitialMarginNasdaq> CalculateInitialMargin()
        {

            decimal min = -1;
            int steps = 0;
            decimal min1 = 0;
            decimal min2 = 0;
            //var initalMarginDict = new Dictionary<string, decimal>();
            var initialMargins = new List<InitialMarginNasdaq>();

            List<decimal> tab1 = new List<decimal>();
            List<decimal> tab12 = new List<decimal>();
            decimal _position = new decimal();
            int i = 0;
            foreach (var c in _availableCodes)
            {
                var code = _mapCodesNasdaq[GetInitialMarginCode(c)];




                if (hasoption[i])
                {
                    List<tradelist> asa = resultList.Where(n => n.risk_group == code).ToList();
                    List<decimal> xx = getsumworstcase(asa);
                    var im = new InitialMarginNasdaq
                    {
                        SpanCode = c,
                        DailyFix = _dailyfixes.Where(n => n.Key.Contains(code)).Select(n => n.Value).Sum(),
                        ScanningRange = _scanningRanges.Where(n => n.Key.Contains(code)).Select(n => n.Value).Sum(),
                        NakedInitialMargin = xx.Min(),
                        InitialMarginValue = xx.Min()
                    };
                    initialMargins.Add(im);


                }
                else
                {
                    _position = 0;
                    List<decimal> poslist = new List<decimal>();
                    var a = GetInitialMarginCode(c);

                    //string code = _availableCodes.FirstOrDefault();

                    int count = 0;
                    foreach (var p in _indexPosValuesNasdaq[c].Values.ToList())
                    {






                        if (count == 0 && p != 0)
                        {

                            int numberofdays = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                            int totaldays =
                                WorkingDaysHelper.BusinessDaysUntil(
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, numberofdays));

                            int wdays =
                                WorkingDaysHelper.BusinessDaysUntil(DateTime.Now,
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, numberofdays));
                            poslist.Add(p*wdays/totaldays);
                            count++;
                            continue;
                        }
                        if (p != 0) poslist.Add(p);
                        count++;
                    }

                    decimal init = 0;
                    
                    decimal previtem = 0;
                    bool applyCorrelation = false;
                    foreach (var day in _worstscenarios.Where(n => n.Key.Contains(code)).ToList())
                    {
                        if (i > 0) applyCorrelation = !sameSign(previtem, poslist.ElementAt(i));
                        if (poslist.ElementAt(i) > 0)
                        {
                            init = init + _worstscenarios[day.Key][0]*poslist.ElementAt(i);
                        }
                        else
                        {
                            init = init + _worstscenarios[day.Key][1]*poslist.ElementAt(i);
                        }
                        previtem = poslist.ElementAt(i);
                        if (i < 1)
                        {
                            i++;
                            continue;
                        }
                        if (applyCorrelation)
                        {
                            Correlation cor = corlist.FirstOrDefault(n => n.key == code);
                            var corel =
                                cor.CorrTable.Where(
                                    n =>
                                        n.x > startendList.ElementAt(i).ElementAt(0) &&
                                        n.x < startendList.ElementAt(i).ElementAt(1) &&
                                        n.y > startendList.ElementAt(i - 1).ElementAt(0) &&
                                        n.y < startendList.ElementAt(i - 1).ElementAt(1));

                            int j = 0;
                            foreach (CorVal x in corel)
                            {
                                if (j == 0) min = x.val;
                                if (x.val < min)
                                    min = x.val;
                            }

                            if (min <= 0.4m && min >= 0.3m)
                            {
                                steps = 6;
                            }
                            else if (min >= 0.4m && min <= 0.5m)
                            {
                                steps = 5;
                            }
                            else if (min >= 0.4m && min <= 0.5m)
                            {
                                steps = 4;
                            }
                            else if (min >= 0.5m && min <= 0.7m)
                            {
                                steps = 3;

                            }
                            else if (min >= 0.7m && min <= 0.85m)
                            {
                                steps = 2;
                            }
                            else if (min >= 0.95m)
                            {
                                steps = 1;
                            }
                        }
                        string tableaname = _worstscenarios.ElementAt(i).Key;
                        string tablebname = _worstscenarios.ElementAt(i - 1).Key;

                        RiskVals tablea = listA.FirstOrDefault(n => n.ticker == tableaname).risk;
                        RiskVals tableb = listA.FirstOrDefault(n => n.ticker == tablebname).risk;

                        tab1 = RiskVAlToList(tablea, poslist.ElementAt(i));
                        tab12 = RiskVAlToList(tableb, poslist.ElementAt(i - 1));

                        min1 = tab1.Min();
                        min2 = tab12.Min();


                        i++;
                    }

                    List<decimal> resultsList = new List<decimal>();
                    if (applyCorrelation)
                    {
                        resultsList = new List<decimal>();
                        decimal result;
                        for (int v = (steps*2); v <= 16 - (steps*2); v = v + 2)
                        {
                            for (int g = -(steps*2); g <= (steps*2); g = g + 2)
                            {
                                if (v >= (steps*2) && v < 16 - (steps*2))
                                {
                                    result = tab1.ElementAt(v) + tab12.ElementAt(v + g);
                                    resultsList.Add(result);
                                }

                            }
                        }
                        resultsList.Add(tab1.ElementAt(0) + tab12.ElementAt(0));
                    }




                    decimal res = 0;
                    if (resultsList.Count > 0)
                        resultsList.Min();
                    decimal naked = min1 + min2;

                    int a1 = startendList.Count;

                    if (!applyCorrelation)
                    {
                        var im = new InitialMarginNasdaq
                        {
                            SpanCode = c,
                            DailyFix = _dailyfixes.Where(n => n.Key.Contains(code)).Select(n => n.Value).Sum(),
                            ScanningRange = _scanningRanges.Where(n => n.Key.Contains(code)).Select(n => n.Value).Sum(),
                            NakedInitialMargin = init,
                            InitialMarginValue = init
                        };
                        initialMargins.Add(im);
                    }
                    else
                    {

                        var im = new InitialMarginNasdaq
                        {
                            SpanCode = c,
                            DailyFix = _dailyfixes.Where(n => n.Key.Contains(code)).Select(n => n.Value).Sum(),
                            ScanningRange = _scanningRanges.Where(n => n.Key.Contains(code)).Select(n => n.Value).Sum(),
                            NakedInitialMargin = naked,
                            InitialMarginValue = res
                        };
                        initialMargins.Add(im);
                    }
                }
                i++;
            }

            return initialMargins;
        }


        bool sameSign(decimal num1, decimal num2)
        {
            if (num1 > 0 && num2 < 0)
                return false;
            if (num1 < 0 && num2 > 0)
                return false;
            return true;
        }




        public List<decimal> RiskVAlToList(RiskVals a, decimal pos)
        {
            List<decimal> res = new List<decimal>();
            decimal delta = decimal.Parse(a.delta) / 100000000;
            if (delta == 1)
            {
                res.Add((decimal.Parse(a.val1) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val2) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val3) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val4) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val5) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val6) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val7) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val8) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val9) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val10) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val11) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val12) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val13) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val14) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val15) / 100) * delta * pos);
                res.Add((decimal.Parse(a.val16) / 100) * delta * pos);
            }
            return res;
        }

        private Dictionary<string, decimal> CalculateInterCommoditySpreadCreditForAllIndexes()
        {
            //net Delta code
            var netDeltaDict = new Dictionary<string, decimal>();
            var WPFRDict = new Dictionary<string, decimal>();
            var interCommSpreadCreditDict = new Dictionary<string, decimal>();

            foreach (var av in _availableCodes)
            {
                var code = GetInitialMarginCode(av);

                decimal netDelta = 0;
                decimal wpfr = 0;
                decimal codePriceRisk = 0;
                decimal interCommSpreadCredit = 0;

                var interComm = _tmpIntercommCreditsNasdaq.FirstOrDefault(n => n.Code1 == code);
                var delta = _deltaNasdaq.FirstOrDefault(n => n.Code == av);

                foreach (var date in _dates)
                {
                    netDelta = netDelta + delta.DateValuesNasdaq[date];

                    //Add Options
                    var options = _optPosValuesNasdaq.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                    foreach (var option in options)
                    {
                        //Ean yparxei Option Value gia ton index
                        var contract =
                            _contractsNasdaq.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice && n.CallPut == option.CallOrPut.Substring(0, 1));
                        if (contract != null)
                        {
                            var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                            netDelta = netDelta + Math.Abs(option.PosValues[date]) * exd.CompositeData;
                        }
                    }
                }
                netDeltaDict.Add(code, netDelta);

                //priceRisk per Code
                codePriceRisk = 0; // _scanningRanges[av][1];

                //WPFR
                wpfr = netDelta != 0 ? Decimal.Round(codePriceRisk / Math.Abs(netDelta), 0) : 0;
                WPFRDict.Add(code, wpfr);

                //InterCommodity Spread Credit
                interCommSpreadCreditDict.Add(code, 0);
            }

            //Run through priority list
            foreach (var pair in _tmpIntercommCreditsNasdaq)
            {
                if ((netDeltaDict[pair.Code1] < 0 && netDeltaDict[pair.Code2] > 0) ||
                    (netDeltaDict[pair.Code1] > 0 && netDeltaDict[pair.Code2] < 0))
                {
                    var numSpreads1 = pair.DeltaRatioCode1 != 0 ? Decimal.Round(Math.Abs(netDeltaDict[pair.Code1] / pair.DeltaRatioCode1), 0) : 0;
                    var numSpreads2 = pair.DeltaRatioCode2 != 0 ? Decimal.Round(Math.Abs(netDeltaDict[pair.Code2] / pair.DeltaRatioCode2), 0) : 0;

                    decimal numSpreads = 0;
                    if (numSpreads1 < numSpreads2)
                        numSpreads = numSpreads1;
                    else
                        numSpreads = numSpreads2;

                    interCommSpreadCreditDict[pair.Code1] = interCommSpreadCreditDict[pair.Code1] +
                                                            WPFRDict[pair.Code1] * numSpreads * pair.DeltaRatioCode1 *
                                                            pair.CreditPerLeg / 100;

                    interCommSpreadCreditDict[pair.Code2] = interCommSpreadCreditDict[pair.Code2] +
                                                            WPFRDict[pair.Code1] * numSpreads * pair.DeltaRatioCode2 *
                                                            pair.CreditPerLeg / 100;

                    int code1sign = netDeltaDict[pair.Code1] < 0 ? -1 : 1;
                    int code2sign = netDeltaDict[pair.Code2] < 0 ? -1 : 1;

                    interCommSpreadCreditDict[pair.Code1] = interCommSpreadCreditDict[pair.Code1] -
                                                            (code1sign * numSpreads * pair.DeltaRatioCode1);

                    interCommSpreadCreditDict[pair.Code2] = interCommSpreadCreditDict[pair.Code2] -
                                                            (code2sign * numSpreads * pair.DeltaRatioCode2);
                }
            }

            return interCommSpreadCreditDict;
        }

        private decimal CalculateSpotCharge(string code)
        {
            var enclear = _tmpEnclearsNasdaq.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));
            var delta = _deltaNasdaq.FirstOrDefault(n => n.Code == code);

            var chargeAmount = enclear.MarginTopUpRate;
            decimal totalDelta = 0;

            foreach (var date in _dates)
            {
                totalDelta = totalDelta + Math.Abs(delta.DateValuesNasdaq[date]);

                //Add Options
                var options = _optPosValuesNasdaq.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                foreach (var option in options)
                {
                    //Ean yparxei Option Value gia ton index
                    var contract =
                        _contractsNasdaq.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice && n.CallPut == option.CallOrPut.Substring(0, 1));
                    if (contract != null)
                    {
                        var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                        totalDelta = totalDelta + Math.Abs(option.PosValues[date] * exd.CompositeData);
                    }
                }
            }

            return Decimal.Round(totalDelta * chargeAmount, 0);
        }

        private decimal CalculateIntermonthSpreadCharge(string code)
        {
            decimal totalShort = 0;
            decimal totalLong = 0;

            var deltaCode = _deltaNasdaq.First(n => n.Code == code);
            foreach (var date in _dates)
            {
                if (deltaCode.DateValuesNasdaq[date] < 0)
                    totalShort = totalShort - deltaCode.DateValuesNasdaq[date];

                if (deltaCode.DateValuesNasdaq[date] > 0)
                    totalLong = totalLong + deltaCode.DateValuesNasdaq[date];

                //Add Options
                var options = _optPosValuesNasdaq.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                foreach (var option in options)
                {
                    //Ean yparxei Option Value gia ton index
                    var contract = _contractsNasdaq.SingleOrDefault(n => n.Index == code && n.Strike == option.StrikePrice
                        && n.CallPut == option.CallOrPut.Substring(0, 1));
                    if (contract != null)
                    {
                        var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                        if (option.CallOrPut.Substring(0, 1) == "C")
                        {
                            if (option.PosValues[date] < 0)
                                totalShort = totalShort - option.PosValues[date] * Math.Abs(exd.CompositeData);

                            if (option.PosValues[date] > 0)
                                totalLong = totalLong + option.PosValues[date] * Math.Abs(exd.CompositeData);
                        }
                        else
                        {
                            if (option.PosValues[date] > 0)
                                totalShort = totalShort + option.PosValues[date] * Math.Abs(exd.CompositeData);

                            if (option.PosValues[date] < 0)
                                totalLong = totalLong - option.PosValues[date] * Math.Abs(exd.CompositeData);
                        }
                    }
                }
            }

            var enclear = _tmpEnclearsNasdaq.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));

            if (totalLong < totalShort)
                return Math.Round(totalLong * enclear.IntermonthSpreadCharge, 0);
            else
                return Math.Round(totalShort * enclear.IntermonthSpreadCharge, 0);
        }


        private void CalcOfScanningRange(string code, int i)
        {
            var scanRange = new Dictionary<int, decimal>();
            //_scanningRanges = new Dictionary<string, decimal>();
            DataTable volatilityTable = new DataTable();
            if (code == "CS4TC") code = "CS5TC";

            var contractdt = new List<DateTime>();
            var volTable = _VolCorrList.Where(x => x.nindex == code).Select(x => x.NasdaqVolCorrTable).FirstOrDefault();
            string text = volTable.Rows[1][0].ToString();
            var dn = _deltaNasdaq.ElementAt(i);
            {
                foreach (var validt in dn.DateValuesNasdaq)
                {
                    if (validt.Value != 0) contractdt.Add(validt.Key);
                }

            }
            //var dat = _deltaNasdaq.
            string text2 = volTable.Rows[15][1].ToString();

            //var 
            //List<VolatilityTable> volTable = new List<VolatilityTable>();
            //studentDetails = ConvertDataTable<Student>(dt);
            // var deliveryPeriod = _tradePositionResults.

            // var scanningRange = _tmpScanningRatesNasdaq.FirstOrDefault(n => n.ContractCode == GetInitialMarginCode(code));
            string dt = "";
            string instr = "";
            foreach (var date in contractdt)
            {
                var mm = date.ToString("MMM", new CultureInfo("en-GB")).ToUpper();
                //                dt = GetMon(mm) + date.ToString("yy");
                dt = mm + date.ToString("yy");
                var contract_instr = code + "_" + dt;
                if (!_dailyfixes.ContainsKey(contract_instr))
                    _dailyfixes.Add(contract_instr, getdailyfix(contract_instr));
                if (!_worstscenarios.ContainsKey(code))
                    _worstscenarios.Add(contract_instr, getworstcase(contract_instr));
                //_condaily.Add(new ContractDailyFix() { contract = code, dailyfix = _dailyfix });
                //instr = code + "_" + dt;

                DateTime startDeliveryDate = _availableTrades.Select(n => n.PeriodFrom).ElementAt(i);
                DateTime stopDeliveryDate = _availableTrades.Select(n => n.PeriodTo).ElementAt(i);
                string type = _availableTrades.Select(n => n.Type).ElementAt(i);
                decimal val = 0;


                if (type == "Call" || type == "Put")
                {
                    hasoption[i] = true;
                    var spantoday = DateTime.Today;
                    if (spantoday.DayOfWeek == DayOfWeek.Monday)
                    {
                        DateTime lastFriday = DateTime.Now.AddDays(-3);
                        spantoday = lastFriday;
                    }
                    if (spantoday.DayOfWeek == DayOfWeek.Sunday)
                    {
                        DateTime lastFriday = DateTime.Now.AddDays(-2);
                        spantoday = lastFriday;
                    }

                    if (spantoday.DayOfWeek == DayOfWeek.Saturday)
                    {
                        DateTime lastFriday = DateTime.Now.AddDays(-1);
                        spantoday = lastFriday;
                    }




                    if (type == "Call")
                        val = _availableTrades.Select(n => n.StrikePrice).ElementAt(i);

                    var spanname = spantoday.AddDays(-1).ToString("yyMMdd");
                    string filePath = @"c:\freightmetrics\Data\NSPANPAR_-001__-SE-_____-" + spanname + "-001.txt";
                    if (!File.Exists(filePath)) filePath = @"192.168.12.165\Freightmetrics\Data\NSPANPAR_-001__-SE-_____-" + spanname + "-001.txt";


                    if (File.Exists(filePath))
                    {
                        tradelist a = LoadSpanfile2(filePath, contract_instr + "_" + "C" + ((int)val).ToString());
                        resultList.Add(a);
                    }



                }
                double span = Span(startDeliveryDate, stopDeliveryDate, volTable);
                decimal test = (decimal)span;
                decimal riskInterval = _dailyfixes[contract_instr] * test;
                ///instr_series.Add(instr);
                if (!_scanningRanges.ContainsKey(contract_instr))
                    _scanningRanges.Add(contract_instr, riskInterval);

            }


        }

        /// <summary>
        /// Calculation of span value
        /// </summary>
        /// <returns></returns>
        public double Span(DateTime startdateIn, DateTime enddateIn, DataTable data)
        {
            int days = 0;
            double val = 0;
            int startindex = 0;
            int endindex = 0;
            int i = 0;
            int startdate = 0;
            int enddate = 0;
            int alldays = 0;
            if (startdateIn.Date > DateTime.Now.Date)
            {
                int.TryParse((startdateIn.Date - DateTime.Now.Date).TotalDays.ToString(), out startdate);
            }
            else
            {
                startdate = 0;
            }
            if (enddateIn.Date > DateTime.Now.Date)
            {
                int.TryParse((enddateIn.Date - DateTime.Now.Date).TotalDays.ToString(), out enddate);
            }
            else
            {
                enddate = 0;
            }

            startendList.Add(new List<int>() { startdate, enddate });
            int prevdays = 0;
            int diff = 0;
            double total = 0;
            foreach (DataRow r in data.Rows)
            {
                int.TryParse(r[0].ToString(), out days);

                if (startdate < days && startindex == 0 && i >= 1) startindex = i;
                if (enddate < days && endindex == 0) endindex = i - 1;
                if (startindex != 0 && endindex != 0) break;
                i++;
            }


            for (int j = startindex; j <= endindex + 1; j++)
            {
                int.TryParse(data.Rows[j][0].ToString(), out days);
                double.TryParse(data.Rows[j][1].ToString(), out val);
                if (j == startindex) int.TryParse(data.Rows[j][0].ToString(), out prevdays);
                else if (j == startindex) int.TryParse(data.Rows[j][0].ToString(), out prevdays);
                else if (j >= 1) int.TryParse(data.Rows[j - 1][0].ToString(), out prevdays);


                if (j == startindex)
                {
                    diff = prevdays - startdate;
                }
                else if (j == endindex + 1)
                {
                    diff = enddate - prevdays;
                }
                else
                {
                    diff = days - prevdays;
                }
                ///////to be  confirmed alldays are not working days but the diff of the end and start date
                alldays = enddate - startdate;
                double test = (double)diff / (double)alldays;
                total = total + (val * ((double)diff / (double)alldays));

            }
            return total;

        }






        private decimal[] CalculationOfRiskScenarios(string code)
        {
            var scanRange = new Dictionary<int, decimal>();

            var scanningRange = _scanningRanges[code];

            //Scanning WorstScenario



            for (var i = 0; i < 16; i++)
            {
                var sr = (decimal)0;

                var riskArrayParam = _rAparamNasdaq.FirstOrDefault(n => n.RiskScenario == i + 1);

                sr = scanningRange * riskArrayParam.PriceVariation * riskArrayParam.WeightFraction;

                //Options

                foreach (var date in _dates)
                {
                    var options = _optPosValuesNasdaq.Where(n => n.Index == code && n.PosValues[date] != 0).ToList();

                    foreach (var option in options)
                    {

                        //Ean yparxei Option Value gia ton index
                        var contract =
                            _contractsNasdaq.SingleOrDefault(
                                n => n.Index == code && n.Strike == option.StrikePrice && n.CallPut == option.CallOrPut.Substring(0, 1));
                        if (contract != null)
                        {
                            var exd = contract.ExpiryDetails.FirstOrDefault(n => n.Date == date);
                            var optSr = Decimal.Round((option.PosValues[date] * exd.LossScenarios[i] / 10), MidpointRounding.AwayFromZero);
                            sr = sr - optSr;
                        }
                    }
                }
                scanRange.Add(i, Decimal.Round(sr, MidpointRounding.AwayFromZero));
            }


            //var worstCase = scanRange[0];
            //var activeScenario = 1;
            //int pairedScenario = 0;

            //for (var i = 1; i < 17; i++)
            //{
            //    if (scanRange[i - 1] < worstCase)
            //    {
            //        worstCase = scanRange[i - 1];
            //        activeScenario = i;
            //    }
            //}
            //if (activeScenario == 15 || activeScenario == 16)
            //    pairedScenario = activeScenario;
            //else
            //{
            //    if (activeScenario % 2 == 0)
            //        pairedScenario = activeScenario - 1;
            //    else
            //        pairedScenario = activeScenario + 1;
            //}

            //var volRisk = -(Math.Round((scanRange[activeScenario - 1] + scanRange[pairedScenario - 1]) / 2, 0));
            //var timeRisk = -(Math.Round((scanRange[0] + scanRange[1]) / 2, 0));
            //var priceRisk = volRisk - timeRisk;

            //if (priceRisk < 0)
            //    priceRisk = 0;

            //var scanningScenario = new decimal[2];
            //scanningScenario[0] = -(Math.Round(worstCase, 0));
            //scanningScenario[1] = priceRisk;
            var scanningScenario = new decimal[2];
            scanningScenario[0] = scanRange.Min(n => n.Value);
            return scanningScenario;
        }


        private List<RiskArrayParameterNasdaq> LoadRiskArrayParameters()
        {
            var raParams = new List<RiskArrayParameterNasdaq>()
            {
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 1,
                    PriceVariation = 0,
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 2,
                    PriceVariation = 0,
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 3,
                    PriceVariation = new decimal(0.33333333),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 4,
                    PriceVariation = new decimal(0.33333333),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 5,
                    PriceVariation = -new decimal(0.33333333),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 6,
                    PriceVariation = -new decimal(0.33333333),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 7,
                    PriceVariation = new decimal(0.66666667),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 8,
                    PriceVariation = new decimal(0.66666667),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 9,
                    PriceVariation = -new decimal(0.66666667),
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 10,
                    PriceVariation = -new decimal(0.66666667),
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 11,
                    PriceVariation = 1,
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 12,
                    PriceVariation = 1,
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 13,
                    PriceVariation = -1,
                    VolatilityVariation = 1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 14,
                    PriceVariation = -1,
                    VolatilityVariation = -1,
                    WeightFraction = 1
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 15,
                    PriceVariation = 2,
                    VolatilityVariation = 0,
                    WeightFraction = new decimal(0.3)
                },
                new RiskArrayParameterNasdaq()
                {
                    RiskScenario = 16,
                    PriceVariation = -2,
                    VolatilityVariation = 0,
                    WeightFraction = new decimal(0.3)
                },
            };

            return raParams;
        }

        private void LoadInitialMargin(List<InitialMarginNasdaq> initialMargins)
        {
            gridInitialMargin.DataSource = initialMargins;
            gridInitialMargin.RefreshDataSource();
        }

        private void InitInitialMarginGrid()
        {
            gridInitialMargin.ForceInitialize();

            var gcIndex = new GridColumn { Name = "Contract Code", FieldName = "SpanCode", Caption = "Contract Code", Visible = true };
            gridViewInitialMargin.Columns.Add(gcIndex);

            var gcDailyFix = new GridColumn
            {
                Name = "Daily Fix",
                FieldName = "DailyFix",
                Caption = "Daily Fix",
                Visible = true
            };
            gcDailyFix.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcDailyFix.DisplayFormat.FormatString = "n2";
            gcDailyFix.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcDailyFix);

            var gcScanningRange = new GridColumn
            {
                Name = "Scanning Range",
                FieldName = "ScanningRange",
                Caption = "Scanning Range",
                Visible = true
            };
            gcScanningRange.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcScanningRange.DisplayFormat.FormatString = "n2";
            gcScanningRange.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcScanningRange);

            var gcNakedInitialMargin = new GridColumn
            {
                Name = "NakedInitialMargin",
                FieldName = "NakedInitialMargin",
                Caption = "Naked Initial Margin",
                Visible = true
            };
            gcNakedInitialMargin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcNakedInitialMargin.DisplayFormat.FormatString = "n2";
            gcNakedInitialMargin.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcNakedInitialMargin);

            var gcInitialMargin = new GridColumn
            {
                Name = "InitialMargin",
                FieldName = "InitialMarginValue",
                Caption = "Initial Margin",
                Visible = true
            };
            gcInitialMargin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gcInitialMargin.DisplayFormat.FormatString = "n2";
            gcInitialMargin.Summary.Add(new GridColumnSummaryItem() { DisplayFormat = "Sum = {0:n2}", SummaryType = SummaryItemType.Sum });
            gridViewInitialMargin.Columns.Add(gcInitialMargin);

            gridViewInitialMargin.ColumnPanelRowHeight = 10;
            gridInitialMargin.BeginUpdate();
            gridInitialMargin.EndUpdate();

        }

        #endregion

        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (System.Reflection.PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

    }


    #region Classes
    internal class tradelist
    {
        public string ticker;
        public string risk_group;
        public string price;
        public string contractid;
        public string cortablename;
        public RiskVals risk = new RiskVals();
        public Correlation corrTable = new Correlation();
    }



    public struct RiskVals
    {
        public string contractid;
        public string intervalsize;
        public string basevolatility;
        public string val1;
        public string val2;
        public string val3;
        public string val4;
        public string val5;
        public string val6;
        public string val7;
        public string val8;
        public string val9;
        public string val10;
        public string val11;
        public string val12;
        public string val13;
        public string val14;
        public string val15;
        public string val16;
        public string delta;
        public string dvc;
    }

    internal class ContractNasdaq
    {
        public string Index { get; set; }
        public string Code { get; set; }
        public string SpanCode { get; set; }
        public string CallPut { get; set; }
        public decimal Strike { get; set; }
        public decimal DailyFix { get; set; }

        public List<ContractExpiryDetail> ExpiryDetails { get; set; }

        public ContractNasdaq()
        {
            ExpiryDetails = new List<ContractExpiryDetail>();
        }
    }
    internal class InstrumentNasdaq
    {
        public string Index { get; set; }
        public string Code { get; set; }
        public string SpanCode { get; set; }
        public decimal DailyFix { get; set; }
    }
    internal class ContractExpiryDetailNasdaq
    {
        public DateTime Date { get; set; }
        public string Expiry { get; set; }
        public string SpanExpiry { get; set; }
        public decimal SettlementPrice { get; set; }
        public decimal CompositeData { get; set; }
        public int FileRow { get; set; }

        public decimal[] LossScenarios { get; set; }

        public ContractExpiryDetailNasdaq()
        {
            LossScenarios = new decimal[16];

        }
    }

    internal class TmpEnclearNasdaq
    {
        public string ContractCode { get; set; }
        public string Contract { get; set; }
        public string ScanningRange { get; set; }
        public decimal IntermonthSpreadCharge { get; set; }
        public decimal VolatitilyRange { get; set; }
        public int ShortOptionMinimum { get; set; }
        public int SpotMonthChargeMethod { get; set; }
        public int MarginTopUpRate { get; set; }
        public int SpotMarginleviedFrom { get; set; }
        public DateTime DateEffectiveFrom { get; set; }
        public int FrontMonthsForApplicationOfSpotMonthCharge { get; set; }
    }

    internal class TmpScanningRangeNasdaq
    {
        public string ContractCode { get; set; }
        public string Contract { get; set; }
        public decimal ScanningRange { get; set; }
        public List<TierDiscount> TierDiscounts { get; set; }

        public TmpScanningRangeNasdaq()
        {
            TierDiscounts = new List<TierDiscount>();
        }

    }

    internal class TierDiscountNasdaq
    {
        public decimal Rate { get; set; }
        public string DateRange { get; set; }
        public DateTime TierFrom { get; set; }
        public DateTime TierTo { get; set; }
    }

    internal class TmpIntercommodityCreditsNasdaq
    {
        public string Code1 { get; set; }
        public int DeltaRatioCode1 { get; set; }

        public string Code2 { get; set; }
        public int DeltaRatioCode2 { get; set; }

        public int Properties { get; set; }
        public int CreditPerLeg { get; set; }
        public DateTime DateEffectiveForm { get; set; }
    }

    internal class RiskArrayParameterNasdaq
    {
        public short RiskScenario { get; set; }
        public decimal PriceVariation { get; set; }
        public int VolatilityVariation { get; set; }
        public decimal WeightFraction { get; set; }
    }

    internal class IndexLossScenariosNasdaq
    {
        public string Index { get; set; }
        public string Expiry { get; set; }
        public string CallPut { get; set; }
        public decimal Strike { get; set; }
        public int FileRow { get; set; }
        public decimal SettlementPrice { get; set; }
        public decimal CompositeDelta { get; set; }

        public decimal LossScenario1 { get; set; }
        public decimal LossScenario2 { get; set; }
        public decimal LossScenario3 { get; set; }
        public decimal LossScenario4 { get; set; }
        public decimal LossScenario5 { get; set; }
        public decimal LossScenario6 { get; set; }
        public decimal LossScenario7 { get; set; }
        public decimal LossScenario8 { get; set; }
        public decimal LossScenario9 { get; set; }
        public decimal LossScenario10 { get; set; }
        public decimal LossScenario11 { get; set; }
        public decimal LossScenario12 { get; set; }
        public decimal LossScenario13 { get; set; }
        public decimal LossScenario14 { get; set; }
        public decimal LossScenario15 { get; set; }
        public decimal LossScenario16 { get; set; }
    }
    internal class ContractDailyFix
    {
        public string contract { get; set; }
        public decimal dailyfix { get; set; }
    }
    internal class volcor
    {
        public string nindex;
        public string table_type;
        public DataTable NasdaqVolCorrTable;
    }
    internal class InitialMarginNasdaq
    {
        public string Index { get; set; }
        public string SpanCode { get; set; }
        //public decimal Price { get; set; }
        //public decimal StrikePrice { get; set; }
        public decimal DailyFix { get; set; }
        public decimal ScanningRange { get; set; }
        public decimal NakedInitialMargin { get; set; }
        public decimal TimeSpreadCredit { get; set; }
        public decimal InitialMarginValue { get; set; }
    }

    public class OptionPositionValuesNasdaq
    {
        public string Index { get; set; }
        public string CallOrPut { get; set; }
        public decimal StrikePrice { get; set; }

        public Dictionary<DateTime, decimal> PosValues;
    }

    //public class ContractsInstrumenSeries
    //{
    //    public string Instr { get; set; }
    //    public List<DatePerids> DatePeriods { get; set; }

    //    public TmpScanningRangeNasdaq()
    //    {
    //        DatePeriods = new List<DatePeriods>();
    //    }
    //}
    internal class DeltaValuesNasdaq
    {
        public string Code { get; set; }

        public Dictionary<DateTime, decimal> DateValuesNasdaq { get; set; }

        public DeltaValuesNasdaq()
        {
            DateValuesNasdaq = new Dictionary<DateTime, decimal>();
        }
    }



    #endregion


}
