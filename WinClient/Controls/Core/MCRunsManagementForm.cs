﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient.Controls.Core
{
    public partial class MCRunsManagementForm : XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;

        //private MCSimulation _mcSimulation = new MCSimulation();

        #endregion

        #region Constructors

        public MCRunsManagementForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        #endregion

        #region GUI Events

        private void MCRunsManagementFormLoad(object sender, EventArgs e)
        {
            BeginGetMCRuns();
        }

        private void btnDelete_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to delete this row?",
                    "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                BeginDeleteMCRuns((MCSimulationInfo)gridMCRunsMainView.GetFocusedRow());
            }
            
            //XtraMessageBox.Show(this,
            //                       "Action not implemented yet!",
            //                       Strings.Freight_Metrics,
            //                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            var mcSimulation = (MCSimulationInfo) gridMCRunsMainView.GetFocusedRow();
            if (mcSimulation.Status == MCStatusEnum.Running)
            {
                XtraMessageBox.Show(this,
                                   "This simulation is still running. Please wait until it is completed to view the results.",
                                   Strings.Freight_Metrics,
                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (mcSimulation.Status == MCStatusEnum.Failed)
            {
                XtraMessageBox.Show(this,
                                   "There are no results for this simulation since it has failed.",
                                   Strings.Freight_Metrics,
                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            BeginGetMCSimulationDetails(mcSimulation);
        }

        private void btnRefresh_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BeginGetMCRuns();
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void MCRunsManagementForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void MCRunsManagementForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            gridMCRuns.BeginUpdate();

            int gridViewColumnindex = 0;

            gridMCRunsMainView.Columns.Clear();

            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "Id",
                                                    FieldName = "Id",
                                                    VisibleIndex = gridViewColumnindex++,
                                                    Width = 100
                                                });

            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Name,
                                                    FieldName = "Name",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = Strings.Status,             
                                                    FieldName = "Status",
                                                    VisibleIndex = gridViewColumnindex++
                                                });     
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "StartTime",
                                                    FieldName = "StartTime",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "EndTime",
                                                    FieldName = "EndTime",
                                                    VisibleIndex = gridViewColumnindex++
                                                });
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                       {
                                                           Caption = "Period From",
                                                           FieldName = "PeriodFrom",
                                                           VisibleIndex = gridViewColumnindex++
                                                       });
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "Period To",
                                                    FieldName = "PeriodTo",
                                                    VisibleIndex = gridViewColumnindex
                                                });

            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "Curve Date",
                                                    FieldName = "CurveDate",
                                                    VisibleIndex = gridViewColumnindex
                                                });
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "Pos Method",
                                                    FieldName = "PosMethod",
                                                    VisibleIndex = gridViewColumnindex
                                                });
            gridMCRunsMainView.Columns.Add(new GridColumn
                                                {
                                                    Caption = "No of Runs",
                                                    FieldName = "NoOfRuns",
                                                    VisibleIndex = gridViewColumnindex
                                                });

            gridMCRuns.EndUpdate();

            gridMCRunsMainView.Columns["StartTime"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            gridMCRunsMainView.Columns["StartTime"].DisplayFormat.FormatString = "g";

            gridMCRunsMainView.Columns["EndTime"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            gridMCRunsMainView.Columns["EndTime"].DisplayFormat.FormatString = "g";
        }

        private void OpenMCSimulation(MCSimulation mcSimulation)
        {
            Dictionary<DateTime, List<double>> monthFrequency;
            List<double> cashBounds;
            List<Dictionary<DateTime, decimal>> allValues =new List<Dictionary<DateTime, decimal>>();

            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            monthFrequency =
                (mcSimulation.MonthFrequency.OrderBy(a => a.Date).ThenBy(a => a.HistNo).GroupBy(a => a.Date).
                    ToDictionary(b => b.Key, b => b.Select(r => r.Value).ToList()));

            cashBounds = mcSimulation.Cashbound.Select(x => x.Value).ToList();

            //var y = mcSimulation.AllValues.OrderBy(a=>a.SimNo).GroupBy(a => a.SimNo).ToDictionary(a=>a.Key, a=>a.Select(r => r.Value));

            var simNo = mcSimulation.Info.NoOfRuns;

            for (var i = 0; i < simNo; i++)
            {
                var z = mcSimulation.AllValues.Where(n => n.SimNo == i).GroupBy(n => n.Date).ToDictionary(n => n.Key,
                                                                                                          n =>
                                                                                                          n.Select(
                                                                                                              a =>
                                                                                                              a.Value).FirstOrDefault());

                allValues.Add(z);
            }

            //Parallel.For(0, simNo, i =>
            //                           {
            //                               var z = mcSimulation.AllValues.Where(n => n.SimNo == i).GroupBy(n => n.Date).ToDictionary(n => n.Key,
            //                                                                                                                         n =>
            //                                                                                                                         n.Select(
            //                                                                                                                             a =>
            //                                                                                                                             a.Value).FirstOrDefault
            //                                                                                                                             ());

            //                               allValues.Add(z);
            //                           });


            Cursor = Cursors.Default;
            lock (_syncObject)
            {
                _dialogForm.Close();
                _dialogForm = null;
            }


            if (MonteCarloSimulationEvent != null) MonteCarloSimulationEvent(mcSimulation, monthFrequency, cashBounds, allValues);
        }
        #endregion

        #region Proxy Calls

        private void BeginGetMCRuns()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                //SessionRegistry.Client.BeginGetMCRuns(new Test1(), EndGetMCRuns, null);
                SessionRegistry.Client.BeginGetMCRuns(EndGetMCRuns, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetMCRuns(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetMCRuns;
                Invoke(action, ar);
                return;
            }
            int? result;
            //List<DomainObject> mcRuns;
            List<MCSimulationInfo> mcRuns;
            try
            {
                result = SessionRegistry.Client.EndGetMCRuns(out mcRuns, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                //gridMCRuns.DataSource = mcRuns.Cast<Test1>().ToList();
                gridMCRuns.DataSource = mcRuns.OrderByDescending(a=>a.Id).ToList();
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginGetMCSimulationDetails(MCSimulationInfo mcSimInfo)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                //SessionRegistry.Client.BeginGetMCRuns(new Test1(), EndGetMCRuns, null);
                SessionRegistry.Client.BeginGetMCSimulationDetails(mcSimInfo, EndGetMCSimulationDetails, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void EndGetMCSimulationDetails(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetMCSimulationDetails;
                Invoke(action, ar);
                return;
            }
            int? result;

            MCSimulation mcSimulation;
            try
            {
                result = SessionRegistry.Client.EndGetMCSimulationDetails(out mcSimulation, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                //fire event for opening new window

                //Transform data for correct entry in OnMonteCarloSimulation screen
                OpenMCSimulation(mcSimulation);

                //OnMonteCarloSimulation(Dictionary<DateTime, List<double>> monthFrequency, List<double> cashBounds, List<Dictionary<DateTime, decimal>> allValues)
                //ViewMCSimulationEvent(mcSimulation);

                //gridMCRuns.DataSource = mcRuns.Cast<Test1>().ToList();
                //gridMCRuns.DataSource = mcRuns.ToList();
                
            }
        }

        private void BeginDeleteMCRuns(MCSimulationInfo mcSimulationInfo)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                //SessionRegistry.Client.BeginGetMCRuns(new Test1(), EndGetMCRuns, null);
                SessionRegistry.Client.BeginDeleteMCRuns(mcSimulationInfo, EndDeleteMCRuns, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndDeleteMCRuns(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndDeleteMCRuns;
                Invoke(action, ar);
                return;
            }
            int? result;
          
            try
            {
                result = SessionRegistry.Client.EndDeleteMCRuns(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                RefreshData();
                //gridMCRuns.DataSource = mcRuns.Cast<Test1>().ToList();
             
                
               
            }
        }

        private void BeginInsertMCRuns()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                //SessionRegistry.Client.BeginGetMCRuns(new Test1(), EndGetMCRuns, null);
                SessionRegistry.Client.BeginInsertMCRuns(EndInsertMCRuns, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void EndInsertMCRuns(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndInsertMCRuns;
                Invoke(action, ar);
                return;
            }
            int? result;
            //List<DomainObject> mcRuns;
            string message;
            try
            {
                result = SessionRegistry.Client.EndInsertMCRuns(out message, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    message,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                //gridMCRuns.DataSource = mcRuns.Cast<Test1>().ToList();
               
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                XtraMessageBox.Show(this,
                                   message,
                                   Strings.Freight_Metrics,
                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


       
        #endregion

        #region Events

        //public event Action<Type> InsertEntityEvent;
        //public event Action<DomainObject> EditEntityEvent;
        //public event Action<DomainObject> ViewEntityEvent;
        //public event Action<object, bool> OnDataSaved;
        public event Action<MCSimulation> ViewMCSimulationEvent;
        public event Action<MCSimulation, Dictionary<DateTime, List<double>>, List<double>, List<Dictionary<DateTime, decimal>>> MonteCarloSimulationEvent;
        public event Action<object, bool> OnDataSaved;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            btnRefresh.PerformClick();
        }

        #endregion

      
    }
}


