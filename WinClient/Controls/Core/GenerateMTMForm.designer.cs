﻿namespace Exis.WinClient.Controls.Core
{
    partial class GenerateMTMForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenerateMTMForm));
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.lblOnlyNonZeroTotalDays = new DevExpress.XtraEditors.LabelControl();
            this.rdgPositionResultsType = new DevExpress.XtraEditors.RadioGroup();
            this.lblIsOptionalPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblIsMinimumPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblIsDraft = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodTo = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodFrom = new DevExpress.XtraEditors.LabelControl();
            this.lblEntity = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkCalculateCommissions = new DevExpress.XtraEditors.CheckEdit();
            this.chkCalculateSums = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionNullify = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionEmbeddedValue = new DevExpress.XtraEditors.CheckEdit();
            this.chkOptionPremium = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseSpotValues = new DevExpress.XtraEditors.CheckEdit();
            this.dtpFilterCurveDate = new DevExpress.XtraEditors.DateEdit();
            this.rdgMTMResultsType = new DevExpress.XtraEditors.RadioGroup();
            this.rdgResultsAggregationType = new DevExpress.XtraEditors.RadioGroup();
            this.txtMarketSensitivityValue = new DevExpress.XtraEditors.SpinEdit();
            this.cmbMarketSensitivityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPositionMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnExportResults = new DevExpress.XtraEditors.SimpleButton();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnGeneratePosition = new DevExpress.XtraEditors.SimpleButton();
            this.gridTradeActionResults = new DevExpress.XtraGrid.GridControl();
            this.gridTradeActionResultsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutGroupRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupTrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemGeneratePosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutFilterCurveDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPositionMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMarketSensitivityValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMTMResultsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionPremium = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionNullify = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOptionEmbeddedValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUseSpotValues = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCalculateSums = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCalculateCommissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupTradeResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupTradeActionResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutItemTradeActionResults = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutResultsActions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutExportResults = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutResultsAggregationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutPositionResultsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList16 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlRoot = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdgPositionResultsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionNullify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionEmbeddedValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgMTMResultsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResultsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGeneratePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMTMResultsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionNullify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionEmbeddedValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateCommissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTradeResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeActionResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExportResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsAggregationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionResultsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.AllowCustomizationMenu = false;
            this.layoutRoot.Controls.Add(this.lblOnlyNonZeroTotalDays);
            this.layoutRoot.Controls.Add(this.rdgPositionResultsType);
            this.layoutRoot.Controls.Add(this.lblIsOptionalPeriod);
            this.layoutRoot.Controls.Add(this.lblIsMinimumPeriod);
            this.layoutRoot.Controls.Add(this.lblIsDraft);
            this.layoutRoot.Controls.Add(this.lblPeriodTo);
            this.layoutRoot.Controls.Add(this.lblPeriodFrom);
            this.layoutRoot.Controls.Add(this.lblEntity);
            this.layoutRoot.Controls.Add(this.labelControl1);
            this.layoutRoot.Controls.Add(this.chkCalculateCommissions);
            this.layoutRoot.Controls.Add(this.chkCalculateSums);
            this.layoutRoot.Controls.Add(this.chkOptionNullify);
            this.layoutRoot.Controls.Add(this.chkOptionEmbeddedValue);
            this.layoutRoot.Controls.Add(this.chkOptionPremium);
            this.layoutRoot.Controls.Add(this.chkUseSpotValues);
            this.layoutRoot.Controls.Add(this.dtpFilterCurveDate);
            this.layoutRoot.Controls.Add(this.rdgMTMResultsType);
            this.layoutRoot.Controls.Add(this.rdgResultsAggregationType);
            this.layoutRoot.Controls.Add(this.txtMarketSensitivityValue);
            this.layoutRoot.Controls.Add(this.cmbMarketSensitivityType);
            this.layoutRoot.Controls.Add(this.cmbPositionMethod);
            this.layoutRoot.Controls.Add(this.btnExportResults);
            this.layoutRoot.Controls.Add(this.btnGeneratePosition);
            this.layoutRoot.Controls.Add(this.gridTradeActionResults);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1643, 0, 426, 731);
            this.layoutRoot.Root = this.layoutGroupRoot;
            this.layoutRoot.Size = new System.Drawing.Size(1219, 645);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // lblOnlyNonZeroTotalDays
            // 
            this.lblOnlyNonZeroTotalDays.Location = new System.Drawing.Point(1300, 31);
            this.lblOnlyNonZeroTotalDays.Name = "lblOnlyNonZeroTotalDays";
            this.lblOnlyNonZeroTotalDays.Size = new System.Drawing.Size(61, 14);
            this.lblOnlyNonZeroTotalDays.StyleController = this.layoutRoot;
            this.lblOnlyNonZeroTotalDays.TabIndex = 45;
            // 
            // rdgPositionResultsType
            // 
            this.rdgPositionResultsType.Location = new System.Drawing.Point(636, 149);
            this.rdgPositionResultsType.Name = "rdgPositionResultsType";
            this.rdgPositionResultsType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Days"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Vessels"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Tonnes")});
            this.rdgPositionResultsType.Size = new System.Drawing.Size(479, 30);
            this.rdgPositionResultsType.StyleController = this.layoutRoot;
            this.rdgPositionResultsType.TabIndex = 35;
            this.rdgPositionResultsType.SelectedIndexChanged += new System.EventHandler(this.RdgPositionResultsTypeSelectedIndexChanged);
            // 
            // lblIsOptionalPeriod
            // 
            this.lblIsOptionalPeriod.Location = new System.Drawing.Point(1039, 31);
            this.lblIsOptionalPeriod.Name = "lblIsOptionalPeriod";
            this.lblIsOptionalPeriod.Size = new System.Drawing.Size(61, 14);
            this.lblIsOptionalPeriod.StyleController = this.layoutRoot;
            this.lblIsOptionalPeriod.TabIndex = 34;
            // 
            // lblIsMinimumPeriod
            // 
            this.lblIsMinimumPeriod.Location = new System.Drawing.Point(864, 31);
            this.lblIsMinimumPeriod.Name = "lblIsMinimumPeriod";
            this.lblIsMinimumPeriod.Size = new System.Drawing.Size(61, 14);
            this.lblIsMinimumPeriod.StyleController = this.layoutRoot;
            this.lblIsMinimumPeriod.TabIndex = 33;
            // 
            // lblIsDraft
            // 
            this.lblIsDraft.Location = new System.Drawing.Point(699, 31);
            this.lblIsDraft.Name = "lblIsDraft";
            this.lblIsDraft.Size = new System.Drawing.Size(61, 14);
            this.lblIsDraft.StyleController = this.layoutRoot;
            this.lblIsDraft.TabIndex = 32;
            // 
            // lblPeriodTo
            // 
            this.lblPeriodTo.Location = new System.Drawing.Point(564, 31);
            this.lblPeriodTo.Name = "lblPeriodTo";
            this.lblPeriodTo.Size = new System.Drawing.Size(76, 14);
            this.lblPeriodTo.StyleController = this.layoutRoot;
            this.lblPeriodTo.TabIndex = 31;
            // 
            // lblPeriodFrom
            // 
            this.lblPeriodFrom.Location = new System.Drawing.Point(409, 31);
            this.lblPeriodFrom.Name = "lblPeriodFrom";
            this.lblPeriodFrom.Size = new System.Drawing.Size(76, 14);
            this.lblPeriodFrom.StyleController = this.layoutRoot;
            this.lblPeriodFrom.TabIndex = 30;
            // 
            // lblEntity
            // 
            this.lblEntity.Location = new System.Drawing.Point(238, 31);
            this.lblEntity.Name = "lblEntity";
            this.lblEntity.Size = new System.Drawing.Size(92, 14);
            this.lblEntity.StyleController = this.layoutRoot;
            this.lblEntity.TabIndex = 29;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.labelControl1.Location = new System.Drawing.Point(12, 31);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.labelControl1.Size = new System.Drawing.Size(177, 14);
            this.labelControl1.StyleController = this.layoutRoot;
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = "Filter Defined in View Trades:";
            // 
            // chkCalculateCommissions
            // 
            this.chkCalculateCommissions.Location = new System.Drawing.Point(1198, 82);
            this.chkCalculateCommissions.Name = "chkCalculateCommissions";
            this.chkCalculateCommissions.Properties.Caption = "";
            this.chkCalculateCommissions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCalculateCommissions.Size = new System.Drawing.Size(70, 19);
            this.chkCalculateCommissions.StyleController = this.layoutRoot;
            this.chkCalculateCommissions.TabIndex = 22;
            // 
            // chkCalculateSums
            // 
            this.chkCalculateSums.Location = new System.Drawing.Point(1066, 82);
            this.chkCalculateSums.Name = "chkCalculateSums";
            this.chkCalculateSums.Properties.Caption = "";
            this.chkCalculateSums.Size = new System.Drawing.Size(58, 19);
            this.chkCalculateSums.StyleController = this.layoutRoot;
            this.chkCalculateSums.TabIndex = 21;
            // 
            // chkOptionNullify
            // 
            this.chkOptionNullify.Location = new System.Drawing.Point(638, 82);
            this.chkOptionNullify.Name = "chkOptionNullify";
            this.chkOptionNullify.Properties.Caption = "";
            this.chkOptionNullify.Size = new System.Drawing.Size(69, 19);
            this.chkOptionNullify.StyleController = this.layoutRoot;
            this.chkOptionNullify.TabIndex = 22;
            // 
            // chkOptionEmbeddedValue
            // 
            this.chkOptionEmbeddedValue.Location = new System.Drawing.Point(808, 82);
            this.chkOptionEmbeddedValue.Name = "chkOptionEmbeddedValue";
            this.chkOptionEmbeddedValue.Properties.Caption = "";
            this.chkOptionEmbeddedValue.Size = new System.Drawing.Size(90, 19);
            this.chkOptionEmbeddedValue.StyleController = this.layoutRoot;
            this.chkOptionEmbeddedValue.TabIndex = 21;
            // 
            // chkOptionPremium
            // 
            this.chkOptionPremium.Location = new System.Drawing.Point(466, 82);
            this.chkOptionPremium.Name = "chkOptionPremium";
            this.chkOptionPremium.Properties.Caption = "";
            this.chkOptionPremium.Size = new System.Drawing.Size(95, 19);
            this.chkOptionPremium.StyleController = this.layoutRoot;
            this.chkOptionPremium.TabIndex = 21;
            // 
            // chkUseSpotValues
            // 
            this.chkUseSpotValues.Location = new System.Drawing.Point(954, 82);
            this.chkUseSpotValues.Name = "chkUseSpotValues";
            this.chkUseSpotValues.Properties.Caption = "";
            this.chkUseSpotValues.Size = new System.Drawing.Size(65, 19);
            this.chkUseSpotValues.StyleController = this.layoutRoot;
            this.chkUseSpotValues.TabIndex = 20;
            // 
            // dtpFilterCurveDate
            // 
            this.dtpFilterCurveDate.EditValue = null;
            this.dtpFilterCurveDate.Location = new System.Drawing.Point(79, 58);
            this.dtpFilterCurveDate.Name = "dtpFilterCurveDate";
            this.dtpFilterCurveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFilterCurveDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpFilterCurveDate.Size = new System.Drawing.Size(191, 20);
            this.dtpFilterCurveDate.StyleController = this.layoutRoot;
            this.dtpFilterCurveDate.TabIndex = 10;
            // 
            // rdgMTMResultsType
            // 
            this.rdgMTMResultsType.Location = new System.Drawing.Point(73, 82);
            this.rdgMTMResultsType.Name = "rdgMTMResultsType";
            this.rdgMTMResultsType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CASH FLOW", "Cash Flow"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("P&L", "P&&L"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("COMBINED", "Combined")});
            this.rdgMTMResultsType.Size = new System.Drawing.Size(305, 20);
            this.rdgMTMResultsType.StyleController = this.layoutRoot;
            this.rdgMTMResultsType.TabIndex = 25;
            // 
            // rdgResultsAggregationType
            // 
            this.rdgResultsAggregationType.Location = new System.Drawing.Point(83, 149);
            this.rdgResultsAggregationType.Name = "rdgResultsAggregationType";
            this.rdgResultsAggregationType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Month"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Quarter"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Calendar")});
            this.rdgResultsAggregationType.Size = new System.Drawing.Size(484, 30);
            this.rdgResultsAggregationType.StyleController = this.layoutRoot;
            this.rdgResultsAggregationType.TabIndex = 23;
            this.rdgResultsAggregationType.SelectedIndexChanged += new System.EventHandler(this.RdgResultsAggregationTypeSelectedIndexChanged);
            // 
            // txtMarketSensitivityValue
            // 
            this.txtMarketSensitivityValue.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtMarketSensitivityValue.Location = new System.Drawing.Point(1122, 58);
            this.txtMarketSensitivityValue.Name = "txtMarketSensitivityValue";
            this.txtMarketSensitivityValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMarketSensitivityValue.Properties.Mask.EditMask = "D";
            this.txtMarketSensitivityValue.Properties.MaxLength = 7;
            this.txtMarketSensitivityValue.Size = new System.Drawing.Size(146, 20);
            this.txtMarketSensitivityValue.StyleController = this.layoutRoot;
            this.txtMarketSensitivityValue.TabIndex = 9;
            // 
            // cmbMarketSensitivityType
            // 
            this.cmbMarketSensitivityType.Location = new System.Drawing.Point(728, 58);
            this.cmbMarketSensitivityType.Name = "cmbMarketSensitivityType";
            this.cmbMarketSensitivityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbMarketSensitivityType.Properties.Items.AddRange(new object[] {
            "None",
            "Absolute",
            "Percentage",
            "Stress"});
            this.cmbMarketSensitivityType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbMarketSensitivityType.Size = new System.Drawing.Size(267, 20);
            this.cmbMarketSensitivityType.StyleController = this.layoutRoot;
            this.cmbMarketSensitivityType.TabIndex = 8;
            this.cmbMarketSensitivityType.SelectedValueChanged += new System.EventHandler(this.CmbMarketSensitivityTypeSelectedValueChanged);
            // 
            // cmbPositionMethod
            // 
            this.cmbPositionMethod.Location = new System.Drawing.Point(359, 58);
            this.cmbPositionMethod.Name = "cmbPositionMethod";
            this.cmbPositionMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPositionMethod.Properties.Items.AddRange(new object[] {
            "Static",
            "Dynamic",
            "Delta"});
            this.cmbPositionMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPositionMethod.Size = new System.Drawing.Size(242, 20);
            this.cmbPositionMethod.StyleController = this.layoutRoot;
            this.cmbPositionMethod.TabIndex = 22;
            this.cmbPositionMethod.SelectedValueChanged += new System.EventHandler(this.cmbPositionMethod_SelectedValueChanged);
            // 
            // btnExportResults
            // 
            this.btnExportResults.ImageIndex = 8;
            this.btnExportResults.ImageList = this.imageList24;
            this.btnExportResults.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnExportResults.Location = new System.Drawing.Point(1291, 148);
            this.btnExportResults.Name = "btnExportResults";
            this.btnExportResults.Size = new System.Drawing.Size(78, 32);
            this.btnExportResults.StyleController = this.layoutRoot;
            this.btnExportResults.TabIndex = 13;
            this.btnExportResults.Text = "Export";
            this.btnExportResults.Click += new System.EventHandler(this.btnExportResults_Click);
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "addItem24x24.ico");
            this.imageList24.Images.SetKeyName(1, "editItem24x24.ico");
            this.imageList24.Images.SetKeyName(2, "refresh24x24.ico");
            this.imageList24.Images.SetKeyName(3, "deleteItem24x24.ico");
            this.imageList24.Images.SetKeyName(4, "saveItem24x24.ico");
            this.imageList24.Images.SetKeyName(5, "exit.ico");
            this.imageList24.Images.SetKeyName(6, "gantt-chart.ico");
            this.imageList24.Images.SetKeyName(7, "line-chart.ico");
            this.imageList24.Images.SetKeyName(8, "export24x24.ico");
            this.imageList24.Images.SetKeyName(9, "view24x24.ico");
            this.imageList24.Images.SetKeyName(10, "activate.ico");
            this.imageList24.Images.SetKeyName(11, "deactivate.ico");
            this.imageList24.Images.SetKeyName(12, "text_sum.ico");
            // 
            // btnGeneratePosition
            // 
            this.btnGeneratePosition.ImageIndex = 7;
            this.btnGeneratePosition.ImageList = this.imageList24;
            this.btnGeneratePosition.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGeneratePosition.Location = new System.Drawing.Point(1277, 53);
            this.btnGeneratePosition.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.btnGeneratePosition.Name = "btnGeneratePosition";
            this.btnGeneratePosition.Size = new System.Drawing.Size(96, 56);
            this.btnGeneratePosition.StyleController = this.layoutRoot;
            this.btnGeneratePosition.TabIndex = 8;
            this.btnGeneratePosition.Text = "Calculate";
            this.btnGeneratePosition.Click += new System.EventHandler(this.btnGeneratePosition_Click);
            // 
            // gridTradeActionResults
            // 
            this.gridTradeActionResults.Location = new System.Drawing.Point(9, 189);
            this.gridTradeActionResults.MainView = this.gridTradeActionResultsView;
            this.gridTradeActionResults.Name = "gridTradeActionResults";
            this.gridTradeActionResults.Size = new System.Drawing.Size(1365, 430);
            this.gridTradeActionResults.TabIndex = 6;
            this.gridTradeActionResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridTradeActionResultsView});
            // 
            // gridTradeActionResultsView
            // 
            this.gridTradeActionResultsView.GridControl = this.gridTradeActionResults;
            this.gridTradeActionResultsView.Name = "gridTradeActionResultsView";
            this.gridTradeActionResultsView.OptionsBehavior.Editable = false;
            this.gridTradeActionResultsView.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridTradeActionResultsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridTradeActionResultsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridTradeActionResultsView.OptionsView.ShowFooter = true;
            this.gridTradeActionResultsView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.GridTradeActionResultsViewCustomSummaryCalculate);
            this.gridTradeActionResultsView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridTradeActionResultsView_CustomUnboundColumnData);
            // 
            // layoutGroupRoot
            // 
            this.layoutGroupRoot.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutGroupRoot.GroupBordersVisible = false;
            this.layoutGroupRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTrades});
            this.layoutGroupRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupRoot.Name = "Root";
            this.layoutGroupRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupRoot.Size = new System.Drawing.Size(1383, 628);
            this.layoutGroupRoot.Text = "Root";
            this.layoutGroupRoot.TextVisible = false;
            // 
            // layoutGroupTrades
            // 
            this.layoutGroupTrades.CustomizationFormText = "Trades";
            this.layoutGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupTrades,
            this.layoutControlGroupTradeResults});
            this.layoutGroupTrades.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTrades.Name = "layoutGroupTrades";
            this.layoutGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTrades.Size = new System.Drawing.Size(1383, 628);
            this.layoutGroupTrades.Text = "Trades";
            this.layoutGroupTrades.TextVisible = false;
            // 
            // layoutControlGroupTrades
            // 
            this.layoutControlGroupTrades.CustomizationFormText = "Filters";
            this.layoutControlGroupTrades.ExpandButtonVisible = true;
            this.layoutControlGroupTrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeDetails});
            this.layoutControlGroupTrades.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTrades.Name = "layoutControlGroupTrades";
            this.layoutControlGroupTrades.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Size = new System.Drawing.Size(1377, 115);
            this.layoutControlGroupTrades.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTrades.Text = "Filters";
            // 
            // layoutGroupTradeDetails
            // 
            this.layoutGroupTradeDetails.CustomizationFormText = "layoutControlGroup1";
            this.layoutGroupTradeDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup2});
            this.layoutGroupTradeDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeDetails.Name = "layoutGroupTradeDetails";
            this.layoutGroupTradeDetails.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeDetails.Size = new System.Drawing.Size(1375, 94);
            this.layoutGroupTradeDetails.Text = "Details";
            this.layoutGroupTradeDetails.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemGeneratePosition,
            this.layoutControlGroup5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1369, 64);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutItemGeneratePosition
            // 
            this.layoutItemGeneratePosition.Control = this.btnGeneratePosition;
            this.layoutItemGeneratePosition.CustomizationFormText = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.Location = new System.Drawing.Point(1267, 0);
            this.layoutItemGeneratePosition.MaxSize = new System.Drawing.Size(100, 60);
            this.layoutItemGeneratePosition.MinSize = new System.Drawing.Size(100, 36);
            this.layoutItemGeneratePosition.Name = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.Size = new System.Drawing.Size(100, 62);
            this.layoutItemGeneratePosition.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemGeneratePosition.Text = "layoutItemGeneratePosition";
            this.layoutItemGeneratePosition.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutItemGeneratePosition.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemGeneratePosition.TextToControlDistance = 0;
            this.layoutItemGeneratePosition.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutFilterCurveDate,
            this.layoutPositionMethod,
            this.layoutMarketSensitivityType,
            this.layoutMarketSensitivityValue,
            this.layoutMTMResultsType,
            this.layoutOptionPremium,
            this.layoutOptionNullify,
            this.layoutOptionEmbeddedValue,
            this.layoutUseSpotValues,
            this.layoutItemCalculateSums,
            this.layoutItemCalculateCommissions});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1267, 62);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutFilterCurveDate
            // 
            this.layoutFilterCurveDate.Control = this.dtpFilterCurveDate;
            this.layoutFilterCurveDate.CustomizationFormText = "Curve Date:";
            this.layoutFilterCurveDate.Location = new System.Drawing.Point(0, 0);
            this.layoutFilterCurveDate.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutFilterCurveDate.MinSize = new System.Drawing.Size(118, 24);
            this.layoutFilterCurveDate.Name = "layoutFilterCurveDate";
            this.layoutFilterCurveDate.Size = new System.Drawing.Size(259, 24);
            this.layoutFilterCurveDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutFilterCurveDate.Text = "Curve Date:";
            this.layoutFilterCurveDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutFilterCurveDate.TextSize = new System.Drawing.Size(59, 13);
            this.layoutFilterCurveDate.TextToControlDistance = 5;
            // 
            // layoutPositionMethod
            // 
            this.layoutPositionMethod.Control = this.cmbPositionMethod;
            this.layoutPositionMethod.CustomizationFormText = "Position Method:";
            this.layoutPositionMethod.Location = new System.Drawing.Point(259, 0);
            this.layoutPositionMethod.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutPositionMethod.MinSize = new System.Drawing.Size(139, 24);
            this.layoutPositionMethod.Name = "layoutPositionMethod";
            this.layoutPositionMethod.Size = new System.Drawing.Size(331, 24);
            this.layoutPositionMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPositionMethod.Text = "Position Method:";
            this.layoutPositionMethod.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutPositionMethod.TextSize = new System.Drawing.Size(80, 13);
            this.layoutPositionMethod.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityType
            // 
            this.layoutMarketSensitivityType.Control = this.cmbMarketSensitivityType;
            this.layoutMarketSensitivityType.CustomizationFormText = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.Location = new System.Drawing.Point(590, 0);
            this.layoutMarketSensitivityType.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityType.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityType.Name = "layoutMarketSensitivityType";
            this.layoutMarketSensitivityType.Size = new System.Drawing.Size(394, 24);
            this.layoutMarketSensitivityType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityType.Text = "Market Sensitivity Type:";
            this.layoutMarketSensitivityType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityType.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityType.TextToControlDistance = 5;
            // 
            // layoutMarketSensitivityValue
            // 
            this.layoutMarketSensitivityValue.Control = this.txtMarketSensitivityValue;
            this.layoutMarketSensitivityValue.CustomizationFormText = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.Location = new System.Drawing.Point(984, 0);
            this.layoutMarketSensitivityValue.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMarketSensitivityValue.MinSize = new System.Drawing.Size(187, 24);
            this.layoutMarketSensitivityValue.Name = "layoutMarketSensitivityValue";
            this.layoutMarketSensitivityValue.Size = new System.Drawing.Size(273, 24);
            this.layoutMarketSensitivityValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMarketSensitivityValue.Text = "Market Sensitivity Value:";
            this.layoutMarketSensitivityValue.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMarketSensitivityValue.TextSize = new System.Drawing.Size(118, 13);
            this.layoutMarketSensitivityValue.TextToControlDistance = 5;
            // 
            // layoutMTMResultsType
            // 
            this.layoutMTMResultsType.Control = this.rdgMTMResultsType;
            this.layoutMTMResultsType.CustomizationFormText = "MTM Result Type:";
            this.layoutMTMResultsType.Location = new System.Drawing.Point(0, 24);
            this.layoutMTMResultsType.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutMTMResultsType.MinSize = new System.Drawing.Size(284, 24);
            this.layoutMTMResultsType.Name = "layoutMTMResultsType";
            this.layoutMTMResultsType.Size = new System.Drawing.Size(367, 28);
            this.layoutMTMResultsType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutMTMResultsType.Text = "MTM Type:";
            this.layoutMTMResultsType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutMTMResultsType.TextSize = new System.Drawing.Size(53, 13);
            this.layoutMTMResultsType.TextToControlDistance = 5;
            // 
            // layoutOptionPremium
            // 
            this.layoutOptionPremium.Control = this.chkOptionPremium;
            this.layoutOptionPremium.CustomizationFormText = "Option Premium:";
            this.layoutOptionPremium.Location = new System.Drawing.Point(367, 24);
            this.layoutOptionPremium.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionPremium.MinSize = new System.Drawing.Size(108, 24);
            this.layoutOptionPremium.Name = "layoutOptionPremium";
            this.layoutOptionPremium.Size = new System.Drawing.Size(183, 28);
            this.layoutOptionPremium.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionPremium.Text = "Option Premium:";
            this.layoutOptionPremium.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionPremium.TextSize = new System.Drawing.Size(79, 13);
            this.layoutOptionPremium.TextToControlDistance = 5;
            // 
            // layoutOptionNullify
            // 
            this.layoutOptionNullify.Control = this.chkOptionNullify;
            this.layoutOptionNullify.CustomizationFormText = "Option Nullify:";
            this.layoutOptionNullify.Location = new System.Drawing.Point(550, 24);
            this.layoutOptionNullify.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionNullify.MinSize = new System.Drawing.Size(97, 24);
            this.layoutOptionNullify.Name = "layoutOptionNullify";
            this.layoutOptionNullify.Size = new System.Drawing.Size(146, 28);
            this.layoutOptionNullify.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionNullify.Text = "Option Nullify:";
            this.layoutOptionNullify.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionNullify.TextSize = new System.Drawing.Size(68, 13);
            this.layoutOptionNullify.TextToControlDistance = 5;
            // 
            // layoutOptionEmbeddedValue
            // 
            this.layoutOptionEmbeddedValue.Control = this.chkOptionEmbeddedValue;
            this.layoutOptionEmbeddedValue.CustomizationFormText = "Option Embedded Value:";
            this.layoutOptionEmbeddedValue.Location = new System.Drawing.Point(696, 24);
            this.layoutOptionEmbeddedValue.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutOptionEmbeddedValue.MinSize = new System.Drawing.Size(121, 24);
            this.layoutOptionEmbeddedValue.Name = "layoutOptionEmbeddedValue";
            this.layoutOptionEmbeddedValue.Size = new System.Drawing.Size(191, 28);
            this.layoutOptionEmbeddedValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutOptionEmbeddedValue.Text = "Option Emb. Value:";
            this.layoutOptionEmbeddedValue.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutOptionEmbeddedValue.TextSize = new System.Drawing.Size(92, 13);
            this.layoutOptionEmbeddedValue.TextToControlDistance = 5;
            // 
            // layoutUseSpotValues
            // 
            this.layoutUseSpotValues.Control = this.chkUseSpotValues;
            this.layoutUseSpotValues.CustomizationFormText = "Use Spot:";
            this.layoutUseSpotValues.Location = new System.Drawing.Point(887, 24);
            this.layoutUseSpotValues.Name = "layoutUseSpotValues";
            this.layoutUseSpotValues.Size = new System.Drawing.Size(121, 28);
            this.layoutUseSpotValues.Text = "Use Spot:";
            this.layoutUseSpotValues.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutUseSpotValues.TextSize = new System.Drawing.Size(47, 13);
            this.layoutUseSpotValues.TextToControlDistance = 5;
            // 
            // layoutItemCalculateSums
            // 
            this.layoutItemCalculateSums.Control = this.chkCalculateSums;
            this.layoutItemCalculateSums.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutItemCalculateSums.CustomizationFormText = "Calculate Sums:";
            this.layoutItemCalculateSums.Location = new System.Drawing.Point(1008, 24);
            this.layoutItemCalculateSums.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutItemCalculateSums.MinSize = new System.Drawing.Size(70, 28);
            this.layoutItemCalculateSums.Name = "layoutItemCalculateSums";
            this.layoutItemCalculateSums.Size = new System.Drawing.Size(105, 28);
            this.layoutItemCalculateSums.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemCalculateSums.Text = "Totals:";
            this.layoutItemCalculateSums.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateSums.TextSize = new System.Drawing.Size(33, 13);
            this.layoutItemCalculateSums.TextToControlDistance = 10;
            // 
            // layoutItemCalculateCommissions
            // 
            this.layoutItemCalculateCommissions.Control = this.chkCalculateCommissions;
            this.layoutItemCalculateCommissions.CustomizationFormText = "Commissions";
            this.layoutItemCalculateCommissions.Location = new System.Drawing.Point(1113, 24);
            this.layoutItemCalculateCommissions.Name = "layoutItemCalculateCommissions";
            this.layoutItemCalculateCommissions.Size = new System.Drawing.Size(144, 28);
            this.layoutItemCalculateCommissions.Text = "Commissions";
            this.layoutItemCalculateCommissions.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutItemCalculateCommissions.TextSize = new System.Drawing.Size(60, 13);
            this.layoutItemCalculateCommissions.TextToControlDistance = 10;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "POS Filters:";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1369, 24);
            this.layoutControlGroup2.Text = "POS Filters:";
            this.layoutControlGroup2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(181, 18);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(181, 18);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            this.layoutControlItem4.TrimClientAreaToControl = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblEntity;
            this.layoutControlItem1.CustomizationFormText = "Book/XX Selected:";
            this.layoutControlItem1.Location = new System.Drawing.Point(181, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(141, 17);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(141, 18);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Enitity:";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(35, 13);
            this.layoutControlItem1.TextToControlDistance = 10;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblPeriodFrom;
            this.layoutControlItem2.CustomizationFormText = "Period From:";
            this.layoutControlItem2.Location = new System.Drawing.Point(322, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(155, 17);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(155, 18);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Period From:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem2.TextToControlDistance = 10;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lblIsDraft;
            this.layoutControlItem7.CustomizationFormText = "Is Draft:";
            this.layoutControlItem7.Location = new System.Drawing.Point(632, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(120, 17);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(120, 18);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Is Draft:";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(45, 13);
            this.layoutControlItem7.TextToControlDistance = 10;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lblPeriodTo;
            this.layoutControlItem6.CustomizationFormText = "Period To:";
            this.layoutControlItem6.Location = new System.Drawing.Point(477, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(155, 17);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(155, 18);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Period To:";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem6.TextToControlDistance = 10;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lblIsMinimumPeriod;
            this.layoutControlItem8.CustomizationFormText = "Is Minimum Period:";
            this.layoutControlItem8.Location = new System.Drawing.Point(752, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(165, 17);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(165, 18);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Is Minimum Period:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            this.layoutControlItem8.TextToControlDistance = 10;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.lblIsOptionalPeriod;
            this.layoutControlItem9.CustomizationFormText = "Is Optional Period:";
            this.layoutControlItem9.Location = new System.Drawing.Point(917, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(175, 17);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(175, 18);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "Is Optional Period:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 13);
            this.layoutControlItem9.TextToControlDistance = 10;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(1353, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 18);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lblOnlyNonZeroTotalDays;
            this.layoutControlItem3.CustomizationFormText = "Only trades with Total Days > 0:";
            this.layoutControlItem3.Location = new System.Drawing.Point(1092, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(261, 17);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(261, 18);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Only trades with Total Days > 0:";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(186, 13);
            this.layoutControlItem3.TextToControlDistance = 10;
            // 
            // layoutControlGroupTradeResults
            // 
            this.layoutControlGroupTradeResults.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroupTradeResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupTradeActionResults});
            this.layoutControlGroupTradeResults.Location = new System.Drawing.Point(0, 115);
            this.layoutControlGroupTradeResults.Name = "layoutControlGroupTradeResults";
            this.layoutControlGroupTradeResults.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTradeResults.Size = new System.Drawing.Size(1377, 507);
            this.layoutControlGroupTradeResults.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupTradeResults.Text = "layoutControlGroupTradeResults";
            this.layoutControlGroupTradeResults.TextVisible = false;
            // 
            // layoutGroupTradeActionResults
            // 
            this.layoutGroupTradeActionResults.CustomizationFormText = "Action Results";
            this.layoutGroupTradeActionResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutItemTradeActionResults,
            this.layoutResultsActions,
            this.layoutControlGroup4});
            this.layoutGroupTradeActionResults.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupTradeActionResults.Name = "layoutGroupTradeActionResults";
            this.layoutGroupTradeActionResults.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutGroupTradeActionResults.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutGroupTradeActionResults.Size = new System.Drawing.Size(1375, 505);
            this.layoutGroupTradeActionResults.Text = "Results";
            // 
            // layoutItemTradeActionResults
            // 
            this.layoutItemTradeActionResults.Control = this.gridTradeActionResults;
            this.layoutItemTradeActionResults.CustomizationFormText = "layoutItemTradeActionResults";
            this.layoutItemTradeActionResults.Location = new System.Drawing.Point(0, 46);
            this.layoutItemTradeActionResults.MinSize = new System.Drawing.Size(50, 204);
            this.layoutItemTradeActionResults.Name = "layoutItemTradeActionResults";
            this.layoutItemTradeActionResults.Size = new System.Drawing.Size(1369, 434);
            this.layoutItemTradeActionResults.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemTradeActionResults.Text = "layoutItemTradeActionResults";
            this.layoutItemTradeActionResults.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemTradeActionResults.TextToControlDistance = 0;
            this.layoutItemTradeActionResults.TextVisible = false;
            // 
            // layoutResultsActions
            // 
            this.layoutResultsActions.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutResultsActions.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutResultsActions.AppearanceGroup.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutResultsActions.CustomizationFormText = "Actions";
            this.layoutResultsActions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutExportResults});
            this.layoutResultsActions.Location = new System.Drawing.Point(1277, 0);
            this.layoutResultsActions.Name = "layoutResultsActions";
            this.layoutResultsActions.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutResultsActions.Size = new System.Drawing.Size(92, 46);
            this.layoutResultsActions.Text = "Actions";
            this.layoutResultsActions.TextVisible = false;
            // 
            // layoutExportResults
            // 
            this.layoutExportResults.Control = this.btnExportResults;
            this.layoutExportResults.CustomizationFormText = "layoutExportResults";
            this.layoutExportResults.Location = new System.Drawing.Point(0, 0);
            this.layoutExportResults.MaxSize = new System.Drawing.Size(85, 40);
            this.layoutExportResults.MinSize = new System.Drawing.Size(80, 36);
            this.layoutExportResults.Name = "layoutExportResults";
            this.layoutExportResults.Size = new System.Drawing.Size(82, 36);
            this.layoutExportResults.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutExportResults.Text = "layoutExportResults";
            this.layoutExportResults.TextSize = new System.Drawing.Size(0, 0);
            this.layoutExportResults.TextToControlDistance = 0;
            this.layoutExportResults.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutResultsAggregationType,
            this.emptySpaceItem1,
            this.layoutPositionResultsType});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1277, 46);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutResultsAggregationType
            // 
            this.layoutResultsAggregationType.Control = this.rdgResultsAggregationType;
            this.layoutResultsAggregationType.CustomizationFormText = "Aggregation Type:";
            this.layoutResultsAggregationType.Location = new System.Drawing.Point(0, 0);
            this.layoutResultsAggregationType.MinSize = new System.Drawing.Size(120, 24);
            this.layoutResultsAggregationType.Name = "layoutResultsAggregationType";
            this.layoutResultsAggregationType.Size = new System.Drawing.Size(556, 34);
            this.layoutResultsAggregationType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutResultsAggregationType.Text = "Aggregation:";
            this.layoutResultsAggregationType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutResultsAggregationType.TextSize = new System.Drawing.Size(63, 13);
            this.layoutResultsAggregationType.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(1104, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(161, 34);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutPositionResultsType
            // 
            this.layoutPositionResultsType.Control = this.rdgPositionResultsType;
            this.layoutPositionResultsType.CustomizationFormText = "Type:";
            this.layoutPositionResultsType.Location = new System.Drawing.Point(556, 0);
            this.layoutPositionResultsType.Name = "layoutPositionResultsType";
            this.layoutPositionResultsType.Size = new System.Drawing.Size(548, 34);
            this.layoutPositionResultsType.Text = "Results Type";
            this.layoutPositionResultsType.TextSize = new System.Drawing.Size(62, 13);
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.Images.SetKeyName(0, "company.ico");
            this.imageList16.Images.SetKeyName(1, "counterparty.ico");
            this.imageList16.Images.SetKeyName(2, "market.ico");
            this.imageList16.Images.SetKeyName(3, "trader.ico");
            this.imageList16.Images.SetKeyName(4, "vessel.ico");
            this.imageList16.Images.SetKeyName(5, "book.ico");
            this.imageList16.Images.SetKeyName(6, "pool.ico");
            this.imageList16.Images.SetKeyName(7, "vesselpool.ico");
            this.imageList16.Images.SetKeyName(8, "broker.ico");
            this.imageList16.Images.SetKeyName(9, "bulletblue.ico");
            this.imageList16.Images.SetKeyName(10, "house.ico");
            this.imageList16.Images.SetKeyName(11, "bullet_ball_glass_grey.ico");
            this.imageList16.Images.SetKeyName(12, "bullet_ball_glass_yellow.ico");
            this.imageList16.Images.SetKeyName(13, "bullet_ball_glass_green.ico");
            this.imageList16.Images.SetKeyName(14, "bullet_ball_glass_red.ico");
            this.imageList16.Images.SetKeyName(15, "calendar.ico");
            this.imageList16.Images.SetKeyName(16, "briefcase.ico");
            this.imageList16.Images.SetKeyName(17, "businessmen.ico");
            // 
            // layoutControlRoot
            // 
            this.layoutControlRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRoot.Name = "layoutControlRoot";
            this.layoutControlRoot.Root = this.layoutControlGroup1;
            this.layoutControlRoot.Size = new System.Drawing.Size(310, 97);
            this.layoutControlRoot.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(310, 97);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnClose});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 0;
            this.btnClose.LargeImageIndex = 5;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1219, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 645);
            this.barDockControlBottom.Size = new System.Drawing.Size(1219, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 645);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1219, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 645);
            // 
            // GenerateMTMForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1219, 677);
            this.Controls.Add(this.layoutRoot);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "GenerateMTMForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewTradesForm_FormClosing);
            this.Load += new System.EventHandler(this.MainControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdgPositionResultsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateCommissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCalculateSums.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionNullify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionEmbeddedValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptionPremium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSpotValues.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilterCurveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgMTMResultsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgResultsAggregationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarketSensitivityValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMarketSensitivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPositionMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTradeActionResultsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemGeneratePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFilterCurveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMarketSensitivityValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMTMResultsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionPremium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionNullify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOptionEmbeddedValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUseSpotValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateSums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCalculateCommissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTradeResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemTradeActionResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExportResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutResultsAggregationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPositionResultsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTrades;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeDetails;
        private DevExpress.XtraGrid.GridControl gridTradeActionResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridTradeActionResultsView;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupTradeActionResults;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemTradeActionResults;
        private DevExpress.XtraEditors.SimpleButton btnGeneratePosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemGeneratePosition;
        private DevExpress.Utils.ImageCollection imageList16;
        private DevExpress.XtraEditors.SimpleButton btnExportResults;
        private DevExpress.XtraLayout.LayoutControlItem layoutExportResults;
        private DevExpress.XtraLayout.LayoutControlGroup layoutResultsActions;
        private DevExpress.XtraEditors.SpinEdit txtMarketSensitivityValue;
        private DevExpress.XtraEditors.ComboBoxEdit cmbMarketSensitivityType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionMethod;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutMarketSensitivityValue;
        private DevExpress.XtraLayout.LayoutControl layoutControlRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.RadioGroup rdgResultsAggregationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutResultsAggregationType;
        private DevExpress.XtraEditors.RadioGroup rdgMTMResultsType;
        private DevExpress.XtraLayout.LayoutControlItem layoutMTMResultsType;
        private DevExpress.XtraEditors.DateEdit dtpFilterCurveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutFilterCurveDate;
        private DevExpress.XtraEditors.CheckEdit chkUseSpotValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutUseSpotValues;
        private DevExpress.XtraEditors.CheckEdit chkOptionEmbeddedValue;
        private DevExpress.XtraEditors.CheckEdit chkOptionPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionEmbeddedValue;
        private DevExpress.XtraEditors.CheckEdit chkOptionNullify;
        private DevExpress.XtraLayout.LayoutControlItem layoutOptionNullify;
        private DevExpress.XtraEditors.CheckEdit chkCalculateSums;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateSums;
        private DevExpress.XtraEditors.CheckEdit chkCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCalculateCommissions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTradeResults;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrades;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LabelControl lblIsOptionalPeriod;
        private DevExpress.XtraEditors.LabelControl lblIsMinimumPeriod;
        private DevExpress.XtraEditors.LabelControl lblIsDraft;
        private DevExpress.XtraEditors.LabelControl lblPeriodTo;
        private DevExpress.XtraEditors.LabelControl lblPeriodFrom;
        private DevExpress.XtraEditors.LabelControl lblEntity;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.RadioGroup rdgPositionResultsType;
        private DevExpress.XtraLayout.LayoutControlItem layoutPositionResultsType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.LabelControl lblOnlyNonZeroTotalDays;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}
