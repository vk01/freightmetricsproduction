﻿namespace Exis.WinClient.Controls.Core
{
    partial class AddEditViewTradeOptionInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditViewTradeOptionInfoControl));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControl();
            this.cmbPurpose = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookUpProfitSharing = new DevExpress.XtraEditors.LookUpEdit();
            this.chkProfitSharing = new DevExpress.XtraEditors.CheckEdit();
            this.txtStrike = new DevExpress.XtraEditors.SpinEdit();
            this.txtClearingFees = new DevExpress.XtraEditors.SpinEdit();
            this.txtUserMTM = new DevExpress.XtraEditors.SpinEdit();
            this.cmbOptionType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbSettlementType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpPremiumDueDate = new DevExpress.XtraEditors.DateEdit();
            this.txtUserDelta = new DevExpress.XtraEditors.SpinEdit();
            this.txtPremium = new DevExpress.XtraEditors.SpinEdit();
            this.txtQuantityDays = new DevExpress.XtraEditors.SpinEdit();
            this.cmbQuantityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPeriodDayCountType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPeriod = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cmbPeriodType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookupClearingHouse = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupClearingAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupAllocationPool = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupAllocationVessel = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbAllocationType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookupClearingBank = new DevExpress.XtraEditors.LookUpEdit();
            this.lookupUnderlyingIndex = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutRootGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAllocationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAllocationPool = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingBank = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUnderlyingIndex = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAllocationVessel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutClearingHouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPeriodDayCountType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutQuantityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutQuantityDays = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPremium = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSettlementType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemOptionType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemPremiumDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemUserMTM = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemUserDelta = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemClearingFees = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemStrike = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPurpose = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutProfitSharing = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutlookupProfitSharing = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            this.layoutRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPurpose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpProfitSharing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProfitSharing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStrike.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClearingFees.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserMTM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOptionType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSettlementType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPremiumDueDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPremiumDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserDelta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPremium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuantityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodDayCountType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingHouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationPool.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationVessel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAllocationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupUnderlyingIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationPool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnderlyingIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationVessel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodDayCountType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPremium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettlementType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemOptionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPremiumDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemUserMTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemUserDelta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemClearingFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemStrike)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProfitSharing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutlookupProfitSharing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutRoot
            // 
            this.layoutRoot.Controls.Add(this.cmbPurpose);
            this.layoutRoot.Controls.Add(this.lookUpProfitSharing);
            this.layoutRoot.Controls.Add(this.chkProfitSharing);
            this.layoutRoot.Controls.Add(this.txtStrike);
            this.layoutRoot.Controls.Add(this.txtClearingFees);
            this.layoutRoot.Controls.Add(this.txtUserMTM);
            this.layoutRoot.Controls.Add(this.cmbOptionType);
            this.layoutRoot.Controls.Add(this.cmbSettlementType);
            this.layoutRoot.Controls.Add(this.dtpPremiumDueDate);
            this.layoutRoot.Controls.Add(this.txtUserDelta);
            this.layoutRoot.Controls.Add(this.txtPremium);
            this.layoutRoot.Controls.Add(this.txtQuantityDays);
            this.layoutRoot.Controls.Add(this.cmbQuantityType);
            this.layoutRoot.Controls.Add(this.cmbPeriodDayCountType);
            this.layoutRoot.Controls.Add(this.cmbPeriod);
            this.layoutRoot.Controls.Add(this.cmbPeriodType);
            this.layoutRoot.Controls.Add(this.lookupClearingHouse);
            this.layoutRoot.Controls.Add(this.lookupClearingAccount);
            this.layoutRoot.Controls.Add(this.lookupAllocationPool);
            this.layoutRoot.Controls.Add(this.lookupAllocationVessel);
            this.layoutRoot.Controls.Add(this.cmbAllocationType);
            this.layoutRoot.Controls.Add(this.lookupClearingBank);
            this.layoutRoot.Controls.Add(this.lookupUnderlyingIndex);
            this.layoutRoot.Controls.Add(this.cmbType);
            this.layoutRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(537, 394, 250, 350);
            this.layoutRoot.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutRoot.Root = this.layoutRootGroup;
            this.layoutRoot.Size = new System.Drawing.Size(1063, 281);
            this.layoutRoot.TabIndex = 0;
            this.layoutRoot.Text = "layoutControl1";
            // 
            // cmbPurpose
            // 
            this.cmbPurpose.Location = new System.Drawing.Point(838, 2);
            this.cmbPurpose.Name = "cmbPurpose";
            this.cmbPurpose.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPurpose.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPurpose.Size = new System.Drawing.Size(223, 20);
            this.cmbPurpose.StyleController = this.layoutRoot;
            this.cmbPurpose.TabIndex = 21;
            // 
            // lookUpProfitSharing
            // 
            this.lookUpProfitSharing.Location = new System.Drawing.Point(829, 74);
            this.lookUpProfitSharing.Name = "lookUpProfitSharing";
            this.lookUpProfitSharing.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::Exis.WinClient.Strings.sf, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lookUpProfitSharing.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, global::Exis.WinClient.Strings.sf, "View", null, true)});
            this.lookUpProfitSharing.Size = new System.Drawing.Size(232, 22);
            this.lookUpProfitSharing.StyleController = this.layoutRoot;
            this.lookUpProfitSharing.TabIndex = 22;
            this.lookUpProfitSharing.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LookUpProfitSharingButtonClick);
            // 
            // chkProfitSharing
            // 
            this.chkProfitSharing.Location = new System.Drawing.Point(801, 74);
            this.chkProfitSharing.Name = "chkProfitSharing";
            this.chkProfitSharing.Properties.Caption = global::Exis.WinClient.Strings.sf;
            this.chkProfitSharing.Size = new System.Drawing.Size(24, 19);
            this.chkProfitSharing.StyleController = this.layoutRoot;
            this.chkProfitSharing.TabIndex = 21;
            this.chkProfitSharing.CheckedChanged += new System.EventHandler(this.ChkProfitSharingCheckedChanged);
            // 
            // txtStrike
            // 
            this.txtStrike.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtStrike.Location = new System.Drawing.Point(952, 100);
            this.txtStrike.Name = "txtStrike";
            this.txtStrike.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtStrike.Properties.Mask.EditMask = "f4";
            this.txtStrike.Properties.MaxLength = 12;
            this.txtStrike.Size = new System.Drawing.Size(109, 20);
            this.txtStrike.StyleController = this.layoutRoot;
            this.txtStrike.TabIndex = 10;
            // 
            // txtClearingFees
            // 
            this.txtClearingFees.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtClearingFees.Location = new System.Drawing.Point(617, 74);
            this.txtClearingFees.Name = "txtClearingFees";
            this.txtClearingFees.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtClearingFees.Properties.Mask.EditMask = "D";
            this.txtClearingFees.Properties.MaxLength = 6;
            this.txtClearingFees.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtClearingFees.Size = new System.Drawing.Size(108, 20);
            this.txtClearingFees.StyleController = this.layoutRoot;
            this.txtClearingFees.TabIndex = 12;
            // 
            // txtUserMTM
            // 
            this.txtUserMTM.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtUserMTM.Location = new System.Drawing.Point(266, 74);
            this.txtUserMTM.Name = "txtUserMTM";
            this.txtUserMTM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtUserMTM.Properties.Mask.EditMask = "f4";
            this.txtUserMTM.Properties.MaxLength = 12;
            this.txtUserMTM.Size = new System.Drawing.Size(107, 20);
            this.txtUserMTM.StyleController = this.layoutRoot;
            this.txtUserMTM.TabIndex = 10;
            // 
            // cmbOptionType
            // 
            this.cmbOptionType.Location = new System.Drawing.Point(174, 2);
            this.cmbOptionType.Name = "cmbOptionType";
            this.cmbOptionType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbOptionType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbOptionType.Size = new System.Drawing.Size(80, 20);
            this.cmbOptionType.StyleController = this.layoutRoot;
            this.cmbOptionType.TabIndex = 6;
            // 
            // cmbSettlementType
            // 
            this.cmbSettlementType.Location = new System.Drawing.Point(631, 2);
            this.cmbSettlementType.Name = "cmbSettlementType";
            this.cmbSettlementType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSettlementType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbSettlementType.Size = new System.Drawing.Size(157, 20);
            this.cmbSettlementType.StyleController = this.layoutRoot;
            this.cmbSettlementType.TabIndex = 19;
            // 
            // dtpPremiumDueDate
            // 
            this.dtpPremiumDueDate.EditValue = null;
            this.dtpPremiumDueDate.Location = new System.Drawing.Point(97, 74);
            this.dtpPremiumDueDate.Name = "dtpPremiumDueDate";
            this.dtpPremiumDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpPremiumDueDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpPremiumDueDate.Size = new System.Drawing.Size(111, 20);
            this.dtpPremiumDueDate.StyleController = this.layoutRoot;
            this.dtpPremiumDueDate.TabIndex = 20;
            // 
            // txtUserDelta
            // 
            this.txtUserDelta.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtUserDelta.Location = new System.Drawing.Point(434, 74);
            this.txtUserDelta.Name = "txtUserDelta";
            this.txtUserDelta.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtUserDelta.Properties.Mask.EditMask = "f4";
            this.txtUserDelta.Properties.MaxLength = 12;
            this.txtUserDelta.Size = new System.Drawing.Size(107, 20);
            this.txtUserDelta.StyleController = this.layoutRoot;
            this.txtUserDelta.TabIndex = 11;
            // 
            // txtPremium
            // 
            this.txtPremium.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPremium.Location = new System.Drawing.Point(833, 100);
            this.txtPremium.MaximumSize = new System.Drawing.Size(80, 20);
            this.txtPremium.MinimumSize = new System.Drawing.Size(80, 20);
            this.txtPremium.Name = "txtPremium";
            this.txtPremium.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPremium.Properties.Mask.EditMask = "f4";
            this.txtPremium.Properties.MaxLength = 12;
            this.txtPremium.Size = new System.Drawing.Size(80, 20);
            this.txtPremium.StyleController = this.layoutRoot;
            this.txtPremium.TabIndex = 9;
            // 
            // txtQuantityDays
            // 
            this.txtQuantityDays.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtQuantityDays.Location = new System.Drawing.Point(535, 100);
            this.txtQuantityDays.Name = "txtQuantityDays";
            this.txtQuantityDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtQuantityDays.Properties.Mask.EditMask = "d";
            this.txtQuantityDays.Properties.MaxLength = 6;
            this.txtQuantityDays.Size = new System.Drawing.Size(51, 20);
            this.txtQuantityDays.StyleController = this.layoutRoot;
            this.txtQuantityDays.TabIndex = 8;
            // 
            // cmbQuantityType
            // 
            this.cmbQuantityType.Location = new System.Drawing.Point(666, 100);
            this.cmbQuantityType.Name = "cmbQuantityType";
            this.cmbQuantityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQuantityType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbQuantityType.Size = new System.Drawing.Size(80, 20);
            this.cmbQuantityType.StyleController = this.layoutRoot;
            this.cmbQuantityType.TabIndex = 18;
            // 
            // cmbPeriodDayCountType
            // 
            this.cmbPeriodDayCountType.Location = new System.Drawing.Point(390, 100);
            this.cmbPeriodDayCountType.Name = "cmbPeriodDayCountType";
            this.cmbPeriodDayCountType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodDayCountType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriodDayCountType.Size = new System.Drawing.Size(58, 20);
            this.cmbPeriodDayCountType.StyleController = this.layoutRoot;
            this.cmbPeriodDayCountType.TabIndex = 17;
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Location = new System.Drawing.Point(169, 100);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriod.Properties.PopupFormMinSize = new System.Drawing.Size(200, 100);
            this.cmbPeriod.Properties.PopupFormSize = new System.Drawing.Size(200, 200);
            this.cmbPeriod.Size = new System.Drawing.Size(99, 20);
            this.cmbPeriod.StyleController = this.layoutRoot;
            this.cmbPeriod.TabIndex = 16;
            // 
            // cmbPeriodType
            // 
            this.cmbPeriodType.Location = new System.Drawing.Point(66, 100);
            this.cmbPeriodType.Name = "cmbPeriodType";
            this.cmbPeriodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriodType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriodType.Size = new System.Drawing.Size(62, 20);
            this.cmbPeriodType.StyleController = this.layoutRoot;
            this.cmbPeriodType.TabIndex = 15;
            this.cmbPeriodType.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodType_SelectedIndexChanged);
            // 
            // lookupClearingHouse
            // 
            this.lookupClearingHouse.Location = new System.Drawing.Point(624, 50);
            this.lookupClearingHouse.Name = "lookupClearingHouse";
            this.lookupClearingHouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupClearingHouse.Size = new System.Drawing.Size(437, 20);
            this.lookupClearingHouse.StyleController = this.layoutRoot;
            this.lookupClearingHouse.TabIndex = 14;
            this.lookupClearingHouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // lookupClearingAccount
            // 
            this.lookupClearingAccount.Location = new System.Drawing.Point(380, 50);
            this.lookupClearingAccount.Name = "lookupClearingAccount";
            this.lookupClearingAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupClearingAccount.Size = new System.Drawing.Size(161, 20);
            this.lookupClearingAccount.StyleController = this.layoutRoot;
            this.lookupClearingAccount.TabIndex = 13;
            this.lookupClearingAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // lookupAllocationPool
            // 
            this.lookupAllocationPool.Location = new System.Drawing.Point(621, 26);
            this.lookupAllocationPool.Name = "lookupAllocationPool";
            this.lookupAllocationPool.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupAllocationPool.Size = new System.Drawing.Size(440, 20);
            this.lookupAllocationPool.StyleController = this.layoutRoot;
            this.lookupAllocationPool.TabIndex = 12;
            // 
            // lookupAllocationVessel
            // 
            this.lookupAllocationVessel.Location = new System.Drawing.Point(253, 26);
            this.lookupAllocationVessel.Name = "lookupAllocationVessel";
            this.lookupAllocationVessel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupAllocationVessel.Size = new System.Drawing.Size(288, 20);
            this.lookupAllocationVessel.StyleController = this.layoutRoot;
            this.lookupAllocationVessel.TabIndex = 11;
            // 
            // cmbAllocationType
            // 
            this.cmbAllocationType.Location = new System.Drawing.Point(82, 26);
            this.cmbAllocationType.Name = "cmbAllocationType";
            this.cmbAllocationType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbAllocationType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbAllocationType.Size = new System.Drawing.Size(81, 20);
            this.cmbAllocationType.StyleController = this.layoutRoot;
            this.cmbAllocationType.TabIndex = 10;
            this.cmbAllocationType.SelectedIndexChanged += new System.EventHandler(this.cmbAllocationType_SelectedIndexChanged);
            // 
            // lookupClearingBank
            // 
            this.lookupClearingBank.Location = new System.Drawing.Point(74, 50);
            this.lookupClearingBank.Name = "lookupClearingBank";
            this.lookupClearingBank.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupClearingBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupClearingBank.Size = new System.Drawing.Size(214, 20);
            this.lookupClearingBank.StyleController = this.layoutRoot;
            this.lookupClearingBank.TabIndex = 7;
            this.lookupClearingBank.EditValueChanged += new System.EventHandler(this.lookupClearingBank_EditValueChanged);
            this.lookupClearingBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookup_KeyDown);
            // 
            // lookupUnderlyingIndex
            // 
            this.lookupUnderlyingIndex.Location = new System.Drawing.Point(347, 2);
            this.lookupUnderlyingIndex.Name = "lookupUnderlyingIndex";
            this.lookupUnderlyingIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupUnderlyingIndex.Size = new System.Drawing.Size(194, 20);
            this.lookupUnderlyingIndex.StyleController = this.layoutRoot;
            this.lookupUnderlyingIndex.TabIndex = 6;
            this.lookupUnderlyingIndex.EditValueChanged += new System.EventHandler(this.lookupUnderlyingIndex_EditValueChanged);
            // 
            // cmbType
            // 
            this.cmbType.Location = new System.Drawing.Point(33, 2);
            this.cmbType.Name = "cmbType";
            this.cmbType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbType.Size = new System.Drawing.Size(71, 20);
            this.cmbType.StyleController = this.layoutRoot;
            this.cmbType.TabIndex = 5;
            // 
            // layoutRootGroup
            // 
            this.layoutRootGroup.CustomizationFormText = "layoutControlGroup1";
            this.layoutRootGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRootGroup.GroupBordersVisible = false;
            this.layoutRootGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutType,
            this.layoutAllocationType,
            this.layoutAllocationPool,
            this.layoutClearingBank,
            this.layoutUnderlyingIndex,
            this.layoutAllocationVessel,
            this.layoutClearingAccount,
            this.layoutClearingHouse,
            this.layoutPeriodType,
            this.layoutPeriod,
            this.layoutPeriodDayCountType,
            this.layoutQuantityType,
            this.layoutQuantityDays,
            this.layoutPremium,
            this.layoutSettlementType,
            this.layoutItemOptionType,
            this.layoutItemPremiumDueDate,
            this.layoutItemUserMTM,
            this.layoutItemUserDelta,
            this.layoutItemClearingFees,
            this.layoutItemStrike,
            this.layoutPurpose,
            this.layoutProfitSharing,
            this.layoutlookupProfitSharing});
            this.layoutRootGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutRootGroup.Name = "layoutRootGroup";
            this.layoutRootGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRootGroup.Size = new System.Drawing.Size(1063, 281);
            this.layoutRootGroup.Text = "layoutRootGroup";
            this.layoutRootGroup.TextVisible = false;
            // 
            // layoutType
            // 
            this.layoutType.Control = this.cmbType;
            this.layoutType.CustomizationFormText = "Type:";
            this.layoutType.Location = new System.Drawing.Point(0, 0);
            this.layoutType.MaxSize = new System.Drawing.Size(106, 24);
            this.layoutType.MinSize = new System.Drawing.Size(106, 24);
            this.layoutType.Name = "layoutType";
            this.layoutType.Size = new System.Drawing.Size(106, 24);
            this.layoutType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutType.Text = "Type:";
            this.layoutType.TextSize = new System.Drawing.Size(28, 13);
            // 
            // layoutAllocationType
            // 
            this.layoutAllocationType.Control = this.cmbAllocationType;
            this.layoutAllocationType.CustomizationFormText = "Allocation Type:";
            this.layoutAllocationType.Location = new System.Drawing.Point(0, 24);
            this.layoutAllocationType.MaxSize = new System.Drawing.Size(165, 24);
            this.layoutAllocationType.MinSize = new System.Drawing.Size(165, 24);
            this.layoutAllocationType.Name = "layoutAllocationType";
            this.layoutAllocationType.Size = new System.Drawing.Size(165, 24);
            this.layoutAllocationType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAllocationType.Text = "Allocation Type:";
            this.layoutAllocationType.TextSize = new System.Drawing.Size(77, 13);
            // 
            // layoutAllocationPool
            // 
            this.layoutAllocationPool.Control = this.lookupAllocationPool;
            this.layoutAllocationPool.CustomizationFormText = "Allocation Pool:";
            this.layoutAllocationPool.Location = new System.Drawing.Point(543, 24);
            this.layoutAllocationPool.Name = "layoutAllocationPool";
            this.layoutAllocationPool.Size = new System.Drawing.Size(520, 24);
            this.layoutAllocationPool.Text = "Allocation Pool:";
            this.layoutAllocationPool.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutClearingBank
            // 
            this.layoutClearingBank.Control = this.lookupClearingBank;
            this.layoutClearingBank.CustomizationFormText = "Clearing Bank:";
            this.layoutClearingBank.Location = new System.Drawing.Point(0, 48);
            this.layoutClearingBank.Name = "layoutClearingBank";
            this.layoutClearingBank.Size = new System.Drawing.Size(290, 24);
            this.layoutClearingBank.Text = "Clearing Bank:";
            this.layoutClearingBank.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutUnderlyingIndex
            // 
            this.layoutUnderlyingIndex.Control = this.lookupUnderlyingIndex;
            this.layoutUnderlyingIndex.CustomizationFormText = "Index:";
            this.layoutUnderlyingIndex.Location = new System.Drawing.Point(256, 0);
            this.layoutUnderlyingIndex.Name = "layoutUnderlyingIndex";
            this.layoutUnderlyingIndex.Size = new System.Drawing.Size(287, 24);
            this.layoutUnderlyingIndex.Text = "Underlying Index:";
            this.layoutUnderlyingIndex.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutAllocationVessel
            // 
            this.layoutAllocationVessel.Control = this.lookupAllocationVessel;
            this.layoutAllocationVessel.CustomizationFormText = "Allocation Vessel:";
            this.layoutAllocationVessel.Location = new System.Drawing.Point(165, 24);
            this.layoutAllocationVessel.Name = "layoutAllocationVessel";
            this.layoutAllocationVessel.Size = new System.Drawing.Size(378, 24);
            this.layoutAllocationVessel.Text = "Allocation Vessel:";
            this.layoutAllocationVessel.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutClearingAccount
            // 
            this.layoutClearingAccount.Control = this.lookupClearingAccount;
            this.layoutClearingAccount.CustomizationFormText = "Clearing Account:";
            this.layoutClearingAccount.Location = new System.Drawing.Point(290, 48);
            this.layoutClearingAccount.Name = "layoutClearingAccount";
            this.layoutClearingAccount.Size = new System.Drawing.Size(253, 24);
            this.layoutClearingAccount.Text = "Clearing Account:";
            this.layoutClearingAccount.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutClearingHouse
            // 
            this.layoutClearingHouse.Control = this.lookupClearingHouse;
            this.layoutClearingHouse.CustomizationFormText = "Clearing House:";
            this.layoutClearingHouse.Location = new System.Drawing.Point(543, 48);
            this.layoutClearingHouse.Name = "layoutClearingHouse";
            this.layoutClearingHouse.Size = new System.Drawing.Size(520, 24);
            this.layoutClearingHouse.Text = "Clearing House:";
            this.layoutClearingHouse.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutPeriodType
            // 
            this.layoutPeriodType.Control = this.cmbPeriodType;
            this.layoutPeriodType.CustomizationFormText = "Period Type:";
            this.layoutPeriodType.Location = new System.Drawing.Point(0, 98);
            this.layoutPeriodType.MaxSize = new System.Drawing.Size(130, 24);
            this.layoutPeriodType.MinSize = new System.Drawing.Size(130, 24);
            this.layoutPeriodType.Name = "layoutPeriodType";
            this.layoutPeriodType.Size = new System.Drawing.Size(130, 183);
            this.layoutPeriodType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodType.Text = "Period Type:";
            this.layoutPeriodType.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutPeriod
            // 
            this.layoutPeriod.Control = this.cmbPeriod;
            this.layoutPeriod.CustomizationFormText = "Period:";
            this.layoutPeriod.Location = new System.Drawing.Point(130, 98);
            this.layoutPeriod.MaxSize = new System.Drawing.Size(140, 24);
            this.layoutPeriod.MinSize = new System.Drawing.Size(140, 24);
            this.layoutPeriod.Name = "layoutPeriod";
            this.layoutPeriod.Size = new System.Drawing.Size(140, 183);
            this.layoutPeriod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriod.Text = "Period:";
            this.layoutPeriod.TextSize = new System.Drawing.Size(34, 13);
            // 
            // layoutPeriodDayCountType
            // 
            this.layoutPeriodDayCountType.Control = this.cmbPeriodDayCountType;
            this.layoutPeriodDayCountType.CustomizationFormText = "Period Day Count Type:";
            this.layoutPeriodDayCountType.Location = new System.Drawing.Point(270, 98);
            this.layoutPeriodDayCountType.MaxSize = new System.Drawing.Size(180, 24);
            this.layoutPeriodDayCountType.MinSize = new System.Drawing.Size(180, 24);
            this.layoutPeriodDayCountType.Name = "layoutPeriodDayCountType";
            this.layoutPeriodDayCountType.Size = new System.Drawing.Size(180, 183);
            this.layoutPeriodDayCountType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPeriodDayCountType.Text = "Period Day Count Type:";
            this.layoutPeriodDayCountType.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutQuantityType
            // 
            this.layoutQuantityType.Control = this.cmbQuantityType;
            this.layoutQuantityType.CustomizationFormText = "Quantity Type:";
            this.layoutQuantityType.Location = new System.Drawing.Point(588, 98);
            this.layoutQuantityType.MaxSize = new System.Drawing.Size(160, 24);
            this.layoutQuantityType.MinSize = new System.Drawing.Size(160, 24);
            this.layoutQuantityType.Name = "layoutQuantityType";
            this.layoutQuantityType.Size = new System.Drawing.Size(160, 183);
            this.layoutQuantityType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutQuantityType.Text = "Quantity Type:";
            this.layoutQuantityType.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutQuantityDays
            // 
            this.layoutQuantityDays.Control = this.txtQuantityDays;
            this.layoutQuantityDays.CustomizationFormText = "Quantity (days):";
            this.layoutQuantityDays.Location = new System.Drawing.Point(450, 98);
            this.layoutQuantityDays.MaxSize = new System.Drawing.Size(138, 24);
            this.layoutQuantityDays.MinSize = new System.Drawing.Size(138, 24);
            this.layoutQuantityDays.Name = "layoutQuantityDays";
            this.layoutQuantityDays.Size = new System.Drawing.Size(138, 183);
            this.layoutQuantityDays.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutQuantityDays.Text = "Quantity (days):";
            this.layoutQuantityDays.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutPremium
            // 
            this.layoutPremium.Control = this.txtPremium;
            this.layoutPremium.CustomizationFormText = "Price:";
            this.layoutPremium.Location = new System.Drawing.Point(748, 98);
            this.layoutPremium.MaxSize = new System.Drawing.Size(168, 24);
            this.layoutPremium.MinSize = new System.Drawing.Size(168, 24);
            this.layoutPremium.Name = "layoutPremium";
            this.layoutPremium.Size = new System.Drawing.Size(168, 183);
            this.layoutPremium.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPremium.Text = "Premium($/day):";
            this.layoutPremium.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutSettlementType
            // 
            this.layoutSettlementType.Control = this.cmbSettlementType;
            this.layoutSettlementType.CustomizationFormText = "Settlement Type:";
            this.layoutSettlementType.Location = new System.Drawing.Point(543, 0);
            this.layoutSettlementType.Name = "layoutSettlementType";
            this.layoutSettlementType.Size = new System.Drawing.Size(247, 24);
            this.layoutSettlementType.Text = "Settlement Type:";
            this.layoutSettlementType.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutItemOptionType
            // 
            this.layoutItemOptionType.Control = this.cmbOptionType;
            this.layoutItemOptionType.CustomizationFormText = "Option Type:";
            this.layoutItemOptionType.Location = new System.Drawing.Point(106, 0);
            this.layoutItemOptionType.Name = "layoutItemOptionType";
            this.layoutItemOptionType.Size = new System.Drawing.Size(150, 24);
            this.layoutItemOptionType.Text = "Option Type:";
            this.layoutItemOptionType.TextSize = new System.Drawing.Size(63, 13);
            // 
            // layoutItemPremiumDueDate
            // 
            this.layoutItemPremiumDueDate.Control = this.dtpPremiumDueDate;
            this.layoutItemPremiumDueDate.CustomizationFormText = "Premium Due Date:";
            this.layoutItemPremiumDueDate.Location = new System.Drawing.Point(0, 72);
            this.layoutItemPremiumDueDate.Name = "layoutItemPremiumDueDate";
            this.layoutItemPremiumDueDate.Size = new System.Drawing.Size(210, 26);
            this.layoutItemPremiumDueDate.Text = "Premium Due Date:";
            this.layoutItemPremiumDueDate.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutItemUserMTM
            // 
            this.layoutItemUserMTM.Control = this.txtUserMTM;
            this.layoutItemUserMTM.CustomizationFormText = "User MTM:";
            this.layoutItemUserMTM.Location = new System.Drawing.Point(210, 72);
            this.layoutItemUserMTM.Name = "layoutItemUserMTM";
            this.layoutItemUserMTM.Size = new System.Drawing.Size(165, 26);
            this.layoutItemUserMTM.Text = "User MTM:";
            this.layoutItemUserMTM.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutItemUserDelta
            // 
            this.layoutItemUserDelta.Control = this.txtUserDelta;
            this.layoutItemUserDelta.CustomizationFormText = "User Delta:";
            this.layoutItemUserDelta.Location = new System.Drawing.Point(375, 72);
            this.layoutItemUserDelta.Name = "layoutItemUserDelta";
            this.layoutItemUserDelta.Size = new System.Drawing.Size(168, 26);
            this.layoutItemUserDelta.Text = "User Delta:";
            this.layoutItemUserDelta.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutItemClearingFees
            // 
            this.layoutItemClearingFees.Control = this.txtClearingFees;
            this.layoutItemClearingFees.CustomizationFormText = "Clearing Fees:";
            this.layoutItemClearingFees.Location = new System.Drawing.Point(543, 72);
            this.layoutItemClearingFees.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutItemClearingFees.MinSize = new System.Drawing.Size(127, 24);
            this.layoutItemClearingFees.Name = "layoutItemClearingFees";
            this.layoutItemClearingFees.Size = new System.Drawing.Size(184, 26);
            this.layoutItemClearingFees.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemClearingFees.Text = "Clearing Fees:";
            this.layoutItemClearingFees.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutItemStrike
            // 
            this.layoutItemStrike.Control = this.txtStrike;
            this.layoutItemStrike.CustomizationFormText = "Strike:";
            this.layoutItemStrike.Location = new System.Drawing.Point(916, 98);
            this.layoutItemStrike.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutItemStrike.MinSize = new System.Drawing.Size(89, 24);
            this.layoutItemStrike.Name = "layoutItemStrike";
            this.layoutItemStrike.Size = new System.Drawing.Size(147, 183);
            this.layoutItemStrike.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemStrike.Text = "Strike:";
            this.layoutItemStrike.TextSize = new System.Drawing.Size(31, 13);
            // 
            // layoutPurpose
            // 
            this.layoutPurpose.Control = this.cmbPurpose;
            this.layoutPurpose.CustomizationFormText = "Purpose:";
            this.layoutPurpose.Location = new System.Drawing.Point(790, 0);
            this.layoutPurpose.Name = "layoutPurpose";
            this.layoutPurpose.Size = new System.Drawing.Size(273, 24);
            this.layoutPurpose.Text = "Purpose:";
            this.layoutPurpose.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutProfitSharing
            // 
            this.layoutProfitSharing.Control = this.chkProfitSharing;
            this.layoutProfitSharing.CustomizationFormText = "Profit Sharing:";
            this.layoutProfitSharing.Location = new System.Drawing.Point(727, 72);
            this.layoutProfitSharing.Name = "layoutProfitSharing";
            this.layoutProfitSharing.Size = new System.Drawing.Size(100, 26);
            this.layoutProfitSharing.Text = "Profit Sharing:";
            this.layoutProfitSharing.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutlookupProfitSharing
            // 
            this.layoutlookupProfitSharing.Control = this.lookUpProfitSharing;
            this.layoutlookupProfitSharing.CustomizationFormText = "layoutControlItem2";
            this.layoutlookupProfitSharing.Location = new System.Drawing.Point(827, 72);
            this.layoutlookupProfitSharing.Name = "layoutlookupProfitSharing";
            this.layoutlookupProfitSharing.Size = new System.Drawing.Size(236, 26);
            this.layoutlookupProfitSharing.Text = "layoutlookupProfitSharing";
            this.layoutlookupProfitSharing.TextSize = new System.Drawing.Size(0, 0);
            this.layoutlookupProfitSharing.TextToControlDistance = 0;
            this.layoutlookupProfitSharing.TextVisible = false;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtUserDelta;
            this.layoutControlItem1.CustomizationFormText = "User Delta:";
            this.layoutControlItem1.Location = new System.Drawing.Point(951, 72);
            this.layoutControlItem1.Name = "layoutItemUserDelta";
            this.layoutControlItem1.Size = new System.Drawing.Size(112, 24);
            this.layoutControlItem1.Text = "User Delta:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // AddEditViewTradeOptionInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutRoot);
            this.Name = "AddEditViewTradeOptionInfoControl";
            this.Size = new System.Drawing.Size(1063, 281);
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            this.layoutRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPurpose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpProfitSharing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProfitSharing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStrike.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClearingFees.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserMTM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOptionType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSettlementType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPremiumDueDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpPremiumDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserDelta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPremium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuantityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodDayCountType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingHouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationPool.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupAllocationVessel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAllocationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupClearingBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupUnderlyingIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRootGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationPool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnderlyingIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAllocationVessel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutClearingHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodDayCountType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutQuantityDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPremium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettlementType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemOptionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemPremiumDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemUserMTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemUserDelta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemClearingFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemStrike)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProfitSharing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutlookupProfitSharing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRootGroup;
        private DevExpress.XtraEditors.ComboBoxEdit cmbType;
        private DevExpress.XtraLayout.LayoutControlItem layoutType;
        private DevExpress.XtraEditors.LookUpEdit lookupUnderlyingIndex;
        private DevExpress.XtraLayout.LayoutControlItem layoutUnderlyingIndex;
        private DevExpress.XtraEditors.LookUpEdit lookupClearingBank;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingBank;
        private DevExpress.XtraEditors.ComboBoxEdit cmbAllocationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutAllocationType;
        private DevExpress.XtraEditors.LookUpEdit lookupAllocationVessel;
        private DevExpress.XtraLayout.LayoutControlItem layoutAllocationVessel;
        private DevExpress.XtraEditors.LookUpEdit lookupAllocationPool;
        private DevExpress.XtraLayout.LayoutControlItem layoutAllocationPool;
        private DevExpress.XtraEditors.LookUpEdit lookupClearingHouse;
        private DevExpress.XtraEditors.LookUpEdit lookupClearingAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutClearingHouse;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodType;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPeriodDayCountType;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodDayCountType;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraEditors.ComboBoxEdit cmbQuantityType;
        private DevExpress.XtraLayout.LayoutControlItem layoutQuantityType;
        private DevExpress.XtraEditors.SpinEdit txtQuantityDays;
        private DevExpress.XtraLayout.LayoutControlItem layoutQuantityDays;
        private DevExpress.XtraEditors.SpinEdit txtPremium;
        private DevExpress.XtraLayout.LayoutControlItem layoutPremium;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSettlementType;
        private DevExpress.XtraLayout.LayoutControlItem layoutSettlementType;
        private DevExpress.XtraEditors.SpinEdit txtStrike;
        private DevExpress.XtraEditors.SpinEdit txtClearingFees;
        private DevExpress.XtraEditors.SpinEdit txtUserDelta;
        private DevExpress.XtraEditors.SpinEdit txtUserMTM;
        private DevExpress.XtraEditors.ComboBoxEdit cmbOptionType;
        private DevExpress.XtraEditors.DateEdit dtpPremiumDueDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemOptionType;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemPremiumDueDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemUserMTM;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemUserDelta;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemClearingFees;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemStrike;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lookUpProfitSharing;
        private DevExpress.XtraEditors.CheckEdit chkProfitSharing;
        private DevExpress.XtraLayout.LayoutControlItem layoutProfitSharing;
        private DevExpress.XtraLayout.LayoutControlItem layoutlookupProfitSharing;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPurpose;
        private DevExpress.XtraLayout.LayoutControlItem layoutPurpose;
    }
}
