﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraRichEdit.Model.History;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using SummaryItemType = DevExpress.Data.SummaryItemType;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;
using DevExpress.XtraEditors.Controls;
using System.Drawing;
using DevExpress.XtraEditors.Popup;
using DevExpress.XtraEditors.ViewInfo;
using static DevExpress.XtraEditors.ViewInfo.CheckedListBoxViewInfo;
using DevExpress.Utils.Win;

namespace Exis.WinClient.Controls.Core
{
    public partial class PositionMTMRatiosReportForm : DevExpress.XtraEditors.XtraForm
    {
        private enum ReportResultEnum
        {
            Commission,
            LockedIn,
            Unrealized,
            TotalFFAOrOptionDays,
            TotalLongDays,
            TotalShortDays,
            NetDays,
            Cover,
            AvgLongPrice,
            AvgShortPrice,
            AvgLongMtmPrice,
            AvgShortMtmPrice,
            IndexEqCharterersRate,
            IndexEqOwnersRate,
            CoverAvgRate,
            CoverAvgRateFFA,
            AdjOption,
            EffCharterersEarningsRate,
            EffOwnersEarningsRate,
            BreakevenPrice,
            NetPhysicalDays,
            NetPaperDays,
            PPCover,
            TcInDays,
            TcOutDays,
            VesselCover,
            TotalPosition,
            TotalMTM,
            TotalLongDaysOption,
            TotalShortDaysOption,
            NetDaysOption,
            CoverOption,
            AvgLongPriceOption,
            AvgShortPriceOption,
            AvgLongMtmPriceOption,
            AvgShortMtmPriceOption,
            BreakevenPriceOption,
            LockedInOption,
            UnrealizedOption,
            IndexAverage,
            TotalCover,
            FFAAverageRate,
            EmptyItem,
            CoverAvgRateOption,
            EffOwnersEarningsRateOption,
            IndexEqOwnersRateOption,
            NetPaperDaysOption,
            TotalCoverOption
        }

        #region Private Properties
        #region Summmarize sumTotals 
        private Dictionary<string, decimal> sumTotalPosition = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalLongDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalShortDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalAvgLongPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalAvgShortPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalNetPhysicalDays = new Dictionary<string, decimal>();//TC+Cargo
        private Dictionary<string, decimal> sumTotalNetPaperDays = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> sumTotalNetPaperDaysOptions = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> sumTotalTcInDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalTcOutDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalFfaDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalFfaNotional = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalPaperNotional = new Dictionary<string, decimal>();//FFA+Option
        private Dictionary<string, decimal> sumTotalPaperNotionalFFA = new Dictionary<string, decimal>();//FFA+Option
        private Dictionary<string, decimal> sumTotalPaperOptional = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalTcOutPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalOptionDays = new Dictionary<string, decimal>();


        private Dictionary<string, decimal> sumTotalCommission = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMLongPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMLongPricePositionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMShortPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMShortPricePositionDays = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> sumTotalLongOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalShortOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalAvgLongOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalAvgShortOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMLongOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMShortOptionDays = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> sumTotalTcInCashFlow = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalTcOutCashFlow = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalTcInCalendarDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalTcInDaysVesselIndex = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalTcOutDaysVesselIndex = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMIndexEqOwnersRateFin = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMIndexEqOwnersRateFinOption = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMEffOwnersEarningsRateFin = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTMEffOwnersEarningsRateFinOption = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> sumTotalMTM = new Dictionary<string, decimal>();
        #endregion 
        private Dictionary<string, decimal> totalPosition = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalLongDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalShortDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalAvgLongPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalAvgShortPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalNetPhysicalDays = new Dictionary<string, decimal>();//TC+Cargo
        private Dictionary<string, decimal> totalNetPaperDays = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> totalNetPaperDaysOptions = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> totalTcInDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalTcOutDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalFfaDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalFfaNotional = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalPaperNotional = new Dictionary<string, decimal>();//FFA+Option
        private Dictionary<string, decimal> totalPaperNotionalFFA = new Dictionary<string, decimal>();//FFA+Option
        private Dictionary<string, decimal> totalPaperOptional = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalTcOutPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalOptionDays = new Dictionary<string, decimal>();


        private Dictionary<string, decimal> totalCommission = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMLongPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMLongPricePositionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMShortPrice = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMShortPricePositionDays = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> totalLongOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalShortOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalAvgLongOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalAvgShortOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMLongOptionDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMShortOptionDays = new Dictionary<string, decimal>();

        private Dictionary<string, decimal> totalTcInCashFlow = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalTcOutCashFlow = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalTcInCalendarDays = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalTcInDaysVesselIndex = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalTcOutDaysVesselIndex = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMIndexEqOwnersRateFin = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> totalMTMIndexEqOwnersRateFinOption = new Dictionary<string, decimal>();

        private Dictionary<string, Dictionary<string, decimal?>> totalAvgLongMTMPrice = new Dictionary<string, Dictionary<string, decimal?>>();
        private Dictionary<string, Dictionary<string, decimal?>> totalAvgShortMTMPrice = new Dictionary<string, Dictionary<string, decimal?>>();
        private Dictionary<string, Dictionary<string, decimal?>> IndexMTMLong = new Dictionary<string, Dictionary<string, decimal?>>();
        private Dictionary<string, Dictionary<string, decimal?>> IndexMTMShort = new Dictionary<string, Dictionary<string, decimal?>>();

        private Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>> mtmCalculationsPerIndex = new Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>>();
        private Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>> posCalculationsPerIndex = new Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>>();

        private Dictionary<string, decimal> totalMTM = new Dictionary<string, decimal>();

        private List<long> indexesIds;
        private List<string> indexesPhysicalFinancial = new List<string>();
        private List<string> indexesPhysicalFinancialTotal = new List<string>();
        private List<string> indexesMTMPhysicalFinancialTotal = new List<string>();
        private Dictionary<long, Dictionary<DateTime, decimal>> forwardIndexesValues;
        private Dictionary<long, Dictionary<DateTime, decimal>> spotIndexesValues;

        private List<TradeInformation> _tradeInformation;
        //private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults;
        //private Dictionary<string, Dictionary<DateTime, decimal>> _tradeMTMResults;
        //private Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMResults;
        //private Dictionary<string, decimal> _tradeEmbeddedValues = new Dictionary<string, decimal>();
        //private Dictionary<DateTime, Dictionary<string, decimal>> _indexesValues = new Dictionary<DateTime, Dictionary<string, decimal>>();

        private Dictionary<string, Dictionary<DateTime, decimal>> _tradeCalendarDaysResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<DateTime, decimal>> _tradeMTMResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMRatiosResults = new Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>();
        private Dictionary<string, decimal> _tradeEmbeddedValues = new Dictionary<string, decimal>();
        private Dictionary<DateTime, Dictionary<string, decimal>> _indexesValues = new Dictionary<DateTime, Dictionary<string, decimal>>();
        private Dictionary<string, string> mtmResultTypes = new Dictionary<string, string> { { "CASH FLOW", CASH_FLOW }, { "COMBINED", COMBINED }, { "P&L", PL } };


        private DateTime _periodFrom;
        private DateTime _periodTo;
        private DateTime _curveDate;

        private WaitDialogForm _dialogForm;
        private object _syncObject = new object();

        private const string IndexAverageConst = "IndexAverage";
        private const string CASH_FLOW = "CASH FLOW";
        private const string PL = "P&L";
        private const string COMBINED = "COMBINED";
        private bool RatioReportPL;
        private bool RatioReportCashFlow;
        private bool RatioReportCombined;

        DataTable positionRatiosDataTable = new DataTable();
        DataTable mtmRatiosDataTable = new DataTable();

        Dictionary<string, DataTable> mtmRationsDataTablePerIndex = new Dictionary<string, DataTable>();
        //private WaitDialogForm _dialogForm;
        //private WaitDialogForm _dialogForm2;

        //private Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMRatiosResults = new Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>();

        private ViewTradeGeneralInfo _viewTradesGeneralInfo;

        private Dictionary<string, GridControl> positionRatiosGridControls;
        private Dictionary<string, GridControl> mtmRatiosGridControls;

        private InitialMarginControl _initialMarginControl;
        private bool dataLoaded;
        private int _physicalVessels;

        #region labels

        private List<string> mtmLabelsFinancial = new List<string>
        {
            //"COMMISSION",
            "COVER AVERAGE RATE:",
            "COVER AVERAGE RATE (only for FFAs):",
            "INDEX EQUIVALENT OWNERS RATE:",
            "EFFECTIVE OWNERS EARNINGS RATE:",
            
            "AVG LONG DEAL PRICE:",
            "AVG SHORT DEAL PRICE:",
            "AVG LONG MTM PRICE:",
            "AVG SHORT MTM PRICE:",
            "LOCKED IN:",
            "UNREALIZED:",
            "BREAKEVEN PRICE:",
            "TOTAL MTM:",
            "(adj) for options:",
            "(incl. OPTIONS) AVG LONG DEAL PRICE:",
            "(incl. OPTIONS) AVG SHORT DEAL PRICE:",
             
            "(incl. OPTIONS) AVG LONG MTM PRICE:",
            "(incl. OPTIONS) AVG SHORT MTM PRICE:",

            //"(incl. OPTIONS) INDEX EQUIVALENT OWNERS RATE:",
            //"(incl. OPTIONS) EFFECTIVE OWNERS EARNINGS RATE:",
            //"(incl. OPTIONS) COVER AVERAGE RATE:",
            "(incl. OPTIONS) LOCKED IN:",
            "(incl. OPTIONS) UNREALIZED:",
            "(incl. OPTIONS) BREAKEVEN PRICE:",
        };
        private List<string> mtmLabelsPhysical = new List<string>
        {
            //"COMMISSION",
            "COVER AVERAGE RATE:",
            "INDEX EQUIVALENT OWNERS RATE:",
            "EFFECTIVE OWNERS EARNINGS RATE:",
            
            "AVG LONG DEAL PRICE:",
            "AVG SHORT DEAL PRICE:",
            "AVG LONG MTM PRICE:",
            "AVG SHORT MTM PRICE:",
            //"INDEX EQUIVALENT CHARTERERS RATE:",            
            //"EFFECTIVE CHARTERERS EARNINGS RATE:",            
            "LOCKED IN:",
            "UNREALIZED:",
            "BREAKEVEN PRICE:",
            "TOTAL MTM:",
            "(incl. OPTIONS) AVG LONG MTM PRICE:",
            "(incl. OPTIONS) AVG SHORT MTM PRICE:",
        };
        private List<string> mtmLabelsTotal = new List<string>
        {
            "TOTAL POSITION:",
            "TC IN DAYS:",
            "TC OUT DAYS:",
            "VESSEL TC COVER (%):",
            "NET PHYSICAL DAYS:",
            "NET PAPER DAYS:",
            "PAPER/PHYSICAL COVER (%):",
            "TOTAL LONG DAYS:",
            "TOTAL SHORT DAYS:",
            "NET DAYS:",
            "SHORT/LONG COVER (%):",
            "COVER (%):",
            "(incl. OPTIONS) TOTAL LONG DAYS:",
            "(incl. OPTIONS) TOTAL SHORT DAYS:",
            "(incl. OPTIONS) NET DAYS:",
            "(incl. OPTIONS) SHORT/LONG COVER (%):",
            "(incl. OPTIONS) COVER (%):",
            "(incl. OPTIONS) COVER AVERAGE RATE:",
            "(incl. OPTIONS) NET PAPER DAYS:",
            "COVER AVERAGE RATE:",
            "COVER AVERAGE RATE (only for FFAs):",
            "INDEX EQUIVALENT OWNERS RATE:",
            "EFFECTIVE OWNERS EARNINGS RATE:",
            
            "AVG LONG DEAL PRICE:",
            "AVG SHORT DEAL PRICE:",
            "AVG LONG MTM PRICE:",
            "AVG SHORT MTM PRICE:",
            "LOCKED IN:",
            "UNREALIZED:",
            "BREAKEVEN PRICE:",
            "(adj) for options:",
            "(incl. OPTIONS) AVG LONG DEAL PRICE:",
            "(incl. OPTIONS) AVG SHORT DEAL PRICE:",
            "(incl. OPTIONS) AVG LONG MTM PRICE:",
            "(incl. OPTIONS) AVG SHORT MTM PRICE:",
            "(incl. OPTIONS) LOCKED IN:",
            "(incl. OPTIONS) UNREALIZED:",
            "(incl. OPTIONS) BREAKEVEN PRICE:",
            //"(incl. OPTIONS) EFFECTIVE OWNERS EARNINGS RATE:",
            //"(incl. OPTIONS) INDEX EQUIVALENT OWNERS RATE:"
        };
        private List<string> posLabelsPhysical = new List<string>()
        {
            "TOTAL POSITION:",
            "TC IN DAYS:",
            "TC OUT DAYS:",
            "VESSEL TC COVER (%):",
            "NET PHYSICAL DAYS:",
            "NET PAPER DAYS:",
            "PAPER/PHYSICAL COVER (%):",
            "TOTAL LONG DAYS:",
            "TOTAL SHORT DAYS:",
            "NET DAYS:",
            "SHORT/LONG COVER (%):",
            "TOTAL FFA/OPTION DAYS:",
            "AVG LONG DEAL PRICE:",
            "AVG SHORT DEAL PRICE:",
            "COVER AVERAGE RATE:",

            "(adj) for options:",
            "(incl. OPTIONS) AVG LONG DEAL PRICE:",
            "(incl. OPTIONS) AVG SHORT DEAL PRICE:",
            "(incl. OPTIONS) TOTAL LONG DAYS:",
            "(incl. OPTIONS) TOTAL SHORT DAYS:",
            "(incl. OPTIONS) NET DAYS:",
            "(incl. OPTIONS) SHORT/LONG COVER (%):",
        };

        private List<string> posLabelsFinancial = new List<string>()
        {
            "TOTAL POSITION:",
            "TC IN DAYS:",
            "TC OUT DAYS:",
            "NET PHYSICAL DAYS:",
            "NET PAPER DAYS:",
            "COVER (%):",
            "COVER AVERAGE RATE:",
            "TOTAL LONG DAYS:",
            "TOTAL SHORT DAYS:",
            "NET DAYS:",
            "SHORT/LONG COVER (%):",
            "TOTAL FFA/OPTION DAYS:",
            "AVG LONG DEAL PRICE:",
            "AVG SHORT DEAL PRICE:",
            "FFA AVERAGE RATE:",
            "(adj) for options:",
            "(incl. OPTIONS) AVG LONG DEAL PRICE:",
            "(incl. OPTIONS) AVG SHORT DEAL PRICE:",
            "(incl. OPTIONS) TOTAL LONG DAYS:",
            "(incl. OPTIONS) TOTAL SHORT DAYS:",
            "(incl. OPTIONS) NET DAYS:",
            "(incl. OPTIONS) SHORT/LONG COVER (%):"
        };

        private List<string> posLabelsTotal = new List<string>()
        {
            "TOTAL POSITION:",
            "TC IN DAYS:",
            "TC OUT DAYS:",
            "VESSEL TC COVER (%):",
            "NET PHYSICAL DAYS:",
            "NET PAPER DAYS:",
            "COVER (%):",
            "COVER AVERAGE RATE:",
            "TOTAL LONG DAYS:",
            "TOTAL SHORT DAYS:",
            "NET DAYS:",
            "SHORT/LONG COVER (%):",
            "AVG LONG DEAL PRICE:",
            "AVG SHORT DEAL PRICE:",
            "(adj) for options:",
            "(incl. OPTIONS) TOTAL LONG DAYS:",
            "(incl. OPTIONS) TOTAL SHORT DAYS:",
            "(incl. OPTIONS) NET DAYS:",
            "(incl. OPTIONS) SHORT/LONG COVER (%):",
            "(incl. OPTIONS) AVG LONG DEAL PRICE:",
            "(incl. OPTIONS) AVG SHORT DEAL PRICE:"

        };

        #endregion

        #endregion

        #region Constructors

        public PositionMTMRatiosReportForm(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            InitializeComponent();

            _viewTradesGeneralInfo = viewTradesGeneralInfo;

            _tradeInformation = _viewTradesGeneralInfo.tradeInformations;
            _periodFrom = _viewTradesGeneralInfo.periodFrom;
            _periodTo = _viewTradesGeneralInfo.periodTo;
            _physicalVessels = _tradeInformation.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).WithDegreeOfParallelism(4).WithMergeOptions(ParallelMergeOptions.FullyBuffered).Where(a => a.PhysicalFinancial == "Physical").Count();
            //_curveDate = curveDate;           

            InitializeControls();
            // InitializeInitialMargin(); //Initialize Initial Margin Tab
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            /*Trade Type Filter*/
            //string[] tradeTypeItems = new string[] {
            //    "TC","FFA","Cargo","Call","Put"
            //};
            //int i = 0;
            foreach (string name in Enum.GetNames(typeof(TradeTypeWithTypeOptionInfoEnum)))
                chkCmbBxEdtTradeType.Properties.Items.Add((int)Enum.Parse(typeof(TradeTypeWithTypeOptionInfoEnum), name), name, CheckState.Checked, true);
            chkCmbBxEdtTradeType.Properties.BeforeShowMenu += Properties_BeforePopup;
            /************************ POSITION GRID CONTROL **********************/

            gridTradePositionResults.BeginUpdate();
            int gridViewColumnindex = 0;
            gridPositionResultsView.Columns.Clear();
            gridPositionResultsView.OptionsView.ColumnAutoWidth = false;

            gridPositionResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridPositionResultsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                                             UnboundColumnType.String, null));

            try
            {
                gridPositionResultsView.RestoreLayoutFromXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }

            gridTradePositionResults.EndUpdate();

            /*********************** MTM GRID CONTROL ***********************/
            gridTradeMTMResults.BeginUpdate();
            gridViewColumnindex = 0;
            gridMTMResultsView.Columns.Clear();
            gridMTMResultsView.OptionsView.ColumnAutoWidth = false;

            gridMTMResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, null));
            gridMTMResultsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                                             UnboundColumnType.String, null));

            try
            {
                gridMTMResultsView.RestoreLayoutFromXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }

            gridTradeMTMResults.EndUpdate();

            btnExport.Enabled = false;

            dtpFilterCurveDate.DateTime = _viewTradesGeneralInfo.lastImportDate;
            dtpFilterCurveDate.Properties.MaxValue = _viewTradesGeneralInfo.lastImportDate;

            cmbPositionMethod.SelectedItem = "Dynamic";
            cmbMarketSensitivityType.SelectedIndex = 0;
            rdgMTMResultsType.SelectedIndex = 2;
            rdgResultsAggregationType.SelectedIndex = 0;

            InitializeDictionaries();


        }

        private void InitializeInitialMargin()
        {
            _initialMarginControl = new InitialMarginControl(_viewTradesGeneralInfo) { Parent = this };
            lcInitialMargin.AddItem(new LayoutControlItem { Name = "lciInitialMargin", Control = _initialMarginControl, TextVisible = false });
        }

        private void InitializeDictionaries()
        {
            List<string> indexList = new List<string>();
            foreach (var trade in _tradeInformation)
            {
                trade.GroupedForwardCurve = trade.ForwardCurveForReports;
                var index = trade.GroupedForwardCurve + "-" + trade.PhysicalFinancial;
                try
                {
                    if(!indexList.Contains(index))
                        indexList.Add(index);
                }
                catch {
                }
                if (!indexesPhysicalFinancial.Contains(index))
                {
                    indexesPhysicalFinancial.Add(index);
                    CreateTabIndexSummaries(index, tbPosIndexSummaries, "POS");
                    CreateTabIndexSummaries(index, tbMTMIndexSummaries, "MTM");

                    var totalPageForCurvePOS = tbPosIndexSummaries.TabPages.Where(n => n.Name == "POS_TOTAL_" + trade.GroupedForwardCurve).FirstOrDefault();
                    var totalPageForCurveMTM = tbMTMIndexSummaries.TabPages.Where(n => n.Name == "MTM_TOTAL_" + trade.GroupedForwardCurve).FirstOrDefault();

                    if (totalPageForCurvePOS == null)
                    {
                        var page = new XtraTabPage { Name = "POS_TOTAL_" + trade.GroupedForwardCurve, Text = trade.GroupedForwardCurve + "-Total" };
                        tbPosIndexSummaries.TabPages.Add(page);

                        indexesPhysicalFinancialTotal.Add(trade.GroupedForwardCurve + "-Total");
                    }
                    else
                    {
                        int lastPositonOfPhysicalFinancial = 0;
                        foreach (XtraTabPage tabPage in tbPosIndexSummaries.TabPages)
                        {
                            if (tabPage.Name.Contains("POS_" + trade.GroupedForwardCurve))
                            {
                                lastPositonOfPhysicalFinancial = tbPosIndexSummaries.TabPages.IndexOf(tabPage);
                            }
                        }
                        tbPosIndexSummaries.TabPages.Move(lastPositonOfPhysicalFinancial + 1, totalPageForCurvePOS);
                    }
                    if (totalPageForCurveMTM == null)
                    {
                        var page = new XtraTabPage { Name = "MTM_TOTAL_" + trade.GroupedForwardCurve, Text = trade.GroupedForwardCurve + "-Total" };
                        tbMTMIndexSummaries.TabPages.Add(page);

                        indexesMTMPhysicalFinancialTotal.Add(trade.GroupedForwardCurve + "-Total");
                    }
                    else
                    {
                        int lastMTMOfPhysicalFinancial = 0;
                        foreach (XtraTabPage tabPage in tbMTMIndexSummaries.TabPages)
                        {
                            if (tabPage.Name.Contains("MTM_" + trade.GroupedForwardCurve))
                            {
                                lastMTMOfPhysicalFinancial = tbMTMIndexSummaries.TabPages.IndexOf(tabPage);
                            }
                        }
                        tbMTMIndexSummaries.TabPages.Move(lastMTMOfPhysicalFinancial + 1, totalPageForCurveMTM);
                    }
                }             

                if (!mtmCalculationsPerIndex.ContainsKey(index))
                {
                    mtmCalculationsPerIndex.Add(index, new Dictionary<string, Dictionary<string, decimal?>>());

                    mtmCalculationsPerIndex[index].Add(trade.GroupedForwardCurve, new Dictionary<string, decimal?>());


                    if (index.Contains("Physical"))
                    {
                        foreach (var l in mtmLabelsPhysical)
                        {
                            mtmCalculationsPerIndex[index].Add(l, new Dictionary<string, decimal?>());
                        }
                    }
                    else if (index.Contains("Financial"))
                    {
                        foreach (var l in mtmLabelsFinancial)
                        {
                            mtmCalculationsPerIndex[index].Add(l, new Dictionary<string, decimal?>());
                        }
                    }

                    if (!mtmCalculationsPerIndex.ContainsKey(trade.GroupedForwardCurve + "-" + "Total"))
                    {
                        mtmCalculationsPerIndex.Add(trade.GroupedForwardCurve + "-" + "Total", new Dictionary<string, Dictionary<string, decimal?>>());
                        mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + "Total"].Add(trade.GroupedForwardCurve, new Dictionary<string, decimal?>());

                        foreach (var l in mtmLabelsTotal)
                        {
                            mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + "Total"].Add(l, new Dictionary<string, decimal?>());
                        }
                    }
                }

                if (!posCalculationsPerIndex.ContainsKey(index))
                {
                    posCalculationsPerIndex.Add(index, new Dictionary<string, Dictionary<string, decimal?>>());

                    if (index.Contains("Physical"))
                    {
                        foreach (var l in posLabelsPhysical)
                        {
                            posCalculationsPerIndex[index].Add(l, new Dictionary<string, decimal?>());
                        }
                    }

                    else if (index.Contains("Financial"))
                    {
                        foreach (var l in posLabelsFinancial)
                        {
                            posCalculationsPerIndex[index].Add(l, new Dictionary<string, decimal?>());
                        }
                    }

                    if (!posCalculationsPerIndex.ContainsKey(trade.GroupedForwardCurve + "-" + "Total"))
                    {
                        posCalculationsPerIndex.Add(trade.GroupedForwardCurve + "-" + "Total", new Dictionary<string, Dictionary<string, decimal?>>());

                        foreach (var l in posLabelsTotal)
                        {
                            posCalculationsPerIndex[trade.GroupedForwardCurve + "-" + "Total"].Add(l, new Dictionary<string, decimal?>());
                        }
                    }
                }
            }
            try
            {
                foreach (var index in indexList)
                {
                    if (!mtmRationsDataTablePerIndex.ContainsKey(index)) mtmRationsDataTablePerIndex.Add(index, new DataTable());

                    if (!totalMTM.ContainsKey(index)) totalMTM.Add(index, 0);

                    if (!totalLongDays.ContainsKey(index)) totalLongDays.Add(index, 0);
                    if (!totalShortDays.ContainsKey(index)) totalShortDays.Add(index, 0);
                    if (!totalAvgLongPrice.ContainsKey(index)) totalAvgLongPrice.Add(index, 0);
                    if (!totalAvgShortPrice.ContainsKey(index)) totalAvgShortPrice.Add(index, 0);
                    if (!totalNetPhysicalDays.ContainsKey(index)) totalNetPhysicalDays.Add(index, 0);
                    if (!totalNetPaperDays.ContainsKey(index)) totalNetPaperDays.Add(index, 0);
                    if (!totalTcInDays.ContainsKey(index)) totalTcInDays.Add(index, 0);
                    if (!totalTcOutDays.ContainsKey(index)) totalTcOutDays.Add(index, 0);
                    if (!totalFfaDays.ContainsKey(index)) totalFfaDays.Add(index, 0);
                    if (!totalOptionDays.ContainsKey(index)) totalOptionDays.Add(index, 0);
                    if (!totalFfaNotional.ContainsKey(index)) totalFfaNotional.Add(index, 0);
                    if (!totalPaperNotional.ContainsKey(index)) totalPaperNotional.Add(index, 0);
                    if (!totalPaperNotionalFFA.ContainsKey(index)) totalPaperNotionalFFA.Add(index, 0);
                    if (!totalPaperOptional.ContainsKey(index)) totalPaperOptional.Add(index, 0);
                    if (!totalTcOutPrice.ContainsKey(index)) totalTcOutPrice.Add(index, 0);

                    if (!totalMTMLongPrice.ContainsKey(index)) totalMTMLongPrice.Add(index, 0);
                    if (!totalMTMLongPricePositionDays.ContainsKey(index)) totalMTMLongPricePositionDays.Add(index, 0);
                    if (!totalMTMShortPrice.ContainsKey(index)) totalMTMShortPrice.Add(index, 0);
                    if (!totalMTMShortPricePositionDays.ContainsKey(index)) totalMTMShortPricePositionDays.Add(index, 0);

                    if (!totalLongOptionDays.ContainsKey(index)) totalLongOptionDays.Add(index, 0);
                    if (!totalShortOptionDays.ContainsKey(index)) totalShortOptionDays.Add(index, 0);
                    if (!totalAvgLongOptionDays.ContainsKey(index)) totalAvgLongOptionDays.Add(index, 0);
                    if (!totalAvgShortOptionDays.ContainsKey(index)) totalAvgShortOptionDays.Add(index, 0);
                    if (!totalMTMLongOptionDays.ContainsKey(index)) totalMTMLongOptionDays.Add(index, 0);
                    if (!totalMTMShortOptionDays.ContainsKey(index)) totalMTMShortOptionDays.Add(index, 0);

                    if (!totalTcInCashFlow.ContainsKey(index)) totalTcInCashFlow.Add(index, 0);
                    if (!totalTcOutCashFlow.ContainsKey(index)) totalTcOutCashFlow.Add(index, 0);
                    if (!totalTcInDaysVesselIndex.ContainsKey(index)) totalTcInDaysVesselIndex.Add(index, 0);
                    if (!totalTcOutDaysVesselIndex.ContainsKey(index)) totalTcOutDaysVesselIndex.Add(index, 0);
                    if (!totalMTMIndexEqOwnersRateFin.ContainsKey(index)) totalMTMIndexEqOwnersRateFin.Add(index, 0);
                    if (!totalMTMIndexEqOwnersRateFinOption.ContainsKey(index)) totalMTMIndexEqOwnersRateFinOption.Add(index, 0);
                }
            }
            catch {
            }
        }

        private void CreateTabIndexSummaries(string index, XtraTabControl tab, string tabType)
        {
            var page = new XtraTabPage { Name = tabType + "_" + index, Text = index };
            tab.TabPages.Add(page);
        }

        private void LoadRatios()
        {
            posCalculationsPerIndex.Clear();
            mtmCalculationsPerIndex.Clear();
            IndexMTMLong.Clear();
            InitializeDictionaries();
            InitSumTotalDictionaries();

            CreatePositionResultGridColumns(gridPositionResultsView, gridTradePositionResults, _tradePositionResults, "POSITION");
            CreatePositionRatiosGridColumns(_tradePositionResults);

            InitSumTotalDictionaries();

            CreateMTMResultGridColumns(gridMTMResultsView, gridTradeMTMResults, _tradeMTMRatiosResults, "MTM");
            CreateMTMRatiosGridColumns(_tradeMTMRatiosResults);

            gridPositionResultsView.OptionsView.ShowFooter = false;
            gridMTMResultsView.OptionsView.ShowFooter = false;
        }

        private GridColumn AddColumn(string name, string caption, int columnIndex,
                                     UnboundColumnType columnType, object Tag)
        {
            var column = new GridColumn();
            column.Visible = true;
            column.AppearanceCell.Options.UseTextOptions = true;
            column.OptionsColumn.FixedWidth = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsColumn.AllowEdit = false;
            column.FieldName = name;
            column.Caption = caption;
            column.Tag = Tag;
            column.VisibleIndex = columnIndex;
            if (columnType != UnboundColumnType.Bound)
                column.UnboundType = columnType;

            switch (columnType)
            {
                case UnboundColumnType.String:
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.DateTime:
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.DisplayFormat.FormatType = FormatType.DateTime;
                    column.DisplayFormat.FormatString = "G";
                    break;
                case UnboundColumnType.Decimal:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.UnboundType = UnboundColumnType.Decimal;
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "N2";
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Integer:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.UnboundType = UnboundColumnType.Integer;
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Boolean:
                    column.UnboundType = UnboundColumnType.Boolean;
                    column.OptionsColumn.AllowEdit = false;
                    break;
            }

            return column;
        }

        #region MTM Tab

        private void CreateMTMResultGridColumns(GridView gridView, GridControl gridControl, Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> tradeResults, string type)
        {
            gridControl.BeginUpdate();
            gridControl.DataSource = null;
            gridView.Columns.Clear();
            gridView.OptionsView.ShowGroupPanel = false;
            gridView.OptionsView.ColumnAutoWidth = false;
            int gridViewColumnindex = 0;

            gridView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));

            List<DateTime> dates = tradeResults[PL].First().Value.Select(a => a.Key).ToList();
            //gridView.Columns.View.            

            GridColumn columnWithSummaryName = gridView.Columns[gridViewColumnindex - 1];

            GridColumnSummaryItem lockedInItem;
            GridColumnSummaryItem avgLongPriceItem;
            GridColumnSummaryItem avgShortPriceItem;
            GridColumnSummaryItem breakevenPriceItem;
            GridColumnSummaryItem coverAvgRateItem;
            GridColumnSummaryItem coverAvgRateFFAItem;
            GridColumnSummaryItem adjOptionItem;
            GridColumnSummaryItem totalMtmItem;
            GridColumnSummaryItem indexEqCharterersRateItem;
            GridColumnSummaryItem indexEqOwnersRateItem;
            GridColumnSummaryItem effCharterersEarningsRateItem;
            GridColumnSummaryItem effOwnersEarningsRateItem;
            GridColumnSummaryItem avgLongMTMPriceItem;
            GridColumnSummaryItem avgshortMTMPriceItem;
            GridColumnSummaryItem unrealizedItem;

            //GridColumnSummaryItem longDaysOptionItem;
            //GridColumnSummaryItem shortDaysOptionItem;
            //GridColumnSummaryItem netDaysOptionItem;                        

            GridColumnSummaryItem avgLongPriceOptionItem;
            GridColumnSummaryItem avgShortPriceOptionItem;
            GridColumnSummaryItem breakevenPriceOptionItem;
            GridColumnSummaryItem avgLongMTMPriceOptionItem;
            GridColumnSummaryItem avgshortMTMPriceOptionItem;
            GridColumnSummaryItem coverAvgRateOptionItem;
            GridColumnSummaryItem unrealizedOptionItem;
            GridColumnSummaryItem lockedInOptionItem;
            GridColumnSummaryItem indexEqOwnersRateOptionItem;
            GridColumnSummaryItem effOwnersEarningsRateOptionItem;

            //Total tab
            GridColumnSummaryItem totalPosition;
            GridColumnSummaryItem longDaysItem;
            GridColumnSummaryItem shortDaysItem;
            GridColumnSummaryItem netDaysItem;
            GridColumnSummaryItem coverItem;
            GridColumnSummaryItem totalCoverItem;
            GridColumnSummaryItem netPhysicalDaysItem;
            GridColumnSummaryItem netPaperDaysItem;
            GridColumnSummaryItem ppCoverItem;
            GridColumnSummaryItem tcInDaysItem;
            GridColumnSummaryItem tcOutDaysItem;
            GridColumnSummaryItem vesselRatioItem;
            GridColumnSummaryItem coverOptionItem;

            GridColumnSummaryItem longDaysOptionItem;
            GridColumnSummaryItem shortDaysOptionItem;
            GridColumnSummaryItem netDaysOptionItem;
            GridColumnSummaryItem netPaperDaysOption;

            GridColumnSummaryItem totalCoverOptionItem;


            int quarterAdded = 0;
            int calendarAdded = 0;
            GridColumn column = null;
            DateTime dTime;
            foreach (DateTime date in dates)
            {
                dTime = date;
                CreateAggregationColumns(ref dTime, ref quarterAdded, ref calendarAdded, ref gridViewColumnindex, ref column);

                if (column != null)
                {
                    gridView.Columns.Add(column);

                    #region summary Items
                    avgLongPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgLongPrice
                    };
                    avgShortPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgShortPrice
                    };
                    breakevenPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.BreakevenPrice
                    };
                    coverAvgRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverAvgRate
                    };
                    coverAvgRateFFAItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverAvgRateFFA
                    };
                    adjOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AdjOption
                    };

                    lockedInItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.LockedIn
                    };

                    totalMtmItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalMTM
                    };
                    indexEqCharterersRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.IndexEqCharterersRate
                    };
                    indexEqOwnersRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.IndexEqOwnersRate
                    };
                    effCharterersEarningsRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.EffCharterersEarningsRate
                    };
                    effOwnersEarningsRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.EffOwnersEarningsRate
                    };
                    avgLongMTMPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgLongMtmPrice
                    };
                    avgshortMTMPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgShortMtmPrice
                    };
                    unrealizedItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.Unrealized
                    };
                    coverAvgRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverAvgRate
                    };
                    coverAvgRateFFAItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverAvgRateFFA
                    };
                    ppCoverItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.PPCover
                    };
                    /*************************** Options Included *****************************/
                    //longDaysOptionItem = new GridColumnSummaryItem
                    //{
                    //    FieldName = column.FieldName,
                    //    SummaryType = SummaryItemType.Custom,
                    //    DisplayFormat = "{0:N2}",
                    //    Tag = ReportResultEnum.TotalLongDaysOption
                    //};
                    //shortDaysOptionItem = new GridColumnSummaryItem
                    //{
                    //    FieldName = column.FieldName,
                    //    SummaryType = SummaryItemType.Custom,
                    //    DisplayFormat = "{0:N2}",
                    //    Tag = ReportResultEnum.TotalShortDaysOption
                    //};
                    //netDaysOptionItem = new GridColumnSummaryItem
                    //{
                    //    FieldName = column.FieldName,
                    //    SummaryType = SummaryItemType.Custom,
                    //    DisplayFormat = "{0:N2}",
                    //    Tag = ReportResultEnum.NetDaysOption
                    //};                    
                    avgLongPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgLongPriceOption
                    };
                    avgShortPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgShortPriceOption
                    };
                    coverAvgRateOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverAvgRateOption
                    };
                    breakevenPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.BreakevenPriceOption
                    };                    
                    avgLongMTMPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgLongMtmPriceOption
                    };
                    avgshortMTMPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgShortMtmPriceOption
                    };
                    lockedInOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.LockedInOption
                    };
                    indexEqOwnersRateOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.IndexEqOwnersRateOption
                    };
                    effOwnersEarningsRateOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.EffOwnersEarningsRateOption
                    };
                    unrealizedOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.UnrealizedOption
                    };


                    var emptyItem1 = new GridColumnSummaryItem
                    {
                        FieldName = "Empty",
                        SummaryType = SummaryItemType.Custom,
                        Tag = ReportResultEnum.EmptyItem
                    };
                    var emptyItem2 = new GridColumnSummaryItem
                    {
                        FieldName = "Empty",
                        SummaryType = SummaryItemType.Custom,
                        Tag = ReportResultEnum.EmptyItem
                    };

                    //var indexItem = new GridColumnSummaryItem
                    //{
                    //    FieldName = column.FieldName,
                    //    SummaryType = SummaryItemType.Custom,
                    //    DisplayFormat = "{0:N2}",
                    //    Tag = IndexAverageConst + "_" + 42
                    //};

                    tcInDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TcInDays
                    };

                    tcOutDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TcOutDays
                    };
                    vesselRatioItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.VesselCover
                    };

                    netPhysicalDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetPhysicalDays
                    };
                    netPaperDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetPaperDays
                    };
                    netDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetDays
                    };
                    totalPosition = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalPosition
                    };
                    longDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalLongDays
                    };
                    shortDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalShortDays
                    };
                    coverItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}", //TODO:
                        Tag = ReportResultEnum.Cover
                    };
                    coverOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverOption
                    };
                    longDaysOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalLongDaysOption
                    };
                    shortDaysOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalShortDaysOption
                    };
                    netDaysOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetDaysOption
                    };
                    netPaperDaysOption = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetPaperDaysOption
                    };
                    totalCoverOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalCoverOption
                    };
                    totalCoverItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}", //TODO:
                        Tag = ReportResultEnum.TotalCover
                    };


                    //column.Summary.Add(indexItem);
                    foreach (var indexId in indexesIds)
                    {
                        var indexItem = new GridColumnSummaryItem
                        {
                            FieldName = column.FieldName,
                            SummaryType = SummaryItemType.Custom,
                            DisplayFormat = "{0:N2}",
                            Tag = IndexAverageConst + "_" + indexId
                        };
                        column.Summary.Add(indexItem);
                    }
                    column.Summary.Add(avgShortPriceOptionItem);
                    column.Summary.Add(totalMtmItem);
                    column.Summary.Add(avgLongPriceItem);
                    column.Summary.Add(avgShortPriceItem);
                    column.Summary.Add(avgLongMTMPriceItem);
                    column.Summary.Add(avgshortMTMPriceItem);
                    column.Summary.Add(indexEqCharterersRateItem);
                    column.Summary.Add(indexEqOwnersRateItem);

                    column.Summary.Add(coverAvgRateItem);
                    column.Summary.Add(coverAvgRateFFAItem);

                    column.Summary.Add(emptyItem1);

                    /**** Option ****/
                    //column.Summary.Add(longDaysOptionItem);
                    //column.Summary.Add(shortDaysOptionItem);
                    //column.Summary.Add(netDaysOptionItem);
                    column.Summary.Add(effCharterersEarningsRateItem);
                    column.Summary.Add(effOwnersEarningsRateItem);
                    column.Summary.Add(lockedInItem);
                    column.Summary.Add(unrealizedItem);
                    column.Summary.Add(breakevenPriceItem);
                    column.Summary.Add(ppCoverItem);

                    column.Summary.Add(emptyItem2);

                    column.Summary.Add(adjOptionItem);
                    column.Summary.Add(avgLongPriceOptionItem);
                    column.Summary.Add(avgLongMTMPriceOptionItem);
                    column.Summary.Add(avgshortMTMPriceOptionItem);
                    column.Summary.Add(lockedInOptionItem);
                    column.Summary.Add(unrealizedOptionItem);                    
                    column.Summary.Add(coverAvgRateOptionItem);                    
                    column.Summary.Add(indexEqOwnersRateOptionItem);
                    column.Summary.Add(effOwnersEarningsRateOptionItem);
                    

                    column.Summary.Add(tcInDaysItem);
                    column.Summary.Add(tcOutDaysItem);
                    column.Summary.Add(netPhysicalDaysItem);
                    column.Summary.Add(netPaperDaysItem);
                    column.Summary.Add(netDaysItem);
                    column.Summary.Add(totalPosition);
                    column.Summary.Add(longDaysItem);
                    column.Summary.Add(shortDaysItem);
                    column.Summary.Add(coverItem);
                    
                    column.Summary.Add(netPaperDaysOption);

                    column.Summary.Add(longDaysOptionItem);
                    column.Summary.Add(shortDaysOptionItem);
                    column.Summary.Add(netDaysOptionItem);
                    column.Summary.Add(vesselRatioItem);
                    column.Summary.Add(coverOptionItem);
                    column.Summary.Add(breakevenPriceOptionItem);
                    column.Summary.Add(totalCoverItem); 
                    column.Summary.Add(totalCoverOptionItem);
                }
                #endregion
            }

            //if (chkCalculateCommissions.Checked)
            //{
            //GridColumn columnCommission = null;
            //columnCommission = AddColumn("Commission", "Commission", gridViewColumnindex++,
            //    UnboundColumnType.Decimal,  "Commission");
            //gridView.Columns.Add(columnCommission);
            //}

            if (chkCalculateSums.Checked)
            {
                GridColumn columnTotal = AddColumn("Total", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total");
                gridView.Columns.Add(columnTotal);
            }

            #region Last Column Summary Items

            #region define summaries
            lockedInItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.LockedIn
            };
            avgLongPriceItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.AvgLongPrice
            };
            avgShortPriceItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.AvgShortPrice
            };
            breakevenPriceItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.BreakevenPrice
            };
            coverAvgRateItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.CoverAvgRate
            };
            coverAvgRateFFAItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.CoverAvgRateFFA
            };
            adjOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.AdjOption
            };

            totalMtmItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TotalMTM
            };
            //indexEqCharterersRateItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    Tag = ReportResultEnum.IndexEqCharterersRate
            //};
            indexEqOwnersRateItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.IndexEqOwnersRate
            };
            effCharterersEarningsRateItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.EffCharterersEarningsRate
            };
            effOwnersEarningsRateItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.EffOwnersEarningsRate
            };
            //avgLongMTMPriceItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    Tag = ReportResultEnum.AvgLongMtmPrice
            //};
            //avgshortMTMPriceItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    Tag = ReportResultEnum.AvgShortMtmPrice
            //};
            unrealizedItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.Unrealized
            };

            tcInDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.TcInDays
            };

            tcOutDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.TcOutDays
            };
            vesselRatioItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.VesselCover
            };

            netPhysicalDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.NetPhysicalDays
            };
            netPaperDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.NetPaperDays
            };
            netDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.NetDays
            };
            /*************************** Options Included *****************************/

            //longDaysOptionItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    DisplayFormat = "{0:N2}",
            //    Tag = ReportResultEnum.TotalLongDaysOption
            //};
            //shortDaysOptionItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    DisplayFormat = "{0:N2}",
            //    Tag = ReportResultEnum.TotalShortDaysOption
            //};
            //netDaysOptionItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    DisplayFormat = "{0:N2}",
            //    Tag = ReportResultEnum.NetDaysOption
            //};


            avgLongPriceOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.AvgLongPriceOption
            };
            avgShortPriceOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.AvgShortPriceOption
            };
            breakevenPriceOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.BreakevenPriceOption
            };
            coverAvgRateOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.CoverAvgRateOption
            };
            unrealizedOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.UnrealizedOption
            };
            //avgLongMTMPriceOptionItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    DisplayFormat = "{0:N2}",
            //    Tag = ReportResultEnum.AvgLongMtmPriceOption
            //};
            //avgshortMTMPriceOptionItem = new GridColumnSummaryItem
            //{
            //    FieldName = columnWithSummaryName.FieldName,
            //    SummaryType = SummaryItemType.Custom,
            //    DisplayFormat = "{0:N2}",
            //    Tag = ReportResultEnum.AvgShortMtmPriceOption
            //};
            lockedInOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.LockedInOption
            };
            indexEqOwnersRateOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.IndexEqOwnersRateOption
            };
            effOwnersEarningsRateOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.EffOwnersEarningsRateOption
            };

            var emptyItem3 = new GridColumnSummaryItem
            {
                FieldName = "Empty",
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.EmptyItem
            };

            var emptyItem4 = new GridColumnSummaryItem
            {
                FieldName = "Empty",
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.EmptyItem
            };

            #endregion

            {
                foreach (var indexId in indexesIds)
                {
                    var indexItem = new GridColumnSummaryItem
                    {
                        FieldName = columnWithSummaryName.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = IndexAverageConst + "_" + indexId
                    };
                    columnWithSummaryName.Summary.Add(indexItem);
                }

                columnWithSummaryName.Summary.Add(totalMtmItem);
                columnWithSummaryName.Summary.Add(avgLongPriceItem);
                columnWithSummaryName.Summary.Add(avgShortPriceItem);
                //columnWithSummaryName.Summary.Add(avgLongMTMPriceItem);
                //columnWithSummaryName.Summary.Add(avgshortMTMPriceItem);
                //columnWithSummaryName.Summary.Add(indexEqCharterersRateItem);
                columnWithSummaryName.Summary.Add(indexEqOwnersRateItem);
                columnWithSummaryName.Summary.Add(coverAvgRateItem);
                columnWithSummaryName.Summary.Add(coverAvgRateFFAItem);

                columnWithSummaryName.Summary.Add(emptyItem3);

                columnWithSummaryName.Summary.Add(tcInDaysItem);
                columnWithSummaryName.Summary.Add(tcOutDaysItem);
                columnWithSummaryName.Summary.Add(netPhysicalDaysItem);
                //column.Summary.Add(netPaperDaysItem);
                columnWithSummaryName.Summary.Add(netDaysItem);

                /**** Option ****/
                //columnWithSummaryName.Summary.Add(longDaysOptionItem);
                //columnWithSummaryName.Summary.Add(shortDaysOptionItem);
                //columnWithSummaryName.Summary.Add(netDaysOptionItem);

                columnWithSummaryName.Summary.Add(emptyItem4);
                columnWithSummaryName.Summary.Add(effCharterersEarningsRateItem);
                columnWithSummaryName.Summary.Add(effOwnersEarningsRateItem);
                columnWithSummaryName.Summary.Add(lockedInItem);
                columnWithSummaryName.Summary.Add(unrealizedItem);
                columnWithSummaryName.Summary.Add(breakevenPriceItem);

                columnWithSummaryName.Summary.Add(adjOptionItem);
                columnWithSummaryName.Summary.Add(indexEqOwnersRateOptionItem);
                columnWithSummaryName.Summary.Add(avgLongPriceOptionItem);
                columnWithSummaryName.Summary.Add(avgShortPriceOptionItem);
                //columnWithSummaryName.Summary.Add(avgLongMTMPriceOptionItem);
               //columnWithSummaryName.Summary.Add(avgshortMTMPriceOptionItem);                
                columnWithSummaryName.Summary.Add(effOwnersEarningsRateOptionItem);
                columnWithSummaryName.Summary.Add(coverAvgRateOptionItem);
                columnWithSummaryName.Summary.Add(lockedInOptionItem);
                columnWithSummaryName.Summary.Add(unrealizedOptionItem);
                columnWithSummaryName.Summary.Add(breakevenPriceOptionItem);

                #endregion

                gridControl.Tag = type;
                gridView.Tag = tradeResults;
                try
                {
                    string temp1 = string.Empty;
                    int idx = 0, idxF=0;
                    var temp = chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString();
                    //var temp1 = Enum.Parse(typeof(TradeTypeWithTypeOptionInfoEnum), a.Type);
                    foreach (var trade in _tradeInformation) {
                        if (chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString().Contains(((int)Enum.Parse(typeof(TradeTypeWithTypeOptionInfoEnum), trade.Type)).ToString()))
                            idx++;                        
                    }                
                    var newList = _tradeInformation.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).WithDegreeOfParallelism(4).WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                        Where(a => { temp1 = a.Identifier; idxF++; if (chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString().Contains(((int)Enum.Parse(typeof(TradeTypeWithTypeOptionInfoEnum), a.Type)).ToString())) return true; return false; }).Select(b => { return b; }).ToList();
                    idxF = 0;                    
                    gridControl.DataSource = newList;
                }
                catch {
                }
            }
            gridView.BestFitColumns();
            try
            {
                //gridView.BeginSummaryUpdate();
                //gridView.EndSummaryUpdate();
            }
            catch
            {

            }
            gridControl.EndUpdate();

        }

        private void CreateMTMRatiosGridColumns(Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> tradeResults)
        {
            mtmRatiosGridControls = new Dictionary<string, GridControl>();

            #region  OLd code

            //var gridViewColumnindex = 1;

            //    mtmRatiosDataTable = new DataTable();
            //    List<DateTime> dates = tradeResults[PL].First().Value.Select(a => a.Key).ToList();

            //    GridColumn columnSummaryName = AddColumn(
            //        "mtmSummaryName",
            //        "Ratio", gridViewColumnindex++,
            //        UnboundColumnType.Bound,
            //        null);
            //    columnSummaryName.Width = 200;
            //    gridViewMTMRatios.Columns.Add(columnSummaryName);
            //    mtmRatiosDataTable.Columns.Add(columnSummaryName.FieldName, typeof (string));

            //    foreach (DateTime date in dates)
            //    {
            //        GridColumn column = AddColumn(
            //            date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R",
            //            date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
            //            UnboundColumnType.Bound,
            //            Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
            //        column.DisplayFormat.FormatType = FormatType.Numeric;
            //        column.DisplayFormat.FormatString = "{0:N2}";
            //        gridViewMTMRatios.Columns.Add(column);
            //        mtmRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
            //    }

            //    for (int j = 0; j < 30; j++)
            //    {
            //        DataRow dr = mtmRatiosDataTable.NewRow();
            //        foreach (var date in dates)
            //            dr[date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R"] = "";
            //        mtmRatiosDataTable.Rows.Add(dr);
            //    }
            //    gridControlMTMRatios.DataSource = mtmRatiosDataTable;

            #endregion

            foreach (var index in indexesPhysicalFinancial)
            {
                var gridStatusControl = new GridControl();
                var gridStatusGridView = new GridView();

                mtmRatiosGridControls.Add(index, gridStatusControl);

                gridStatusControl.Name = "gridStatusControl";
                gridStatusControl.Dock = DockStyle.Fill;
                gridStatusControl.MainView = gridStatusGridView;

                gridStatusGridView.Name = "gridStatusGridView";
                gridStatusGridView.GridControl = gridStatusControl;
                gridStatusGridView.OptionsView.ShowGroupPanel = false;


                var gridViewColumnindex = 1;

                mtmRatiosDataTable = new DataTable();
                List<DateTime> dates = tradeResults[PL].First().Value.Select(a => a.Key).ToList();

                GridColumn columnSummaryName = AddColumn(
                    "mtmSummaryName",
                     "MTM Ratio - " + index, gridViewColumnindex++,
                    UnboundColumnType.Bound,
                    null);
                columnSummaryName.Width = 200;
                gridStatusGridView.Columns.Add(columnSummaryName);

                mtmRatiosDataTable.Columns.Add(columnSummaryName.FieldName, typeof(string));

                //foreach (DateTime date in dates)
                //{
                //    GridColumn column = AddColumn(
                //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R",
                //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                //        UnboundColumnType.Bound,
                //        Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                //    column.DisplayFormat.FormatType = FormatType.Numeric;
                //    column.DisplayFormat.FormatString = "{0:N2}";
                //    gridStatusGridView.Columns.Add(column);
                //    mtmRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                //}
                int quarterAdded = 0;
                int calendarAdded = 0;
                GridColumn column = null;
                DateTime dTime;
                foreach (DateTime date in dates)
                {
                    dTime = date;
                    CreateAggregationSummaryColumns(ref dTime, ref quarterAdded, ref calendarAdded, ref gridViewColumnindex, ref column);
                    if (column != null)
                    {
                        gridStatusGridView.Columns.Add(column);
                        mtmRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                    }
                }

                if (chkCalculateSums.Checked)
                {
                    //GridColumn columnTotal = AddColumn("Total" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total");
                    //gridStatusGridView.Columns.Add(columnTotal);
                    //mtmRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);

                    //another approach
                    GridColumn columnTotal = AddColumn("Total_2" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total_2");
                    gridStatusGridView.Columns.Add(columnTotal);
                    mtmRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);
                }

                tbMTMIndexSummaries.TabPages.Where(n => n.Name == "MTM_" + index).FirstOrDefault().Controls.Clear();
                tbMTMIndexSummaries.TabPages.Where(n => n.Name == "MTM_" + index).FirstOrDefault().Controls.Add(gridStatusControl);

                try
                {
                    CopyMTMCustomSummaries(index, mtmRatiosDataTable, dates);
                }
                catch
                {

                }
                gridStatusControl.DataSource = mtmRatiosDataTable;
                gridStatusControl.ForceInitialize();
                //save tables foreach index for export
            }
            try
            {
                foreach (var index in indexesMTMPhysicalFinancialTotal)
                {
                    var gridStatusControl = new GridControl();
                    var gridStatusGridView = new GridView();

                    mtmRatiosGridControls.Add(index, gridStatusControl);

                    gridStatusControl.Name = "gridStatusControl";
                    gridStatusControl.Dock = DockStyle.Fill;
                    gridStatusControl.MainView = gridStatusGridView;

                    gridStatusGridView.Name = "gridStatusGridView";
                    gridStatusGridView.GridControl = gridStatusControl;
                    gridStatusGridView.OptionsView.ShowGroupPanel = false;

                    var gridViewColumnindex = 1;

                    mtmRatiosDataTable = new DataTable();
                    List<DateTime> dates = tradeResults[PL].First().Value.Select(a => a.Key).ToList();

                    GridColumn columnSummaryName = AddColumn(
                        "mtmSummaryName",
                        "MTM Ratio - " + index, gridViewColumnindex++,
                        UnboundColumnType.Bound,
                        null);
                    columnSummaryName.Width = 200;
                    gridStatusGridView.Columns.Add(columnSummaryName);
                    mtmRatiosDataTable.Columns.Add(columnSummaryName.FieldName, typeof(string));

                    int quarterAdded = 0;
                    int calendarAdded = 0;
                    GridColumn column = null;
                    DateTime dTime;
                    foreach (DateTime date in dates)
                    {
                        dTime = date;
                        CreateAggregationSummaryColumns(ref dTime, ref quarterAdded, ref calendarAdded, ref gridViewColumnindex, ref column);
                        if (column != null)
                        {
                            gridStatusGridView.Columns.Add(column);
                            mtmRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                        }
                    }

                    if (chkCalculateSums.Checked)
                    {
                        //GridColumn columnTotal = AddColumn("Total" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total");
                        //gridStatusGridView.Columns.Add(columnTotal);
                        //mtmRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);

                        //another approach
                        GridColumn columnTotal = AddColumn("Total_2" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total_2");
                        gridStatusGridView.Columns.Add(columnTotal);
                        mtmRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);
                    }

                    var curve = index.Substring(0, index.IndexOf("-Total"));

                    tbMTMIndexSummaries.TabPages.Where(n => n.Name == "MTM_TOTAL_" + curve).FirstOrDefault().Controls.Clear();
                    tbMTMIndexSummaries.TabPages.Where(n => n.Name == "MTM_TOTAL_" + curve).FirstOrDefault().Controls.Add(gridStatusControl);
                    try
                    {
                        CopyMTMCustomSummaries(index, mtmRatiosDataTable, dates);
                    }
                    catch
                    {

                    }

                    gridStatusControl.DataSource = mtmRatiosDataTable;
                    gridStatusControl.ForceInitialize();
                }
            }
            catch
            {
            }
        }

        private void CopyMTMCustomSummaries(string index, DataTable mtmRatiosDataTable, List<DateTime> dates)
        {
            string[] indexParts = index.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            string indexStr = indexParts[0];
            #region Old
            //for (int i = 0; i < gridMTMResultsView.Columns.Count; i++)
            //{
            //    GridColumn column = gridMTMResultsView.Columns[i];

            //    if (column.Summary != null)
            //    {
            //        for (int j = 0; j < column.Summary.Count; j++)
            //        {
            //            GridColumnSummaryItem summary = column.Summary[j];

            //            if (summary.Tag != null && (summary.Tag.GetType() == typeof(ReportResultEnum) &&
            //                                        ((ReportResultEnum)summary.Tag == ReportResultEnum.IndexEqOwnersRate
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.IndexEqCharterersRate
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.EffCharterersEarningsRate
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.EffOwnersEarningsRate
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.Unrealized
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.LockedIn
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.BreakevenPrice
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgLongMtmPrice
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgShortMtmPrice
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.CoverAvgRate
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AdjOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgLongPrice
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgShortPrice
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.LockedInOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.UnrealizedOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.TotalLongDaysOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.TotalShortDaysOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.NetDaysOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.CoverOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgLongPriceOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgShortPriceOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgShortMtmPriceOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.AvgLongMtmPriceOption
            //                                         || (ReportResultEnum)summary.Tag == ReportResultEnum.BreakevenPriceOption)
            //                                        || summary.Tag.ToString().Contains(IndexAverageConst)))
            //            {
            //                if (mtmRatiosDataTable.Columns.Contains(column.FieldName + "_R"))
            //                {
            //                    decimal value;
            //                    if (Decimal.TryParse(summary.SummaryValue.ToString(), out value))
            //                        mtmRatiosDataTable.Rows[j][column.FieldName + "_R"] = Math.Round(value, 3);
            //                    else
            //                        mtmRatiosDataTable.Rows[j][column.FieldName + "_R"] = summary.SummaryValue;
            //                }
            //                if (column.FieldName == "PhysicalFinancial")
            //                    mtmRatiosDataTable.Rows[j]["mtmSummaryName"] = summary.SummaryValue;
            //            }
            //        }
            //    }
            //}

            #endregion

            #region Index

            DataRow drIndex = mtmRatiosDataTable.NewRow();
            drIndex[0] = indexStr;
            string dateString = "", strQuarter;
            int quarterAdded = 0, calendarAdded = 0, intQuarter, intCalendar;
            decimal? indexMonthTotal = null;
            //Add Index Row
            foreach (DateTime date in dates)
            {
                //var dateString = date.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    dateString = date.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                {
                    quarterAdded = 0;
                    intQuarter = ((date.Month - 1) / 3) + 1;
                    if (intQuarter != quarterAdded)
                    {
                        quarterAdded = intQuarter;
                        strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                        dateString = strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB"));
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                {
                    calendarAdded = 0;
                    intCalendar = date.Year;
                    if (intCalendar != calendarAdded)
                    {
                        calendarAdded = intCalendar;
                        dateString = "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB"));
                    }
                }
                try
                {
                    decimal? value = null;
                    if (mtmCalculationsPerIndex[index][indexStr].TryGetValue(dateString, out value))
                    {
                        decimal indexMonth = Math.Round(Convert.ToDecimal(mtmCalculationsPerIndex[index][indexStr][dateString]), 3);
                        drIndex[dateString + "_R"] = indexMonth;
                        indexMonthTotal = (indexMonthTotal??0) + indexMonth;
                    }
                    else
                    {
                        drIndex[dateString + "_R"] = "N/A";
                    }
                }
                catch
                {
                    drIndex[dateString + "_R"] = "N/A";
                }

            }
            if (chkCalculateSums.Checked)
            {
                if (indexMonthTotal == null)
                {
                    drIndex["Total" + "_2_R"] = "N/A";
                }
                else
                {
                    drIndex["Total" + "_2_R"] = indexMonthTotal / dates.Count;
                }
            }

            mtmRatiosDataTable.Rows.Add(drIndex);

            #endregion

            #region Labels
            List<string> posLabels;
            if (index.Contains("Physical"))
            {
                posLabels = mtmLabelsPhysical;
            }
            else if (index.Contains("Financial"))
            {
                posLabels = mtmLabelsFinancial;
            }
            else
            {
                posLabels = mtmLabelsTotal;
            }
            foreach (var l in posLabels)
            {
                if ((l == "TOTAL MTM:" && index == index.Substring(0, index.IndexOf("-")) + "-Financial") || l == "(incl. OPTIONS) NET PAPER DAYS:") continue;
                if ((l == "(incl. OPTIONS) AVG LONG MTM PRICE:" && index == index.Substring(0, index.IndexOf("-")) + "-Physical")) continue;
                if ((l == "(incl. OPTIONS) AVG SHORT MTM PRICE:" && index == index.Substring(0, index.IndexOf("-")) + "-Physical")) continue;
                //if (((index == index.Substring(0, index.IndexOf("-")) + "-Financial") || index == index.Substring(0, index.IndexOf("-")) + "-Total") &&  (l == "(incl. OPTIONS) INDEX EQUIVALENT OWNERS RATE:" || l == "(incl. OPTIONS) EFFECTIVE OWNERS EARNINGS RATE:" || l == "(incl. OPTIONS) COVER AVERAGE RATE:")) continue;
                if ((index == index.Substring(0, index.IndexOf("-")) + "-Total") && ((l == "TOTAL POSITION:") ||
                    (l == "TC IN DAYS:") ||
                    (l == "TC OUT DAYS:") ||
                    (l == "VESSEL TC COVER (%):") ||
                    (l == "NET PHYSICAL DAYS:") ||
                    (l == "NET PAPER DAYS:") ||
                    (l == "PAPER/PHYSICAL COVER (%):") ||
                    (l == "TOTAL LONG DAYS:") ||
                    (l == "TOTAL SHORT DAYS:") ||
                    (l == "NET DAYS:") ||
                    (l == "SHORT/LONG COVER (%):") ||
                    (l == "COVER (%):") ||
                    (l == "(incl. OPTIONS) TOTAL LONG DAYS:") ||
                    (l == "(incl. OPTIONS) TOTAL SHORT DAYS:") ||
                    (l == "(incl. OPTIONS) NET DAYS:") ||
                    (l == "(incl. OPTIONS) SHORT/LONG COVER (%):") ||
                    (l == "(incl. OPTIONS) COVER (%):") ||
                    (l == "(incl. OPTIONS) COVER AVERAGE RATE:") ||
                    (l == "(incl. OPTIONS) NET PAPER DAYS:")
                 ))
                {
                    continue;                   
                }

                if (l == "AVG LONG DEAL PRICE:")
                {
                    AddRowWithFirstCellLabel("FFAs only", ref mtmRatiosDataTable);
                }
                //if (l == "AVG LONG MTM PRICE:" && index == index.Substring(0, index.IndexOf("-")) + "-Physical") mtmRatiosDataTable.Rows.Add(mtmRatiosDataTable.NewRow());

                DataRow dr = mtmRatiosDataTable.NewRow();
                dr[0] = l;
                //Add Label Row
                foreach (DateTime date in dates)
                {

                    //var dateString = date.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                    if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                    {
                        dateString = date.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                    }
                    else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                    {
                        quarterAdded = 0;
                        intQuarter = ((date.Month - 1) / 3) + 1;
                        if (intQuarter != quarterAdded)
                        {
                            quarterAdded = intQuarter;
                            strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                            dateString = strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB"));
                        }
                    }
                    else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                    {
                        calendarAdded = 0;
                        intCalendar = date.Year;
                        if (intCalendar != calendarAdded)
                        {
                            calendarAdded = intCalendar;
                            dateString = "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB"));
                        }
                    }
                    if (l == "(adj) for options:")
                    {
                        dr[dateString + "_R"] = "";
                    }
                    else if (mtmCalculationsPerIndex[index][l].ContainsKey(dateString))
                    {
                        var valueFromDictionary = mtmCalculationsPerIndex[index][l][dateString];

                        if (valueFromDictionary == null)
                            dr[dateString + "_R"] = "N/A";
                        else
                        {
                            if (l.Contains("(%)"))
                                dr[dateString + "_R"] = Convert.ToDecimal(mtmCalculationsPerIndex[index][l][dateString]).ToString("P", CultureInfo.InvariantCulture);
                            else
                                dr[dateString + "_R"] = Math.Round(Convert.ToDecimal(mtmCalculationsPerIndex[index][l][dateString]), 3);
                        }
                    }
                    else
                    {
                        //if (l != "FFAs ONLY:" )
                            dr[dateString + "_R"] = "N/A";
                    }
                }               
                try
                {                    
                    if (chkCalculateSums.Checked)
                    {
                        decimal? valueFromDictionary;                       
                        try
                        {
                            if (l == "(adj) for options:")
                            {
                                dr[dateString + "_R"] = "";
                            }
                            else
                            {
                                valueFromDictionary = GenerateTotalsMTM(index, l, dates);
                                if (valueFromDictionary == null)
                                {
                                    dr["Total" + "_2_R"] = "N/A";
                                }
                                else
                                {
                                    if (l.Contains("(%)"))
                                        dr["Total" + "_2_R"] = Convert.ToDecimal(valueFromDictionary).ToString("P", CultureInfo.InvariantCulture);
                                    else
                                        dr["Total" + "_2_R"] = Math.Round(Convert.ToDecimal(valueFromDictionary), 3);
                                }
                            }
                        }
                        catch
                        {
                        }
                        
                    }
                }
                catch
                {

                }

                mtmRatiosDataTable.Rows.Add(dr);
            }
            #endregion
        }

        #endregion

        #region Position Tab

        private void CreatePositionResultGridColumns(GridView gridView, GridControl gridControl, Dictionary<string, Dictionary<DateTime, decimal>> tradeResults, string type)
        {
            gridControl.BeginUpdate();
            gridControl.DataSource = null;
            gridView.Columns.Clear();
            gridView.OptionsView.ShowGroupPanel = false;
            gridView.OptionsView.ColumnAutoWidth = false;
            int gridViewColumnindex = 0;


            gridView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));
            gridView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                            UnboundColumnType.Bound, null));

            GridColumn columnWithSummaryName = gridView.Columns[gridViewColumnindex - 1];

            List<DateTime> dates = tradeResults.First().Value.Select(a => a.Key).ToList();

            GridColumnSummaryItem totalPosition;
            GridColumnSummaryItem longDaysItem;
            GridColumnSummaryItem shortDaysItem;
            GridColumnSummaryItem netDaysItem;
            GridColumnSummaryItem coverItem;
            GridColumnSummaryItem totalCoverItem;
            GridColumnSummaryItem avgLongPriceItem;
            GridColumnSummaryItem avgShortPriceItem;
            GridColumnSummaryItem netPhysicalDaysItem;
            GridColumnSummaryItem netPaperDaysItem;
            GridColumnSummaryItem ppCoverItem;
            GridColumnSummaryItem tcInDaysItem;
            GridColumnSummaryItem tcOutDaysItem;
            GridColumnSummaryItem vesselRatioItem;
            GridColumnSummaryItem coverAvgRateItem;
            GridColumnSummaryItem adjOptionItem;
            GridColumnSummaryItem ffaOrOptionDaysItem;
            GridColumnSummaryItem tcOutPriceItem;
            GridColumnSummaryItem ffaAvgRateItem;

            GridColumnSummaryItem longDaysOptionItem;
            GridColumnSummaryItem shortDaysOptionItem;
            GridColumnSummaryItem netDaysOptionItem;
            GridColumnSummaryItem coverOptionItem;
            GridColumnSummaryItem avgLongPriceOptionItem;
            GridColumnSummaryItem avgShortPriceOptionItem;
            GridColumnSummaryItem totalCoverOptionItem;
            GridColumnSummaryItem coverAvgRateOptionItem;

            int quarterAdded = 0;
            int calendarAdded = 0;
            GridColumn column = null;
            DateTime dTime;
            #region summary Items
            foreach (DateTime date in dates)
            {
                dTime = date;
                CreateAggregationColumns(ref dTime, ref quarterAdded, ref calendarAdded, ref gridViewColumnindex, ref column);
                if (column != null)
                {
                    gridView.Columns.Add(column);

                    /************************* POSITION ***********************/
                    totalPosition = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalPosition
                    };
                    longDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalLongDays
                    };
                    shortDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalShortDays
                    };
                    netDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetDays
                    };
                    coverItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}", //TODO:
                        Tag = ReportResultEnum.Cover
                    };
                    totalCoverItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}", //TODO:
                        Tag = ReportResultEnum.TotalCover
                    };
                    avgLongPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgLongPrice
                    };
                    avgShortPriceItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgShortPrice
                    };
                    netPhysicalDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetPhysicalDays
                    };
                    netPaperDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetPaperDays
                    };
                    ppCoverItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.PPCover
                    };
                    tcInDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TcInDays
                    };
                    tcOutDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TcOutDays
                    };
                    vesselRatioItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.VesselCover
                    };
                    coverAvgRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverAvgRate
                    };
                    adjOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AdjOption
                    };
                    ffaOrOptionDaysItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalFFAOrOptionDays
                    };
                    ffaAvgRateItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.FFAAverageRate
                    };

                    /*************************** Options Included *****************************/

                    longDaysOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalLongDaysOption
                    };
                    shortDaysOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.TotalShortDaysOption
                    };
                    netDaysOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.NetDaysOption
                    };
                    coverOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.CoverOption
                    };
                    avgLongPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgLongPriceOption
                    };
                    avgShortPriceOptionItem = new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        SummaryType = SummaryItemType.Custom,
                        DisplayFormat = "{0:N2}",
                        Tag = ReportResultEnum.AvgShortPriceOption
                    };
                    var emptyItem1 = new GridColumnSummaryItem
                    {
                        FieldName = "Empty",
                        SummaryType = SummaryItemType.Custom,
                        Tag = ReportResultEnum.EmptyItem
                    };
                    
                    column.Summary.Add(tcInDaysItem);
                    column.Summary.Add(tcOutDaysItem);
                    column.Summary.Add(vesselRatioItem);
                    column.Summary.Add(netPhysicalDaysItem);
                    column.Summary.Add(netPaperDaysItem);
                    column.Summary.Add(totalPosition);
                    column.Summary.Add(ppCoverItem);
                    column.Summary.Add(ffaOrOptionDaysItem);
                    column.Summary.Add(longDaysItem);
                    column.Summary.Add(shortDaysItem);
                    column.Summary.Add(netDaysItem);
                    column.Summary.Add(coverItem);
                    column.Summary.Add(totalCoverItem);
                    column.Summary.Add(ffaAvgRateItem);

                    column.Summary.Add(emptyItem1);

                    /**** Option ****/
                    column.Summary.Add(longDaysOptionItem);
                    column.Summary.Add(shortDaysOptionItem);
                    column.Summary.Add(netDaysOptionItem);
                    column.Summary.Add(coverOptionItem);

                    column.Summary.Add(avgLongPriceItem);
                    column.Summary.Add(avgShortPriceItem);
                    column.Summary.Add(coverAvgRateItem);
                    column.Summary.Add(adjOptionItem);
                    column.Summary.Add(avgLongPriceOptionItem);
                    column.Summary.Add(avgShortPriceOptionItem);
                }
            }
            #endregion

            if (chkCalculateSums.Checked)
            {
                GridColumn columnTotal = AddColumn("Total", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total");
                gridView.Columns.Add(columnTotal);
                //different approach
                // columnTotal = AddColumn("_Total", "_Total", gridViewColumnindex++, UnboundColumnType.Decimal, "_Total");
                //gridView.Columns.Add(columnTotal);
            }

            #region Last Column Summary Items

            #region define summaries

            totalPosition = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TotalPosition
            };

            longDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TotalLongDays
            };
            shortDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TotalShortDays
            };
            netDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.NetDays
            };
            coverItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.Cover
            };
            totalCoverItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TotalCover
            };
            avgLongPriceItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.AvgLongPrice
            };
            avgShortPriceItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.AvgShortPrice
            };
            netPhysicalDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.NetPhysicalDays
            };
            netPaperDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.NetPaperDays
            };
            ppCoverItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.PPCover
            };
            tcInDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TcInDays
            };
            tcOutDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TcOutDays
            };
            vesselRatioItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.VesselCover
            };
            coverAvgRateItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.CoverAvgRate
            };
            adjOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.AdjOption
            };
            ffaOrOptionDaysItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.TotalFFAOrOptionDays
            };
            ffaAvgRateItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.FFAAverageRate
            };

            /*************************** Options Included *****************************/

            longDaysOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.TotalLongDaysOption
            };
            shortDaysOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.TotalShortDaysOption
            };
            netDaysOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.NetDaysOption
            };
            coverOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.CoverOption
            };
            avgLongPriceOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.AvgLongPriceOption
            };
            avgShortPriceOptionItem = new GridColumnSummaryItem
            {
                FieldName = columnWithSummaryName.FieldName,
                SummaryType = SummaryItemType.Custom,
                DisplayFormat = "{0:N2}",
                Tag = ReportResultEnum.AvgShortPriceOption
            };
            var emptyItem3 = new GridColumnSummaryItem
            {
                FieldName = "Empty",
                SummaryType = SummaryItemType.Custom,
                Tag = ReportResultEnum.EmptyItem
            };

            #endregion
            
            columnWithSummaryName.Summary.Add(tcInDaysItem);
            columnWithSummaryName.Summary.Add(tcOutDaysItem);
            columnWithSummaryName.Summary.Add(vesselRatioItem);
            columnWithSummaryName.Summary.Add(netPhysicalDaysItem);
            columnWithSummaryName.Summary.Add(netPaperDaysItem);
            columnWithSummaryName.Summary.Add(totalPosition);
            columnWithSummaryName.Summary.Add(ppCoverItem);
            columnWithSummaryName.Summary.Add(ffaOrOptionDaysItem);
            columnWithSummaryName.Summary.Add(longDaysItem);
            columnWithSummaryName.Summary.Add(shortDaysItem);
            columnWithSummaryName.Summary.Add(netDaysItem);
            columnWithSummaryName.Summary.Add(coverItem);
            columnWithSummaryName.Summary.Add(totalCoverItem);
            columnWithSummaryName.Summary.Add(ffaAvgRateItem);

            columnWithSummaryName.Summary.Add(emptyItem3);

            /**** Option ****/
            columnWithSummaryName.Summary.Add(longDaysOptionItem);
            columnWithSummaryName.Summary.Add(shortDaysOptionItem);
            columnWithSummaryName.Summary.Add(netDaysOptionItem);
            columnWithSummaryName.Summary.Add(coverOptionItem);

            columnWithSummaryName.Summary.Add(avgLongPriceItem);
            columnWithSummaryName.Summary.Add(avgShortPriceItem);
            columnWithSummaryName.Summary.Add(coverAvgRateItem);
            columnWithSummaryName.Summary.Add(adjOptionItem);
            columnWithSummaryName.Summary.Add(avgLongPriceOptionItem);
            columnWithSummaryName.Summary.Add(avgShortPriceOptionItem);

            #endregion

            gridControl.Tag = type;
            gridView.Tag = tradeResults;
            gridControl.DataSource = _tradeInformation.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).WithDegreeOfParallelism(4).WithMergeOptions(ParallelMergeOptions.FullyBuffered).
                        Where(a => { if (chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString().Contains(((int)Enum.Parse(typeof(TradeTypeWithTypeOptionInfoEnum), a.Type)).ToString())) return true; return false; }).Select(b => { return b; }).ToList();

            gridView.BestFitColumns();
            gridControl.EndUpdate();
        }

        private void CreatePositionRatiosGridColumns(Dictionary<string, Dictionary<DateTime, decimal>> tradeResults)
        {
            #region old
            //var gridViewColumnindex = 1;
            //positionRatiosDataTable = new DataTable();
            //List<DateTime> dates = tradeResults.First().Value.Select(a => a.Key).ToList();

            //GridColumn columnSummaryName = AddColumn(
            //        "positionSummaryName",
            //        "Ratio", gridViewColumnindex++,
            //        UnboundColumnType.Bound,
            //        null);
            //columnSummaryName.Width = 200;
            //gridViewPositionRatios.Columns.Add(columnSummaryName);
            //positionRatiosDataTable.Columns.Add(columnSummaryName.FieldName, typeof(string));

            //foreach (DateTime date in dates)
            //{
            //    GridColumn column = AddColumn(
            //        date.ToString("MMM-yyyy", new CultureInfo("en-GB"))+ "_R",
            //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
            //        UnboundColumnType.Bound,
            //        Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
            //    column.DisplayFormat.FormatType = FormatType.Numeric;
            //    column.DisplayFormat.FormatString = "{0:N2}";
            //    gridViewPositionRatios.Columns.Add(column);
            //    positionRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
            //}

            //for (int j = 0; j < 18; j++ )
            //{
            //    DataRow dr = positionRatiosDataTable.NewRow();
            //    foreach (var date in dates)
            //        dr[date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R"] = "";
            //    positionRatiosDataTable.Rows.Add(dr);
            //}
            //gridControlPositionRatios.DataSource = positionRatiosDataTable;
            #endregion

            positionRatiosGridControls = new Dictionary<string, GridControl>();

            foreach (var index in indexesPhysicalFinancial)
            {
                var gridPositionControl = new GridControl();
                var gridPositionGridView = new GridView();
                positionRatiosGridControls.Add(index, gridPositionControl);

                gridPositionControl.Name = "gridStatusControl";
                gridPositionControl.Dock = DockStyle.Fill;
                gridPositionControl.MainView = gridPositionGridView;

                gridPositionGridView.Name = "gridStatusGridView";
                gridPositionGridView.GridControl = gridPositionControl;
                gridPositionGridView.OptionsView.ShowGroupPanel = false;

                var gridViewColumnindex = 1;
                positionRatiosDataTable = new DataTable();
                List<DateTime> dates = tradeResults.First().Value.Select(a => a.Key).ToList();

                GridColumn columnSummaryName = AddColumn(
                    "positionSummaryName",
                    "Position Ratio - " + index, gridViewColumnindex++,
                    UnboundColumnType.Bound,
                    null);
                columnSummaryName.Width = 200;
                gridPositionGridView.Columns.Add(columnSummaryName);
                positionRatiosDataTable.Columns.Add(columnSummaryName.FieldName, typeof(string));

                //foreach (DateTime date in dates)
                //{
                //    GridColumn column = AddColumn(
                //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R",
                //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                //        UnboundColumnType.Bound,
                //        Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                //    column.DisplayFormat.FormatType = FormatType.Numeric;
                //    column.DisplayFormat.FormatString = "{0:N2}";
                //    gridPositionGridView.Columns.Add(column);
                //    positionRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                //}
                int quarterAdded = 0;
                int calendarAdded = 0;
                GridColumn column = null;
                DateTime dTime;
                foreach (DateTime date in dates)
                {
                    dTime = date;
                    CreateAggregationSummaryColumns(ref dTime, ref quarterAdded, ref calendarAdded, ref gridViewColumnindex, ref column);
                    if (column != null)
                    {
                        gridPositionGridView.Columns.Add(column);
                        positionRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                    }
                }

                if (chkCalculateSums.Checked)
                {
                    //GridColumn columnTotal = AddColumn("Total" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total");
                    //gridPositionGridView.Columns.Add(columnTotal);
                    //positionRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);

                    //another approach
                    GridColumn columnTotal = AddColumn("Total_2" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total_2");
                    gridPositionGridView.Columns.Add(columnTotal);
                    positionRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);
                }

                tbPosIndexSummaries.TabPages.Where(n => n.Name == "POS_" + index).FirstOrDefault().Controls.Clear();
                tbPosIndexSummaries.TabPages.Where(n => n.Name == "POS_" + index).FirstOrDefault().Controls.Add(gridPositionControl);

                try
                {
                    CopyPositionCustomSummaries(index, positionRatiosDataTable, dates);
                }
                catch
                {

                }
                gridPositionControl.DataSource = positionRatiosDataTable;
                gridPositionControl.ForceInitialize();
            }

            foreach (var index in indexesPhysicalFinancialTotal)
            {
                var gridPositionControl = new GridControl();
                var gridPositionGridView = new GridView();
                positionRatiosGridControls.Add(index, gridPositionControl);

                gridPositionControl.Name = "gridStatusControl";
                gridPositionControl.Dock = DockStyle.Fill;
                gridPositionControl.MainView = gridPositionGridView;

                gridPositionGridView.Name = "gridStatusGridView";
                gridPositionGridView.GridControl = gridPositionControl;
                gridPositionGridView.OptionsView.ShowGroupPanel = false;

                var gridViewColumnindex = 1;
                positionRatiosDataTable = new DataTable();
                List<DateTime> dates = tradeResults.First().Value.Select(a => a.Key).ToList();

                GridColumn columnSummaryName = AddColumn(
                    "positionSummaryName",
                    "Position Ratio - " + index, gridViewColumnindex++,
                    UnboundColumnType.Bound,
                    null);
                columnSummaryName.Width = 200;
                gridPositionGridView.Columns.Add(columnSummaryName);
                positionRatiosDataTable.Columns.Add(columnSummaryName.FieldName, typeof(string));

                //foreach (DateTime date in dates)
                //{
                //    GridColumn column = AddColumn(
                //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R",
                //        date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                //        UnboundColumnType.Bound,
                //        Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                //    column.DisplayFormat.FormatType = FormatType.Numeric;
                //    column.DisplayFormat.FormatString = "{0:N2}";
                //    gridPositionGridView.Columns.Add(column);
                //    positionRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                //}
                int quarterAdded = 0;
                int calendarAdded = 0;
                GridColumn column = null;
                DateTime dTime;
                foreach (DateTime date in dates)
                {
                    dTime = date;
                    CreateAggregationSummaryColumns(ref dTime, ref quarterAdded, ref calendarAdded, ref gridViewColumnindex, ref column);
                    if (column != null)
                    {
                        gridPositionGridView.Columns.Add(column);
                        positionRatiosDataTable.Columns.Add(column.FieldName, column.ColumnType);
                    }
                }

                if (chkCalculateSums.Checked)
                {
                    //GridColumn columnTotal = AddColumn("Total" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total");
                    //gridPositionGridView.Columns.Add(columnTotal);
                    //positionRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);

                    //another approach
                    GridColumn columnTotal = AddColumn("Total_2" + "_R", "Total", gridViewColumnindex++, UnboundColumnType.Decimal, "Total_2");
                    gridPositionGridView.Columns.Add(columnTotal);
                    positionRatiosDataTable.Columns.Add(columnTotal.FieldName, columnTotal.ColumnType);
                }

                var curve = index.Substring(0, index.IndexOf("-Total"));

                tbPosIndexSummaries.TabPages.Where(n => n.Name == "POS_TOTAL_" + curve).FirstOrDefault().Controls.Clear();
                tbPosIndexSummaries.TabPages.Where(n => n.Name == "POS_TOTAL_" + curve).FirstOrDefault().Controls.Add(gridPositionControl);
                try
                {
                    CopyPositionCustomSummaries(index, positionRatiosDataTable, dates);
                }
                catch
                {

                }

                gridPositionControl.DataSource = positionRatiosDataTable;
                gridPositionControl.ForceInitialize();
            }
        }

        private void CopyPositionCustomSummaries(string index, DataTable positionRatiosDataTable, List<DateTime> dates)
        {
            List<string> posLabels;
            if (index.Contains("Physical"))
            {
                posLabels = posLabelsPhysical;
            }
            else if (index.Contains("Financial"))
            {
                posLabels = posLabelsFinancial;
            }
            else
            {
                posLabels = posLabelsTotal;
            }

            string dateString = "", strQuarter;
            DataRow dr;
            int quarterAdded = 0, calendarAdded = 0, intQuarter, intCalendar;
            foreach (var l in posLabels)
            {
                //if (l == "AVG LONG DEAL PRICE:") positionRatiosDataTable.Rows.Add(positionRatiosDataTable.NewRow());
                if (l == "TOTAL LONG DAYS:")
                {
                    AddRowWithFirstCellLabel("FFAs only", ref positionRatiosDataTable);
                }
                dr = positionRatiosDataTable.NewRow();
                dr[0] = l;                
                //Add Label Row
                foreach (DateTime date in dates)
                {

                    if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                    {
                        dateString = date.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                    }
                    else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                    {
                        quarterAdded = 0;
                        intQuarter = ((date.Month - 1) / 3) + 1;
                        if (intQuarter != quarterAdded)
                        {
                            quarterAdded = intQuarter;
                            strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                            dateString = strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB"));
                        }
                    }
                    else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                    {
                        calendarAdded = 0;
                        intCalendar = date.Year;
                        if (intCalendar != calendarAdded)
                        {
                            calendarAdded = intCalendar;
                            dateString = "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB"));
                        }
                    }
                    if (l == "(adj) for options:")
                    {
                        dr[dateString + "_R"] = "";
                    }
                    else if (posCalculationsPerIndex[index][l].ContainsKey(dateString))
                    {

                        var valueFromDictionary = posCalculationsPerIndex[index][l][dateString];

                        if (valueFromDictionary == null)
                            dr[dateString + "_R"] = "N/A";
                        else
                        {
                            if (l.Contains("(%)"))
                                dr[dateString + "_R"] = Convert.ToDecimal(posCalculationsPerIndex[index][l][dateString]).ToString("P", CultureInfo.InvariantCulture);
                            else
                                dr[dateString + "_R"] = Math.Round(Convert.ToDecimal(posCalculationsPerIndex[index][l][dateString]), 3);
                        }
                    }
                    else
                    {
                        dr[dateString + "_R"] = "N/A";
                    }


                }
                if (chkCalculateSums.Checked)
                {
                    //Call new approach total calculations
                    try
                    {
                        if (l == "(adj) for options:")
                        {
                            dr[dateString + "_R"] = "";
                        }
                        else
                        {
                            var valueFromDictionary = GenerateTotalsPosition(index, l, dates);
                            if (valueFromDictionary == null)
                                dr["Total" + "_2_R"] = "N/A";
                            else
                            {
                                if (l.Contains("(%)"))
                                    dr["Total" + "_2_R"] = Convert.ToDecimal(valueFromDictionary).ToString("P", CultureInfo.InvariantCulture);
                                else
                                    dr["Total" + "_2_R"] = Math.Round(Convert.ToDecimal(valueFromDictionary), 3);
                            }
                        }
                    }
                    catch {
                    }                   
                }

                positionRatiosDataTable.Rows.Add(dr);
            }
        }

        #endregion

        #endregion

        #region GUI Event Handlers

        #region Position
        private void GridTradePositionResultsViewCustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                if (e.Row is TradeInformation)
                {
                    var tradeInformation = (TradeInformation)e.Row;

                    if (e.Column.FieldName == "Type")
                    {
                        if (tradeInformation.Type == "Option")
                            e.Value = tradeInformation.TradeInfo.OptionInfo.Type.ToString("g");
                    }
                    else if (e.Column.FieldName == "EmbeddedValue")
                    {
                        var embeddedValues = (Dictionary<string, decimal>)e.Column.Tag;
                        e.Value = (embeddedValues == null || embeddedValues.Count == 0 || !embeddedValues.ContainsKey(tradeInformation.Identifier)) ? 0 : embeddedValues[tradeInformation.Identifier];
                    }
                    else if (e.Column.FieldName == "Total")
                    {
                        var tradePositions = (Dictionary<string, Dictionary<DateTime, decimal>>)gridPositionResultsView.Tag;
                        e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value);
                    }
                    else
                    {
                        var tradePositions = (Dictionary<string, Dictionary<DateTime, decimal>>)gridPositionResultsView.Tag;
                        //DateTime date = (DateTime) e.Column.Tag;                        
                        if (tradePositions.Keys.Contains(tradeInformation.Identifier))
                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {

                                DateTime date = (DateTime)e.Column.Tag;
                                e.Value = tradePositions[tradeInformation.Identifier][date];

                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {

                                e.Value = tradePositions[tradeInformation.Identifier].Where(
                                    a =>
                                    {
                                        int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                        string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                        return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName;
                                    }).Select(a => a.Value).Sum();

                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                e.Value = tradePositions[tradeInformation.Identifier].Where(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(a => a.Value).Sum();
                            }
                        //e.Value = tradePositions[tradeInformation.Identifier][date];
                    }
                }
            }
        }

        private void GridTradePositionResultsViewCustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            // Get the summary ID. 
            var summaryType = (ReportResultEnum)((GridSummaryItem)e.Item).Tag;
            var month = ((GridSummaryItem)e.Item).FieldName;
            var view = sender as GridView;
            GridColumnSummaryItemCollection summaries;

            string indexPhysicalFinancial;

            // Initialization 
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {
                foreach (var indx in indexesPhysicalFinancial)
                {
                    totalPosition[indx] = 0;
                    totalLongDays[indx] = 0;
                    totalShortDays[indx] = 0;
                    totalAvgLongPrice[indx] = 0;
                    totalAvgShortPrice[indx] = 0;
                    totalNetPaperDays[indx] = 0;
                    totalNetPhysicalDays[indx] = 0;
                    totalTcInDays[indx] = 0;
                    totalTcOutDays[indx] = 0;
                    totalFfaDays[indx] = 0;
                    totalOptionDays[indx] = 0;
                    totalFfaNotional[indx] = 0;
                    totalPaperNotional[indx] = 0;                    
                    totalTcOutPrice[indx] = 0;

                    totalLongOptionDays[indx] = 0;
                    totalShortOptionDays[indx] = 0;
                    totalAvgLongOptionDays[indx] = 0;
                    totalAvgShortOptionDays[indx] = 0;
                }

                foreach (var indx in indexesPhysicalFinancialTotal)
                {
                    totalPosition[indx] = 0;
                    totalLongDays[indx] = 0;
                    totalShortDays[indx] = 0;
                    totalAvgLongPrice[indx] = 0;
                    totalAvgShortPrice[indx] = 0;
                    totalNetPaperDays[indx] = 0;
                    totalNetPhysicalDays[indx] = 0;
                    totalTcInDays[indx] = 0;
                    totalTcOutDays[indx] = 0;
                    totalFfaDays[indx] = 0;
                    totalOptionDays[indx] = 0;
                    totalFfaNotional[indx] = 0;
                    totalPaperNotional[indx] = 0;                    
                    totalTcOutPrice[indx] = 0;

                    totalLongOptionDays[indx] = 0;
                    totalShortOptionDays[indx] = 0;
                    totalAvgLongOptionDays[indx] = 0;
                    totalAvgShortOptionDays[indx] = 0;
                }

            }
            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                #region Calculation
                decimal calc = 0;
                TradeInformation tradeInfo;
                tradeInfo = view.GetRow(e.RowHandle) as TradeInformation;
                indexPhysicalFinancial = tradeInfo.GroupedForwardCurve + "-" + tradeInfo.PhysicalFinancial;

                if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                    return;
                switch (summaryType)
                {
                    case ReportResultEnum.TotalPosition: // Total position 

                        totalPosition[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        sumTotalPosition[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;

                    case ReportResultEnum.TotalLongDays: // Total long days 

                        if (tradeInfo != null && ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                         //     ||
                         //    (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                         //     tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                         ))
                        {                            
                            totalLongDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalLongDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalLongDays[(tradeInfo.ForwardCurveForReports + "-Total")] += Convert.ToDecimal(e.FieldValue);
                        }
                        break;
                    case ReportResultEnum.TotalShortDays: // Total short days

                        if (tradeInfo != null && (
                            (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                             || (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))))
                        {                            
                            totalShortDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalShortDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalShortDays[(tradeInfo.ForwardCurveForReports + "-Total")] += Convert.ToDecimal(e.FieldValue); 
                        }                            
                        break;
                    case ReportResultEnum.TotalLongDaysOption:

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                             ||
                             //  (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             //    tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                             //  ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                               //tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")
                               Convert.ToDecimal(e.FieldValue) > 0
                              )))
                            totalLongOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.TotalShortDaysOption:

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                             ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                            ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                              //tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")
                              Convert.ToDecimal(e.FieldValue) < 0
                              )))
                            totalShortOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.AvgLongPrice: //AvgLongPrice

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))))
                        {
                            totalAvgLongPrice[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                            sumTotalAvgLongPrice[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                            sumTotalAvgLongPrice[(tradeInfo.ForwardCurveForReports + "-Total")] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                        }
                        break;
                    case ReportResultEnum.AvgLongPriceOption:

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                            // tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")
                            Convert.ToDecimal(e.FieldValue) > 0
                             )))
                        {
                            if (tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g"))
                            {
                                totalAvgLongOptionDays[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(e.FieldValue);
                                sumTotalAvgLongOptionDays[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(e.FieldValue);
                                sumTotalAvgLongOptionDays[(tradeInfo.ForwardCurveForReports+"-Total")] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(e.FieldValue);
                            }
                            else
                            {
                                totalAvgLongOptionDays[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                                sumTotalAvgLongOptionDays[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                                sumTotalAvgLongOptionDays[(tradeInfo.ForwardCurveForReports+"-Total")] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                            }
                        }
                        break;
                    case ReportResultEnum.AvgShortPrice: //AvgShortPrice                        
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))))
                        {
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);
                            }
                            catch
                            {
                            }
                            calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                            totalAvgShortPrice[indexPhysicalFinancial] += calc;
                            sumTotalAvgShortPrice[indexPhysicalFinancial] += calc;
                            sumTotalAvgShortPrice[(tradeInfo.ForwardCurveForReports+"-Total")] += calc;                            
                        }
                        break;
                    case ReportResultEnum.AvgShortPriceOption:

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                             //tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")
                             Convert.ToDecimal(e.FieldValue) < 0
                             )))
                        {
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);
                            }
                            catch
                            {
                            }
                            if (tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g"))
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(valueField);
                                totalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[(tradeInfo.ForwardCurveForReports+"-Total")] += calc;

                            }
                            else
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                                totalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[(tradeInfo.ForwardCurveForReports+"-Total")] += calc;
                            }
                            counter++;
                        }
                        break;
                    case ReportResultEnum.NetPaperDays:

                        if (tradeInfo != null &&
                            (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") ||
                             tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") ||
                             tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")))
                            totalNetPaperDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.NetPhysicalDays:

                        if (tradeInfo != null &&
                            (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") ||
                             tradeInfo.Type == TradeTypeEnum.Cargo.ToString("g")))
                            totalNetPhysicalDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.TcInDays:

                        if (tradeInfo != null &&
                            (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")))
                            totalTcInDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.TcOutDays:

                        if (tradeInfo != null &&
                            (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")))
                            totalTcOutDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.TotalFFAOrOptionDays: // Total FFA Or Option Days
                    case ReportResultEnum.FFAAverageRate:
                        if (tradeInfo != null && (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g")))
                        {
                            totalFfaDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalFfaDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            totalFfaNotional[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                            sumTotalFfaNotional[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                            sumTotalFfaNotional[(tradeInfo.ForwardCurveForReports+"-Total")] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                        }
                        if (tradeInfo != null && (tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")))
                        {
                            totalOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);                            
                            sumTotalOptionDays[(tradeInfo.ForwardCurveForReports+"-Total")] += Convert.ToDecimal(e.FieldValue);
                        }

                        break;
                    case ReportResultEnum.CoverAvgRate:
                        {
                            if (tradeInfo != null &&
                                ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") ||
                                  tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") ||
                                  tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g"))))
                            {
                                calc = 0;
                                if (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g"))
                                {
                                    calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                                    totalPaperNotional[indexPhysicalFinancial] += calc;
                                    sumTotalPaperNotional[indexPhysicalFinancial] += calc;
                                    sumTotalPaperNotional[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;
                                }
                                else
                                {
                                    calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(e.FieldValue);
                                    totalPaperNotional[indexPhysicalFinancial] += calc;
                                    sumTotalPaperNotional[indexPhysicalFinancial] += calc;
                                    sumTotalPaperNotional[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;
                                }
                            }
                            break;
                        }
                    case ReportResultEnum.AdjOption:
                        {
                            if (tradeInfo != null && (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")))
                            {
                                totalTcOutPrice[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                                sumTotalTcOutPrice[indexPhysicalFinancial] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                                sumTotalTcOutPrice[(tradeInfo.ForwardCurveForReports + "-Total")] += (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(e.FieldValue);
                            }
                            break;
                        }
                }
                #endregion;
            }
            // Finalization 
            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                #region Finalize
                string fieldName;
                e.TotalValue = "N/A";
                switch (summaryType)
                {
                    #region TotalPosition
                    case ReportResultEnum.TotalPosition:
                        fieldName = "TOTAL POSITION:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalPosition;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                if (indx.Contains("Physical"))
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = totalPosition[indx];
                                }
                                else
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = (posCalculationsPerIndex[indx]["TC IN DAYS:"][month]??0) + (posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month]??0);
                                }

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + (posCalculationsPerIndex[indx][fieldName][month]?? 0);
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal? totalPositionForTotal = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        //totalPositionForTotal = totalPositionForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;

                                        //if (indx.Contains("Physical"))
                                        //{
                                        //    totalPositionForTotal += totalPosition[indx];
                                        //}
                                        //else
                                        //{
                                            totalPositionForTotal = (posCalculationsPerIndex[indx]["TC IN DAYS:"][month]??0) + (posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month]??0);
                                       // }
                                    }
                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalPositionForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + totalPositionForTotal;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TotalLongDays
                    case ReportResultEnum.TotalLongDays:
                        fieldName = "TOTAL LONG DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalLongDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                posCalculationsPerIndex[indx][fieldName][month] = totalLongDays[indx];

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + totalLongDays[indx];
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalLongDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalLongDaysForTotal = totalLongDaysForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;
                                    }

                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalLongDaysForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + totalLongDaysForTotal;

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TotalShortDays
                    case ReportResultEnum.TotalShortDays:
                        fieldName = "TOTAL SHORT DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalShortDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                posCalculationsPerIndex[indx][fieldName][month] = totalShortDays[indx];

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + totalShortDays[indx];

                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalShortDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalShortDaysForTotal = totalShortDaysForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;
                                    }

                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalShortDaysForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + totalShortDaysForTotal;

                            }
                            e.TotalValue = "N/A";
                        }
                        break;

                    #endregion

                    #region TotalLongDaysOption
                    case ReportResultEnum.TotalLongDaysOption:
                        fieldName = "(incl. OPTIONS) TOTAL LONG DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalLongDaysOption;
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    posCalculationsPerIndex[indx][fieldName][month] = totalLongOptionDays[indx];

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + totalLongOptionDays[indx];
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalLongOptionDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalLongOptionDaysForTotal = totalLongOptionDaysForTotal + totalLongOptionDays[indx];
                                    }

                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalLongOptionDaysForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + totalLongOptionDaysForTotal;

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TotalShortDaysOption
                    case ReportResultEnum.TotalShortDaysOption:
                        fieldName = "(incl. OPTIONS) TOTAL SHORT DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalShortDaysOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    posCalculationsPerIndex[indx][fieldName][month] = totalShortOptionDays[indx];

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + totalShortOptionDays[indx];
                                }

                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalShortOptionDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalShortOptionDaysForTotal = totalShortOptionDaysForTotal + totalShortOptionDays[indx];
                                    }

                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalShortOptionDaysForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + totalShortOptionDaysForTotal;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region NetDays
                    case ReportResultEnum.NetDays:
                        fieldName = "NET DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.NetDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    //Calculation
                                    var sumShortDays = posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month];
                                    var sumLongDays = posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month];

                                    posCalculationsPerIndex[indx][fieldName][month] = sumLongDays + sumShortDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + sumLongDays + sumShortDays;
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                //Calculation
                                var sumShortDays = posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month];

                                posCalculationsPerIndex[totalIndx][fieldName][month] = sumLongDays + sumShortDays;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + sumLongDays + sumShortDays;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region NetDaysOption
                    case ReportResultEnum.NetDaysOption:
                        fieldName = "(incl. OPTIONS) NET DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.NetDaysOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    //Calculation
                                    var sumShortDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                    var sumLongDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                    posCalculationsPerIndex[indx][fieldName][month] = sumLongDays + sumShortDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + sumLongDays + sumShortDays;
                                }

                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                //Calculation
                                var sumShortDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                posCalculationsPerIndex[totalIndx][fieldName][month] = sumLongDays + sumShortDays;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + sumLongDays + sumShortDays;

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region Cover - SHORT/LONG COVER (%)
                    case ReportResultEnum.Cover:
                        fieldName = "SHORT/LONG COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());
                                
                                var sumShortDays = posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month];

                                if (sumLongDays != 0)
                                {                                    
                                    posCalculationsPerIndex[indx][fieldName][month] = Math.Abs((decimal)(sumShortDays / sumLongDays));

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());
                                
                                var sumShortDays = posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month];

                                if (sumLongDays != 0)
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)(sumShortDays / sumLongDays));

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region CoverOption - (incl. OPTIONS) SHORT/LONG COVER (%)
                    case ReportResultEnum.CoverOption:
                        fieldName = "(incl. OPTIONS) SHORT/LONG COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.CoverOption;

                            foreach (var indx in indexesPhysicalFinancial) {

                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    //Calculation
                                    var sumShortDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                    var sumLongDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                    if (sumLongDays != 0)
                                    {                                        
                                        posCalculationsPerIndex[indx][fieldName][month] = Math.Abs((decimal)(sumShortDays / sumLongDays));

                                        if (chkCalculateSums.Checked)
                                            posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                    }
                                    else
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                    }
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var sumShortDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                if (sumLongDays != 0)
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)(sumShortDays / sumLongDays));

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgLongPrice - AVG LONG DEAL PRICE
                    case ReportResultEnum.AvgLongPrice:
                        fieldName = "AVG LONG DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgLongPrice;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                var sumLongDays = posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month];

                                if (sumLongDays != 0)
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = totalAvgLongPrice[indx] / sumLongDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = null;
                                }

                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var sumLongDays = posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month];
                                decimal totalAvgLongPriceForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalAvgLongPriceForTotal = totalAvgLongPriceForTotal + totalAvgLongPrice[indx];
                                    }
                                }
                                if (sumLongDays != 0)
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgLongPriceForTotal / sumLongDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgLongPriceOption - (incl. OPTIONS) AVG LONG DEAL PRICE
                    case ReportResultEnum.AvgLongPriceOption:
                        fieldName = "(incl. OPTIONS) AVG LONG DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgLongPriceOption;
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    var sumLongDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                    if (sumLongDays != 0)
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = totalAvgLongOptionDays[indx] / sumLongDays;
                                    }
                                    else
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                    }
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var sumLongDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];
                                decimal totalAvgLongOptionDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalAvgLongOptionDaysForTotal = totalAvgLongOptionDaysForTotal + totalAvgLongOptionDays[indx];
                                    }

                                }

                                if (sumLongDays != 0)
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgLongOptionDaysForTotal / sumLongDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgShortPrice - AVG SHORT DEAL PRICE
                    case ReportResultEnum.AvgShortPrice:
                        fieldName = "AVG SHORT DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgShortPrice;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                var sumShortDays = posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month];

                                if (sumShortDays != 0)
                                {                                    
                                    posCalculationsPerIndex[indx][fieldName][month] = totalAvgShortPrice[indx] / sumShortDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var sumShortDays = posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month];
                                decimal totalAvgShortPriceForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalAvgShortPriceForTotal = totalAvgShortPriceForTotal + totalAvgShortPrice[indx];
                                    }
                                }
                                if (sumShortDays != 0)
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)(totalAvgShortPriceForTotal / (sumShortDays)));

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgShortPriceOption - (incl. OPTIONS) AVG SHORT DEAL PRICE
                    case ReportResultEnum.AvgShortPriceOption:
                        fieldName = "(incl. OPTIONS) AVG SHORT DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgShortPriceOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    var sumShortDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];

                                    if (sumShortDays != 0)
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = totalAvgShortOptionDays[indx] / sumShortDays;

                                        if (chkCalculateSums.Checked)
                                            posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                    }
                                    else
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                    }
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var sumShortDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                decimal totalAvgShortgOptionDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalAvgShortgOptionDaysForTotal = totalAvgShortgOptionDaysForTotal + totalAvgShortOptionDays[indx];
                                    }
                                }
                                if (sumShortDays != 0)
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgShortgOptionDaysForTotal / sumShortDays;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                {
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region NetPaperDays
                    case ReportResultEnum.NetPaperDays:
                        fieldName = "NET PAPER DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.NetPaperDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                posCalculationsPerIndex[indx][fieldName][month] = totalNetPaperDays[indx];                               
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal netPaperDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        netPaperDaysForTotal = netPaperDaysForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;
                                    }
                                }
                                posCalculationsPerIndex[totalIndx][fieldName][month] = netPaperDaysForTotal;                               
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region NetPhysicalDays
                    case ReportResultEnum.NetPhysicalDays:
                        fieldName = "NET PHYSICAL DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.NetPhysicalDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                if (indx.Contains("Financial"))
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = totalTcInDays[indx];

                                    string[] indexParts = indx.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                    string sameIndexOfPhysical = indexParts[0] + "-Physical";

                                    if (posCalculationsPerIndex.ContainsKey(sameIndexOfPhysical))
                                    {
                                        if (posCalculationsPerIndex[sameIndexOfPhysical]["TC IN DAYS:"].ContainsKey(month))
                                        {
                                            posCalculationsPerIndex[indx][fieldName][month] = totalTcInDays[indx] + posCalculationsPerIndex[sameIndexOfPhysical]["TC IN DAYS:"][month];
                                        }
                                    }
                                }
                                else
                                {
                                    // var tcInDays = totalTcInDays[indx];
                                    // var tcOutDays = totalTcOutDays[indx];
                                    // posCalculationsPerIndex[indx][fieldName ][month] = tcInDays+ tcOutDays;
                                    posCalculationsPerIndex[indx][fieldName][month] = totalNetPhysicalDays[indx];
                                }
                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalNetPhysicalDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")) && indx.Contains("Physical"))
                                    {
                                        totalNetPhysicalDaysForTotal = totalNetPhysicalDaysForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;
                                    }

                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalNetPhysicalDaysForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region PPCover - PAPER/PHYSICAL COVER (%):
                    case ReportResultEnum.PPCover:
                        fieldName = "PAPER/PHYSICAL COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.PPCover;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].Any(a => a.Key == fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    var netPaperDays = posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month];
                                    var netPhysicalDays = posCalculationsPerIndex[indx]["NET PHYSICAL DAYS:"][month];

                                    if (netPhysicalDays != 0)
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = (((-1) * netPaperDays) / netPhysicalDays) * 100;

                                        if (chkCalculateSums.Checked)
                                            posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                    }
                                    else
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region Total Cover - COVER (%):
                    case ReportResultEnum.TotalCover:
                        fieldName = "COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalCover;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].Any(a => a.Key == fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    var netPaperDays = posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month];
                                    var tcInDays = posCalculationsPerIndex[indx]["TC IN DAYS:"][month];
                                    var tcOutDays = posCalculationsPerIndex[indx]["TC OUT DAYS:"][month];

                                    if (tcInDays != 0)
                                        posCalculationsPerIndex[indx][fieldName][month] = (-1) * ((tcOutDays + netPaperDays) / tcInDays);
                                    else
                                        posCalculationsPerIndex[indx][fieldName][month] = null;

                                    if (chkCalculateSums.Checked)
                                        posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + (posCalculationsPerIndex[indx][fieldName][month] ?? 0);
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var netPaperDays = posCalculationsPerIndex[totalIndx]["NET PAPER DAYS:"][month];
                                var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month];
                                var tcInDays = posCalculationsPerIndex[totalIndx]["TC IN DAYS:"][month];

                                if (tcInDays != 0)
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = (-1) * ((tcOutDays + netPaperDays) / tcInDays);
                                else
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + (posCalculationsPerIndex[totalIndx][fieldName][month] ?? 0);
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TcInDays - TC IN DAYS
                    case ReportResultEnum.TcInDays:
                        fieldName = "TC IN DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TcInDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                if (indx.Contains("Financial"))
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = totalTcInDays[indx];

                                    string[] indexParts = indx.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                    string sameIndexOfPhysical = indexParts[0] + "-Physical";

                                    if (posCalculationsPerIndex.ContainsKey(sameIndexOfPhysical))
                                    {
                                        if (posCalculationsPerIndex[sameIndexOfPhysical][fieldName].ContainsKey(month))
                                        {
                                            posCalculationsPerIndex[indx][fieldName][month] = totalTcInDays[indx] + totalTcInDays[sameIndexOfPhysical];
                                        }
                                    }
                                }
                                else
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = totalTcInDays[indx];
                                }                                
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalTcInDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")) && indx.Contains("Physical"))
                                    {
                                        totalTcInDaysForTotal = totalTcInDaysForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;
                                    }
                                }

                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalTcInDaysForTotal;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TcOutDays - TC OUT DAYS
                    case ReportResultEnum.TcOutDays:
                        fieldName = "TC OUT DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TcOutDays;
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                posCalculationsPerIndex[indx][fieldName][month] = totalTcOutDays[indx];

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                decimal totalTcOutDaysForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalTcOutDaysForTotal = totalTcOutDaysForTotal + posCalculationsPerIndex[indx][fieldName][month] ?? 0;
                                    }
                                }
                                posCalculationsPerIndex[totalIndx][fieldName][month] = totalTcOutDaysForTotal;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region VesselCover - VESSEL TC COVER (%)
                    case ReportResultEnum.VesselCover:
                        fieldName = "VESSEL TC COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.VesselCover;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    var tcInDays = posCalculationsPerIndex[indx]["TC IN DAYS:"][month];
                                    var tcOutDays = posCalculationsPerIndex[indx]["TC OUT DAYS:"][month];

                                    if (tcInDays != 0)
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = Math.Abs(((decimal)tcOutDays / (decimal)tcInDays) * 100);

                                        if (chkCalculateSums.Checked)
                                            posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                    }
                                    else
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var tcInDays = posCalculationsPerIndex[totalIndx]["TC IN DAYS:"][month];
                                var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month];

                                if (tcInDays != 0)
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs(((decimal)tcOutDays / (decimal)tcInDays) * 100);
                                else
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TotalFFADays - TOTAL FFA/OPTION DAYS
                    case ReportResultEnum.TotalFFAOrOptionDays:
                        fieldName = "TOTAL FFA/OPTION DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalFFAOrOptionDays;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    var totalFFADaysValue = totalFfaDays[indx];
                                    var totalOptionDaysValue = totalOptionDays[indx];
                                    if (totalOptionDaysValue != 0)
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = totalFFADaysValue / totalOptionDaysValue;

                                        if (chkCalculateSums.Checked)
                                            posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                    }
                                    else
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                    }
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region CoverAvgRate - COVER AVG RATE
                    case ReportResultEnum.CoverAvgRate:
                        fieldName = "COVER AVERAGE RATE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.CoverAvgRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                var sumFfaAndOptionDays = posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month] ?? 0;

                                if (sumFfaAndOptionDays != 0)
                                {
                                    posCalculationsPerIndex[indx][fieldName][month] = totalPaperNotional[indx] / sumFfaAndOptionDays;                                    
                                }
                                else
                                    posCalculationsPerIndex[indx][fieldName][month] = null;
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                                if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                                    posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                                var netPaperDays = posCalculationsPerIndex[totalIndx]["NET PAPER DAYS:"][month] ?? 0;
                                var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month] ?? 0;
                                decimal totalFfaAndOptonNotionalForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        //totalFfaNotionalForTotal = totalFfaNotionalForTotal + totalFfaNotional[indx];
                                        totalFfaAndOptonNotionalForTotal = totalFfaAndOptonNotionalForTotal + totalPaperNotional[indx] + totalTcOutPrice[indx];
                                    }
                                }
                                if (netPaperDays + tcOutDays != 0)
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)totalFfaAndOptonNotionalForTotal / (netPaperDays + tcOutDays));
                                else
                                    posCalculationsPerIndex[totalIndx][fieldName][month] = null;

                                if (chkCalculateSums.Checked)
                                    posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region FFAAvgRate - FFA AVG RATE
                    case ReportResultEnum.FFAAverageRate:
                        fieldName = "FFA AVERAGE RATE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.FFAAverageRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    //var sumFfaDays = posCalculationsPerIndex[indx]["TOTAL FFA/OPTION DAYS:"][month];
                                    var sumFfaDays = totalFfaDays[indx];

                                    if (sumFfaDays != 0)
                                    {
                                        posCalculationsPerIndex[indx][fieldName][month] = totalFfaNotional[indx] / sumFfaDays;

                                        if (chkCalculateSums.Checked)
                                            posCalculationsPerIndex[indx][fieldName]["Total"] = (posCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[indx][fieldName][month];
                                    }
                                    else
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AdjOption - (adj) for options:
                    case ReportResultEnum.AdjOption:
                        fieldName = "(adj) for options:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AdjOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (posCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    if (!posCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                        posCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());
                                    if (chkCalculateSums.Checked && !posCalculationsPerIndex[indx][fieldName].ContainsKey("Total"))
                                        posCalculationsPerIndex[indx][fieldName].Add("Total", new decimal?());

                                    //var paperDays = Convert.ToDecimal(posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month]);

                                    //if (paperDays != 0)
                                    //{
                                    //    posCalculationsPerIndex[indx][fieldName][month] = totalPaperNotional[indx] / paperDays;

                                    //}
                                    //else
                                        posCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            //foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            //{
                            //    if (!posCalculationsPerIndex[totalIndx][fieldName].ContainsKey(month))
                            //        posCalculationsPerIndex[totalIndx][fieldName].Add(month, new decimal?());
                            //    if (chkCalculateSums.Checked && !posCalculationsPerIndex[totalIndx][fieldName].ContainsKey("Total"))
                            //        posCalculationsPerIndex[totalIndx][fieldName].Add("Total", new decimal?());

                            //    var paperDays = Convert.ToDecimal(posCalculationsPerIndex[totalIndx]["NET PAPER DAYS:"][month]);

                            //    decimal totalPaperNotionalForTotal = 0;
                            //    foreach (var indx in indexesPhysicalFinancial)
                            //    {
                            //        if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                            //        {
                            //            totalPaperNotionalForTotal = totalPaperNotionalForTotal + totalPaperNotional[indx];
                            //        }
                            //    }
                            //    if (paperDays != 0)
                            //        posCalculationsPerIndex[totalIndx][fieldName][month] = totalPaperNotionalForTotal / paperDays;
                            //    else
                            //        posCalculationsPerIndex[totalIndx][fieldName][month] = null;

                            //    if (chkCalculateSums.Checked)
                            //        posCalculationsPerIndex[totalIndx][fieldName]["Total"] = (posCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + posCalculationsPerIndex[totalIndx][fieldName][month];
                            //}
                            e.TotalValue = "N/A";
                        }
                        break;
                        #endregion
                }
                #endregion
            }
        }

        private void GridPositionResultsViewCustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            if (e.Info.SummaryItem.Tag != null &&
                ((ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.NetPhysicalDays
                   || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.NetPaperDays
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.PPCover
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TcInDays
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TcOutDays
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.VesselCover
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalLongDays
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalShortDays
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.NetDays
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.Cover
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalLongDaysOption
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalShortDaysOption
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.NetDaysOption
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.CoverOption
               || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalPosition
               ))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion 

        #region MTM

        private void GridTradeMtmResultsViewCustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                if (e.Row is TradeInformation)
                {
                    var tradeInformation = (TradeInformation)e.Row;

                    bool isCommissionCalculation = chkCalculateCommissions.Checked;

                    decimal commissionPerMonth = 0;
                    decimal totalCommission = 0;

                    var numOfTradeMotnhsInSelectedPeriod = 0;
                    var firstDay = _periodFrom;
                    var lastDay = _periodTo;

                    DateTime tradeDateInSelectedPeriod = tradeInformation.PeriodFrom.Date;
                    while (tradeDateInSelectedPeriod < firstDay)
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddDays(1);

                    while (tradeDateInSelectedPeriod >= firstDay &&
                           tradeDateInSelectedPeriod <= tradeInformation.PeriodTo.Date)
                    {
                        numOfTradeMotnhsInSelectedPeriod++;
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddMonths(1);
                    }

                    if (tradeInformation.TradeInfo.TradeBrokerInfos.Count > 0 && isCommissionCalculation)
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.Option)
                        {
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                  tradeInformation.TradeInfo.OptionInfo.Premium *
                                                  tradeInformation.TradeInfo.OptionInfo.QuantityDays);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                        else if (tradeInformation.Trade.Type == TradeTypeEnum.FFA)
                        {
                            //JIRA:FREIG-11
                            //commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                            //                      tradeInformation.TradeInfo.FFAInfo.Price *
                            //                      tradeInformation.TradeInfo.FFAInfo.QuantityDays);
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                  tradeInformation.TradeInfo.FFAInfo.Price *
                                                  tradeInformation.TradeInfo.FFAInfo.TotalDaysOfTrade);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                    }

                    if (e.Column.FieldName == "Type")
                    {
                        if (tradeInformation.Type == "Option")
                            e.Value = tradeInformation.TradeInfo.OptionInfo.Type.ToString("g");
                    }
                    else if (e.Column.FieldName == "Total")
                    {
                        var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                        if (((string)rdgMTMResultsType.EditValue).ToUpper() == "CASH FLOW")
                        {
                            e.Value = tradeMTMs[CASH_FLOW][tradeInformation.Identifier].Sum(a => a.Value);
                        }
                        else if (((string)rdgMTMResultsType.EditValue).ToUpper() == "COMBINED")
                        {
                            e.Value = tradeMTMs[COMBINED][tradeInformation.Identifier].Sum(a => a.Value);
                        }
                        else
                        {
                            e.Value = tradeMTMs[PL][tradeInformation.Identifier].Sum(a => a.Value);
                        }

                        //if (isCommissionCalculation)
                        //    e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value) - totalCommission;
                        //else
                        //    e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value);

                    }
                    else if (e.Column.FieldName == "Commission")
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.TC || tradeInformation.Trade.Type == TradeTypeEnum.Cargo
                            || tradeInformation.TradeInfo.TradeBrokerInfos == null || tradeInformation.TradeInfo.TradeBrokerInfos.Count == 0)
                            return;

                        if (tradeInformation.TradeInfo.TradeBrokerInfos.Count > 1)
                        {
                            e.Value = "More than 1 brokers";
                            return;
                        }
                        e.Value = totalCommission;
                    }
                    else
                    {
                        var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                        var mtmResultTypeKey = mtmResultTypes.Where(a => a.Key == ((string)rdgMTMResultsType.EditValue).ToUpper()).Select(q => q.Key).SingleOrDefault();
                        var mtmResultTypeKValue = mtmResultTypes.Where(a => a.Key == ((string)rdgMTMResultsType.EditValue).ToUpper()).Select(q => q.Value).SingleOrDefault();
                        if (tradeMTMs[mtmResultTypeKey].Keys.Contains(tradeInformation.Identifier))
                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {                                
                                e.Value = tradeMTMs[mtmResultTypeKValue][tradeInformation.Identifier][(DateTime)e.Column.Tag];
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                e.Value = tradeMTMs[mtmResultTypeKValue][tradeInformation.Identifier].Where(
                                    a =>
                                    {
                                        int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                        string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                        return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName;
                                    }).Select(a => a.Value).Sum();
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                e.Value = tradeMTMs[mtmResultTypeKValue][tradeInformation.Identifier].Where(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(a => a.Value).Sum();
                            }
                    }
                }
            }
        }
        int counter=0;
        private void GridTradeMtmResultsViewCustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            ReportResultEnum summaryType;
            if ((e.Item as GridSummaryItem).Tag.GetType() == typeof(ReportResultEnum))
                summaryType = (ReportResultEnum)(e.Item as GridSummaryItem).Tag;
            else
                summaryType = ReportResultEnum.IndexAverage;

            var view = sender as GridView;
            GridColumnSummaryItemCollection summaries;
            string indexPhysicalFinancial;

            var month = ((GridSummaryItem)e.Item).FieldName;

            // Initialization 
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {
                foreach (var indx in indexesPhysicalFinancial)
                {
                    totalMTM[indx] = 0;
                    totalCommission[indx] = 0;
                    totalMTMLongPrice[indx] = 0;
                    totalMTMLongPricePositionDays[indx] = 0;
                    totalMTMShortPrice[indx] = 0;
                    totalMTMShortPricePositionDays[indx] = 0;

                    totalMTMLongOptionDays[indx] = 0;
                    totalMTMShortOptionDays[indx] = 0;

                    //totalTcInDaysVesselIndex[indx] = 0;
                    //totalTcOutDaysVesselIndex[indx] = 0;

                    totalTcInCashFlow[indx] = 0;
                    totalTcOutCashFlow[indx] = 0;

                    totalTcInCalendarDays[indx] = 0;

                    totalMTMIndexEqOwnersRateFin[indx] = 0;
                    totalMTMIndexEqOwnersRateFinOption[indx] = 0;

                    ///////totalPosition[indx] = 0;
                    totalLongDays[indx] = 0;
                    totalShortDays[indx] = 0;
                    totalAvgLongPrice[indx] = 0;
                    totalAvgShortPrice[indx] = 0;
                    totalNetPaperDays[indx] = 0;
                    totalNetPhysicalDays[indx] = 0;

                    totalOptionDays[indx] = 0;

                    totalLongOptionDays[indx] = 0;
                    totalShortOptionDays[indx] = 0;
                    totalAvgLongOptionDays[indx] = 0;
                    totalAvgShortOptionDays[indx] = 0;

                    //totalAvgLongMTMPrice[indx] = 0;
                    //totalAvgShortMTMPrice[indx] = 0;
                    totalFfaNotional[indx] = 0;
                    totalPaperNotional[indx] = 0;
                    totalPaperNotionalFFA[indx] = 0;
                    totalPaperOptional[indx] = 0;
                    //totalNetPaperDaysOptions[indx] = 0;
                }

                foreach (var indx in indexesMTMPhysicalFinancialTotal)
                {
                    totalPosition[indx] = 0;
                    totalTcInDays[indx] = 0;
                    totalTcOutDays[indx] = 0;

                    totalLongDays[indx] = 0;
                    totalShortDays[indx] = 0;
                    totalAvgLongPrice[indx] = 0;
                    totalAvgShortPrice[indx] = 0;
                    totalNetPaperDays[indx] = 0;
                    totalNetPhysicalDays[indx] = 0;

                    totalFfaDays[indx] = 0;
                    totalOptionDays[indx] = 0;
                    totalFfaNotional[indx] = 0;
                    totalPaperNotional[indx] = 0;
                    totalPaperNotionalFFA[indx] = 0;
                    totalPaperOptional[indx] = 0;
                    totalTcOutPrice[indx] = 0;

                    totalLongOptionDays[indx] = 0;
                    totalShortOptionDays[indx] = 0;
                    totalAvgLongOptionDays[indx] = 0;
                    totalAvgShortOptionDays[indx] = 0;

                    totalNetPaperDaysOptions[indx] = 0;

                    totalMTMIndexEqOwnersRateFin[indx] = 0;
                    totalMTMIndexEqOwnersRateFinOption[indx] = 0;
                }
            }
            // Calculation 
            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                #region Calculation
                decimal calc = 0;
                TradeInformation tradeInfo;
                tradeInfo = view.GetRow(e.RowHandle) as TradeInformation;
                indexPhysicalFinancial = tradeInfo.GroupedForwardCurve + "-" + tradeInfo.PhysicalFinancial;

                if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                    return;
                switch (summaryType)
                {
                    //case ReportResultEnum.TotalPosition: // Total position 

                    //    totalPosition[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                    //    break;
                    case ReportResultEnum.TotalMTM:
                        if (tradeInfo != null)
                        {
                            // totalMTM[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                            if (tradeMTMs.Keys.Contains(tradeInfo.Identifier))
                            {
                                decimal val = tradeMTMs[COMBINED][tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];
                                totalMTM[indexPhysicalFinancial] += val;
                                sumTotalMTM[indexPhysicalFinancial] += val;
                            }
                        }
                        break;
                    //case ReportResultEnum.TcInDays:

                    //    if (tradeInfo != null && (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")))
                    //    {
                    //        totalTcInDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                    //        sumTotalTcInDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                    //        sumTotalTcInDays[tradeInfo.ForwardCurve+"-Total"] += Convert.ToDecimal(e.FieldValue);
                    //    }
                    //    break;
                    //case ReportResultEnum.TcOutDays:

                    //    if (tradeInfo != null && (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")))
                    //    {
                    //        totalTcOutDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                    //        sumTotalTcOutDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                    //        sumTotalTcInDays[tradeInfo.ForwardCurve+"-Total"] += Convert.ToDecimal(e.FieldValue);
                    //    }
                    //    break;
                    case ReportResultEnum.Commission:
                        //TODO
                        if (tradeInfo != null)
                            totalCommission[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        break;
                    case ReportResultEnum.IndexEqCharterersRate:
                        if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                        {
                            var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                            if (tradeMTMs.Keys.Contains(tradeInfo.Identifier))
                            {
                                decimal val = tradeMTMs[CASH_FLOW][tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];
                                totalTcOutCashFlow[indexPhysicalFinancial] += val;
                                sumTotalTcOutCashFlow[indexPhysicalFinancial] += val;
                            }
                        }
                        break;
                    case ReportResultEnum.IndexEqOwnersRate:
                        var daysOpt = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));
                        if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                        {
                            var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                            if (tradeMTMs[COMBINED].Keys.Contains(tradeInfo.Identifier))
                            {                                
                                decimal val = 0;
                                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                                {
                                    val = tradeMTMs[COMBINED][tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];

                                }
                                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                                {
                                    val = tradeMTMs[COMBINED][tradeInfo.Identifier].Where(
                                            a =>
                                            {
                                                int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                                string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                                return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == ((GridSummaryItem)e.Item).FieldName;
                                            }).Select(a => a.Value).Sum();
                                }
                                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                                {
                                    val = tradeMTMs[COMBINED][tradeInfo.Identifier].Where(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == ((GridSummaryItem)e.Item).FieldName).Select(a => a.Value).Sum();
                                }

                                totalTcInCashFlow[indexPhysicalFinancial] += val;
                                sumTotalTcInCashFlow[indexPhysicalFinancial] += val;
                                totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += val;
                                sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += val;
                            }

                            //totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] +=  Convert.ToDecimal(e.FieldValue);
                            //sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] +=  Convert.ToDecimal(e.FieldValue);
                        }

                        if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.FFA.ToString("g"))
                        {
                            totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                            sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                        }

                        if (tradeInfo != null && tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g"))
                        {                            
                            totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                            sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                        }
                        if (tradeInfo != null && tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g"))
                        {
                            totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                            sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                        }
                        break;
                    case ReportResultEnum.AvgLongMtmPrice:
                        var positionDays = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                ))
                        {
                            totalMTMLongPrice[indexPhysicalFinancial] += positionDays * Convert.ToDecimal(e.FieldValue);
                            totalMTMLongPricePositionDays[indexPhysicalFinancial] += positionDays;
                            sumTotalMTMLongPrice[indexPhysicalFinancial] += positionDays * Convert.ToDecimal(e.FieldValue);
                            sumTotalMTMLongPricePositionDays[indexPhysicalFinancial] += positionDays;
                        }
                        break;
                    case ReportResultEnum.AvgLongMtmPriceOption:
                        var posiDays = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") ||
                               tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                ))
                        {
                            totalMTMLongOptionDays[indexPhysicalFinancial] += posiDays * Convert.ToDecimal(e.FieldValue);
                            //totalMTMLongOptionPositionDays[indexPhysicalFinancial] += posiDays;
                            sumTotalMTMLongOptionDays[indexPhysicalFinancial] += posiDays * Convert.ToDecimal(e.FieldValue);

                            totalMTMLongPricePositionDays[indexPhysicalFinancial] += posiDays;
                            sumTotalMTMLongPricePositionDays[indexPhysicalFinancial] += posiDays;
                        }
                        break;
                    case ReportResultEnum.AvgShortMtmPrice:
                        var posDays = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))))
                        {
                            totalMTMShortPrice[indexPhysicalFinancial] += posDays * Convert.ToDecimal(e.FieldValue);
                            totalMTMShortPricePositionDays[indexPhysicalFinancial] += posDays;
                            sumTotalMTMShortPrice[indexPhysicalFinancial] += posDays * Convert.ToDecimal(e.FieldValue);
                            sumTotalMTMShortPricePositionDays[indexPhysicalFinancial] += posDays;
                        }
                        break;
                    case ReportResultEnum.AvgShortMtmPriceOption:
                        var positDays = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") ||
                               tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))))
                        {
                            totalMTMShortOptionDays[indexPhysicalFinancial] += positDays * Convert.ToDecimal(e.FieldValue);
                            //totalMTMShortOptionPositionDays[indexPhysicalFinancial] != positDays;
                            sumTotalMTMShortOptionDays[indexPhysicalFinancial] += positDays * Convert.ToDecimal(e.FieldValue);
                        }
                        break;
                    case ReportResultEnum.EffCharterersEarningsRate:

                        if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.TC.ToString("g")
                            && tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                        {
                            var pDays = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));
                            var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                            decimal val = tradeMTMs[CASH_FLOW][tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];
                            totalTcOutCashFlow[indexPhysicalFinancial] += val;
                            sumTotalTcOutCashFlow[indexPhysicalFinancial] += val;

                            if (tradeInfo.TradeInfo.TCInfo.VesselIndex != 0)
                            {
                                totalTcOutDaysVesselIndex[indexPhysicalFinancial] += pDays / tradeInfo.TradeInfo.TCInfo.VesselIndex;
                                sumTotalTcOutDaysVesselIndex[indexPhysicalFinancial] += pDays / tradeInfo.TradeInfo.TCInfo.VesselIndex;
                            }
                        }
                        break;
                    case ReportResultEnum.EffOwnersEarningsRate:
                    {
                        var daysOp = Convert.ToDecimal(gridPositionResultsView.GetRowCellValue(e.RowHandle, ((GridSummaryItem)e.Item).FieldName));
                        if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g") ||
                                (tradeInfo != null && tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g")) ||
                                (tradeInfo != null && tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) ||
                                (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.FFA.ToString("g"))
                                )
                        {
                            var tradeMTMs = (Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>)gridMTMResultsView.Tag;
                            if (tradeMTMs[COMBINED].Keys.Contains(tradeInfo.Identifier))
                            {
                                decimal val = 0;
                                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                                {
                                    val = tradeMTMs[COMBINED][tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];

                                }
                                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                                {
                                    val = tradeMTMs[COMBINED][tradeInfo.Identifier].Where(
                                            a =>
                                            {
                                                int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                                string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                                return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == ((GridSummaryItem)e.Item).FieldName;
                                            }).Select(a => a.Value).Sum();
                                }
                                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                                {
                                    val = tradeMTMs[COMBINED][tradeInfo.Identifier].Where(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == ((GridSummaryItem)e.Item).FieldName).Select(a => a.Value).Sum();
                                }

                                totalTcInCashFlow[indexPhysicalFinancial] += val;
                                    //sumTotalTcInCashFlow[indexPhysicalFinancial] += val;
                                sumTotalTcInCashFlow[indexPhysicalFinancial] += val;
                                totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += val;///Convert.ToDecimal(e.FieldValue);
                                sumTotalMTMEffOwnersEarningsRateFinOption[indexPhysicalFinancial] += val;// Convert.ToDecimal(e.FieldValue);
                            }
                            if (indexPhysicalFinancial.Contains("Physical"))
                                sumTotalTcInCalendarDays[indexPhysicalFinancial] += GetTradeData_BaseAggregation(_tradeCalendarDaysResults, tradeInfo, ((GridSummaryItem)e.Item).FieldName);
                                //totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                                //sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.TC.ToString("g") && tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                            {
                                totalTcInCalendarDays[indexPhysicalFinancial] = GetTradeData_BaseAggregation(_tradeCalendarDaysResults, tradeInfo, ((GridSummaryItem)e.Item).FieldName);//_tradeCalendarDaysResults[tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];                                
                                //sumTotalTcInCalendarDays[indexPhysicalFinancial] += GetTradeData_BaseAggregation(_tradeCalendarDaysResults, tradeInfo, ((GridSummaryItem)e.Item).FieldName);                                                                                                                                                         //sumTotalTcInCalendarDays[indexPhysicalFinancial] += _tradeCalendarDaysResults[tradeInfo.Identifier][Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName)];
                            }
                        }

                        //if (tradeInfo != null && tradeInfo.Type == TradeTypeEnum.FFA.ToString("g"))
                        //{
                        //    totalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                        //    //sumTotalMTMIndexEqOwnersRateFinOption[indexPhysicalFinancial] += (Convert.ToDecimal(e.FieldValue));
                        //}
                        break;
                    }
                    case ReportResultEnum.TotalLongDaysOption:
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                             ||
                             //  (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             //    tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                             //  ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                               //tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")
                               Convert.ToDecimal(e.FieldValue) > 0
                              )))
                        {
                            totalLongOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalLongOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        }
                        break;
                    case ReportResultEnum.TotalShortDaysOption:
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                             ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                              tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))
                            ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                              //tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")
                              Convert.ToDecimal(e.FieldValue) < 0
                              )))
                        {
                            totalShortOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                            sumTotalShortOptionDays[indexPhysicalFinancial] += Convert.ToDecimal(e.FieldValue);
                        }
                        break;
                    case ReportResultEnum.AvgLongPrice: //AvgLongPrice

                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))))
                        {
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo,month);
                            }
                            catch {
                            }
                            calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                            totalAvgLongPrice[indexPhysicalFinancial] += calc;
                            sumTotalAvgLongPrice[indexPhysicalFinancial] += (calc < 0) ? 0 : calc; 
                            sumTotalAvgLongPrice[(tradeInfo.ForwardCurveForReports+"-Total")] += (calc < 0) ? 0 : calc;
                        }
                        break;
                    case ReportResultEnum.AvgLongPriceOption:
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")) ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                            // tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")
                            Convert.ToDecimal(e.FieldValue) > 0
                             )))
                        {
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo,month);
                            }
                            catch
                            {
                            }
                            if (tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g"))
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(valueField);
                            }
                            else
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                            }
                            totalAvgLongOptionDays[indexPhysicalFinancial] += calc;                            
                            sumTotalAvgLongOptionDays[indexPhysicalFinancial] += (calc < 0)?0:calc;
                            sumTotalAvgLongOptionDays[(tradeInfo.ForwardCurveForReports+"-Total")] += (calc < 0) ? 0 : calc;
                        }
                        break;
                    case ReportResultEnum.AvgShortPriceOption:
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                             //tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")
                             Convert.ToDecimal(e.FieldValue) < 0
                             )))
                        {
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);
                            }
                            catch
                            {
                            }

                            calc = 0;
                            if (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g"))
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                                totalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;
                            }
                            else
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(valueField);
                                totalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[indexPhysicalFinancial] += calc;
                                sumTotalAvgShortOptionDays[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;
                            }
                            counter++;
                        }
                        break;
                    case ReportResultEnum.NetPaperDaysOption:
                       decimal valueField1 = 0;
                        try
                        {
                            valueField1 = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);//((Dictionary<string, Dictionary<DateTime, decimal>>)gridPositionResultsView.Tag)[tradeInfo.Identifier][Convert.ToDateTime(month)];
                        }
                        catch
                        {
                        }
                        if (tradeInfo != null &&
                            (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") ||
                             tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") ||
                             tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g") ||
                                                                                          tradeInfo.Type == TradeTypeEnum.TC.ToString("g") ))
                                                                                          //||tradeInfo.Type == TradeTypeEnum.Cargo.ToString("g")))
                            totalNetPaperDaysOptions[(tradeInfo.ForwardCurveForReports+"-Total")] += Convert.ToDecimal(valueField1);
                        break;                    
                    case ReportResultEnum.AvgShortPrice: //AvgShortPrice                        
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")) ||
                             (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g"))))
                        {
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);
                            }
                            catch
                            {
                            }
                            calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                            totalAvgShortPrice[indexPhysicalFinancial] += calc;
                            sumTotalAvgShortPrice[indexPhysicalFinancial] += calc;
                            sumTotalAvgShortPrice[(tradeInfo.ForwardCurveForReports+"-Total")] += calc;                            
                        }
                        break;
                    case ReportResultEnum.CoverAvgRateFFA:
                        if (tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") )))
                        {
                            //decimal valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo,month);//((GridSummaryItem)e.Item).FieldName)];
                            decimal valueField = 0;
                            try
                            {
                                valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);
                            }
                            catch
                            {
                            }
                            calc = 0;

                            calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                            totalPaperNotionalFFA[indexPhysicalFinancial] += calc;
                            sumTotalPaperNotionalFFA[indexPhysicalFinancial] += calc;
                            sumTotalPaperNotionalFFA[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;

                        }
                        break;
                    case ReportResultEnum.CoverAvgRate:
                        if(tradeInfo != null &&
                            ((tradeInfo.Type == TradeTypeEnum.FFA.ToString("g") ||
                              tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") ||
                              tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g") ||
                             ((tradeInfo.Type == TradeOPTIONInfoTypeEnum.Call.ToString("g") || tradeInfo.Type == TradeOPTIONInfoTypeEnum.Put.ToString("g")) &&
                            // tradeInfo.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g")
                            Convert.ToDecimal(e.FieldValue) > 0
                             ))))
                        {                            
                            decimal valueField = 0;
                            valueField = GetTradeData_BaseAggregation(_tradePositionResults, tradeInfo, month);
                          
                            calc = 0;
                            if (tradeInfo.Type == TradeTypeEnum.FFA.ToString("g"))
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.Price + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.Price) * Convert.ToDecimal(valueField);
                                totalPaperOptional[indexPhysicalFinancial] += calc;
                                sumTotalPaperOptional[indexPhysicalFinancial] += calc;
                                sumTotalPaperOptional[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;
                            }
                            else
                            {
                                calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(valueField);
                                totalPaperOptional[indexPhysicalFinancial] += calc;
                                sumTotalPaperOptional[indexPhysicalFinancial] += calc;
                                sumTotalPaperOptional[(tradeInfo.ForwardCurveForReports + "-Total")] += calc;
                            }
                        }
                        break;
                    case ReportResultEnum.AdjOption:
                        if (tradeInfo != null &&
                            (tradeInfo.Type == TradeTypeEnum.TC.ToString("g") &&
                             tradeInfo.Direction == TradeInfoDirectionEnum.OutOrSell.ToString("g")))
                        {
                            calc = (tradeInfo.FCCoverAssocOffset != null ? tradeInfo.StrikePrice + tradeInfo.FCCoverAssocOffset.Value : tradeInfo.StrikePrice) * Convert.ToDecimal(e.FieldValue);
                            totalTcOutPrice[indexPhysicalFinancial] += calc;
                            sumTotalTcOutPrice[indexPhysicalFinancial] += calc;
                        }
                        break;
                }
                #endregion


            }
            // Finalization 
            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                string fieldName;
                e.TotalValue = "N/A";
                switch (summaryType)
                {
                    #region Commissions
                    case ReportResultEnum.Commission:
                        fieldName = "COMMITION:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.Commission;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (!mtmCalculationsPerIndex[indx][fieldName].ContainsKey(month))
                                    mtmCalculationsPerIndex[indx][fieldName].Add(month, new decimal?());

                                mtmCalculationsPerIndex[indx][fieldName][month] = totalCommission[indx];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion                

                    #region LockedIn
                    case ReportResultEnum.LockedIn:
                        fieldName = "LOCKED IN:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.LockedIn;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month]);
                                var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month]);
                                var avgShortPrice = (decimal)(posCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month] ?? 0);
                                var avgLongPrice = (decimal)(posCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month] ?? 0);
                                
                                if (totalLongDays < totalShortDays)
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = totalLongDays * (avgShortPrice - avgLongPrice);
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = totalShortDays * (avgShortPrice - avgLongPrice);
                                }
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                //if (chkCalculateSums.Checked)
                                //{
                                //    if (totalLongDays <= totalShortDays)
                                //    {
                                //        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + (totalLongDays * (avgShortPrice - avgLongPrice));
                                //    }
                                //    else if (totalLongDays > totalShortDays)
                                //    {
                                //        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + (totalShortDays * (avgLongPrice - avgShortPrice));
                                //    }
                                //    //mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                //}
                            }

                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);                                

                                var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month]);
                                var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month]);
                                var avgShortPrice = (decimal)(posCalculationsPerIndex[totalIndx]["AVG SHORT DEAL PRICE:"][month]??0);
                                var avgLongPrice = (decimal)(posCalculationsPerIndex[totalIndx]["AVG LONG DEAL PRICE:"][month]??0);

                                if (totalLongDays < totalShortDays)
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalLongDays * (avgShortPrice - avgLongPrice);
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalShortDays * (avgShortPrice - avgLongPrice);
                                }
                            }
                        }
                        e.TotalValue = "N/A";
                        break;
                    #endregion

                    #region BreakevenPrice
                    case ReportResultEnum.BreakevenPrice:
                        fieldName = "BREAKEVEN PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.BreakevenPrice;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                var lockedIn = mtmCalculationsPerIndex[indx]["LOCKED IN:"][month];                                
                                var avgShortPrice = mtmCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month];
                                var avgLongPrice = mtmCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month];
                                var longDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month]);
                                var shortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month]);
                                var netDays = posCalculationsPerIndex[indx.Substring(0, indx.IndexOf("-"))+"-Total"]["NET DAYS:"][month];

                                if (indx.Contains("Physical"))
                                {
                                    if (longDays < shortDays)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgShortPrice) - (lockedIn ?? 0)) / netDays;
                                    else if (longDays > shortDays)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgLongPrice) - (lockedIn ?? 0)) / netDays;
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                                else {
                                    if (netDays.HasValue && netDays < 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgShortPrice) - (lockedIn ??0 )) / netDays;
                                    else if (netDays.HasValue && netDays != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgLongPrice) - (lockedIn ?? 0)) / netDays;
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                                //if (chkCalculateSums.Checked)
                                //{
                                //    if (netDays < 0)
                                //        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + (avgShortPrice - (lockedIn / netDays));
                                //    else if (netDays > 0)
                                //        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + (avgLongPrice - (lockedIn / netDays));
                                //    //mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                //}
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                decimal? avgShortPrice = null;
                                decimal? avgLongPrice = null;
                                var longDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month]);
                                var shortDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month]);
                                var lockedIn = mtmCalculationsPerIndex[totalIndx]["LOCKED IN:"][month];
                                var netDays = posCalculationsPerIndex[totalIndx]["NET DAYS:"][month];
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        avgShortPrice = mtmCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month];
                                        avgLongPrice = mtmCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month];
                                    }

                                }
                                if (netDays.HasValue && netDays < 0)
                                    result = ((netDays * avgShortPrice) - (lockedIn ?? 0)) / netDays;
                                else if (netDays.HasValue && netDays !=0)
                                    result = ((netDays * avgLongPrice) - (lockedIn ?? 0)) / netDays;
                                else
                                    result = null;
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = result;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region Unrealized
                    case ReportResultEnum.Unrealized:
                        fieldName = "UNREALIZED:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.Unrealized;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month]);
                                var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month]);
                                var netDays = (decimal)posCalculationsPerIndex[indx]["NET DAYS:"][month];

                                if (indx.Contains("Physical"))
                                {
                                    if (totalLongDays > totalShortDays)
                                    {
                                        var avgLongMTMPrice = mtmCalculationsPerIndex[indx]["AVG LONG MTM PRICE:"][month];
                                        var avgLongPrice = mtmCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month];

                                        mtmCalculationsPerIndex[indx][fieldName][month] = (avgLongMTMPrice - avgLongPrice) * netDays;
                                    }
                                    else if (totalLongDays < totalShortDays)
                                    {
                                        var avgShortMTMPrice = mtmCalculationsPerIndex[indx]["AVG SHORT MTM PRICE:"][month];
                                        var avgShortPrice = mtmCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month];

                                        mtmCalculationsPerIndex[indx][fieldName][month] = (avgShortMTMPrice - avgShortPrice) * netDays;
                                    }
                                    else
                                    {
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                    }
                                }
                                else
                                {
                                    //decimal indexValue = GetIndexValueWithAggregation(e, indx, month);

                                    //if (netDays > 0)
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = (indexValue - mtmCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month]) * netDays;
                                    //else if (netDays <= 0)
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = (indexValue - mtmCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month]) * netDays;
                                    //else
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = null;

                                    if (netDays < 0)
                                    {
                                        var avgShortMTMPrice = mtmCalculationsPerIndex[indx]["AVG SHORT MTM PRICE:"][month];
                                        var avgShortPrice = mtmCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month];
                                        mtmCalculationsPerIndex[indx][fieldName][month] = (avgShortPrice - avgShortMTMPrice) * Math.Abs(netDays);
                                    }
                                    else
                                    {
                                        var avgLongMTMPrice = mtmCalculationsPerIndex[indx]["AVG LONG MTM PRICE:"][month];
                                        var avgLongPrice = mtmCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month];
                                        mtmCalculationsPerIndex[indx][fieldName][month] = (avgLongMTMPrice - avgLongPrice) * netDays;
                                    }
                                }
                                //if (chkCalculateSums.Checked)
                                //    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                decimal avgLongMTMPrice = 0;
                                decimal avgLongPrice = 0;
                                decimal avgShortMTMPrice = 0;
                                decimal avgShortPrice = 0;
                                var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month]);
                                var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month]);
                                var netDays = (decimal)posCalculationsPerIndex[totalIndx]["NET DAYS:"][month];
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        avgLongMTMPrice += (mtmCalculationsPerIndex[indx]["AVG LONG MTM PRICE:"][month] ?? 0);
                                        avgLongPrice += (mtmCalculationsPerIndex[indx]["AVG LONG DEAL PRICE:"][month] ??0);
                                        avgShortMTMPrice += (mtmCalculationsPerIndex[indx]["AVG SHORT MTM PRICE:"][month] ?? 0);
                                        avgShortPrice += (mtmCalculationsPerIndex[indx]["AVG SHORT DEAL PRICE:"][month] ?? 0);                                        
                                    }
                                }
                                if (netDays < 0)
                                {
                                    result = (avgShortPrice - avgShortMTMPrice) * Math.Abs(netDays);
                                }
                                else
                                {
                                    result = (avgLongMTMPrice - avgLongPrice) * netDays;
                                }
                                //decimal indexValue = GetIndexValueWithAggregation(e, totalIndx, month);
                                //avgLongPrice = (mtmCalculationsPerIndex[totalIndx]["AVG LONG DEAL PRICE:"][month] ?? 0);
                                //avgShortPrice = (mtmCalculationsPerIndex[totalIndx]["AVG SHORT DEAL PRICE:"][month] ?? 0);
                                //if (netDays > 0)
                                //    result = (indexValue - avgLongPrice) * netDays;
                                //else if (netDays < 0)
                                //    result = (indexValue - avgShortPrice) * netDays;
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = result;
                                if (chkCalculateSums.Checked)
                                 mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TotalMTM
                    case ReportResultEnum.TotalMTM:
                        fieldName = "TOTAL MTM:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalMTM;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                                //mtmCalculationsPerIndex[indx][fieldName ][month] = totalMTM[indx];

                                var tcInDays = posCalculationsPerIndex[indx]["TC IN DAYS:"][month];

                                if (tcInDays != 0)
                                    mtmCalculationsPerIndex[indx][fieldName][month] = totalMTM[indx] / tcInDays;
                                else
                                    mtmCalculationsPerIndex[indx][fieldName][month] = null;

                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region IndexEqCharteresRate
                    case ReportResultEnum.IndexEqCharterersRate:
                        fieldName = "INDEX EQUIVALENT CHARTERERS RATE:";
                        //Get Tc Days From Position Results Grid
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.IndexEqCharterersRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var tcOutDays = posCalculationsPerIndex[indx]["TC OUT DAYS:"][month];

                                    if (tcOutDays != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcOutCashFlow[indx] / tcOutDays;
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                if (mtmCalculationsPerIndex[totalIndx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                    var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month];

                                    if (tcOutDays != 0)
                                        mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalTcOutCashFlow[totalIndx] / tcOutDays;
                                    else
                                        mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region IndexEqOwnersRate
                    //case ReportResultEnum.IndexEqOwnersRate:
                    //    //Get Tc Days From Position Results Grid
                    //    fieldName = "INDEX EQUIVALENT OWNERS RATE:";
                    //    if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                    //    {
                    //        e.TotalValue = fieldName;
                    //    }
                    //    else
                    //    {
                    //        ((GridSummaryItem)e.Item).Tag = ReportResultEnum.IndexEqOwnersRate;

                    //        foreach (var indx in indexesPhysicalFinancial)
                    //        {
                    //            if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                    //            {
                    //                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                    //                var tcInDays = posCalculationsPerIndex[indx]["TC IN DAYS:"][month];

                    //                if (tcInDays != 0)
                    //                {
                    //                    if (indx.Contains("Physical"))
                    //                    {
                    //                        mtmCalculationsPerIndex[indx][fieldName][month] = totalMTMIndexEqOwnersRateFin[indx] / tcInDays;
                    //                    }
                    //                    else
                    //                    {
                    //                        mtmCalculationsPerIndex[indx][fieldName][month] = (totalMTMIndexEqOwnersRateFin[indx] + totalMTMIndexEqOwnersRateFin[indx.Substring(0, indx.IndexOf("-")) + "-Physical"]) / tcInDays;
                    //                    }
                    //                }
                    //                else
                    //                    mtmCalculationsPerIndex[indx][fieldName][month] = null;

                    //                if (chkCalculateSums.Checked)
                    //                    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                    //            }
                    //        }

                    //        foreach (var totalIndx in indexesPhysicalFinancialTotal)
                    //        {                                
                    //            AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                    //            var tcInDays = posCalculationsPerIndex[totalIndx]["TC IN DAYS:"][month];
                    //            decimal? result = null ;
                    //            foreach (var indx in indexesPhysicalFinancial)
                    //            {
                    //                if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                    //                {
                    //                    if (tcInDays != 0)
                    //                    {

                    //                        if (indx.Contains("Physical"))
                    //                        {
                    //                            result = totalTcInCashFlow[indx] / tcInDays;
                    //                        }
                    //                        else
                    //                        {                                                
                    //                            result = (totalMTMIndexEqOwnersRateFin[indx] + ((totalMTMIndexEqOwnersRateFin.Keys.Contains(indx.Substring(0, indx.IndexOf("-")) + "-Physical")) ? totalMTMIndexEqOwnersRateFin[indx.Substring(0, indx.IndexOf("-")) + "-Physical"] : 0)) / tcInDays;
                    //                        }
                    //                    }
                    //                }
                    //                mtmCalculationsPerIndex[totalIndx][fieldName][month] =  result;
                    //            }
                    //            if (chkCalculateSums.Checked)
                    //                mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;
                    //        }
                    //        e.TotalValue = "N/A";
                    //    }
                    //    break;
                    #endregion

                    #region EffCharterersEarningsRate
                    case ReportResultEnum.EffCharterersEarningsRate:
                        //Get Tc Days From Position Results Grid
                        fieldName = "EFFECTIVE CHARTERERS EARNINGS RATE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.EffCharterersEarningsRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    if (totalTcOutDaysVesselIndex[indx] != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcOutCashFlow[indx] / totalTcOutDaysVesselIndex[indx];
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;

                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                            }
                        }
                        break;
                    #endregion

                    #region EffOwnersEarningsRate
                    //case ReportResultEnum.EffOwnersEarningsRate:
                    //    //Get Tc Days From Position Results Grid
                    //    fieldName = "EFFECTIVE OWNERS EARNINGS RATE:";
                    //    if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                    //    {
                    //        e.TotalValue = fieldName;
                    //    }
                    //    else
                    //    {
                    //        ((GridSummaryItem)e.Item).Tag = ReportResultEnum.EffOwnersEarningsRate;

                    //        foreach (var indx in indexesPhysicalFinancial)
                    //        {
                    //            AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                    //            //  if (totalTcInDaysVesselIndex[indx] != 0)
                    //            //       mtmCalculationsPerIndex[indx][fieldName ][month] =
                    //            //         totalTcInCashFlow[indx]/totalTcInDaysVesselIndex[indx];

                    //            //  else
                    //            //    mtmCalculationsPerIndex[indx][fieldName ][month] = null;                                
                    //            if (indx.Contains("Physical"))
                    //            {
                    //                //var calendarDays = totalTcInCalendarDays[indx];
                    //                //if (calendarDays == 0) continue;
                    //                //mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / calendarDays;
                    //                var calendarDays = GetTotalTcInCalendarDays(indx, totalTcInCalendarDays);
                    //                if (calendarDays == 0) continue;
                    //                mtmCalculationsPerIndex[indx][fieldName][month] = (totalMTMIndexEqOwnersRateFin[indx] ) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //            }
                    //            else
                    //            {
                    //                var calendarDays = GetTotalTcInCalendarDays(indx, totalTcInCalendarDays);
                    //                if (calendarDays == 0) continue;
                    //                mtmCalculationsPerIndex[indx][fieldName][month] = (totalMTMIndexEqOwnersRateFin[indx] + totalMTMIndexEqOwnersRateFin[indx.Substring(0, indx.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //            }
                    //            //var calendarDays = totalTcInCalendarDays[indx];                                
                    //            //if (rdgResultsAggregationType.SelectedIndex == 0) {
                    //            //    var currentDate = Convert.ToDateTime(month);
                    //            //    var daysInMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

                    //            //    //if (!indx.Contains("Physical"))
                    //            //    //    mtmCalculationsPerIndex[indx][fieldName ][month] = totalTcInCashFlow[indx] / daysInMonth;
                    //            //    //else
                    //            //    //    mtmCalculationsPerIndex[indx][fieldName ][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (daysInMonth * _physicalVessels) : daysInMonth);
                    //            //    if (calendarDays == 0) continue;
                    //            //    if (!indx.Contains("Physical"))
                    //            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / calendarDays;
                    //            //    else
                    //            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //            //}
                    //            //else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                    //            //{
                    //            //    //int dayOfQuarter = GetDaysOfQuarter(month);
                    //            //    if (calendarDays == 0) continue;
                    //            //    if (!indx.Contains("Physical"))
                    //            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / calendarDays;
                    //            //    else
                    //            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //            //}
                    //            //else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                    //            //{
                    //            //    //int dayInYear = GetDaysOfYear(Convert.ToDateTime(month.Substring(4)+"-01-01"));
                    //            //    if (calendarDays == 0) continue;
                    //            //    if (!indx.Contains("Physical"))
                    //            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / calendarDays;
                    //            //    else
                    //            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //            //}
                    //            //if (chkCalculateSums.Checked)
                    //            //    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                    //        }
                    //        foreach (var totalIndx in indexesPhysicalFinancialTotal)
                    //        {
                    //            AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                    //            decimal? result = null;
                    //            foreach (var indx in indexesPhysicalFinancial)
                    //            {
                    //                if (indx.Contains("Physical"))
                    //                {
                    //                    var calendarDays = totalTcInCalendarDays[indx];
                    //                    if (calendarDays == 0) continue;
                    //                    result = totalTcInCashFlow[indx] / calendarDays;
                    //                }
                    //                else
                    //                {
                    //                    var calendarDays = GetTotalTcInCalendarDays(indx, totalTcInCalendarDays);
                    //                    if (calendarDays == 0) continue;
                    //                    result = (totalMTMIndexEqOwnersRateFin[indx] + totalMTMIndexEqOwnersRateFin[indx.Substring(0, indx.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //                }
                    //                mtmCalculationsPerIndex[totalIndx][fieldName][month] =  result;
                    //            }
                    //            if (chkCalculateSums.Checked)
                    //                mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;
                    //        }
                    //        e.TotalValue = "N/A";
                    //    }
                    //    break;
                    #endregion

                    #region CoverAvgRate - COVER AVERAGE RATE
                    //case ReportResultEnum.CoverAvgRate:
                    //    fieldName = "COVER AVERAGE RATE:";
                    //    if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                    //    {
                    //        e.TotalValue = fieldName;
                    //    }
                    //    else
                    //    {

                    //        ((GridSummaryItem)e.Item).Tag = ReportResultEnum.CoverAvgRate;

                    //        foreach (var indx in indexesPhysicalFinancial)
                    //        {
                    //            AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                    //            var sumFfaAndOptionDays = posCalculationsPerIndex[indx]["NET DAYS:"][month] ?? 0;

                    //            //if (sumFfaAndOptionDays != 0)
                    //            //{
                    //            //    mtmCalculationsPerIndex[indx][fieldName][month] = totalPaperNotional[indx] / sumFfaAndOptionDays;

                    //            //}
                    //            //else
                    //            //    mtmCalculationsPerIndex[indx][fieldName][month] = null;
                    //            if (sumFfaAndOptionDays < 0)
                    //            {
                    //                if(totalPaperNotional[indx] !=0)
                    //                    {

                    //                }
                    //                mtmCalculationsPerIndex[indx][fieldName][month] = totalPaperNotional[indx] / sumFfaAndOptionDays;

                    //            }
                    //            else
                    //                mtmCalculationsPerIndex[indx][fieldName][month] = null;
                    //        }
                    //        foreach (var totalIndx in indexesPhysicalFinancialTotal)
                    //        {
                    //            AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);                                

                    //            var netPaperDays = posCalculationsPerIndex[totalIndx]["NET DAYS:"][month] ?? 0;
                    //            var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month] ?? 0;
                    //            decimal totalFfaAndOptonNotionalForTotal = 0;
                    //            foreach (var indx in indexesPhysicalFinancial)
                    //            {
                    //                if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                    //                {
                    //                    //totalFfaNotionalForTotal = totalFfaNotionalForTotal + totalFfaNotional[indx];
                    //                    totalFfaAndOptonNotionalForTotal = totalFfaAndOptonNotionalForTotal + totalPaperNotional[indx] + totalTcOutPrice[indx];
                    //                }
                    //            }
                    //            if (netPaperDays + tcOutDays != 0)
                    //                mtmCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)totalFfaAndOptonNotionalForTotal / (netPaperDays + tcOutDays));
                    //            else
                    //                mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                
                    //        }
                    //        e.TotalValue = "N/A";
                    //    }
                    //    break;
                    #endregion

                    #region CoverAvgRate - COVER AVERAGE RATE (only for FFAs)
                    case ReportResultEnum.CoverAvgRateFFA:
                        fieldName = "COVER AVERAGE RATE (only for FFAs):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {

                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.CoverAvgRateFFA;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (indx != indx.Substring(0, indx.IndexOf("-")) + "-Physical")
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var sumFfaAndOptionDays = posCalculationsPerIndex[indx]["NET DAYS:"][month] ?? 0;

                                    //if (sumFfaAndOptionDays != 0)
                                    //{
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = totalPaperNotional[indx] / sumFfaAndOptionDays;

                                    //}
                                    //else
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                    if (sumFfaAndOptionDays < 0)
                                    {
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalPaperNotionalFFA[indx] / sumFfaAndOptionDays;

                                    }
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {

                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var netPaperDays = posCalculationsPerIndex[totalIndx]["NET DAYS:"][month] ?? 0;
                                var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month] ?? 0;
                                decimal totalFfaAndOptonNotionalForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        //totalFfaNotionalForTotal = totalFfaNotionalForTotal + totalFfaNotional[indx];
                                        totalFfaAndOptonNotionalForTotal = totalFfaAndOptonNotionalForTotal + totalPaperNotionalFFA[indx] + totalTcOutPrice[indx];
                                    }
                                }
                                if (netPaperDays + tcOutDays != 0)
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)totalFfaAndOptonNotionalForTotal / (netPaperDays + tcOutDays));
                                else
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;

                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgLongPrice - AVG LONG DEAL PRICE
                    case ReportResultEnum.AvgLongPrice:
                        fieldName = "AVG LONG DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgLongPrice;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                var sumLongDays = posCalculationsPerIndex[indx]["TOTAL LONG DAYS:"][month];

                                if (sumLongDays != 0)
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = totalAvgLongPrice[indx] / sumLongDays;

                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var sumLongDays = posCalculationsPerIndex[totalIndx]["TOTAL LONG DAYS:"][month];
                                decimal totalAvgLongPriceForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalAvgLongPriceForTotal = totalAvgLongPriceForTotal + totalAvgLongPrice[indx];
                                    }

                                }
                                if (sumLongDays != 0)
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgLongPriceForTotal / sumLongDays;                                   
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }

                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];                                
                            }
                            e.TotalValue = "N/A";
                        }
                        break;

                    #endregion                    

                    #region AvgShortPrice - AVG SHORT DEAL PRICE
                    case ReportResultEnum.AvgShortPrice:
                        fieldName = "AVG SHORT DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgShortPrice;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                var sumShortDays = posCalculationsPerIndex[indx]["TOTAL SHORT DAYS:"][month];

                                if (sumShortDays != 0)
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = totalAvgShortPrice[indx] / sumShortDays;

                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var sumShortDays = posCalculationsPerIndex[totalIndx]["TOTAL SHORT DAYS:"][month];
                                decimal totalAvgShortPriceForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        totalAvgShortPriceForTotal = totalAvgShortPriceForTotal + totalAvgShortPrice[indx];
                                    }

                                }
                                if (sumShortDays != 0)
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)(totalAvgShortPriceForTotal / (sumShortDays)));                                    
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgLongMtmPrice - AVG LONG MTM PRICE:
                    case ReportResultEnum.AvgLongMtmPrice:
                        fieldName = "AVG LONG MTM PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                                if (indx.Contains("Physical"))
                                {
                                    if (totalMTMLongPricePositionDays[indx] != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalMTMLongPrice[indx] / totalMTMLongPricePositionDays[indx];
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                                else
                                {                                    
                                    mtmCalculationsPerIndex[indx][fieldName][month] = mtmCalculationsPerIndex[indx][indx.Substring(0, indx.IndexOf("-"))][month];                                    
                                }                              
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                //foreach (var indx in indexesPhysicalFinancial)
                                //{
                                //    if (indx.Contains("Physical"))
                                //    {
                                //        if (totalMTMLongPricePositionDays[indx] != 0)
                                //            mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalMTMLongPrice[indx] / totalMTMLongPricePositionDays[indx];
                                //        else
                                //            mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                //    }
                                //    else
                                //    {
                                //        mtmCalculationsPerIndex[totalIndx][fieldName][month] = mtmCalculationsPerIndex[indx][indx.Substring(0, indx.IndexOf("-"))][month];
                                //    }
                                //}
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = mtmCalculationsPerIndex[totalIndx][totalIndx.Substring(0, totalIndx.IndexOf("-"))][month];
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                            }

                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgShortMtmPrice - AVG SHORT MTM PRICE:
                    case ReportResultEnum.AvgShortMtmPrice:
                        fieldName = "AVG SHORT MTM PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                                /* if (totalMTMShortPricePositionDays[indx] != 0)
                                     mtmCalculationsPerIndex[indx][fieldName ][month] = totalMTMShortPrice[indx] /totalMTMShortPricePositionDays[indx];
                                 else
                                     mtmCalculationsPerIndex[indx][fieldName ][month] = null;
                                     */
                                if (indx.Contains("Physical"))
                                {
                                    if (totalMTMShortPricePositionDays[indx] != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalMTMShortPrice[indx] / totalMTMShortPricePositionDays[indx];
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                                else
                                {
                                    var fc = indx.Substring(0, indx.IndexOf("-"));
                                    mtmCalculationsPerIndex[indx][fieldName][month] = mtmCalculationsPerIndex[indx][fc][month];
                                }
                                //var fc = indx.Substring(0, indx.IndexOf("-"));
                                //mtmCalculationsPerIndex[indx][fieldName ][month] = mtmCalculationsPerIndex[indx][fc][month];

                                //if (chkCalculateSums.Checked) //have to chck again this process, temporary fix on CopyMTMCustoSummarys
                                //{                               
                                //     mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                //}
                            }

                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    //if (indx.Contains("Physical"))
                                    //{
                                    //    if (totalMTMShortPricePositionDays[indx] != 0)
                                    //        mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalMTMShortPrice[indx] / totalMTMShortPricePositionDays[indx];
                                    //    else
                                    //        mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                    //}
                                    //else
                                    //{
                                    //    var fc = indx.Substring(0, indx.IndexOf("-"));
                                    //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = mtmCalculationsPerIndex[indx][fc][month];
                                    //}
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = mtmCalculationsPerIndex[totalIndx][totalIndx.Substring(0, totalIndx.IndexOf("-"))][month];
                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion                   

                    #region AdjOption - (adj) for options:
                    case ReportResultEnum.AdjOption:
                        fieldName = "(adj) for options:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AdjOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var paperDays = Convert.ToDecimal(posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month]);

                                    //if (paperDays != 0)
                                    //{
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = totalPaperNotional[indx] / paperDays;
                                        
                                    //}
                                    //else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var paperDays = Convert.ToDecimal(posCalculationsPerIndex[totalIndx]["NET PAPER DAYS:"][month]);

                                //decimal totalPaperNotionalForTotal = 0;
                                //foreach (var indx in indexesPhysicalFinancial)
                                //{
                                //    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                //    {
                                //        totalPaperNotionalForTotal = totalPaperNotionalForTotal + totalPaperNotional[indx];
                                //    }
                                //}
                                //if (paperDays != 0)
                                //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalPaperNotionalForTotal / paperDays;
                                //else
                                //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;

                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;

                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion          

                    #region with OPTIONS

                    #region TotalLongDaysOption - (incl. OPTIONS) TOTAL LONG DAYS
                    case ReportResultEnum.TotalLongDaysOption:
                        fieldName = "(incl. OPTIONS) TOTAL LONG DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalLongDaysOption;
                            //foreach (var indx in indexesPhysicalFinancial)
                            //{
                            //    if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                            //    {
                            //        AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                            //        //mtmCalculationsPerIndex[indx][fieldName][month] = totalLongOptionDays[indx];

                            //        mtmCalculationsPerIndex[indx][fieldName][month] = posCalculationsPerIndex[indx][fieldName][month];
                            //    }
                            //}
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                //decimal totalLongOptionDaysForTotal = 0;
                                //foreach (var indx in indexesPhysicalFinancial)
                                //{
                                //    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                //    {
                                //        totalLongOptionDaysForTotal = totalLongOptionDaysForTotal + totalLongOptionDays[indx];
                                //    }

                                //}
                                //mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalLongOptionDaysForTotal;
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = posCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region TotalShortDaysOption - (incl. OPTIONS) TOTAL SHORT DAYS
                    case ReportResultEnum.TotalShortDaysOption:
                        fieldName = "(incl. OPTIONS) TOTAL SHORT DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalShortDaysOption;

                            //foreach (var indx in indexesPhysicalFinancial)
                            //{
                            //    if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                            //    {
                            //        AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                            //        mtmCalculationsPerIndex[indx][fieldName][month] = totalShortOptionDays[indx];

                           
                            //    }
                            //}
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                //decimal totalShortOptionDaysForTotal = 0;
                                //foreach (var indx in indexesPhysicalFinancial)
                                //{
                                //    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                //    {
                                //        totalShortOptionDaysForTotal = totalShortOptionDaysForTotal + totalShortOptionDays[indx];
                                //    }

                                //}

                                //mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalShortOptionDaysForTotal;
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = posCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region NetDaysOption - (incl. OPTIONS) NET DAYS
                    case ReportResultEnum.NetDaysOption:
                        fieldName = "(incl. OPTIONS) NET DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.NetDaysOption;
                            
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var sumShortDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = sumLongDays + sumShortDays;                                
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region CoverOption - (incl. OPTIONS) SHORT/LONG COVER (%)
                    case ReportResultEnum.CoverOption:
                        fieldName = "(incl. OPTIONS) SHORT/LONG COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.CoverOption;
                            
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var sumShortDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                var sumLongDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                if (sumLongDays != 0)
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)(sumShortDays / sumLongDays));                                    
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                }
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region Avg Long Deal Price Option - (incl. OPTIONS) AVG LONG DEAL PRICE
                    case ReportResultEnum.AvgLongPriceOption:
                        fieldName = "(incl. OPTIONS) AVG LONG DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {                            
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgLongPriceOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (indx != indx.Substring(0, indx.IndexOf("-")) + "-Physical")
                                    if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                    {
                                        AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                        //var sumLongDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];

                                        //if (sumLongDays != 0)
                                        //    mtmCalculationsPerIndex[indx][fieldName][month] = totalAvgLongOptionDays[indx] / sumLongDays;
                                        //else
                                        //{
                                        //    mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                        //}
                                        var result = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month];
                                        if (result != null)
                                            mtmCalculationsPerIndex[indx][fieldName][month] = result;
                                        else
                                            mtmCalculationsPerIndex[indx][fieldName][month] = null;

                                        if (chkCalculateSums.Checked)
                                            mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                    }
                            }
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var sumLongDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month];
                                decimal? totalAvgLongOptionDaysForTotal = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        //totalAvgLongOptionDaysForTotal = totalAvgLongOptionDaysForTotal + totalAvgLongOptionDays[indx];
                                        totalAvgLongOptionDaysForTotal = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month];
                                    }
                                }

                                //if (sumLongDays != 0)
                                //{
                                //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgLongOptionDaysForTotal / sumLongDays;                                    
                                //}
                                //else
                                //{
                                //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                //}
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgLongOptionDaysForTotal;

                                //if (chkCalculateSums.Checked)
                                //    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region Avg Short Deal Price Option - (incl. OPTIONS) AVG SHORT DEAL PRICE
                    case ReportResultEnum.AvgShortPriceOption:
                        fieldName = "(incl. OPTIONS) AVG SHORT DEAL PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {                            
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.AvgShortPriceOption;
                            foreach (var indx in indexesPhysicalFinancial)
                            {                                
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    //var sumFfaAndOptionDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month] ?? 0;                                    

                                    //if (sumFfaAndOptionDays != 0)
                                    //{
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = totalAvgShortOptionDays[indx] / sumFfaAndOptionDays;                                        
                                    //}
                                    //else
                                    //    mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                    var result = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month];
                                    if(result != null)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = result;
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                            }
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                //var sumShortDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month];
                                decimal? totalAvgShortgOptionDaysForTotal = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        //totalAvgShortgOptionDaysForTotal = totalAvgShortgOptionDaysForTotal + totalAvgShortOptionDays[indx];
                                        totalAvgShortgOptionDaysForTotal = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month];
                                    }

                                }
                                if (totalAvgShortgOptionDaysForTotal != null)
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgShortgOptionDaysForTotal;
                                else
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                //if (sumShortDays != 0)
                                //{
                                //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalAvgShortgOptionDaysForTotal / sumShortDays;                                    
                                //}
                                //else
                                //{
                                //    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                                //}
                                //if (chkCalculateSums.Checked)
                                //    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region CoverAvgRate - COVER AVERAGE RATE
                    case ReportResultEnum.CoverAvgRate:
                        fieldName = "COVER AVERAGE RATE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.CoverAvgRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var sumFfaAndOptionDays = posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month] ?? 0;

                                    if (sumFfaAndOptionDays != 0)
                                    {
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalPaperOptional[indx] / sumFfaAndOptionDays;                                        
                                    }
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;                                  
                                }
                            }
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var netPaperDays = posCalculationsPerIndex[totalIndx]["NET PAPER DAYS:"][month] ?? 0;
                                var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month] ?? 0;
                                decimal totalFfaAndOptonNotionalForTotal = 0;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        //totalFfaNotionalForTotal = totalFfaNotionalForTotal + totalFfaNotional[indx];
                                        totalFfaAndOptonNotionalForTotal = totalFfaAndOptonNotionalForTotal + totalPaperOptional[indx] + totalTcOutPrice[indx];
                                    }
                                }
                                if (netPaperDays + tcOutDays != 0)
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)totalFfaAndOptonNotionalForTotal / (netPaperDays + tcOutDays));
                                else
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;

                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region EffOwnersEarningsRate - EFFECTIVE OWNERS EARNINGS RATE
                    case ReportResultEnum.EffOwnersEarningsRate:
                        //Get Tc Days From Position Results Grid
                        fieldName = "EFFECTIVE OWNERS EARNINGS RATE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.EffOwnersEarningsRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                                    //  if (totalTcInDaysVesselIndex[indx] != 0)
                                    //       mtmCalculationsPerIndex[indx][fieldName ][month] =
                                    //         totalTcInCashFlow[indx]/totalTcInDaysVesselIndex[indx];

                                    //  else
                                    //    mtmCalculationsPerIndex[indx][fieldName ][month] = null;

                                    if (indx.Contains("Physical"))
                                    {
                                        var calendarDays = totalTcInCalendarDays[indx];
                                        if (calendarDays == 0) continue;
                                        //mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / calendarDays;
                                        mtmCalculationsPerIndex[indx][fieldName][month] = (totalMTMIndexEqOwnersRateFinOption[indx]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                                    }
                                    else
                                    {
                                        var calendarDays = GetTotalTcInCalendarDays(indx, totalTcInCalendarDays);
                                        if (calendarDays == 0) continue;
                                        mtmCalculationsPerIndex[indx][fieldName][month] = (totalMTMIndexEqOwnersRateFinOption[indx] + totalMTMIndexEqOwnersRateFinOption[indx.Substring(0, indx.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                                    }

                                    //var temp = totalTcInCashFlow[indx];
                                    //if (rdgResultsAggregationType.SelectedIndex == 0)
                                    //{
                                    //    var currentDate = Convert.ToDateTime(month);
                                    //    var daysInMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
                                    //    if (!indx.Contains("Physical"))
                                    //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / daysInMonth;
                                    //    else
                                    //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (daysInMonth * _physicalVessels) : daysInMonth);
                                    //}
                                    //else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                                    //{
                                    //    int dayOfQuarter = GetDaysOfQuarter(month);
                                    //    if (!indx.Contains("Physical"))
                                    //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / dayOfQuarter;
                                    //    else
                                    //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (dayOfQuarter * _physicalVessels) : dayOfQuarter);
                                    //}
                                    //else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                                    //{
                                    //    int dayInYear = GetDaysOfYear(Convert.ToDateTime(month.Substring(4) + "-01-01"));
                                    //    if (!indx.Contains("Physical"))
                                    //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / dayInYear;
                                    //    else
                                    //        mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / ((_physicalVessels > 0) ? (dayInYear * _physicalVessels) : dayInYear);
                                    //}
                                    //if (chkCalculateSums.Checked)
                                    //    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (totalIndx.Substring(0, totalIndx.IndexOf("-")) == indx.Substring(0, indx.IndexOf("-")))
                                    {
                                        if (indx.Contains("Physical"))
                                        {
                                            var calendarDays = totalTcInCalendarDays[indx];
                                            if (calendarDays == 0) continue;
                                            //result = totalTcInCashFlow[indx] / calendarDays;
                                            result = (totalMTMIndexEqOwnersRateFinOption[indx]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                                        }
                                        else
                                        {
                                            var calendarDays = GetTotalTcInCalendarDays(indx, totalTcInCalendarDays);
                                            if (calendarDays == 0) continue;
                                            result = (totalMTMIndexEqOwnersRateFinOption[indx] + totalMTMIndexEqOwnersRateFinOption[indx.Substring(0, indx.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                                        }
                                    }
                                        mtmCalculationsPerIndex[totalIndx][fieldName][month] =  result;                                    
                                    //if (chkCalculateSums.Checked)
                                    //    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                            }
                        }
                        break;
                    #endregion

                    #region IndexEqOwnersRate - INDEX EQUIVALENT OWNERS RATE
                    case ReportResultEnum.IndexEqOwnersRate:
                        //Get Tc Days From Position Results Grid
                        fieldName = "INDEX EQUIVALENT OWNERS RATE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.IndexEqOwnersRate;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var tcInDays = posCalculationsPerIndex[indx]["TC IN DAYS:"][month];

                                    if (tcInDays != 0)
                                    {
                                        if (indx.Contains("Physical"))
                                        {
                                            mtmCalculationsPerIndex[indx][fieldName][month] = totalTcInCashFlow[indx] / tcInDays;
                                        }
                                        else
                                        {
                                            mtmCalculationsPerIndex[indx][fieldName][month] = (totalMTMIndexEqOwnersRateFinOption[indx] + totalMTMIndexEqOwnersRateFinOption[indx.Substring(0, indx.IndexOf("-")) + "-Physical"]) / tcInDays;
                                        }
                                    }
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;                                    
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var tcInDays = posCalculationsPerIndex[totalIndx]["TC IN DAYS:"][month];
                                decimal? result = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (totalIndx.Substring(0, totalIndx.IndexOf("-")) == indx.Substring(0, indx.IndexOf("-")))
                                    {
                                        if (tcInDays != 0)
                                        {

                                            if (indx.Contains("Physical"))
                                            {
                                                result = totalTcInCashFlow[indx] / tcInDays;
                                                if (result != 0)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                result = (totalMTMIndexEqOwnersRateFinOption[indx] + ((totalMTMIndexEqOwnersRateFinOption.Keys.Contains(indx.Substring(0, indx.IndexOf("-")) + "-Physical")) ? totalMTMIndexEqOwnersRateFinOption[indx.Substring(0, indx.IndexOf("-")) + "-Physical"] : 0)) / tcInDays;
                                            }
                                        }
                                    }
                                        mtmCalculationsPerIndex[totalIndx][fieldName][month] = result;
                                }
                                //if (chkCalculateSums.Checked)
                                //    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region LockedInOption - (incl. OPTIONS) LOCKED IN
                    case ReportResultEnum.LockedInOption:
                        fieldName = "(incl. OPTIONS) LOCKED IN:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = "(incl. OPTIONS) LOCKED IN:";
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.LockedInOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month]);
                                    var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month]);
                                    var avgShortPrice = (decimal)(posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month]??0);
                                    var avgLongPrice = (decimal)(posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month]??0);

                                    if (totalLongDays < totalShortDays)
                                    {
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalLongDays * (avgShortPrice - avgLongPrice);
                                    }
                                    else 
                                    {
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalShortDays * (avgShortPrice - avgLongPrice) ;
                                    }

                                    //if (chkCalculateSums.Checked) //have to chck again this process, temporary fix on CopyMTMCustoSummarys
                                    //    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month]);
                                        var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month]);
                                        var avgShortPrice = (decimal)(posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month]??0);
                                        var avgLongPrice = (decimal)(posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month]??0);

                                        if (totalLongDays < totalShortDays)
                                        {
                                            result = totalLongDays * (avgShortPrice - avgLongPrice);
                                        }
                                        else 
                                        {
                                            result = totalShortDays * (avgShortPrice - avgLongPrice);
                                        }
                                    }
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] =  result;
                                }
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;                            
                            }
                        }
                        break;
                    #endregion

                    #region BreakevenPriceOption - (incl. OPTIONS) BREAKEVEN PRICE
                    case ReportResultEnum.BreakevenPriceOption:
                        fieldName = "(incl. OPTIONS) BREAKEVEN PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.BreakevenPriceOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var lockedIn = mtmCalculationsPerIndex[indx.Substring(0, indx.IndexOf("-"))+ "-Financial"]["(incl. OPTIONS) LOCKED IN:"][month];
                                    var avgShortPrice = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month];
                                    var avgLongPrice = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month];
                                    var longDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month]);
                                    var shortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month]);
                                    var netDays = posCalculationsPerIndex[indx]["(incl. OPTIONS) NET DAYS:"][month];

                                    if (indx.Contains("Physical"))
                                    {
                                        if (longDays < shortDays)
                                            mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgShortPrice) - (lockedIn ?? 0)) / netDays;
                                        else if (longDays > shortDays)
                                            mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgLongPrice) - (lockedIn ?? 0)) / netDays;
                                        else
                                            mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                    }
                                    else
                                    {
                                        if (netDays.HasValue &&  netDays < 0)
                                            mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgShortPrice) - (lockedIn ?? 0)) / netDays;
                                        else if (netDays.HasValue && netDays != 0)
                                            mtmCalculationsPerIndex[indx][fieldName][month] = ((netDays * avgLongPrice) - (lockedIn ?? 0)) / netDays;
                                        else
                                            mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                    }

                                    //if (chkCalculateSums.Checked) //have to chck again this process, temporary fix on CopyMTMCustoSummarys
                                    //    mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                decimal? avgShortPrice = null;
                                decimal? avgLongPrice = null;                                
                                var longDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month]);
                                var shortDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month]);
                                var lockedIn = mtmCalculationsPerIndex[totalIndx]["(incl. OPTIONS) LOCKED IN:"][month];
                                var netDays = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) NET DAYS:"][month];
                                //foreach (var indx in indexesPhysicalFinancial)
                                //{
                                //    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                //    {                                      
                                //        avgShortPrice = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month];
                                //        avgLongPrice = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month];                                                                                                                                                                   
                                //    }

                                //}
                                avgShortPrice = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month];
                                avgLongPrice = posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month];
                                if (netDays.HasValue && netDays < 0)
                                    result = ((netDays * avgShortPrice) - (lockedIn ?? 0)) / netDays;
                                else if (netDays.HasValue && netDays!= 0)
                                    result = ((netDays * avgLongPrice) - (lockedIn ?? 0)) / netDays;
                                else
                                    result = null;
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] =  result;
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;
                            }
                        }
                        break;
                    #endregion

                    #region UnrealizedOption - (incl. OPTIONS) UNREALIZED
                    case ReportResultEnum.UnrealizedOption:
                        fieldName = "(incl. OPTIONS) UNREALIZED:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.UnrealizedOption;

                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                if (mtmCalculationsPerIndex[indx].ContainsKey(fieldName))
                                {
                                    AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                                    var netDays = (decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) NET DAYS:"][month];
                                    var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month]);
                                    var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[indx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month]);

                                    if (indx.Contains("Physical"))
                                    {
                                        if (totalLongDays > totalShortDays)
                                        {
                                            var avgLongPrice = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month];
                                            var avgLongMTMPrice = mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG MTM PRICE:"][month];

                                            mtmCalculationsPerIndex[indx][fieldName][month] = (avgLongMTMPrice - avgLongPrice) * netDays;
                                        }
                                        else if (totalLongDays < totalShortDays)
                                        {
                                            var avgShortMTMPrice = mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT MTM PRICE:"][month];
                                            var avgShortPrice = posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month];

                                            mtmCalculationsPerIndex[indx][fieldName][month] = (avgShortMTMPrice - avgShortPrice) * netDays;
                                        }
                                        else
                                        {
                                            mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                        }
                                    }
                                    else
                                    {
                                        //decimal indexValue = GetIndexValueWithAggregation(e, indx, month);

                                        //if (netDays > 0)
                                        //    mtmCalculationsPerIndex[indx][fieldName][month] = (indexValue - posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month]) * netDays;
                                        //else if (netDays <= 0)
                                        //    mtmCalculationsPerIndex[indx][fieldName][month] = (indexValue - posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month]) * netDays;
                                        if (netDays < 0)
                                        {
                                            var avgShortMTMPrice = (mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT MTM PRICE:"][month] ?? 0);
                                            var avgShortPrice = (posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month] ?? 0);
                                            mtmCalculationsPerIndex[indx][fieldName][month] = (avgShortPrice - avgShortMTMPrice) * Math.Abs(netDays);
                                        }
                                        else
                                        {
                                            var avgLongMTMPrice = (mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG MTM PRICE:"][month] ?? 0);
                                            var avgLongPrice = (posCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month] ?? 0);
                                            mtmCalculationsPerIndex[indx][fieldName][month] = (avgLongMTMPrice - avgLongPrice) * netDays;
                                        }
                                    }

                                    if (chkCalculateSums.Checked) //have to chck again this process, temporary fix on CopyMTMCustoSummarys
                                        mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                }

                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                decimal avgLongMTMPrice = 0;
                                decimal avgLongPrice = 0;
                                decimal avgShortMTMPrice = 0;
                                decimal avgShortPrice = 0;
                                var totalLongDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL LONG DAYS:"][month]);
                                var totalShortDays = Math.Abs((decimal)posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) TOTAL SHORT DAYS:"][month]);
                                var netDays = (decimal)posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) NET DAYS:"][month];
                                //foreach (var indx in indexesPhysicalFinancial)
                                //{
                                //    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                //    {
                                //        avgLongMTMPrice += (mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG MTM PRICE:"][month] ?? 0);
                                //        avgLongPrice += (((mtmCalculationsPerIndex[indx].ContainsKey("(incl. OPTIONS) AVG LONG DEAL PRICE:"))?mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month]:0) ?? 0);
                                //        avgShortMTMPrice += (mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT MTM PRICE:"][month] ?? 0);
                                //        avgShortPrice += (((mtmCalculationsPerIndex[indx].ContainsKey("(incl. OPTIONS) AVG SHORT DEAL PRICE:"))?mtmCalculationsPerIndex[indx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month]:0) ?? 0);
                                //    }                                    
                                //}
                                avgLongMTMPrice = (mtmCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG LONG MTM PRICE:"][month] ?? 0);
                                avgLongPrice = (posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month]  ?? 0);
                                avgShortMTMPrice = (mtmCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG SHORT MTM PRICE:"][month] ?? 0);
                                avgShortPrice = (posCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month]  ?? 0);
                                if (netDays < 0)
                                {
                                    result = (avgShortPrice - avgShortMTMPrice) * Math.Abs(netDays);
                                }
                                else
                                {
                                    result = (avgLongMTMPrice - avgLongPrice) * netDays;
                                }
                                //decimal indexValue = GetIndexValueWithAggregation(e, totalIndx, month);
                                //avgLongPrice = (mtmCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG LONG DEAL PRICE:"][month] ?? 0);
                                //avgShortPrice = (mtmCalculationsPerIndex[totalIndx]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"][month] ?? 0);
                                //if (netDays > 0)
                                //    result = (indexValue - avgLongPrice) * netDays;
                                //else if (netDays <= 0)
                                //    result = (indexValue - avgShortPrice) * netDays;
                                //if(result.HasValue && result.Value != 0)
                                //{
                                //}
                                if(result  == 15738)
                                {
                                }
                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = result;
                            }
                            e.TotalValue = "N/A";                            
                        }
                        break;
                    #endregion

                    #region Total Cover Option - (incl. OPTIONS)  NET PAPER DAYS:
                    case ReportResultEnum.NetPaperDaysOption:
                        fieldName = "(incl. OPTIONS) NET PAPER DAYS:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.NetPaperDaysOption;
                            
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                //mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + "Total"].Add(l, new Dictionary<string, decimal?>());
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                mtmCalculationsPerIndex[totalIndx][fieldName][month] = totalNetPaperDaysOptions[totalIndx];
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region Total Cover Option - (incl. OPTIONS) COVER (%):
                    case ReportResultEnum.TotalCoverOption:
                        fieldName = "(incl. OPTIONS) COVER (%):";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            ((GridSummaryItem)e.Item).Tag = ReportResultEnum.TotalCoverOption;

                            //foreach (var indx in indexesPhysicalFinancial)
                            //{
                            //    if (mtmCalculationsPerIndex[indx].Any(a => a.Key == fieldName))
                            //    {
                            //        AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);

                            //        var netPaperDays = posCalculationsPerIndex[indx]["NET PAPER DAYS:"][month];
                            //        var tcInDays = posCalculationsPerIndex[indx]["TC IN DAYS:"][month];

                            //        if (tcInDays != 0)
                            //        {
                            //            //mtmCalculationsPerIndex[indx][fieldName ][month] = (((-1) * netPaperDays) / tcInDays) * 100;
                            //            mtmCalculationsPerIndex[indx][fieldName][month] = Math.Abs((decimal)(netPaperDays / tcInDays) * 100);

                            //            if (chkCalculateSums.Checked)
                            //                mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                            //        }
                            //        else
                            //            mtmCalculationsPerIndex[indx][fieldName][month] = null;
                            //    }
                            //}
                            foreach (var totalIndx in indexesMTMPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);

                                var netPaperDays = mtmCalculationsPerIndex[totalIndx]["(incl. OPTIONS) NET PAPER DAYS:"][month];
                                var tcOutDays = posCalculationsPerIndex[totalIndx]["TC OUT DAYS:"][month];
                                var tcInDays = posCalculationsPerIndex[totalIndx]["TC IN DAYS:"][month];

                                if (tcInDays != 0)
                                {
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = Math.Abs((decimal)((netPaperDays + tcOutDays) / tcInDays) * 100);

                                    if (chkCalculateSums.Checked)
                                        mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[totalIndx][fieldName][month];
                                }
                                else
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = null;
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgShortMtmPrice - (incl. OPTIONS) AVG LONG MTM PRICE:
                    case ReportResultEnum.AvgLongMtmPriceOption:
                        fieldName = "(incl. OPTIONS) AVG LONG MTM PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                                if (indx.Contains("Physical"))
                                {
                                    if (totalMTMLongPricePositionDays[indx] != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalMTMLongPrice[indx] / totalMTMLongPricePositionDays[indx];
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                                else
                                {
                                    mtmCalculationsPerIndex[indx][fieldName][month] = mtmCalculationsPerIndex[indx][indx.Substring(0, indx.IndexOf("-"))][month];
                                }
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        if (indx.Contains("Physical"))
                                        {
                                            if (totalMTMLongPricePositionDays[indx] != 0)
                                                result = totalMTMLongPrice[indx] / totalMTMLongPricePositionDays[indx];
                                            
                                        }
                                        else
                                        {
                                            result = mtmCalculationsPerIndex[indx][indx.Substring(0, indx.IndexOf("-"))][month];
                                        }
                                    }
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = result;//(mtmCalculationsPerIndex[totalIndx][fieldName][month] ?? 0) + result;
                                }
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;                            
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #region AvgShortMtmPrice  - (incl. OPTIONS) AVG SHORT MTM PRICE:
                    case ReportResultEnum.AvgShortMtmPriceOption:
                        fieldName = "(incl. OPTIONS) AVG SHORT MTM PRICE:";
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue = fieldName;
                        }
                        else
                        {
                            foreach (var indx in indexesPhysicalFinancial)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indx, fieldName, month);
                                /* if (totalMTMShortPricePositionDays[indx] != 0)
                                     mtmCalculationsPerIndex[indx][fieldName ][month] = totalMTMShortPrice[indx] /totalMTMShortPricePositionDays[indx];
                                 else
                                     mtmCalculationsPerIndex[indx][fieldName ][month] = null;
                                     */
                                if (indx.Contains("Physical"))
                                {
                                    if (totalMTMShortPricePositionDays[indx] != 0)
                                        mtmCalculationsPerIndex[indx][fieldName][month] = totalMTMShortPrice[indx] / totalMTMShortPricePositionDays[indx];
                                    else
                                        mtmCalculationsPerIndex[indx][fieldName][month] = null;
                                }
                                else
                                {
                                    var fc = indx.Substring(0, indx.IndexOf("-"));
                                    mtmCalculationsPerIndex[indx][fieldName][month] = mtmCalculationsPerIndex[indx][fc][month];
                                }
                                //var fc = indx.Substring(0, indx.IndexOf("-"));
                                //mtmCalculationsPerIndex[indx][fieldName ][month] = mtmCalculationsPerIndex[indx][fc][month];

                                //if (chkCalculateSums.Checked) //have to chck again this process, temporary fix on CopyMTMCustoSummarys
                                //{                               
                                //     mtmCalculationsPerIndex[indx][fieldName]["Total"] = (mtmCalculationsPerIndex[indx][fieldName]["Total"] ?? 0) + mtmCalculationsPerIndex[indx][fieldName][month];
                                //}
                            }
                            foreach (var totalIndx in indexesPhysicalFinancialTotal)
                            {
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, totalIndx, fieldName, month);
                                decimal? result = null;
                                foreach (var indx in indexesPhysicalFinancial)
                                {
                                    if (indx.Substring(0, indx.IndexOf("-")) == totalIndx.Substring(0, totalIndx.IndexOf("-")))
                                    {
                                        if (indx.Contains("Physical"))
                                        {
                                            if (totalMTMShortPricePositionDays[indx] != 0)
                                                result = totalMTMShortPrice[indx] / totalMTMShortPricePositionDays[indx];                                            
                                        }
                                        else
                                        {
                                            var fc = indx.Substring(0, indx.IndexOf("-"));
                                            result = mtmCalculationsPerIndex[indx][fc][month];
                                        }
                                    }
                                    mtmCalculationsPerIndex[totalIndx][fieldName][month] = result;//(mtmCalculationsPerIndex[totalIndx][fieldName][month] ?? 0) + result;
                                }
                                if (chkCalculateSums.Checked)
                                    mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] = (mtmCalculationsPerIndex[totalIndx][fieldName]["Total"] ?? 0) + result;                            
                            }
                            e.TotalValue = "N/A";
                        }
                        break;
                    #endregion

                    #endregion                  

                    #region IndexAvgRate
                    case ReportResultEnum.IndexAverage:
                        long actualIndexId = Convert.ToInt32((((GridSummaryItem)e.Item).Tag).ToString().Replace(IndexAverageConst + "_", ""));
                        //In case of trades with multiple indexes per market, we have to include not default index values too (eg 10TC_S, 5TC_S)
                        var trade = _tradeInformation.First(a => a.TradeInfo.MTMFwdIndexId == actualIndexId);
                        List<long> indexesToCheck = new List<long>();
                        if (trade.ForwardCurve != trade.ForwardCurveForReports)
                        {
                            var tradeWithMainIndexId = _tradeInformation.FirstOrDefault(a => a.ForwardCurve == trade.ForwardCurveForReports);
                            if (tradeWithMainIndexId != null)
                            {
                                indexesToCheck.Add(tradeWithMainIndexId.TradeInfo.MTMFwdIndexId);
                            }
                            else
                            {
                                indexesToCheck.Add(actualIndexId);
                            }
                        }
                        else
                        {
                            indexesToCheck.Add(actualIndexId);
                        }
                        
                        var indexPhyFin = trade.GroupedForwardCurve + "-" + trade.PhysicalFinancial;
                        if (((GridSummaryItem)e.Item).FieldName == "PhysicalFinancial")
                        {
                            e.TotalValue =
                                _tradeInformation.First(a => a.TradeInfo.MTMFwdIndexId == actualIndexId).TradeInfo.
                                    MTMFwdIndexName;
                        }
                        else
                        {
                            var otherPhyFin = string.Empty;
                            var checkOtherPhyFin = false;
                            if (indexPhyFin.Contains("Physical"))
                                otherPhyFin = "Financial";
                            else
                                otherPhyFin = "Physical";

                            if (mtmCalculationsPerIndex.ContainsKey(trade.GroupedForwardCurve + "-" + otherPhyFin))
                            {
                                checkOtherPhyFin = true;
                            }                            
                            if (!mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve].ContainsKey(month))
                            {
                                mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve].Add(month, new decimal?());
                                AddDictionFieldsMonthTotal(mtmCalculationsPerIndex, indexPhyFin.Substring(0, indexPhyFin.IndexOf("-")) + "-Total", trade.GroupedForwardCurve, month);
                                //mtmCalculationsPerIndex[indexPhyFin.Substring(0, indexPhyFin.IndexOf("-")) + "-Total"][trade.GroupedForwardCurve].Add(month, new decimal?());

                                if (checkOtherPhyFin && !mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + otherPhyFin][trade.GroupedForwardCurve].ContainsKey(month))
                                    mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + otherPhyFin][trade.GroupedForwardCurve].Add(month, new decimal?());
                            }

                            string date = view.Columns[((GridSummaryItem)e.Item).FieldName].Tag.ToString();
                            DateTime periodFrom = new DateTime();
                            DateTime periodTo = new DateTime();

                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                //"MMM-yyyy", new CultureInfo("en-GB")
                                DateTime dateCol = Convert.ToDateTime(date);

                                periodFrom = new DateTime(dateCol.Year, dateCol.Month, 1);
                                periodTo = new DateTime(dateCol.Year, dateCol.Month, periodFrom.AddMonths(1).AddDays(-1).Day);
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                FirstLastDaysOfQuarter(month, out periodFrom, out periodTo);
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                periodFrom = new DateTime(Convert.ToInt32(month.Substring(4)), 1, 1);
                                periodTo = new DateTime(Convert.ToInt32(month.Substring(4)), 12, 31);
                            }

                            if (periodTo < _curveDate || (periodTo > _curveDate && periodTo < DateTime.Now.Date))//Use spot values
                            {
                                if (spotIndexesValues.Any(a => indexesToCheck.Contains(a.Key)))
                                {
                                    var spotValuesToAverage = new List<decimal>();

                                    foreach (long indexId in indexesToCheck)
                                    {
                                        if (spotIndexesValues.Any(a => a.Key == indexId))
                                        {
                                            var valuesDict = spotIndexesValues.Single(a => a.Key == indexId).Value;
                                            if (valuesDict.Any(a => a.Key >= periodFrom && a.Key <= periodTo))
                                            {
                                                spotValuesToAverage.AddRange(valuesDict.Where(a => a.Key >= periodFrom && a.Key <= periodTo).Select(a => a.Value));
                                            }
                                        }
                                    }
                                    mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month] = spotValuesToAverage.Count > 0 ? (decimal?)spotValuesToAverage.Average() : null;
                                    mtmCalculationsPerIndex[indexPhyFin.Substring(0, indexPhyFin.IndexOf("-")) + "-Total"][trade.GroupedForwardCurve][month] = mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month];

                                    if (checkOtherPhyFin)
                                    {
                                        mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + otherPhyFin][trade.GroupedForwardCurve][month] = spotValuesToAverage.Count > 0 ? (decimal?)spotValuesToAverage.Average() : null;
                                    }

                                    if (!IndexMTMLong.ContainsKey(indexPhyFin))
                                        IndexMTMLong.Add(indexPhyFin, new Dictionary<string, decimal?>());
                                    if (!IndexMTMLong[indexPhyFin].ContainsKey(month))
                                        IndexMTMLong[indexPhyFin].Add(month, mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month]);
                                    else
                                        IndexMTMLong[indexPhyFin][month] = mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month];
                                }
                            }
                            else
                            {
                                if (forwardIndexesValues.Any(b => indexesToCheck.Contains(b.Key)))
                                {
                                    var forwardValuesToAverage = new List<decimal>();

                                    foreach (long indexId in indexesToCheck)
                                    {
                                        if (forwardIndexesValues.Any(a => a.Key == indexId))
                                        {
                                            var valuesDict = forwardIndexesValues.Single(a => a.Key == indexId).Value;
                                            if (valuesDict.Any(a => a.Key >= periodFrom && a.Key <= periodTo))
                                            {
                                                forwardValuesToAverage.AddRange(valuesDict.Where(a => a.Key >= periodFrom && a.Key <= periodTo).Select(a => a.Value));
                                            }
                                        }
                                    }
                                    mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month] = forwardValuesToAverage.Count > 0 ? (decimal?)forwardValuesToAverage.Average() : null;
                                    mtmCalculationsPerIndex[indexPhyFin.Substring(0, indexPhyFin.IndexOf("-")) + "-Total"][trade.GroupedForwardCurve][month] = forwardValuesToAverage.Count > 0 ? (decimal?)forwardValuesToAverage.Average() : null;
                                    if (!IndexMTMLong.ContainsKey(indexPhyFin))
                                        IndexMTMLong.Add(indexPhyFin, new Dictionary<string, decimal?>());

                                    if (!IndexMTMLong[indexPhyFin].ContainsKey(month))
                                        IndexMTMLong[indexPhyFin].Add(month, mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month]);
                                    else
                                        IndexMTMLong[indexPhyFin][month] = mtmCalculationsPerIndex[indexPhyFin][trade.GroupedForwardCurve][month];

                                    if (checkOtherPhyFin)
                                    {
                                        mtmCalculationsPerIndex[trade.GroupedForwardCurve + "-" + otherPhyFin][trade.GroupedForwardCurve][month] = forwardValuesToAverage.Count > 0 ? (decimal?)forwardValuesToAverage.Average() : null;
                                    }
                                }
                            }
                        }
                        break;
                        #endregion
                }
            }
        }

        private void GridMTMResultsViewCustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            if (e.Info.SummaryItem.Tag != null && (e.Info.SummaryItem.Tag.GetType() == typeof(ReportResultEnum) &&
                ((ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.IndexEqOwnersRate
                || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.IndexEqCharterersRate
                 //|| (ReportResultEnum) e.Info.SummaryItem.Tag == ReportResultEnum.EffEarningsRate
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.EffCharterersEarningsRate
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.EffOwnersEarningsRate
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.Unrealized
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.LockedIn
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.BreakevenPrice
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgLongMtmPrice
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgShortMtmPrice
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AdjOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgLongPrice
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgShortPrice
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.CoverAvgRate

                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.LockedInOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.UnrealizedOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalLongDaysOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.TotalShortDaysOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.NetDaysOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.CoverOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgLongPriceOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgShortPriceOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgShortMtmPriceOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.AvgLongMtmPriceOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.CoverAvgRateOption
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.BreakevenPriceOption)
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.Commission
                 || (ReportResultEnum)e.Info.SummaryItem.Tag == ReportResultEnum.IndexEqOwnersRateOption
                 || e.Info.SummaryItem.Tag.ToString().Contains(IndexAverageConst)))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion

        private void btnCalculateClick(object sender, EventArgs e)
        {
            _tradeMTMResults.Clear();
            _tradeEmbeddedValues.Clear();
            _indexesValues.Clear();
            _tradePositionResults.Clear();
            _tradeMTMRatiosResults.Clear();

            _curveDate = dtpFilterCurveDate.DateTime.Date;

            if (cmbMarketSensitivityType.SelectedItem != null &&
                (cmbMarketSensitivityType.SelectedItem.ToString() != "None" && cmbMarketSensitivityType.SelectedItem.ToString() != "Stress")
                && (txtMarketSensitivityValue.EditValue == null || txtMarketSensitivityValue.Value == 0))
            {
                XtraMessageBox.Show(this,
                                    "Please provide a valid value for field 'Market Sensitivity Value'.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            BeginGeneratePosition();

            btnExport.Enabled = true;
        }

        private void BtnExportResultsClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            string strDateFrom = _periodFrom.Year + "_" + _periodFrom.Month + "_" + _periodFrom.Day;
            string strDateTo = _periodTo.Year + "_" + _periodTo.Month + "_" + _periodTo.Day;

            gridPositionResultsView.OptionsPrint.PrintHorzLines = false;
            gridPositionResultsView.OptionsPrint.PrintVertLines = false;
            gridPositionResultsView.OptionsPrint.AutoWidth = false;
            gridPositionResultsView.OptionsPrint.PrintFooter = false;

            gridMTMResultsView.OptionsPrint.PrintHorzLines = false;
            gridMTMResultsView.OptionsPrint.PrintVertLines = false;
            gridMTMResultsView.OptionsPrint.AutoWidth = false;
            gridMTMResultsView.OptionsPrint.PrintFooter = false;

            string fileName = "FreightMetrics_RatiosReport_From_" + strDateFrom + "_To_" + strDateTo;
            string sheetName = "Ratios Report";

            try
            {
                var grids = new List<GridControl>();

                grids.Add(gridTradePositionResults);
                grids.AddRange(positionRatiosGridControls.Values);
                grids.Add(gridTradeMTMResults);
                grids.AddRange(mtmRatiosGridControls.Values);

                //var getInitialMargin
                //List<GridControl> initialMarginGrids;
                //_initialMarginControl.GetGrids(out initialMarginGrids);
                //grids.AddRange(initialMarginGrids);

                var ps = new PrintingSystem();
                var compositeLink = new DevExpress.XtraPrintingLinks.CompositeLink();
                compositeLink.PrintingSystem = ps;
                foreach (GridControl grid in grids)
                {
                    var link = new PrintableComponentLink();
                    link.Component = grid;
                    compositeLink.Links.Add(link);
                }

                if (gridPositionResultsView.DataRowCount > 60000)
                {
                    compositeLink.ExportToXlsx(
                        Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName +
                        ".xlsx",
                        new XlsxExportOptions
                        {
                            ExportHyperlinks = false,
                            ExportMode = XlsxExportMode.SingleFile,
                            SheetName = sheetName,
                            ShowGridLines = false,
                            TextExportMode = TextExportMode.Value
                        });
                }
                else
                {
                    compositeLink.ExportToXls(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xls", new XlsExportOptions { ExportHyperlinks = false, ExportMode = XlsExportMode.SingleFile, SheetName = sheetName, ShowGridLines = false, TextExportMode = TextExportMode.Value });
                }

                Cursor = Cursors.Default;

                XtraMessageBox.Show(this, "File with name: '" + fileName + "' exported to Desktop successfully."
                                    ,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this, string.Format(Strings.Failed_to_write_file_to_Desktop__Details___0_, exc.Message)
                                    ,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
            //DialogResult = DialogResult.OK;
            //if (OnDataSaved != null)
            //    OnDataSaved(this, true);
        }

        #endregion

        #region Proxy Calls

        private void BeginGeneratePosition()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);


            RatioReportPL = true;

            try
            {
                SessionRegistry.Client.BeginGeneratePositionExtraTradeTypeOptions(dtpFilterCurveDate.DateTime.Date,
                                                            _viewTradesGeneralInfo.periodFrom.Date,
                                                            _viewTradesGeneralInfo.periodTo.Date,
                                                            (string)cmbPositionMethod.SelectedItem,
                                                            (string)cmbMarketSensitivityType.SelectedItem,
                                                            txtMarketSensitivityValue.Value,
                                                            chkUseSpotValues.Checked,
                                                            chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString(),
                                                            _viewTradesGeneralInfo.tradeIdentifiers,
                                                             EndGeneratePosition, null);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGeneratePosition(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGeneratePosition;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<string, Dictionary<DateTime, decimal>> tradePositions;
            Dictionary<string, Dictionary<DateTime, decimal>> tradePositionsWithoutWeights;
            string errorMessage = "";
            try
            {
                result = SessionRegistry.Client.EndGeneratePositionExtraTradeTypeOptions(out tradePositions, out tradePositionsWithoutWeights, out errorMessage, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //_formState = FormStateEnum.AfterSearch;
            //SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? Strings.
                                              There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                //XtraMessageBox.Show(this,
                //                    Strings.
                //                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                //                    Strings.Freight_Metrics,
                //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                _tradePositionResults = tradePositions;
                _tradeCalendarDaysResults = tradePositionsWithoutWeights;

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                //_initialMarginControl.LoadPositionValues(tradePositions);

                BeginGenerateMarkToMarket();
                //btnMarkToMarket.PerformClick();
            }
        }

        private void BeginGenerateMarkToMarket()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGenerateMarkToMarketExtraTradeTypeOptions(_viewTradesGeneralInfo.positionDate,
                                                                 dtpFilterCurveDate.DateTime.Date,
                                                                 _viewTradesGeneralInfo.periodFrom,
                                                                 _viewTradesGeneralInfo.periodTo,
                                                                 (string)cmbPositionMethod.SelectedItem,
                                                                 (string)cmbMarketSensitivityType.SelectedItem,
                                                                 txtMarketSensitivityValue.Value,
                                                                 chkUseSpotValues.Checked, chkOptionPremium.Checked,
                                                                 chkOptionEmbeddedValue.Checked, chkOptionNullify.Checked,
                                                                 RatioReportPL ? "P&L" : RatioReportCashFlow ? "CASH FLOW" : RatioReportCombined ? "COMBINED" : (string)rdgMTMResultsType.EditValue,
                                                                 chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString(),
                                                                 _viewTradesGeneralInfo.tradeIdentifiers,
                                                                 new Dictionary<long, string>(),
                                                                 EndGenerateMarkToMarketExtraTradeTypeOptions, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGenerateMarkToMarketExtraTradeTypeOptions(IAsyncResult ar) {
            EndGenerateMarkToMarket(ar);
        }
        private void EndGenerateMarkToMarket(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGenerateMarkToMarket;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<string, Dictionary<DateTime, decimal>> tradeMTMs;
            Dictionary<string, decimal> tradeEmbeddedValues;
            Dictionary<DateTime, Dictionary<string, decimal>> indexesValues;
            string errorMessage = "";
            try
            {
                result = SessionRegistry.Client.EndGenerateMarkToMarketExtraTradeTypeOptions(out tradeMTMs, out tradeEmbeddedValues, out indexesValues, out errorMessage, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //_formState = FormStateEnum.AfterExecution;
            //SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? Strings.
                                              There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //XtraMessageBox.Show(this,
                //                    Strings.
                //                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                //                    Strings.Freight_Metrics,
                //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    "Failed to find BFA index values for the selected trade sign dates.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                //_formState = FormStateEnum.AfterExecution;
                //SetGuiControls();
            }
            else if (result == 0) //everything is OK
            {
                _tradeMTMResults = tradeMTMs;
                _tradeEmbeddedValues = tradeEmbeddedValues;
                _indexesValues = indexesValues;

                //if (!RatioReportPL && !RatioReportCashFlow)
                //    CreateResultGridColumns(tradeMTMs, tradeEmbeddedValues, "MTM");

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                RatioReportPL = false;
                RatioReportCashFlow = true;
                RatioReportCombined = false;
                _tradeMTMRatiosResults.Add("P&L", tradeMTMs);
                BeginGenerateMarkToMarket2();

            }
        }
        private void BeginGenerateMarkToMarket2()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGenerateMarkToMarketExtraTradeTypeOptions(_viewTradesGeneralInfo.positionDate,
                                                                 dtpFilterCurveDate.DateTime.Date,
                                                                 _viewTradesGeneralInfo.periodFrom,
                                                                 _viewTradesGeneralInfo.periodTo,
                                                                 (string)cmbPositionMethod.SelectedItem,
                                                                 (string)cmbMarketSensitivityType.SelectedItem,
                                                                 txtMarketSensitivityValue.Value,
                                                                 chkUseSpotValues.Checked, chkOptionPremium.Checked,
                                                                 chkOptionEmbeddedValue.Checked, chkOptionNullify.Checked,
                                                                 RatioReportPL ? "P&L" : RatioReportCashFlow ? "CASH FLOW" : RatioReportCombined ? "COMBINED" : (string)rdgMTMResultsType.EditValue,
                                                                 chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString(),
                                                                 _viewTradesGeneralInfo.tradeIdentifiers,
                                                                 new Dictionary<long, string>(),
                                                                 EndGenerateMarkToMarket2, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGenerateMarkToMarket2(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGenerateMarkToMarket2;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<string, Dictionary<DateTime, decimal>> tradeMTMs;
            Dictionary<string, decimal> tradeEmbeddedValues;
            Dictionary<DateTime, Dictionary<string, decimal>> indexesValues;
            string errorMessage = "";

            try
            {
                result = SessionRegistry.Client.EndGenerateMarkToMarketExtraTradeTypeOptions(out tradeMTMs, out tradeEmbeddedValues, out indexesValues, out errorMessage, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //_formState = FormStateEnum.AfterSearch;
            //SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? Strings.
                                              There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //XtraMessageBox.Show(this,
                //                    Strings.
                //                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                //                    Strings.Freight_Metrics,
                //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    "Failed to find BFA index values for the selected trade sign dates.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                _tradeMTMResults = tradeMTMs;
                _tradeEmbeddedValues = tradeEmbeddedValues;
                _indexesValues = indexesValues;

                //if (!RatioReportPL && !RatioReportCashFlow)
                //    CreateResultGridColumns(tradeMTMs, tradeEmbeddedValues, "MTM");

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                RatioReportPL = false;
                RatioReportCashFlow = false;
                RatioReportCombined = true;

                _tradeMTMRatiosResults.Add("CASH FLOW", tradeMTMs);
                BeginGenerateMarkToMarket3();
                //if (RatiosReportEvent != null)
                //    RatiosReportEvent((List<TradeInformation>)gridTradeDetails.DataSource, _tradePositionResults,
                //                      _tradeMTMRatiosResults, dtpFilterPeriodFrom.DateTime.Date,
                //                      dtpFilterPeriodTo.DateTime.Date, dtpFilterCurveDate.DateTime.Date);

                //GetAverageIndexesValues();

            }
        }

        private void BeginGenerateMarkToMarket3()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);
            var tradeIdsByPhysicalFinancialType = _viewTradesGeneralInfo.tradeInformations.Select(ti => ti.Trade.Id).ToList().Select((k, i) => new { k, v = _viewTradesGeneralInfo.tradeInformations.Select(ti => ti.PhysicalFinancial).ToList()[i] })
              .ToDictionary(x => x.k, x => x.v);
            try
            {
                SessionRegistry.Client.BeginGenerateMarkToMarketExtraTradeTypeOptions(_viewTradesGeneralInfo.positionDate,
                                                                 dtpFilterCurveDate.DateTime.Date,
                                                                 _viewTradesGeneralInfo.periodFrom,
                                                                 _viewTradesGeneralInfo.periodTo,
                                                                 (string)cmbPositionMethod.SelectedItem,
                                                                 (string)cmbMarketSensitivityType.SelectedItem,
                                                                 txtMarketSensitivityValue.Value,
                                                                 chkUseSpotValues.Checked, chkOptionPremium.Checked,
                                                                 chkOptionEmbeddedValue.Checked, chkOptionNullify.Checked,
                                                                 RatioReportPL ? "P&L" : RatioReportCashFlow ? "CASH FLOW" : RatioReportCombined ? "COMBINED" : (string)rdgMTMResultsType.EditValue,
                                                                 chkCmbBxEdtTradeType.Properties.GetCheckedItems().ToString(),
                                                                 _viewTradesGeneralInfo.tradeIdentifiers,
                                                                 tradeIdsByPhysicalFinancialType,
                                                                 EndGenerateMarkToMarket3, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGenerateMarkToMarket3(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGenerateMarkToMarket3;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<string, Dictionary<DateTime, decimal>> tradeMTMs;
            Dictionary<string, decimal> tradeEmbeddedValues;
            Dictionary<DateTime, Dictionary<string, decimal>> indexesValues;
            string errorMessage = "";

            try
            {
                result = SessionRegistry.Client.EndGenerateMarkToMarketExtraTradeTypeOptions(out tradeMTMs, out tradeEmbeddedValues, out indexesValues, out errorMessage, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //_formState = FormStateEnum.AfterSearch;
            //SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? Strings.
                                              There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //XtraMessageBox.Show(this,
                //                    Strings.
                //                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                //                    Strings.Freight_Metrics,
                //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    "Failed to find BFA index values for the selected trade sign dates.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                _tradeMTMResults = tradeMTMs;
                _tradeEmbeddedValues = tradeEmbeddedValues;
                _indexesValues = indexesValues;

                //if (!RatioReportPL && !RatioReportCashFlow)
                //    CreateResultGridColumns(tradeMTMs, tradeEmbeddedValues, "MTM");
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                RatioReportPL = false;
                RatioReportCashFlow = false;
                RatioReportCombined = false;

                _tradeMTMRatiosResults.Add("COMBINED", tradeMTMs);

                GetAverageIndexesValues();

            }
        }

        private void GetAverageIndexesValues()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                indexesIds =
                        (_tradeInformation.Where(a => a.Trade.Type == TradeTypeEnum.FFA).Select(
                            a => a.TradeInfo.MTMFwdIndexId).ToList().Union(
                                _tradeInformation.Where(a => a.Trade.Type == TradeTypeEnum.TC).Select(
                                    a => a.TradeInfo.MTMFwdIndexId).ToList()).Union(
                                        _tradeInformation.Where(a => a.Trade.Type == TradeTypeEnum.Option).Select(
                                            a => a.TradeInfo.MTMFwdIndexId).ToList()).Union(
                                                _tradeInformation.Where(a => a.Trade.Type == TradeTypeEnum.Cargo).Select
                                                    (a => a.TradeInfo.MTMFwdIndexId).ToList())).Distinct().ToList();

                SessionRegistry.Client.BeginGetAverageIndexesValues(indexesIds, _curveDate, _periodFrom, _periodTo, EndGetAverageIndexesValues, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetAverageIndexesValues(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetAverageIndexesValues;
                Invoke(action, ar);
                return;
            }

            int? result;

            try
            {
                result = SessionRegistry.Client.EndGetAverageIndexesValues(out forwardIndexesValues, out spotIndexesValues, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                LoadRatios();

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        #endregion



        #region Event

        private void tbControl_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            if (e.Page.Text == "Initial Margin")
            {
                SetInitialMarginParameters();
            }
            else
            {
                SetDefaultsParameters();
            }
        }

        private void SetInitialMarginParameters()
        {
            cmbPositionMethod.Text = "Static";
            chkCalculateSums.Checked = true;
        }

        private void SetDefaultsParameters()
        {
            cmbPositionMethod.Text = "Dynamic";
            //chkCalculateSums.Checked = false;
        }

        private void Properties_BeforePopup(object sender, EventArgs e)
        {
            CheckedListBoxControl listBox = ((sender as IPopupControl).PopupWindow as PopupContainerForm).Controls.OfType<PopupContainerControl>().First().Controls.OfType<CheckedListBoxControl>().First();
            listBox.ItemCheck -= ListBox_ItemCheck;
            listBox.ItemCheck += ListBox_ItemCheck;
        }

        private void ListBox_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            CheckedListBoxControl control = sender as CheckedListBoxControl;
            Point point = (control.Parent as PopupContainerControl).PointToClient(MousePosition);
            CheckedItemInfo t = (control.GetViewInfo() as CheckedListBoxViewInfo).GetItemInfoByPoint(point) as CheckedItemInfo;
            if (t != null)
            {
                if (!t.CheckArgs.Bounds.Contains(new Point(point.X, t.CheckArgs.Bounds.Y)))
                    chkCmbBxEdtTradeType.ClosePopup();
            }

        }

        private void RdgResultsAggregationTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeMTMResults.Tag != null && gridTradePositionResults.Tag.ToString() != null)
                LoadRatios();
        }

        #endregion

        private void chkOptionPremium_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CreateAggregationColumns(ref DateTime date, ref int quarterAdded, ref int calendarAdded, ref int gridViewColumnindex, ref GridColumn column)
        {
            column = null;
            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
            {
                column = AddColumn(
                date.ToString("MMM-yyyy", new CultureInfo("en-GB")),
                date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                UnboundColumnType.Decimal,
                Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
            }
            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
            {
                int intQuarter = ((date.Month - 1) / 3) + 1;
                if (intQuarter != quarterAdded)
                {
                    quarterAdded = intQuarter;
                    string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                    column = AddColumn(
                        strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")),
                        strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                        UnboundColumnType.Decimal, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                }
            }
            else  // Calendar
            {
                int intCalendar = date.Year;
                if (intCalendar != calendarAdded)
                {
                    calendarAdded = intCalendar;

                    column = AddColumn(
                        "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")),
                        "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                        UnboundColumnType.Decimal, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                }
            }
        }
        private void CreateAggregationSummaryColumns(ref DateTime date, ref int quarterAdded, ref int calendarAdded, ref int gridViewColumnindex, ref GridColumn column)
        {
            column = null;
            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
            {
                column = AddColumn(
                date.ToString("MMM-yyyy", new CultureInfo("en-GB")) + "_R",
                date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                UnboundColumnType.Decimal,
                Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
            }
            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
            {
                int intQuarter = ((date.Month - 1) / 3) + 1;
                if (intQuarter != quarterAdded)
                {
                    quarterAdded = intQuarter;
                    string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                    column = AddColumn(
                        strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")) + "_R",
                        strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                        UnboundColumnType.Decimal, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                }
            }
            else  // Calendar
            {
                int intCalendar = date.Year;
                if (intCalendar != calendarAdded)
                {
                    calendarAdded = intCalendar;

                    column = AddColumn(
                        "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")) + "_R",
                        "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                        UnboundColumnType.Decimal, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                }
            }
        }
        private int GetDaysOfQuarter(string quarter)
        {
            TimeSpan days = new TimeSpan();
            string[] quarterYear = quarter.Split('-');
            DateTime dateTimeStart = new DateTime(), dateTimeEnd = DateTime.Now;
            //string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
            switch (quarterYear[0].Substring(1))
            {
                case "1":
                {
                    dateTimeStart = Convert.ToDateTime(quarterYear[1] + "-01-01");
                    dateTimeEnd = Convert.ToDateTime(quarterYear[1] + "-03-31");
                    break;
                }
                case "2":
                {
                    dateTimeStart = Convert.ToDateTime(quarterYear[1] + "-04-01");
                    dateTimeEnd = Convert.ToDateTime(quarterYear[1] + "-06-30");
                    break;
                }
                case "3":
                {
                    dateTimeStart = Convert.ToDateTime(quarterYear[1] + "-07-01");
                    dateTimeEnd = Convert.ToDateTime(quarterYear[1] + "-09-30");
                    break;
                }
                case "4":
                {
                    dateTimeStart = Convert.ToDateTime(quarterYear[1] + "-10-01");
                    dateTimeEnd = Convert.ToDateTime(quarterYear[1] + "-12-31");
                    break;
                }
            }
            days = dateTimeEnd - dateTimeStart;
            return days.Days;
        }
        private DateTime FirstDayOfQuarter(DateTime DateIn)
        {
            // Calculate first day of DateIn quarter,
            // with quarters starting at the beginning of Jan/Apr/Jul/Oct
            int intQuarterNum = (DateIn.Month - 1) / 3 + 1;
            return new DateTime(DateIn.Year, 3 * intQuarterNum - 2, 1);
        }
        private DateTime LastDayOfQuarter(DateTime DateIn)
        {
            // Calculate last day of DateIn quarter,
            // with quarters ending at the end of Mar/Jun/Sep/Dec
            int intQuarterNum = (DateIn.Month - 1) / 3 + 1;
            return new DateTime(DateIn.Year, ((3 * intQuarterNum) < 12) ? (3 * intQuarterNum) + 1 : (3 * intQuarterNum), 1);
        }
        private void FirstLastDaysOfQuarter(string quarter, out DateTime firstDay, out DateTime lastDay)
        {
            string[] quarterYear = quarter.Split('-');
            firstDay = new DateTime();
            lastDay = new DateTime();
            //string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
            switch (quarterYear[0].Substring(1))
            {
                case "1":
                {
                    firstDay = Convert.ToDateTime(quarterYear[1] + "-01-01");
                    lastDay = Convert.ToDateTime(quarterYear[1] + "-03-31");
                    break;
                }
                case "2":
                {
                    firstDay = Convert.ToDateTime(quarterYear[1] + "-04-01");
                    lastDay = Convert.ToDateTime(quarterYear[1] + "-06-30");
                    break;
                }
                case "3":
                {
                    firstDay = Convert.ToDateTime(quarterYear[1] + "-07-01");
                    lastDay = Convert.ToDateTime(quarterYear[1] + "-09-30");
                    break;
                }
                case "4":
                {
                    firstDay = Convert.ToDateTime(quarterYear[1] + "-10-01");
                    lastDay = Convert.ToDateTime(quarterYear[1] + "-12-31");
                    break;
                }
            }
        }
        private int GetDaysOfYear(DateTime dateTime) {
            return DateTime.IsLeapYear(dateTime.Year) ? 365 : 366;
        }
        private void AddDictionFieldsMonthTotal(Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>> dictionary, string indx, string fieldName, string month) {
            try
            {                
                if (!dictionary[indx][fieldName].ContainsKey(month))
                    dictionary[indx][fieldName].Add(month, new decimal?());
                if (chkCalculateSums.Checked && !dictionary[indx][fieldName].ContainsKey("Total"))
                    dictionary[indx][fieldName].Add("Total", new decimal?());
            }
            catch {
            }
        }
        private decimal GetIndexValueWithAggregation(CustomSummaryEventArgs e, string indexType, string month) {
            string indx = indexType;
            decimal indexValue = 0;
            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
            {                
                if (_indexesValues[DateTime.Parse(month)].ContainsKey(indx.Substring(0, 6)))
                    indexValue = _indexesValues[DateTime.Parse(month)][indx.Substring(0, 6)];
                else
                    indexValue = 0;
            }
            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
            {
                string quarter = month;
                indexValue = _indexesValues.Where(a => {
                    int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                    string strQuarter = intQuarter == 1
                                            ? "Q1"
                                            : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                    return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;
                }).Any(a => a.Value.Count > 0) ? _indexesValues.Where(a => {
                    int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                    string strQuarter = intQuarter == 1
                                            ? "Q1"
                                            : intQuarter == 2
                                                ? "Q2"
                                                : intQuarter == 3 ? "Q3" : "Q4";
                    return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                            quarter;
                }).Where(a => a.Value.Count > 0 && a.Value.ContainsKey(indx.Substring(0, 6))).Select(a => a.Value[indx.Substring(0, 6)]).Average() : 0;
            }
            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
            {
                string calendar = ((GridSummaryItem)e.Item).Tag.ToString();
                if (_indexesValues.Any(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar && a.Value.Count > 0 && a.Value.ContainsKey(indx.Substring(0, 6))))
                {
                    indexValue = _indexesValues.Where(a =>
                             "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar &&
                             a.Value.Count > 0 && a.Value.ContainsKey(indx.Substring(0, 6))).Select(a => a.Value[indx.Substring(0, 6)]).
                            Average();
                }
                else
                    indexValue = 0;
            }
            return indexValue;
        }
        private decimal GetIndexValueWithAggregation(string indexType, string month)
        {
            string indx = indexType;
            decimal indexValue = 0;
            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
            {
                indexValue = _indexesValues[DateTime.Parse(month)][indx.Substring(0, 6)];
            }
            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
            {
                string quarter = month;
                indexValue = _indexesValues.Where(a => {
                    int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                    string strQuarter = intQuarter == 1
                                            ? "Q1"
                                            : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                    return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;
                }).Any(a => a.Value.Count > 0) ? _indexesValues.Where(a => {
                    int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                    string strQuarter = intQuarter == 1
                                            ? "Q1"
                                            : intQuarter == 2
                                                ? "Q2"
                                                : intQuarter == 3 ? "Q3" : "Q4";
                    return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;
                }).Where(a => a.Value.Count > 0 && a.Value.ContainsKey(indx.Substring(0, 6))).Select(a => a.Value[indx.Substring(0, 6)]).Average() : 0;
            }
            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
            {
                string calendar = month;//((GridSummaryItem)e.Item).Tag.ToString();
                if (_indexesValues.Any(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar && a.Value.Count > 0 && a.Value.ContainsKey(indx.Substring(0, 6))))
                {
                    indexValue = _indexesValues.Where(a =>
                             "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar &&
                             a.Value.Count > 0 && a.Value.ContainsKey(indx.Substring(0, 6))).Select(a => a.Value[indx.Substring(0, 6)]).
                            Average();
                }
                else
                    indexValue = 0;
            }
            return indexValue;
        }
        private string GetIndexValueTotal(string index, string fieldName) {
            List<DateTime> dates = _tradeMTMRatiosResults[PL].First().Value.Select(a => a.Key).ToList();
            string dateString = "";            
            decimal totalIndexValue = 0;
            foreach (DateTime date in dates)
            {                
                var fc = fieldName;
                
                dateString = GetColumnDescriptionBaseDatePeriod(date);

                if (mtmCalculationsPerIndex[index][fc].ContainsKey(dateString))
                {
                    var valueFromDictionary = mtmCalculationsPerIndex[index][fc][dateString];

                    if (valueFromDictionary != null)
                        totalIndexValue = totalIndexValue + Math.Round(Convert.ToDecimal(mtmCalculationsPerIndex[index][fc][dateString]), 3);
                }
            }
            return totalIndexValue != 0 ? totalIndexValue.ToString() : null;
        }
        private decimal? GenerateTotalsPosition(string index, string label, List<DateTime> dates)
        {
            decimal? total = null;

            switch (label) {
                case "AVG LONG DEAL PRICE:": {

                    var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);

                    if (sumLongDays != 0)
                    {                        
                        total = sumTotalAvgLongPrice[index] / sumLongDays;                        
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "AVG SHORT DEAL PRICE:":
                {
                    var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);

                    if (sumShortDays != 0)
                    {                        
                        total = sumTotalAvgShortPrice[index] / sumShortDays;
                    }
                    else
                    {
                        total = null;
                    }
                    
                    break;
                }
                case "TOTAL POSITION:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL POSITION:", dates);
                    break;
                }
                case "TC IN DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);
                    //if (index.Contains("Financial"))
                    //{
                    //    total = totalTcInDays[index];

                    //    string[] indexParts = index.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                    //    string sameIndexOfPhysical = indexParts[0] + "-Physical";

                    //    if (posCalculationsPerIndex.ContainsKey(sameIndexOfPhysical))
                    //    {
                    //        total = 0;
                    //        decimal? dc = 0;
                    //        foreach (DateTime date in dates)
                    //        {
                    //            if (posCalculationsPerIndex[sameIndexOfPhysical][label].ContainsKey(GetColumnDescriptionBaseDatePeriod(date)))
                    //            {
                    //                dc = totalTcInDays[index] + totalTcInDays[sameIndexOfPhysical];
                    //            }
                    //            total = total + dc;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);//sumTotalTcInDays[index];
                    //}
                    break;
                }
                case "TC OUT DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);
                    break;
                }
                
                case "NET PHYSICAL DAYS:":
                {
                    //total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PHYSICAL DAYS:", dates);
                    if (index.Contains("Financial"))
                    {
                        total = sumTotalTcInDays[index];

                        string[] indexParts = index.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                        string sameIndexOfPhysical = indexParts[0] + "-Physical";

                        if (posCalculationsPerIndex.ContainsKey(sameIndexOfPhysical))
                        {
                            total = 0;
                            decimal? dc = 0;
                            foreach (DateTime date in dates)
                            {
                                if (posCalculationsPerIndex[sameIndexOfPhysical]["TC IN DAYS:"].ContainsKey(GetColumnDescriptionBaseDatePeriod(date)))
                                {
                                    dc = sumTotalTcInDays[index] + posCalculationsPerIndex[sameIndexOfPhysical]["TC IN DAYS:"][GetColumnDescriptionBaseDatePeriod(date)];
                                }
                                total = total + dc;
                            }
                        }
                    }
                    else
                    {                        
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PHYSICAL DAYS:", dates);//sumTotalNetPhysicalDays[index];
                    }
                    break;
                }
                case "NET PAPER DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);
                    break;
                }
                case "COVER (%):":
                {
                    var netPaperDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);
                    var tcInDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);
                    var tcOutDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);

                    if (tcInDays != 0)
                    {
                        total = (-1) * ((tcOutDays + netPaperDays) / tcInDays);
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "COVER AVERAGE RATE:":
                {                    
                    var sumFfaAndOptionDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates) ;

                    if (sumFfaAndOptionDays != 0)
                    {
                        total = sumTotalPaperNotional[index] / sumFfaAndOptionDays;
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "NET DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates) + GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);
                    break;
                }
                case "SHORT/LONG COVER (%):":
                {                    
                    var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);
                    var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);

                    if (sumLongDays != 0)
                    {                        
                        total = Math.Abs((decimal)(sumShortDays / sumLongDays));
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "TOTAL FFA/OPTION DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);
                    var totalFFADaysValue = sumTotalFfaDays[index];
                    var totalOptionDaysValue = sumTotalOptionDays[index];

                    if (totalOptionDaysValue != 0)
                    {
                        total = totalFFADaysValue / totalOptionDaysValue;                       
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }

                case "FFA AVERAGE RATE:":
                {                    
                    var sumFfaDays = sumTotalFfaDays[index];

                    if (sumFfaDays != 0)
                    {
                        total = sumTotalFfaNotional[index] / sumFfaDays;
                    }
                    else
                    {
                        total = null;
                    }

                    break;
                }
                case "(adj) for options:":
                {
                    //var paperDays = Convert.ToDecimal(GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates));

                    //if (paperDays != 0)
                    //{
                    //    total = sumTotalPaperNotional[index] / paperDays;                        
                    //}
                    //else
                    //{
                        total = null;
                    //}
                    break;
                }
                case "(incl. OPTIONS) AVG LONG DEAL PRICE:":
                {                    
                    var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);

                    if (sumLongDays != 0)
                    {
                        total = sumTotalAvgLongOptionDays[index] / sumLongDays;
                        posCalculationsPerIndex[index]["(incl. OPTIONS) AVG LONG DEAL PRICE:"]["Total"] = total;
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "(incl. OPTIONS) AVG SHORT DEAL PRICE:":
                {                    
                    var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);

                    if (sumShortDays != 0)
                    {
                        total = sumTotalAvgShortOptionDays[index] / sumShortDays;
                        posCalculationsPerIndex[index]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"]["Total"] = total;
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "(incl. OPTIONS) TOTAL LONG DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);
                    break;
                }
                case "(incl. OPTIONS) TOTAL SHORT DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                    break;
                }
                case "(incl. OPTIONS) NET DAYS:":
                {                    
                    var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                    var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);

                    total = sumLongDays + sumShortDays;
                    break;
                }
                case "(incl. OPTIONS) SHORT/LONG COVER (%):":
                {                    
                    var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                    var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);

                    if (sumLongDays != 0)
                    {                        
                        total = Math.Abs((decimal)(sumShortDays / sumLongDays));                       
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "TOTAL LONG DAYS:":
                {
                    total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);                    
                    break;
                }
                case "TOTAL SHORT DAYS:":
                {
                    total =  GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);                    
                    break;
                }
                case "VESSEL TC COVER (%):":
                {                    
                    var tcInDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);
                    var tcOutDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);

                    if (tcInDays != 0)
                    {
                        total = Math.Abs(((decimal)tcOutDays / (decimal)tcInDays) * 100);
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }
                case "PAPER/PHYSICAL COVER (%):":
                {                    
                    var netPaperDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);
                    var netPhysicalDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PHYSICAL DAYS:", dates);

                    if (netPhysicalDays != 0)
                    {
                        total = (((-1) * netPaperDays) / netPhysicalDays) * 100;
                    }
                    else
                    {
                        total = null;
                    }
                    break;
                }               
            }
            return total;
        }
        private decimal? GenerateTotalsMTM(string index, string label, List<DateTime> dates)
        {            
            decimal? total = null;
            try
            {
                
                switch (label)
                {                   
                    case "LOCKED IN:":
                    {
                        var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates));
                        var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates));
                        var avgLongPrice = GetTotalAvgLongDealPrice(index, dates, "TOTAL LONG DAYS:", sumTotalAvgLongPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);
                        var avgShortPrice = GetTotalAvgShortDealPrice(index, dates, "TOTAL SHORT DAYS:", sumTotalAvgShortPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);

                        if (totalLongDays < totalShortDays)
                        {
                            total = totalLongDays * (avgShortPrice - avgLongPrice);
                        }
                        else
                        {
                            total = totalShortDays * (avgShortPrice - avgLongPrice);
                        }
                        break;
                    }
                    case "BREAKEVEN PRICE:":
                    {
                        decimal? lockedIn = null;//GetTotalLockedIn(index, dates, "TOTAL LONG DAYS:", "TOTAL SHORT DAYS:", sumTotalAvgLongPrice, sumTotalAvgShortPrice);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "LOCKED IN:", dates);
                        var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates));
                        var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates));
                        var avgLongPrice = GetTotalAvgLongDealPrice(index, dates, "TOTAL LONG DAYS:", sumTotalAvgLongPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);
                        var avgShortPrice = GetTotalAvgShortDealPrice(index, dates, "TOTAL SHORT DAYS:", sumTotalAvgShortPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);
                        var netDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET DAYS:", dates);

                        if (totalLongDays < totalShortDays)
                        {
                            lockedIn = totalLongDays * (avgShortPrice - avgLongPrice);
                        }
                        else
                        {
                            lockedIn = totalShortDays * (avgShortPrice - avgLongPrice);
                        }


                        if (index.Contains("Physical"))
                        {
                            if (totalLongDays < totalShortDays)
                                total = avgShortPrice - (lockedIn / netDays);
                            else if (totalLongDays > totalShortDays)
                                total = avgLongPrice - (lockedIn / netDays);
                            else
                                total = null;
                        }
                        else
                        {
                            if (netDays < 0)
                                total = ((netDays * avgShortPrice) - lockedIn) / netDays;
                            else if (netDays > 0)
                                total = ((netDays * avgLongPrice) - lockedIn) / netDays;
                            else
                                total = null;
                        }
                        break;
                    }
                    case "UNREALIZED:":
                    {
                        var lockedIn = GetTotalLockedIn(index, dates, "TOTAL LONG DAYS:", "TOTAL SHORT DAYS:", sumTotalAvgLongPrice, sumTotalAvgShortPrice);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "LOCKED IN:", dates);
                        var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates));
                        var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates));
                        var netDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET DAYS:", dates);

                        if (index.Contains("Physical"))
                        {
                            if (totalLongDays > totalShortDays)
                            {
                                //var avgLongMTMPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG LONG MTM PRICE:", dates);
                                var avgLongMTMPrice = GetTotalAVGLongMTMPrice(index, dates);
                                //var avgLongPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);
                                var avgLongPrice = GetTotalAvgLongDealPrice(index, dates, "TOTAL LONG DAYS:", sumTotalAvgLongPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);

                                total = (avgLongMTMPrice - avgLongPrice) * netDays;
                            }
                            else if (totalLongDays < totalShortDays)
                            {
                                //var avgShortMTMPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG SHORT MTM PRICE:", dates);
                                var avgShortMTMPrice = GetTotalAVGShortMTMPrice(index, dates);
                                //var avgShortPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);
                                var avgShortPrice = GetTotalAvgShortDealPrice(index, dates, "TOTAL SHORT DAYS:", sumTotalAvgLongPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);

                                total = (avgShortMTMPrice - avgShortPrice) * netDays;
                            }
                            else
                            {
                                total = null;
                            }
                        }
                        else
                        {                            
                            if (netDays < 0)
                            {
                                //var avgShortMTMPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG SHORT MTM PRICE:", dates);
                                var avgShortMTMPrice = GetTotalAVGShortMTMPrice(index, dates);
                                //var avgShortPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);
                                var avgShortPrice = GetTotalAvgShortDealPrice(index, dates, "TOTAL SHORT DAYS:", sumTotalAvgShortPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);
                                total = (avgShortPrice - avgShortMTMPrice) * Math.Abs(netDays);
                            }
                            else
                            {
                                //var avgLongMTMPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG LONG MTM PRICE:", dates);
                                var avgLongMTMPrice = GetTotalAVGLongMTMPrice(index, dates);
                                //var avgLongPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);
                                var avgLongPrice = GetTotalAvgLongDealPrice(index, dates, "TOTAL LONG DAYS:", sumTotalAvgShortPrice);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);
                                total = (avgLongMTMPrice - avgLongPrice) * netDays;
                            }
                        }
                        break;
                    }                    
                    case "INDEX EQUIVALENT CHARTERERS RATE:":
                    {

                        //var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);
                        var tcOutDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);

                        if (tcOutDays != 0)
                            total = sumTotalTcOutCashFlow[index] / tcOutDays;
                        else
                            total = null;

                        break;
                    }
                    //case "INDEX EQUIVALENT OWNERS RATE:":
                    //{
                    //    //var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);
                    //    var tcInDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);

                    //    if (tcInDays != 0)
                    //    {
                    //        //if (index.Contains("Physical"))
                    //        //{
                    //        //    total = sumTotalTcInCashFlow[index] / tcInDays;
                    //        //}
                    //        //else
                    //        //{
                    //        if (index == index.Substring(0, index.IndexOf("-")) + "-Total")
                    //        {
                    //            total = (sumTotalMTMIndexEqOwnersRateFin[index.Substring(0, index.IndexOf("-")) + "-Financial"] + sumTotalMTMIndexEqOwnersRateFin[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / tcInDays;
                    //        }
                    //        else if (!index.Contains("Physical"))
                    //        {
                    //            total = (sumTotalMTMIndexEqOwnersRateFin[index] + sumTotalMTMIndexEqOwnersRateFin[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / tcInDays;
                    //        }
                    //        else
                    //        {
                    //            total = (sumTotalMTMIndexEqOwnersRateFin[index]) / tcInDays;
                    //        }
                    //        //}
                    //    }
                    //    else
                    //        total = null;

                    //    break;
                    //}
                    case "INDEX EQUIVALENT OWNERS RATE:":
                    {
                        var tcInDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);

                        if (tcInDays != 0)
                        {
                            if (index.Contains("Physical"))
                            {
                                //total = sumTotalTcInCashFlow[index] / tcInDays;
                                total = sumTotalMTMIndexEqOwnersRateFinOption[index] / tcInDays;
                            }
                            else if(index.Contains("Financial"))
                            {
                                total = (sumTotalMTMIndexEqOwnersRateFinOption[index] + sumTotalMTMIndexEqOwnersRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / tcInDays;
                            }
                            else
                            {
                                total = (sumTotalMTMIndexEqOwnersRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Financial"] + sumTotalMTMIndexEqOwnersRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / tcInDays;
                            }
                        }
                        else
                            total = null;

                        break;
                    }
                    case "EFFECTIVE CHARTERERS EARNINGS RATE:":
                    {
                        //var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);
                        if (sumTotalTcOutDaysVesselIndex[index] != 0)
                            total = sumTotalTcOutCashFlow[index] / sumTotalTcOutDaysVesselIndex[index];
                        else
                            total = null;
                        break;
                    }
                    //case "EFFECTIVE OWNERS EARNINGS RATE:":
                    //{                        
                    //    //if (index.Contains("Physical"))
                    //    //{
                    //    //    var calendarDays = sumTotalTcInCalendarDays[index];
                    //    //    if (calendarDays == 0)
                    //    //    {
                    //    //        total = null;
                    //    //        break;
                    //    //    }
                    //    //    total = sumTotalTcInCashFlow[index] / calendarDays;
                    //    //}
                    //    //else
                    //    //{
                    //    var calendarDays = GetTotalTcInCalendarDays(index, sumTotalTcInCalendarDays);// sumTotalTcInCalendarDays[index.Substring(0, index.IndexOf("-")) + "-Physical"];
                    //    if (calendarDays == 0)
                    //    {
                    //        total = null;
                    //        break;
                    //    }
                    //    if (_physicalVessels != 0)
                    //    {
                    //        // vessel number cause to  calculate multiple days. fix is required . so for time being calendare days will be divided with vessels to temporary balance the days
                    //        calendarDays = calendarDays / _physicalVessels;
                    //        if (index == index.Substring(0, index.IndexOf("-")) + "-Total")
                    //        {
                    //            total = (sumTotalMTMEffOwnersEarningsRateFin[index.Substring(0, index.IndexOf("-")) + "-Financial"] + sumTotalMTMEffOwnersEarningsRateFin[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //        }
                    //        else if (!index.Contains("Physical"))
                    //        {
                    //            total = (sumTotalMTMEffOwnersEarningsRateFin[index] + sumTotalMTMEffOwnersEarningsRateFin[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //        }
                    //        else
                    //        {
                    //            total = (sumTotalMTMEffOwnersEarningsRateFin[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        total = null;
                    //    }
                    //    //}
                    //    break;
                    //}
                    case "EFFECTIVE OWNERS EARNINGS RATE:":
                    {
                        
                        var calendarDays = GetTotalTcInCalendarDays(index, sumTotalTcInCalendarDays);//sumTotalTcInCalendarDays[index.Substring(0, index.IndexOf("-")) + "-Physical"];
                        if (calendarDays == 0)
                        {
                            total = null;
                            break;
                        }
                        if (_physicalVessels != 0)
                        {
                            // vessel number cause to  calculate multiple days. fix is required . so for time being calendare days will be divided with vessels to temporary balance the days
                            calendarDays = calendarDays / _physicalVessels;
                            //total = (sumTotalMTMEffOwnersEarningsRateFinOption[index] + sumTotalMTMEffOwnersEarningsRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);

                            if (index == index.Substring(0, index.IndexOf("-")) + "-Total")
                            {
                                total = (sumTotalMTMEffOwnersEarningsRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Financial"] + sumTotalMTMEffOwnersEarningsRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                            }
                            else if (!index.Contains("Physical"))
                            {
                                total = (sumTotalMTMEffOwnersEarningsRateFinOption[index] + sumTotalMTMEffOwnersEarningsRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                            }
                            else
                            {
                                total = (sumTotalMTMEffOwnersEarningsRateFinOption[index.Substring(0, index.IndexOf("-")) + "-Physical"]) / ((_physicalVessels > 0) ? (calendarDays * _physicalVessels) : calendarDays);
                            }

                        }
                        else
                        {
                            total = null;
                        }
                        
                        break;
                    }
                    case "AVG LONG MTM PRICE:":
                    {
                        if (index.Contains("Physical"))
                        {
                            if (sumTotalMTMLongPricePositionDays[index] != 0)
                                total = sumTotalMTMLongPrice[index] / sumTotalMTMLongPricePositionDays[index];
                            else
                                total = null;
                        }
                        else
                        {                            
                            total = CalculateIndexTotalValue(mtmCalculationsPerIndex, index, dates);
                        }

                        break;
                    }
                    case "AVG SHORT MTM PRICE:":
                    {
                        if (index.Contains("Physical"))
                        {
                            if (sumTotalMTMShortPricePositionDays[index] != 0)
                                total = sumTotalMTMShortPrice[index] / sumTotalMTMShortPricePositionDays[index];
                            else
                                total = null;
                        }
                        else
                        {
                            total = CalculateIndexTotalValue(mtmCalculationsPerIndex,index,dates);
                        }
                        break;
                    }
                    case "AVG LONG DEAL PRICE:":
                    {
                        var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);

                        if (sumLongDays != 0)
                        {
                            total = sumTotalAvgLongPrice[index] / sumLongDays;
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "AVG SHORT DEAL PRICE:":
                    {
                        var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);

                        if (sumShortDays != 0)
                        {
                            decimal totavlValue = sumTotalAvgShortPrice[index] / sumShortDays;
                            if (index == index.Substring(0, index.IndexOf("-")) + "-Total")
                            {
                                total = Convert.ToDecimal(Math.Abs(totavlValue));
                            }
                            else
                            {
                                total = totavlValue;
                            }
                        }
                        else
                        {
                            total = null;
                        }                        
                        break;
                    }
                    case "TOTAL POSITION:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL POSITION:", dates);
                        break;
                    }
                    case "TC IN DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);
                        if (index.Contains("Financial"))
                        {
                            total = sumTotalTcInDays[index];

                            string[] indexParts = index.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                            string sameIndexOfPhysical = indexParts[0] + "-Physical";

                            if (mtmCalculationsPerIndex.ContainsKey(sameIndexOfPhysical))
                            {
                                total = 0;
                                decimal? dc = 0;
                                foreach (DateTime date in dates)
                                {
                                    if (mtmCalculationsPerIndex[sameIndexOfPhysical][label].ContainsKey(GetColumnDescriptionBaseDatePeriod(date)))
                                    {
                                        dc = sumTotalTcInDays[index] + sumTotalTcInDays[sameIndexOfPhysical];
                                    }
                                    total = total + dc;
                                }
                            }
                        }
                        else
                        {
                            total = sumTotalTcInDays[index];
                        }
                        break;
                    }
                    case "TC OUT DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);
                        break;
                    }
                    case "NET PHYSICAL DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PHYSICAL DAYS:", dates);
                        if (index.Contains("Financial"))
                        {
                            total = sumTotalTcInDays[index];

                            string[] indexParts = index.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                            string sameIndexOfPhysical = indexParts[0] + "-Physical";

                            if (mtmCalculationsPerIndex.ContainsKey(sameIndexOfPhysical))
                            {
                                total = 0;
                                decimal? dc = 0;
                                foreach (DateTime date in dates)
                                {
                                    if (posCalculationsPerIndex[sameIndexOfPhysical]["TC IN DAYS:"].ContainsKey(GetColumnDescriptionBaseDatePeriod(date)))
                                    {
                                        total = sumTotalTcInDays[index] + posCalculationsPerIndex[sameIndexOfPhysical]["TC IN DAYS:"][GetColumnDescriptionBaseDatePeriod(date)];
                                    }
                                    total = total + dc;
                                }
                            }
                        }
                        else
                        {
                            total = sumTotalNetPhysicalDays[index];
                        }
                        break;
                    }
                    case "NET PAPER DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);
                        break;
                    }
                    case "COVER (%):":
                    {
                        var netPaperDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);
                        var tcInDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);
                        //var tcOutDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);

                        //if (tcInDays != 0)
                        //{
                        //    total = (-1) * ((tcOutDays + netPaperDays) / tcInDays);
                        //}
                        //else
                        //{
                        //    total = null;
                        //}
                        if (tcInDays != 0)
                        {
                            total = Math.Abs((decimal)(netPaperDays / tcInDays) * 100);
                        }
                        else
                            total = null;
                        break;
                    }
                    //case "COVER AVERAGE RATE:":
                    //{
                    //    var sumFfaAndOptionDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);

                    //    if (sumFfaAndOptionDays != 0)
                    //    {
                    //        total = sumTotalPaperNotional[index] / sumFfaAndOptionDays;
                    //    }
                    //    else
                    //    {
                    //        total = null;
                    //    }
                    //    break;
                    //}
                    case "COVER AVERAGE RATE (only for FFAs):":
                    {
                        var sumFfaAndOptionDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET DAYS:", dates);

                        if (sumFfaAndOptionDays != 0)
                        {
                            total = sumTotalPaperNotionalFFA[index] / sumFfaAndOptionDays;
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "NET DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates) + GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);
                        break;
                    }
                    case "SHORT/LONG COVER (%):":
                    {
                        var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);
                        var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);

                        if (sumLongDays != 0)
                        {
                            total = Math.Abs((decimal)(sumShortDays / sumLongDays));
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "TOTAL FFA/OPTION DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);
                        var totalFFADaysValue = sumTotalFfaDays[index];
                        var totalOptionDaysValue = sumTotalOptionDays[index];

                        if (totalOptionDaysValue != 0)
                        {
                            total = totalFFADaysValue / totalOptionDaysValue;
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "FFA AVERAGE RATE:":
                    {
                        var sumFfaDays = sumTotalFfaDays[index];

                        if (sumFfaDays != 0)
                        {
                            total = sumTotalFfaNotional[index] / sumFfaDays;
                        }
                        else
                        {
                            total = null;
                        }

                        break;
                    }
                    case "TOTAL LONG DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL LONG DAYS:", dates);
                        break;
                    }
                    case "TOTAL SHORT DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TOTAL SHORT DAYS:", dates);
                        break;
                    }
                    case "VESSEL TC COVER (%):":
                    {
                        var tcInDays = sumTotalTcInDays[index];//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "TC IN DAYS:", dates);
                        var tcOutDays = sumTotalTcOutDays[index];//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "TC OUT DAYS:", dates);

                        if (tcInDays != 0)
                        {
                            total = Math.Abs(((decimal)tcOutDays / (decimal)tcInDays) * 100);
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "PAPER/PHYSICAL COVER (%):":
                    {
                        var netPaperDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);
                        var netPhysicalDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PHYSICAL DAYS:", dates);

                        if (netPhysicalDays != 0)
                        {
                            total = (((-1) * netPaperDays) / netPhysicalDays) * 100;
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "(adj) for options:":
                    {
                        //var paperDays = Convert.ToDecimal(GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates));

                        //if (paperDays != 0)
                        //{
                        //    total = sumTotalPaperNotional[index] / paperDays;
                        //}
                        //else
                        //{
                            total = null;
                        //}
                        break;
                    }
                    case "(incl. OPTIONS) AVG LONG DEAL PRICE:":
                    {
                        //var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);
                        //if (index != index.Substring(0, index.IndexOf("-")) + "-Physical")
                        //    if (sumLongDays != 0)
                        //    {
                        //        total = sumTotalAvgLongOptionDays[index] / sumLongDays;
                        //    }
                        //    else
                        //    {
                        //        total = null;
                        //    }
                        total = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG LONG DEAL PRICE:"]["Total"];
                        break;
                    }
                    case "(incl. OPTIONS) AVG SHORT DEAL PRICE:":
                    {
                        //var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                        //if (index != index.Substring(0, index.IndexOf("-")) + "-Physical")
                        //    if (sumShortDays != 0)
                        //    {
                        //        total = sumTotalAvgShortOptionDays[index] / sumShortDays;
                        //    }
                        //    else
                        //    {
                        //        total = null;
                        //    }                        
                        total = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"]["Total"];
                        break;
                    }
                    case "(incl. OPTIONS) AVG LONG MTM PRICE:":
                    {
                        if (index.Contains("Physical"))
                        {
                            if (sumTotalMTMLongOptionDays[index] != 0)
                                total = sumTotalMTMLongPrice[index] / sumTotalMTMLongOptionDays[index];
                            else
                                total = null;
                        }
                        else
                        {
                            total = CalculateIndexTotalValue(mtmCalculationsPerIndex, index, dates);
                        }

                        break;
                    }
                    case "(incl. OPTIONS) AVG SHORT MTM PRICE:":
                    {
                        if (index.Contains("Physical"))
                        {
                            if (sumTotalMTMShortOptionDays[index] != 0)
                                total = sumTotalMTMShortPrice[index] / sumTotalMTMShortOptionDays[index];
                            else
                                total = null;
                        }
                        else
                        {
                            total = CalculateIndexTotalValue(mtmCalculationsPerIndex, index, dates);
                        }
                        break;
                    }
                    case "COVER AVERAGE RATE:":
                    {
                        var sumFfaAndOptionDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "NET PAPER DAYS:", dates);

                        if (sumFfaAndOptionDays != 0)
                        {
                            total = sumTotalPaperOptional[index] / sumFfaAndOptionDays;
                        }
                        else
                            total = null;

                        break;
                    }                                        
                    case "(incl. OPTIONS) LOCKED IN:":
                    {
                        var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates));
                        var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates));
                        var avgShortPrice = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"]["Total"];//GetTotalAvgShortDealPrice(index, dates, "(incl. OPTIONS) TOTAL SHORT DAYS:", sumTotalAvgShortOptionDays);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG SHORT DEAL PRICE:", dates);
                        var avgLongPrice = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG LONG DEAL PRICE:"]["Total"];//GetTotalAvgLongDealPrice(index, dates, "(incl. OPTIONS) TOTAL LONG DAYS:", sumTotalAvgLongOptionDays);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG LONG DEAL PRICE:", dates);

                        if (totalLongDays < totalShortDays)
                        {
                            total = totalLongDays * (avgShortPrice - avgLongPrice);
                        }
                        else
                        {
                            total = totalShortDays * (avgShortPrice - avgLongPrice);
                        }

                        break;
                    }
                    case "(incl. OPTIONS) BREAKEVEN PRICE:":
                    {
                        decimal? lockedIn = null;//mtmCalculationsPerIndex[index]["(incl. OPTIONS) LOCKED IN:"]["Total"];//GetTotalLockedIn(index, dates, "(incl. OPTIONS) TOTAL LONG DAYS:", "(incl. OPTIONS) TOTAL SHORT DAYS:", sumTotalAvgLongOptionDays, sumTotalAvgShortOptionDays);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "(incl. OPTIONS) LOCKED IN:", dates);
                        var avgShortPrice = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"]["Total"];//GetTotalAvgShortDealPrice(index, dates, "(incl. OPTIONS) TOTAL SHORT DAYS:", sumTotalAvgShortOptionDays);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG SHORT DEAL PRICE:", dates);
                        var avgLongPrice = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG LONG DEAL PRICE:"]["Total"];//GetTotalAvgLongDealPrice(index, dates, "(incl. OPTIONS) TOTAL LONG DAYS:", sumTotalAvgLongOptionDays);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG LONG DEAL PRICE:", dates);
                        var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates));
                        var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates));
                        var netDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) NET DAYS:", dates);

                        if (totalLongDays < totalShortDays)
                        {
                            lockedIn = totalLongDays * (avgShortPrice - avgLongPrice);
                        }
                        else
                        {
                            lockedIn = totalShortDays * (avgShortPrice - avgLongPrice);
                        }
                        

                        if (index.Contains("Physical"))
                        {
                            if (totalLongDays < totalShortDays)
                                total = avgShortPrice - (lockedIn / netDays);
                            else 
                                total = avgLongPrice - (lockedIn / netDays);                         
                        }
                        else
                        {
                            if (netDays < 0)
                                total = ((netDays * avgShortPrice) - lockedIn) / netDays;
                            else
                                total = ((netDays * avgLongPrice) - lockedIn) / netDays;                            

                        }

                        break;
                    }
                    case "(incl. OPTIONS) UNREALIZED:":
                    {
                        var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates));
                        var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates));
                        var netDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) NET DAYS:", dates);

                        if (index.Contains("Physical"))
                        {
                            if (totalLongDays > totalShortDays)
                            {
                                var avgLongPrice = GetTotalAvgLongDealPrice(index, dates, "(incl. OPTIONS) TOTAL LONG DAYS:", sumTotalAvgLongOptionDays);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG LONG DEAL PRICE:", dates);
                                //var avgLongMTMPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG LONG MTM PRICE:", dates);
                                var avgLongMTMPrice = GetTotalAVGLongMTMPrice(index, dates);

                                total = (avgLongMTMPrice - avgLongPrice) * netDays;
                            }
                            else if (totalLongDays < totalShortDays)
                            {
                                //var avgShortMTMPrice = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG SHORT MTM PRICE:", dates);
                                var avgShortMTMPrice = GetTotalAVGShortMTMPrice(index, dates);
                                var avgShortPrice = GetTotalAvgShortDealPrice(index, dates, "(incl. OPTIONS) TOTAL SHORT DAYS:", sumTotalAvgShortOptionDays);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG SHORT DEAL PRICE:", dates);

                                total = (avgShortMTMPrice - avgShortPrice) * netDays;
                            }
                            else
                            {
                                total = null;
                            }
                        }
                        else
                        {
                            //decimal indexValue = 0;
                            //foreach (DateTime date in dates)
                            //{
                            //    indexValue += GetIndexValueWithAggregation(index, GetColumnDescriptionBaseDatePeriod(date));
                            //}

                            //if (netDays > 0)
                            //    total = (indexValue - GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG LONG DEAL PRICE:", dates)) * netDays;
                            //else if (netDays <= 0)
                            //    total = (indexValue - GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) AVG SHORT DEAL PRICE:", dates)) * netDays;
                            if (netDays < 0)
                            {
                                var avgShortMTMPrice = GetTotalAVGShortMTMPrice(index, dates);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG SHORT MTM PRICE:", dates);                                
                                var avgShortPrice = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG SHORT DEAL PRICE:"]["Total"];//GetTotalAvgShortDealPrice(index, dates, "(incl. OPTIONS) TOTAL SHORT DAYS:", sumTotalAvgShortOptionDays);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "(incl. OPTIONS) AVG SHORT DEAL PRICE:", dates);
                                total = (avgShortPrice - avgShortMTMPrice) * Math.Abs(netDays);
                            }
                            else
                            {
                                var avgLongMTMPrice = GetTotalAVGLongMTMPrice(index, dates);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "AVG LONG MTM PRICE:", dates);
                                var avgLongPrice = posCalculationsPerIndex[index]["(incl. OPTIONS) AVG LONG DEAL PRICE:"]["Total"]; //GetTotalAvgLongDealPrice(index, dates, "(incl. OPTIONS) TOTAL LONG DAYS:", sumTotalAvgLongOptionDays);//GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "(incl. OPTIONS) AVG LONG DEAL PRICE:", dates);
                                total = (avgLongMTMPrice - avgLongPrice) * netDays;
                            }
                        }

                        break;
                    }
                    case "(incl. OPTIONS) TOTAL LONG DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);
                        break;
                    }
                    case "(incl. OPTIONS) TOTAL SHORT DAYS:":
                    {
                        total = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                        break;
                    }
                    case "(incl. OPTIONS) NET DAYS:":
                    {
                        var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                        var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);

                        total = sumLongDays + sumShortDays;
                        break;
                    }
                    case "(incl. OPTIONS) SHORT/LONG COVER (%):":
                    {
                        var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL SHORT DAYS:", dates);
                        var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "(incl. OPTIONS) TOTAL LONG DAYS:", dates);

                        if (sumLongDays != 0)
                        {
                            total = Math.Abs((decimal)(sumShortDays / sumLongDays));
                        }
                        else
                        {
                            total = null;
                        }
                        break;
                    }
                    case "(incl. OPTIONS) COVER (%):":
                    {
                        var netPaperDays = GetSumValueFromDictionaryForPeriod(mtmCalculationsPerIndex, index, "(incl. OPTIONS) NET PAPER DAYS:", dates);
                        var tcInDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC IN DAYS:", dates);
                        var tcOutDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "TC OUT DAYS:", dates);

                        if (tcInDays != 0)
                        {
                            total = (-1) * ((tcOutDays + netPaperDays) / tcInDays);
                        }
                        else
                        {
                            total = null;
                        }
                        //if (tcInDays != 0)
                        //{
                        //    total = Math.Abs((decimal)(netPaperDays / tcInDays) * 100);

                        //}
                        //else
                        //    total = null;
                        break;
                    }
                }
            }
            catch
            {

            }
            return total;
        }
        private decimal GetSumValueFromDictionaryForPeriod(Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>> dictionaryData, string index, string label, List<DateTime> dates)
        {
            decimal sum = 0;
            Dictionary<string, decimal?> tempData = new Dictionary<string, decimal?>();
            string dateString = "", strQuarter;            
            int quarterAdded = 0, calendarAdded = 0, intQuarter, intCalendar;
            try
            {
                foreach (DateTime date in dates)
                {
                    if (!tempData.ContainsKey(GetColumnDescriptionBaseDatePeriod(date)))
                    {
                        tempData.Add(GetColumnDescriptionBaseDatePeriod(date), dictionaryData[index][label][GetColumnDescriptionBaseDatePeriod(date)]);
                        sum = sum + (dictionaryData[index][label][GetColumnDescriptionBaseDatePeriod(date)] ?? 0);
                    }
                }
            }
            catch {
            }
            return sum;
        }
        private string GetColumnDescriptionBaseDatePeriod(DateTime date) {            
            string dateString = "", strQuarter;
            int quarterAdded = 0, calendarAdded = 0, intQuarter, intCalendar;
            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
            {
                dateString = date.ToString("MMM-yyyy", new CultureInfo("en-GB"));
            }
            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
            {
                quarterAdded = 0;
                intQuarter = ((date.Month - 1) / 3) + 1;
                if (intQuarter != quarterAdded)
                {
                    quarterAdded = intQuarter;
                    strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                    dateString = strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB"));
                }
            }
            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
            {
                calendarAdded = 0;
                intCalendar = date.Year;
                if (intCalendar != calendarAdded)
                {
                    calendarAdded = intCalendar;
                    dateString = "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB"));
                }
            }
            return dateString;
        }
        private void InitSumTotalDictionaries()
        {
            foreach (var indexPhysicalFinancial in indexesPhysicalFinancial)
            {
                InitializeDecimalDictionary(sumTotalPosition, indexPhysicalFinancial,0);
                InitializeDecimalDictionary(sumTotalLongDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalShortDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalAvgLongPrice, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalAvgShortPrice, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalNetPhysicalDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalNetPaperDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalNetPaperDaysOptions, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcInDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcOutDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalFfaNotional, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalPaperNotional, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalPaperNotionalFFA, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalPaperOptional, indexPhysicalFinancial, 0); 
                InitializeDecimalDictionary(sumTotalNetPhysicalDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcOutPrice, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalOptionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalCommission, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcInCashFlow, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcOutCashFlow, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcInCalendarDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcInDaysVesselIndex, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalTcOutDaysVesselIndex, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalFfaDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalAvgShortOptionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalAvgLongOptionDays, indexPhysicalFinancial, 0);

                InitializeDecimalDictionary(sumTotalLongOptionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalShortOptionDays, indexPhysicalFinancial, 0);

                InitializeDecimalDictionary(sumTotalMTMLongPrice, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMLongPricePositionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMShortPrice, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMShortPricePositionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMLongOptionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMShortOptionDays, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMIndexEqOwnersRateFin, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMIndexEqOwnersRateFinOption, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTMEffOwnersEarningsRateFin, indexPhysicalFinancial, 0); 
                InitializeDecimalDictionary(sumTotalMTMEffOwnersEarningsRateFinOption, indexPhysicalFinancial, 0);
                InitializeDecimalDictionary(sumTotalMTM, indexPhysicalFinancial, 0);
            }
            foreach (var indx in indexesPhysicalFinancialTotal)
            {
                InitializeDecimalDictionary(sumTotalPosition, indx, 0);
                InitializeDecimalDictionary(sumTotalLongDays, indx, 0);
                InitializeDecimalDictionary(sumTotalShortDays, indx, 0);
                InitializeDecimalDictionary(sumTotalAvgLongPrice, indx, 0);
                InitializeDecimalDictionary(sumTotalAvgShortPrice, indx, 0);
                InitializeDecimalDictionary(sumTotalNetPhysicalDays, indx, 0);
                InitializeDecimalDictionary(sumTotalNetPaperDays, indx, 0);
                InitializeDecimalDictionary(sumTotalNetPaperDaysOptions, indx, 0);
                InitializeDecimalDictionary(sumTotalTcInDays, indx, 0);
                InitializeDecimalDictionary(sumTotalTcOutDays, indx, 0);
                InitializeDecimalDictionary(sumTotalFfaNotional, indx, 0);
                InitializeDecimalDictionary(sumTotalPaperNotional, indx, 0);
                InitializeDecimalDictionary(sumTotalPaperNotionalFFA, indx, 0);
                InitializeDecimalDictionary(sumTotalPaperOptional, indx, 0); 
                InitializeDecimalDictionary(sumTotalNetPhysicalDays, indx, 0);
                InitializeDecimalDictionary(sumTotalTcOutPrice, indx, 0);
                InitializeDecimalDictionary(sumTotalOptionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalCommission, indx, 0);
                InitializeDecimalDictionary(sumTotalTcInCashFlow, indx, 0);
                InitializeDecimalDictionary(sumTotalTcOutCashFlow, indx, 0);
                InitializeDecimalDictionary(sumTotalTcInCalendarDays, indx, 0);
                InitializeDecimalDictionary(sumTotalTcInDaysVesselIndex, indx, 0);
                InitializeDecimalDictionary(sumTotalTcOutDaysVesselIndex, indx, 0);
                InitializeDecimalDictionary(sumTotalFfaDays, indx, 0);
                InitializeDecimalDictionary(sumTotalAvgShortOptionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalAvgLongOptionDays, indx, 0);

                InitializeDecimalDictionary(sumTotalLongOptionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalShortOptionDays, indx, 0);

                InitializeDecimalDictionary(sumTotalMTMLongPrice, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMLongPricePositionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMShortPrice, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMShortPricePositionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMLongOptionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMShortOptionDays, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMIndexEqOwnersRateFin, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMIndexEqOwnersRateFinOption, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMEffOwnersEarningsRateFin, indx, 0);
                InitializeDecimalDictionary(sumTotalMTMEffOwnersEarningsRateFinOption, indx, 0);
                InitializeDecimalDictionary(sumTotalMTM, indx, 0);
            }
        }
        private void InitializeDecimalDictionary(Dictionary<string, decimal> diction, string dictionaryKey,decimal dictionValue) {
            if (!diction.ContainsKey(dictionaryKey))
                diction.Add(dictionaryKey, 0);
            else
                diction[dictionaryKey] = 0;
        }
        private decimal? CalculateIndexTotalValue(Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>> dictionaryData, string index, List<DateTime> dates) {
            string fc = index.Substring(0, index.IndexOf("-"));
            decimal? dividend = 0;
            int divisor = 0;
            foreach (DateTime date in dates)
            {
                dividend += dictionaryData[index][fc][GetColumnDescriptionBaseDatePeriod(date)] * DateTime.DaysInMonth(date.Year, date.Month);
                divisor += DateTime.DaysInMonth(date.Year, date.Month);
            }
            return (divisor != 0)?(dividend / divisor):null;
        }
        private decimal GetTradeData_BaseAggregation(Dictionary<string, Dictionary<DateTime, decimal>> dictionaryData, TradeInformation tradeInformation, string period) {
            decimal valueData = 0;

            if (dictionaryData.Keys.Contains(tradeInformation.Identifier))
            {
                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    DateTime date = Convert.ToDateTime(period);
                    valueData = dictionaryData[tradeInformation.Identifier][date];
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                {
                    valueData = dictionaryData[tradeInformation.Identifier].Where(
                        a =>
                        {
                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                            string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == period;
                        }).Select(a => a.Value).Sum();
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                {
                    valueData = dictionaryData[tradeInformation.Identifier].Where(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == period).Select(a => a.Value).Sum();
                }
            }
            return valueData;
        }
        private decimal GetTotalAvgLongDealPrice(string index,List<DateTime> dates, string label, Dictionary<string,decimal> sourceDiction)
        {
            decimal value = 0;
            var sumLongDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, label, dates);
            if (sumLongDays != 0)
            {
                value = sourceDiction[index] / sumLongDays;
            }
            return value;
        }
        private decimal GetTotalAvgShortDealPrice(string index, List<DateTime> dates,string label, Dictionary<string, decimal> sourceDiction)
        {
            decimal value = 0;
            var sumShortDays = GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, label, dates);

            if (sumShortDays != 0)
            {
                decimal totavlValue = sourceDiction[index] / sumShortDays;
                if (index == index.Substring(0, index.IndexOf("-")) + "-Total")
                {
                    value = Convert.ToDecimal(Math.Abs(totavlValue));
                }
                else
                {
                    value = totavlValue;
                }
            }
            return value;
        }
        private decimal GetTotalLockedIn(string index, List<DateTime> dates,string totalLongLabel,string totalShortLabel, Dictionary<string, decimal> sourceDictionLong, Dictionary<string, decimal> sourceDictionShort)
        {            
            decimal value = 0;
            var totalLongDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, totalLongLabel, dates));
            var totalShortDays = Math.Abs((decimal)GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, totalShortLabel, dates));
            var avgShortPrice = GetTotalAvgLongDealPrice(index, dates, totalLongLabel, sourceDictionLong);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG SHORT DEAL PRICE:", dates);
            var avgLongPrice = GetTotalAvgShortDealPrice(index, dates, totalShortLabel, sourceDictionShort);//GetSumValueFromDictionaryForPeriod(posCalculationsPerIndex, index, "AVG LONG DEAL PRICE:", dates);

            if (totalLongDays > totalShortDays)
            {
                value = totalLongDays * (avgShortPrice - avgLongPrice);
            }
            else
            {
                value = totalShortDays * (avgShortPrice - avgLongPrice);
            }
            return value;
        }
        private decimal? GetTotalAVGLongMTMPrice(string index, List<DateTime> dates)
        {
            decimal? value = 0;
            if (index.Contains("Physical"))
            {
                if (sumTotalMTMLongPricePositionDays[index] != 0)
                    value = sumTotalMTMLongPrice[index] / sumTotalMTMLongPricePositionDays[index];
                else
                    value = null;
            }
            else
            {
                value = CalculateIndexTotalValue(mtmCalculationsPerIndex, index, dates);
            }
            return value;
        }
        private decimal? GetTotalAVGShortMTMPrice(string index, List<DateTime> dates)
        {
            decimal? value = 0;
            if (index.Contains("Physical"))
            {
                if (sumTotalMTMShortPricePositionDays[index] != 0)
                    value = sumTotalMTMShortPrice[index] / sumTotalMTMShortPricePositionDays[index];
                else
                    value = null;
            }
            else
            {
                value = CalculateIndexTotalValue(mtmCalculationsPerIndex, index, dates);
            }
            return value;
        }
        private decimal GetTotalTcInCalendarDays(string index, Dictionary<string, decimal> dict) {
            if(!index.Contains("Physical"))
                index = index.Substring(0, index.IndexOf("-")) + "-Physical";
            if (dict.ContainsKey(index))
            {
                return dict[index];
            }
            else
            {
                return 0;
            }
        }
        private void AddRowWithFirstCellLabel(string label,ref DataTable dt) {
            try
            {
                DataRow newRow = dt.NewRow();
                newRow[0] = label;
                dt.Rows.Add(newRow);
            }
            catch
            {

            }
        }
    }
}