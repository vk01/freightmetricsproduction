﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using Exis.Domain;
using Exis.WinClient.General;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;

namespace Exis.WinClient.Controls.Core
{
    public partial class AddEditViewTradeCargoInfoControl : DevExpress.XtraEditors.XtraUserControl
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private Market _market;
        private DateTime _signDate;
        private TradeInfo _tradeInfo;

        #endregion

        #region Constructors

        public AddEditViewTradeCargoInfoControl()
        {
            InitializeComponent();

            _formActionType = FormActionTypeEnum.Add;
            InitializeControls();
        }

        public AddEditViewTradeCargoInfoControl(FormActionTypeEnum formActionType)
        {
            InitializeComponent();

            _formActionType = formActionType;
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            lookupVessel.Properties.DisplayMember = "Name";
            lookupVessel.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Vessel_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupVessel.Properties.Columns.Add(col);
            lookupVessel.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupVessel.Properties.SearchMode = SearchMode.AutoFilter;
            lookupVessel.Properties.NullValuePrompt =
                Strings.Select_a_Vessel___;

            lookUpIndex_1.Properties.DisplayMember = "Name";
            lookUpIndex_1.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookUpIndex_1.Properties.Columns.Add(col);
            lookUpIndex_1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookUpIndex_1.Properties.SearchMode = SearchMode.AutoFilter;
            lookUpIndex_1.Properties.NullText = "";
            lookUpIndex_1.Properties.NullValuePrompt =
                Strings.Select_a_Market_Index___;

            cmbType.Properties.Items.Add(TradeInfoDirectionEnum.InOrBuy);
            cmbType.Properties.Items.Add(TradeInfoDirectionEnum.OutOrSell);
            cmbType.SelectedItem = TradeInfoDirectionEnum.InOrBuy;

            txtRate_1.Properties.ReadOnly = true;
            lookUpIndex_1.Properties.ReadOnly = true;

            cmbRateType_1.Properties.Items.Add(TradeInfoLegRateTypeEnum.Fixed);
            cmbRateType_1.Properties.Items.Add(TradeInfoLegRateTypeEnum.IndexLinked);
            cmbRateType_1.SelectedItem = TradeInfoLegRateTypeEnum.Fixed;

            cmbStatus_1.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.Pending);
            cmbStatus_1.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.Declared);
            cmbStatus_1.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.NonDeclared);
            cmbStatus_1.Properties.ReadOnly = true;
        }

        private void SetVesselIndex()
        {
            if (lookupVessel.EditValue == null)
                txtVesselIndex.EditValue = 100;
            else
            {
                var vesselId = (long)lookupVessel.EditValue;
                txtVesselIndex.EditValue =
                    ((List<VesselBenchmarking>)txtVesselIndex.Tag).Where(
                        a =>
                        a.VesselId == vesselId && _signDate > a.PeriodFrom.Date && _signDate.Date < a.PeriodTo.Date).
                        Any()
                        ? ((List<VesselBenchmarking>)txtVesselIndex.Tag).Where(
                            a =>
                            a.VesselId == vesselId && _signDate > a.PeriodFrom.Date && _signDate.Date < a.PeriodTo.Date)
                              .First().Benchmark
                        : 100;
            }
        }

        #endregion
        
        #region Public Methods

        public void SetInitializationData(List<Vessel> vessels, List<Index> indexes, List<VesselBenchmarking> vesselBenchmarkings)
        {
            lookupVessel.Tag = vessels;
            lookUpIndex_1.Tag = indexes;
            txtVesselIndex.Tag = vesselBenchmarkings;
        }

        public void SetMarket(Market market)
        {
            _market = market;

            lookupVessel.EditValue = null;
            lookupVessel.Properties.DataSource = null;

            lookUpIndex_1.EditValue = null;
            lookUpIndex_1.Properties.DataSource = null;

            foreach (object item in layoutLegsGroup.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutLeg = (LayoutControlGroup)item;
                    if (layoutLeg.Name.StartsWith("layoutLeg_"))
                    {
                        foreach (object item1 in layoutLeg.Items)
                        {
                            if (item1.GetType() == typeof(LayoutControlItem))
                            {
                                var layoutItem = (LayoutControlItem)item1;
                                if (layoutItem.Name.StartsWith("layoutIndex_"))
                                {
                                    var lookupLegIndex = (LookUpEdit)layoutItem.Control;
                                    lookupLegIndex.EditValue = null;
                                    lookupLegIndex.Properties.DataSource = null;

                                    if (market != null)
                                    {
                                        lookupLegIndex.Properties.DataSource =
                                            ((List<Index>)lookUpIndex_1.Tag).Where(a => a.MarketId == market.Id).
                                                ToList();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (market != null)
            {
                lookupVessel.Properties.DataSource =
                    ((List<Vessel>)lookupVessel.Tag).Where(a => a.MarketId == market.Id).ToList();

                lookUpIndex_1.Properties.DataSource =
                    ((List<Index>)lookUpIndex_1.Tag).Where(a => a.MarketId == market.Id).ToList();
            }
        }

        public void SetSignDate(DateTime signDate)
        {
            _signDate = signDate;
            SetVesselIndex();
        }

        public bool ValidateData()
        {
            errorProvider.ClearErrors();
            if (cmbType.SelectedItem == null)
                errorProvider.SetError(cmbType, Strings.Field_is_mandatory);
            //if (lookupVessel.EditValue == null)
            //    errorProvider.SetError(lookupVessel, Strings.Field_is_mandatory);
            foreach (object item in layoutLegsGroup.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutLeg = (LayoutControlGroup) item;
                    if (layoutLeg.Name.StartsWith("layoutLeg_"))
                    {
                        foreach (object item1 in layoutLeg.Items)
                        {
                            if (item1.GetType() == typeof (LayoutControlItem))
                            {
                                var layoutItem = (LayoutControlItem) item1;
                                if (layoutItem.Name.StartsWith("layoutPeriodFrom_") ||
                                    layoutItem.Name.StartsWith("layoutPeriodTo_") ||
                                    layoutItem.Name.StartsWith("layoutRateType_"))
                                {
                                    var baseEdit = (BaseEdit) layoutItem.Control;
                                    if (String.IsNullOrEmpty(Convert.ToString(baseEdit.EditValue)))
                                    {
                                        errorProvider.SetError(baseEdit, Strings.Field_is_mandatory);
                                    }
                                }
                            }
                        }

                        string layoutLegSuffix = layoutLeg.Name.Substring(layoutLeg.Name.IndexOf("_") + 1);
                        DateEdit periodFrom =
                            (DateEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutPeriodFrom_" + layoutLegSuffix)).
                                Control;
                        DateEdit periodTo =
                            (DateEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutPeriodTo_" + layoutLegSuffix)).
                                Control;
                        ComboBoxEdit rateType =
                            (ComboBoxEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutRateType_" + layoutLegSuffix)).
                                Control;
                        SpinEdit indexPerc =
                            (SpinEdit)
                            ((LayoutControlItem)layoutLeg.Items.FindByName("layoutIndexPerc_" + layoutLegSuffix)).Control;
                        SpinEdit rate =
                            (SpinEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutRate_" + layoutLegSuffix)).Control;
                        LookUpEdit index =
                            (LookUpEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutIndex_" + layoutLegSuffix)).Control;
                        CheckEdit isOptional =
                            (CheckEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutOptional_" + layoutLegSuffix)).
                                Control;
                        ComboBoxEdit optionalStatus =
                            (ComboBoxEdit)
                            ((LayoutControlItem) layoutLeg.Items.FindByName("layoutStatus_" + layoutLegSuffix)).Control;

                        if (periodFrom.EditValue != null && periodTo.EditValue != null &&
                            periodFrom.DateTime.Date >= periodTo.DateTime.Date)
                            errorProvider.SetError(periodFrom,
                                                   Strings.
                                                       Date_To__field_should_be_greater_than_the_relevant__Date_From__field_for_each_Leg_);
                        //if (rateType.SelectedItem != null &&
                        //    (TradeInfoLegRateTypeEnum) rateType.SelectedItem == TradeInfoLegRateTypeEnum.Fixed &&
                        //    rate.Value == 0) errorProvider.SetError(rate, Strings.Field_is_mandatory);
                        if (rateType.SelectedItem != null &&
                            (TradeInfoLegRateTypeEnum) rateType.SelectedItem == TradeInfoLegRateTypeEnum.IndexLinked &&
                            index.EditValue == null) errorProvider.SetError(index, Strings.Field_is_mandatory);
                        if (rateType.SelectedItem != null &&
                            (TradeInfoLegRateTypeEnum)rateType.SelectedItem == TradeInfoLegRateTypeEnum.IndexLinked &&
                            indexPerc.Value == 0) errorProvider.SetError(indexPerc, Strings.Field_is_mandatory);
                        if (isOptional.EditValue != null && (bool) isOptional.EditValue &&
                            optionalStatus.SelectedItem == null)
                            errorProvider.SetError(optionalStatus, Strings.Field_is_mandatory);
                        if (layoutLegSuffix != "1")
                        {
                            DateEdit previousDateTo =
                                (DateEdit)
                                ((LayoutControlItem)
                                 ((LayoutControlGroup)
                                  layoutLegsGroup.Items.FindByName("layoutLeg_" + (Convert.ToInt32(layoutLegSuffix) - 1)))
                                     .Items.FindByName("layoutPeriodTo_" + (Convert.ToInt32(layoutLegSuffix) - 1))).
                                    Control;
                            DateEdit currentDateFrom =
                                (DateEdit)
                                ((LayoutControlItem)
                                 ((LayoutControlGroup)
                                  layoutLegsGroup.Items.FindByName("layoutLeg_" + (Convert.ToInt32(layoutLegSuffix)))).
                                     Items.FindByName("layoutPeriodFrom_" + (Convert.ToInt32(layoutLegSuffix)))).Control;

                            //Not applicable to Cargo Trade
                            //if (currentDateFrom.EditValue != null && previousDateTo.EditValue != null && currentDateFrom.DateTime.Date != previousDateTo.DateTime.Date.AddDays(1))
                            //    errorProvider.SetError(currentDateFrom,
                            //                           Strings.
                            //                               Date_From__field_of_each_Leg_should_be_equal_to_the__Date_To__field_of_the_previous_Leg_plus_1_day_);

                            CheckEdit previousOptional =
                                (CheckEdit)
                                ((LayoutControlItem)
                                 ((LayoutControlGroup)
                                  layoutLegsGroup.Items.FindByName("layoutLeg_" + (Convert.ToInt32(layoutLegSuffix) - 1)))
                                     .Items.FindByName("layoutOptional_" + (Convert.ToInt32(layoutLegSuffix) - 1))).
                                    Control;
                            CheckEdit currentOptional =
                                (CheckEdit)
                                ((LayoutControlItem)
                                 ((LayoutControlGroup)
                                  layoutLegsGroup.Items.FindByName("layoutLeg_" + (Convert.ToInt32(layoutLegSuffix)))).
                                     Items.FindByName("layoutOptional_" + (Convert.ToInt32(layoutLegSuffix)))).Control;

                            if (previousOptional.EditValue != null && (bool) previousOptional.EditValue &&
                                currentOptional.EditValue != null && !(bool) currentOptional.EditValue)
                                errorProvider.SetError(currentOptional,
                                                       Strings.
                                                           Current_Leg_must_be_optional_because_the_previous_Leg_is_Optional_);

                            if (currentOptional.EditValue != null && (bool) currentOptional.EditValue)
                            {
                                ComboBoxEdit previousStatus =
                                    (ComboBoxEdit)
                                    ((LayoutControlItem)
                                     ((LayoutControlGroup)
                                      layoutLegsGroup.Items.FindByName("layoutLeg_" +
                                                                       (Convert.ToInt32(layoutLegSuffix) - 1))).Items.
                                         FindByName("layoutStatus_" + (Convert.ToInt32(layoutLegSuffix) - 1))).Control;
                                ComboBoxEdit currentStatus =
                                    (ComboBoxEdit)
                                    ((LayoutControlItem)
                                     ((LayoutControlGroup)
                                      layoutLegsGroup.Items.FindByName("layoutLeg_" + (Convert.ToInt32(layoutLegSuffix))))
                                         .Items.FindByName("layoutStatus_" + (Convert.ToInt32(layoutLegSuffix)))).
                                        Control;

                                if (previousStatus.SelectedItem != null && currentStatus.SelectedItem != null
                                    &&
                                    (TradeInfoLegOptionalStatusEnum) currentStatus.SelectedItem ==
                                    TradeInfoLegOptionalStatusEnum.Declared
                                    &&
                                    ((TradeInfoLegOptionalStatusEnum) previousStatus.SelectedItem ==
                                     TradeInfoLegOptionalStatusEnum.Pending
                                     ||
                                     (TradeInfoLegOptionalStatusEnum) previousStatus.SelectedItem ==
                                     TradeInfoLegOptionalStatusEnum.NonDeclared))
                                    errorProvider.SetError(currentStatus,
                                                           Strings.
                                                               Status_for_the_current_Leg_cannot_be__Accepted__when_the_Status_of_the_previous_Leg_is__Pending__or__Declined__);

                                if (previousStatus.SelectedItem != null && currentStatus.SelectedItem != null
                                    &&
                                    (TradeInfoLegOptionalStatusEnum) currentStatus.SelectedItem ==
                                    TradeInfoLegOptionalStatusEnum.Pending
                                    &&
                                    (TradeInfoLegOptionalStatusEnum) previousStatus.SelectedItem ==
                                    TradeInfoLegOptionalStatusEnum.Declared)
                                    errorProvider.SetError(currentStatus,
                                                           Strings.
                                                               Status_for_the_current_Leg_cannot_be__Pending__when_the_Status_of_the_previous_Leg_is__Declined__);
                            }
                        }
                    }
                }
            }

            return !errorProvider.HasErrors;
        }

        public void GetData(out TradeInfoDirectionEnum direction, out string loadingPort, out string dischargingPort, out long? vesselId, out decimal vesselIndex, out decimal? address, out List<TradeCargoInfoLeg> legs)
        {
            direction = (TradeInfoDirectionEnum)cmbType.SelectedItem;
            vesselId = (long?)lookupVessel.EditValue;
            address = txtAddress.Value == 0 ? null : (decimal?)txtAddress.Value;
            vesselIndex = (decimal)txtVesselIndex.EditValue;
            loadingPort = txtLoadingPort.EditValue == null ? null : txtLoadingPort.EditValue.ToString();
            dischargingPort = txtDischargingPort.EditValue == null ? null : txtDischargingPort.EditValue.ToString();
            
            legs = new List<TradeCargoInfoLeg>();
            foreach (object item in layoutLegsGroup.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutLeg = (LayoutControlGroup)item;
                    if (layoutLeg.Name.StartsWith("layoutLeg_"))
                    {
                        string layoutLegSuffix = layoutLeg.Name.Substring(layoutLeg.Name.IndexOf("_") + 1);
                        DateEdit periodFrom = (DateEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutPeriodFrom_" + layoutLegSuffix)).Control;
                        DateEdit periodTo = (DateEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutPeriodTo_" + layoutLegSuffix)).Control;
                        ComboBoxEdit rateType = (ComboBoxEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutRateType_" + layoutLegSuffix)).Control;
                        SpinEdit rate = (SpinEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutRate_" + layoutLegSuffix)).Control;
                        LookUpEdit index = (LookUpEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutIndex_" + layoutLegSuffix)).Control;
                        CheckEdit isOptional = (CheckEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutOptional_" + layoutLegSuffix)).Control;
                        ComboBoxEdit status = (ComboBoxEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutStatus_" + layoutLegSuffix)).Control;
                        SpinEdit indexPercentage = (SpinEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutIndexPerc_" + layoutLegSuffix)).Control;
                        SpinEdit quantity = (SpinEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutQuantity_" + layoutLegSuffix)).Control;
                        SpinEdit quantityVariation = (SpinEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutQuantityVariation_" + layoutLegSuffix)).Control;
                        SpinEdit tce = (SpinEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutTce_" + layoutLegSuffix)).Control;

                        var leg = new TradeCargoInfoLeg
                                      {
                                          PeriodFrom = periodFrom.DateTime.Date,
                                          PeriodTo = periodTo.DateTime.Date,
                                          IsOptional = (bool) isOptional.EditValue,
                                          OptionalStatus =
                                              status.SelectedItem == null
                                                  ? (TradeInfoLegOptionalStatusEnum?) null
                                                  : (TradeInfoLegOptionalStatusEnum) status.SelectedItem,
                                          RateType = (TradeInfoLegRateTypeEnum) rateType.SelectedItem,
                                          Rate =
                                              (TradeInfoLegRateTypeEnum) rateType.SelectedItem ==
                                              TradeInfoLegRateTypeEnum.IndexLinked
                                                  ? null
                                                  : (decimal?) rate.Value,
                                          IndexId =
                                              (TradeInfoLegRateTypeEnum) rateType.SelectedItem ==
                                              TradeInfoLegRateTypeEnum.Fixed
                                                  ? null
                                                  : (long?) index.EditValue,
                                          Identifier = Convert.ToInt16(layoutLegSuffix),
                                          IndexPercentage = (TradeInfoLegRateTypeEnum) rateType.SelectedItem ==
                                                            TradeInfoLegRateTypeEnum.Fixed
                                                                ? null
                                                                : (decimal?) indexPercentage.Value,
                                          Quantity = Convert.ToInt32(quantity.EditValue),
                                          QuantityVariation = Convert.ToDecimal(quantityVariation.EditValue),
                                          Tce =
                                              (tce.EditValue == null || tce.Value == 0)
                                                  ? (decimal?) null
                                                  : tce.Value
                                      };
                        legs.Add(leg);
                    }
                }
            }
        }

        public void LoadData(TradeInfo tradeInfo)
        {
            _tradeInfo = tradeInfo;

            cmbType.EditValue = _tradeInfo.Direction;
            lookupVessel.EditValue = _tradeInfo.CargoInfo.VesselId;
            txtVesselIndex.EditValue = _tradeInfo.CargoInfo.VesselIndex;
            txtLoadingPort.EditValue = _tradeInfo.CargoInfo.LoadingPort;
            txtDischargingPort.EditValue = _tradeInfo.CargoInfo.DischargingPort;
            txtAddress.EditValue = _tradeInfo.CargoInfo.Address;

            #region Load Legs

            layoutLegsControl.BeginUpdate();
            layoutLegsGroup.Clear();

            foreach(TradeCargoInfoLeg tradeCargoInfoLeg in _tradeInfo.CargoInfo.Legs)
            {
                int counter = Convert.ToInt32(layoutLegsGroup.Items.Count + 1);

                var chkOptional_new = new CheckEdit();
                var cmbStatus_new = new ComboBoxEdit();
                var txtIndexPerc_new = new SpinEdit();
                var lookupIndex_new = new LookUpEdit();
                var txtRate_new = new SpinEdit();
                var cmbRateType_new = new ComboBoxEdit();
                var dtpPeriodTo_new = new DateEdit();
                var dtpPeriodFrom_new = new DateEdit();
                var txtQuantity_new = new SpinEdit();
                var txtQuantityVariation_new = new SpinEdit();
                var txtTce_new = new SpinEdit();

                //
                // chkOptional
                //
                chkOptional_new.Name = "chkIsOptional_" + counter;
                chkOptional_new.CheckedChanged += new EventHandler(ChkOptionalNewCheckedChanged);
                chkOptional_new.Text = "";

                // 
                // cmbStatus
                // 
                cmbStatus_new.Name = "cmbStatus_" + counter;
                cmbStatus_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbStatus_new.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.Pending);
                cmbStatus_new.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.Declared);
                cmbStatus_new.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.NonDeclared);
                cmbStatus_new.Properties.ReadOnly = true;


                // 
                // txtIndexPerc
                // 
                txtIndexPerc_new.Name = "txtIndexPerc_" + counter;
                txtIndexPerc_new.Properties.EditMask = "P2";
                txtIndexPerc_new.Properties.MinValue = 0;
                txtIndexPerc_new.Properties.MaxValue = 999;
                txtIndexPerc_new.Properties.MaxLength = 6;
                txtIndexPerc_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                //txtIndexPerc_new.Value = 100;
                txtIndexPerc_new.Properties.ReadOnly = true;
                //   txtIndexPerc_new.TabIndex = 7;

                // 
                // lookupIndex
                // 
                lookupIndex_new.Properties.ReadOnly = true;
                lookupIndex_new.Name = "lookupIndex_" + counter;
                lookupIndex_new.Properties.DisplayMember = "Name";
                lookupIndex_new.Properties.ValueMember = "Id";
                var col = new LookUpColumnInfo
                {
                    Caption = Strings.Market_Index,
                    FieldName = "Name",
                    Width = 0
                };
                lookupIndex_new.Properties.Columns.Add(col);
                lookupIndex_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                lookupIndex_new.Properties.SearchMode = SearchMode.AutoFilter;
                lookupIndex_new.Properties.NullText = "";
                lookupIndex_new.Properties.NullValuePrompt =
                    Strings.Select_a_Market_Index___;
                if (_market != null)
                {
                    if (lookUpIndex_1.Tag != null)
                        lookupIndex_new.Properties.DataSource =
                            ((List<Index>)lookUpIndex_1.Tag).Where(a => a.MarketId == _market.Id).ToList();
                }
                //    cmbIndex_new.TabIndex = 8;

                // 
                // txtRate
                // 
                txtRate_new.Name = "txtRate_" + counter;
                txtRate_new.Properties.ReadOnly = false;
                txtRate_new.Properties.EditMask = "f2";
                txtRate_new.Properties.MinValue = 0;
                txtRate_new.Properties.MaxValue = 0;
                txtRate_new.Properties.MaxLength = 9;
                txtRate_new.Value = 1;
                //   txtRate_new.TabIndex = 7;

                // 
                // cmbRateType
                // 
                cmbRateType_new.Name = "cmbRateType_" + counter;
                cmbRateType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                cmbRateType_new.Properties.Items.Add(TradeInfoLegRateTypeEnum.Fixed);
                cmbRateType_new.Properties.Items.Add(TradeInfoLegRateTypeEnum.IndexLinked);
                cmbRateType_new.SelectedItem = TradeInfoLegRateTypeEnum.Fixed;
                cmbRateType_new.SelectedIndexChanged += CmbRateType1SelectedIndexChanged;
                //    cmbRateType_new.TabIndex = 6;

                // 
                // dtpPeriodTo
                // 
                dtpPeriodTo_new.EditValue = null;
                dtpPeriodTo_new.Name = "dtpPeriodTo_" + counter;
                //    dtpPeriodTo_new.TabIndex = 5;

                // 
                // dtpPeriodFrom
                // 
                dtpPeriodFrom_new.EditValue = null;
                dtpPeriodFrom_new.Name = "dtpPeriodFrom_" + counter;
                //    dtpPeriodFrom_new.TabIndex = 4;

                // 
                // txtQuantity
                // 
                txtQuantity_new.Name = "txtQuantity_" + counter;
                txtQuantity_new.Properties.EditMask = "D";
                txtQuantity_new.Properties.MinValue = 1;
                txtQuantity_new.Properties.MaxValue = 500000;
                txtQuantity_new.Properties.MaxLength = 6;
                txtQuantity_new.Value = 0;

                // 
                // txtQuantityVariation
                // 
                txtQuantityVariation_new.Name = "txtQuantityVariation_" + counter;
                txtQuantityVariation_new.Properties.EditMask = "P0";
                txtQuantityVariation_new.Properties.MinValue = 1;
                txtQuantityVariation_new.Properties.MaxValue = 999;
                txtQuantityVariation_new.Properties.MaxLength = 3;
                txtQuantityVariation_new.Properties.Mask.UseMaskAsDisplayFormat = true;
                txtQuantityVariation_new.Value = 100;

                // 
                // txtTce
                // 
                txtTce_new.Name = "txtTce_" + counter;
                txtTce_new.Properties.ReadOnly = false;
                txtTce_new.Properties.EditMask = "f2";
                txtTce_new.Properties.MinValue = 0;
                txtTce_new.Properties.MaxValue = 0;
                txtTce_new.Properties.MaxLength = 9;
                txtTce_new.Value = 0;

                LayoutControlGroup layoutLeg_new = layoutLegsGroup.AddGroup();

                // 
                // layoutLeg
                // 
                layoutLeg_new.GroupBordersVisible = false;
                layoutLeg_new.Name = "layoutLeg_" + counter;
                layoutLeg_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
                layoutLeg_new.Tag = counter;
                layoutLeg_new.Text = "layoutLeg";
                layoutLeg_new.TextVisible = false;
                layoutLeg_new.DefaultLayoutType = LayoutType.Horizontal;
                // 
                // layoutPeriodFrom
                // 
                LayoutControlItem layoutPeriodFrom_new = layoutLeg_new.AddItem();
                layoutPeriodFrom_new.Control = dtpPeriodFrom_new;
                layoutPeriodFrom_new.ControlMaxSize = new Size(99, 20);
                layoutPeriodFrom_new.ControlMinSize = new Size(99, 20);
                layoutPeriodFrom_new.Name = "layoutPeriodFrom_" + counter;
                layoutPeriodFrom_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutPeriodFrom_new.Text = "Period From:";
                // 
                // layoutPeriodTo
                // 
                LayoutControlItem layoutPeriodTo_new = layoutLeg_new.AddItem();
                layoutPeriodTo_new.Control = dtpPeriodTo_new;
                layoutPeriodTo_new.ControlMaxSize = new Size(99, 20);
                layoutPeriodTo_new.ControlMinSize = new Size(99, 20);
                layoutPeriodTo_new.Name = "layoutPeriodTo_" + counter;
                layoutPeriodTo_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutPeriodTo_new.Text = "Period To:";
                // 
                // layoutRateType
                // 
                LayoutControlItem layoutRateType_new = layoutLeg_new.AddItem();
                layoutRateType_new.Control = cmbRateType_new;
                layoutRateType_new.ControlMaxSize = new Size(99, 20);
                layoutRateType_new.ControlMinSize = new Size(99, 20);
                layoutRateType_new.Name = "layoutRateType_" + counter;
                layoutRateType_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutRateType_new.Text = "Rate Type:";

                // 
                // layoutRate
                // 
                LayoutControlItem layoutRate_new = layoutLeg_new.AddItem();
                layoutRate_new.Control = txtRate_new;
                layoutRate_new.ControlMaxSize = new Size(79, 20);
                layoutRate_new.ControlMinSize = new Size(79, 20);
                layoutRate_new.Name = "layoutRate_" + counter;
                layoutRate_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutRate_new.Text = "Rate($/day):";

                // 
                // layoutTce
                // 
                LayoutControlItem layoutTce_new = layoutLeg_new.AddItem();
                layoutTce_new.Control = txtTce_new;
                layoutTce_new.ControlMaxSize = new Size(79, 20);
                layoutTce_new.ControlMinSize = new Size(79, 20);
                layoutTce_new.Name = "layoutTce_" + counter;
                layoutTce_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutTce_new.Text = "TCE($/day):";

                // 
                // layoutIndex
                // 
                LayoutControlItem layoutIndex_new = layoutLeg_new.AddItem();
                layoutIndex_new.Control = lookupIndex_new;
                layoutIndex_new.ControlMaxSize = new Size(100, 20);
                layoutIndex_new.ControlMinSize = new Size(100, 20);
                layoutIndex_new.Name = "layoutIndex_" + counter;
                layoutIndex_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutIndex_new.Text = "Index:";

                // 
                // layoutIndexPercentage
                // 
                LayoutControlItem layoutIndexPerc_new = layoutLeg_new.AddItem();
                layoutIndexPerc_new.Control = txtIndexPerc_new;
                layoutIndexPerc_new.ControlMaxSize = new Size(54, 20);
                layoutIndexPerc_new.ControlMinSize = new Size(54, 20);
                layoutIndexPerc_new.Name = "layoutIndexPerc_" + counter;
                layoutIndexPerc_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutIndexPerc_new.Text = "Index Perc.:";

                // 
                // layoutQuantity
                // 
                LayoutControlItem layoutQuantity_new = layoutLeg_new.AddItem();
                layoutQuantity_new.Control = txtQuantity_new;
                layoutQuantity_new.ControlMaxSize = new Size(0, 20);
                layoutQuantity_new.ControlMinSize = new Size(54, 20);
                layoutQuantity_new.Name = "layoutQuantity_" + counter;
                layoutQuantity_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutQuantity_new.Text = "Quantity (tons):";

                // 
                // layoutQuantityVariation
                // 
                LayoutControlItem layoutQuantityVariation_new = layoutLeg_new.AddItem();
                layoutQuantityVariation_new.Control = txtQuantityVariation_new;
                layoutQuantityVariation_new.ControlMaxSize = new Size(54, 20);
                layoutQuantityVariation_new.ControlMinSize = new Size(54, 20);
                layoutQuantityVariation_new.Name = "layoutQuantityVariation_" + counter;
                layoutQuantityVariation_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutQuantityVariation_new.Text = "Quant. Variation:";

                // 
                // layouchkIsOptional
                // 
                LayoutControlItem layouchkIsOptional_new = layoutLeg_new.AddItem();
                layouchkIsOptional_new.Control = chkOptional_new;
                layouchkIsOptional_new.ControlMaxSize = new Size(0, 19);
                layouchkIsOptional_new.ControlMinSize = new Size(17, 19);
                layouchkIsOptional_new.Name = "layoutOptional_" + counter;
                layouchkIsOptional_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layouchkIsOptional_new.Text = "Optional:";
                layouchkIsOptional_new.TextAlignMode = TextAlignModeItem.CustomSize;
                layouchkIsOptional_new.TextToControlDistance = 1;
                // 
                // layoutStatus
                // 
                LayoutControlItem layoutStatus_new = layoutLeg_new.AddItem();
                layoutStatus_new.Control = cmbStatus_new;
                layoutStatus_new.ControlMaxSize = new Size(90, 20);
                layoutStatus_new.ControlMinSize = new Size(90, 20);
                layoutStatus_new.Name = "layoutStatus_" + counter;
                layoutStatus_new.Text = "Status:";
                layoutStatus_new.SizeConstraintsType = SizeConstraintsType.Custom;

                chkOptional_new.EditValue = tradeCargoInfoLeg.IsOptional;
                cmbStatus_new.EditValue = tradeCargoInfoLeg.OptionalStatus;
                txtIndexPerc_new.EditValue = tradeCargoInfoLeg.IndexPercentage;
                lookupIndex_new.EditValue = tradeCargoInfoLeg.IndexId;
                txtRate_new.EditValue = tradeCargoInfoLeg.Rate;
                cmbRateType_new.EditValue = tradeCargoInfoLeg.RateType;
                dtpPeriodTo_new.EditValue = tradeCargoInfoLeg.PeriodTo;
                dtpPeriodFrom_new.EditValue = tradeCargoInfoLeg.PeriodFrom;
                txtQuantity_new.EditValue = tradeCargoInfoLeg.Quantity;
                txtQuantityVariation_new.EditValue = tradeCargoInfoLeg.QuantityVariation;
                txtTce_new.EditValue = tradeCargoInfoLeg.Tce;
            }
            layoutLegsControl.EndUpdate();
            layoutLegsControl.BestFit();

            #endregion
        }

        public void SetReadOnlyState()
        {
            cmbType.Properties.ReadOnly = true;
            lookupVessel.Properties.ReadOnly = true;
            txtVesselIndex.Properties.ReadOnly = true;
            txtLoadingPort.Properties.ReadOnly = true;
            txtDischargingPort.Properties.ReadOnly = true;
            txtAddress.Properties.ReadOnly = true;

            foreach (object item in layoutLegsGroup.Items)
            {
                if (item.GetType() == typeof (LayoutControlGroup))
                {
                    var layoutLeg = (LayoutControlGroup) item;
                    if (layoutLeg.Name.StartsWith("layoutLeg_"))
                    {
                        string layoutLegSuffix = layoutLeg.Name.Substring(layoutLeg.Name.IndexOf("_") + 1);
                        ((DateEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutPeriodFrom_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((DateEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutPeriodTo_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((ComboBoxEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutRateType_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((SpinEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutRate_" + layoutLegSuffix)).Control).
                            Properties.ReadOnly = true;
                        ((LookUpEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutIndex_" + layoutLegSuffix)).Control).
                            Properties.ReadOnly = true;
                        ((CheckEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutOptional_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((ComboBoxEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutStatus_" + layoutLegSuffix)).Control).
                            Properties.ReadOnly = true;
                        ((SpinEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutIndexPerc_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((SpinEdit)
                         ((LayoutControlItem) layoutLeg.Items.FindByName("layoutQuantity_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((SpinEdit)
                         ((LayoutControlItem)layoutLeg.Items.FindByName("layoutTce_" + layoutLegSuffix)).
                             Control).Properties.ReadOnly = true;
                        ((SpinEdit)
                         ((LayoutControlItem)
                          layoutLeg.Items.FindByName("layoutQuantityVariation_" + layoutLegSuffix)).Control).Properties.
                            ReadOnly = true;
                    }
                }
            }
        }

        #endregion

        #region GUI Events

        private void CmbRateType1SelectedIndexChanged(object sender, EventArgs e)
        {
            var comboRateType = (ComboBoxEdit)sender;
            if (!comboRateType.Name.Contains("_"))
            {
                txtRate_1.Value = 0;
                lookUpIndex_1.EditValue = null;

                if (((TradeInfoLegRateTypeEnum)cmbRateType_1.SelectedItem) == TradeInfoLegRateTypeEnum.Fixed)
                {
                    txtRate_1.Properties.ReadOnly = false;
                    lookUpIndex_1.Properties.ReadOnly = true;
                    txtIndexPerc_1.Properties.ReadOnly = true;
                }
                else
                {
                    txtRate_1.Properties.ReadOnly = true;
                    lookUpIndex_1.Properties.ReadOnly = false;
                    txtIndexPerc_1.Properties.ReadOnly = false;
                }
            }
            else
            {
                int counter = Convert.ToInt32(comboRateType.Name.Substring(comboRateType.Name.IndexOf("_") + 1));
                SpinEdit spinLegRate = null;
                LookUpEdit lookupLegIndex = null;
                SpinEdit spinIndexPerc = null;

                foreach (object item in layoutLegsGroup.Items)
                {
                    if (item.GetType() == typeof(LayoutControlGroup))
                    {
                        var layoutLeg = (LayoutControlGroup)item;
                        if (layoutLeg.Name == "layoutLeg_" + counter)
                        {
                            foreach (object item1 in layoutLeg.Items)
                            {
                                if (item1.GetType() == typeof(LayoutControlItem))
                                {
                                    var layoutItem = (LayoutControlItem)item1;
                                    if (layoutItem.Name == "layoutRate_" + counter)
                                    {
                                        spinLegRate = (SpinEdit)layoutItem.Control;
                                    }
                                    else if (layoutItem.Name == "layoutIndex_" + counter)
                                    {
                                        lookupLegIndex = (LookUpEdit)layoutItem.Control;
                                    }
                                    else if(layoutItem.Name == "layoutIndexPerc_" + counter)
                                    {
                                        spinIndexPerc = (SpinEdit) layoutItem.Control;
                                    }
                                }
                            }
                        }
                    }
                }

                if (((TradeInfoLegRateTypeEnum)comboRateType.SelectedItem) == TradeInfoLegRateTypeEnum.Fixed)
                {
                    spinLegRate.Properties.ReadOnly = false;
                    lookupLegIndex.Properties.ReadOnly = true;
                    spinIndexPerc.Properties.ReadOnly = true;
                    lookupLegIndex.EditValue = null;
                    spinIndexPerc.EditValue = null;
                }
                else
                {
                    if (_formActionType == FormActionTypeEnum.Add)
                    {
                        spinLegRate.Value = 0;
                        spinIndexPerc.Value = 100;
                    }
                    spinLegRate.Properties.ReadOnly = true;
                    spinIndexPerc.Properties.ReadOnly = false;
                    lookupLegIndex.Properties.ReadOnly = false;
                }
            }
        }

        private void BtnAddLegClick(object sender, EventArgs e)
        {
            layoutLegsControl.BeginUpdate();

            int counter = Convert.ToInt32(layoutLegsGroup.Items[layoutLegsGroup.Items.Count - 1].Tag) + 1;

            var chkOptional_new = new CheckEdit();
            var cmbStatus_new = new ComboBoxEdit();
            var txtIndexPerc_new = new SpinEdit();
            var lookupIndex_new = new LookUpEdit();
            var txtRate_new = new SpinEdit();
            var cmbRateType_new = new ComboBoxEdit();
            var dtpPeriodTo_new = new DateEdit();
            var dtpPeriodFrom_new = new DateEdit();
            var txtQuantity_new = new SpinEdit();
            var txtQuantityVariation_new = new SpinEdit();
            var txtTce_new = new SpinEdit(); 

            //
            // chkOptional
            //
            chkOptional_new.Name = "chkIsOptional_" + counter;
            chkOptional_new.CheckedChanged += new EventHandler(ChkOptionalNewCheckedChanged);
            chkOptional_new.Text = "";

            // 
            // cmbStatus
            // 
            cmbStatus_new.Name = "cmbStatus_" + counter;
            cmbStatus_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbStatus_new.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.Pending);
            cmbStatus_new.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.Declared);
            cmbStatus_new.Properties.Items.Add(TradeInfoLegOptionalStatusEnum.NonDeclared);
            cmbStatus_new.Properties.ReadOnly = true;

            
            // 
            // txtIndexPerc
            // 
            txtIndexPerc_new.Name = "txtIndexPerc_" + counter;
            txtIndexPerc_new.Properties.EditMask = "P2";
            txtIndexPerc_new.Properties.MinValue = 0;
            txtIndexPerc_new.Properties.MaxValue = 999;
            txtIndexPerc_new.Properties.MaxLength = 6;
            txtIndexPerc_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtIndexPerc_new.Value = 100;
            txtIndexPerc_new.Properties.ReadOnly = true;
            //   txtIndexPerc_new.TabIndex = 7;
            
            // 
            // lookupIndex
            // 
            lookupIndex_new.Properties.ReadOnly = true;
            lookupIndex_new.Name = "lookupIndex_" + counter;
            lookupIndex_new.Properties.DisplayMember = "Name";
            lookupIndex_new.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupIndex_new.Properties.Columns.Add(col);
            lookupIndex_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupIndex_new.Properties.SearchMode = SearchMode.AutoFilter;
            lookupIndex_new.Properties.NullText = "";
            lookupIndex_new.Properties.NullValuePrompt =
                Strings.Select_a_Market_Index___;
            if (_market != null)
            {
                if (lookUpIndex_1.Tag != null)
                    lookupIndex_new.Properties.DataSource =
                        ((List<Index>)lookUpIndex_1.Tag).Where(a => a.MarketId == _market.Id).ToList();
            }
            //    cmbIndex_new.TabIndex = 8;
            
            // 
            // txtRate
            // 
            txtRate_new.Name = "txtRate_" + counter;
            txtRate_new.Properties.ReadOnly = false;
            txtRate_new.Properties.EditMask = "f2";
            txtRate_new.Properties.MinValue = 0;
            txtRate_new.Properties.MaxValue = 0;
            txtRate_new.Properties.MaxLength = 8;
            txtRate_new.Value = 1;
            //   txtRate_new.TabIndex = 7;
            
            // 
            // cmbRateType
            // 
            cmbRateType_new.Name = "cmbRateType_" + counter;
            cmbRateType_new.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cmbRateType_new.Properties.Items.Add(TradeInfoLegRateTypeEnum.Fixed);
            cmbRateType_new.Properties.Items.Add(TradeInfoLegRateTypeEnum.IndexLinked);
            cmbRateType_new.SelectedItem = TradeInfoLegRateTypeEnum.Fixed;
            cmbRateType_new.SelectedIndexChanged += CmbRateType1SelectedIndexChanged;
            //    cmbRateType_new.TabIndex = 6;
            
            // 
            // dtpPeriodTo
            // 
            dtpPeriodTo_new.EditValue = null;
            dtpPeriodTo_new.Name = "dtpPeriodTo_" + counter;
            //    dtpPeriodTo_new.TabIndex = 5;
            
            // 
            // dtpPeriodFrom
            // 
            dtpPeriodFrom_new.EditValue = null;
            dtpPeriodFrom_new.Name = "dtpPeriodFrom_" + counter;
            //    dtpPeriodFrom_new.TabIndex = 4;

            // 
            // txtQuantity
            // 
            txtQuantity_new.Name = "txtQuantity_" + counter;
            txtQuantity_new.Properties.EditMask = "D";
            txtQuantity_new.Properties.MinValue = 1;
            txtQuantity_new.Properties.MaxValue = 500000;
            txtQuantity_new.Properties.MaxLength = 6;
            txtQuantity_new.Value = 0;

            // 
            // txtQuantityVariation
            // 
            txtQuantityVariation_new.Name = "txtQuantityVariation_" + counter;
            txtQuantityVariation_new.Properties.EditMask = "P0";
            txtQuantityVariation_new.Properties.MinValue = 1;
            txtQuantityVariation_new.Properties.MaxValue = 999;
            txtQuantityVariation_new.Properties.MaxLength = 3;
            txtQuantityVariation_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtQuantityVariation_new.Value = 100;

            // 
            // txtTce
            // 
            txtTce_new.Name = "txtTce_" + counter;
            txtTce_new.Properties.ReadOnly = false;
            txtTce_new.Properties.EditMask = "f2";
            txtTce_new.Properties.MinValue = 0;
            txtTce_new.Properties.MaxValue = 0;
            txtTce_new.Properties.MaxLength = 9;
            txtTce_new.Value = 0;

            LayoutControlGroup layoutLeg_new = layoutLegsGroup.AddGroup();

            // 
            // layoutLeg
            // 
            layoutLeg_new.GroupBordersVisible = false;
            layoutLeg_new.Name = "layoutLeg_" + counter;
            layoutLeg_new.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            layoutLeg_new.Tag = counter;
            layoutLeg_new.Text = "layoutLeg";
            layoutLeg_new.TextVisible = false;
            layoutLeg_new.DefaultLayoutType = LayoutType.Horizontal;
            // 
            // layoutPeriodFrom
            // 
            LayoutControlItem layoutPeriodFrom_new = layoutLeg_new.AddItem();
            layoutPeriodFrom_new.Control = dtpPeriodFrom_new;
            layoutPeriodFrom_new.ControlMaxSize = new Size(99, 20);
            layoutPeriodFrom_new.ControlMinSize = new Size(99, 20);
            layoutPeriodFrom_new.Name = "layoutPeriodFrom_" + counter;
            layoutPeriodFrom_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutPeriodFrom_new.Text = "Period From:";
            // 
            // layoutPeriodTo
            // 
            LayoutControlItem layoutPeriodTo_new = layoutLeg_new.AddItem();
            layoutPeriodTo_new.Control = dtpPeriodTo_new;
            layoutPeriodTo_new.ControlMaxSize = new Size(99, 20);
            layoutPeriodTo_new.ControlMinSize = new Size(99, 20);
            layoutPeriodTo_new.Name = "layoutPeriodTo_" + counter;
            layoutPeriodTo_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutPeriodTo_new.Text = "Period To:";
            // 
            // layoutRateType
            // 
            LayoutControlItem layoutRateType_new = layoutLeg_new.AddItem();
            layoutRateType_new.Control = cmbRateType_new;
            layoutRateType_new.ControlMaxSize = new Size(99, 20);
            layoutRateType_new.ControlMinSize = new Size(99, 20);
            layoutRateType_new.Name = "layoutRateType_" + counter;
            layoutRateType_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutRateType_new.Text = "Rate Type:";

            // 
            // layoutRate
            // 
            LayoutControlItem layoutRate_new = layoutLeg_new.AddItem();
            layoutRate_new.Control = txtRate_new;
            layoutRate_new.ControlMaxSize = new Size(79, 20);
            layoutRate_new.ControlMinSize = new Size(79, 20);
            layoutRate_new.Name = "layoutRate_" + counter;
            layoutRate_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutRate_new.Text = "Rate($/day):";

            // 
            // layoutTce
            // 
            LayoutControlItem layoutTce_new = layoutLeg_new.AddItem();
            layoutTce_new.Control = txtTce_new;
            layoutTce_new.ControlMaxSize = new Size(79, 20);
            layoutTce_new.ControlMinSize = new Size(79, 20);
            layoutTce_new.Name = "layoutTce_" + counter;
            layoutTce_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutTce_new.Text = "TCE($/day):";

            // 
            // layoutIndex
            // 
            LayoutControlItem layoutIndex_new = layoutLeg_new.AddItem();
            layoutIndex_new.Control = lookupIndex_new;
            layoutIndex_new.ControlMaxSize = new Size(100, 20);
            layoutIndex_new.ControlMinSize = new Size(100, 20);
            layoutIndex_new.Name = "layoutIndex_" + counter;
            layoutIndex_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutIndex_new.Text = "Index:";
            
            // 
            // layoutIndexPercentage
            // 
            LayoutControlItem layoutIndexPerc_new = layoutLeg_new.AddItem();
            layoutIndexPerc_new.Control = txtIndexPerc_new;
            layoutIndexPerc_new.ControlMaxSize = new Size(54, 20);
            layoutIndexPerc_new.ControlMinSize = new Size(54, 20);
            layoutIndexPerc_new.Name = "layoutIndexPerc_" + counter;
            layoutIndexPerc_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutIndexPerc_new.Text = "Index Perc.:";

            // 
            // layoutQuantity
            // 
            LayoutControlItem layoutQuantity_new = layoutLeg_new.AddItem();
            layoutQuantity_new.Control = txtQuantity_new;
            layoutQuantity_new.ControlMaxSize = new Size(0, 20);
            layoutQuantity_new.ControlMinSize = new Size(54, 20);
            layoutQuantity_new.Name = "layoutQuantity_" + counter;
            layoutQuantity_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutQuantity_new.Text = "Quantity (tons):";

            // 
            // layoutQuantityVariation
            // 
            LayoutControlItem layoutQuantityVariation_new = layoutLeg_new.AddItem();
            layoutQuantityVariation_new.Control = txtQuantityVariation_new;
            layoutQuantityVariation_new.ControlMaxSize = new Size(54, 20);
            layoutQuantityVariation_new.ControlMinSize = new Size(54, 20);
            layoutQuantityVariation_new.Name = "layoutQuantityVariation_" + counter;
            layoutQuantityVariation_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutQuantityVariation_new.Text = "Quant. Variation:";

            // 
            // layouchkIsOptional
            // 
            LayoutControlItem layouchkIsOptional_new = layoutLeg_new.AddItem();
            layouchkIsOptional_new.Control = chkOptional_new;
            layouchkIsOptional_new.ControlMaxSize = new Size(0, 19);
            layouchkIsOptional_new.ControlMinSize = new Size(17, 19);
            layouchkIsOptional_new.Name = "layoutOptional_" + counter;
            layouchkIsOptional_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layouchkIsOptional_new.Text = "Optional:";
            layouchkIsOptional_new.TextAlignMode = TextAlignModeItem.CustomSize;
            layouchkIsOptional_new.TextToControlDistance = 1;
            // 
            // layoutStatus
            // 
            LayoutControlItem layoutStatus_new = layoutLeg_new.AddItem();
            layoutStatus_new.Control = cmbStatus_new;
            layoutStatus_new.ControlMaxSize = new Size(90, 20);
            layoutStatus_new.ControlMinSize = new Size(90, 20);
            layoutStatus_new.Name = "layoutStatus_" + counter;
            layoutStatus_new.Text = "Status:";
            layoutStatus_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutLegsControl.EndUpdate();
            layoutLegsControl.BestFit();
        }

        private void BtnRemoveLegClick(object sender, EventArgs e)
        {
            if (layoutLegsGroup.Items.Count == 1) return;

            layoutLegsControl.BeginUpdate();

            foreach (
                LayoutControlItem layoutControlItem in
                    ((LayoutControlGroup)layoutLegsGroup.Items[layoutLegsGroup.Items.Count - 1]).Items)
            {
                layoutLegsControl.Controls.Remove(layoutControlItem.Control);
            }

            layoutLegsGroup.RemoveAt(layoutLegsGroup.Items.Count - 1);
            layoutLegsControl.EndUpdate();
        }

        private void ChkIsOptional1CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckEdit)sender).Checked)
            {
                if (_formActionType == FormActionTypeEnum.Add)
                {
                    cmbStatus_1.Properties.ReadOnly = true;
                    cmbStatus_1.SelectedItem = TradeInfoLegOptionalStatusEnum.Pending;
                }
                else if (_formActionType == FormActionTypeEnum.Edit)
                {
                    cmbStatus_1.Properties.ReadOnly = false;
                }
            }
            else
            {
                cmbStatus_1.Properties.ReadOnly = true;
                cmbStatus_1.SelectedItem = null;
            }
        }

        private void ChkOptionalNewCheckedChanged(object sender, EventArgs e)
        {
            string layoutLegSuffix = ((CheckEdit)sender).Name.Replace("chkIsOptional_", "");

            LayoutControlGroup layoutLeg = (LayoutControlGroup)layoutLegsGroup.Items.FindByName("layoutLeg_" + layoutLegSuffix);
            ComboBoxEdit optionalStatus = (ComboBoxEdit)((LayoutControlItem)layoutLeg.Items.FindByName("layoutStatus_" + layoutLegSuffix)).Control;

            if (((CheckEdit)sender).Checked)
            {
                if (_formActionType == FormActionTypeEnum.Add)
                {
                    optionalStatus.Properties.ReadOnly = true;
                    optionalStatus.SelectedItem = TradeInfoLegOptionalStatusEnum.Pending;
                }
                else if (_formActionType == FormActionTypeEnum.Edit)
                {
                    optionalStatus.Properties.ReadOnly = false;
                }
            }
            else
            {
                optionalStatus.Properties.ReadOnly = true;
                optionalStatus.SelectedItem = null;
            }
        }

        private void LookupVesselEditValueChanged(object sender, EventArgs e)
        {
            SetVesselIndex();
        }

        #endregion

    }
}
