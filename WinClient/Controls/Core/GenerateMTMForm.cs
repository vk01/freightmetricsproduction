﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Itenso.TimePeriod;
using SummaryItemType = DevExpress.Data.SummaryItemType;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;
using DevExpress.XtraEditors.Controls;

namespace Exis.WinClient.Controls.Core
{
    public partial class GenerateMTMForm : DevExpress.XtraEditors.XtraForm
    {
        #region Nested type: FormStateEnum

        private enum FormStateEnum
        {
            BeforeExecution,
            WhileExecuting,
            AfterExecution
        }
        #endregion

        #region Private Properties

        private AppParameter _appParameter;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm1;
        private WaitDialogForm _dialogForm2;

        private TreeListNode _expandedNode;
        private FormStateEnum _formState = FormStateEnum.BeforeExecution;
        private bool isLoad;

        private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<DateTime, decimal>> _tradeMTMResults = new Dictionary<string, Dictionary<DateTime, decimal>>();
        private Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMRatiosResults = new Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>();
        private Dictionary<string, decimal> _tradeEmbeddedValues = new Dictionary<string, decimal>();
        private Dictionary<DateTime, Dictionary<string, decimal>> _indexesValues = new Dictionary<DateTime, Dictionary<string, decimal>>();

        private Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>> mtmCalculationsPerIndex = new Dictionary<string, Dictionary<string, Dictionary<string, decimal?>>>();

        Dictionary<string, decimal> sumIndex = new Dictionary<string, decimal>();
        Dictionary<string, decimal> sumIndexCounter = new Dictionary<string, decimal>();

        // private bool RatioReportPL;
        // private bool RatioReportCashFlow;
        private bool InSelection;


        //params for query 
        private DateTime _positionDate;
        private DateTime _periodFrom;
        private DateTime _periodTo;
        private List<string> _tradeIdentifiers;
        private List<TradeInformation> _tradeInformations;
        private List<Book> _books;
        private HierarchyNodeInfo _selectedHierarchyNode;

        //params for info
        private ViewTradeGeneralInfo _viewTradesGeneralInfo;

        #endregion

        #region Constructors

        public GenerateMTMForm(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            _viewTradesGeneralInfo = viewTradesGeneralInfo;

            InitializeComponent();
            SetGuiControls();
            InitializeControls();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {

            if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject == null)
                lblEntity.Text = _viewTradesGeneralInfo.selectedHierarchyNode.StrObject;
            else
            {
                if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Company))
                    lblEntity.Text = "Company" + "-" +
                                     ((Company)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Book))
                    lblEntity.Text = "Book" + "-" +
                                     ((Book)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Market))
                    lblEntity.Text = "Market" + "-" +
                                     ((Market)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Trader))
                    lblEntity.Text = "Trader" + "-" +
                                     ((Trader)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Vessel))
                    lblEntity.Text = "Vessel" + "-" +
                                     ((Vessel)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
            }


            lblPeriodFrom.Text = _viewTradesGeneralInfo.periodFrom.ToShortDateString();
            lblPeriodTo.Text = _viewTradesGeneralInfo.periodTo.ToShortDateString();
            lblIsDraft.Text = _viewTradesGeneralInfo.IsDraft.ToString();
            lblIsMinimumPeriod.Text = _viewTradesGeneralInfo.IsMinimumPeriod.ToString();
            lblIsOptionalPeriod.Text = _viewTradesGeneralInfo.IsOptionalPeriod.ToString();
            lblOnlyNonZeroTotalDays.Text = _viewTradesGeneralInfo.OnlyNonZeroTotalDays.ToString();


            int gridViewColumnindex = 0;
            GridColumn column = AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                          UnboundColumnType.Bound, false, null);
            column.SummaryItem.SummaryType = SummaryItemType.Count;
            column.SummaryItem.DisplayFormat = "Count: {0:N0}";

            gridTradeActionResults.BeginUpdate();
            gridTradeActionResultsView.Columns.Clear();
            gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;

            gridTradeActionResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Books", Strings.Books, gridViewColumnindex++,
                                                             UnboundColumnType.String, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                             UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                                                             UnboundColumnType.String, false, null));

            try
            {
                gridTradeActionResultsView.RestoreLayoutFromXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }

            gridTradeActionResults.EndUpdate();


            var col = new LookUpColumnInfo
            {
                Caption = Strings.Model_name,
                FieldName = "Name",
                Width = 0
            };

            //dtpFilterCurveDate.Properties.MaxValue = DateTime.Today.AddDays(-1);
            //dtpFilterCurveDate.DateTime = DateTime.Today.AddDays(-1);
            dtpFilterCurveDate.DateTime = _viewTradesGeneralInfo.lastImportDate;
            dtpFilterCurveDate.Properties.MaxValue = _viewTradesGeneralInfo.lastImportDate;

            cmbPositionMethod.SelectedItem = "Dynamic";
            cmbMarketSensitivityType.SelectedIndex = 0;

            rdgResultsAggregationType.SelectedIndex = 0;
            rdgPositionResultsType.SelectedIndex = 0;
            rdgMTMResultsType.SelectedIndex = 0;
        }

        private void SetGuiControls()
        {
            if (_formState == FormStateEnum.BeforeExecution)
            {
                btnGeneratePosition.Enabled = true;
                gridTradeActionResults.DataSource = null;
                SetEnabledForWorkingStatus(true);
                btnExportResults.Enabled = false;
            }
            else if (_formState == FormStateEnum.WhileExecuting)
            {

                btnGeneratePosition.Enabled = false;

                SetEnabledForWorkingStatus(true);
                btnExportResults.Enabled = false;
            }
            else if (_formState == FormStateEnum.AfterExecution)
            {
                btnGeneratePosition.Enabled = true;
                SetEnabledForWorkingStatus(true);
                if (gridTradeActionResultsView.DataRowCount > 0) btnExportResults.Enabled = true;

            }
        }

        private GridColumn AddColumn(string name, string caption, int columnIndex, UnboundColumnType columnType, bool isFixedLeft, object Tag)
        {
            var column = new GridColumn();
            column.Visible = true;
            column.AppearanceCell.Options.UseTextOptions = true;
            column.OptionsColumn.FixedWidth = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsColumn.AllowEdit = false;
            column.FieldName = name;
            column.Caption = caption;
            column.Tag = Tag;
            column.Fixed = isFixedLeft ? FixedStyle.Left : FixedStyle.None;
            column.VisibleIndex = columnIndex;
            if (columnType != UnboundColumnType.Bound)
                column.UnboundType = columnType;

            switch (columnType)
            {
                case UnboundColumnType.String:
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.DateTime:
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.DisplayFormat.FormatType = FormatType.DateTime;
                    column.DisplayFormat.FormatString = "G";
                    break;
                case UnboundColumnType.Decimal:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.UnboundType = UnboundColumnType.Decimal;
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "N3";
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Integer:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.UnboundType = UnboundColumnType.Integer;
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Boolean:
                    column.UnboundType = UnboundColumnType.Boolean;
                    column.OptionsColumn.AllowEdit = false;
                    break;
            }

            return column;
        }

        private void CreateResultGridColumns(Dictionary<string, Dictionary<DateTime, decimal>> tradeResults, Dictionary<string, decimal> tradeEmbeddedValues, string type)
        {
            var groupedColumns = gridTradeActionResultsView.GroupedColumns;

            gridTradeActionResults.BeginUpdate();
            gridTradeActionResults.DataSource = null;
            gridTradeActionResultsView.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
            gridTradeActionResultsView.Columns.Clear();
            gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;
            int gridViewColumnindex = 0;

            if (type == "MTM" || type == "POSITION")
            {
                gridTradeActionResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Books", Strings.Books, gridViewColumnindex++,
                                                                UnboundColumnType.String, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve, gridViewColumnindex++,
                                                                 UnboundColumnType.Bound, false, null));
                GridColumn lastColumn = AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++, UnboundColumnType.String, false, null);
                gridTradeActionResultsView.Columns.Add(lastColumn);

                if (type == "MTM")
                {
                    GridColumn column = AddColumn(
                        "EmbeddedValue",
                        "Embedded Value", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, tradeEmbeddedValues);
                    gridTradeActionResultsView.Columns.Add(column);

                    if (chkCalculateSums.Checked)
                    {
                        //Created only to make a void summary item, so that no custom summary is diplayed in that cell
                        var dummySummarytem = new GridColumnSummaryItem();
                        dummySummarytem.DisplayFormat = "Totals:";
                        dummySummarytem.FieldName = "dummie";
                        dummySummarytem.SummaryType = SummaryItemType.Custom;
                        column.Summary.Add(dummySummarytem);
                    }
                    lastColumn = column;
                }

                List<DateTime> dates = tradeResults.First().Value.Select(a => a.Key).ToList();               

                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    foreach (DateTime date in dates)
                    {
                        GridColumn column = AddColumn(
                            date.ToString("MMM-yyyy", new CultureInfo("en-GB")),
                            date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                            UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                        gridTradeActionResultsView.Columns.Add(column);
                        if (chkCalculateSums.Checked)
                        {
                            var item = new GridGroupSummaryItem();
                            item.FieldName = column.FieldName;
                            item.ShowInGroupColumnFooter = column;
                            item.SummaryType = SummaryItemType.Sum;
                            item.DisplayFormat = "{0:N3}";
                            gridTradeActionResultsView.GroupSummary.Add(item);

                            column.SummaryItem.SummaryType = SummaryItemType.Sum;
                            column.SummaryItem.DisplayFormat = "{0:N3}";
                        }
                        if (type == "MTM")
                        {
                            DateTime dt = (DateTime)column.Tag;
                            foreach (var pair in _indexesValues[dt])
                            {
                                if (string.IsNullOrEmpty(pair.Key)) continue;

                                var item = new GridColumnSummaryItem();
                                item.FieldName = pair.Key;
                                item.SummaryType = SummaryItemType.Custom;
                                item.DisplayFormat = "{0:N3}";
                                item.Tag = pair.Key +"___"+ dt.ToString("MMM-yyyy", new CultureInfo("en-GB")); //dt.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                                column.Summary.Add(item);
                            }
                        }
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                {
                    int quarterAdded = 0;
                    foreach (DateTime date in dates)
                    {
                        int intQuarter = ((date.Month - 1) / 3) + 1;
                        if (intQuarter != quarterAdded)
                        {
                            quarterAdded = intQuarter;
                            string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                            GridColumn column = AddColumn(
                                strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")),
                                strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                                UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                            gridTradeActionResultsView.Columns.Add(column);
                            if (chkCalculateSums.Checked)
                            {
                                var item = new GridGroupSummaryItem();
                                item.FieldName = column.FieldName;
                                item.ShowInGroupColumnFooter = column;
                                item.SummaryType = SummaryItemType.Sum;
                                item.DisplayFormat = "{0:N3}";
                                gridTradeActionResultsView.GroupSummary.Add(item);

                                column.SummaryItem.SummaryType = SummaryItemType.Sum;
                                column.SummaryItem.DisplayFormat = "{0:N3}";
                            }
                            if (type == "MTM")
                            {
                                var dt = (DateTime)column.Tag;
                                foreach (var pair in _indexesValues[dt])
                                {
                                    if (string.IsNullOrEmpty(pair.Key)) continue;

                                    var item = new GridColumnSummaryItem();
                                    item.FieldName = pair.Key;
                                    item.SummaryType = SummaryItemType.Custom;
                                    item.DisplayFormat = "{0:N3}";
                                    item.Tag = pair.Key + "___" + column.FieldName;
                                    column.Summary.Add(item);
                                }
                            }
                        }
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                {
                    int calendarAdded = 0;
                    foreach (DateTime date in dates)
                    {
                        int intCalendar = date.Year;
                        if (intCalendar != calendarAdded)
                        {
                            calendarAdded = intCalendar;

                            GridColumn column = AddColumn(
                                "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")),
                                "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                                UnboundColumnType.Decimal, false, Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                            if (chkCalculateSums.Checked)
                            {
                                var item = new GridGroupSummaryItem();
                                item.FieldName = column.FieldName;
                                item.ShowInGroupColumnFooter = column;
                                item.SummaryType = SummaryItemType.Sum;
                                item.DisplayFormat = "{0:N3}";
                                gridTradeActionResultsView.GroupSummary.Add(item);

                                column.SummaryItem.SummaryType = SummaryItemType.Sum;
                                column.SummaryItem.DisplayFormat = "{0:N3}";
                            }
                            gridTradeActionResultsView.Columns.Add(column);
                            if (type == "MTM")
                            {
                                var dt = (DateTime)column.Tag;
                                foreach (var pair in _indexesValues[dt])
                                {
                                    if (string.IsNullOrEmpty(pair.Key)) continue;

                                    var item = new GridColumnSummaryItem();
                                    item.FieldName = pair.Key;
                                    item.SummaryType = SummaryItemType.Custom;
                                    item.DisplayFormat = "{0:N3}";
                                    item.Tag = pair.Key + "___" + column.FieldName;//column.FieldName;
                                    column.Summary.Add(item);
                                }
                            }
                        }
                    }
                }

                GridColumn columnCommission = null;
                if (type == "MTM" && chkCalculateCommissions.Checked)
                {
                    columnCommission = AddColumn("Commission", "Commission", gridViewColumnindex++, UnboundColumnType.Decimal, false, "Commission");
                    gridTradeActionResultsView.Columns.Add(columnCommission);
                }


                if (chkCalculateSums.Checked)
                {
                    GridColumn columnTotal = AddColumn(
                        "Total",
                        "Total", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, "Total");
                    gridTradeActionResultsView.Columns.Add(columnTotal);

                    var item = new GridGroupSummaryItem();
                    item.FieldName = columnTotal.FieldName;
                    item.ShowInGroupColumnFooter = columnTotal;
                    item.SummaryType = SummaryItemType.Sum;
                    item.DisplayFormat = "{0:N3}";
                    gridTradeActionResultsView.GroupSummary.Add(item);
                    columnTotal.SummaryItem.SummaryType = SummaryItemType.Sum;
                    columnTotal.SummaryItem.DisplayFormat = "{0:N3}";

                    if (type == "MTM" && columnCommission != null)
                    {
                        var commissionSumItem = new GridGroupSummaryItem();
                        commissionSumItem.FieldName = columnCommission.FieldName;
                        commissionSumItem.ShowInGroupColumnFooter = columnCommission;
                        commissionSumItem.SummaryType = SummaryItemType.Sum;
                        commissionSumItem.DisplayFormat = "{0:N3}";
                        gridTradeActionResultsView.GroupSummary.Add(commissionSumItem);
                        columnCommission.SummaryItem.SummaryType = SummaryItemType.Sum;
                        columnCommission.SummaryItem.DisplayFormat = "{0:N3}";
                    }
                }
                if (type == "MTM")
                {
                    var index = _indexesValues.First(a => a.Value.Count > 0).Value;
                    foreach (var pair in index)
                    {
                        var item = new GridColumnSummaryItem();
                        item.FieldName = pair.Key;
                        item.SummaryType = SummaryItemType.Custom;
                        item.DisplayFormat = "{0:N3}";
                        item.Tag = "Index";
                        lastColumn.Summary.Add(item);
                    }
                }
                gridTradeActionResults.Tag = type;
                gridTradeActionResultsView.Tag = tradeResults;

                gridTradeActionResults.DataSource = _viewTradesGeneralInfo.tradeInformations;

            }

            if (groupedColumns.Count > 0)
            {
                foreach (GridColumn groupedColumn in groupedColumns)
                {
                    if (gridTradeActionResultsView.Columns.ColumnByFieldName(groupedColumn.FieldName) != null)
                        gridTradeActionResultsView.Columns[groupedColumn.FieldName].Group();
                }
            }

            gridTradeActionResults.EndUpdate();

            _formState = FormStateEnum.AfterExecution;
            SetGuiControls();
        }

        private void SetEnabledForWorkingStatus(bool status)
        {

        }

        #endregion

        #region GUI Events

        private void MainControl_Load(object sender, EventArgs e)
        {
            isLoad = true;

        }

        private void ViewTradesForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            try
            {
                gridTradeActionResultsView.SaveLayoutToXml(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }
        }

        private void cmbPositionMethod_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedPositionMethod = (string)cmbPositionMethod.SelectedItem;
            if (selectedPositionMethod == "Static")
            {
                chkOptionNullify.Enabled = true;
            }
            else
            {
                chkOptionNullify.Enabled = false;
                chkOptionNullify.EditValue = false;
            }
        }

        private void gridTradeActionResultsView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                if (e.Row is TradeInformation)
                {
                    bool isMTMAndcommissionCalculation = chkCalculateCommissions.Checked &&
                                            (gridTradeActionResults.Tag != null &&
                                             gridTradeActionResults.Tag.ToString() == "MTM");

                    var tradeInformation = (TradeInformation)e.Row;

                    decimal commissionPerMonth = 0;
                    decimal totalCommission = 0;

                    var numOfTradeMotnhsInSelectedPeriod = 0;
                    var firstDay = _periodFrom;
                    var lastDay = _periodTo;

                    DateTime tradeDateInSelectedPeriod = tradeInformation.PeriodFrom.Date;
                    while (tradeDateInSelectedPeriod < firstDay)
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddDays(1);

                    while (tradeDateInSelectedPeriod >= firstDay && tradeDateInSelectedPeriod <= tradeInformation.PeriodTo.Date)
                    {
                        numOfTradeMotnhsInSelectedPeriod++;
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddMonths(1);
                    }

                    if (isMTMAndcommissionCalculation && tradeInformation.TradeInfo.TradeBrokerInfos.Count > 0)
                    {

                        if (tradeInformation.Trade.Type == TradeTypeEnum.Option)
                        {
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                    tradeInformation.TradeInfo.OptionInfo.Premium *
                                                    tradeInformation.TradeInfo.OptionInfo.QuantityDays);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                        else if (tradeInformation.Trade.Type == TradeTypeEnum.FFA)
                        {
                            //JIRA:FREIG-11
                            //commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission/100*
                            //                        tradeInformation.TradeInfo.FFAInfo.Price*
                            //                        tradeInformation.TradeInfo.FFAInfo.QuantityDays);
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                    tradeInformation.TradeInfo.FFAInfo.Price *
                                                    tradeInformation.TradeInfo.FFAInfo.TotalDaysOfTrade);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                    }

                    if (e.Column.FieldName == "Type")
                    {
                        if (tradeInformation.Type == "Option")
                            e.Value = tradeInformation.TradeInfo.OptionInfo.Type.ToString("g");
                    }
                    else if (e.Column.FieldName == "Books")
                    {
                        string books = tradeInformation.TradeInfo.TradeInfoBooks.Aggregate("", (current, tradeInfoBook) =>
                                                                                           current + _viewTradesGeneralInfo.books.Single(a => a.Id == tradeInfoBook.BookId).Name + ", ");
                        e.Value = books.Substring(0, books.LastIndexOf(','));
                    }
                    else if (e.Column.FieldName == "EmbeddedValue")
                    {
                        var embeddedValues = (Dictionary<string, decimal>)e.Column.Tag;
                        e.Value = (embeddedValues == null || embeddedValues.Count == 0 || !embeddedValues.ContainsKey(tradeInformation.Identifier))
                                      ? 0
                                      : embeddedValues[tradeInformation.Identifier];
                    }
                    else if (e.Column.FieldName == "Total")
                    {
                        var tradePositions = (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                        if (isMTMAndcommissionCalculation)
                        {
                            if (rdgPositionResultsType.SelectedIndex == 0) // Days
                            {
                                e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value) - totalCommission;

                            }
                            else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        a.Value /
                                        (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                          new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum() - totalCommission;

                            }
                            else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        (a.Value /
                                         (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                           new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                        tradeInformation.MarketTonnes).Sum() - totalCommission;

                            }
                        }
                        else
                        {
                            if (rdgPositionResultsType.SelectedIndex == 0) // Days
                            {
                                e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value);

                            }
                            else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        a.Value /
                                        (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                          new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();

                            }
                            else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                        (a.Value /
                                         (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                           new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                        tradeInformation.MarketTonnes).Sum();

                            }
                        }
                    }
                    else if (e.Column.FieldName == "Commission" && isMTMAndcommissionCalculation)
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.TC || tradeInformation.Trade.Type == TradeTypeEnum.Cargo
                            || tradeInformation.TradeInfo.TradeBrokerInfos == null || tradeInformation.TradeInfo.TradeBrokerInfos.Count == 0)
                            return;

                        if (tradeInformation.TradeInfo.TradeBrokerInfos.Count > 1)
                        {
                            e.Value = "More than 1 brokers";
                            return;
                        }
                        e.Value = totalCommission;
                    }
                    else
                    {
                        #region POSITION Calculation
                        if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
                        {
                            var tradePositions =
                                (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = tradePositions[tradeInformation.Identifier][date];

                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => a.Value).Single();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = tradePositions[tradeInformation.Identifier][date] /
                                              (((new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)) -
                                                new DateTime(date.Year, date.Month, 1)).Days + 1);


                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => a.Value/(((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) - new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Single();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = (tradePositions[tradeInformation.Identifier][date] /
                                               (((new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)) -
                                                 new DateTime(date.Year, date.Month, 1)).Days + 1)) *
                                              tradeInformation.MarketTonnes;

                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => (a.Value / (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) - new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) * tradeInformation.MarketTonnes).Single();
                                }
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName;
                                        }).Select(
                                                a => a.Value).Sum();

                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                                    ? "Q1"
                                                                    : intQuarter == 2
                                                                          ? "Q2"
                                                                          : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                                a =>
                                                a.Value /
                                                (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                                  new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();

                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                                    ? "Q1"
                                                                    : intQuarter == 2
                                                                          ? "Q2"
                                                                          : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                                a =>
                                                (a.Value /
                                                 (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                                   new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                                tradeInformation.MarketTonnes).Sum();

                                }
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                            a => a.Value).Sum();

                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName)
                                                  .Select(
                                                      a =>
                                                      a.Value /
                                                      (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(
                                                          -1)) -
                                                        new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();

                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName)
                                                  .Select(
                                                      a =>
                                                      (a.Value /
                                                       (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(
                                                           -1)) -
                                                         new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                                      tradeInformation.MarketTonnes).Sum();

                                }
                            }
                        }
                        #endregion

                        else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM" && e.Column.Tag != "Commission")
                        {
                            var tradeMTMs = (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;
                            decimal comm = 0;
                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                var currentDateColumn = (DateTime)e.Column.Tag;

                                var firstDayInCurrMonth = new DateTime(currentDateColumn.Year, currentDateColumn.Month, 1);
                                var lastDayInCurrMonth = firstDayInCurrMonth.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom, tradeInformation.PeriodTo);
                                ITimePeriod monthPeriod = new TimeRange(firstDayInCurrMonth, lastDayInCurrMonth);
                                bool intersects = tradePeriod.IntersectsWith(monthPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    comm = commissionPerMonth;
                                }
                                e.Value = tradeMTMs[tradeInformation.Identifier][(DateTime)e.Column.Tag] - comm;

                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                var currentDateColumn = (DateTime)e.Column.Tag;
                                var currentQuarter = new Quarter(currentDateColumn);

                                var firstDayInQuarter = new DateTime(currentQuarter.FirstMonthStart.Year,
                                                                     currentQuarter.FirstMonthStart.Month, 1);
                                var lastDayInQuarter = currentQuarter.LastMonthStart.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom, tradeInformation.PeriodTo);
                                ITimePeriod quarterPeriod = new TimeRange(firstDayInQuarter, lastDayInQuarter);
                                bool intersects = tradePeriod.IntersectsWith(quarterPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    var tradeRange = new TimeRange(tradePeriod);
                                    var calendarRange = new TimeRange(quarterPeriod);
                                    var selectedDatesRange = new TimeRange(_periodFrom, _periodTo);

                                    var s = selectedDatesRange.GetIntersection(calendarRange).GetIntersection(tradeRange);
                                    DateDiff a = new DateDiff(s.Start, s.End);
                                    var numOfTradeMonthsInCurrQuarter = a.ElapsedDays > 0 ? a.Months + 1 : a.Months;
                                    comm = commissionPerMonth * numOfTradeMonthsInCurrQuarter;
                                }

                                e.Value = tradeMTMs[tradeInformation.Identifier].Where(
                                   a =>
                                   {
                                       int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                       string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                       return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName;
                                   }).Select(
                                           a => a.Value).Sum() - comm;

                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                var currentDate = (DateTime)e.Column.Tag;
                                var currentYear = new Year(currentDate);

                                var firstDayInYear = new DateTime(currentYear.FirstMonthStart.Year,
                                                                     currentYear.FirstMonthStart.Month, 1);
                                var lastDayInYear = currentYear.LastMonthStart.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom, tradeInformation.PeriodTo);
                                ITimePeriod calendarPeriod = new TimeRange(firstDayInYear, lastDayInYear);
                                bool intersects = tradePeriod.IntersectsWith(calendarPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    var tradeRange = new TimeRange(tradePeriod);
                                    var calendarRange = new TimeRange(calendarPeriod);
                                    var selectedDatesRange = new TimeRange(_periodFrom, _periodTo);

                                    var s = selectedDatesRange.GetIntersection(calendarRange).GetIntersection(tradeRange);
                                    DateDiff a = new DateDiff(s.Start, s.End);
                                    var numOfTradeMonthsInCurrYear = a.ElapsedDays > 0 ? a.Months + 1 : a.Months;
                                    comm = commissionPerMonth * numOfTradeMonthsInCurrYear;
                                }

                                e.Value =
                                    tradeMTMs[tradeInformation.Identifier].Where(
                                        a =>
                                        "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName)
                                        .Select(a => a.Value).Sum() - comm;

                            }
                        }
                    }
                }
                else
                {
                    var cashFlowInfo = (CashFlowInformation)e.Row;
                    if (e.Column.Tag.GetType() == typeof(DateTime))
                    {
                        var date = (DateTime)e.Column.Tag;

                        if (cashFlowInfo.Type == CashFlowInformationTypeEnum.Item)
                        {
                            e.Value = cashFlowInfo.Results.First()[date];
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.GroupHeader)
                        {
                            decimal value = 0;
                            //ProcessCashFlowInfo(cashFlowInfo, ref value, date, cashFlowInfo.Type);
                            e.Value = value;
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.GroupFooter)
                        {
                            decimal value = 0;
                            // ProcessCashFlowInfo(cashFlowInfo, ref value, date, cashFlowInfo.Type);
                            e.Value = value;
                        }
                    }
                    else if (e.Column.FieldName == "Totals" && cashFlowInfo.Type == CashFlowInformationTypeEnum.Item)
                    {
                        if (cashFlowInfo.Name == "Break Even All" || cashFlowInfo.Name == "Break Even Spot" || cashFlowInfo.Name == "Break Even Balloon"
                            || cashFlowInfo.Name == "Break Even Equity" || cashFlowInfo.Name == "Valuation" || cashFlowInfo.Name == "Discount Factor"
                            || cashFlowInfo.Name == "Present Value" || cashFlowInfo.Name == "Break Even Scrap")
                        {
                            e.Value = cashFlowInfo.Results.Aggregate<Dictionary<DateTime, decimal>, decimal>(0, (current, dictionary) => current + dictionary.Values.Sum());

                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.Item && cashFlowInfo.Name == "IRR")
                        {
                            List<double> values = new List<double>();
                            foreach (Dictionary<DateTime, decimal> dictionary in cashFlowInfo.Results)
                            {
                                foreach (KeyValuePair<DateTime, decimal> keyValuePair in dictionary)
                                {
                                    values.Add(Convert.ToDouble(keyValuePair.Value));
                                }

                            }

                            if (values[0] != 0 && values.Any(a => a != 0))
                            {
                                var calculator = new NewtonRaphsonIRRCalculator(values.ToArray());
                                try
                                {
                                    e.Value = calculator.Execute();
                                }
                                catch (Exception exc)
                                {
                                    e.Value = "There was an error while computing IRR item. " + exc.Message;
                                }
                            }
                            else
                            {
                                e.Value = 0;
                            }
                        }
                    }
                }
            }
        }

        private void btnGeneratePosition_Click(object sender, EventArgs e)
        {
            if (cmbMarketSensitivityType.SelectedItem != null &&
                (cmbMarketSensitivityType.SelectedItem.ToString() != "None" && cmbMarketSensitivityType.SelectedItem.ToString() != "Stress")
                && (txtMarketSensitivityValue.EditValue == null || txtMarketSensitivityValue.Value == 0))
            {
                XtraMessageBox.Show(this,
                                    "Please provide a valid value for field 'Market Sensitivity Value'.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _formState = FormStateEnum.WhileExecuting;
            SetGuiControls();
            BeginGenerateMarkToMarket();
        }
                
        private void GridTradeActionResultsViewCustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            if (((GridSummaryItem)e.Item).Tag == null || (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() != "MTM"))
                return;

            string month="";
            string indexName;
            var view = sender as GridView;
            // Get the summary ID. 
            var summaryType = ((GridSummaryItem)e.Item).Tag;
            if ((string)summaryType != "Index")
            {                
                month = ((string)summaryType).Substring(((string)summaryType).IndexOf("___")).Trim('_');
                
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                {                    
                    sumIndex[(((string)summaryType).Substring(0, ((string)summaryType).IndexOf("___")))] = 0;
                    sumIndexCounter[(((string)summaryType).Substring(0, ((string)summaryType).IndexOf("___")))] = 0;
                }
            }

            #region Calculation
            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                //var temp = view.GetRow(e.RowHandle);
                //string indexName = ((GridSummaryItem)e.Item).Tag;              
                TradeInformation tradeInfo = view.GetRow(e.RowHandle) as TradeInformation;
                indexName = tradeInfo.ForwardCurve;// + "-" + tradeInfo.PhysicalFinancial;
                if (!sumIndex.ContainsKey(indexName)) {
                    sumIndex.Add(indexName,new decimal());
                }
                if (!sumIndexCounter.ContainsKey(indexName))
                {
                    sumIndexCounter.Add(indexName, new decimal());
                }                                

                if (rdgResultsAggregationType.SelectedIndex == 0) //Month
                {
                    DateTime dt = Convert.ToDateTime(((GridSummaryItem)e.Item).FieldName);
                    if (_indexesValues[dt][indexName] != 0)
                    {                       
                        if (_indexesValues[dt][indexName] != 0)
                        {                            
                            sumIndex[indexName] = sumIndex[indexName] + _indexesValues[dt][indexName];
                            sumIndexCounter[indexName]++;
                        }                                                    
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) //Quarter
                {
                    string quarter = month;                   
                    _indexesValues.Where(a => {
                        int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                        string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                        return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;})
                    .Where(a => a.Value.Count > 0 && a.Value.ContainsKey(indexName) && a.Value[indexName] != 0)
                    .ForEach(g => {
                        sumIndex[indexName] = sumIndex[indexName] + g.Value[indexName];
                        sumIndexCounter[indexName]++;
                    });
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2)//Calendar
                {
                    string calendar = month;

                    if (_indexesValues.Any(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar && a.Value.Count > 0 && a.Value.ContainsKey(indexName) && a.Value[indexName] != 0))
                    {                       
                        _indexesValues.Where(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar && a.Value.Count > 0 && a.Value.ContainsKey(indexName) && a.Value[indexName] != 0)
                        .ForEach(g => {
                            sumIndex[indexName] = sumIndex[indexName] + g.Value[indexName];
                            sumIndexCounter[indexName]++;                                    
                        });                            
                    }
                }
            }
            #endregion
            // Finalization 
            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                //string fieldName;
                e.TotalValue = 0;

                if ((string)((GridSummaryItem)e.Item).Tag == "Index")
                {
                    e.TotalValue = ((GridSummaryItem)e.Item).FieldName;
                }
                else
                {
                    indexName = ((GridSummaryItem)e.Item).FieldName;
                    if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                    {
                        DateTime dt = Convert.ToDateTime(((string)((GridSummaryItem)e.Item).Tag).Substring(((string)((GridSummaryItem)e.Item).Tag).IndexOf("___")).Trim('_'));
                        var temp = _indexesValues[dt];
                        e.TotalValue = _indexesValues[dt][indexName];
                    }
                    else if (rdgResultsAggregationType.SelectedIndex == 1)//Quarter
                    {
                        string quarter = ((string)((GridSummaryItem)e.Item).Tag).Substring(((string)((GridSummaryItem)e.Item).Tag).IndexOf("___")).Trim('_');
                        e.TotalValue = _indexesValues.Where(
                            a =>
                            {
                                int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                string strQuarter = intQuarter == 1
                                                        ? "Q1"
                                                        : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;
                            }).Any(a => a.Value.Count > 0)
                                           ? _indexesValues.Where(
                                               a =>
                                               {
                                                   int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                                   string strQuarter = intQuarter == 1
                                                                           ? "Q1"
                                                                           : intQuarter == 2
                                                                                 ? "Q2"
                                                                                 : intQuarter == 3 ? "Q3" : "Q4";
                                                   return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                          quarter;
                                               }).Where(a => a.Value.Count > 0 && a.Value.ContainsKey(indexName)).Select(a => a.Value[indexName]).Average()
                                           : 0;
                    }
                    else if (rdgResultsAggregationType.SelectedIndex == 2)//Calendar
                    {
                        string calendar = ((string)((GridSummaryItem)e.Item).Tag).Substring(((string)((GridSummaryItem)e.Item).Tag).IndexOf("___")).Trim('_');

                        if (_indexesValues.Any(a => "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar && a.Value.Count > 0 && a.Value.ContainsKey(indexName)))
                        {
                            e.TotalValue =
                                _indexesValues.Where(
                                    a =>
                                    "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar &&
                                    a.Value.Count > 0 && a.Value.ContainsKey(indexName)).Select(a => a.Value[indexName]).
                                    Average();
                        }
                        else
                            e.TotalValue = 0;
                    }
                }
            }
        }

        private void btnExportResults_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            string strDateFrom = _viewTradesGeneralInfo.periodFrom.Year + "_" + _viewTradesGeneralInfo.periodFrom.Month + "_" + _viewTradesGeneralInfo.periodFrom.Day;
            string strDateTo = _viewTradesGeneralInfo.periodFrom.Year + "_" + _viewTradesGeneralInfo.periodFrom.Month + "_" + _viewTradesGeneralInfo.periodFrom.Day;

            gridTradeActionResultsView.OptionsPrint.PrintHorzLines = false;
            gridTradeActionResultsView.OptionsPrint.PrintVertLines = false;
            gridTradeActionResultsView.OptionsPrint.AutoWidth = false;

            string fileName = "File";
            string sheetName = "Sheet";

            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                fileName = "FreightMetrics_PositionReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "Position Report";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                fileName = "FreightMetrics_MTMReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "MTM Report";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "CASHFLOW")
            {
                fileName = "FreightMetrics_CashFlowReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "Cash Flow Report";
            }

            try
            {
                if (gridTradeActionResultsView.DataRowCount > 60000)
                {
                    gridTradeActionResultsView.ExportToXlsx(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xlsx", new XlsxExportOptions { ExportHyperlinks = false, ExportMode = XlsxExportMode.SingleFile, SheetName = sheetName, ShowGridLines = false, TextExportMode = TextExportMode.Value });
                }
                else
                {
                    gridTradeActionResultsView.ExportToXls(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xls", new XlsExportOptions { ExportHyperlinks = false, ExportMode = XlsExportMode.SingleFile, SheetName = sheetName, ShowGridLines = false, TextExportMode = TextExportMode.Value });
                }
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this, "File with name: '" + fileName + "' exported to Desktop successfully."
                                    ,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this, string.Format(Strings.Failed_to_write_file_to_Desktop__Details___0_, exc.Message)
                                    ,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void RdgResultsAggregationTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                btnGeneratePosition.PerformClick();
            }
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                CreateResultGridColumns(_tradeMTMResults, _tradeEmbeddedValues, "MTM");
            }
        }

        private void RdgPositionResultsTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                btnGeneratePosition.PerformClick();
            }
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                CreateResultGridColumns(_tradeMTMResults, _tradeEmbeddedValues, "MTM");
            }
        }

        private void CmbMarketSensitivityTypeSelectedValueChanged(object sender, EventArgs e)
        {
            txtMarketSensitivityValue.Properties.ReadOnly = (cmbMarketSensitivityType.SelectedItem != null &&
                                                             (cmbMarketSensitivityType.SelectedItem.ToString() ==
                                                              "Stress" ||
                                                              cmbMarketSensitivityType.SelectedItem.ToString() == "None"));
        }

        #endregion

        #region Proxy Calls

        private void BeginGenerateMarkToMarket()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm1 = new WaitDialogForm(this);
            var tradeIdsByPhysicalFinancialType = _viewTradesGeneralInfo.tradeInformations.Select(ti => ti.Trade.Id).ToList().Select((k, i) => new { k, v = _viewTradesGeneralInfo.tradeInformations.Select(ti => ti.PhysicalFinancial).ToList()[i] })
              .ToDictionary(x => x.k, x => x.v);
            try
            {
                SessionRegistry.Client.BeginGenerateMarkToMarket(_viewTradesGeneralInfo.positionDate,
                                                                 dtpFilterCurveDate.DateTime.Date,
                                                                 _viewTradesGeneralInfo.periodFrom,
                                                                 _viewTradesGeneralInfo.periodTo,
                                                                 (string)cmbPositionMethod.SelectedItem,
                                                                 (string)cmbMarketSensitivityType.SelectedItem,
                                                                 txtMarketSensitivityValue.Value,
                                                                 chkUseSpotValues.Checked, chkOptionPremium.Checked,
                                                                 chkOptionEmbeddedValue.Checked, chkOptionNullify.Checked,
                                                                 (string)rdgMTMResultsType.EditValue,
                                                                 _viewTradesGeneralInfo.tradeIdentifiers,
                                                                 tradeIdsByPhysicalFinancialType,
                                                                 EndGenerateMarkToMarket, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGenerateMarkToMarket(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGenerateMarkToMarket;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<string, Dictionary<DateTime, decimal>> tradeMTMs;
            Dictionary<string, decimal> tradeEmbeddedValues;
            Dictionary<DateTime, Dictionary<string, decimal>> indexesValues;
            string errorMessage = "";
            try
            {
                result = SessionRegistry.Client.EndGenerateMarkToMarket(out tradeMTMs, out tradeEmbeddedValues, out indexesValues, out errorMessage, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _formState = FormStateEnum.AfterExecution;
            SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? Strings.
                                              There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //XtraMessageBox.Show(this,
                //                    Strings.
                //                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                //                    Strings.Freight_Metrics,
                //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                XtraMessageBox.Show(this,
                                    "Failed to find BFA index values for the selected trade sign dates.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                _formState = FormStateEnum.AfterExecution;
                SetGuiControls();
            }
            else if (result == 0) //everything is OK
            {
                _tradeMTMResults = tradeMTMs;
                _tradeEmbeddedValues = tradeEmbeddedValues;
                _indexesValues = indexesValues;

                CreateResultGridColumns(tradeMTMs, tradeEmbeddedValues, "MTM");

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }

            }
        }

        #endregion

        #region Events

        public event Action<MemoryStream, string> ViewChartEvent;
        public event Action<long, long, string, TradeTypeEnum> OnViewTrade;
        public event Action<long, long, string, TradeTypeEnum> OnEditTrade;
        public event Action<Dictionary<DateTime, List<double>>, List<double>, List<Dictionary<DateTime, decimal>>> MonteCarloSimulationEvent;
        public event Action<List<TradeInformation>, Dictionary<string, Dictionary<DateTime, decimal>>, Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>, DateTime, DateTime, DateTime> RatiosReportEvent;
        public event Action<object, bool> OnDataSaved;
        public event Action OnOpenActivateTradesForm;
        public event Action OnTradeDeactivated;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            //if (treeNavigation.FocusedNode != null)
            //{
            //    var filters = new List<HierarchyNodeInfo>();
            //    TreeListNode node = treeNavigation.FocusedNode;
            //    var entity = (HierarchyNodeInfo) node.Tag;
            //    filters.Add(entity);

            //    TreeListNode parentNode = node.ParentNode;
            //    while (parentNode != null)
            //    {
            //        filters.Add((HierarchyNodeInfo) parentNode.Tag);
            //        parentNode = parentNode.ParentNode;
            //    }

            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesByFilters(filters);
            //}
            //else
            //{
            //    if (treeBookNavigation.FocusedNode == null)
            //        return;
            //    var filters = new List<HierarchyNodeInfo>();
            //    TreeListNode node = treeBookNavigation.FocusedNode;
            //    var entity = (HierarchyNodeInfo) node.Tag;
            //    filters.Add(entity);

            //    TreeListNode parentNode = node.ParentNode;
            //    while (parentNode != null)
            //    {
            //        filters.Add((HierarchyNodeInfo) parentNode.Tag);
            //        parentNode = parentNode.ParentNode;
            //    }

            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesByFilters(filters);
            //}
        }

        #endregion

    }

}