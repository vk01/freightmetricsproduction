﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using Exis.Domain;
using Extreme.Statistics;
using DevExpress.XtraEditors.Controls;

namespace Exis.WinClient.Controls.Core
{
    public partial class MonteCarloSimulationForm : DevExpress.XtraEditors.XtraForm
    {

        private MCSimulation _mcSimulation;

        private Dictionary<DateTime, List<double>> _monthFrequency;
        private List<double> _cashBounds;
        private List<Dictionary<DateTime, decimal>> _allValues;

        private Dictionary<DateTime, List<double>> _quarterFrequency;
        private List<double> _quarterCashBounds;
        private List<Dictionary<DateTime, decimal>> _quarterAllValues;

        private Dictionary<DateTime, List<double>> _calendarFrequency;
        private List<double> _calendarCashBounds;
        private List<Dictionary<DateTime, decimal>> _calendarAllValues;

        private Dictionary<DateTime, List<double>> _allValuesFrequency;
        private List<double> _allValuesCashBounds;
        private List<Dictionary<DateTime, decimal>> _AllValues;

        private bool isLoad;

        #region Constructor

        public MonteCarloSimulationForm(MCSimulation mcSimulation, Dictionary<DateTime, List<double>> monthFrequency, List<double> cashBounds,
                                        List<Dictionary<DateTime, decimal>> allValues)
        {
            InitializeComponent();

            _mcSimulation = mcSimulation;
            _monthFrequency = monthFrequency;
            _cashBounds = cashBounds;
            _allValues = allValues;

            chart.HardwareAcceleration = false;

            InitializeControls();
        }

        #endregion

        #region Private methods

        private void ComputeMonteCarloByPeriodType2(PeriodTypeEnum periodType)
        {
            var customPeriods = new List<CustomPeriod>();
            DateTime minDate = _monthFrequency.Keys.Min(a => a.Date);
            DateTime maxDate = _monthFrequency.Keys.Max(a => a.Date);
            DateTime currentDate = minDate;

            switch (periodType)
            {
                case PeriodTypeEnum.Month:
                    chart.Series.Clear();

                    foreach (DateTime month in _monthFrequency.Keys)
                    {
                        double lowerBound = _cashBounds[0];
                        Series series = new Series(month.ToString("MMM-yyyy", new CultureInfo("en-GB")), ViewType.Area3D);
                        series.ArgumentScaleType = ScaleType.Qualitative;
                        series.ValueScaleType = ScaleType.Numerical;

                        for (int i = 1; i < _cashBounds.Count; i++)
                        {
                            double upperBound = _cashBounds[i];
                            double value =
                                _monthFrequency.Where(a => a.Key == month).Select(a => a.Value[i - 1]).Single();
                            series.Points.Add(
                                new SeriesPoint(Math.Floor(lowerBound).ToString() + " To " + Math.Floor(upperBound),
                                                value));
                            lowerBound = upperBound;
                        }

                        chart.Series.AddRange(new Series[] {series});

                        // Access the series options. 
                        series.Label.PointOptions.PointView = PointView.ArgumentAndValues;

                        // Customize the view-type-specific properties of the series. 
                        Area3DSeriesView myView = (Area3DSeriesView) series.View;
                        myView.Transparency = 70;
                    }
                    ((XYDiagram3D) chart.Diagram).ZoomPercent = 120;
                    break;
                case PeriodTypeEnum.Quarter:
                    chart.Series.Clear();

                    customPeriods = new List<CustomPeriod>();
                    while (currentDate <= maxDate)
                    {
                        int intQuarter = ((currentDate.Month - 1)/3) + 1;
                        string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                        var period = new CustomPeriod()
                                         {
                                             Date = currentDate.ToShortDateString(),
                                             Description =
                                                 strQuarter + currentDate.ToString("-yy", new CultureInfo("en-GB")),
                                             EditValue = strQuarter + currentDate.ToString("-yy", new CultureInfo("en-GB"))
                                         };
                        customPeriods.Add(period);
                        currentDate = currentDate.AddMonths(3);
                    }

                    foreach (CustomPeriod period in customPeriods)
                    {
                        double lowerBound = _cashBounds[0];
                        Series series = new Series(period.Description, ViewType.Area3D);
                        series.ArgumentScaleType = ScaleType.Qualitative;
                        series.ValueScaleType = ScaleType.Numerical;

                        for (int i = 1; i < _cashBounds.Count; i++)
                        {
                            var dt = Convert.ToDateTime(period.Date);
                            double upperBound = _cashBounds[i];
                            double value = 0;
                            foreach (var month in _monthFrequency.Keys.Where(a => a.Date <= dt.AddMonths(3) && a.Date >= dt))
                            {
                                value = value +
                                        _monthFrequency.Where(a => a.Key == month).Select(a => a.Value[i - 1]).Single();

                            }
                            series.Points.Add(
                                new SeriesPoint(Math.Floor(lowerBound).ToString() + " To " + Math.Floor(upperBound),
                                                value));
                            lowerBound = upperBound;
                        }

                        chart.Series.AddRange(new Series[] {series});

                        // Access the series options. 
                        series.Label.PointOptions.PointView = PointView.ArgumentAndValues;

                        // Customize the view-type-specific properties of the series. 
                        Area3DSeriesView myView = (Area3DSeriesView) series.View;
                        myView.Transparency = 70;
                    }
                    ((XYDiagram3D) chart.Diagram).ZoomPercent = 120;
                    break;
                case PeriodTypeEnum.Calendar:
                    chart.Series.Clear();

                    customPeriods = new List<CustomPeriod>();
                    while (currentDate <= maxDate)
                    {
                        var period = new CustomPeriod()
                                         {
                                             Date = currentDate.ToShortDateString(),
                                             Description = "Cal" + currentDate.ToString("-yy", new CultureInfo("en-GB")),
                                             EditValue = "Cal" + currentDate.ToString("-yy", new CultureInfo("en-GB"))
                                         };
                        customPeriods.Add(period);
                        currentDate = currentDate.AddYears(1);
                    }

                    foreach (CustomPeriod period in customPeriods)
                    {
                        double lowerBound = _cashBounds[0];
                        Series series = new Series(period.Description, ViewType.Area3D);
                        series.ArgumentScaleType = ScaleType.Qualitative;
                        series.ValueScaleType = ScaleType.Numerical;

                        for (int i = 1; i < _cashBounds.Count; i++)
                        {
                            var dt = Convert.ToDateTime(period.Date);
                            double upperBound = _cashBounds[i];
                            double value = 0;
                            foreach (var month in _monthFrequency.Keys.Where(a => a.Date <= dt.AddYears(1) && a.Date >= dt))
                            {
                                value = value +
                                        _monthFrequency.Where(a => a.Key == month).Select(a => a.Value[i - 1]).Single();

                            }
                            series.Points.Add(
                                new SeriesPoint(Math.Floor(lowerBound).ToString() + " To " + Math.Floor(upperBound),
                                                value));
                            lowerBound = upperBound;
                        }

                        chart.Series.AddRange(new Series[] {series});

                        // Access the series options. 
                        series.Label.PointOptions.PointView = PointView.ArgumentAndValues;

                        // Customize the view-type-specific properties of the series. 
                        Area3DSeriesView myView = (Area3DSeriesView) series.View;
                        myView.Transparency = 70;
                    }
                    ((XYDiagram3D) chart.Diagram).ZoomPercent = 120;
                    break;

            }

        }

        private void ComputeMonteCarloByPeriodType(PeriodTypeEnum periodType)
        {
            try
            {
                var customPeriods = new List<CustomPeriod>();
                DateTime minDate = _monthFrequency.Keys.Min(a => a.Date);
                DateTime maxDate = _monthFrequency.Keys.Max(a => a.Date);
                DateTime currentDate = minDate;

                switch (periodType)
                {
                    case PeriodTypeEnum.Month:

                        chart.ClearCache();
                        chart.ClearSelection();
                        chart.SuspendLayout();
                        chart.Series.Clear();

                        foreach (DateTime month in _monthFrequency.Keys)
                        {
                            double lowerBound = _cashBounds[0];
                            Series series = new Series(month.ToString("MMM-yyyy", new CultureInfo("en-GB")),
                                                       ViewType.Area3D);
                            series.ArgumentScaleType = ScaleType.Qualitative;
                            series.ValueScaleType = ScaleType.Numerical;

                            for (int i = 1; i < _cashBounds.Count; i++)
                            {
                                double upperBound = _cashBounds[i];
                                double value =
                                    _monthFrequency.Where(a => a.Key == month).Select(a => a.Value[i - 1]).Single();
                                series.Points.Add(
                                    new SeriesPoint(Math.Floor(lowerBound).ToString() + " To " + Math.Floor(upperBound),
                                                    value));
                                lowerBound = upperBound;
                            }

                            chart.Series.BeginUpdate();
                            chart.Series.AddRange(new Series[] {series});
                            chart.Series.EndUpdate();

                            // Access the series options. 
                            series.Label.PointOptions.PointView = PointView.ArgumentAndValues;

                            // Customize the view-type-specific properties of the series. 
                            Area3DSeriesView myView = (Area3DSeriesView) series.View;
                            myView.Transparency = 70;
                        }
                        ((XYDiagram3D) chart.Diagram).ZoomPercent = 120;
                        chart.ResumeLayout();

                        break;
                    case PeriodTypeEnum.Quarter:

                        #region Compute Quarters

                        customPeriods = new List<CustomPeriod>();
                        while (currentDate <= maxDate)
                        {
                            int intQuarter = ((currentDate.Month - 1)/3) + 1;
                            string strQuarter = intQuarter == 1
                                                    ? "Q1"
                                                    : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                            bool uncompletedPeriod = false;
                            var firstDay = new DateTime(currentDate.Year, 3*intQuarter - 2, 1);
                            for (int i = 0; i < 3; i++)
                            {
                                if (!_monthFrequency.Keys.Contains(firstDay.AddMonths(i)))
                                {
                                    uncompletedPeriod = true;
                                    break;
                                }
                            }

                            if (uncompletedPeriod)
                            {
                                currentDate = currentDate.AddMonths(3);
                                continue;
                            }

                            var period = new CustomPeriod()
                                             {
                                                 Date = firstDay.ToShortDateString(),
                                                 Description =
                                                     strQuarter + firstDay.ToString("-yy", new CultureInfo("en-GB")),
                                                 EditValue =
                                                     strQuarter + firstDay.ToString("-yy", new CultureInfo("en-GB"))
                                             };
                            customPeriods.Add(period);
                            currentDate = currentDate.AddMonths(3);
                        }

                        #endregion

                        #region Compute values per Quarter

                        if (customPeriods.Count == 0) return;

                        _quarterAllValues = new List<Dictionary<DateTime, decimal>>();
                        foreach (Dictionary<DateTime, decimal> run in _allValues)
                        {
                            var dic = new Dictionary<DateTime, decimal>();
                            foreach (CustomPeriod customPeriod in customPeriods)
                            {
                                var dt = Convert.ToDateTime(customPeriod.Date);
                                decimal sumQuarter = (from date in run.Keys
                                                      where date.Date >= dt &&
                                                            date.Date < dt.AddMonths(3)
                                                      select run[date]).Sum();

                                dic[dt] = sumQuarter;
                            }
                            _quarterAllValues.Add(dic);
                        }

                        #endregion

                        #region Compute Cash Bounds

                        decimal minimumPrice = _quarterAllValues.Select(a => a.Values.Min()).Min();
                        decimal maximumPrice = _quarterAllValues.Select(a => a.Values.Max()).Max();

                        if (minimumPrice == maximumPrice)
                        {
                            XtraMessageBox.Show(this,
                                                "Cannot compute cash bounds. Minimum Value = Maximum Value = " +
                                                minimumPrice.ToString(), Strings.Freight_Metrics,
                                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        chart.ClearCache();
                        chart.ClearSelection();
                        chart.SuspendLayout();
                        chart.Series.Clear();

                        var histograms = new Dictionary<DateTime, Histogram>();
                        List<DateTime> quarters = _quarterAllValues.First().Keys.ToList();

                        foreach (DateTime month2 in quarters)
                        {
                            var histogram = new Histogram((double) minimumPrice, (double) maximumPrice, 10);

                            foreach (var finalPrice in _quarterAllValues)
                            {
                                histogram.Increment((double) finalPrice[month2]);
                            }
                            histograms.Add(month2, histogram);
                        }

                        _quarterFrequency = new Dictionary<DateTime, List<double>>();
                        foreach (var histogram in histograms)
                        {
                            var monthValues = new List<double>();
                            foreach (HistogramBin histogramBin in histogram.Value.Bins)
                            {
                                monthValues.Add(histogramBin.Value);
                            }
                            _quarterFrequency.Add(histogram.Key, monthValues);
                        }

                        _quarterCashBounds = new List<double>();
                        foreach (HistogramBin histogramBin in histograms.First().Value.Bins)
                        {
                            if (!_quarterCashBounds.Exists(a => a == histogramBin.LowerBound))
                                _quarterCashBounds.Add(histogramBin.LowerBound);
                            if (!_quarterCashBounds.Exists(a => a == histogramBin.UpperBound))
                                _quarterCashBounds.Add(histogramBin.UpperBound);
                        }
                        _quarterCashBounds = _quarterCashBounds.OrderBy(a => a).ToList();

                        #endregion

                        #region Create chart series

                        foreach (CustomPeriod period in customPeriods)
                        {
                            double lowerBound = _quarterCashBounds[0];
                            Series series = new Series(period.Description, ViewType.Area3D);
                            series.ArgumentScaleType = ScaleType.Qualitative;
                            series.ValueScaleType = ScaleType.Numerical;


                            for (int i = 1; i < _quarterCashBounds.Count; i++)
                            {
                                var dt = Convert.ToDateTime(period.Date);
                                double upperBound = _quarterCashBounds[i];
                                double value =
                                    _quarterFrequency.Where(a => a.Key == dt).Select(a => a.Value[i - 1]).
                                        Single();
                                series.Points.Add(
                                    new SeriesPoint(Math.Floor(lowerBound).ToString() + " To " + Math.Floor(upperBound),
                                                    value));
                                lowerBound = upperBound;
                            }

                            chart.Series.BeginUpdate();
                            chart.Series.AddRange(new Series[] {series});
                            chart.Series.EndUpdate();

                            // Access the series options. 
                            series.Label.PointOptions.PointView = PointView.ArgumentAndValues;

                            // Customize the view-type-specific properties of the series. 
                            Area3DSeriesView myView = (Area3DSeriesView) series.View;
                            myView.Transparency = 70;
                        }
                        ((XYDiagram3D) chart.Diagram).ZoomPercent = 120;

                        chart.ResumeLayout();

                        #endregion

                        break;
                    case PeriodTypeEnum.Calendar:

                        #region Compute Calendars

                        customPeriods = new List<CustomPeriod>();
                        while (currentDate <= maxDate)
                        {
                            bool uncompletedPeriod = false;
                            var firstDay = new DateTime(currentDate.Year, 1, 1);
                            for (int i = 0; i < 12; i++)
                            {
                                if (!_monthFrequency.Keys.Contains(firstDay.AddMonths(i)))
                                {
                                    uncompletedPeriod = true;
                                    break;
                                }
                            }

                            if (uncompletedPeriod)
                            {
                                currentDate = currentDate.AddYears(1);
                                continue;
                            }

                            var period = new CustomPeriod()
                                             {
                                                 Date = firstDay.ToShortDateString(),
                                                 Description =
                                                     "Cal" + firstDay.ToString("-yy", new CultureInfo("en-GB")),
                                                 EditValue = "Cal" + firstDay.ToString("-yy", new CultureInfo("en-GB"))
                                             };
                            customPeriods.Add(period);
                            currentDate = currentDate.AddYears(1);
                        }

                        #endregion

                        #region Compute values per Calendar

                        if (customPeriods.Count == 0) return;

                        _calendarAllValues = new List<Dictionary<DateTime, decimal>>();
                        foreach (Dictionary<DateTime, decimal> run in _allValues)
                        {
                            var dic = new Dictionary<DateTime, decimal>();
                            foreach (CustomPeriod customPeriod in customPeriods)
                            {
                                var dt = Convert.ToDateTime(customPeriod.Date);
                                decimal sumQuarter = (from date in run.Keys
                                                      where date.Date >= dt &&
                                                            date.Date < dt.AddYears(1)
                                                      select run[date]).Sum();

                                dic[dt] = sumQuarter;
                            }
                            _calendarAllValues.Add(dic);
                        }

                        #endregion

                        #region Compute Cash Bounds

                        decimal calendarMinimumPrice = _calendarAllValues.Select(a => a.Values.Min()).Min();
                        decimal calendarMaximumPrice = _calendarAllValues.Select(a => a.Values.Max()).Max();

                        if (calendarMaximumPrice == calendarMinimumPrice)
                        {
                            XtraMessageBox.Show(this,
                                                "Cannot compute cash bounds. Minimum Value = Maximum Value = " +
                                                calendarMinimumPrice.ToString(), Strings.Freight_Metrics,
                                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        chart.ClearCache();
                        chart.ClearSelection();
                        chart.SuspendLayout();
                        chart.Series.Clear();

                        var calendarHistograms = new Dictionary<DateTime, Histogram>();
                        List<DateTime> calendars = _calendarAllValues.First().Keys.ToList();

                        foreach (DateTime month2 in calendars)
                        {
                            var histogram = new Histogram((double) calendarMinimumPrice, (double) calendarMaximumPrice,
                                                          10);

                            foreach (var finalPrice in _calendarAllValues)
                            {
                                histogram.Increment((double) finalPrice[month2]);
                            }
                            calendarHistograms.Add(month2, histogram);
                        }

                        _calendarFrequency = new Dictionary<DateTime, List<double>>();
                        foreach (var histogram in calendarHistograms)
                        {
                            var calendarValues = new List<double>();
                            foreach (HistogramBin histogramBin in histogram.Value.Bins)
                            {
                                calendarValues.Add(histogramBin.Value);
                            }
                            _calendarFrequency.Add(histogram.Key, calendarValues);
                        }

                        _calendarCashBounds = new List<double>();
                        foreach (HistogramBin histogramBin in calendarHistograms.First().Value.Bins)
                        {
                            if (!_calendarCashBounds.Exists(a => a == histogramBin.LowerBound))
                                _calendarCashBounds.Add(histogramBin.LowerBound);
                            if (!_calendarCashBounds.Exists(a => a == histogramBin.UpperBound))
                                _calendarCashBounds.Add(histogramBin.UpperBound);
                        }
                        _calendarCashBounds = _calendarCashBounds.OrderBy(a => a).ToList();

                        #endregion

                        #region Create chart series

                        foreach (CustomPeriod period in customPeriods)
                        {
                            double lowerBound = _calendarCashBounds[0];
                            Series series = new Series(period.Description, ViewType.Area3D);
                            series.ArgumentScaleType = ScaleType.Qualitative;
                            series.ValueScaleType = ScaleType.Numerical;


                            for (int i = 1; i < _calendarCashBounds.Count; i++)
                            {
                                var dt = Convert.ToDateTime(period.Date);
                                double upperBound = _calendarCashBounds[i];
                                double value =
                                    _calendarFrequency.Where(a => a.Key == dt).Select(a => a.Value[i - 1]).
                                        Single();
                                series.Points.Add(
                                    new SeriesPoint(Math.Floor(lowerBound).ToString() + " To " + Math.Floor(upperBound),
                                                    value));
                                lowerBound = upperBound;
                            }

                            chart.Series.BeginUpdate();
                            chart.Series.AddRange(new Series[] {series});
                            chart.Series.EndUpdate();

                            // Access the series options. 
                            series.Label.PointOptions.PointView = PointView.ArgumentAndValues;

                            // Customize the view-type-specific properties of the series. 
                            Area3DSeriesView myView = (Area3DSeriesView) series.View;
                            myView.Transparency = 70;
                        }
                        ((XYDiagram3D) chart.Diagram).ZoomPercent = 120;

                        chart.ResumeLayout();

                        #endregion

                        break;
                }
            }
            catch (Exception exc)
            {
                XtraMessageBox.Show(this, "An error occurred while Monte Carlo Simulation.", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InitializeControls()
        {
            isLoad = true;

            lblSimulationName.Text = "Simulation Info:" +_mcSimulation.Info.Name;
            lblStartTime.Text = _mcSimulation.Info.StartTime.ToString("G", CultureInfo.InvariantCulture);
            lblEndTime.Text = _mcSimulation.Info.EndTime.ToString("G", CultureInfo.InvariantCulture);
            lblNoOfRuns.Text = _mcSimulation.Info.NoOfRuns.ToString();
            lblUser.Text = _mcSimulation.Info.Cruser;
            lblCurveDate.Text = _mcSimulation.Info.CurveDate.ToString("d", CultureInfo.InvariantCulture);
            lblPeriodFrom.Text = _mcSimulation.Info.PeriodFrom.ToString("d", CultureInfo.InvariantCulture);
            lblPeriodTo.Text = _mcSimulation.Info.PeriodTo.ToString("d", CultureInfo.InvariantCulture);
            lblMakSensValue.Text = _mcSimulation.Info.MSensValue.ToString();
            lblMarkSensType.Text = _mcSimulation.Info.MSensType;
            lblPosMethod.Text = _mcSimulation.Info.PosMethod;
            lblMTMType.Text = _mcSimulation.Info.MTMType;
            lblIsOptionPremium.Text = (_mcSimulation.Info.IsOptionPremium==1)?"True":"False";
            lblUseSpot.Text = (_mcSimulation.Info.UseSpot==1)?"True":"False";

            lblTrades.Text = _mcSimulation.Info.ExternalCodes;


            cmbPeriodType.Properties.Items.Add(PeriodTypeEnum.Month);
            cmbPeriodType.Properties.Items.Add(PeriodTypeEnum.Quarter);
            cmbPeriodType.Properties.Items.Add(PeriodTypeEnum.Calendar);
            cmbPeriodType.SelectedItem = PeriodTypeEnum.Month;

            cmbPercentagePeriods.Properties.ValueMember = "EditValue";
            cmbPercentagePeriods.Properties.DisplayMember = "Description";
            cmbPercentilePeriods.Properties.ValueMember = "EditValue";
            cmbPercentilePeriods.Properties.DisplayMember = "Description";

            txtFrom.EditValue = null;
            txtTo.EditValue = null;

            FilterPeriods();
            isLoad = false;
        }

        private void FilterPeriods()
        {
            bool uncompletedPeriod = false;
            cmbPercentagePeriods.Properties.DataSource = null;
            cmbPercentagePeriods.Properties.DataSource = null;

            if (cmbPeriodType.EditValue == null) return;

            var customPeriods = new List<CustomPeriod>();
            DateTime minDate = _monthFrequency.Keys.Min(a => a.Date);
            DateTime maxDate = _monthFrequency.Keys.Max(a => a.Date);
            DateTime currentDate = minDate;

            //if ((PeriodTypeEnum)cmbPeriodType.SelectedItem == PeriodTypeEnum.Month)

            customPeriods.AddRange(_monthFrequency.Keys.Select(month => new CustomPeriod()
                                                                            {
                                                                                Date = month.Date.ToShortDateString(),
                                                                                Description =
                                                                                    month.Date.ToString("MMM-yy",
                                                                                                        new CultureInfo
                                                                                                            ("en-GB")),
                                                                                EditValue = month.Date
                                                                            }));

            while (currentDate <= maxDate)
            {
                uncompletedPeriod = false;

                int intQuarter = ((currentDate.Month - 1)/3) + 1;
                string strQuarter = intQuarter == 1 ? "Q1" : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                var firstDay = new DateTime(currentDate.Year, 3*intQuarter - 2, 1);
                for (int i = 0; i < 3; i++)
                {
                    if (!_monthFrequency.Keys.Contains(firstDay.AddMonths(i)))
                    {
                        uncompletedPeriod = true;
                        break;
                    }
                }
                if (uncompletedPeriod)
                {
                    currentDate = currentDate.AddMonths(3);
                    continue;
                }

                var period = new CustomPeriod()
                                 {
                                     Date = firstDay.ToShortDateString(),
                                     Description =
                                         strQuarter + firstDay.ToString("-yy", new CultureInfo("en-GB")),
                                     EditValue = strQuarter + firstDay.ToString("-yy", new CultureInfo("en-GB"))
                                 };
                customPeriods.Add(period);
                currentDate = currentDate.AddMonths(3);
            }
            currentDate = minDate;
            while (currentDate <= maxDate)
            {
                uncompletedPeriod = false;
                var firstDay = new DateTime(currentDate.Year, 1, 1);
                for (int i = 0; i < 12; i++)
                {
                    if (!_monthFrequency.Keys.Contains(firstDay.AddMonths(i)))
                    {
                        uncompletedPeriod = true;
                        break;
                    }
                }
                if (uncompletedPeriod)
                {
                    currentDate = currentDate.AddYears(1);
                    continue;
                }
                var period = new CustomPeriod()
                                 {
                                     Date = firstDay.ToShortDateString(),
                                     Description = "Cal" + firstDay.ToString("-yy", new CultureInfo("en-GB")),
                                     EditValue = "Cal" + firstDay.ToString("-yy", new CultureInfo("en-GB"))
                                 };
                customPeriods.Add(period);
                currentDate = currentDate.AddYears(1);
            }
            customPeriods.Add(new CustomPeriod() {Date = "All", Description = "All", EditValue = "All"});

            cmbPercentagePeriods.Properties.DataSource = customPeriods;
            cmbPercentilePeriods.Properties.DataSource = customPeriods;
            cmbPercentagePeriods.Tag = customPeriods;
            cmbPercentilePeriods.Tag = customPeriods;

        }

        #endregion

        #region GUI event handlers

        private void MonteCarloSimulationFormLoad(object sender, EventArgs e)
        {
            ComputeMonteCarloByPeriodType(PeriodTypeEnum.Month);
        }

        private void BtnComputeProbabilityClick(object sender, EventArgs e)
        {
            lblProbability.Text = "";

            if (cmbPercentagePeriods.EditValue == null)
            {
                XtraMessageBox.Show(this, "Please select a period type.", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(txtFrom.Text) && string.IsNullOrEmpty(txtTo.Text))
            {
                XtraMessageBox.Show(this, "Fields 'From' and 'To' cannot be both empty.", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (cmbPercentagePeriods.EditValue.ToString().Contains("Q"))
            {
                if (_quarterAllValues == null)
                    cmbPeriodType.SelectedItem = PeriodTypeEnum.Quarter;

                var values = new List<decimal>();

                var customPeriodStr = cmbPercentagePeriods.EditValue as string;
                DateTime firstDate = Convert.ToDateTime(((List<CustomPeriod>) cmbPercentagePeriods.Tag).Where(a => a.EditValue == customPeriodStr).Single().Date);

                values.AddRange(_quarterAllValues.Select(a => a[firstDate]));

                int count;
                decimal probability = 0;
                int numOfValues = _quarterAllValues.Count;

                if (!string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    count =
                        values.Where(
                            a => a >= Convert.ToDecimal(txtFrom.EditValue) && a <= Convert.ToDecimal(txtTo.EditValue)).
                            ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                else if (!string.IsNullOrEmpty(txtFrom.Text) && string.IsNullOrEmpty(txtTo.Text))
                {
                    count = values.Where(a => a >= Convert.ToDecimal(txtFrom.EditValue)).ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                else if (string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    count = values.Where(a => a <= Convert.ToDecimal(txtTo.EditValue)).ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                lblProbability.Text = probability.ToString("F") + @"%";
            }
            else if (cmbPercentagePeriods.EditValue.ToString().Contains("Cal"))
            {
                if (_calendarAllValues == null)
                    cmbPeriodType.SelectedItem = PeriodTypeEnum.Calendar;

                var values = new List<decimal>();
                var customPeriodStr = cmbPercentagePeriods.EditValue as string;
                DateTime firstDate = Convert.ToDateTime(((List<CustomPeriod>) cmbPercentagePeriods.Tag).Where(a => a.EditValue == customPeriodStr).Single().Date);

                values.AddRange(_calendarAllValues.Select(a => a[firstDate]));

                int count;
                decimal probability = 0;
                int numOfValues = _calendarAllValues.Count;

                if (!string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    count =
                        values.Where(
                            a => a >= Convert.ToDecimal(txtFrom.EditValue) && a <= Convert.ToDecimal(txtTo.EditValue)).
                            ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                else if (!string.IsNullOrEmpty(txtFrom.Text) && string.IsNullOrEmpty(txtTo.Text))
                {
                    count = values.Where(a => a >= Convert.ToDecimal(txtFrom.EditValue)).ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                else if (string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    count = values.Where(a => a <= Convert.ToDecimal(txtTo.EditValue)).ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                lblProbability.Text = probability.ToString("F") + @"%";
            }
            else
            {
                DateTime selectedSeries;
                selectedSeries = cmbPercentagePeriods.EditValue.ToString() == "All" ? DateTime.MinValue : Convert.ToDateTime(cmbPercentagePeriods.EditValue);

                var values = new List<decimal>();
                if (selectedSeries != DateTime.MinValue)
                    values.AddRange(_allValues.Select(a => a[selectedSeries]));
                else
                    values.AddRange(from a in _allValues from keyValuePair in a select keyValuePair.Value);

                int count;
                decimal probability = 0;
                int numOfSeries = _monthFrequency.Keys.Count;
                int numOfValues = selectedSeries != DateTime.MinValue ? _allValues.Count : (_allValues.Count*numOfSeries);

                if (!string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    count =
                        values.Where(
                            a => a >= Convert.ToDecimal(txtFrom.EditValue) && a <= Convert.ToDecimal(txtTo.EditValue)).
                            ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                else if (!string.IsNullOrEmpty(txtFrom.Text) && string.IsNullOrEmpty(txtTo.Text))
                {
                    count = values.Where(a => a >= Convert.ToDecimal(txtFrom.EditValue)).ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                else if (string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    count = values.Where(a => a <= Convert.ToDecimal(txtTo.EditValue)).ToList().Count;

                    probability = (decimal) (100*count)/numOfValues;
                }
                lblProbability.Text = probability.ToString("F") + @"%";
            }

        }

        private void BtnComputePercentileClick(object sender, EventArgs e)
        {
            lblPercentile.Text = "";

            if (cmbPercentilePeriods.EditValue == null)
            {
                XtraMessageBox.Show(this, "Please select a period type.", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (string.IsNullOrEmpty(txtProbability.Text))
            {
                XtraMessageBox.Show(this, "Field 'Probability' cannot be empty", Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (cmbPercentilePeriods.EditValue.ToString().Contains("Q"))
            {
                if (_quarterAllValues == null)
                    cmbPeriodType.SelectedItem = PeriodTypeEnum.Quarter;

                var values = new List<decimal>();
                var customPeriodStr = cmbPercentilePeriods.EditValue as string;
                DateTime firstDate = Convert.ToDateTime(((List<CustomPeriod>) cmbPercentilePeriods.Tag).Where(a => a.EditValue == customPeriodStr).Single().Date);
                values.AddRange(_quarterAllValues.Select(a => a[firstDate]));

                var probability = (decimal) txtProbability.EditValue;
                decimal percentile = Percentile(values.OrderBy(a => a).ToArray(), probability);

                lblPercentile.Text = percentile.ToString("F");
            }
            else if (cmbPercentilePeriods.EditValue.ToString().Contains("Cal"))
            {
                if (_calendarAllValues == null)
                    cmbPeriodType.SelectedItem = PeriodTypeEnum.Calendar;

                var values = new List<decimal>();
                var customPeriodStr = cmbPercentilePeriods.EditValue as string;
                DateTime firstDate = Convert.ToDateTime(((List<CustomPeriod>) cmbPercentilePeriods.Tag).Where(a => a.EditValue == customPeriodStr).Single().Date);

                values.AddRange(_calendarAllValues.Select(a => a[firstDate]));

                var probability = (decimal) txtProbability.EditValue;
                decimal percentile = Percentile(values.OrderBy(a => a).ToArray(), probability);

                lblPercentile.Text = percentile.ToString("F");
            }
            else
            {
                DateTime selectedSeries;
                selectedSeries = cmbPercentilePeriods.EditValue.ToString() == "All" ? DateTime.MinValue : Convert.ToDateTime(cmbPercentilePeriods.EditValue);

                var values = new List<decimal>();
                if (selectedSeries != DateTime.MinValue)
                    values.AddRange(_allValues.Select(a => a[selectedSeries]));
                else
                {
                    //values.AddRange(from a in _allValues from keyValuePair in a select keyValuePair.Value);

                    var _sumAllValues = new List<Dictionary<DateTime, decimal>>();
                    var dt = DateTime.MinValue;

                    foreach (Dictionary<DateTime, decimal> run in _allValues)
                    {
                        //sum all months per simulation entry
                        decimal sum = (from date in run.Keys
                                              select run[date]).Sum();
                        var dic = new Dictionary<DateTime, decimal>();
                        dic[dt] = sum;
                        _sumAllValues.Add(dic);
                    }
                  

                    values.AddRange(_sumAllValues.Select(a => a[selectedSeries]));
                }

                var probability = (decimal) txtProbability.EditValue;
                decimal percentile = Percentile(values.OrderBy(a => a).ToArray(), probability);

                lblPercentile.Text = percentile.ToString("F");
            }
            //int count;
            //decimal percentile = 0;
            //decimal probability = (decimal)txtProbability.EditValue;
            //int numOfSeries = _monthFrequency.Keys.Count;
            //int numOfValues = selectedSeries != DateTime.MinValue ? _allValues.Count : (_allValues.Count * numOfSeries);

            //values = new List<decimal>() { 7, 16, 17, 18, 19, 19, 20, 22, 31, 34, 38, 60 };
            //percentile = Percentile(values.ToArray(), probability);
            //lblPercentile.Text = percentile.ToString("F");
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void CmbPeriodTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoad || cmbPeriodType.EditValue == null || string.IsNullOrEmpty(cmbPeriodType.EditValue.ToString()))
                return;

            ComputeMonteCarloByPeriodType((PeriodTypeEnum) cmbPeriodType.EditValue);
        }

        #endregion

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion

        /// <summary>
        /// Calculate percentile of a sorted data set
        /// </summary>
        /// <param name="sortedData">array of decimal values</param>
        /// <param name="p">percentile, value 0-100</param>
        /// <returns></returns>
        internal static decimal Percentile(decimal[] sortedData, decimal p)
        {
            if (p >= 100) return sortedData[sortedData.Length - 1];

            decimal position = (decimal) (sortedData.Length + 1)*p/100;
            decimal leftNumber = 0, rightNumber = 0;

            decimal n = p/100*(sortedData.Length - 1) + 1;

            if (sortedData.Count() == 1)
                return sortedData[0];

            if (position >= 1)
            {
                leftNumber = sortedData[(int) Math.Floor(n) - 1];
                rightNumber = sortedData[(int) Math.Floor(n)];
            }
            else
            {
                leftNumber = sortedData[0]; // first data
                rightNumber = sortedData[1]; // first data
            }

            if (leftNumber == rightNumber)
                return leftNumber;
            decimal part = n - System.Math.Floor(n);
            return leftNumber + part*(rightNumber - leftNumber);
        }

        #region Custom class

        public class CustomPeriod
        {
            //public DateTime Date { get; set; }
            public string Date { get; set; }
            public string Description { get; set; }
            public object EditValue { get; set; }
        }

        #endregion
    }
}