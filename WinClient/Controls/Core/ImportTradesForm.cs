﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTG.Spreadsheet;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using System.IO;
using System.Globalization;

namespace Exis.WinClient.Controls.Core
{
    public partial class ImportTradesForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private Properties

        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm;
        private const string licenceCode = "SOZT-CSWE-M2SN-CXMT";

        #endregion

        #region Contructor

        public ImportTradesForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Private Methods

        private void ParseFfaOptionFileAndImport()
        {
            try
            {
                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbook;
                string fileName = btnSelectFFAOptionTradesFile.EditValue.ToString();
                string extension = Path.GetExtension(fileName);
                switch (extension.ToLower())
                {
                    case ".xlsx":
                        {
                            wbook = ExcelWorkbook.ReadXLSX(fileName);
                            break;
                        }
                    case ".xls":
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                    case ".csv":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName);
                            break;
                        }
                    case ".txt":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                            break;
                        }
                    default:
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                }

                var columnNames = new List<string>()
                                               {
                                                   "Broker",
                                                   "Broker Rate",
                                                   "Buy Sell",
                                                   "Company",
                                                   "Contract No",
                                                   "Contract Type",
                                                   "Counter Party",
                                                   "End Date",
                                                   "Fixed Rate",
                                                   "Hedged Against",
                                                   "Keyword",
                                                   "Period",
                                                   "Period Type",
                                                   "Premium Rate",
                                                   "Qty Traded Per Month",
                                                   "Start Date",
                                                   "Status",
                                                   "Trade Code",
                                                   "Trade Date",
                                                   "Trader",
                                                   "Clearing House",
                                                   "Closed Out Quantity Total"
                                               };

                ExcelCellCollection cells = wbook.Worksheets[0].Cells;
                //Create a dictionary which holds column name and index
                var columnIndexes = new Dictionary<string, int>();
                int dataFirstRow = 0;
                bool found = false;
                int k, l = 0;

                for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                        }
                    }
                    if (found)
                        break;
                }
                

                string missingNames =
                    columnNames.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                                                                                                        (current,
                                                                                                         columnName) =>
                                                                                                        current +
                                                                                                        columnName +
                                                                                                        ", ");
                if (!String.IsNullOrEmpty(missingNames))
                {
                    XtraMessageBox.Show(this,
                                        "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                                        "' do(es) not exist in the selected file.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var tmpImportFfaOptionTrades = new List<TmpImportFfaOptionTrade>();
                for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                {
                    if (wbook.Worksheets[0].Cells[i, columnIndexes["Contract No"]].Value == null
                        || string.IsNullOrEmpty(wbook.Worksheets[0].Cells[i, columnIndexes["Contract No"]].Value.ToString()))
                        continue;

                    TmpImportFfaOptionTrade trade;
                    trade = new TmpImportFfaOptionTrade
                    {
                        Broker = wbook.Worksheets[0].Cells[i, columnIndexes["Broker"]].Value != null
                                     ? wbook.Worksheets[0].Cells[i, columnIndexes["Broker"]].Value.
                                           ToString()
                                     : null,
                        BrokerRate =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Broker Rate"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Broker Rate"]].Value.
                                      ToString()
                                : null,
                        BuySell = wbook.Worksheets[0].Cells[i, columnIndexes["Buy Sell"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Buy Sell"]].Value.
                                            ToString()
                                      : null,
                        Company = wbook.Worksheets[0].Cells[i, columnIndexes["Company"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Company"]].Value.
                                            ToString()
                                      : null,
                        ContractNo =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Contract No"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Contract No"]].Value.
                                      ToString()
                                : null,
                        ContractType =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Contract Type"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Contract Type"]].Value.
                                      ToString()
                                : null,
                        Counterparty =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Counter Party"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Counter Party"]].Value.
                                      ToString()
                                : null,
                        EndDate = wbook.Worksheets[0].Cells[i, columnIndexes["End Date"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["End Date"]].Value.
                                            ToString()
                                      : null,
                        FileName = fileName.Substring(fileName.LastIndexOf('\\') + 1),
                        FixedRate =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Fixed Rate"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Fixed Rate"]].Value.
                                      ToString()
                                : null,
                        HedgedAgainst =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Hedged Against"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Hedged Against"]].Value.
                                      ToString()
                                : null,
                        Keyword = wbook.Worksheets[0].Cells[i, columnIndexes["Keyword"]].Value != null
                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Keyword"]].Value.
                                            ToString()
                                      : null,
                        Period = wbook.Worksheets[0].Cells[i, columnIndexes["Period"]].Value != null
                                     ? wbook.Worksheets[0].Cells[i, columnIndexes["Period"]].Value.
                                           ToString()
                                     : null,
                        PeriodType =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Period Type"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Period Type"]].Value.
                                      ToString()
                                : null,
                        PremiumRate =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Premium Rate"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Premium Rate"]].Value.
                                      ToString
                                      ()
                                : null,
                        QtyTradedPerMonth =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Qty Traded Per Month"]].Value !=
                            null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Qty Traded Per Month"]].
                                      Value.ToString()
                                : null,
                        RowNumber = i,
                        StartDate =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Start Date"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Start Date"]].Value.
                                      ToString()
                                : null,
                        Status = wbook.Worksheets[0].Cells[i, columnIndexes["Status"]].Value != null
                                     ? wbook.Worksheets[0].Cells[i, columnIndexes["Status"]].Value.
                                           ToString()
                                     : null,
                        TradeCode =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Trade Code"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Trade Code"]].Value.
                                      ToString()
                                : null,
                        TradeDate =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Trade Date"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Trade Date"]].Value.
                                      ToString()
                                : null,
                        Trader = wbook.Worksheets[0].Cells[i, columnIndexes["Trader"]].Value != null
                                     ? wbook.Worksheets[0].Cells[i, columnIndexes["Trader"]].Value.
                                           ToString()
                                     : null,
                        ClearingHouse = wbook.Worksheets[0].Cells[i, columnIndexes["Clearing House"]].Value != null
                                            ? wbook.Worksheets[0].Cells[i, columnIndexes["Clearing House"]].Value.
                                                  ToString()
                                            : null,
                        ClosedOutQuantityTotal =
                            wbook.Worksheets[0].Cells[i, columnIndexes["Closed Out Quantity Total"]].Value != null
                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Closed Out Quantity Total"]].Value.
                                      ToString
                                      ()
                                : null,
                    };
                    tmpImportFfaOptionTrades.Add(trade);
                }
                ImportTrades(tmpImportFfaOptionTrades, null, null, null);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this,
                                    "Error occurred while parsing excel file1. Error: '"+ exc.Message +"'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ParseCargoFileAndImport()
        {
            try
            {
                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbook;
                string fileName = btnSelectCargoTradesFile.EditValue.ToString();
                string extension = Path.GetExtension(fileName);
                switch (extension.ToLower())
                {
                    case ".xlsx":
                        {
                            wbook = ExcelWorkbook.ReadXLSX(fileName);
                            break;
                        }
                    case ".xls":
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                    case ".csv":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName);
                            break;
                        }
                    case ".txt":
                        {
                            wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                            break;
                        }
                    default:
                        {
                            wbook = ExcelWorkbook.ReadXLS(fileName);
                            break;
                        }
                }

                var columnNames = new List<string>()
                                               {
                                                   "Base TCE Rate",
                                                   "Charterer",
                                                   "Date",
                                                   "Est Duration",
                                                   "Our Reference",
                                                   "Laydays Date",
                                                   "Fixed By",
                                                   "Trade Route",
                                                   "Size Adjust (%)"
                                               };

                ExcelCellCollection cells = wbook.Worksheets[0].Cells;
                //Create a dictionary which holds column name and index
                var columnIndexes = new Dictionary<string, int>();
                int dataFirstRow = 0;
                bool found = false;
                int k, l = 0;

                for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                {
                    for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                    {
                        ExcelCell cell = cells[k, l];
                        if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && columnNames.Contains(cell.Value.ToString().Trim()))
                        {
                            if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);

                            dataFirstRow = k + 1;
                            found = true;
                            continue;
                        }
                        if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) && columnNames.Contains(cell.Value.ToString().Trim()))
                            columnIndexes.Add(cell.Value.ToString().Trim(), l);
                    }
                    if (found)
                        break;
                }
                
                string missingNames =
                    columnNames.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                                                                                                        (current,
                                                                                                         columnName) =>
                                                                                                        current +
                                                                                                        columnName +
                                                                                                        ", ");
                if (!String.IsNullOrEmpty(missingNames))
                {
                    XtraMessageBox.Show(this,
                                        "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                                        "' do(es) not exist in the selected file.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var tmpImportCargoTrades = new List<TmpImportCargoTrade>();
                for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                {
                    if (wbook.Worksheets[0].Cells[i, columnIndexes["Our Reference"]].Value == null
                        || string.IsNullOrEmpty(wbook.Worksheets[0].Cells[i, columnIndexes["Our Reference"]].Value.ToString()))
                        continue;

                    var trade = new TmpImportCargoTrade
                                    {
                                        FileName = fileName.Substring(fileName.LastIndexOf('\\') + 1),
                                        RowNumber = i,
                                        BaseTceRate =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Base TCE Rate"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Base TCE Rate"]].Value.
                                                      ToString()
                                                : null,
                                        Charterer =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Charterer"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Charterer"]].Value.
                                                      ToString()
                                                : null,
                                        Date = wbook.Worksheets[0].Cells[i, columnIndexes["Date"]].Value != null
                                                   ? wbook.Worksheets[0].Cells[i, columnIndexes["Date"]].Value.
                                                         ToString()
                                                   : null,
                                        EstDuration =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Est Duration"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Est Duration"]].Value.
                                                      ToString()
                                                : null,
                                        OurRef = wbook.Worksheets[0].Cells[i, columnIndexes["Our Reference"]].Value != null
                                                    ? wbook.Worksheets[0].Cells[i, columnIndexes["Our Reference"]].Value.
                                                          ToString()
                                                    : null,
                                        LaydaysDate =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Laydays Date"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Laydays Date"]].Value.
                                                      ToString()
                                                : null,
                                        FixedBy =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Fixed By"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Fixed By"]].Value.
                                                      ToString()
                                                : null,
                                        TradeRoute =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Trade Route"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Trade Route"]].Value.
                                                      ToString()
                                                : null,
                                        SizeAdjust =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Size Adjust (%)"]].Value != null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Size Adjust (%)"]].Value.
                                                      ToString()
                                                : null
                                    };
                    tmpImportCargoTrades.Add(trade);
                }
                ImportTrades(null, tmpImportCargoTrades, null, null);
            }
            catch (Exception exc)
            {
                XtraMessageBox.Show(this,
                                    "Error occurred while parsing excel file.Error: '"+ exc.Message +"'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ParseTcFilesAndImport()
        {
            Cursor = Cursors.WaitCursor;
            bool errordata = false;
            string errorloc = null;
            try
            {
                var tmpImportTcInTrades = new List<TmpImportTcInTrade>();
                var tmpImportTcOutTrades = new List<TmpImportTcOutTrade>();

                ExcelWorkbook.SetLicenseCode(licenceCode);

                ExcelWorkbook wbook;
                string folderName = btnSelectTCTradesFolder.EditValue.ToString();
                if (File.Exists(@folderName + "\\importlog.txt"))
                {
                    File.Delete(@folderName + "\\importlog.txt");
                }
                string[] filePaths = Directory.GetFiles(folderName);



                if (filePaths.Count() != 2)
                {
                    Cursor = Cursors.Default;
                    XtraMessageBox.Show(this,
                                        "The selected folder does not contain valid TC In/Out excel files.",
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var tcInColumnNames = new List<string>()
                                              {
                                                  "Analysis Code",
                                                  "Broker 1",
                                                  "Broker 2",
                                                  "Broker 3",
                                                  "Company1",
                                                  "Company",
                                                  "CP Date",                                                  
                                                  "Fixed By",
                                                  "Head Fixture Ref",
                                                  "Rate",
                                                  "Rate Type",
                                                  "Size Adjust",
                                                  "Trade Route",
                                                  "Head Fixture Name",
                                                  "Status",
                                                  "Period From",
                                                  "Period To",
                                                  "Period To Max",
                                                  "Period To Min",
                                                  "Year of build",
                                                  "Disponent Owner"
                                              };

                var tcOutColumnNames = new List<string>()
                                              {
                                                  "chart id",
                                                  "start date (GMT)",
                                                  "end date (GMT)",
                                                  "net dy",
                                                  "Vessel",
                                                  "Voy",
                                                  "our ref",
                                                  "Period Start",
                                                  "Period End",
                                                  "dwat",
                                                  "Charterers"
                                              };
                var checkNullColumns = new List<string>()
                                              {
                                                  "CP Date",
                                                  "Period From",
                                                  "Period To",
                                                  "Period To Max",
                                                  "Period To Min",
                                                  "Rate Type"
                                              };

                foreach (string fileName in filePaths)
                {
                    string extension = Path.GetExtension(fileName);
                    switch (extension.ToLower())
                    {
                        case ".xlsx":
                            {
                                wbook = ExcelWorkbook.ReadXLSX(fileName);
                                break;
                            }
                        case ".xls":
                            {
                                wbook = ExcelWorkbook.ReadXLS(fileName);
                                break;
                            }
                        case ".csv":
                            {
                                wbook = ExcelWorkbook.ReadCSV(fileName);
                                break;
                            }
                        case ".txt":
                            {
                                wbook = ExcelWorkbook.ReadCSV(fileName, ',', Encoding.Default);
                                break;
                            }
                        default:
                            {
                                wbook = ExcelWorkbook.ReadXLS(fileName);
                                break;
                            }
                    }
                    
                    ExcelCellCollection cells = wbook.Worksheets[0].Cells;
                    //Create a dictionary which holds column name and index
                    var columnIndexes = new Dictionary<string, int>();
                    int dataFirstRow = 0;
                    bool found = false;
                    bool isTcIn = false;
                    int k, l = 0;

                    for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                    {
                        for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                        {
                            ExcelCell cell = cells[k, l];
                            if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && cell.Value.ToString().Trim() == "Analysis Code")
                            {
                                isTcIn = true;
                                found = true;
                                break;
                            }
                            if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && cell.Value.ToString().Trim() == "chart id")
                            {
                                found = true;
                                isTcIn = false;
                                break;
                            }
                         
                        }
                        if (found)
                            break;
                    }

                    found = false;
                    if (isTcIn)
                    {
                        for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                        {
                            for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                            {
                                ExcelCell cell = cells[k, l];
                                if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && tcInColumnNames.Contains(cell.Value.ToString().Trim()))
                                {
                                    if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                                    columnIndexes.Add(cell.Value.ToString().Trim(), l);

                                    dataFirstRow = k + 1;
                                    found = true;
                                    continue;
                                }
                                if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) && tcInColumnNames.Contains(cell.Value.ToString().Trim()))
                                {
                                    if (columnIndexes.ContainsKey(cell.Value.ToString().Trim())) continue;
                                    columnIndexes.Add(cell.Value.ToString().Trim(), l);
                                }
                            }
                            if (found)
                                break;
                        }

                        string missingNames =
                            tcInColumnNames.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                                                                                                                (current,
                                                                                                                 columnName) =>
                                                                                                                current +
                                                                                                                columnName +
                                                                                                                ", ");
                        if (!String.IsNullOrEmpty(missingNames))
                        {
                            XtraMessageBox.Show(this,
                                                "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                                                "' do(es) not exist in the selected file.",
                                                Strings.Freight_Metrics,
                                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        
                        for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                        {
                            if (wbook.Worksheets[0].Cells[i, columnIndexes["Head Fixture Ref"]].Value == null
                                || string.IsNullOrEmpty(wbook.Worksheets[0].Cells[i, columnIndexes["Head Fixture Ref"]].Value.ToString()))
                                continue;
                            if (wbook.Worksheets[0].Cells[i, columnIndexes["CP Date"]].Value == null
                               || string.IsNullOrEmpty(wbook.Worksheets[0].Cells[i, columnIndexes["CP Date"]].Value.ToString()))
                            {
                                using (StreamWriter w = File.AppendText(folderName + "\\importlog.txt"))
                                {
                                    string data = string.Empty;
                                    int m; ExcelCell cell;
                                    for (m = 0; m < wbook.Worksheets[0].Columns.Count; m++)
                                    {
                                        cell = cells[i, m];
                                        data += (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))?cell.Value.ToString() + ", " : ", ";
                                    }
                                    Log("(Row Line-" + (i + 1) + ") :" + data, w);
                                }
                                continue;
                            }
                                foreach (string dt in checkNullColumns) {
                                    if (wbook.Worksheets[0].Cells[i, columnIndexes[dt]].Value == null
                                        || string.IsNullOrEmpty(wbook.Worksheets[0].Cells[i, columnIndexes[dt]].Value.ToString()))
                                        {                                           
                                          errordata = true;
                                          errorloc = "Column:" + dt + " - Row:" + (i+1);
                                         }
                                }
                             
                            if (errordata)
                            {
                                Cursor = Cursors.Default;
                                tmpImportTcInTrades.Clear();
                                XtraMessageBox.Show(this,Strings.This_excel_file_contains_empty_or_null_columns__Import_cannot_continue_ + errorloc,                                   
                                                  Strings.Freight_Metrics,
                                                  MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }

                             var trade = new TmpImportTcInTrade
                                            {
                                                FileName = fileName.Substring(fileName.LastIndexOf('\\') + 1),
                                                RowNumber = i,
                                                AnalysisCode =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Analysis Code"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Analysis Code"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                Broker1 =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Broker 1"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Broker 1"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                Broker2 =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Broker 2"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Broker 2"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                Broker3 =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Broker 3"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Broker 3"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                Company =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Company1"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Company1"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                Company1 =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Company"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Company"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                CpDate = wbook.Worksheets[0].Cells[i, columnIndexes["CP Date"]].Value !=
                                                         null
                                                             ? wbook.Worksheets[0].Cells[i, columnIndexes["CP Date"]].
                                                                   Value
                                                                   .
                                                                   ToString()
                                                             : null,
                                                FixedBy =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Fixed By"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Fixed By"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                HeadFixtureRef = wbook.Worksheets[0].Cells[i, columnIndexes["Head Fixture Ref"]].Value !=
                                                         null
                                                             ? wbook.Worksheets[0].Cells[i, columnIndexes["Head Fixture Ref"]].
                                                                   Value
                                                                   .
                                                                   ToString()
                                                             : null,
                                                Rate = wbook.Worksheets[0].Cells[i, columnIndexes["Rate"]].Value !=
                                                       null
                                                           ? wbook.Worksheets[0].Cells[i, columnIndexes["Rate"]].
                                                                 Value
                                                                 .
                                                                 ToString()
                                                           : null,
                                                RateType =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Rate Type"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Rate Type"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                SizeAdjust =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Size Adjust"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Size Adjust"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                TradeRoute =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Trade Route"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Trade Route"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                Vessel = wbook.Worksheets[0].Cells[i, columnIndexes["Head Fixture Name"]].Value !=
                                                         null
                                                             ? wbook.Worksheets[0].Cells[i, columnIndexes["Head Fixture Name"]].
                                                                   Value
                                                                   .
                                                                   ToString()
                                                             : null,
                                                Status = wbook.Worksheets[0].Cells[i, columnIndexes["Status"]].Value !=
                                                         null
                                                             ? wbook.Worksheets[0].Cells[i, columnIndexes["Status"]].
                                                                   Value
                                                                   .
                                                                   ToString()
                                                             : null,
                                                PeriodFrom =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Period From"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Period From"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                PeriodTo =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Period To"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Period To"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                PeriodToMax =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Period To Max"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Period To Max"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                PeriodToMin =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Period To Min"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Period To Min"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                YearBuilt =
                                                    wbook.Worksheets[0].Cells[i, columnIndexes["Year of build"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Year of build"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null,
                                                DisponentOwner =
                                            wbook.Worksheets[0].Cells[i, columnIndexes["Disponent Owner"]].Value !=
                                            null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Disponent Owner"]].
                                                      Value
                                                      .
                                                      ToString()
                                                : null
                                            };
                            tmpImportTcInTrades.Add(trade);
                        }
                    }
                    else
                    {
                            for (k = 0; k < wbook.Worksheets[0].Rows.Count; k++)
                            {
                                for (l = 0; l < wbook.Worksheets[0].Columns.Count; l++)
                                {
                                    ExcelCell cell = cells[k, l];
                                    if (!found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()) && tcOutColumnNames.Contains(cell.Value.ToString().Trim()))
                                    {
                                        columnIndexes.Add(cell.Value.ToString().Trim(), l);

                                        dataFirstRow = k + 1;
                                        found = true;
                                        continue;
                                    }
                                    if (found && cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString().Trim()) && tcOutColumnNames.Contains(cell.Value.ToString().Trim()))
                                        columnIndexes.Add(cell.Value.ToString().Trim(), l);
                                }
                                if (found)
                                    break;
                            }

                            string missingNames =
                                tcOutColumnNames.Where(columnName => !columnIndexes.Keys.Contains(columnName)).Aggregate("",
                                                                                                                    (current,
                                                                                                                     columnName) =>
                                                                                                                    current +
                                                                                                                    columnName +
                                                                                                                    ", ");
                            if (!String.IsNullOrEmpty(missingNames))
                            {
                                Cursor = Cursors.Default;
                                XtraMessageBox.Show(this,
                                                    "Error occurred while parsing excel file. Error: Column(s) '" + missingNames.TrimEnd(',', ' ') +
                                                    "' do(es) not exist in the selected file.",
                                                    Strings.Freight_Metrics,
                                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }

                            for (int i = dataFirstRow; i < wbook.Worksheets[0].Rows.Count; i++)
                            {
                                if (wbook.Worksheets[0].Cells[i, columnIndexes["our ref"]].Value == null
                                    || string.IsNullOrEmpty(wbook.Worksheets[0].Cells[i, columnIndexes["our ref"]].Value.ToString())
                                    || wbook.Worksheets[0].Cells[i, columnIndexes["our ref"]].Value.ToString().Contains("Col "))
                                    continue;

                                var trade = new TmpImportTcOutTrade();
                                trade.FileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                                trade.RowNumber = i;
                                trade.ChartId = wbook.Worksheets[0].Cells[i, columnIndexes["chart id"]].Value !=
                                                null
                                                    ? wbook.Worksheets[0].Cells[i, columnIndexes["chart id"]].
                                                          Value
                                                          .
                                                          ToString()
                                                    : null;
                                trade.StartDate = wbook.Worksheets[0].Cells[i, columnIndexes["start date (GMT)"]].
                                                      Value !=
                                                  null
                                                      ? wbook.Worksheets[0].Cells[
                                                              i, columnIndexes["start date (GMT)"]].Value.ToString()

                                                      : null;
                                trade.EndDate = wbook.Worksheets[0].Cells[i, columnIndexes["end date (GMT)"]].Value != null
                                                    ? wbook.Worksheets[0].Cells[i, columnIndexes["end date (GMT)"]].Value.ToString()
                                                    : null;
                                trade.TceEstimate = wbook.Worksheets[0].Cells[i, columnIndexes["net dy"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["net dy"]].
                                                              Value
                                                              .
                                                              ToString()
                                                        : null;
                                trade.Vessel = wbook.Worksheets[0].Cells[i, columnIndexes["Vessel"]].Value !=
                                               null
                                                   ? wbook.Worksheets[0].Cells[i, columnIndexes["Vessel"]].
                                                         Value
                                                         .
                                                         ToString()
                                                   : null;
                                trade.Voy = wbook.Worksheets[0].Cells[i, columnIndexes["Voy"]].Value !=
                                            null
                                                ? wbook.Worksheets[0].Cells[i, columnIndexes["Voy"]].
                                                      Value
                                                      .
                                                      ToString()
                                                : null;
                                trade.OurRef = wbook.Worksheets[0].Cells[i, columnIndexes["our ref"]].Value !=
                                               null
                                                   ? wbook.Worksheets[0].Cells[i, columnIndexes["our ref"]].
                                                         Value
                                                         .
                                                         ToString()
                                                   : null;
                                trade.PeriodStart = wbook.Worksheets[0].Cells[i, columnIndexes["Period Start"]].Value !=
                                                    null
                                                        ? wbook.Worksheets[0].Cells[i, columnIndexes["Period Start"]].
                                                                Value.ToString()
                                                        : null;
                                trade.PeriodEnd = wbook.Worksheets[0].Cells[i, columnIndexes["Period End"]].Value !=
                                                  null
                                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Period End"]].
                                                              Value.ToString()
                                                      : null;
                                trade.VesselSize = wbook.Worksheets[0].Cells[i, columnIndexes["dwat"]].Value !=
                                                  null
                                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["dwat"]].
                                                              Value.ToString()
                                                      : null;
                                trade.Charterer = wbook.Worksheets[0].Cells[i, columnIndexes["Charterers"]].Value !=
                                                  null
                                                      ? wbook.Worksheets[0].Cells[i, columnIndexes["Charterers"]].
                                                              Value.ToString()
                                                      : null;
                                tmpImportTcOutTrades.Add(trade);
                            
                        }
                    }
                }

                if (errordata) ImportTrades(null, null, null, null);
                else ImportTrades(null, null, tmpImportTcInTrades, tmpImportTcOutTrades);
            }
            catch (Exception exc)
            {
                XtraMessageBox.Show(this,
                                    "Error occurred while parsing excel file.Error: '" + exc.Message + "'",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region GUI Handlers

        private void BtnImportFfaOptionTradesButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var ofd = new OpenFileDialog
                          {
                              Filter = @"Excel files (*.xls)|*.xls;*.xlsx",
                              RestoreDirectory = true,
                              Title = Strings.Open_File
                          };
            if (ofd.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = ofd.FileName;
        }

        private void btnSelectCargoTradesFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = @"Excel files (*.xls)|*.xls;*.xlsx",
                RestoreDirectory = true,
                Title = Strings.Open_File
            };
            //ofd.InitialDirectory = _appParameters.Where(a => a.Code == ftpXmlFolder).SingleOrDefault() == null
            //                           ? ""
            //                           : _appParameters.Where(a => a.Code == ftpXmlFolder).Single().Value;

            if (ofd.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = ofd.FileName;
        }

        private void btnSelectTCTradesFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.Description = "Select the folder where the files are located";

            if (dialog.ShowDialog() == DialogResult.OK)
                ((ButtonEdit)sender).EditValue = dialog.SelectedPath;
        }

        private void BtnImportFfaOptionTradesClick(object sender, EventArgs e)
        {
            if(btnSelectFFAOptionTradesFile.EditValue == null)
            {
                XtraMessageBox.Show(this,
                                    "Select a file to import data from.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ParseFfaOptionFileAndImport();
        }

        private void BtnImportCargoTradesClick(object sender, EventArgs e)
        {
            if (btnSelectCargoTradesFile.EditValue == null)
            {
                XtraMessageBox.Show(this,
                                    "Select a file to import data from.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ParseCargoFileAndImport();
        }

        private void BtnImportTcTradesClick(object sender, EventArgs e)
        {
            if (btnSelectTCTradesFolder.EditValue == null)
            {
                XtraMessageBox.Show(this,
                                    "Select a folder to import data from.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ParseTcFilesAndImport();
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        #endregion

        #region Proxy Calls

        private void ImportTrades(List<TmpImportFfaOptionTrade> tmpImportFfaOptionTrades, List<TmpImportCargoTrade> tmpImportCargoTrades, List<TmpImportTcInTrade> tmpImportTcInTrades, List<TmpImportTcOutTrade> tmpImportTcOutTrades)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginImportTrades(tmpImportFfaOptionTrades, tmpImportCargoTrades, tmpImportTcInTrades, tmpImportTcOutTrades, EndInsertTradesToTempTables, null);
            }
            catch (Exception)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                Cursor = Cursors.Default;
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndInsertTradesToTempTables(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndInsertTradesToTempTables;
                Invoke(action, ar);
                return;
            }
            int? result;
            string errorMessage = "";
            try
            {
                result = SessionRegistry.Client.EndImportTrades(out errorMessage, ar);
            }
            catch (Exception exc)
            {
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                Cursor = Cursors.Default;
                SessionRegistry.ResetClientService();

                if (exc.GetType() == typeof(TimeoutException))
                {
                    XtraMessageBox.Show(this,
                                    "The call to the service has timed out. Please try again.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    XtraMessageBox.Show(this,
                                        string.IsNullOrEmpty(errorMessage)
                                            ? Strings.
                                                  There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_
                                            : errorMessage,
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? Strings.
                                              There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(result == 1)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    string.IsNullOrEmpty(errorMessage)
                                        ? "There was an error during the insertion of the trades."
                                        : errorMessage,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 7) //another import is in progress
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    "An import is already in progress. Please try again later.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }

                XtraMessageBox.Show(this,
                                    "Trades were imported successfully.",
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion

        //Log Writer
        public static void Log(string logMessage, TextWriter w)
        {         
            // w.Write("\r\nLog Entry : ");
            // w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("");
        }
    }
}