﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTreeList;
using Exis.Domain;
using Exis.WinClient.General;
using Exis.WinClient.SpecialForms;
using Padding = DevExpress.XtraLayout.Utils.Padding;
using Region = Exis.Domain.Region;
using DevExpress.XtraTreeList.Nodes;

namespace Exis.WinClient.Controls.Core
{
    public partial class AddEditViewTradeForm : XtraForm
    {
        #region Private Properties

        private readonly FormActionTypeEnum _formActionType;
        private TradeTypeEnum _tradeType;
        private WaitDialogForm _dialogForm;
        private XtraUserControl _extraTradeInfoControl;
        private Trade _trade;
        private long _tradeInfoId;
        private bool isLoad;
        private object _syncObject = new object();

        private List<Company> _brokers;
        List<TradeInfoBook> _tradeInfoBooks = new List<TradeInfoBook>();

        private bool fetchAllData;

        #endregion

        #region Constructors

        public AddEditViewTradeForm(TradeTypeEnum tradeType)
        {
            _formActionType = FormActionTypeEnum.Add;
            _tradeType = tradeType;
            InitializeComponent();
            InitializeControls(tradeType);
        }

        public AddEditViewTradeForm(FormActionTypeEnum formActionType, long tradeInfoId, TradeTypeEnum tradeType)
        {
            _formActionType = formActionType;
            _tradeInfoId = tradeInfoId;
            _tradeType = tradeType;

            fetchAllData = true;
            InitializeComponent();
            InitializeControls(_tradeType);
        }

        public AddEditViewTradeForm(FormActionTypeEnum formActionType, Trade trade)
        {
            _formActionType = formActionType;
            _tradeType = trade.Type;
            _trade = trade;
            _tradeInfoId = trade.Info.Id;

            fetchAllData = false;
            InitializeComponent();
            InitializeControls(_tradeType);
        }

        #endregion

        #region Private Methods

        private void InitializeControls(TradeTypeEnum tradeType)
        {
            isLoad = true;

            LayoutControlItem layoutItem;
            layoutTradeExtraInfo.BeginUpdate();
            switch (tradeType)
            {
                case TradeTypeEnum.TC:
                    _extraTradeInfoControl = _formActionType == FormActionTypeEnum.Add
                                                 ? new AddEditViewTradeTCInfoControl()
                                                 : new AddEditViewTradeTCInfoControl(_formActionType);
                    layoutItem = new LayoutControlItem
                    {
                        Name = _formActionType == FormActionTypeEnum.Add ? "layoutAddTrade" : "layoutTrade" + _formActionType.ToString("g"),
                        Parent = layoutTradeExtraInfo,
                        Control = _extraTradeInfoControl,
                        TextVisible = false
                    };
                    layoutItem.Control = _extraTradeInfoControl;
                    break;
                case TradeTypeEnum.FFA:
                    _extraTradeInfoControl = _formActionType == FormActionTypeEnum.Add
                                                 ? new AddEditViewTradeFFAInfoControl()
                                                 : new AddEditViewTradeFFAInfoControl(_formActionType);
                    layoutItem = new LayoutControlItem
                    {
                        Name = _formActionType == FormActionTypeEnum.Add ? "layoutAddTrade" : "layoutTrade" + _formActionType.ToString("g"),
                        Parent = layoutTradeExtraInfo,
                        Control = _extraTradeInfoControl,
                        TextVisible = false
                    };
                    layoutItem.Control = _extraTradeInfoControl;
                    break;
                case TradeTypeEnum.Cargo:
                    _extraTradeInfoControl = _formActionType == FormActionTypeEnum.Add
                                                 ? new AddEditViewTradeCargoInfoControl()
                                                 : new AddEditViewTradeCargoInfoControl(_formActionType);
                    layoutItem = new LayoutControlItem
                    {
                        Name = _formActionType == FormActionTypeEnum.Add ? "layoutAddTrade" : "layoutTrade" + _formActionType.ToString("g"),
                        Parent = layoutTradeExtraInfo,
                        Control = _extraTradeInfoControl,
                        TextVisible = false
                    };
                    layoutItem.Control = _extraTradeInfoControl;
                    break;
                case TradeTypeEnum.Option:
                    _extraTradeInfoControl = _formActionType == FormActionTypeEnum.Add
                                                 ? new AddEditViewTradeOptionInfoControl()
                                                 : new AddEditViewTradeOptionInfoControl(_formActionType);
                    layoutItem = new LayoutControlItem
                    {
                        Name = _formActionType == FormActionTypeEnum.Add ? "layoutAddTrade" : "layoutTrade" + _formActionType.ToString("g"),
                        Parent = layoutTradeExtraInfo,
                        Control = _extraTradeInfoControl,
                        TextVisible = false
                    };
                    layoutItem.Control = _extraTradeInfoControl;
                    break;
            }
            layoutTradeExtraInfo.EndUpdate();

            cmbState.Properties.Items.Add(TradeStateEnum.Draft);
            cmbState.Properties.Items.Add(TradeStateEnum.Confirmed);
            cmbState.Properties.Items.Add(TradeStateEnum.Locked);
            cmbState.SelectedIndex = 0;
            cmbState.Properties.ReadOnly = _formActionType != FormActionTypeEnum.Edit;

            dtpSignDate.DateTime = DateTime.Now;
            dtpSignDate.Properties.MaxValue = DateTime.Now;

            txtVentureShare.Properties.ReadOnly = true;
            txtVentureShare.Value = 100;

            treeBook.BeginUpdate();
            treeBook.Columns.Add();
            treeBook.Columns[0].Caption = Strings.Book_Name;
            treeBook.Columns[0].VisibleIndex = 0;
            treeBook.EndUpdate();

            lookupCompany.Properties.DisplayMember = "Name";
            lookupCompany.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Company_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupCompany.Properties.Columns.Add(col);
            lookupCompany.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCompany.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCompany.Properties.NullValuePrompt =
                Strings.Select_a_Company___;

            lookupCounterparty.Properties.DisplayMember = "Name";
            lookupCounterparty.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Counterparty_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupCounterparty.Properties.Columns.Add(col);
            lookupCounterparty.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupCounterparty.Properties.SearchMode = SearchMode.AutoFilter;
            lookupCounterparty.Properties.NullValuePrompt =
                Strings.Select_a_Counterparty___;

            lookupTrader.Properties.DisplayMember = "Name";
            lookupTrader.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Trader_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupTrader.Properties.Columns.Add(col);
            lookupTrader.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupTrader.Properties.SearchMode = SearchMode.AutoFilter;
            lookupTrader.Properties.NullValuePrompt =
                Strings.Select_a_Trader___;

            lookupBrokerInfo_1.Properties.DisplayMember = "Name";
            lookupBrokerInfo_1.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Broker_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupBrokerInfo_1.Properties.Columns.Add(col);
            lookupBrokerInfo_1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupBrokerInfo_1.Properties.SearchMode = SearchMode.AutoFilter;
            lookupBrokerInfo_1.Properties.NullValuePrompt =
                Strings.Select_a_Broker___;

            lookupMarketSegment.Properties.DisplayMember = "Name";
            lookupMarketSegment.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Segment,
                FieldName = "Name",
                Width = 0
            };
            lookupMarketSegment.Properties.Columns.Add(col);
            lookupMarketSegment.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMarketSegment.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMarketSegment.Properties.NullValuePrompt =
                Strings.Select_a_Market___;

            lookupMarketRegion.Properties.DisplayMember = "Name";
            lookupMarketRegion.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Region,
                FieldName = "Name",
                Width = 0
            };
            lookupMarketRegion.Properties.Columns.Add(col);
            lookupMarketRegion.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMarketRegion.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMarketRegion.Properties.NullValuePrompt =
                Strings.Select_a_Market_Region___;

            lookupMarketRoute.Properties.DisplayMember = "Name";
            lookupMarketRoute.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Route,
                FieldName = "Name",
                Width = 0
            };
            lookupMarketRoute.Properties.Columns.Add(col);
            col = new LookUpColumnInfo
            {
                Caption = Strings.Market_Route_Description,
                FieldName = "Description",
                Width = 0
            };
            lookupMarketRoute.Properties.Columns.Add(col);
            lookupMarketRoute.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMarketRoute.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMarketRoute.Properties.NullValuePrompt =
                Strings.Select_a_Market_Route___;

            lookupMTMForwardCurve.Properties.DisplayMember = "Name";
            lookupMTMForwardCurve.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.MTM_Forward_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupMTMForwardCurve.Properties.Columns.Add(col);
            lookupMTMForwardCurve.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMTMForwardCurve.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMTMForwardCurve.Properties.NullValuePrompt = Strings.Select_an_Index___;

            lookupMTMStressCurve.Properties.DisplayMember = "Name";
            lookupMTMStressCurve.Properties.ValueMember = "Id";
            col = new LookUpColumnInfo
            {
                Caption = Strings.MTM_Stress_Index,
                FieldName = "Name",
                Width = 0
            };
            lookupMTMStressCurve.Properties.Columns.Add(col);
            lookupMTMStressCurve.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupMTMStressCurve.Properties.SearchMode = SearchMode.AutoFilter;
            lookupMTMStressCurve.Properties.NullValuePrompt = Strings.Select_an_Index___;

            if (_formActionType == FormActionTypeEnum.Add)
                layoutItemTradeVersions.Visibility = LayoutVisibility.Never;
            if (_formActionType == FormActionTypeEnum.Edit)
                txtExternalCode.Properties.ReadOnly = true;
            if (_formActionType == FormActionTypeEnum.View)
            {
                btnSave.Visibility = BarItemVisibility.Never;
                layoutItemAddBroker.Visibility = LayoutVisibility.Never;
                layoutItemRemoveBroker.Visibility = LayoutVisibility.Never;

                cmbState.Properties.ReadOnly = true;
                txtExternalCode.Properties.ReadOnly = true;
                lookupCompany.Properties.ReadOnly = true;
                lookupCounterparty.Properties.ReadOnly = true;
                dtpSignDate.Properties.ReadOnly = true;
                lookupTrader.Properties.ReadOnly = true;
                txtStrategySet.Properties.ReadOnly = true;
                chkIsJointVenture.Properties.ReadOnly = true;
                treeBook.OptionsBehavior.Editable = false;
                lookupMarketRegion.Properties.ReadOnly = true;
                lookupMarketRoute.Properties.ReadOnly = true;
                lookupMarketSegment.Properties.ReadOnly = true;
                lookupMTMForwardCurve.Properties.ReadOnly = true;
                lookupMTMStressCurve.Properties.ReadOnly = true;
                txtComments.Properties.ReadOnly = true;
                txtVentureShare.Properties.ReadOnly = true;
            }
        }

        private void InitializeData(TradeTypeEnum tradeType)
        {
            switch (tradeType)
            {
                case TradeTypeEnum.TC:
                    AEVTradeInitializationData(false);
                    break;
                case TradeTypeEnum.FFA:
                    AEVTradeInitializationData(true);
                    break;
                case TradeTypeEnum.Cargo:
                    AEVTradeInitializationData(false);
                    break;
                case TradeTypeEnum.Option:
                    AEVTradeInitializationData(true);
                    break;
            }
        }

        private void FilterTraders()
        {
            lookupTrader.EditValue = null;
            lookupTrader.Properties.DataSource = null;

            if (lookupCompany.EditValue == null) return;

            lookupTrader.Properties.DataSource =
                ((List<Trader>)lookupTrader.Tag).Where(
                    a => a.Companies.Select(b => b.Id).Contains(Convert.ToInt32(lookupCompany.EditValue))).Select(a => a)
                    .OrderBy(a => a.Name).ToList();
        }

        private void FilterMarketSegments()
        {
            lookupMarketSegment.EditValue = null;
            lookupMarketSegment.Properties.DataSource = null;

            if (lookupTrader.EditValue == null) return;

            lookupMarketSegment.Properties.DataSource =
                ((List<Trader>)lookupTrader.Tag).Where(a => a.Id == (long)lookupTrader.EditValue).Single().Markets;
        }

        private void FilterMTMIndexes()
        {
            lookupMTMForwardCurve.EditValue = null;
            lookupMTMForwardCurve.Properties.DataSource = null;

            if (lookupMarketSegment.EditValue == null) return;

            lookupMTMForwardCurve.Properties.DataSource =
                ((List<Index>)lookupMTMForwardCurve.Tag).Where(
                    a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue)).ToList();

            lookupMTMStressCurve.EditValue = null;
            lookupMTMStressCurve.Properties.DataSource = null;

            lookupMTMStressCurve.Properties.DataSource =
                ((List<Index>)lookupMTMStressCurve.Tag).Where(
                    a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue)).ToList();
        }

        private void FilterBooks()
        {
            if (lookupTrader.EditValue == null) return;

            Trader trader = ((List<Trader>)lookupTrader.Tag).Where(a => a.Id == (long)lookupTrader.EditValue).Single();

            List<BookClient> bookClients = trader.Books.Select(a => new BookClient(a)).ToList();

            //foreach (var bookClient in bookClients)
            //{
            //    bookClient.ChildrenBooks = bookClient.Book.ChildrenBooks.Select(a => new BookClient(a)).ToList();
            //}

            var rootBookClient = new BookClient(null, trader.Books.ToList());
            rootBookClient.ChildrenBooks =
                trader.Books.Where(a => a.ParentBookId == null).Select(a => new BookClient(a, trader.Books.ToList())).
                    ToList();

            treeBook.Tag = bookClients;
            treeBook.DataSource = rootBookClient;
        }

        private bool ValidateData()
        {
            if (String.IsNullOrEmpty(txtExternalCode.Text.Trim()))
                errorProvider.SetError(txtExternalCode, Strings.Field_is_mandatory);
            if (lookupCompany.EditValue == null) errorProvider.SetError(lookupCompany, Strings.Field_is_mandatory);
            if (lookupCounterparty.EditValue == null)
                errorProvider.SetError(lookupCounterparty, Strings.Field_is_mandatory);
            if (lookupTrader.EditValue == null) errorProvider.SetError(lookupTrader, Strings.Field_is_mandatory);
            if (lookupMarketSegment.EditValue == null)
                errorProvider.SetError(lookupMarketSegment, Strings.Field_is_mandatory);
            if (lookupMarketRegion.EditValue == null)
                errorProvider.SetError(lookupMarketRegion, Strings.Field_is_mandatory);
            if (lookupMarketRoute.EditValue == null)
                errorProvider.SetError(lookupMarketRoute, Strings.Field_is_mandatory);
            if (lookupMTMForwardCurve.EditValue == null)
                errorProvider.SetError(lookupMTMForwardCurve, Strings.Field_is_mandatory);
            if (lookupMTMStressCurve.EditValue == null)
                errorProvider.SetError(lookupMTMStressCurve, Strings.Field_is_mandatory);

            foreach (object item in layoutBrokers.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBroker = (LayoutControlGroup)item;
                    if (layoutBroker.Name.StartsWith("layoutBroker_"))
                    {
                        string layoutLegSuffix = layoutBroker.Name.Substring(layoutBroker.Name.IndexOf("_") + 1);
                        LookUpEdit brokerInfo = (LookUpEdit)((LayoutControlItem)layoutBroker.Items.FindByName("layoutBrokerInfo_" + layoutLegSuffix)).Control;
                        if (brokerInfo.EditValue == null) errorProvider.SetError(brokerInfo, Strings.Field_is_mandatory);
                    }
                }
            }

            bool blExtraInfoIsValid;
            switch (_tradeType)
            {
                case TradeTypeEnum.TC:
                    blExtraInfoIsValid = ((AddEditViewTradeTCInfoControl)_extraTradeInfoControl).ValidateData();
                    if (!errorProvider.HasErrors) return blExtraInfoIsValid;
                    break;
                case TradeTypeEnum.FFA:
                    blExtraInfoIsValid = ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).ValidateData();
                    if (!errorProvider.HasErrors) return blExtraInfoIsValid;
                    break;
                case TradeTypeEnum.Cargo:
                    blExtraInfoIsValid = ((AddEditViewTradeCargoInfoControl)_extraTradeInfoControl).ValidateData();
                    if (!errorProvider.HasErrors) return blExtraInfoIsValid;
                    break;
                case TradeTypeEnum.Option:
                    blExtraInfoIsValid = ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).ValidateData();
                    if (!errorProvider.HasErrors) return blExtraInfoIsValid;
                    break;
            }

            return !errorProvider.HasErrors;
        }

        private void PropagateCheckStateToChildren(TreeListNode node)
        {
            foreach (TreeListNode treeListNode in node.Nodes)
            {
                treeListNode.Checked = node.Checked;
                PropagateCheckStateToChildren(treeListNode);
            }
        }

        private void PropagateCheckState(TreeListNode node)
        {
            PropagateCheckStateToChildren(node);

            bool checkState = node.Checked;
            node = node.ParentNode;
            while (node != null)
            {
                if (checkState)
                {
                    node.Checked = true;
                }
                else
                {
                    bool allChildNodesUnchecked = true;
                    foreach (TreeListNode treeListNode in node.Nodes)
                    {
                        if (!treeListNode.Checked) continue;
                        allChildNodesUnchecked = false;
                        break;
                    }
                    if (allChildNodesUnchecked)
                    {
                        node.Checked = false;
                    }
                }
                node = node.ParentNode;
            }
        }

        private void LoadData()
        {
            lblId.Text = _trade.Id.ToString();
            lblCode.Text = _trade.Info.Code;
            cmbState.EditValue = _trade.State;
            txtExternalCode.EditValue = _trade.Info.ExternalCode;
            lookupCompany.EditValue = _trade.Info.CompanyId;
            lookupCounterparty.EditValue = _trade.Info.CounterpartyId;
            dtpSignDate.EditValue = _trade.Info.SignDate;
            lookupTrader.EditValue = _trade.Info.TraderId;
            txtStrategySet.EditValue = _trade.Info.StrategySet;
            chkIsJointVenture.EditValue = _trade.Info.IsJointVenture;
            txtVentureShare.EditValue = _trade.Info.JointVentureShare;
            lookupMarketSegment.EditValue = _trade.Info.MarketId;
            lookupMarketRegion.EditValue = _trade.Info.RegionId;
            lookupMarketRoute.EditValue = _trade.Info.RouteId;
            lookupMTMForwardCurve.EditValue = _trade.Info.MTMFwdIndexId;
            lookupMTMStressCurve.EditValue = _trade.Info.MTMStressIndexId;
            txtComments.EditValue = _trade.Info.Comments;

            #region Load Books
            treeBook.ExpandAll();
            foreach (TradeInfoBook tradeInfoBook in _trade.Info.TradeInfoBooks)
            {
                treeBook.NodesIterator.DoOperation(a =>
                {
                    if (((BookClient)treeBook.GetDataRecordByNode(a)).Book.Id == tradeInfoBook.BookId)
                    {
                        a.Checked = true;
                    }
                });
            }

            #endregion

            #region Load Brokers
            layoutControl2.BeginUpdate();
            layoutBrokers.Clear();
            foreach (TradeBrokerInfo tradeBrokerInfo in _trade.Info.TradeBrokerInfos)
            {
                int counter = Convert.ToInt32(layoutBrokers.Items.Count + 1);

                var lookupBroker_new = new LookUpEdit();
                lookupBroker_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View;
                var txtBrokerCommission_new = new SpinEdit();
                txtBrokerCommission_new.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View;

                // 
                // lookupBroker
                // 
                lookupBroker_new.Name = "lookupBrokerInfo_" + counter;
                lookupBroker_new.Properties.DisplayMember = "Name";
                lookupBroker_new.Properties.ValueMember = "Id";
                var col = new LookUpColumnInfo
                {
                    Caption = Strings.Broker_Name,
                    FieldName = "Name",
                    Width = 0
                };
                lookupBroker_new.Properties.Columns.Add(col);
                lookupBroker_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                lookupBroker_new.Properties.SearchMode = SearchMode.AutoFilter;
                lookupBroker_new.Properties.NullValuePrompt = Strings.Select_a_Broker___;
                lookupBroker_new.QueryPopUp += new System.ComponentModel.CancelEventHandler(lookupBroker_new_QueryPopUp);
                lookupBroker_new.Properties.DataSource = _brokers;
                // 
                // txtBrokerCommission
                // 
                txtBrokerCommission_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
                txtBrokerCommission_new.Name = "txtBrokerCommission_" + counter;
                txtBrokerCommission_new.Properties.Mask.EditMask = @"P2";
                txtBrokerCommission_new.Properties.MinValue = 0;
                txtBrokerCommission_new.Properties.MaxValue = 100;
                txtBrokerCommission_new.Properties.MaxLength = 6;
                txtBrokerCommission_new.Value = 100;
                txtBrokerCommission_new.Properties.Mask.UseMaskAsDisplayFormat = true;

                LayoutControlGroup layoutBroker_new = layoutBrokers.AddGroup();

                // 
                // layoutBroker
                // 
                layoutBroker_new.GroupBordersVisible = false;
                layoutBroker_new.Name = "layoutBroker_" + counter;
                layoutBroker_new.Padding = new Padding(0, 0, 0, 0);
                layoutBroker_new.Tag = counter;
                layoutBroker_new.Text = "layoutBroker";
                layoutBroker_new.TextVisible = false;
                layoutBroker_new.DefaultLayoutType = LayoutType.Horizontal;
                // 
                // layoutBrokerName
                // 
                LayoutControlItem layoutBrokerName_new = layoutBroker_new.AddItem();
                layoutBrokerName_new.Control = lookupBroker_new;
                layoutBrokerName_new.Name = "layoutBrokerInfo_" + counter;
                layoutBrokerName_new.Text = "Broker:";
                // 
                // layoutBrokerCommission
                // 
                LayoutControlItem layoutBrokerCommission_new = layoutBroker_new.AddItem();
                layoutBrokerCommission_new.Control = txtBrokerCommission_new;
                layoutBrokerCommission_new.ControlMaxSize = new Size(60, 20);
                layoutBrokerCommission_new.ControlMinSize = new Size(60, 20);
                layoutBrokerCommission_new.Name = "layoutBrokerCommission_" + counter;
                layoutBrokerCommission_new.SizeConstraintsType = SizeConstraintsType.Custom;
                layoutBrokerCommission_new.Text = "Commission:";

                lookupBroker_new.EditValue = tradeBrokerInfo.BrokerId;
                txtBrokerCommission_new.EditValue = tradeBrokerInfo.Commission;
            }
            layoutControl2.EndUpdate();
            layoutControl2.BestFit();

            #endregion

            #region Load Specific Trade Type Control Data

            switch (_tradeType)
            {
                case TradeTypeEnum.TC:
                    var extraControlTc = (AddEditViewTradeTCInfoControl)_extraTradeInfoControl;
                    extraControlTc.LoadData(_trade.Info);
                    if (_formActionType == FormActionTypeEnum.View)
                        extraControlTc.SetReadOnlyState();
                    break;
                case TradeTypeEnum.FFA:
                    var extraControlFfa = (AddEditViewTradeFFAInfoControl)_extraTradeInfoControl;
                    extraControlFfa.LoadData(_trade.Info);
                    if (_formActionType == FormActionTypeEnum.View)
                        extraControlFfa.SetReadOnlyState();
                    break;
                case TradeTypeEnum.Cargo:
                    var extraControlCargo = (AddEditViewTradeCargoInfoControl)_extraTradeInfoControl;
                    extraControlCargo.LoadData(_trade.Info);
                    if (_formActionType == FormActionTypeEnum.View)
                        extraControlCargo.SetReadOnlyState();
                    break;
                case TradeTypeEnum.Option:
                    var extraControlOption = (AddEditViewTradeOptionInfoControl)_extraTradeInfoControl;
                    extraControlOption.LoadData(_trade.Info);
                    if (_formActionType == FormActionTypeEnum.View)
                        extraControlOption.SetReadOnlyState();
                    break;
            }

            #endregion
        }

        #endregion

        #region Proxy Calls

        private void AEVTradeInitializationData(bool getExtraData)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginAEVTradeInitializationData(_tradeInfoId,
                                                                       _formActionType,
                                                                       getExtraData,
                                                                       EndAEVTradeInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndAEVTradeInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndAEVTradeInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<Vessel> vessels;
            List<Market> marketSegments;
            List<Route> marketRoutes;
            List<Region> marketRegions;
            List<Company> companies, counterparties, banks;
            List<Trader> traders;
            List<Index> indexes;
            List<Account> accounts;
            List<ClearingHouse> clearingHouses;
            List<VesselPool> vesselPools;
            List<VesselBenchmarking> vesselBenchmarkings;
            List<TradeVersionInfo> tradeVersionInfos;
            List<TradeVersionInfo> tcTradeInfos;
            Trade trade;
            try
            {
                result = SessionRegistry.Client.EndAEVTradeInitializationData(out companies, out counterparties,
                                                                              out traders,
                                                                              out _brokers, out marketSegments,
                                                                              out marketRegions,
                                                                              out marketRoutes, out vessels,
                                                                              out indexes, out banks,
                                                                              out accounts, out clearingHouses,
                                                                              out vesselPools, out vesselBenchmarkings,
                                                                              out tradeVersionInfos, out trade, out tcTradeInfos,
                                                                              ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                {
                    _trade = trade;
                    _tradeType = trade.Type;
                }

                lookupCompany.Properties.DataSource = companies;
                lookupCompany.Tag = companies;
                lookupCounterparty.Properties.DataSource = counterparties;
                lookupCounterparty.Tag = counterparties;
                lookupTrader.Tag = traders;
                lookupBrokerInfo_1.Properties.DataSource = _brokers;
                lookupBrokerInfo_1.Tag = _brokers;
                lookupMarketSegment.Tag = marketSegments;

                lookupMarketRegion.Properties.DataSource = marketRegions;
                lookupMarketRegion.Tag = marketRegions;
                lookupMarketRegion.EditValue =
                    ((List<Region>)lookupMarketRegion.Tag).Where(a => a.IsDefault).SingleOrDefault() == null
                        ? (long?)null
                        : ((List<Region>)lookupMarketRegion.Tag).Where(a => a.IsDefault).Single().Id;

                lookupMarketRoute.Properties.DataSource = marketRoutes.OrderBy(a => a.Id).ToList();
                lookupMarketRoute.Tag = marketRoutes;
                lookupMarketRoute.EditValue =
                    ((List<Route>)lookupMarketRoute.Tag).Where(a => a.IsDefault).SingleOrDefault() == null
                        ? (long?)null
                        : ((List<Route>)lookupMarketRoute.Tag).Where(a => a.IsDefault).SingleOrDefault().Id;

                lookupMTMForwardCurve.Tag = indexes;
                lookupMTMStressCurve.Tag = indexes;

                lookUpVersions.Properties.DisplayMember = "Code";
                lookUpVersions.Properties.ValueMember = "TradeInfoId";
                var col = new LookUpColumnInfo
                {
                    Caption = Strings.Code,
                    FieldName = "Code",
                    Width = 0
                };
                lookUpVersions.Properties.Columns.Add(col);
                col = new LookUpColumnInfo
                {
                    Caption = "External Code",
                    FieldName = "ExternalCode",
                    Width = 0
                };
                lookUpVersions.Properties.Columns.Add(col);
                col = new LookUpColumnInfo
                {
                    Caption = Strings.Date_From,
                    FieldName = "DateFrom",
                    Width = 0
                };
                lookUpVersions.Properties.Columns.Add(col);
                col = new LookUpColumnInfo
                {
                    Caption = Strings.Date_To,
                    FieldName = "DateTo",
                    Width = 0
                };
                lookUpVersions.Properties.Columns.Add(col);
                lookUpVersions.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                lookUpVersions.Properties.SearchMode = SearchMode.AutoFilter;

                lookUpVersions.Properties.DataSource = tradeVersionInfos;
                lookUpVersions.EditValue = _tradeInfoId;

                switch (_tradeType)
                {
                    case TradeTypeEnum.TC:
                        ((AddEditViewTradeTCInfoControl)_extraTradeInfoControl).SetInitializationData(vessels, indexes,
                                                                                                       vesselBenchmarkings);
                        break;
                    case TradeTypeEnum.FFA:
                        ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetInitializationData(indexes, vessels,
                                                                                                        vesselPools,
                                                                                                        banks, accounts,
                                                                                                        clearingHouses);
                        break;
                    case TradeTypeEnum.Cargo:
                        ((AddEditViewTradeCargoInfoControl)_extraTradeInfoControl).SetInitializationData(vessels,
                                                                                                          indexes,
                                                                                                          vesselBenchmarkings);
                        break;
                    case TradeTypeEnum.Option:
                        ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetInitializationData(indexes,
                                                                                                           vessels,
                                                                                                           vesselPools,
                                                                                                           banks,
                                                                                                           accounts,
                                                                                                           clearingHouses, tcTradeInfos);

                        break;
                }

                if (_tradeType == TradeTypeEnum.Option && _formActionType != FormActionTypeEnum.Add)
                    ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetSignDate(_trade.Info.SignDate,
                                                                                                 vesselPools);

                else if (_tradeType == TradeTypeEnum.FFA && _formActionType != FormActionTypeEnum.Add)
                    ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetSignDate(_trade.Info.SignDate,
                                                                                              vesselPools);

                if (_formActionType == FormActionTypeEnum.Edit || _formActionType == FormActionTypeEnum.View)
                {
                    LoadData();
                }

                isLoad = false;
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }

        private void BeginInsertNewTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo, List<TradeTcInfoLeg> tradeTCInfoLegs,
            TradeFfaInfo tradeFfaInfo, TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs, TradeOptionInfo tradeOptionInfo,
            List<TradeBrokerInfo> tradeBrokerInfos, List<Trade> poolTrades)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginInsertNewTrade(trade, tradeInfo, tradeTcInfo, tradeTCInfoLegs,
                                                           tradeFfaInfo, tradeCargoInfo, tradeCargoInfoLegs,
                                                           tradeOptionInfo,
                                                           tradeBrokerInfos, _tradeInfoBooks, poolTrades,
                                                           EndBeginInsertNewTrade, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndBeginInsertNewTrade(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndBeginInsertNewTrade;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndInsertNewTrade(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        private void BeginUpdateTrade(Trade trade, TradeInfo tradeInfo, TradeTcInfo tradeTcInfo, List<TradeTcInfoLeg> tradeTCInfoLegs,
            TradeFfaInfo tradeFfaInfo, TradeCargoInfo tradeCargoInfo, List<TradeCargoInfoLeg> tradeCargoInfoLegs, TradeOptionInfo tradeOptionInfo, List<TradeBrokerInfo> tradeBrokerInfos, List<Trade> poolTrades)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginUpdateTrade(trade, tradeInfo, tradeTcInfo, tradeTCInfoLegs,
                                                        tradeFfaInfo, tradeCargoInfo, tradeCargoInfoLegs,
                                                        tradeOptionInfo,
                                                        tradeBrokerInfos, _tradeInfoBooks, poolTrades,
                                                        EndUpdateTrade, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndUpdateTrade(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndUpdateTrade;
                Invoke(action, ar);
                return;
            }
            int? result;
            try
            {
                result = SessionRegistry.Client.EndUpdateTrade(ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this, Strings.Entity_inserted_updated_successfully_, Strings.Freight_Metrics, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;
                if (OnDataSaved != null)
                    OnDataSaved(this, true);
            }
        }

        private void BeginGetVesselPoolsBySignDate(DateTime signDate)
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGetVesselPoolsBySignDate(signDate, EndGetVesselPoolsBySignDate, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGetVesselPoolsBySignDate(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGetVesselPoolsBySignDate;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<VesselPool> vesselPools;
            try
            {
                result = SessionRegistry.Client.EndGetVesselPoolsBySignDate(out vesselPools, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                Cursor = Cursors.Default;

                if (_tradeType == TradeTypeEnum.FFA)
                    ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetSignDate(dtpSignDate.DateTime, vesselPools);
                else if (_tradeType == TradeTypeEnum.Option)
                    ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetSignDate(dtpSignDate.DateTime, vesselPools);

                lock (_syncObject)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                }
            }
        }


        #endregion

        #region GUI Events 

        private void chkIsJointVenture_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsJointVenture.Checked)
            {
                txtVentureShare.Value = 50;
                txtVentureShare.Properties.ReadOnly = _formActionType == FormActionTypeEnum.View;
            }
            else
            {
                txtVentureShare.Value = 100;
                txtVentureShare.Properties.ReadOnly = true;
            }
        }

        private void AddEditViewTradeControl_Load(object sender, EventArgs e)
        {
            InitializeData(_tradeType);
        }

        private void btnAddBroker_Click(object sender, EventArgs e)
        {
            foreach (object item in layoutBrokers.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBroker = (LayoutControlGroup)item;
                    if (layoutBroker.Name.StartsWith("layoutBroker_"))
                    {
                        string layoutLegSuffix = layoutBroker.Name.Substring(layoutBroker.Name.IndexOf("_") + 1);
                        var broker = (LookUpEdit)((LayoutControlItem)layoutBroker.Items.FindByName("layoutBrokerInfo_" + layoutLegSuffix)).Control;
                        if (broker.EditValue == null)
                            return;
                    }
                }
            }

            layoutControl2.BeginUpdate();

            int counter;
            if (layoutBrokers.Items.Count == 0)
                counter = 1;
            else counter = Convert.ToInt32(layoutBrokers.Items[layoutBrokers.Items.Count - 1].Tag) + 1;

            var lookupBroker_new = new LookUpEdit();
            var txtBrokerCommission_new = new SpinEdit();
            // 
            // lookupBroker
            // 
            lookupBroker_new.Name = "lookupBrokerInfo_" + counter;
            lookupBroker_new.Properties.DisplayMember = "Name";
            lookupBroker_new.Properties.ValueMember = "Id";
            var col = new LookUpColumnInfo
            {
                Caption = Strings.Broker_Name,
                FieldName = "Name",
                Width = 0
            };
            lookupBroker_new.Properties.Columns.Add(col);
            lookupBroker_new.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            lookupBroker_new.Properties.SearchMode = SearchMode.AutoFilter;
            lookupBroker_new.Properties.NullValuePrompt =
                Strings.Select_a_Broker___;
            lookupBroker_new.QueryPopUp += new System.ComponentModel.CancelEventHandler(lookupBroker_new_QueryPopUp);

            //    cmbIndex_new.TabIndex = 8;

            // 
            // txtBrokerCommission
            // 
            txtBrokerCommission_new.EditValue = new decimal(new[]
                                                                {
                                                                    0,
                                                                    0,
                                                                    0,
                                                                    0
                                                                });
            txtBrokerCommission_new.Name = "txtBrokerCommission_" + counter;
            txtBrokerCommission_new.Properties.Mask.EditMask = @"P2";
            txtBrokerCommission_new.Properties.MinValue = 0;
            txtBrokerCommission_new.Properties.MaxValue = 100;
            txtBrokerCommission_new.Properties.MaxLength = 6;
            txtBrokerCommission_new.Value = 0;
            txtBrokerCommission_new.Properties.Mask.UseMaskAsDisplayFormat = true;
            //   txtRate_new.TabIndex = 7;

            LayoutControlGroup layoutBroker_new = layoutBrokers.AddGroup();

            // 
            // layoutBroker
            // 
            layoutBroker_new.GroupBordersVisible = false;
            layoutBroker_new.Name = "layoutBroker_" + counter;
            layoutBroker_new.Padding = new Padding(0, 0, 0, 0);
            layoutBroker_new.Tag = counter;
            layoutBroker_new.Text = "layoutBroker";
            layoutBroker_new.TextVisible = false;
            layoutBroker_new.DefaultLayoutType = LayoutType.Horizontal;
            // 
            // layoutBrokerName
            // 
            LayoutControlItem layoutBrokerName_new = layoutBroker_new.AddItem();
            layoutBrokerName_new.Control = lookupBroker_new;
            layoutBrokerName_new.Name = "layoutBrokerInfo_" + counter;
            layoutBrokerName_new.Text = "Broker:";
            // 
            // layoutBrokerCommission
            // 
            LayoutControlItem layoutBrokerCommission_new = layoutBroker_new.AddItem();
            layoutBrokerCommission_new.Control = txtBrokerCommission_new;
            layoutBrokerCommission_new.ControlMaxSize = new Size(60, 20);
            layoutBrokerCommission_new.ControlMinSize = new Size(60, 20);
            layoutBrokerCommission_new.Name = "layoutBrokerCommission_" + counter;
            layoutBrokerCommission_new.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutBrokerCommission_new.Text = "Commission:";

            layoutControl2.EndUpdate();
            layoutControl2.BestFit();
        }

        private void lookupBroker_new_QueryPopUp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedBrokerIds = new List<long>();
            foreach (object item in layoutBrokers.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBroker = (LayoutControlGroup)item;
                    if (layoutBroker.Name.StartsWith("layoutBroker_"))
                    {
                        string layoutLegSuffix = layoutBroker.Name.Substring(layoutBroker.Name.IndexOf("_") + 1);
                        var broker = (LookUpEdit)((LayoutControlItem)layoutBroker.Items.FindByName("layoutBrokerInfo_" + layoutLegSuffix)).Control;
                        if (broker.EditValue != null && broker.Name != ((LookUpEdit)sender).Name)
                            selectedBrokerIds.Add((long)broker.EditValue);
                    }
                }
            }

            ((LookUpEdit)sender).Properties.DataSource = _brokers.Where(a => !selectedBrokerIds.Contains(a.Id)).ToList();
        }

        private void lookupBrokerInfo_1_QueryPopUp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedBrokerIds = new List<long>();
            foreach (object item in layoutBrokers.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBroker = (LayoutControlGroup)item;
                    if (layoutBroker.Name.StartsWith("layoutBroker_"))
                    {
                        string layoutLegSuffix = layoutBroker.Name.Substring(layoutBroker.Name.IndexOf("_") + 1);
                        var broker = (LookUpEdit)((LayoutControlItem)layoutBroker.Items.FindByName("layoutBrokerInfo_" + layoutLegSuffix)).Control;
                        if (broker.EditValue != null && broker.Name != ((LookUpEdit)sender).Name)
                            selectedBrokerIds.Add((long)broker.EditValue);
                    }
                }
            }

            ((LookUpEdit)sender).Properties.DataSource = _brokers.Where(a => !selectedBrokerIds.Contains(a.Id)).ToList();
        }

        private void btnRemoveBroker_Click(object sender, EventArgs e)
        {
            if (layoutBrokers.Items.Count == 1) return;

            layoutControl2.BeginUpdate();

            foreach (
                LayoutControlItem layoutControlItem in
                    ((LayoutControlGroup)layoutBrokers.Items[layoutBrokers.Items.Count - 1]).Items)
            {
                layoutControl2.Controls.Remove(layoutControlItem.Control);
            }

            layoutBrokers.RemoveAt(layoutBrokers.Items.Count - 1);
            layoutControl2.EndUpdate();
        }

        private void dtpSignDate_DateTimeChanged(object sender, EventArgs e)
        {
            switch (_tradeType)
            {
                case TradeTypeEnum.TC:
                    ((AddEditViewTradeTCInfoControl)_extraTradeInfoControl).SetSignDate(dtpSignDate.DateTime);
                    break;
                case TradeTypeEnum.FFA:
                    if (!isLoad)
                        BeginGetVesselPoolsBySignDate(dtpSignDate.DateTime.Date);
                    else
                        ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetSignDate(dtpSignDate.DateTime, null);
                    break;
                case TradeTypeEnum.Cargo:
                    ((AddEditViewTradeCargoInfoControl)_extraTradeInfoControl).SetSignDate(dtpSignDate.DateTime);
                    break;
                case TradeTypeEnum.Option:
                    if (!isLoad)
                        BeginGetVesselPoolsBySignDate(dtpSignDate.DateTime.Date);
                    else
                        ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetSignDate(dtpSignDate.DateTime, null);
                    break;
            }
        }

        private void lookupMarketSegment_EditValueChanged(object sender, EventArgs e)
        {
            FilterMTMIndexes();

            switch (_tradeType)
            {
                case TradeTypeEnum.TC:
                    if (lookupMarketSegment.EditValue == null)
                    {
                        ((AddEditViewTradeTCInfoControl)_extraTradeInfoControl).SetMarket(null);
                    }
                    else
                    {
                        ((AddEditViewTradeTCInfoControl)_extraTradeInfoControl).SetMarket(
                            ((List<Market>)lookupMarketSegment.Tag).Where(
                                a => a.Id == (long)lookupMarketSegment.EditValue)
                                .
                                SingleOrDefault());
                    }

                    //lookupMTMForwardCurve.EditValue =
                    //    ((List<Index>) lookupMTMForwardCurve.Tag).Where(
                    //        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).Any()
                    //        ? ((List<Index>) lookupMTMForwardCurve.Tag).Where(
                    //            a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).
                    //              Single().Id
                    //        : (long?) null;

                    //lookupMTMStressCurve.EditValue =
                    //    ((List<Index>) lookupMTMStressCurve.Tag).Where(
                    //        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).Any()
                    //        ? ((List<Index>) lookupMTMStressCurve.Tag).Where(
                    //            a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).
                    //              Single().Id
                    //        : (long?) null;

                    break;
                case TradeTypeEnum.FFA:
                    if (lookupMarketSegment.EditValue == null)
                    {
                        ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetMarket(null);
                    }
                    else
                    {
                        ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetMarket(
                            ((List<Market>)lookupMarketSegment.Tag).Where(
                                a => a.Id == (long)lookupMarketSegment.EditValue)
                                .
                                SingleOrDefault());
                    }
                    break;
                case TradeTypeEnum.Cargo:
                    if (lookupMarketSegment.EditValue == null)
                    {
                        ((AddEditViewTradeCargoInfoControl)_extraTradeInfoControl).SetMarket(null);
                    }
                    else
                    {
                        ((AddEditViewTradeCargoInfoControl)_extraTradeInfoControl).SetMarket(
                            ((List<Market>)lookupMarketSegment.Tag).Where(
                                a => a.Id == (long)lookupMarketSegment.EditValue)
                                .
                                SingleOrDefault());
                    }
                    //lookupMTMForwardCurve.EditValue =
                    //    ((List<Index>) lookupMTMForwardCurve.Tag).Where(
                    //        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).Any()
                    //        ? ((List<Index>) lookupMTMForwardCurve.Tag).Where(
                    //            a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).
                    //              Single().Id
                    //        : (long?) null;

                    //lookupMTMStressCurve.EditValue =
                    //    ((List<Index>) lookupMTMStressCurve.Tag).Where(
                    //        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).Any()
                    //        ? ((List<Index>) lookupMTMStressCurve.Tag).Where(
                    //            a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.IsMarketDefault).
                    //              Single().Id
                    //        : (long?) null;
                    break;

                case TradeTypeEnum.Option:
                    if (lookupMarketSegment.EditValue == null)
                    {
                        ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetMarket(null);
                    }
                    else
                    {
                        ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetMarket(
                            ((List<Market>)lookupMarketSegment.Tag).Where(
                                a => a.Id == (long)lookupMarketSegment.EditValue)
                                .
                                SingleOrDefault());
                    }
                    break;
            }
        }

        private void lookupCompany_EditValueChanged(object sender, EventArgs e)
        {
            FilterTraders();
            switch (_tradeType)
            {
                case TradeTypeEnum.TC:
                    break;
                case TradeTypeEnum.FFA:
                    if (lookupCompany.EditValue == null)
                    {
                        ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetCompany(null);
                    }
                    else
                    {
                        ((AddEditViewTradeFFAInfoControl)_extraTradeInfoControl).SetCompany(
                            ((List<Company>)lookupCompany.Tag).Where(a => a.Id == (long)lookupCompany.EditValue)
                                .
                                SingleOrDefault());
                    }
                    break;
                case TradeTypeEnum.Cargo:
                    break;
                case TradeTypeEnum.Option:
                    if (lookupCompany.EditValue == null)
                    {
                        ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetCompany(null);
                    }
                    else
                    {
                        ((AddEditViewTradeOptionInfoControl)_extraTradeInfoControl).SetCompany(
                            ((List<Company>)lookupCompany.Tag).Where(a => a.Id == (long)lookupCompany.EditValue)
                                .
                                SingleOrDefault());
                    }
                    break;
            }
        }

        private void lookupTrader_EditValueChanged(object sender, EventArgs e)
        {
            FilterMarketSegments();
            FilterBooks();
        }

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            errorProvider.ClearErrors();

            bool isValid = ValidateData();
            if (!isValid)
            {
                XtraMessageBox.Show(this, Strings.There_are_validation_errors_in_your_form_, Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Trade objTrade;
            if (_formActionType == FormActionTypeEnum.Add)
                objTrade = new Trade { State = TradeStateEnum.Draft, Status = ActivationStatusEnum.Active, Type = _tradeType };
            else
            {
                _trade.State = (TradeStateEnum)cmbState.EditValue;
                objTrade = _trade;
            }

            var objTradeInfo = new TradeInfo
            {
                Comments = txtComments.Text.Trim(),
                CompanyId = (long)lookupCompany.EditValue,
                CounterpartyId = (long)lookupCounterparty.EditValue,
                MTMFwdIndexId = (long)lookupMTMForwardCurve.EditValue,
                MTMStressIndexId = (long)lookupMTMStressCurve.EditValue,
                ExternalCode = txtExternalCode.Text.Trim(),
                IsJointVenture = chkIsJointVenture.Checked,
                JointVentureShare =
                                           chkIsJointVenture.Checked ? (int)txtVentureShare.Value : (int?)null,
                MarketId = (long)lookupMarketSegment.EditValue,
                RegionId = (long)lookupMarketRegion.EditValue,
                RouteId = (long)lookupMarketRoute.EditValue,
                SignDate = dtpSignDate.DateTime.Date,
                TraderId = (long)lookupTrader.EditValue,
                StrategySet = txtStrategySet.Text.Trim(),
                Version = 1
            };

            List<TradeBrokerInfo> tradeBrokerInfos = new List<TradeBrokerInfo>();
            foreach (object item in layoutBrokers.Items)
            {
                if (item.GetType() == typeof(LayoutControlGroup))
                {
                    var layoutBroker = (LayoutControlGroup)item;
                    if (layoutBroker.Name.StartsWith("layoutBroker_"))
                    {
                        string layoutLegSuffix = layoutBroker.Name.Substring(layoutBroker.Name.IndexOf("_") + 1);
                        SpinEdit commission = (SpinEdit)((LayoutControlItem)layoutBroker.Items.FindByName("layoutBrokerCommission_" + layoutLegSuffix)).Control;
                        LookUpEdit broker = (LookUpEdit)((LayoutControlItem)layoutBroker.Items.FindByName("layoutBrokerInfo_" + layoutLegSuffix)).Control;

                        var tradeBrokerInfo = new TradeBrokerInfo { BrokerId = (long)broker.EditValue, Commission = commission.Value };
                        tradeBrokerInfos.Add(tradeBrokerInfo);
                    }
                }
            }

            TradeTcInfo objTradeTcInfo = null;
            TradeCargoInfo objTradeCargoInfo = null;
            var objTradeTCInfoLegs = new List<TradeTcInfoLeg>();
            var objTradeCargoInfoLegs = new List<TradeCargoInfoLeg>();
            TradeFfaInfo objTradeFfaInfo = null;
            TradeOptionInfo objTradeOptionInfo = null;
            TradeInfoDirectionEnum direction;
            List<Trade> poolTrades = new List<Trade>();
            VesselPool vesselPool;

            switch (_tradeType)
            {
                case TradeTypeEnum.TC:
                    string delivery;
                    string redelivery;
                    long vesselId;
                    decimal vesselIndex;

                    decimal? address;
                    int? ballastBonus;
                    bool isBareboat;

                    var extraControlTC = (AddEditViewTradeTCInfoControl)_extraTradeInfoControl;

                    extraControlTC.GetData(out delivery, out direction, out redelivery, out isBareboat, out vesselId,
                                           out vesselIndex, out address,
                                           out ballastBonus, out objTradeTCInfoLegs);

                    objTradeInfo.Direction = direction;

                    objTradeTcInfo = new TradeTcInfo
                    {
                        Delivery = delivery,
                        Redelivery = redelivery,
                        VesselId = vesselId,
                        VesselIndex = vesselIndex,
                        Address = address,
                        BallastBonus = ballastBonus,
                        IsBareboat = isBareboat
                    };

                    objTradeInfo.PeriodFrom = objTradeTCInfoLegs.First().PeriodFrom;
                    objTradeInfo.PeriodTo = objTradeTCInfoLegs.Last().PeriodTo;

                    break;
                case TradeTypeEnum.FFA:
                    long indexId2;
                    TradeInfoVesselAllocationTypeEnum allocationType;
                    long? allocationVesselId;
                    long? allocationVesselPoolId;
                    long? clearingBankId;
                    long? clearingAccountId;
                    long? clearingHouseId;
                    PeriodTypeEnum periodType;
                    string periodString;
                    List<DateTime> periodDates;
                    TradeInfoPeriodDayCountTypeEnum periodDayCountType;
                    TradeInfoQuantityTypeEnum quantityType;
                    decimal quantity;
                    decimal price;
                    int ffaClearingFees;
                    decimal closedOutQuantityTotal;
                    TradeInfoSettlementTypeEnum settlementType;
                    TradeFFAInfoPurposeEnum purpose;

                    var extraControlFFA = (AddEditViewTradeFFAInfoControl)_extraTradeInfoControl;
                    extraControlFFA.GetData(out direction, out indexId2,
                                            out allocationType, out allocationVesselId,
                                            out allocationVesselPoolId, out clearingBankId, out clearingAccountId,
                                            out clearingHouseId, out periodType, out periodString, out periodDates,
                                            out periodDayCountType, out quantityType, out quantity, out price,
                                            out settlementType, out purpose, out vesselPool, out ffaClearingFees,
                                            out closedOutQuantityTotal);

                    DateTime perFrom = DateTime.Now;
                    DateTime perTo = DateTime.Now;

                    switch (periodType)
                    {
                        case PeriodTypeEnum.Month:
                            perFrom = periodDates.OrderBy(a => a).Take(1).Select(
                                b => new DateTime(b.Year, b.Month, 1)).Single();
                            perTo = periodDates.OrderByDescending(a => a).Take(1).Select(
                                b => new DateTime(b.Year, b.Month, 1)).Single().AddMonths(1).
                                AddDays(-1);
                            break;
                        case PeriodTypeEnum.Quarter:
                            perFrom = periodDates.OrderBy(a => a).Take(1).Select(
                                b => new DateTime(b.Year, ((((b.Month - 1) / 3) + 1) - 1) * 3 + 1, 1)).Single();
                            perTo = periodDates.OrderByDescending(a => a).Take(1).Select(
                                b => new DateTime(b.Year, ((((b.Month - 1) / 3) + 1) - 1) * 3 + 1, 1)).Single().AddMonths(3)
                                .
                                AddDays(-1);
                            break;
                        case PeriodTypeEnum.Calendar:
                            perFrom = periodDates.OrderBy(a => a).Take(1).Select(
                                b => new DateTime(b.Year, 1, 1)).Single();
                            perTo = periodDates.OrderByDescending(a => a).Take(1).Select(
                                b => new DateTime(b.Year, 1, 1)).Single().AddYears(1).
                                AddDays(-1);
                            break;
                    }

                    objTradeInfo.PeriodFrom = perFrom.Date;
                    objTradeInfo.PeriodTo = perTo.Date;
                    objTradeInfo.Direction = direction;

                    objTradeFfaInfo = new TradeFfaInfo
                    {
                        AccountId = clearingAccountId,
                        BankId = clearingBankId,
                        ClearingHouseId = clearingHouseId,
                        IndexId = indexId2,
                        AllocationType = allocationType,
                        PeriodDayCountType = periodDayCountType,
                        Period = periodString,
                        PeriodType = periodType,
                        VesselPoolId = allocationVesselPoolId,
                        QuantityDays = quantity,
                        QuantityType = quantityType,
                        Price = price,
                        VesselId = allocationVesselId,
                        SettlementType = settlementType,
                        Purpose = purpose,
                        ClearingFees = ffaClearingFees,
                        ClosedOutQuantityTotal = closedOutQuantityTotal
                    };

                    objTradeInfo.FFAInfo = objTradeFfaInfo;
                    objTradeInfo.TradeBrokerInfos = tradeBrokerInfos;
                    objTradeInfo.TradeInfoBooks = _tradeInfoBooks;
                    objTrade.Info = objTradeInfo;

                    if (vesselPool != null)
                    {
                        List<Vessel> vessels = vesselPool.CurrentVessels;

                        //Compute quantity days
                        decimal sumOfVesselIndexes = 0;
                        sumOfVesselIndexes = vessels.Aggregate(sumOfVesselIndexes,
                                                               (current, vessel) =>
                                                               current + vessel.CurrentVesselBenchMarking.Benchmark);

                        foreach (var vessel in vessels)
                        {
                            var vesselTrade = objTrade.Clone();
                            vesselTrade.Info.FFAInfo.VesselId = vessel.Id;
                            vesselTrade.Info.FFAInfo.VesselPoolId = null;
                            vesselTrade.Info.FFAInfo.AllocationType = TradeInfoVesselAllocationTypeEnum.Single;
                            vesselTrade.Info.FFAInfo.QuantityDays = (vessel.CurrentVesselBenchMarking.Benchmark * objTradeFfaInfo.QuantityDays) / sumOfVesselIndexes;

                            poolTrades.Add(vesselTrade);
                        }
                        foreach (var vessel in vessels)
                        {
                            var vesselTrade = objTrade.Clone();
                            vesselTrade.Info.Direction = vesselTrade.Info.Direction == TradeInfoDirectionEnum.OutOrSell
                                                             ? TradeInfoDirectionEnum.InOrBuy
                                                             : TradeInfoDirectionEnum.OutOrSell;
                            vesselTrade.Info.FFAInfo.VesselId = vessel.Id;
                            vesselTrade.Info.FFAInfo.VesselPoolId = null;
                            vesselTrade.Info.FFAInfo.AllocationType = TradeInfoVesselAllocationTypeEnum.Single;
                            vesselTrade.Info.FFAInfo.QuantityDays = (vessel.CurrentVesselBenchMarking.Benchmark * objTradeFfaInfo.QuantityDays) / sumOfVesselIndexes;

                            poolTrades.Add(vesselTrade);
                        }
                    }

                    break;
                case TradeTypeEnum.Cargo:
                    string loadingPort;
                    string dischargingPort;
                    long? vesselIdC;
                    decimal vesselIndexC;
                    decimal? addressC;

                    var extraControlCargo = (AddEditViewTradeCargoInfoControl)_extraTradeInfoControl;

                    extraControlCargo.GetData(out direction, out loadingPort, out dischargingPort, out vesselIdC,
                                           out vesselIndexC, out addressC,
                                           out objTradeCargoInfoLegs);

                    objTradeInfo.Direction = direction;

                    objTradeCargoInfo = new TradeCargoInfo
                    {
                        VesselId = vesselIdC,
                        VesselIndex = vesselIndexC,
                        Address = addressC,
                        LoadingPort = loadingPort,
                        DischargingPort = dischargingPort
                    };

                    objTradeInfo.PeriodFrom = objTradeCargoInfoLegs.First().PeriodFrom;
                    objTradeInfo.PeriodTo = objTradeCargoInfoLegs.Last().PeriodTo;
                    break;
                case TradeTypeEnum.Option:
                    long indexIdOp;
                    TradeInfoVesselAllocationTypeEnum allocationTypeOp;
                    long? allocationVesselIdOp;
                    long? allocationVesselPoolIdOp;
                    long? clearingBankIdOp;
                    long? clearingAccountIdOp;
                    long? clearingHouseIdOp;
                    PeriodTypeEnum periodTypeOp;
                    string periodStringOp;
                    List<DateTime> periodDatesOp;
                    TradeInfoPeriodDayCountTypeEnum periodDayCountTypeOp;
                    TradeInfoQuantityTypeEnum quantityTypeOp;
                    decimal quantityOp;
                    TradeInfoSettlementTypeEnum settlementTypeOp;
                    TradeOPTIONInfoTypeEnum tradeOptionInfoTypeEnum;
                    decimal strike;
                    decimal premium;
                    int clearingFees;
                    DateTime premiumDueDate;
                    decimal? userMTM;
                    decimal? userDelta;
                    long? profitSharing;
                    TradeFFAInfoPurposeEnum optionPurpose;

                    var extraControlOption = (AddEditViewTradeOptionInfoControl)_extraTradeInfoControl;
                    extraControlOption.GetData(out direction, out indexIdOp,
                                               out allocationTypeOp, out allocationVesselIdOp,
                                               out allocationVesselPoolIdOp, out clearingBankIdOp,
                                               out clearingAccountIdOp,
                                               out clearingHouseIdOp, out periodTypeOp, out periodStringOp,
                                               out periodDatesOp,
                                               out periodDayCountTypeOp, out quantityTypeOp, out quantityOp, out premium,
                                               out settlementTypeOp, out tradeOptionInfoTypeEnum, out strike,
                                               out clearingFees, out premiumDueDate, out userMTM, out userDelta,
                                               out vesselPool, out profitSharing, out optionPurpose);

                    DateTime perFromOp = DateTime.Now;
                    DateTime perToOp = DateTime.Now;

                    switch (periodTypeOp)
                    {
                        case PeriodTypeEnum.Month:
                            perFromOp = periodDatesOp.OrderBy(a => a).Take(1).Select(
                                b => new DateTime(b.Year, b.Month, 1)).Single();
                            perToOp = periodDatesOp.OrderByDescending(a => a).Take(1).Select(
                                b => new DateTime(b.Year, b.Month, 1)).Single().AddMonths(1).
                                AddDays(-1);
                            break;
                        case PeriodTypeEnum.Quarter:
                            perFromOp = periodDatesOp.OrderBy(a => a).Take(1).Select(
                                b => new DateTime(b.Year, ((((b.Month - 1) / 3) + 1) - 1) * 3 + 1, 1)).Single();
                            perToOp = periodDatesOp.OrderByDescending(a => a).Take(1).Select(
                                b => new DateTime(b.Year, ((((b.Month - 1) / 3) + 1) - 1) * 3 + 1, 1)).Single().AddMonths(3)
                                .
                                AddDays(-1);
                            break;
                        case PeriodTypeEnum.Calendar:
                            perFromOp = periodDatesOp.OrderBy(a => a).Take(1).Select(
                                b => new DateTime(b.Year, 1, 1)).Single();
                            perToOp = periodDatesOp.OrderByDescending(a => a).Take(1).Select(
                                b => new DateTime(b.Year, 1, 1)).Single().AddYears(1).
                                AddDays(-1);
                            break;
                    }

                    objTradeInfo.PeriodFrom = perFromOp.Date;
                    objTradeInfo.PeriodTo = perToOp.Date;
                    objTradeInfo.Direction = direction;

                    objTradeOptionInfo = new TradeOptionInfo
                    {
                        AccountId = clearingAccountIdOp,
                        BankId = clearingBankIdOp,
                        ClearingHouseId = clearingHouseIdOp,
                        IndexId = indexIdOp,
                        AllocationType = allocationTypeOp,
                        PeriodDayCountType = periodDayCountTypeOp,
                        Period = periodStringOp,
                        PeriodType = periodTypeOp,
                        VesselPoolId = allocationVesselPoolIdOp,
                        QuantityDays = quantityOp,
                        QuantityType = quantityTypeOp,
                        Premium = premium,
                        VesselId = allocationVesselIdOp,
                        SettlementType = settlementTypeOp,
                        ClearingFees = clearingFees,
                        PremiumDueDate = premiumDueDate,
                        Strike = strike,
                        Type = tradeOptionInfoTypeEnum,
                        UserDelta = userDelta,
                        UserMtm = userMTM,
                        ProfitSharing = profitSharing,
                        Purpose = optionPurpose
                    };

                    objTradeInfo.OptionInfo = objTradeOptionInfo;
                    objTradeInfo.TradeBrokerInfos = tradeBrokerInfos;
                    objTradeInfo.TradeInfoBooks = _tradeInfoBooks;
                    objTrade.Info = objTradeInfo;

                    if (vesselPool != null)
                    {
                        List<Vessel> vessels = vesselPool.CurrentVessels;

                        //Compute quantity days
                        decimal sumOfVesselIndexes = 0;
                        sumOfVesselIndexes = vessels.Aggregate(sumOfVesselIndexes,
                                                               (current, vessel) =>
                                                               current + vessel.CurrentVesselBenchMarking.Benchmark);


                        foreach (var vessel in vessels)
                        {
                            var vesselTrade = objTrade.Clone();
                            vesselTrade.Info.OptionInfo.VesselId = vessel.Id;
                            vesselTrade.Info.OptionInfo.VesselPoolId = null;
                            vesselTrade.Info.OptionInfo.AllocationType = TradeInfoVesselAllocationTypeEnum.Single;
                            vesselTrade.Info.OptionInfo.QuantityDays = (vessel.CurrentVesselBenchMarking.Benchmark * objTradeOptionInfo.QuantityDays) / sumOfVesselIndexes;

                            poolTrades.Add(vesselTrade);
                        }
                        foreach (var vessel in vessels)
                        {
                            var vesselTrade = objTrade.Clone();
                            vesselTrade.Info.Direction = vesselTrade.Info.Direction == TradeInfoDirectionEnum.OutOrSell
                                                             ? TradeInfoDirectionEnum.InOrBuy
                                                             : TradeInfoDirectionEnum.OutOrSell;
                            vesselTrade.Info.OptionInfo.VesselId = vessel.Id;
                            vesselTrade.Info.OptionInfo.VesselPoolId = null;
                            vesselTrade.Info.OptionInfo.AllocationType = TradeInfoVesselAllocationTypeEnum.Single;
                            vesselTrade.Info.OptionInfo.QuantityDays = (vessel.CurrentVesselBenchMarking.Benchmark * objTradeOptionInfo.QuantityDays) / sumOfVesselIndexes;

                            poolTrades.Add(vesselTrade);
                        }
                    }
                    break;
            }

            if (_formActionType == FormActionTypeEnum.Add)
            {
                BeginInsertNewTrade(objTrade, objTradeInfo, objTradeTcInfo, objTradeTCInfoLegs, objTradeFfaInfo,
                                objTradeCargoInfo, objTradeCargoInfoLegs, objTradeOptionInfo, tradeBrokerInfos, poolTrades);
            }
            else if (_formActionType == FormActionTypeEnum.Edit)
            {
                BeginUpdateTrade(objTrade, objTradeInfo, objTradeTcInfo, objTradeTCInfoLegs, objTradeFfaInfo,
                                 objTradeCargoInfo, objTradeCargoInfoLegs, objTradeOptionInfo, tradeBrokerInfos, poolTrades);
            }
        }

        private void btnClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        private void AddEditViewTradeForm_Activated(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Show();
            }
        }

        private void AddEditViewTradeForm_Deactivate(object sender, EventArgs e)
        {
            lock (_syncObject)
            {
                if (_dialogForm != null) _dialogForm.Hide();
            }
        }

        private void treeBook_NodeChanged(object sender, DevExpress.XtraTreeList.NodeChangedEventArgs e)
        {
            if (e.ChangeType == NodeChangeTypeEnum.CheckedState)
            {
                var bookClient = (BookClient)treeBook.GetDataRecordByNode(e.Node);
                if (e.Node.Checked)
                {
                    _tradeInfoBooks.Add(new TradeInfoBook() { BookId = bookClient.Book.Id });
                }
                else
                {
                    if (_tradeInfoBooks.Select(a => a.BookId).Contains(bookClient.Book.Id))
                    {
                        TradeInfoBook bookToRemove = _tradeInfoBooks.Where(a => a.BookId == bookClient.Book.Id).Single();
                        _tradeInfoBooks.Remove(bookToRemove);
                    }
                }
            }
        }

        private void treeBook_AfterCheckNode(object sender, NodeEventArgs e)
        {
            PropagateCheckState(e.Node);
        }

        private void lookUpVersions_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind != ButtonPredefines.Glyph || lookUpVersions.EditValue == null) return;

            if (OnViewTrade != null)
                OnViewTrade((long)lookUpVersions.GetColumnValue("TradeId"), (long)lookUpVersions.EditValue,
                            lookUpVersions.GetColumnValue("ExternalCode").ToString() + " [" + lookUpVersions.Text + "]",
                            _tradeType);
        }

        private void treeBook_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            if (_formActionType == FormActionTypeEnum.View)
                e.CanCheck = false;
        }

        #endregion

        #region Public Methods

        public void SetIndexesAccordingToUnderlyingIndex(long? indexId)
        {
            if (indexId == null)
            {
                lookupMTMForwardCurve.EditValue = null;
                lookupMTMForwardCurve.Properties.DataSource = null;

                lookupMTMStressCurve.EditValue = null;
                lookupMTMStressCurve.Properties.DataSource = null;

                lookupMTMForwardCurve.Properties.ReadOnly = false;
                lookupMTMStressCurve.Properties.ReadOnly = false;
            }
            else
            {
                lookupMTMForwardCurve.EditValue =
                    ((List<Index>)lookupMTMForwardCurve.Tag).Where(
                        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.Id == indexId).Any()
                        ? ((List<Index>)lookupMTMForwardCurve.Tag).Where(
                            a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.Id == indexId).Single
                              ().
                              Id
                        : (long?)null;

                lookupMTMStressCurve.EditValue =
                    ((List<Index>)lookupMTMStressCurve.Tag).Where(
                        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.Id == indexId).Any()
                        ? ((List<Index>)lookupMTMStressCurve.Tag).Where(
                            a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue) && a.Id == indexId).Single
                              ().Id
                        : (long?)null;

                lookupMTMForwardCurve.Properties.DataSource =
                ((List<Index>)lookupMTMForwardCurve.Tag).Where(
                    a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue)).ToList();

                lookupMTMStressCurve.Properties.DataSource =
                    ((List<Index>)lookupMTMStressCurve.Tag).Where(
                        a => a.MarketId == Convert.ToInt64(lookupMarketSegment.EditValue)).ToList();

                lookupMTMForwardCurve.Properties.ReadOnly = true;
                lookupMTMStressCurve.Properties.ReadOnly = true;
            }
        }

        public void ViewTCTrade(long tradeId, long tradeInfoId, string externalCode, TradeTypeEnum tradeType)
        {
            if (OnViewTrade != null)
                OnViewTrade(tradeId, tradeInfoId,
                            externalCode,
                            tradeType);
        }

        #endregion

        #region Public Properties

        public TradeTypeEnum TradeType
        {
            get { return _tradeType; }
        }

        public DialogResult Result { get; set; }

        public Trade Trade
        {
            get { return _trade; }
        }

        public FormActionTypeEnum FormActionType { get { return _formActionType; } }


        #endregion

        #region Events

        public event Action<long, long, string, TradeTypeEnum> OnViewTrade;
        public event Action<object, bool> OnDataSaved;

        #endregion


    }
}