﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.About;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraRichEdit.Utils;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Exis.Domain;
using Exis.WinClient.General;
using Itenso.TimePeriod;
using SummaryItemType = DevExpress.Data.SummaryItemType;
using WaitDialogForm = Exis.WinClient.SpecialForms.WaitDialogForm;
using DevExpress.XtraEditors.Controls;
using DTG.Spreadsheet;
using System.Text;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraLayout;
using System.Xml;
using System.Diagnostics;

namespace Exis.WinClient.Controls.Core
{
    public partial class GeneratePOSForm : DevExpress.XtraEditors.XtraForm
    {
        #region Nested type: FormStateEnum

        private enum FormStateEnum
        {
            BeforeExecution,
            WhileExecuting,
            AfterExecution
        }

        #endregion

        #region Private Properties

        private const string licenceCode = "SOZT-CSWE-M2SN-CXMT";

        private AppParameter _appParameter;
        private readonly object _syncObject = new object();
        private WaitDialogForm _dialogForm1;
        private WaitDialogForm _dialogForm2;

        private TreeListNode _expandedNode;
        private FormStateEnum _formState = FormStateEnum.BeforeExecution;
        private bool isLoad;

        private Dictionary<string, Dictionary<DateTime, decimal>> _tradePositionResults =
            new Dictionary<string, Dictionary<DateTime, decimal>>();

        private Dictionary<string, Dictionary<DateTime, decimal>> _indexPositionResults =
            new Dictionary<string, Dictionary<DateTime, decimal>>();

        private Dictionary<string, Dictionary<DateTime, decimal>> _tradeMTMResults =
            new Dictionary<string, Dictionary<DateTime, decimal>>();

        private Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>> _tradeMTMRatiosResults =
            new Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>();

        private Dictionary<string, decimal> _tradeEmbeddedValues = new Dictionary<string, decimal>();

        private Dictionary<DateTime, Dictionary<string, decimal>> _indexesValues =
            new Dictionary<DateTime, Dictionary<string, decimal>>();

        private bool RatioReportPL;
        private bool RatioReportCashFlow;
        private bool InSelection;


        //params for query 
        private DateTime _positionDate;
        private DateTime _periodFrom;
        private DateTime _periodTo;
        private List<string> _tradeIdentifiers;
        private List<TradeInformation> _tradeInformations;
        private List<Book> _books;
        private HierarchyNodeInfo _selectedHierarchyNode;

        //params for info
        private ViewTradeGeneralInfo _viewTradesGeneralInfo;

        private Dictionary<string, string> _spanCodes = new Dictionary<string, string>
        {
            {"P", "PTOO"},
            {"S", "STOO"},
            {"C", "CTOO"},
            {"H", "HTOO"}
        };

        private Dictionary<string, string> _initMarginCodes = new Dictionary<string, string>
        {
            {"P", "PTC"},
            {"S", "STC"},
            {"C", "CTC"},
            {"H", "HTC"}
        };
        private List<TmpScanningRange> _tmpScanningRates;
        private List<TmpIntercommodityCredits> _tmpIntercommCredits;
        private List<TmpEnclear> _tmpEnclears;

        private List<string> _availableCodes;
        private List<DateTime> _dates;
        private Dictionary<string, Dictionary<DateTime, decimal>> _indexPosValues;
        private List<OptionPositionValues> _optPosValues;
        private List<DeltaValues> _delta;
        private List<RiskArrayParameter> _rAparam;
        private List<Contract> _contracts;

        private Dictionary<string, decimal[]> _scanningScenario;
        private Dictionary<string, decimal> _intermonthSpreadCharge;
        private Dictionary<string, decimal> _spotCharge;
        private Dictionary<string, decimal> _interCommoditySpreadCredit;

        private List<InitialMargin> _initialMargins;

        private bool imTabLoaded;
        private bool _optionParametersLoaded;


        private InitialMarginControl _initialMarginControl;
        private InitialMarginNasdaqControl _initialMarginNasdaqControl;

        #endregion

        #region Constructors

        public GeneratePOSForm(ViewTradeGeneralInfo viewTradesGeneralInfo)
        {
            _viewTradesGeneralInfo = viewTradesGeneralInfo;

            InitializeComponent();
            SetGuiControls();
            InitializeControls();
            InitializeInitialMargin();
        }

        #endregion

        #region Private Methods

        private void InitializeControls()
        {
            if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject == null)
                lblEntity.Text = _viewTradesGeneralInfo.selectedHierarchyNode.StrObject;
            else
            {
                if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Company))
                    lblEntity.Text = "Company" + "-" +
                                     ((Company)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Book))
                    lblEntity.Text = "Book" + "-" +
                                     ((Book)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Market))
                    lblEntity.Text = "Market" + "-" +
                                     ((Market)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Trader))
                    lblEntity.Text = "Trader" + "-" +
                                     ((Trader)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
                else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Vessel))
                    lblEntity.Text = "Vessel" + "-" +
                                     ((Vessel)_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject).Name;
            }

            lblPeriodFrom.Text = _viewTradesGeneralInfo.periodFrom.ToShortDateString();
            lblPeriodTo.Text = _viewTradesGeneralInfo.periodTo.ToShortDateString();
            lblIsDraft.Text = _viewTradesGeneralInfo.IsDraft.ToString();
            lblIsMinimumPeriod.Text = _viewTradesGeneralInfo.IsMinimumPeriod.ToString();
            lblIsOptionalPeriod.Text = _viewTradesGeneralInfo.IsOptionalPeriod.ToString();
            lblOnlyNonZeroTotalDays.Text = _viewTradesGeneralInfo.OnlyNonZeroTotalDays.ToString();


            int gridViewColumnindex = 0;
            GridColumn column = AddColumn("Type", Strings.Type, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null);
            column.SummaryItem.SummaryType = SummaryItemType.Count;
            column.SummaryItem.DisplayFormat = "Count: {0:N0}";

            gridTradeActionResults.BeginUpdate();
            gridTradeActionResultsView.Columns.Clear();
            gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;

            gridTradeActionResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                UnboundColumnType.Bound, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Books", Strings.Books, gridViewColumnindex++,
                UnboundColumnType.String, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code,
                gridViewColumnindex++,
                UnboundColumnType.Bound, true, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve,
                gridViewColumnindex++,
                UnboundColumnType.Bound, false, null));
            gridTradeActionResultsView.Columns.Add(AddColumn("PhysicalFinancial", "Physical/Financial",
                gridViewColumnindex++,
                UnboundColumnType.String, false, null));

            try
            {
                gridTradeActionResultsView.RestoreLayoutFromXml(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                    @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }

            gridTradeActionResults.EndUpdate();


            var col = new LookUpColumnInfo
            {
                Caption = Strings.Model_name,
                FieldName = "Name",
                Width = 0
            };

            //dtpFilterCurveDate.Properties.MaxValue = DateTime.Today.AddDays(-1);
            //dtpFilterCurveDate.DateTime = DateTime.Today.AddDays(-1);
            dtpFilterCurveDate.DateTime = _viewTradesGeneralInfo.lastImportDate;
            dtpFilterCurveDate.Properties.MaxValue = _viewTradesGeneralInfo.lastImportDate;

            cmbPositionMethod.SelectedItem = "Dynamic";
            cmbMarketSensitivityType.SelectedIndex = 0;

            rdgResultsAggregationType.SelectedIndex = 0;
            rdgPositionResultsType.SelectedIndex = 0;

        }

        private void InitializeInitialMargin()
        {
            //TODO: Here choose type of intial margin calculation
            _initialMarginControl = new InitialMarginControl(_viewTradesGeneralInfo) { Parent = this };
            lcInitialMargin.AddItem(new LayoutControlItem { Name = "lciInitialMargin", Control = _initialMarginControl, TextVisible = false });
            _initialMarginNasdaqControl = new InitialMarginNasdaqControl(_viewTradesGeneralInfo) { Parent = this };
            lcIMNasdaq.AddItem(new LayoutControlItem { Name = "lciIMNasdaq", Control = _initialMarginNasdaqControl, TextVisible = false });
        }

        private void SetGuiControls()
        {
            if (_formState == FormStateEnum.BeforeExecution)
            {
                btnGeneratePosition.Enabled = true;
                gridTradeActionResults.DataSource = null;
                SetEnabledForWorkingStatus(true);
                btnExportResults.Enabled = false;
                btnExportToCSV.Enabled = false;
                chkReverseNumberSign.Enabled = false;
            }
            else if (_formState == FormStateEnum.WhileExecuting)
            {

                btnGeneratePosition.Enabled = false;

                SetEnabledForWorkingStatus(true);
                btnExportResults.Enabled = false;
                btnExportToCSV.Enabled = false;
                chkReverseNumberSign.Enabled = false;
            }
            else if (_formState == FormStateEnum.AfterExecution)
            {
                btnGeneratePosition.Enabled = true;
                SetEnabledForWorkingStatus(true);
                if (gridTradeActionResultsView.DataRowCount > 0)
                {
                    btnExportResults.Enabled = true;
                    btnExportToCSV.Enabled =  (rdgResultsAggregationType.SelectedIndex == 0 && rdgPositionResultsType.SelectedIndex == 0
                        && _viewTradesGeneralInfo.selectedHierarchyNode.DomainObject != null
                        && (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Company) ||
                            _viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Book)));
                    chkReverseNumberSign.Enabled = btnExportToCSV.Enabled;
                }

            }

            //btnExportInitialMargin.Enabled = false;
        }

        private GridColumn AddColumn(string name, string caption, int columnIndex, UnboundColumnType columnType,
            bool isFixedLeft, object Tag)
        {
            var column = new GridColumn();
            column.Visible = true;
            column.AppearanceCell.Options.UseTextOptions = true;
            column.OptionsColumn.FixedWidth = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsColumn.AllowEdit = false;
            column.FieldName = name;
            column.Caption = caption;
            column.Tag = Tag;
            column.VisibleIndex = columnIndex;
            column.Fixed = isFixedLeft ? FixedStyle.Left : FixedStyle.None;
            if (columnType != UnboundColumnType.Bound)
                column.UnboundType = columnType;

            switch (columnType)
            {
                case UnboundColumnType.String:
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.DateTime:
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.DisplayFormat.FormatType = FormatType.DateTime;
                    column.DisplayFormat.FormatString = "G";
                    break;
                case UnboundColumnType.Decimal:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.DisplayFormat.Format = SessionRegistry.ClientCultureInfo;
                    column.UnboundType = UnboundColumnType.Decimal;
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "N3";
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Integer:
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                    column.UnboundType = UnboundColumnType.Integer;
                    column.OptionsColumn.AllowEdit = false;
                    break;
                case UnboundColumnType.Boolean:
                    column.UnboundType = UnboundColumnType.Boolean;
                    column.OptionsColumn.AllowEdit = false;
                    break;
            }

            return column;
        }

        private void CreateResultGridColumns(Dictionary<string, Dictionary<DateTime, decimal>> tradeResults,
            Dictionary<string, decimal> tradeEmbeddedValues, string type)
        {
            var groupedColumns = gridTradeActionResultsView.GroupedColumns;

            gridTradeActionResults.BeginUpdate();
            gridTradeActionResults.DataSource = null;
            gridTradeActionResultsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
            gridTradeActionResultsView.Columns.Clear();
            gridTradeActionResultsView.OptionsView.ColumnAutoWidth = false;
            int gridViewColumnindex = 0;

            if (type == "MTM" || type == "POSITION")
            {
                gridTradeActionResultsView.Columns.Add(AddColumn("Type", Strings.Type, gridViewColumnindex++,
                    UnboundColumnType.Bound, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Books", Strings.Books, gridViewColumnindex++,
                    UnboundColumnType.String, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("ExternalCode", Strings.External_Code,
                    gridViewColumnindex++,
                    UnboundColumnType.Bound, true, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("LegNumber", Strings.Leg_Number, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Company", Strings.Company, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Counterparty", Strings.Counterparty,
                    gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Status", Strings.Status, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("SignDate", Strings.Sign_Date, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Direction", Strings.Direction, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Price", Strings.Price, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("StrikePrice", Strings.Strike_Price,
                    gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("PeriodFrom", Strings.Period_From,
                    gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("PeriodTo", Strings.Period_To, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Vessel", Strings.Vessel, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("Brokers", Strings.Brokers, gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                gridTradeActionResultsView.Columns.Add(AddColumn("ForwardCurve", Strings.Forward_Curve,
                    gridViewColumnindex++,
                    UnboundColumnType.Bound, false, null));
                GridColumn lastColumn = AddColumn("PhysicalFinancial", "Physical/Financial", gridViewColumnindex++,
                    UnboundColumnType.String, false, null);
                gridTradeActionResultsView.Columns.Add(lastColumn);

                if (type == "MTM")
                {
                    GridColumn column = AddColumn(
                        "EmbeddedValue",
                        "Embedded Value", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, tradeEmbeddedValues);
                    gridTradeActionResultsView.Columns.Add(column);

                    if (chkCalculateSums.Checked)
                    {
                        //Created only to make a void summary item, so that no custom summary is diplayed in that cell
                        var dummySummarytem = new GridColumnSummaryItem();
                        dummySummarytem.DisplayFormat = "Totals:";
                        dummySummarytem.FieldName = "dummie";
                        dummySummarytem.SummaryType = SummaryItemType.Custom;
                        column.Summary.Add(dummySummarytem);
                    }
                    lastColumn = column;
                }

                //List<DateTime> _dates = tradeResults.First().Value.Select(a => a.Key).ToList();

                #region Calculation Month - Quarter - Calendar

                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    foreach (DateTime date in _dates)
                    {
                        GridColumn column = AddColumn(
                            date.ToString("MMM-yyyy", new CultureInfo("en-GB")),
                            date.ToString("MMM-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                            UnboundColumnType.Decimal, false,
                            Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                        gridTradeActionResultsView.Columns.Add(column);
                        if (chkCalculateSums.Checked)
                        {
                            var item = new GridGroupSummaryItem();
                            item.FieldName = column.FieldName;
                            item.ShowInGroupColumnFooter = column;
                            item.SummaryType = SummaryItemType.Sum;
                            item.DisplayFormat = "{0:N3}";
                            gridTradeActionResultsView.GroupSummary.Add(item);

                            column.SummaryItem.SummaryType = SummaryItemType.Sum;
                            column.SummaryItem.DisplayFormat = "{0:N3}";
                        }
                        if (type == "MTM")
                        {
                            DateTime dt = (DateTime)column.Tag;
                            foreach (var pair in _indexesValues[dt])
                            {
                                if (string.IsNullOrEmpty(pair.Key)) continue;

                                var item = new GridColumnSummaryItem();
                                item.FieldName = pair.Key;
                                item.SummaryType = SummaryItemType.Custom;
                                item.DisplayFormat = "{0:N3}";
                                item.Tag = dt.ToString("MMM-yyyy", new CultureInfo("en-GB"));
                                column.Summary.Add(item);
                            }
                        }
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                {
                    int quarterAdded = 0;
                    foreach (DateTime date in _dates)
                    {
                        int intQuarter = ((date.Month - 1) / 3) + 1;
                        if (intQuarter != quarterAdded)
                        {
                            quarterAdded = intQuarter;
                            string strQuarter = intQuarter == 1
                                ? "Q1"
                                : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";

                            GridColumn column = AddColumn(
                                strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")),
                                strQuarter + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                                UnboundColumnType.Decimal, false,
                                Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                            gridTradeActionResultsView.Columns.Add(column);
                            if (chkCalculateSums.Checked)
                            {
                                var item = new GridGroupSummaryItem();
                                item.FieldName = column.FieldName;
                                item.ShowInGroupColumnFooter = column;
                                item.SummaryType = SummaryItemType.Sum;
                                item.DisplayFormat = "{0:N3}";
                                gridTradeActionResultsView.GroupSummary.Add(item);

                                column.SummaryItem.SummaryType = SummaryItemType.Sum;
                                column.SummaryItem.DisplayFormat = "{0:N3}";
                            }
                            if (type == "MTM")
                            {
                                var dt = (DateTime)column.Tag;
                                foreach (var pair in _indexesValues[dt])
                                {
                                    if (string.IsNullOrEmpty(pair.Key)) continue;

                                    var item = new GridColumnSummaryItem();
                                    item.FieldName = pair.Key;
                                    item.SummaryType = SummaryItemType.Custom;
                                    item.DisplayFormat = "{0:N3}";
                                    item.Tag = column.FieldName;
                                    column.Summary.Add(item);
                                }
                            }
                        }
                    }
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                {
                    int calendarAdded = 0;
                    foreach (DateTime date in _dates)
                    {
                        int intCalendar = date.Year;
                        if (intCalendar != calendarAdded)
                        {
                            calendarAdded = intCalendar;

                            GridColumn column = AddColumn(
                                "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")),
                                "CAL" + date.ToString("-yyyy", new CultureInfo("en-GB")), gridViewColumnindex++,
                                UnboundColumnType.Decimal, false,
                                Convert.ToDateTime(date.ToString("MMM-yyyy", new CultureInfo("en-GB"))));
                            if (chkCalculateSums.Checked)
                            {
                                var item = new GridGroupSummaryItem();
                                item.FieldName = column.FieldName;
                                item.ShowInGroupColumnFooter = column;
                                item.SummaryType = SummaryItemType.Sum;
                                item.DisplayFormat = "{0:N3}";
                                gridTradeActionResultsView.GroupSummary.Add(item);

                                column.SummaryItem.SummaryType = SummaryItemType.Sum;
                                column.SummaryItem.DisplayFormat = "{0:N3}";
                            }
                            gridTradeActionResultsView.Columns.Add(column);
                            if (type == "MTM")
                            {
                                var dt = (DateTime)column.Tag;
                                foreach (var pair in _indexesValues[dt])
                                {
                                    if (string.IsNullOrEmpty(pair.Key)) continue;

                                    var item = new GridColumnSummaryItem();
                                    item.FieldName = pair.Key;
                                    item.SummaryType = SummaryItemType.Custom;
                                    item.DisplayFormat = "{0:N3}";
                                    item.Tag = column.FieldName;
                                    column.Summary.Add(item);
                                }
                            }
                        }
                    }
                }

                #endregion

                GridColumn columnCommission = null;
                if (type == "MTM" && chkCalculateCommissions.Checked)
                {
                    columnCommission = AddColumn("Commission", "Commission", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, "Commission");
                    gridTradeActionResultsView.Columns.Add(columnCommission);
                }


                if (chkCalculateSums.Checked)
                {
                    GridColumn columnTotal = AddColumn(
                        "Total",
                        "Total", gridViewColumnindex++,
                        UnboundColumnType.Decimal, false, "Total");
                    gridTradeActionResultsView.Columns.Add(columnTotal);

                    var item = new GridGroupSummaryItem();
                    item.FieldName = columnTotal.FieldName;
                    item.ShowInGroupColumnFooter = columnTotal;
                    item.SummaryType = SummaryItemType.Sum;
                    item.DisplayFormat = "{0:N3}";
                    gridTradeActionResultsView.GroupSummary.Add(item);
                    columnTotal.SummaryItem.SummaryType = SummaryItemType.Sum;
                    columnTotal.SummaryItem.DisplayFormat = "{0:N3}";

                    if (type == "MTM" && columnCommission != null)
                    {
                        var commissionSumItem = new GridGroupSummaryItem();
                        commissionSumItem.FieldName = columnCommission.FieldName;
                        commissionSumItem.ShowInGroupColumnFooter = columnCommission;
                        commissionSumItem.SummaryType = SummaryItemType.Sum;
                        commissionSumItem.DisplayFormat = "{0:N3}";
                        gridTradeActionResultsView.GroupSummary.Add(commissionSumItem);
                        columnCommission.SummaryItem.SummaryType = SummaryItemType.Sum;
                        columnCommission.SummaryItem.DisplayFormat = "{0:N3}";
                    }
                }

                if (type == "MTM")
                {
                    var index = _indexesValues.First(a => a.Value.Count > 0).Value;
                    foreach (var pair in index)
                    {
                        var item = new GridColumnSummaryItem();
                        item.FieldName = pair.Key;
                        item.SummaryType = SummaryItemType.Custom;
                        item.DisplayFormat = "{0:N3}";
                        item.Tag = "Index";
                        lastColumn.Summary.Add(item);
                    }
                }
                gridTradeActionResults.Tag = type;
                gridTradeActionResultsView.Tag = tradeResults;

                gridTradeActionResults.DataSource = _viewTradesGeneralInfo.tradeInformations;

            }

            if (groupedColumns.Count > 0)
            {
                foreach (GridColumn groupedColumn in groupedColumns)
                {
                    if (gridTradeActionResultsView.Columns.ColumnByFieldName(groupedColumn.FieldName) != null)
                        gridTradeActionResultsView.Columns[groupedColumn.FieldName].Group();
                }
            }

            gridTradeActionResults.EndUpdate();

            _formState = FormStateEnum.AfterExecution;
            SetGuiControls();
        }

        private void ProcessCashFlowGroups(long? parentId, CashFlowInformation parentGroupInfo,
            Dictionary<long, Dictionary<DateTime, decimal>> cashFlowItemResults, List<CashFlowItem> cashFlowItems,
            List<CashFlowGroupItem> cashFlowItemGroups, List<CashFlowGroup> cashFlowGroups,
            List<CashFlowModelGroup> cashFlowModelGroups, ref Dictionary<int, CashFlowInformation> cashFlowInfos,
            ref int index, int level)
        {
            string strLevel = "";
            for (int i = 1; i <= level; i++)
            {
                strLevel = strLevel + "--";
            }
            List<CashFlowModelGroup> modelGroups =
                cashFlowModelGroups.Where(a => a.ParentGroupId == parentId).OrderBy(a => a.Order).ToList();
            if (parentGroupInfo != null) parentGroupInfo.ChildrenGroupInfo = new List<CashFlowInformation>();
            foreach (CashFlowModelGroup modelGroup in modelGroups)
            {
                List<CashFlowItem> items = (from objCashFlowItem in cashFlowItems
                                            from objCashFlowItemGroup in cashFlowItemGroups
                                            where objCashFlowItem.Id == objCashFlowItemGroup.ItemId
                                                  && objCashFlowItemGroup.GroupId == modelGroup.GroupId
                                            orderby objCashFlowItemGroup.Order
                                            select objCashFlowItem).ToList();

                CashFlowInformation cashFlowInfo = new CashFlowInformation
                {
                    Name = strLevel + cashFlowGroups.Single(a => a.Id == modelGroup.GroupId).Name,
                    Type = CashFlowInformationTypeEnum.GroupHeader,
                    Results =
                        cashFlowItemResults.Where(a => items.Select(b => b.Id).Contains(a.Key))
                            .Select(a => a.Value)
                            .ToList(),
                    GroupOperator = modelGroup.Operator
                };
                cashFlowInfos.Add(index++, cashFlowInfo);
                if (parentGroupInfo != null) parentGroupInfo.ChildrenGroupInfo.Add(cashFlowInfo);
                foreach (CashFlowItem item in items)
                {
                    cashFlowInfos.Add(index++,
                        new CashFlowInformation
                        {
                            Id = item.Id,
                            Name = strLevel + item.Name,
                            Type = CashFlowInformationTypeEnum.Item,
                            Results = cashFlowItemResults.Where(a => a.Key == item.Id).Select(a => a.Value).ToList()
                        });
                }
                ProcessCashFlowGroups(modelGroup.GroupId, cashFlowInfo, cashFlowItemResults, cashFlowItems,
                    cashFlowItemGroups, cashFlowGroups, cashFlowModelGroups, ref cashFlowInfos, ref index, level + 1);
                CashFlowInformation cashFlowInfo2 = new CashFlowInformation
                {
                    Id = modelGroup.GroupId,
                    Name = strLevel + cashFlowGroups.Single(a => a.Id == modelGroup.GroupId).Name,
                    Type = CashFlowInformationTypeEnum.GroupFooter,
                    GroupItems = items,
                    Results =
                        cashFlowItemResults.Where(a => items.Select(b => b.Id).Contains(a.Key))
                            .Select(a => a.Value)
                            .ToList(),
                    GroupHeaderInfo = cashFlowInfo,
                    GroupOperator = modelGroup.Operator
                };
                cashFlowInfos.Add(index++, cashFlowInfo2);
                // if (parentGroupInfo != null) parentGroupInfo.ChildrenGroupInfo.Add(cashFlowInfo2);
            }
        }

        private void SetEnabledForWorkingStatus(bool status)
        {

        }

        #endregion

        #region GUI Events

        private void MainControl_Load(object sender, EventArgs e)
        {
            isLoad = true;

        }

        private void ViewTradesForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            try
            {
                gridTradeActionResultsView.SaveLayoutToXml(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                    @"\Exis.FreightMetrics.ViewTrades.Results.Layout.xml");
            }
            catch (Exception)
            {
            }
        }

        private void cmbPositionMethod_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedPositionMethod = (string)cmbPositionMethod.SelectedItem;
            //if (selectedPositionMethod == "Static")
            //{
            //    chkOptionNullify.Enabled = true;
            //}
            //else
            //{
            //    chkOptionNullify.Enabled = false;
            //    chkOptionNullify.EditValue = false;
            //}
        }

        private void gridTradeActionResultsView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                if (e.Row is TradeInformation)
                {
                    bool isMTMAndcommissionCalculation = chkCalculateCommissions.Checked &&
                                                         (gridTradeActionResults.Tag != null &&
                                                          gridTradeActionResults.Tag.ToString() == "MTM");

                    var tradeInformation = (TradeInformation)e.Row;

                    decimal commissionPerMonth = 0;
                    decimal totalCommission = 0;

                    var numOfTradeMotnhsInSelectedPeriod = 0;
                    var firstDay = _periodFrom;
                    var lastDay = _periodTo;

                    DateTime tradeDateInSelectedPeriod = tradeInformation.PeriodFrom.Date;
                    while (tradeDateInSelectedPeriod < firstDay)
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddDays(1);

                    while (tradeDateInSelectedPeriod >= firstDay &&
                           tradeDateInSelectedPeriod <= tradeInformation.PeriodTo.Date)
                    {
                        numOfTradeMotnhsInSelectedPeriod++;
                        tradeDateInSelectedPeriod = tradeDateInSelectedPeriod.AddMonths(1);
                    }

                    if (isMTMAndcommissionCalculation && tradeInformation.TradeInfo.TradeBrokerInfos.Count > 0)
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.Option)
                        {
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                  tradeInformation.TradeInfo.OptionInfo.Premium *
                                                  tradeInformation.TradeInfo.OptionInfo.QuantityDays);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                        else if (tradeInformation.Trade.Type == TradeTypeEnum.FFA)
                        {
                            //JIRA:FREIG-11
                            //commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                            //                      tradeInformation.TradeInfo.FFAInfo.Price *
                            //                      tradeInformation.TradeInfo.FFAInfo.QuantityDays);
                            commissionPerMonth = (tradeInformation.TradeInfo.TradeBrokerInfos[0].Commission / 100 *
                                                  tradeInformation.TradeInfo.FFAInfo.Price *
                                                  tradeInformation.TradeInfo.FFAInfo.TotalDaysOfTrade);
                            totalCommission = commissionPerMonth * numOfTradeMotnhsInSelectedPeriod;
                        }
                    }

                    if (e.Column.FieldName == "Type")
                    {
                        if (tradeInformation.Type == "Option")
                            e.Value = tradeInformation.TradeInfo.OptionInfo.Type.ToString("g");
                    }
                    else if (e.Column.FieldName == "Books")
                    {
                        string books = tradeInformation.TradeInfo.TradeInfoBooks.Aggregate("",
                            (current, tradeInfoBook) =>
                                current + _viewTradesGeneralInfo.books.Single(a => a.Id == tradeInfoBook.BookId).Name +
                                ", ");
                        e.Value = books.Substring(0, books.LastIndexOf(','));
                    }
                    else if (e.Column.FieldName == "EmbeddedValue")
                    {
                        var embeddedValues = (Dictionary<string, decimal>)e.Column.Tag;
                        e.Value = (embeddedValues == null || embeddedValues.Count == 0 ||
                                   !embeddedValues.ContainsKey(tradeInformation.Identifier))
                            ? 0
                            : embeddedValues[tradeInformation.Identifier];
                    }
                    else if (e.Column.FieldName == "Total")
                    {
                        var tradePositions =
                            (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                        if (isMTMAndcommissionCalculation)
                        {
                            if (rdgPositionResultsType.SelectedIndex == 0) // Days
                            {
                                e.Value = (tradePositions[tradeInformation.Identifier].Sum(a => a.Value) -
                                          totalCommission);
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                            a.Value /
                                            (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                              new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum() -
                                    totalCommission;
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                            (a.Value /
                                             (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                               new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                            tradeInformation.MarketTonnes).Sum() - totalCommission;
                            }
                        }
                        else
                        {
                            if (rdgPositionResultsType.SelectedIndex == 0) // Days
                            {
                                e.Value = tradePositions[tradeInformation.Identifier].Sum(a => a.Value);
                                e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                            a.Value /
                                            (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                              new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();
                                e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                            }
                            else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                            {
                                e.Value =
                                    tradePositions[tradeInformation.Identifier].Select(
                                        a =>
                                            (a.Value /
                                             (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                               new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                            tradeInformation.MarketTonnes).Sum();
                                e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                            }
                        }
                    }
                    else if (e.Column.FieldName == "Commission" && isMTMAndcommissionCalculation)
                    {
                        if (tradeInformation.Trade.Type == TradeTypeEnum.TC ||
                            tradeInformation.Trade.Type == TradeTypeEnum.Cargo
                            || tradeInformation.TradeInfo.TradeBrokerInfos == null ||
                            tradeInformation.TradeInfo.TradeBrokerInfos.Count == 0)
                            return;

                        if (tradeInformation.TradeInfo.TradeBrokerInfos.Count > 1)
                        {
                            e.Value = "More than 1 brokers";
                            return;
                        }
                        e.Value = totalCommission;
                    }
                    else
                    {
                        #region POSITION Calculation

                        if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
                        {
                            var tradePositions =
                                (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = tradePositions[tradeInformation.Identifier][date];// *Convert.ToDecimal(tradeInformation.Ownership);
                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => a.Value).Single();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = tradePositions[tradeInformation.Identifier][date] /
                                              (((new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)) -
                                                new DateTime(date.Year, date.Month, 1)).Days + 1);
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => a.Value/(((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) - new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Single();
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    DateTime date = (DateTime)e.Column.Tag;
                                    e.Value = (tradePositions[tradeInformation.Identifier][date] /
                                               (((new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)) -
                                                 new DateTime(date.Year, date.Month, 1)).Days + 1)) *
                                              tradeInformation.MarketTonnes;
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                    //e.Value = tradePositions.Single(a => a.Key == tradeInformation.Identifier).Value.Where(
                                    //    a => a.Key.ToString("MMM-yyyy", new CultureInfo("en-GB")) == e.Column.FieldName).Select(
                                    //        a => (a.Value / (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) - new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) * tradeInformation.MarketTonnes).Single();
                                }
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                ? "Q1"
                                                : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                            a => a.Value).Sum();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                ? "Q1"
                                                : intQuarter == 2
                                                    ? "Q2"
                                                    : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                            a =>
                                                a.Value /
                                                (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                                  new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                        {
                                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                            string strQuarter = intQuarter == 1
                                                ? "Q1"
                                                : intQuarter == 2
                                                    ? "Q2"
                                                    : intQuarter == 3 ? "Q3" : "Q4";
                                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                                   e.Column.FieldName;
                                        }).Select(
                                            a =>
                                                (a.Value /
                                                 (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(-1)) -
                                                   new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                                tradeInformation.MarketTonnes).Sum();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                }
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                if (rdgPositionResultsType.SelectedIndex == 0) // Days
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                            "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                            e.Column.FieldName).Select(
                                                a => a.Value).Sum();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;

                                }
                                else if (rdgPositionResultsType.SelectedIndex == 1) // Vessels
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                            "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                            e.Column.FieldName)
                                        .Select(
                                            a =>
                                                a.Value /
                                                (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(
                                                    -1)) -
                                                  new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)).Sum();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                }
                                else if (rdgPositionResultsType.SelectedIndex == 2) // Tonnes
                                {
                                    e.Value = tradePositions[tradeInformation.Identifier].Where(
                                        a =>
                                            "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                            e.Column.FieldName)
                                        .Select(
                                            a =>
                                                (a.Value /
                                                 (((new DateTime(a.Key.Year, a.Key.Month, 1).AddMonths(1).AddDays(
                                                     -1)) -
                                                   new DateTime(a.Key.Year, a.Key.Month, 1)).Days + 1)) *
                                                tradeInformation.MarketTonnes).Sum();
                                    e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                                }
                            }
                        }
                        #endregion

                        else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM" &&
                                 e.Column.Tag != "Commission")
                        {
                            var tradeMTMs =
                                (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;
                            decimal comm = 0;
                            if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                            {
                                var currentDateColumn = (DateTime)e.Column.Tag;

                                var firstDayInCurrMonth = new DateTime(currentDateColumn.Year,
                                    currentDateColumn.Month, 1);
                                var lastDayInCurrMonth = firstDayInCurrMonth.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom,
                                    tradeInformation.PeriodTo);
                                ITimePeriod monthPeriod = new TimeRange(firstDayInCurrMonth, lastDayInCurrMonth);
                                bool intersects = tradePeriod.IntersectsWith(monthPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    comm = commissionPerMonth;
                                }
                                e.Value = tradeMTMs[tradeInformation.Identifier][(DateTime)e.Column.Tag] - comm;
                                e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 1) // Quarter
                            {
                                var currentDateColumn = (DateTime)e.Column.Tag;
                                var currentQuarter = new Quarter(currentDateColumn);

                                var firstDayInQuarter = new DateTime(currentQuarter.FirstMonthStart.Year,
                                    currentQuarter.FirstMonthStart.Month, 1);
                                var lastDayInQuarter = currentQuarter.LastMonthStart.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom,
                                    tradeInformation.PeriodTo);
                                ITimePeriod quarterPeriod = new TimeRange(firstDayInQuarter, lastDayInQuarter);
                                bool intersects = tradePeriod.IntersectsWith(quarterPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    var tradeRange = new TimeRange(tradePeriod);
                                    var calendarRange = new TimeRange(quarterPeriod);
                                    var selectedDatesRange = new TimeRange(_periodFrom, _periodTo);

                                    var s =
                                        selectedDatesRange.GetIntersection(calendarRange)
                                            .GetIntersection(tradeRange);
                                    DateDiff a = new DateDiff(s.Start, s.End);
                                    var numOfTradeMonthsInCurrQuarter = a.ElapsedDays > 0 ? a.Months + 1 : a.Months;
                                    comm = commissionPerMonth * numOfTradeMonthsInCurrQuarter;
                                }

                                e.Value = tradeMTMs[tradeInformation.Identifier].Where(
                                    a =>
                                    {
                                        int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                        string strQuarter = intQuarter == 1
                                            ? "Q1"
                                            : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                                        return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                               e.Column.FieldName;
                                    }).Select(
                                        a => a.Value).Sum() - comm;
                                e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                            }
                            else if (rdgResultsAggregationType.SelectedIndex == 2) // Calendar
                            {
                                var currentDate = (DateTime)e.Column.Tag;
                                var currentYear = new Year(currentDate);

                                var firstDayInYear = new DateTime(currentYear.FirstMonthStart.Year,
                                    currentYear.FirstMonthStart.Month, 1);
                                var lastDayInYear = currentYear.LastMonthStart.AddMonths(1).AddDays(-1);

                                ITimePeriod tradePeriod = new TimeRange(tradeInformation.PeriodFrom,
                                    tradeInformation.PeriodTo);
                                ITimePeriod calendarPeriod = new TimeRange(firstDayInYear, lastDayInYear);
                                bool intersects = tradePeriod.IntersectsWith(calendarPeriod);

                                if (intersects && isMTMAndcommissionCalculation)
                                {
                                    var tradeRange = new TimeRange(tradePeriod);
                                    var calendarRange = new TimeRange(calendarPeriod);
                                    var selectedDatesRange = new TimeRange(_periodFrom, _periodTo);

                                    var s =
                                        selectedDatesRange.GetIntersection(calendarRange)
                                            .GetIntersection(tradeRange);
                                    DateDiff a = new DateDiff(s.Start, s.End);
                                    var numOfTradeMonthsInCurrYear = a.ElapsedDays > 0 ? a.Months + 1 : a.Months;
                                    comm = commissionPerMonth * numOfTradeMonthsInCurrYear;
                                }

                                e.Value =
                                    tradeMTMs[tradeInformation.Identifier].Where(
                                        a =>
                                            "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                            e.Column.FieldName)
                                        .Select(a => a.Value).Sum() - comm;
                                e.Value = Convert.ToDecimal(e.Value) * tradeInformation.Ownership;
                            }
                        }
                    }
                }
                else
                {
                    var cashFlowInfo = (CashFlowInformation)e.Row;
                    if (e.Column.Tag.GetType() == typeof(DateTime))
                    {
                        var date = (DateTime)e.Column.Tag;

                        if (cashFlowInfo.Type == CashFlowInformationTypeEnum.Item)
                        {
                            e.Value = cashFlowInfo.Results.First()[date];
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.GroupHeader)
                        {
                            decimal value = 0;
                            //ProcessCashFlowInfo(cashFlowInfo, ref value, date, cashFlowInfo.Type);
                            e.Value = value;
                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.GroupFooter)
                        {
                            decimal value = 0;
                            // ProcessCashFlowInfo(cashFlowInfo, ref value, date, cashFlowInfo.Type);
                            e.Value = value;
                        }
                    }
                    else if (e.Column.FieldName == "Totals" && cashFlowInfo.Type == CashFlowInformationTypeEnum.Item)
                    {
                        if (cashFlowInfo.Name == "Break Even All" || cashFlowInfo.Name == "Break Even Spot" ||
                            cashFlowInfo.Name == "Break Even Balloon"
                            || cashFlowInfo.Name == "Break Even Equity" || cashFlowInfo.Name == "Valuation" ||
                            cashFlowInfo.Name == "Discount Factor"
                            || cashFlowInfo.Name == "Present Value" || cashFlowInfo.Name == "Break Even Scrap")
                        {
                            e.Value = cashFlowInfo.Results.Aggregate<Dictionary<DateTime, decimal>, decimal>(0,
                                (current,
                                    dictionary)
                                    =>
                                    current +
                                    dictionary.
                                        Values.
                                        Sum());

                        }
                        else if (cashFlowInfo.Type == CashFlowInformationTypeEnum.Item && cashFlowInfo.Name == "IRR")
                        {
                            List<double> values = new List<double>();
                            foreach (Dictionary<DateTime, decimal> dictionary in cashFlowInfo.Results)
                            {
                                foreach (KeyValuePair<DateTime, decimal> keyValuePair in dictionary)
                                {
                                    values.Add(Convert.ToDouble(keyValuePair.Value));
                                }

                            }

                            if (values[0] != 0 && values.Any(a => a != 0))
                            {
                                var calculator = new NewtonRaphsonIRRCalculator(values.ToArray());
                                try
                                {
                                    e.Value = calculator.Execute();
                                }
                                catch (Exception exc)
                                {
                                    e.Value = "There was an error while computing IRR item. " + exc.Message;
                                }
                            }
                            else
                            {
                                e.Value = 0;
                            }
                        }
                    }
                }
            }
        }

        private void btnGeneratePosition_Click(object sender, EventArgs e)
        {
            if (cmbMarketSensitivityType.SelectedItem != null &&
                (cmbMarketSensitivityType.SelectedItem.ToString() != "None" &&
                 cmbMarketSensitivityType.SelectedItem.ToString() != "Stress")
                && (txtMarketSensitivityValue.EditValue == null || txtMarketSensitivityValue.Value == 0))
            {
                XtraMessageBox.Show(this,
                    "Please provide a valid value for field 'Market Sensitivity Value'.",
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _formState = FormStateEnum.WhileExecuting;
            SetGuiControls();
            BeginGeneratePosition();
        }

        private void btnExportResults_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            string strDateFrom = _viewTradesGeneralInfo.periodFrom.Year + "_" + _viewTradesGeneralInfo.periodFrom.Month + "_" + _viewTradesGeneralInfo.periodFrom.Day;
            string strDateTo = _viewTradesGeneralInfo.periodFrom.Year + "_" + _viewTradesGeneralInfo.periodFrom.Month + "_" + _viewTradesGeneralInfo.periodFrom.Day;

            //string strDateFrom = "2014_12_01";
            //string strDateTo = "2014_12_31";

            gridTradeActionResultsView.OptionsPrint.PrintHorzLines = false;
            gridTradeActionResultsView.OptionsPrint.PrintVertLines = false;
            gridTradeActionResultsView.OptionsPrint.AutoWidth = false;

            string fileName = "File";
            string sheetName = "Sheet";

            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                fileName = "FreightMetrics_PositionReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "Position Report";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                fileName = "FreightMetrics_MTMReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "MTM Report";
            }
            else if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "CASHFLOW")
            {
                fileName = "FreightMetrics_CashFlowReport_From_" + strDateFrom + "_To_" + strDateTo;
                sheetName = "Cash Flow Report";
            }

            try
            {
                if (gridTradeActionResultsView.DataRowCount > 60000)
                {
                    gridTradeActionResultsView.ExportToXlsx(
                        Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName +
                        ".xlsx",
                        new XlsxExportOptions
                        {
                            ExportHyperlinks = false,
                            ExportMode = XlsxExportMode.SingleFile,
                            SheetName = sheetName,
                            ShowGridLines = false,
                            TextExportMode = TextExportMode.Value
                        });
                }
                else
                {
                    gridTradeActionResultsView.ExportToXls(
                        Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\" + fileName + ".xls",
                        new XlsExportOptions
                        {
                            ExportHyperlinks = false,
                            ExportMode = XlsExportMode.SingleFile,
                            SheetName = sheetName,
                            ShowGridLines = false,
                            TextExportMode = TextExportMode.Value
                        });
                }
                Cursor = Cursors.Default;
                XtraMessageBox.Show(this, "File with name: '" + fileName + "' exported to Desktop successfully."
                    ,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this,
                    string.Format(Strings.Failed_to_write_file_to_Desktop__Details___0_, exc.Message)
                    ,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExportToCSV_Click(object sender, EventArgs e)
        {
            BeginApplicationParametersInitializationData();
        }

        private void BeginApplicationParametersInitializationData()
        {
            Cursor = Cursors.WaitCursor;

            try
            {
                SessionRegistry.Client.BeginApplicationParametersInitializationData(EndApplicationParametersInitializationData, null);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;

                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndApplicationParametersInitializationData(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndApplicationParametersInitializationData;
                Invoke(action, ar);
                return;
            }
            int? result;
            List<AppParameter> appParameters;
            try
            {
                result = SessionRegistry.Client.EndApplicationParametersInitializationData(out appParameters, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;

                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;

                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                var _appParameterSpanPosConvExePath = appParameters.FirstOrDefault(n => n.Code.Equals("SPAN_POS_CONV_EXE_PATH"));
                var _appParameterSpanPosConvFilesPath = appParameters.FirstOrDefault(n => n.Code.Equals("SPAN_POS_CONV_FILES_PATH"));


                if (_appParameterSpanPosConvExePath == null || _appParameterSpanPosConvFilesPath == null) //Exception Occurred
                {
                    Cursor = Cursors.Default;

                    XtraMessageBox.Show(this,
                                        Strings.Failed_to_retrieve_application_parameter + " " + (_appParameterSpanPosConvExePath == null ? "SPAN_POS_CONV_EXE_PATH" : "SPAN_POS_CONV_FILES_PATH"),
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!Directory.Exists(_appParameterSpanPosConvExePath.Value) || !Directory.Exists(_appParameterSpanPosConvFilesPath.Value))
                {
                    Cursor = Cursors.Default;

                    XtraMessageBox.Show(this,
                                        Strings.
                                            The_folder_does_not_exist + ": " + (!Directory.Exists(_appParameterSpanPosConvExePath.Value) ? _appParameterSpanPosConvExePath.Value : _appParameterSpanPosConvFilesPath.Value),
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!File.Exists(Path.Combine(_appParameterSpanPosConvExePath.Value, "SpanPosConv.exe")) || !File.Exists(Path.Combine(_appParameterSpanPosConvExePath.Value, "SpanPosConversion.dll")))
                {
                    Cursor = Cursors.Default;

                    XtraMessageBox.Show(this,
                                        Strings.
                                            Failed_to_find_file + " " + (!File.Exists(Path.Combine(_appParameterSpanPosConvExePath.Value, "SpanPosConv.exe")) ? Path.Combine(_appParameterSpanPosConvExePath.Value, "SpanPosConv.exe") : Path.Combine(_appParameterSpanPosConvExePath.Value, "SpanPosConversion.dll")),
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    bool reverseNumberSigns = chkReverseNumberSign.Checked;

                    string strDateFrom = _viewTradesGeneralInfo.periodFrom.Year + "_" + _viewTradesGeneralInfo.periodFrom.Month + "_" + _viewTradesGeneralInfo.periodFrom.Day;
                    string strDateTo = _viewTradesGeneralInfo.periodTo.Year + "_" + _viewTradesGeneralInfo.periodTo.Month + "_" + _viewTradesGeneralInfo.periodTo.Day;

                    string fileName = "File";
                    string outputFileName = "File";
                    string filePath = _appParameterSpanPosConvFilesPath.Value;//Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                    string executablePath = string.Empty;

                    fileName = filePath + @"\FreightMetrics_PositionXML_From_" + strDateFrom + "_To_" + strDateTo + ".csv";
                    outputFileName = filePath + @"\FreightMetrics_PositionXML_From_" + strDateFrom + "_To_" + strDateTo + ".pos";
                    executablePath = Path.Combine(_appParameterSpanPosConvExePath.Value, "SpanPosConv.exe");

                    //fileName = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\FreightMetrics_PositionXML_From_" + strDateFrom + "_To_" + strDateTo + ".csv";
                    try
                    {
                        if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Company))
                        {
                            var companiesDict = new Dictionary<string, List<TradeInformation>>();
                            foreach (TradeInformation tradeInformation in _viewTradesGeneralInfo.tradeInformations)
                            {
                                if (tradeInformation.Company != null)
                                {
                                    if (!companiesDict.ContainsKey(tradeInformation.Company))
                                        companiesDict.Add(tradeInformation.Company, new List<TradeInformation>() { tradeInformation });
                                    else
                                        companiesDict[tradeInformation.Company].Add(tradeInformation);

                                }

                            }
                            if (companiesDict.Count > 0)
                            {
                                using (var textWriter = File.CreateText(fileName))
                                {
                                    //using (var csvWriter = new CsvWriter(textWriter))
                                    //{
                                        var records = new List<CSVRecord>();
                                    var titleRow = new CSVRecord();
                                    titleRow.date = "date";
                                    titleRow.issetl = "issetl";
                                    titleRow.firm = "firm";
                                    titleRow.acctid = "acctid";
                                    titleRow.acctType = "acctType";
                                    titleRow.isCust = "isCust";
                                    titleRow.seg = "seg";
                                    titleRow.currency = "currency";
                                    titleRow.ec = "ec";
                                    titleRow.cc = "cc";
                                    titleRow.exch = "exch";
                                    titleRow.pfCode = "pfCode";
                                    titleRow.pfType = "pfType";
                                    titleRow.pe = "pe";
                                    titleRow.undPe = "undPe";
                                    titleRow.o = "o";
                                    titleRow.k = "k";
                                    titleRow.net = "net";
                                    titleRow.coupon = "coupon";
                                    records.Add(titleRow);

                                    #region trade each row
                                    /*foreach (var company in companiesDict)
                                    {
                                        var tradePositions =
                                               (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                                        foreach (TradeInformation tradeInformation in company.Value)
                                        {
                                            foreach (DateTime date in _dates)
                                            {

                                                if (tradeInformation.Trade.Type == TradeTypeEnum.Option)
                                                {
                                                    string pfCode = string.Empty;
                                                    if (tradeInformation.ForwardCurve == "5TC_C")
                                                    {
                                                        pfCode = "CSQ";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "TC_H")
                                                    {
                                                        pfCode = "H6Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "4TC_P")
                                                    {
                                                        pfCode = "P4Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "5TC_S")
                                                    {
                                                        pfCode = "S6Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "10TC_S")
                                                    {
                                                        pfCode = "S0Q";
                                                    }

                                                    string oValue = string.Empty;
                                                    if (tradeInformation.TradeInfo.OptionInfo.Type == TradeOPTIONInfoTypeEnum.Call)
                                                    {
                                                        oValue = "C";
                                                    }
                                                    else
                                                    {
                                                        oValue = "P";
                                                    }

                                                    var value = tradePositions[tradeInformation.Identifier][date];
                                                    value = Convert.ToDecimal(value) * tradeInformation.Ownership;

                                                    var record = new CSVRecord
                                                    {
                                                        date = dtpFilterCurveDate.DateTime.Date.ToString("yyyyMMdd", new CultureInfo("en-GB")),
                                                        issetl = "1",
                                                        firm = "ADMI",
                                                        acctid = company.Key,
                                                        acctType = "S",
                                                        isCust = "1",
                                                        seg = "CUST",
                                                        currency = "USD",
                                                        ec = "NFX",
                                                        cc = "",//PTCM
                                                        exch = "PBT",
                                                        pfCode = pfCode,
                                                        pfType = "OOF",
                                                        pe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                        undPe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                        o = oValue,
                                                        k = tradeInformation.StrikePrice.ToString("0.00", new CultureInfo("en-GB")),
                                                        net = value.ToString("0.00", new CultureInfo("en-GB")),
                                                        coupon = ""

                                                    };

                                                    records.Add(record);
                                                }
                                                else if (tradeInformation.Trade.Type == TradeTypeEnum.FFA)
                                                {
                                                    string pfCode = string.Empty;
                                                    if (tradeInformation.ForwardCurve == "5TC_C")
                                                    {
                                                        pfCode = "CS5Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "TC_H")
                                                    {
                                                        pfCode = "HS6Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "4TC_P")
                                                    {
                                                        pfCode = "PM4Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "5TC_S")
                                                    {
                                                        pfCode = "SM6Q";
                                                    }
                                                    else if (tradeInformation.ForwardCurve == "10TC_S")
                                                    {
                                                        pfCode = "S10Q";
                                                    }

                                                    var value = tradePositions[tradeInformation.Identifier][date];
                                                    value = Convert.ToDecimal(value) * tradeInformation.Ownership;

                                                    var record = new CSVRecord
                                                    {
                                                        date = dtpFilterCurveDate.DateTime.Date.ToString("yyyyMMdd", new CultureInfo("en-GB")),
                                                        issetl = "1",
                                                        firm = "ADMI",
                                                        acctid = company.Key,
                                                        acctType = "S",
                                                        isCust = "1",
                                                        seg = "CUST",
                                                        currency = "USD",
                                                        ec = "NFX",
                                                        cc = "",//PTCM
                                                        exch = "PBT",
                                                        pfCode = pfCode,
                                                        pfType = "FUT",
                                                        pe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                        undPe = "",
                                                        o = "",
                                                        k = "",
                                                        net = value.ToString("0.00", new CultureInfo("en-GB")),
                                                        coupon = ""

                                                    };

                                                    records.Add(record);
                                                }
                                                else
                                                {
                                                    continue;
                                                }

                                            }

                                        }

                                    }*/
                                    #endregion

                                    foreach (var company in companiesDict)
                                    {
                                        var tradePositions =
                                               (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                                        var groupedFFATrades = company.Value.Where(t => t.Trade.Type == TradeTypeEnum.FFA).Select(t=>t.ForwardCurve).Distinct().ToList();

                                        foreach (var ffaFC in groupedFFATrades)
                                        {
                                            foreach (DateTime date in _dates)
                                            {
                                                string pfCode = string.Empty;
                                                if (ffaFC == "5TC_C")
                                                {
                                                    pfCode = "CPTM";
                                                }
                                                else if (ffaFC == "TC_H")
                                                {
                                                    pfCode = "HTCM";
                                                }
                                                else if (ffaFC == "4TC_P")
                                                {
                                                    pfCode = "PTCM";
                                                }
                                                else if (ffaFC == "5TC_P")
                                                {
                                                    pfCode = "P5TC";
                                                }
                                                else if (ffaFC == "5TC_S")
                                                {
                                                    pfCode = "STCM";
                                                }
                                                else if (ffaFC == "10TC_S")
                                                {
                                                    pfCode = "SPTM";
                                                }

                                                var tradeInformationsOfFC = company.Value.Where(t => t.Trade.Type == TradeTypeEnum.FFA && t.ForwardCurve == ffaFC).ToList();
                                                decimal sumValue = 0;
                                                foreach(var tradeInformation in tradeInformationsOfFC)
                                                {
                                                    var value = tradePositions[tradeInformation.Identifier][date];
                                                    if(reverseNumberSigns)
                                                    {
                                                        sumValue = sumValue + (-1) * Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                    else
                                                    {
                                                        sumValue = sumValue + Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                    
                                                }                                                

                                                var record = new CSVRecord
                                                {
                                                    date = dtpFilterCurveDate.DateTime.Date.ToString("yyyyMMdd", new CultureInfo("en-GB")),
                                                    issetl = "1",
                                                    firm = "ADMI",
                                                    acctid = company.Key,
                                                    acctType = "S",
                                                    isCust = "1",
                                                    seg = "CUST",
                                                    currency = "USD",
                                                    ec = "ECC",
                                                    cc = "",
                                                    exch = "EEX",
                                                    pfCode = pfCode,
                                                    pfType = "FUT",
                                                    pe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                    undPe = "",
                                                    o = "",
                                                    k = "",
                                                    net = sumValue.ToString("0.00", new CultureInfo("en-GB")),
                                                    coupon = ""

                                                };

                                                records.Add(record);
                                            }
                                        }

                                        var groupedOptionTrades = company.Value.Where(t => t.Trade.Type == TradeTypeEnum.Option).GroupBy(t => new { t.ForwardCurve, t.StrikePrice, t.TradeInfo.OptionInfo.Type }).
                                            Select(t=>t.Key).ToList();
                                        foreach (var optionFCStrikePrice in groupedOptionTrades)
                                        {
                                            foreach (DateTime date in _dates)
                                            {
                                                string pfCode = string.Empty;
                                                if (optionFCStrikePrice.ForwardCurve == "5TC_C")
                                                {
                                                    pfCode = "OCPM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "TC_H")
                                                {
                                                    pfCode = "OHTM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "4TC_P")
                                                {
                                                    pfCode = "OPTM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "5TC_P")
                                                {
                                                    pfCode = "OP5M";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "5TC_S")
                                                {
                                                    pfCode = "OTSM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "10TC_S")
                                                {
                                                    pfCode = "OPSM";
                                                }

                                                string oValue = string.Empty;
                                                if (optionFCStrikePrice.Type == TradeOPTIONInfoTypeEnum.Call)
                                                {
                                                    oValue = "C";
                                                }
                                                else
                                                {
                                                    oValue = "P";
                                                }

                                                var tradeInformationsOfFCStrikePrice = company.Value.Where(t => t.Trade.Type == TradeTypeEnum.Option && t.ForwardCurve == optionFCStrikePrice.ForwardCurve
                                                && t.StrikePrice == optionFCStrikePrice.StrikePrice && t.Type == optionFCStrikePrice.Type.ToString("g")).ToList();
                                                decimal sumValue = 0;
                                                foreach (var tradeInformation in tradeInformationsOfFCStrikePrice)
                                                {
                                                    var value = tradePositions[tradeInformation.Identifier][date];
                                                    if ((reverseNumberSigns && optionFCStrikePrice.Type == TradeOPTIONInfoTypeEnum.Call) ||
                                                        (!reverseNumberSigns && optionFCStrikePrice.Type == TradeOPTIONInfoTypeEnum.Put)
                                                        )
                                                    {
                                                        sumValue = sumValue + (-1) * Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                    else
                                                    {
                                                        sumValue = sumValue + Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                }
                                                

                                                var record = new CSVRecord
                                                {
                                                    date = dtpFilterCurveDate.DateTime.Date.ToString("yyyyMMdd", new CultureInfo("en-GB")),
                                                    issetl = "1",
                                                    firm = "ADMI",
                                                    acctid = company.Key,
                                                    acctType = "S",
                                                    isCust = "1",
                                                    seg = "CUST",
                                                    currency = "USD",
                                                    ec = "ECC",//"NFX"
                                                    cc = "",
                                                    exch = "EEX",//"PBT"
                                                    pfCode = pfCode,
                                                    pfType = "OOF",
                                                    pe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                    undPe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                    o = oValue,
                                                    k = optionFCStrikePrice.StrikePrice.ToString("0.00", new CultureInfo("en-GB")),
                                                    net = sumValue.ToString("0.00", new CultureInfo("en-GB")),
                                                    coupon = ""

                                                };

                                                records.Add(record);
                                            }
                                        }


                                        

                                    }
                                    
                                    

                                    // csvWriter.WriteRecords(records);
                                    foreach (var item in records)
                                    {
                                        var sb = new StringBuilder();
                                        var line = "";
                                        foreach (var prop in typeof(CSVRecord).GetProperties())
                                        {
                                            line += prop.GetValue(item, null) + ",";
                                        }
                                        line = line.Substring(0, line.Length - 1);
                                        sb.AppendLine(line);
                                        textWriter.Write(sb);
                                    }
                                    // }
                                }
                            }
                        }
                        else if (_viewTradesGeneralInfo.selectedHierarchyNode.DomainObject.GetType() == typeof(Book))
                        {
                            var parentBooksDict = new Dictionary<string, List<TradeInformation>>();
                            foreach (TradeInformation tradeInformation in _viewTradesGeneralInfo.tradeInformations)
                            {
                                if (tradeInformation.TradeInfo.TradeInfoBooks != null && tradeInformation.TradeInfo.TradeInfoBooks.Count > 0)
                                {
                                    var associatedBooks = tradeInformation.TradeInfo.TradeInfoBooks.Select(a => a.BookId).ToList();
                                    var parentBook = _viewTradesGeneralInfo.books.SingleOrDefault(a => associatedBooks.Contains(a.Id) && a.ParentBookId == null);
                                    if (parentBook != null)
                                    {
                                        if (!parentBooksDict.ContainsKey(parentBook.Name))
                                            parentBooksDict.Add(parentBook.Name, new List<TradeInformation>() { tradeInformation });
                                        else
                                            parentBooksDict[parentBook.Name].Add(tradeInformation);
                                    }
                                }

                            }
                            if (parentBooksDict.Count > 0)
                            {
                                using (var textWriter = File.CreateText(fileName))
                                {
                                //    using (var csvWriter = new CsvWriter(textWriter))
                                //    {
                                        var records = new List<CSVRecord>();
                                        var titleRow = new CSVRecord();
                                        titleRow.date = "date";
                                        titleRow.issetl = "issetl";
                                        titleRow.firm = "firm";
                                        titleRow.acctid = "acctid";
                                        titleRow.acctType = "acctType";
                                        titleRow.isCust = "isCust";
                                        titleRow.seg = "seg";
                                        titleRow.currency = "currency";
                                        titleRow.ec = "ec";
                                        titleRow.cc = "cc";
                                        titleRow.exch = "exch";
                                        titleRow.pfCode = "pfCode";
                                        titleRow.pfType = "pfType";
                                        titleRow.pe = "pe";
                                        titleRow.undPe = "undPe";
                                        titleRow.o = "o";
                                        titleRow.k = "k";
                                        titleRow.net = "net";
                                        titleRow.coupon = "coupon";
                                        records.Add(titleRow);

                                    foreach (var parentBook in parentBooksDict)
                                        {
                                        var tradePositions =
                                       (Dictionary<string, Dictionary<DateTime, decimal>>)gridTradeActionResultsView.Tag;

                                        var groupedFFATrades = parentBook.Value.Where(t => t.Trade.Type == TradeTypeEnum.FFA).Select(t => t.ForwardCurve).Distinct().ToList();

                                        foreach (var ffaFC in groupedFFATrades)
                                        {
                                            foreach (DateTime date in _dates)
                                            {
                                                string pfCode = string.Empty;
                                                if (ffaFC == "5TC_C")
                                                {
                                                    pfCode = "CPTM";
                                                }
                                                else if (ffaFC == "TC_H")
                                                {
                                                    pfCode = "HTCM";
                                                }
                                                else if (ffaFC == "4TC_P")
                                                {
                                                    pfCode = "PTCM";
                                                }
                                                else if (ffaFC == "5TC_P")
                                                {
                                                    pfCode = "P5TC";
                                                }
                                                else if (ffaFC == "5TC_S")
                                                {
                                                    pfCode = "STCM";
                                                }
                                                else if (ffaFC == "10TC_S")
                                                {
                                                    pfCode = "SPTM";
                                                }

                                                var tradeInformationsOfFC = parentBook.Value.Where(t => t.Trade.Type == TradeTypeEnum.FFA && t.ForwardCurve == ffaFC).ToList();
                                                decimal sumValue = 0;
                                                foreach (var tradeInformation in tradeInformationsOfFC)
                                                {
                                                    var value = tradePositions[tradeInformation.Identifier][date];
                                                    if (reverseNumberSigns)
                                                    {
                                                        sumValue = sumValue + (-1) * Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                    else
                                                    {
                                                        sumValue = sumValue + Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                }

                                                var record = new CSVRecord
                                                {
                                                    date = dtpFilterCurveDate.DateTime.Date.ToString("yyyyMMdd", new CultureInfo("en-GB")),
                                                    issetl = "1",
                                                    firm = "ADMI",
                                                    acctid = parentBook.Key,
                                                    acctType = "S",
                                                    isCust = "1",
                                                    seg = "CUST",
                                                    currency = "USD",
                                                    ec = "ECC",//"NFX",
                                                    cc = "",
                                                    exch = "EEX",//"PBT",
                                                    pfCode = pfCode,
                                                    pfType = "FUT",
                                                    pe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                    undPe = "",
                                                    o = "",
                                                    k = "",
                                                    net = sumValue.ToString("0.00", new CultureInfo("en-GB")),
                                                    coupon = ""

                                                };

                                                records.Add(record);
                                            }
                                        }

                                        var groupedOptionTrades = parentBook.Value.Where(t => t.Trade.Type == TradeTypeEnum.Option).GroupBy(t => new { t.ForwardCurve, t.StrikePrice, t.TradeInfo.OptionInfo.Type }).
                                            Select(t => t.Key).ToList();
                                        foreach (var optionFCStrikePrice in groupedOptionTrades)
                                        {
                                            foreach (DateTime date in _dates)
                                            {
                                                string pfCode = string.Empty;
                                                if (optionFCStrikePrice.ForwardCurve == "5TC_C")
                                                {
                                                    pfCode = "OCPM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "TC_H")
                                                {
                                                    pfCode = "OHTM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "4TC_P")
                                                {
                                                    pfCode = "OPTM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "5TC_P")
                                                {
                                                    pfCode = "OP5M";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "5TC_S")
                                                {
                                                    pfCode = "OTSM";
                                                }
                                                else if (optionFCStrikePrice.ForwardCurve == "10TC_S")
                                                {
                                                    pfCode = "OPSM";
                                                }

                                                string oValue = string.Empty;
                                                if (optionFCStrikePrice.Type == TradeOPTIONInfoTypeEnum.Call)
                                                {
                                                    oValue = "C";
                                                }
                                                else
                                                {
                                                    oValue = "P";
                                                }

                                                var tradeInformationsOfFCStrikePrice = parentBook.Value.Where(t => t.Trade.Type == TradeTypeEnum.Option && t.ForwardCurve == optionFCStrikePrice.ForwardCurve
                                                && t.StrikePrice == optionFCStrikePrice.StrikePrice && t.Type == optionFCStrikePrice.Type.ToString("g")).ToList();
                                                decimal sumValue = 0;
                                                foreach (var tradeInformation in tradeInformationsOfFCStrikePrice)
                                                {
                                                    var value = tradePositions[tradeInformation.Identifier][date];
                                                    if ((reverseNumberSigns && optionFCStrikePrice.Type == TradeOPTIONInfoTypeEnum.Call) ||
                                                       (!reverseNumberSigns && optionFCStrikePrice.Type == TradeOPTIONInfoTypeEnum.Put)
                                                    )
                                                    {
                                                        sumValue = sumValue + (-1) * Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                    else
                                                    {
                                                        sumValue = sumValue + Convert.ToDecimal(value) * tradeInformation.Ownership;
                                                    }
                                                }


                                                var record = new CSVRecord
                                                {
                                                    date = dtpFilterCurveDate.DateTime.Date.ToString("yyyyMMdd", new CultureInfo("en-GB")),
                                                    issetl = "1",
                                                    firm = "ADMI",
                                                    acctid = parentBook.Key,
                                                    acctType = "S",
                                                    isCust = "1",
                                                    seg = "CUST",
                                                    currency = "USD",
                                                    ec = "ECC",//"NFX",
                                                    cc = "",
                                                    exch = "EEX",//"PBT",
                                                    pfCode = pfCode,
                                                    pfType = "OOF",
                                                    pe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                    undPe = date.ToString("yyyyMM", new CultureInfo("en-GB")),
                                                    o = oValue,
                                                    k = optionFCStrikePrice.StrikePrice.ToString("0.00", new CultureInfo("en-GB")),
                                                    net = sumValue.ToString("0.00", new CultureInfo("en-GB")),
                                                    coupon = ""

                                                };

                                                records.Add(record);
                                            }
                                        }


                                    }

                                    //     csvWriter.WriteRecords(records);
                                    foreach (var item in records)
                                    {
                                        var sb = new StringBuilder();
                                        var line = "";
                                        foreach (var prop in typeof(CSVRecord).GetProperties())
                                        {
                                            line += prop.GetValue(item, null) + ",";
                                        }
                                        line = line.Substring(0, line.Length - 1);
                                        sb.AppendLine(line);
                                        textWriter.Write(sb);
                                    }
                                    // }
                                }
                            }
                        }

                        
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.CreateNoWindow = false;
                        startInfo.UseShellExecute = false;
                        startInfo.FileName = executablePath;
                        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        startInfo.Arguments = "/i " + fileName + " /o " + outputFileName;

                        try
                        {
                            // Start the process with the info we specified.
                            // Call WaitForExit and then the using statement will close.
                            using (Process exeProcess = Process.Start(startInfo))
                            {
                                exeProcess.WaitForExit();
                            }

                            XtraMessageBox.Show(this, "File with name: '" + fileName + "' got processed successfully."
                          ,
                          Strings.Freight_Metrics,
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch
                        {
                            // Log error.
                            XtraMessageBox.Show(this, "There was a problem processing the file with name: '" + fileName + "' .",
                          Strings.Freight_Metrics,
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        

                        Cursor = Cursors.Default;

                    }
                    catch (Exception exc)
                    {
                        Cursor = Cursors.Default;

                        XtraMessageBox.Show(this,
                            string.Format(Strings.Failed_to_write_file_to_Desktop__Details___0_, exc.Message)
                            ,
                            Strings.Freight_Metrics,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }



                }
            }
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void GridTradeActionResultsViewCustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            if (((GridSummaryItem)e.Item).Tag == null ||
                (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() != "MTM"))
                return;

            if ((string)((GridSummaryItem)e.Item).Tag == "Index")
            {
                e.TotalValue = ((GridSummaryItem)e.Item).FieldName;
            }
            else
            {
                string indexName = ((GridSummaryItem)e.Item).FieldName;
                if (rdgResultsAggregationType.SelectedIndex == 0) // Month
                {
                    DateTime dt = Convert.ToDateTime(((GridSummaryItem)e.Item).Tag);
                    e.TotalValue = _indexesValues[dt][indexName];
                    e.TotalValue = 22;
                }
                else if (rdgResultsAggregationType.SelectedIndex == 1) //Quarter
                {
                    string quarter = ((GridSummaryItem)e.Item).Tag.ToString();
                    e.TotalValue = _indexesValues.Where(
                        a =>
                        {
                            int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                            string strQuarter = intQuarter == 1
                                ? "Q1"
                                : intQuarter == 2 ? "Q2" : intQuarter == 3 ? "Q3" : "Q4";
                            return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == quarter;
                        }).Any(a => a.Value.Count > 0)
                        ? _indexesValues.Where(
                            a =>
                            {
                                int intQuarter = ((a.Key.Month - 1) / 3) + 1;
                                string strQuarter = intQuarter == 1
                                    ? "Q1"
                                    : intQuarter == 2
                                        ? "Q2"
                                        : intQuarter == 3 ? "Q3" : "Q4";
                                return strQuarter + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) ==
                                       quarter;
                            })
                            .Where(a => a.Value.Count > 0 && a.Value.ContainsKey(indexName))
                            .Select(a => a.Value[indexName])
                            .Average()
                        : 0;
                }
                else if (rdgResultsAggregationType.SelectedIndex == 2) //Calendar
                {
                    string calendar = ((GridSummaryItem)e.Item).Tag.ToString();

                    if (
                        _indexesValues.Any(
                            a =>
                                "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar &&
                                a.Value.Count > 0 && a.Value.ContainsKey(indexName)))
                    {
                        e.TotalValue =
                            _indexesValues.Where(
                                a =>
                                    "CAL" + a.Key.ToString("-yyyy", new CultureInfo("en-GB")) == calendar &&
                                    a.Value.Count > 0 && a.Value.ContainsKey(indexName))
                                .Select(a => a.Value[indexName])
                                .
                                Average();
                    }
                    else
                        e.TotalValue = 0;
                }
            }
        }

        private void RdgResultsAggregationTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                btnGeneratePosition.PerformClick();
            }
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                CreateResultGridColumns(_tradeMTMResults, _tradeEmbeddedValues, "MTM");
            }
        }

        private void RdgPositionResultsTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "POSITION")
            {
                btnGeneratePosition.PerformClick();
            }
            if (gridTradeActionResults.Tag != null && gridTradeActionResults.Tag.ToString() == "MTM")
            {
                CreateResultGridColumns(_tradeMTMResults, _tradeEmbeddedValues, "MTM");
            }
        }

        private void CmbMarketSensitivityTypeSelectedValueChanged(object sender, EventArgs e)
        {
            txtMarketSensitivityValue.Properties.ReadOnly = (cmbMarketSensitivityType.SelectedItem != null &&
                                                             (cmbMarketSensitivityType.SelectedItem.ToString() ==
                                                              "Stress" ||
                                                              cmbMarketSensitivityType.SelectedItem.ToString() == "None"));
        }

        #endregion

        #region Proxy Calls

        private void BeginGeneratePosition()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm1 = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginGeneratePosition(dtpFilterCurveDate.DateTime.Date,
                    _viewTradesGeneralInfo.periodFrom.Date, _viewTradesGeneralInfo.periodTo.Date,
                    (string)cmbPositionMethod.SelectedItem, (string)cmbMarketSensitivityType.SelectedItem,
                    txtMarketSensitivityValue.Value, chkUseSpotValues.Checked, _viewTradesGeneralInfo.tradeIdentifiers,
                    EndGeneratePosition, null);
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                    Strings.
                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndGeneratePosition(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndGeneratePosition;
                Invoke(action, ar);
                return;
            }

            int? result;
            Dictionary<string, Dictionary<DateTime, decimal>> tradePositions;
            Dictionary<string, Dictionary<DateTime, decimal>> tradePositionsWithoutWeights;
            string errorMessage = "";
            try
            {
                result = SessionRegistry.Client.EndGeneratePosition(out tradePositions,out tradePositionsWithoutWeights, out errorMessage, ar);
            }
            catch (Exception)
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                    Strings.
                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _formState = FormStateEnum.AfterExecution;
            SetGuiControls();

            if (result == null) //Exception Occurred
            {
                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }
                XtraMessageBox.Show(this,
                    string.IsNullOrEmpty(errorMessage)
                        ? Strings.
                            There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_
                        : errorMessage,
                    Strings.Freight_Metrics,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                //XtraMessageBox.Show(this,
                //                    Strings.
                //                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                //                    Strings.Freight_Metrics,
                //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 0) //everything is OK
            {
                _tradePositionResults = tradePositions;

                _availableCodes =
                    _viewTradesGeneralInfo.tradeInformations
                        .Select(n => n.ForwardCurve)
                        .ToList();

                _dates = tradePositions.First().Value.Select(a => a.Key).ToList();

                if (_viewTradesGeneralInfo.tradeInformations.Any(n => n.Type == "FFA" || n.Type == "OPTION" || n.Type == "Put" || n.Type == "Call"))
                {
                    tbpgInitialMargin.PageEnabled = true;

                    //Position Summaries Per Index
                    _indexPosValues = new Dictionary<string, Dictionary<DateTime, decimal>>();

                    //var groups = _viewTradesGeneralInfo.tradeInformations.GroupBy(n => n.ForwardCurve).ToList();
                    foreach (var c in _availableCodes)
                    {
                        //find trades with same c
                        var tradesWithSameCode =
                            _viewTradesGeneralInfo.tradeInformations.Where(
                                n => n.ForwardCurve == c && (n.Type == "FFA" || n.Type == "OPTION"))
                                .Select(n => n.Identifier)
                                .ToList();
                        var tradesTC =
                                _viewTradesGeneralInfo.tradeInformations.Where(
                                n => n.ForwardCurve == c && (n.Type == "TC"))
                                .Select(n => n.Identifier)
                                .ToList();

                        foreach (var t in tradePositions)
                        {
                            if (tradesWithSameCode.Contains(t.Key))
                            {
                                if (!_indexPosValues.ContainsKey(c))
                                    _indexPosValues.Add(c, new Dictionary<DateTime, decimal>() { });

                                foreach (var date in _dates)
                                {
                                    if (!_indexPosValues[c].ContainsKey(date))
                                        _indexPosValues[c].Add(date, new decimal());

                                    var trade = _viewTradesGeneralInfo.tradeInformations.Single(n => n.Identifier == t.Key);

                                    decimal tradeDateValue;
                                    if (trade.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                    {
                                        tradeDateValue = Math.Abs(tradePositions[t.Key][date]);
                                    }
                                    else
                                        tradeDateValue = Math.Abs(tradePositions[t.Key][date]) * (-1);
                                    //_indexPosValues[c][date] = _indexPosValues[c][date] +tradePositions[t.Key][date];
                                    _indexPosValues[c][date] = _indexPosValues[c][date] + tradeDateValue;
                                }


                            }
                            else
                            {
                                var trade = _viewTradesGeneralInfo.tradeInformations.Single(n => n.Identifier == t.Key);

                                //if (trade.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                //{
                                //    if (trade.SubType == (decimal)TradeInfoCompanySubtypeEnum.ShipOwning)
                                //    {
                                //        t.Value = t.Value * (-1);
                                //    }                                    

                                //}
                            }

                            //if (tradesTC.Contains(t.key))
                            //{

                            //}
                        }
                    }

                    //Option Position Summaries
                    var optPos =
                        _viewTradesGeneralInfo.tradeInformations.GroupBy(
                            x => new { x.ForwardCurve, x.StrikePrice, x.Type })
                            .Select(
                                x =>
                                    new
                                    {
                                        ForwardCurve = x.Key.ForwardCurve,
                                        StrikePrice = x.Key.StrikePrice,
                                        CallOrPut = x.Key.Type
                                    }).ToList();

                    _optPosValues = new List<OptionPositionValues>();
                    foreach (var c in optPos)
                    {
                        if (c.StrikePrice != 0)
                        {
                            var optPosValue = new OptionPositionValues
                            {
                                Index = c.ForwardCurve,
                                StrikePrice = c.StrikePrice,
                                CallOrPut = c.CallOrPut
                            };

                            var optionInTrades =
                                _viewTradesGeneralInfo.tradeInformations.Where(
                                    n => n.ForwardCurve == optPosValue.Index
                                         && n.Type == optPosValue.CallOrPut &&
                                         n.StrikePrice ==
                                         optPosValue.StrikePrice).ToList();

                            optPosValue.PosValues = new Dictionary<DateTime, decimal>();
                            foreach (var date in _dates)
                            {
                                var sum = new decimal();
                                foreach (var option in optionInTrades)
                                {
                                    var tradeDateValue = tradePositions[option.Identifier][date];
                                    if (option.Type == "Put")
                                    {
                                        if (option.Direction == TradeInfoDirectionEnum.InOrBuy.ToString("g"))
                                            tradeDateValue = Math.Abs(tradeDateValue);
                                        else
                                            tradeDateValue = Math.Abs(tradeDateValue) * (-1);
                                    }
                                    sum = sum + tradeDateValue;
                                }
                                optPosValue.PosValues.Add(date, sum);
                            }

                            _optPosValues.Add(optPosValue);
                        }
                    }

                    //if (_optPosValues.Count <= 0)
                    //{
                    //    lcgSpanOptionsParameters.Visibility = LayoutVisibility.Never;
                    //}
                }

                if (!RatioReportPL && !RatioReportCashFlow)
                    CreateResultGridColumns(tradePositions, null, "POSITION");

                Cursor = Cursors.Default;
                lock (_syncObject)
                {
                    _dialogForm1.Close();
                    _dialogForm1 = null;
                }

                //if (RatioReportPL)
                //    btnMarkToMarket.PerformClick();


                _initialMarginControl.LoadPositionValues(tradePositions);
                _initialMarginNasdaqControl.LoadPositionValues(tradePositions);
            }
        }

        #endregion

        #region Events

        public event Action<MemoryStream, string> ViewChartEvent;
        public event Action<long, long, string, TradeTypeEnum> OnViewTrade;
        public event Action<long, long, string, TradeTypeEnum> OnEditTrade;

        public event Action<Dictionary<DateTime, List<double>>, List<double>, List<Dictionary<DateTime, decimal>>>
            MonteCarloSimulationEvent;

        public event
            Action
                <List<TradeInformation>, Dictionary<string, Dictionary<DateTime, decimal>>,
                    Dictionary<string, Dictionary<string, Dictionary<DateTime, decimal>>>, DateTime, DateTime, DateTime>
            RatiosReportEvent;

        public event Action OnOpenActivateTradesForm;
        public event Action OnTradeDeactivated;

        #endregion

        #region Public Methods

        public void RefreshData()
        {
            //if (treeNavigation.FocusedNode != null)
            //{
            //    var filters = new List<HierarchyNodeInfo>();
            //    TreeListNode node = treeNavigation.FocusedNode;
            //    var entity = (HierarchyNodeInfo) node.Tag;
            //    filters.Add(entity);

            //    TreeListNode parentNode = node.ParentNode;
            //    while (parentNode != null)
            //    {
            //        filters.Add((HierarchyNodeInfo) parentNode.Tag);
            //        parentNode = parentNode.ParentNode;
            //    }

            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesByFilters(filters);
            //}
            //else
            //{
            //    if (treeBookNavigation.FocusedNode == null)
            //        return;
            //    var filters = new List<HierarchyNodeInfo>();
            //    TreeListNode node = treeBookNavigation.FocusedNode;
            //    var entity = (HierarchyNodeInfo) node.Tag;
            //    filters.Add(entity);

            //    TreeListNode parentNode = node.ParentNode;
            //    while (parentNode != null)
            //    {
            //        filters.Add((HierarchyNodeInfo) parentNode.Tag);
            //        parentNode = parentNode.ParentNode;
            //    }

            //    _formState = FormStateEnum.BeforeSearchTree;
            //    SetGuiControls();

            //    BeginGetTradesByFilters(filters);
            //}
        }


        #endregion
        
    }

    public class CSVRecord
    {
        public string date { get; set; }
        public string issetl { get; set; }
        public string firm { get; set; }
        public string acctid { get; set; }
        public string acctType { get; set; }
        public string isCust { get; set; }
        public string seg { get; set; }
        public string currency { get; set; }
        public string ec { get; set; }
        public string cc { get; set; }
        public string exch { get; set; }
        public string pfCode { get; set; }
        public string pfType { get; set; }
        public string pe { get; set; }
        public string undPe { get; set; }
        public string o { get; set; }
        public string k { get; set; }
        public string net { get; set; }
        public string coupon { get; set; }
    }



}