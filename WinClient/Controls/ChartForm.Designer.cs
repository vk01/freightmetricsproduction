﻿namespace Exis.WinClient.Controls
{
    partial class ChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChartForm));
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.chart = new DevExpress.XtraCharts.ChartControl();
            this.layoutRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutGroupGraph = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnExport = new DevExpress.XtraBars.BarLargeButtonItem();
            this.BtnPrint = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList24
            // 
            this.imageList24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "export24x24.ico");
            this.imageList24.Images.SetKeyName(1, "print24x24.ico");
            this.imageList24.Images.SetKeyName(2, "exit.ico");
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.Controls.Add(this.chart);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(569, 275, 250, 350);
            this.layoutControl.Root = this.layoutRoot;
            this.layoutControl.Size = new System.Drawing.Size(1091, 592);
            this.layoutControl.TabIndex = 3;
            this.layoutControl.Text = "layoutControl1";
            // 
            // chart
            // 
            this.chart.Location = new System.Drawing.Point(14, 33);
            this.chart.Name = "chart";
            this.chart.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel3.LineVisible = true;
            this.chart.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chart.Size = new System.Drawing.Size(1063, 545);
            this.chart.TabIndex = 9;
            // 
            // layoutRoot
            // 
            this.layoutRoot.CustomizationFormText = "layoutRoot";
            this.layoutRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutRoot.GroupBordersVisible = false;
            this.layoutRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupGraph});
            this.layoutRoot.Location = new System.Drawing.Point(0, 0);
            this.layoutRoot.Name = "layoutRoot";
            this.layoutRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutRoot.Size = new System.Drawing.Size(1091, 592);
            this.layoutRoot.Text = "layoutRoot";
            this.layoutRoot.TextVisible = false;
            // 
            // layoutGroupGraph
            // 
            this.layoutGroupGraph.CustomizationFormText = "Information";
            this.layoutGroupGraph.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutChart});
            this.layoutGroupGraph.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupGraph.Name = "layoutGroupGraph";
            this.layoutGroupGraph.Size = new System.Drawing.Size(1091, 592);
            this.layoutGroupGraph.Text = "Chart";
            // 
            // layoutChart
            // 
            this.layoutChart.Control = this.chart;
            this.layoutChart.CustomizationFormText = "layoutChart";
            this.layoutChart.Location = new System.Drawing.Point(0, 0);
            this.layoutChart.Name = "layoutChart";
            this.layoutChart.Size = new System.Drawing.Size(1067, 549);
            this.layoutChart.Text = "layoutChart";
            this.layoutChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutChart.TextToControlDistance = 0;
            this.layoutChart.TextVisible = false;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExport,
            this.BtnPrint,
            this.btnClose});
            this.barManager1.LargeImages = this.imageList24;
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnExport
            // 
            this.btnExport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnExport.Caption = "Export";
            this.btnExport.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnExport.Id = 0;
            this.btnExport.LargeImageIndex = 0;
            this.btnExport.Name = "btnExport";
            // 
            // BtnPrint
            // 
            this.BtnPrint.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.BtnPrint.Caption = "Print";
            this.BtnPrint.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.BtnPrint.Id = 1;
            this.BtnPrint.LargeImageIndex = 1;
            this.BtnPrint.Name = "BtnPrint";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnClose.Caption = "Close";
            this.btnClose.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.btnClose.Id = 2;
            this.btnClose.LargeImageIndex = 2;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1091, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 592);
            this.barDockControlBottom.Size = new System.Drawing.Size(1091, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 592);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1091, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 592);
            // 
            // ChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 624);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ChartForm";
            this.Load += new System.EventHandler(this.ChartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraCharts.ChartControl chart;
        private DevExpress.XtraLayout.LayoutControlGroup layoutRoot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroupGraph;
        private DevExpress.XtraLayout.LayoutControlItem layoutChart;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem btnExport;
        private DevExpress.XtraBars.BarLargeButtonItem BtnPrint;
        private DevExpress.XtraBars.BarLargeButtonItem btnClose;
    }
}