﻿using System;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Exis.WinClient.Controls
{
    public partial class ChartForm : XtraForm
    {
        private readonly MemoryStream _chartData;

        public ChartForm(MemoryStream chartData)
        {
            InitializeComponent();
            _chartData = chartData;
        }

        private void ChartForm_Load(object sender, EventArgs e)
        {
            chart.LoadFromStream(_chartData);
            _chartData.Close();
            _chartData.Dispose();
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (OnDataSaved != null)
                OnDataSaved(this, true);
        }

        #region Events

        public event Action<object, bool> OnDataSaved;

        #endregion


    }
}