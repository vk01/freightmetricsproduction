﻿using System;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.ClientsEngineService;
using Exis.WinClient.General;
using Exis.WinClient.Resources;
using Exis.WinClient.SpecialForms;

namespace Exis.WinClient
{
    public partial class ServConnSettingsForm : XtraForm
    {
        #region Private Properties

        private WaitDialogForm _dialogForm;

        #endregion

        #region Constructors

        public ServConnSettingsForm()
        {
            InitializeComponent();
            Icon = ComponentImages.settings16x16;
            LoadSettings();
        }

        #endregion

        #region Private Methods

        private void LoadSettings()
        {
            txtServerName.Text = GeneralSettings.Default.CnMachineName;
            txtServerPort.Value = String.IsNullOrEmpty(GeneralSettings.Default.CnPort)
                                      ? 9000
                                      : Convert.ToInt32(GeneralSettings.Default.CnPort);
        }

        private void SaveSettings()
        {
            GeneralSettings.Default.CnMachineName = txtServerName.Text.Trim();
            GeneralSettings.Default.CnPort = txtServerPort.Value.ToString();
            GeneralSettings.Default.Save();
        }

        #endregion

        #region Bar Events Handling

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string uri = txtServerName.Text.Trim() + ":" + txtServerPort.Value;
            var clientsTcpAddress =
                new EndpointAddress("net.tcp://" + uri + "/Exis/FreightMetrics/ClientsEngine");

            var clientsBindingTcp = new NetTcpBinding()
            {
                OpenTimeout = TimeSpan.FromSeconds(10),
                SendTimeout = TimeSpan.FromSeconds(10),
                ReceiveTimeout = TimeSpan.FromSeconds(10),
            };

            clientsBindingTcp.Security.Mode = SecurityMode.None;
            //clientsBindingTcp.Security.Mode = SecurityMode.Message;
            //clientsBindingTcp.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            var clientTcp = new ClientsServiceClient(clientsBindingTcp, clientsTcpAddress);

            clientsBindingTcp.ReliableSession.Enabled = true;

            //clientTcp.ClientCredentials.Windows.ClientCredential.UserName = "elanast";
            //clientTcp.ClientCredentials.Windows.ClientCredential.Password = "elanast";

            try
            {
                Cursor = Cursors.WaitCursor;
                _dialogForm = new WaitDialogForm(this);
                clientTcp.BeginPollService(EndPollService, clientTcp);
            }
            catch (Exception)
            {
                _dialogForm.Close();
                _dialogForm = null;
                XtraMessageBox.Show(this, Strings.
                                              Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndPollService(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndPollService;
                Invoke(action, ar);
                return;
            }
            ClientsServiceClient clientTcp = (ClientsServiceClient) ar.AsyncState;
            Cursor = Cursors.Default;
            try
            {
                clientTcp.EndPollService(ar);
                clientTcp.Close();
                SaveSettings();
                _dialogForm.Close();
                _dialogForm = null;

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                _dialogForm.Close();
                _dialogForm = null;
                XtraMessageBox.Show(this, Strings.
                                              Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        #endregion
    }
}