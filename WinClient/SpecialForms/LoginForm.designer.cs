﻿namespace Exis.WinClient.SpecialForms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.txtUser = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtServerPort = new DevExpress.XtraEditors.SpinEdit();
            this.txtServerName = new DevExpress.XtraEditors.TextEdit();
            this.btnSubmit = new DevExpress.XtraEditors.SimpleButton();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSubmit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServerPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSubmit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUser
            // 
            resources.ApplyResources(this.txtUser, "txtUser");
            this.txtUser.Name = "txtUser";
            this.txtUser.StyleController = this.layoutControl1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtServerPort);
            this.layoutControl1.Controls.Add(this.txtServerName);
            this.layoutControl1.Controls.Add(this.btnSubmit);
            this.layoutControl1.Controls.Add(this.txtUser);
            this.layoutControl1.Controls.Add(this.txtPassword);
            this.layoutControl1.Controls.Add(this.btnExit);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup;
            // 
            // txtServerPort
            // 
            resources.ApplyResources(this.txtServerPort, "txtServerPort");
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtServerPort.StyleController = this.layoutControl1;
            // 
            // txtServerName
            // 
            resources.ApplyResources(this.txtServerName, "txtServerName");
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.StyleController = this.layoutControl1;
            // 
            // btnSubmit
            // 
            this.btnSubmit.ImageIndex = 0;
            this.btnSubmit.ImageList = this.imageList24;
            this.btnSubmit.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            resources.ApplyResources(this.btnSubmit, "btnSubmit");
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.StyleController = this.layoutControl1;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // imageList24
            // 
            resources.ApplyResources(this.imageList24, "imageList24");
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "event32x32.ico");
            this.imageList24.Images.SetKeyName(1, "exit.ico");
            // 
            // txtPassword
            // 
            resources.ApplyResources(this.txtPassword, "txtPassword");
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.PasswordChar = '*';
            this.txtPassword.StyleController = this.layoutControl1;
            // 
            // btnExit
            // 
            this.btnExit.ImageIndex = 1;
            this.btnExit.ImageList = this.imageList24;
            this.btnExit.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            resources.ApplyResources(this.btnExit, "btnExit");
            this.btnExit.Name = "btnExit";
            this.btnExit.StyleController = this.layoutControl1;
            this.btnExit.TabStop = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // layoutControlGroup
            // 
            resources.ApplyResources(this.layoutControlGroup, "layoutControlGroup");
            this.layoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup.GroupBordersVisible = false;
            this.layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemUserName,
            this.layoutControlItemPassword,
            this.layoutSubmit,
            this.layoutExit,
            this.layoutServerName,
            this.layoutServerPort,
            this.emptySpaceItem1});
            this.layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup.Name = "layoutControlGroup";
            this.layoutControlGroup.Size = new System.Drawing.Size(312, 125);
            this.layoutControlGroup.TextVisible = false;
            // 
            // layoutControlItemUserName
            // 
            this.layoutControlItemUserName.Control = this.txtUser;
            resources.ApplyResources(this.layoutControlItemUserName, "layoutControlItemUserName");
            this.layoutControlItemUserName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemUserName.Name = "layoutControlItemUserName";
            this.layoutControlItemUserName.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItemUserName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.SupportHorzAlignment;
            this.layoutControlItemUserName.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItemPassword
            // 
            this.layoutControlItemPassword.Control = this.txtPassword;
            resources.ApplyResources(this.layoutControlItemPassword, "layoutControlItemPassword");
            this.layoutControlItemPassword.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemPassword.Name = "layoutControlItemPassword";
            this.layoutControlItemPassword.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItemPassword.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.SupportHorzAlignment;
            this.layoutControlItemPassword.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutSubmit
            // 
            this.layoutSubmit.Control = this.btnSubmit;
            resources.ApplyResources(this.layoutSubmit, "layoutSubmit");
            this.layoutSubmit.Location = new System.Drawing.Point(240, 0);
            this.layoutSubmit.MaxSize = new System.Drawing.Size(52, 52);
            this.layoutSubmit.MinSize = new System.Drawing.Size(52, 52);
            this.layoutSubmit.Name = "layoutSubmit";
            this.layoutSubmit.Size = new System.Drawing.Size(52, 52);
            this.layoutSubmit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutSubmit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutSubmit.TextToControlDistance = 0;
            this.layoutSubmit.TextVisible = false;
            // 
            // layoutExit
            // 
            this.layoutExit.Control = this.btnExit;
            resources.ApplyResources(this.layoutExit, "layoutExit");
            this.layoutExit.Location = new System.Drawing.Point(240, 52);
            this.layoutExit.MaxSize = new System.Drawing.Size(52, 52);
            this.layoutExit.MinSize = new System.Drawing.Size(52, 52);
            this.layoutExit.Name = "layoutExit";
            this.layoutExit.Size = new System.Drawing.Size(52, 53);
            this.layoutExit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutExit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutExit.TextToControlDistance = 0;
            this.layoutExit.TextVisible = false;
            // 
            // layoutServerName
            // 
            this.layoutServerName.Control = this.txtServerName;
            resources.ApplyResources(this.layoutServerName, "layoutServerName");
            this.layoutServerName.Location = new System.Drawing.Point(0, 48);
            this.layoutServerName.Name = "layoutServerName";
            this.layoutServerName.Size = new System.Drawing.Size(240, 24);
            this.layoutServerName.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutServerPort
            // 
            this.layoutServerPort.Control = this.txtServerPort;
            resources.ApplyResources(this.layoutServerPort, "layoutServerPort");
            this.layoutServerPort.Location = new System.Drawing.Point(0, 72);
            this.layoutServerPort.Name = "layoutServerPort";
            this.layoutServerPort.Size = new System.Drawing.Size(169, 33);
            this.layoutServerPort.TextSize = new System.Drawing.Size(86, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem1, "emptySpaceItem1");
            this.emptySpaceItem1.Location = new System.Drawing.Point(169, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(71, 33);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LoginForm3
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "LoginForm3";
            this.ShowInTaskbar = false;
            this.Activated += new System.EventHandler(this.LoginForm_Activated);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.txtUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSubmit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtUser;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPassword;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraLayout.LayoutControlItem layoutExit;
        private DevExpress.XtraEditors.SimpleButton btnSubmit;
        private DevExpress.XtraLayout.LayoutControlItem layoutSubmit;
        private DevExpress.XtraEditors.TextEdit txtServerName;
        private DevExpress.XtraLayout.LayoutControlItem layoutServerName;
        private DevExpress.XtraEditors.SpinEdit txtServerPort;
        private DevExpress.XtraLayout.LayoutControlItem layoutServerPort;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}