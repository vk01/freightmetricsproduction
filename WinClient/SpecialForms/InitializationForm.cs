using System;
using System.ComponentModel;
using System.Media;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using Exis.WinClient.ClientsEngineService;
using Exis.WinClient.General;
using Exis.WinClient.ReportsEngineService;

namespace Exis.WinClient
{
    public partial class InitializationForm : Form
    {
        private SoundPlayer Player = new SoundPlayer();

        public InitializationForm()
        {
            InitializeComponent();
        }

        private void InitializationForm_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            CheckConnection();
        
        }


        private void CheckConnection()
        {
            string uri = GeneralSettings.Default.CnMachineName + ":" +
                         (String.IsNullOrEmpty(GeneralSettings.Default.CnPort) ? "9000" : GeneralSettings.Default.CnPort);

            try
            {
                var clientsTcpAddress =
                    new EndpointAddress("net.tcp://" + uri + "/Exis/FreightMetrics/ClientsEngine");
                var reportsTcpAddress =
                    new EndpointAddress("net.tcp://" + uri + "/Exis/FreightMetrics/ReportsEngine");

                var clientsBindingTcp = new NetTcpBinding
                {
                    MaxBufferSize = Int32.MaxValue,
                    MaxReceivedMessageSize = Int32.MaxValue,
                    //OpenTimeout = TimeSpan.FromSeconds(540),//9'
                    //SendTimeout = TimeSpan.FromSeconds(540),
                    //ReceiveTimeout = TimeSpan.FromSeconds(540),
                    //OpenTimeout = TimeSpan.FromSeconds(900),//15'(production)
                    //SendTimeout = TimeSpan.FromSeconds(900),
                    //ReceiveTimeout = TimeSpan.FromSeconds(900),
                    OpenTimeout = TimeSpan.FromSeconds(1800),//30'(test)
                    SendTimeout = TimeSpan.FromSeconds(1800),
                    ReceiveTimeout = TimeSpan.FromSeconds(1800),
                    ReaderQuotas =
                        new XmlDictionaryReaderQuotas
                        {
                            MaxStringContentLength = Int32.MaxValue,
                            MaxArrayLength = Int32.MaxValue
                        }
                };

                clientsBindingTcp.Security.Mode = SecurityMode.None;
                //clientsBindingTcp.Security.Mode = SecurityMode.Message;
                //clientsBindingTcp.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

                clientsBindingTcp.ReliableSession.Enabled = true;
                clientsBindingTcp.ReliableSession.Ordered = true;
                clientsBindingTcp.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

                var clientTcp = new ClientsServiceClient(clientsBindingTcp, clientsTcpAddress);
                var reportsTcp = new ReportsServiceClient(clientsBindingTcp, reportsTcpAddress);


                foreach (OperationDescription op in clientTcp.Endpoint.Contract.Operations)
                {
                    var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                    if (dataContractBehavior != null)
                    {
                        dataContractBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                    }
                }

                foreach (OperationDescription op in reportsTcp.Endpoint.Contract.Operations)
                {
                    var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                    if (dataContractBehavior != null)
                    {
                        dataContractBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                    }
                }

                //clientTcp.ClientCredentials.Windows.ClientCredential.UserName = "elanast";
                //clientTcp.ClientCredentials.Windows.ClientCredential.Password = "elanast";

                clientTcp.BeginPollService(EndPollService,
                                           new ConnectionInfo
                                           {
                                               ClientsServiceClient = clientTcp,
                                               ReportsServiceClient = reportsTcp,
                                               ClientsEndpointAddress = clientsTcpAddress,
                                               ReportsEndpointAddress = reportsTcpAddress,
                                               Binding = clientsBindingTcp
                                           });
            }
            catch (Exception)
            {
                XtraMessageBox.Show(this, Strings.
                        Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_, Strings.Freight_Metrics,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }

        private void EndPollService(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndPollService;
                Invoke(action, ar);
                return;
            }
            ConnectionInfo connectionInfo = (ConnectionInfo)ar.AsyncState;
            Player.Stop();
            Player.Dispose();
            try
            {
                connectionInfo.ClientsServiceClient.EndPollService(ar);
                SessionRegistry.Client = connectionInfo.ClientsServiceClient;
                SessionRegistry.Reports = connectionInfo.ReportsServiceClient;

                SessionRegistry.ClientBinding = connectionInfo.Binding;
                SessionRegistry.ReportsBinding = connectionInfo.Binding;
                SessionRegistry.ClientEndpointAddress = connectionInfo.ClientsEndpointAddress;
                SessionRegistry.ReportsEndpointAddress = connectionInfo.ReportsEndpointAddress;

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                XtraMessageBox.Show(this, Strings.
                                              Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }
    }

    internal class ConnectionInfo
    {

        internal ClientsServiceClient ClientsServiceClient;
        internal ReportsServiceClient ReportsServiceClient;
        internal EndpointAddress ClientsEndpointAddress;
        internal EndpointAddress ReportsEndpointAddress;
        internal System.ServiceModel.Channels.Binding Binding;
    }
}