﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Exis.WinClient.SpecialForms
{
    public class WaitDialogForm : XtraForm
    {
        private string caption = "";
        private LabelControl labelControl1;

        public WaitDialogForm(XtraForm parent)
        {
            InitializeComponent();
            FormBorderStyle = (this).FormBorderStyle;
            ControlBox = false;
                StartPosition = FormStartPosition.CenterScreen;
            ShowInTaskbar = false;
            TopMost = true;
            Show(parent);
            Refresh();
        }

        public override bool AllowFormSkin
        {
            get { return false; }
        }

        public string Caption
        {
            get { return caption; }
            set
            {
                caption = value;
                Refresh();
            }
        }

        public string GetCaption()
        {
            return Caption;
        }

        public void SetCaption(string newCaption)
        {
            Caption = newCaption;
        }

        private void InitializeComponent()
        {
            labelControl1 = new LabelControl();
            SuspendLayout();
            // 
            // labelControl1
            // 
            labelControl1.Appearance.Font = new Font("Tahoma", 15.75F, FontStyle.Bold, GraphicsUnit.Point, ((161)));
            labelControl1.AutoSizeMode = LabelAutoSizeMode.None;
            labelControl1.Location = new Point(76, 12);
            labelControl1.Name = "labelControl1";
            labelControl1.Size = new Size(118, 58);
            labelControl1.TabIndex = 0;
            labelControl1.Text = "Working...";
            // 
            // WaitDialogForm
            // 
            ClientSize = new Size(258, 88);
            Controls.Add(labelControl1);
            Name = "WaitDialogForm";
            ResumeLayout(false);
        }
    }
}