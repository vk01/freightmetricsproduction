﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Description;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.Domain;
using Exis.WinClient.ClientsEngineService;
using Exis.WinClient.General;
using Exis.WinClient.Resources;
using Exis.WinClient.ReportsEngineService;
using System.ServiceModel;
using System.Xml;
using System.Media;

namespace Exis.WinClient.SpecialForms
{
    public partial class LoginForm : XtraForm
    {
        #region Private Properties

        private WaitDialogForm _dialogForm;
        private SoundPlayer Player = new SoundPlayer();
        private MainForm _mainForm= new MainForm();
        private bool _main_start;

        private bool _serverPortValid = false;
        #endregion

        #region Constructors

        public LoginForm()
        {
            InitializeComponent();
            Icon = ComponentImages.login16x16;
            LoadSettings();

            _mainForm.OnLogout += OnLogout;
            _main_start = true;
        }

        private void LoadSettings()
        {
            txtServerName.Text = GeneralSettings.Default.CnMachineName;
            txtServerPort.Value = String.IsNullOrEmpty(GeneralSettings.Default.CnPort)
                                      ? 9000
                                      : Convert.ToInt32(GeneralSettings.Default.CnPort);
            
            //for test
            //txtServerName.Text = "192.168.12.165";
            //txtServerName.Properties.ReadOnly = true;
            //txtServerPort.Value = 9000;
            //txtServerPort.Properties.ReadOnly = true;

        }

        private void SaveSettings()
        {
            GeneralSettings.Default.CnMachineName = txtServerName.Text.Trim();
            GeneralSettings.Default.CnPort = txtServerPort.Value.ToString();
            GeneralSettings.Default.Save();
        }
        #endregion

        #region GUI Events

        private void LoginForm_Activated(object sender, EventArgs e)
        {
            txtUser.Focus();
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSubmit.PerformClick();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Escape)
            {
                btnExit.PerformClick();
                e.Handled = true;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            CheckConnection();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void CheckConnection()
        {
            string uri = txtServerName.Text.Trim() + ":" + txtServerPort.Value;
            var clientsTcpAddress =
                new EndpointAddress("net.tcp://" + uri + "/Exis/FreightMetrics/ClientsEngine");

            var clientsBindingTcp = new NetTcpBinding()
            {
                OpenTimeout = TimeSpan.FromSeconds(10),
                SendTimeout = TimeSpan.FromSeconds(10),
                ReceiveTimeout = TimeSpan.FromSeconds(10),
            };

            clientsBindingTcp.Security.Mode = SecurityMode.None;
            //clientsBindingTcp.Security.Mode = SecurityMode.Message;
            //clientsBindingTcp.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            var clientTcp = new ClientsServiceClient(clientsBindingTcp, clientsTcpAddress);

            clientsBindingTcp.ReliableSession.Enabled = true;

            //clientTcp.ClientCredentials.Windows.ClientCredential.UserName = "elanast";
            //clientTcp.ClientCredentials.Windows.ClientCredential.Password = "elanast";

            try
            {
                Cursor = Cursors.WaitCursor;
                _dialogForm = new WaitDialogForm(this);
                clientTcp.BeginPollService(EndPollService, clientTcp);
            }
            catch (Exception)
            {
                _dialogForm.Close();
                _dialogForm = null;
                XtraMessageBox.Show(this, Strings.
                                              Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CreateConnection()
        {
            string uri = GeneralSettings.Default.CnMachineName + ":" +
                         (String.IsNullOrEmpty(GeneralSettings.Default.CnPort) ? "9000" : GeneralSettings.Default.CnPort);

            try
            {
                var clientsTcpAddress =
                    new EndpointAddress("net.tcp://" + uri + "/Exis/FreightMetrics/ClientsEngine");
                var reportsTcpAddress =
                    new EndpointAddress("net.tcp://" + uri + "/Exis/FreightMetrics/ReportsEngine");

                var clientsBindingTcp = new NetTcpBinding
                {
                    MaxBufferSize = Int32.MaxValue,
                    MaxReceivedMessageSize = Int32.MaxValue,
                    OpenTimeout = TimeSpan.FromSeconds(540),//9'
                    SendTimeout = TimeSpan.FromSeconds(540),
                    ReceiveTimeout = TimeSpan.FromSeconds(540),
                    ReaderQuotas =
                        new XmlDictionaryReaderQuotas
                        {
                            MaxStringContentLength = Int32.MaxValue,
                            MaxArrayLength = Int32.MaxValue
                        }
                };

                clientsBindingTcp.Security.Mode = SecurityMode.None;
                //clientsBindingTcp.Security.Mode = SecurityMode.Message;
                //clientsBindingTcp.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

                clientsBindingTcp.ReliableSession.Enabled = true;
                clientsBindingTcp.ReliableSession.Ordered = true;
                clientsBindingTcp.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

                var clientTcp = new ClientsServiceClient(clientsBindingTcp, clientsTcpAddress);
                var reportsTcp = new ReportsServiceClient(clientsBindingTcp, reportsTcpAddress);


                foreach (OperationDescription op in clientTcp.Endpoint.Contract.Operations)
                {
                    var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                    if (dataContractBehavior != null)
                    {
                        dataContractBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                    }
                }

                foreach (OperationDescription op in reportsTcp.Endpoint.Contract.Operations)
                {
                    var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                    if (dataContractBehavior != null)
                    {
                        dataContractBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                    }
                }

                //clientTcp.ClientCredentials.Windows.ClientCredential.UserName = "elanast";
                //clientTcp.ClientCredentials.Windows.ClientCredential.Password = "elanast";

                clientTcp.BeginPollService(EndPollService,
                                           new ConnectionInfo
                                           {
                                               ClientsServiceClient = clientTcp,
                                               ReportsServiceClient = reportsTcp,
                                               ClientsEndpointAddress = clientsTcpAddress,
                                               ReportsEndpointAddress = reportsTcpAddress,
                                               Binding = clientsBindingTcp
                                           });
            }
            catch (Exception)
            {
                XtraMessageBox.Show(this, Strings.
                        Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_, Strings.Freight_Metrics,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                //DialogResult = DialogResult.Cancel;
                //Close();
            }
        }

        private void EndPollService(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndPollService;
                Invoke(action, ar);
                return;
            }

            if (_serverPortValid)
            {
                _dialogForm.Close();
                _dialogForm = null;

                ConnectionInfo connectionInfo = (ConnectionInfo) ar.AsyncState;
                Player.Stop();
                Player.Dispose();
                try
                {
                    connectionInfo.ClientsServiceClient.EndPollService(ar);
                    SessionRegistry.Client = connectionInfo.ClientsServiceClient;
                    SessionRegistry.Reports = connectionInfo.ReportsServiceClient;

                    SessionRegistry.ClientBinding = connectionInfo.Binding;
                    SessionRegistry.ReportsBinding = connectionInfo.Binding;
                    SessionRegistry.ClientEndpointAddress = connectionInfo.ClientsEndpointAddress;
                    SessionRegistry.ReportsEndpointAddress = connectionInfo.ReportsEndpointAddress;

                    BeginValidateCredentials();
                    _serverPortValid = false;
                    /* DialogResult = DialogResult.OK;
                    Close();*/
                }
                catch (Exception)
                {
                    XtraMessageBox.Show(this, Strings.
                                                  Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //DialogResult = DialogResult.Cancel;
                    //Close();
                }
            }else
            {
                ClientsServiceClient clientTcp = (ClientsServiceClient)ar.AsyncState;
                Cursor = Cursors.Default;
                try
                {
                    clientTcp.EndPollService(ar);
                    clientTcp.Close();
                    SaveSettings();
                    /*_dialogForm.Close();
                    _dialogForm = null;*/

                    _serverPortValid = true;

                   /* DialogResult = DialogResult.OK;
                    Close();*/
                    CreateConnection();
                }
                catch (Exception)
                {
                    _dialogForm.Close();
                    _dialogForm = null;
                    XtraMessageBox.Show(this, Strings.
                                                  Either_there_is_a_problem_connecting_to_the_server_or_the_provided_connection_string_does_not_seem_to_be_valid_,
                                        Strings.Freight_Metrics,
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion

        #region Form Controls Handlers

        #endregion

        #region Proxy Calls

        private void BeginValidateCredentials()
        {
            Cursor = Cursors.WaitCursor;
            _dialogForm = new WaitDialogForm(this);

            try
            {
                SessionRegistry.Client.BeginValidateCredentials(txtUser.Text.Trim(), txtPassword.Text.Trim(),
                                                                EndValidateCredentials, null);
            }
            catch (Exception)
            {
                _dialogForm.Close();
                _dialogForm = null;
                SessionRegistry.ResetClientService();
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_communicating_with_the_server__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EndValidateCredentials(IAsyncResult ar)
        {
            if (InvokeRequired)
            {
                Action<IAsyncResult> action = EndValidateCredentials;
                Invoke(action, ar);
                return;
            }

            User user;

            int? result = SessionRegistry.Client.EndValidateCredentials(out user, ar);
            Cursor = Cursors.Default;
            _dialogForm.Close();
            _dialogForm = null;

            if (result == null) //Exception Occurred
            {
                // Form Exception
                XtraMessageBox.Show(this,
                                    Strings.
                                        There_was_an_error_during_your_request__Please_try_again__If_the_problem_persists__contact_support_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result == 1)
            {
                XtraMessageBox.Show(this, Strings.User_Credentials_are_not_validated__Please_try_again_,
                                    Strings.Freight_Metrics,
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (result == 0) //everything is OK
            {
                SessionRegistry.AppUser = user;
                SessionRegistry.ProgramInfo.UserName = user.Login;
                SessionRegistry.UserHasPermissions = new List<string>();
                SessionRegistry.UserHasNotPermissions = new List<string>();
                SessionRegistry.Client.Close();
                SessionRegistry.Client = new ClientsServiceClient(SessionRegistry.ClientBinding,
                                                                  SessionRegistry.ClientEndpointAddress);

                foreach (OperationDescription op in SessionRegistry.Client.Endpoint.Contract.Operations)
                {
                    var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                    if (dataContractBehavior != null)
                    {
                        dataContractBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                    }
                }
                DialogResult = DialogResult.OK;

                //this.Hide();

                //var mainForm = new MainForm();
                //_mainForm.OnLogout += OnLogout;
                //try
                //{
                //    if (_mainForm == null && !_main_start)
                //    {
                //        _mainForm = new MainForm();
                //        _mainForm.OnLogout += OnLogout;
                //        _mainForm.Show();
                //    }
                //    else
                //    {
                //        XtraMessageBox.Show("test");
                //        _mainForm.Show();
                //    }
                //}
                //catch (Exception e)
                //{

                //}
            }
        }

        #endregion

        private void OnLogout()
        {
            _main_start = false;
            
            //_mainForm.Hide();
            _mainForm.Close();
            _mainForm = null;
            
            this.ShowDialog();
        }
    }

    internal class ConnectionInfo
    {

        internal ClientsServiceClient ClientsServiceClient;
        internal ReportsServiceClient ReportsServiceClient;
        internal EndpointAddress ClientsEndpointAddress;
        internal EndpointAddress ReportsEndpointAddress;
        internal System.ServiceModel.Channels.Binding Binding;
    }
}