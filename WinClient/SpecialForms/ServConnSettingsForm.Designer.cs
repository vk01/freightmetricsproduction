﻿namespace Exis.WinClient
{
    partial class ServConnSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServConnSettingsForm));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.imageList24 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnSubmit = new DevExpress.XtraEditors.SimpleButton();
            this.txtServerPort = new DevExpress.XtraEditors.SpinEdit();
            this.txtServerName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutServerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServerPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSubmit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExit = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSubmit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExit)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnExit);
            this.layoutControl1.Controls.Add(this.btnSubmit);
            this.layoutControl1.Controls.Add(this.txtServerPort);
            this.layoutControl1.Controls.Add(this.txtServerName);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // btnExit
            // 
            this.btnExit.ImageIndex = 1;
            this.btnExit.ImageList = this.imageList24;
            this.btnExit.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            resources.ApplyResources(this.btnExit, "btnExit");
            this.btnExit.Name = "btnExit";
            this.btnExit.StyleController = this.layoutControl1;
            this.btnExit.TabStop = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // imageList24
            // 
            resources.ApplyResources(this.imageList24, "imageList24");
            this.imageList24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageList24.ImageStream")));
            this.imageList24.Images.SetKeyName(0, "event32x32.ico");
            this.imageList24.Images.SetKeyName(1, "exit.ico");
            // 
            // btnSubmit
            // 
            this.btnSubmit.ImageIndex = 0;
            this.btnSubmit.ImageList = this.imageList24;
            this.btnSubmit.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            resources.ApplyResources(this.btnSubmit, "btnSubmit");
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.StyleController = this.layoutControl1;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtServerPort
            // 
            resources.ApplyResources(this.txtServerPort, "txtServerPort");
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtServerPort.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.txtServerPort.Properties.MinValue = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.txtServerPort.StyleController = this.layoutControl1;
            // 
            // txtServerName
            // 
            resources.ApplyResources(this.txtServerName, "txtServerName");
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.StyleController = this.layoutControl1;
            // 
            // layoutControlGroup1
            // 
            resources.ApplyResources(this.layoutControlGroup1, "layoutControlGroup1");
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutServerName,
            this.layoutServerPort,
            this.layoutSubmit,
            this.layoutExit});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(367, 124);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutServerName
            // 
            this.layoutServerName.Control = this.txtServerName;
            resources.ApplyResources(this.layoutServerName, "layoutServerName");
            this.layoutServerName.Location = new System.Drawing.Point(0, 0);
            this.layoutServerName.MaxSize = new System.Drawing.Size(294, 24);
            this.layoutServerName.MinSize = new System.Drawing.Size(294, 24);
            this.layoutServerName.Name = "layoutServerName";
            this.layoutServerName.Size = new System.Drawing.Size(294, 24);
            this.layoutServerName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutServerName.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutServerPort
            // 
            this.layoutServerPort.Control = this.txtServerPort;
            resources.ApplyResources(this.layoutServerPort, "layoutServerPort");
            this.layoutServerPort.Location = new System.Drawing.Point(0, 24);
            this.layoutServerPort.MaxSize = new System.Drawing.Size(144, 24);
            this.layoutServerPort.MinSize = new System.Drawing.Size(144, 24);
            this.layoutServerPort.Name = "layoutServerPort";
            this.layoutServerPort.Size = new System.Drawing.Size(294, 80);
            this.layoutServerPort.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutServerPort.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutSubmit
            // 
            this.layoutSubmit.Control = this.btnSubmit;
            resources.ApplyResources(this.layoutSubmit, "layoutSubmit");
            this.layoutSubmit.Location = new System.Drawing.Point(294, 0);
            this.layoutSubmit.MaxSize = new System.Drawing.Size(52, 52);
            this.layoutSubmit.MinSize = new System.Drawing.Size(52, 52);
            this.layoutSubmit.Name = "layoutSubmit";
            this.layoutSubmit.Size = new System.Drawing.Size(53, 52);
            this.layoutSubmit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutSubmit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutSubmit.TextToControlDistance = 0;
            this.layoutSubmit.TextVisible = false;
            // 
            // layoutExit
            // 
            this.layoutExit.Control = this.btnExit;
            resources.ApplyResources(this.layoutExit, "layoutExit");
            this.layoutExit.Location = new System.Drawing.Point(294, 52);
            this.layoutExit.MaxSize = new System.Drawing.Size(52, 52);
            this.layoutExit.MinSize = new System.Drawing.Size(52, 52);
            this.layoutExit.Name = "layoutExit";
            this.layoutExit.Size = new System.Drawing.Size(53, 52);
            this.layoutExit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutExit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutExit.TextToControlDistance = 0;
            this.layoutExit.TextVisible = false;
            // 
            // ServConnSettingsForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ServConnSettingsForm";
            this.ShowInTaskbar = false;
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageList24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServerPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSubmit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtServerName;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutServerName;
        private DevExpress.XtraEditors.SpinEdit txtServerPort;
        private DevExpress.XtraLayout.LayoutControlItem layoutServerPort;
        private DevExpress.Utils.ImageCollection imageList24;
        private DevExpress.XtraEditors.SimpleButton btnSubmit;
        private DevExpress.XtraLayout.LayoutControlItem layoutSubmit;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraLayout.LayoutControlItem layoutExit;
    }
}