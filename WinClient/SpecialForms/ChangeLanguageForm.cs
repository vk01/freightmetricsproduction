﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Exis.WinClient.Resources;

namespace Exis.WinClient.SpecialForms
{
    public partial class ChangeLanguageForm : Form
    {
        public ChangeLanguageForm()
        {
            InitializeComponent();
            Icon = ComponentImages.changeLanguage16x16;

            radioGroup.SelectedIndex = radioGroup.Properties.Items.GetItemIndexByValue("English");
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            GeneralSettings.Default.CnGUILanguage = radioGroup.SelectedIndex == radioGroup.Properties.Items.GetItemIndexByValue("English") ? Convert.ToString("EN") : Convert.ToString("GR");
            GeneralSettings.Default.Save();
            DialogResult = DialogResult.OK;
            Close();
        }

    }
}