﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Exis.WinClient.Resources;

namespace Exis.WinClient.SpecialForms
{
    public partial class AboutForm : DevExpress.XtraEditors.XtraForm
    {
        public AboutForm()
        {
            InitializeComponent();

            Icon = ComponentImages.about16x16;
            btnOK.Image = ComponentImages.ok24x24.ToBitmap();

            //Version vers = new Version(Application.ProductVersion);
            //lblVersion.Text = "Version: " + vers.Build;
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                if (ad != null)
                    lblVersion.Text = "Version: " + ad.CurrentVersion.ToString();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}