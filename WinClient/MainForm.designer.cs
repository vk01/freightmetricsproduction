﻿namespace Exis.WinClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.bmnuViewTrades = new DevExpress.XtraBars.BarButtonItem();
            this.bsmiEntities = new DevExpress.XtraBars.BarSubItem();
            this.bmnuCompanies = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuTraders = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuVesselPools = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuBooks = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuBankAccounts = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuProfitCenters = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuDesks = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuClearingHouses = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuIndexes = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuContacts = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuLoansManagement = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuHierarchyManagement = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuInterestRefRates = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuExchangeRates = new DevExpress.XtraBars.BarButtonItem();
            this.bsmiImport = new DevExpress.XtraBars.BarSubItem();
            this.bmnuBalticInputData = new DevExpress.XtraBars.BarButtonItem();
            this.bsmiMonteCarlo = new DevExpress.XtraBars.BarSubItem();
            this.bmnuRiskManagement = new DevExpress.XtraBars.BarButtonItem();
            this.de = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuUsersManagement = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuApplication = new DevExpress.XtraBars.BarSubItem();
            this.bmnuSettings = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuApplicationParametersManagement = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuChangeUILanguage = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuChangePassword = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuHelp = new DevExpress.XtraBars.BarSubItem();
            this.bmnuAbout = new DevExpress.XtraBars.BarButtonItem();
            this.imageCollection4BarManger = new DevExpress.Utils.ImageCollection(this.components);
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem6 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageCollection24 = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuAddTCTrade = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuAddFFATrade = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuAddCargoTrade = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuAddOptionTrade = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuMCSimulations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuImportTrades = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.bmnuLogIn = new DevExpress.XtraBars.BarButtonItem();
            this.LargeImageCollection_32 = new DevExpress.Utils.ImageCollection(this.components);
            this.rpMainMenu = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpEntitiesManagement = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpApplicationSettings = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            ((System.ComponentModel.ISupportInitialize)(this.mdiManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection4BarManger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LargeImageCollection_32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // mdiManager
            // 
            this.mdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.mdiManager.MdiParent = this;
            this.mdiManager.PageRemoved += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.MdiManagerPageRemoved);
            // 
            // bmnuViewTrades
            // 
            this.bmnuViewTrades.Caption = "View Trades";
            this.bmnuViewTrades.Id = 59;
            this.bmnuViewTrades.LargeImageIndex = 0;
            this.bmnuViewTrades.Name = "bmnuViewTrades";
            this.bmnuViewTrades.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BmnuViewTradesClick);
            // 
            // bsmiEntities
            // 
            this.bsmiEntities.Caption = "Entities";
            this.bsmiEntities.Id = 23;
            this.bsmiEntities.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuCompanies),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuTraders),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuVesselPools),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuBooks),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuBankAccounts),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuProfitCenters),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuDesks),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuClearingHouses),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuIndexes),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuContacts),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuLoansManagement),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuHierarchyManagement),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuInterestRefRates),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuExchangeRates)});
            this.bsmiEntities.Name = "bsmiEntities";
            // 
            // bmnuCompanies
            // 
            this.bmnuCompanies.Caption = "Companies";
            this.bmnuCompanies.Id = 35;
            this.bmnuCompanies.ImageIndex = 10;
            this.bmnuCompanies.Name = "bmnuCompanies";
            this.bmnuCompanies.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuCompaniesClick);
            // 
            // bmnuTraders
            // 
            this.bmnuTraders.Caption = "Traders";
            this.bmnuTraders.Id = 36;
            this.bmnuTraders.ImageIndex = 0;
            this.bmnuTraders.Name = "bmnuTraders";
            this.bmnuTraders.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuTradersClick);
            // 
            // bmnuVesselPools
            // 
            this.bmnuVesselPools.Caption = "Vessel Pools";
            this.bmnuVesselPools.Id = 38;
            this.bmnuVesselPools.LargeImageIndex = 15;
            this.bmnuVesselPools.Name = "bmnuVesselPools";
            this.bmnuVesselPools.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuVesselPoolsClick);
            // 
            // bmnuBooks
            // 
            this.bmnuBooks.Caption = "Books";
            this.bmnuBooks.Id = 39;
            this.bmnuBooks.ImageIndex = 9;
            this.bmnuBooks.Name = "bmnuBooks";
            this.bmnuBooks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuBooksClick);
            // 
            // bmnuBankAccounts
            // 
            this.bmnuBankAccounts.Caption = "Bank Accounts";
            this.bmnuBankAccounts.Id = 40;
            this.bmnuBankAccounts.ImageIndex = 4;
            this.bmnuBankAccounts.Name = "bmnuBankAccounts";
            this.bmnuBankAccounts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuBankAccountsClick);
            // 
            // bmnuProfitCenters
            // 
            this.bmnuProfitCenters.Caption = "Profit Centers";
            this.bmnuProfitCenters.Id = 41;
            this.bmnuProfitCenters.ImageIndex = 1;
            this.bmnuProfitCenters.Name = "bmnuProfitCenters";
            this.bmnuProfitCenters.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuProfitCentersClick);
            // 
            // bmnuDesks
            // 
            this.bmnuDesks.Caption = "Desks";
            this.bmnuDesks.Id = 42;
            this.bmnuDesks.ImageIndex = 2;
            this.bmnuDesks.Name = "bmnuDesks";
            this.bmnuDesks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuDesksClick);
            // 
            // bmnuClearingHouses
            // 
            this.bmnuClearingHouses.Caption = "Clearing Houses";
            this.bmnuClearingHouses.Id = 43;
            this.bmnuClearingHouses.ImageIndex = 6;
            this.bmnuClearingHouses.Name = "bmnuClearingHouses";
            this.bmnuClearingHouses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuClearingHousesClick);
            // 
            // bmnuIndexes
            // 
            this.bmnuIndexes.Caption = "Indices";
            this.bmnuIndexes.Id = 45;
            this.bmnuIndexes.LargeImageIndex = 7;
            this.bmnuIndexes.Name = "bmnuIndexes";
            this.bmnuIndexes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuIndexesClick);
            // 
            // bmnuContacts
            // 
            this.bmnuContacts.Caption = "Contacts";
            this.bmnuContacts.Id = 46;
            this.bmnuContacts.LargeImageIndex = 9;
            this.bmnuContacts.Name = "bmnuContacts";
            this.bmnuContacts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuContactsClick);
            // 
            // bmnuLoansManagement
            // 
            this.bmnuLoansManagement.Caption = "Loans";
            this.bmnuLoansManagement.Id = 47;
            this.bmnuLoansManagement.ImageIndex = 3;
            this.bmnuLoansManagement.Name = "bmnuLoansManagement";
            this.bmnuLoansManagement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuLoansManagementClick);
            // 
            // bmnuHierarchyManagement
            // 
            this.bmnuHierarchyManagement.Caption = "Entities Hierarchy";
            this.bmnuHierarchyManagement.Id = 48;
            this.bmnuHierarchyManagement.LargeImageIndex = 6;
            this.bmnuHierarchyManagement.Name = "bmnuHierarchyManagement";
            this.bmnuHierarchyManagement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuHierarchyManagementClick);
            // 
            // bmnuInterestRefRates
            // 
            this.bmnuInterestRefRates.Caption = "Interest Ref. Rates";
            this.bmnuInterestRefRates.Id = 49;
            this.bmnuInterestRefRates.ImageIndex = 8;
            this.bmnuInterestRefRates.Name = "bmnuInterestRefRates";
            this.bmnuInterestRefRates.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuInterestRefRatesClick);
            // 
            // bmnuExchangeRates
            // 
            this.bmnuExchangeRates.Caption = "Exchange Rates";
            this.bmnuExchangeRates.Id = 50;
            this.bmnuExchangeRates.ImageIndex = 7;
            this.bmnuExchangeRates.Name = "bmnuExchangeRates";
            this.bmnuExchangeRates.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuExchangeRatesClick);
            // 
            // bsmiImport
            // 
            this.bsmiImport.Caption = "Import";
            this.bsmiImport.Id = 24;
            this.bsmiImport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuBalticInputData)});
            this.bsmiImport.Name = "bsmiImport";
            // 
            // bmnuBalticInputData
            // 
            this.bmnuBalticInputData.Caption = "Download && Import Data";
            this.bmnuBalticInputData.Id = 57;
            this.bmnuBalticInputData.LargeImageIndex = 3;
            this.bmnuBalticInputData.Name = "bmnuBalticInputData";
            this.bmnuBalticInputData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuBalticInputDataClick);
            // 
            // bsmiMonteCarlo
            // 
            this.bsmiMonteCarlo.Caption = "Monte Carlo";
            this.bsmiMonteCarlo.Id = 61;
            this.bsmiMonteCarlo.Name = "bsmiMonteCarlo";
            // 
            // bmnuRiskManagement
            // 
            this.bmnuRiskManagement.Caption = "Paremeterize Risk Management";
            this.bmnuRiskManagement.Id = 31;
            this.bmnuRiskManagement.LargeImageIndex = 4;
            this.bmnuRiskManagement.Name = "bmnuRiskManagement";
            this.bmnuRiskManagement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuRiskManagementClick);
            // 
            // de
            // 
            this.de.Caption = "Cash Flow Models";
            this.de.Id = 32;
            this.de.LargeImageIndex = 5;
            this.de.Name = "de";
            this.de.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuCashFlowClick);
            // 
            // bmnuUsersManagement
            // 
            this.bmnuUsersManagement.Caption = "Users";
            this.bmnuUsersManagement.Id = 33;
            this.bmnuUsersManagement.LargeImageIndex = 10;
            this.bmnuUsersManagement.Name = "bmnuUsersManagement";
            this.bmnuUsersManagement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuUsersManagementClick);
            // 
            // bmnuApplication
            // 
            this.bmnuApplication.Caption = "Application";
            this.bmnuApplication.Id = 0;
            this.bmnuApplication.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuSettings),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuApplicationParametersManagement),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuChangeUILanguage),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuChangePassword)});
            this.bmnuApplication.Name = "bmnuApplication";
            // 
            // bmnuSettings
            // 
            this.bmnuSettings.Caption = "Connection Settings";
            this.bmnuSettings.Id = 5;
            this.bmnuSettings.LargeImageIndex = 11;
            this.bmnuSettings.Name = "bmnuSettings";
            this.bmnuSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BmnuSettingsItemClick);
            // 
            // bmnuApplicationParametersManagement
            // 
            this.bmnuApplicationParametersManagement.Caption = "Application Parameters";
            this.bmnuApplicationParametersManagement.Id = 29;
            this.bmnuApplicationParametersManagement.LargeImageIndex = 12;
            this.bmnuApplicationParametersManagement.Name = "bmnuApplicationParametersManagement";
            this.bmnuApplicationParametersManagement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuApplicationParametersManagementClick);
            // 
            // bmnuChangeUILanguage
            // 
            this.bmnuChangeUILanguage.Caption = "Change UI Language";
            this.bmnuChangeUILanguage.Id = 3;
            this.bmnuChangeUILanguage.LargeImageIndex = 13;
            this.bmnuChangeUILanguage.Name = "bmnuChangeUILanguage";
            this.bmnuChangeUILanguage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bmnuChangeUILanguage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BmnuChangeUiLanguageItemClick);
            // 
            // bmnuChangePassword
            // 
            this.bmnuChangePassword.Caption = "Change Password";
            this.bmnuChangePassword.Enabled = false;
            this.bmnuChangePassword.Id = 4;
            this.bmnuChangePassword.ImageIndex = 3;
            this.bmnuChangePassword.Name = "bmnuChangePassword";
            this.bmnuChangePassword.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BmnuChangePasswordItemClick);
            // 
            // bmnuHelp
            // 
            this.bmnuHelp.Caption = "Help";
            this.bmnuHelp.Id = 7;
            this.bmnuHelp.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmnuAbout)});
            this.bmnuHelp.Name = "bmnuHelp";
            // 
            // bmnuAbout
            // 
            this.bmnuAbout.Caption = "About";
            this.bmnuAbout.Id = 8;
            this.bmnuAbout.ImageIndex = 12;
            this.bmnuAbout.Name = "bmnuAbout";
            this.bmnuAbout.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bmnuAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BmnuAboutItemClick);
            // 
            // imageCollection4BarManger
            // 
            this.imageCollection4BarManger.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection4BarManger.ImageStream")));
            this.imageCollection4BarManger.Images.SetKeyName(0, "login16x16.ico");
            this.imageCollection4BarManger.Images.SetKeyName(1, "logout16x16.ico");
            this.imageCollection4BarManger.Images.SetKeyName(2, "changeLanguage16x16.ico");
            this.imageCollection4BarManger.Images.SetKeyName(3, "key1_edit.png");
            this.imageCollection4BarManger.Images.SetKeyName(4, "settings16x16.ico");
            this.imageCollection4BarManger.Images.SetKeyName(5, "about16x16.ico");
            this.imageCollection4BarManger.Images.SetKeyName(6, "help16x16.ico");
            this.imageCollection4BarManger.Images.SetKeyName(7, "loan_16x16.png");
            this.imageCollection4BarManger.Images.SetKeyName(8, "bank_accounts_24x24.png");
            this.imageCollection4BarManger.Images.SetKeyName(9, "measure_16x16.png");
            this.imageCollection4BarManger.Images.SetKeyName(10, "trader_16x16.png");
            this.imageCollection4BarManger.Images.SetKeyName(11, "addItem24x24.ico");
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Risk Management";
            this.barSubItem3.Id = 25;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Cash Flows";
            this.barSubItem4.Id = 26;
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Users";
            this.barSubItem5.Id = 27;
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barSubItem6
            // 
            this.barSubItem6.Caption = "Logout";
            this.barSubItem6.Id = 28;
            this.barSubItem6.Name = "barSubItem6";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 30;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Logout";
            this.barButtonItem6.Id = 34;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "Trades";
            this.barButtonItem23.Id = 51;
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Monte Carlo";
            this.barButtonItem1.Id = 60;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Images = this.imageCollection24;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bmnuApplication,
            this.bmnuChangeUILanguage,
            this.bmnuChangePassword,
            this.bmnuSettings,
            this.bmnuHelp,
            this.bmnuAbout,
            this.bsmiEntities,
            this.bsmiImport,
            this.barSubItem3,
            this.barSubItem4,
            this.barSubItem5,
            this.barSubItem6,
            this.bmnuApplicationParametersManagement,
            this.barButtonItem2,
            this.bmnuRiskManagement,
            this.de,
            this.bmnuUsersManagement,
            this.barButtonItem6,
            this.bmnuCompanies,
            this.bmnuTraders,
            this.bmnuVesselPools,
            this.bmnuBooks,
            this.bmnuBankAccounts,
            this.bmnuProfitCenters,
            this.bmnuDesks,
            this.bmnuClearingHouses,
            this.bmnuIndexes,
            this.bmnuContacts,
            this.bmnuLoansManagement,
            this.bmnuHierarchyManagement,
            this.bmnuInterestRefRates,
            this.bmnuExchangeRates,
            this.barButtonItem23,
            this.bmnuBalticInputData,
            this.bmnuViewTrades,
            this.barButtonItem1,
            this.bsmiMonteCarlo,
            this.barButtonItem3,
            this.barButtonItem4,
            this.bmnuAddTCTrade,
            this.bmnuAddFFATrade,
            this.bmnuAddCargoTrade,
            this.bmnuAddOptionTrade,
            this.bmnuMCSimulations,
            this.barButtonItem5,
            this.bmnuImportTrades,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barButtonItem12,
            this.bmnuLogIn});
            this.ribbon.LargeImages = this.LargeImageCollection_32;
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbon.MaxItemId = 69;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageHeaderItemLinks.Add(this.bmnuAbout);
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMainMenu,
            this.rpEntitiesManagement,
            this.rpApplicationSettings});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1});
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1130, 128);
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageCollection24
            // 
            this.imageCollection24.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection24.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection24.ImageStream")));
            this.imageCollection24.Images.SetKeyName(0, "trader_24x24.png");
            this.imageCollection24.Images.SetKeyName(1, "profit_24x24.png");
            this.imageCollection24.Images.SetKeyName(2, "desks_16x16.png");
            this.imageCollection24.Images.SetKeyName(3, "loan2_24x24.png");
            this.imageCollection24.Images.SetKeyName(4, "bank_account2_24x24.png");
            this.imageCollection24.Images.SetKeyName(5, "bank_accounts_24x24.png");
            this.imageCollection24.Images.SetKeyName(6, "clearing_house_32x32.png");
            this.imageCollection24.Images.SetKeyName(7, "currency_exchange-24x24.png");
            this.imageCollection24.Images.SetKeyName(8, "percentage_24x24.png");
            this.imageCollection24.Images.SetKeyName(9, "books_32x32.png");
            this.imageCollection24.Images.SetKeyName(10, "companies_32x32.png");
            this.imageCollection24.Images.SetKeyName(11, "login_32x32.png");
            this.imageCollection24.Images.SetKeyName(12, "about16x16.ico");
            this.imageCollection24.Images.SetKeyName(13, "addItem24x24.ico");
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 15;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 16;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bmnuAddTCTrade
            // 
            this.bmnuAddTCTrade.Caption = "Add TC Trade";
            this.bmnuAddTCTrade.Id = 17;
            this.bmnuAddTCTrade.ImageIndex = 13;
            this.bmnuAddTCTrade.Name = "bmnuAddTCTrade";
            this.bmnuAddTCTrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuAddTCTradeClick);
            // 
            // bmnuAddFFATrade
            // 
            this.bmnuAddFFATrade.Caption = "Add FFA Trade";
            this.bmnuAddFFATrade.Id = 18;
            this.bmnuAddFFATrade.ImageIndex = 13;
            this.bmnuAddFFATrade.Name = "bmnuAddFFATrade";
            this.bmnuAddFFATrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuAddFFATradeClick);
            // 
            // bmnuAddCargoTrade
            // 
            this.bmnuAddCargoTrade.Caption = "Add Cargo Trade";
            this.bmnuAddCargoTrade.Id = 19;
            this.bmnuAddCargoTrade.ImageIndex = 13;
            this.bmnuAddCargoTrade.Name = "bmnuAddCargoTrade";
            this.bmnuAddCargoTrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuAddCargoTradeClick);
            // 
            // bmnuAddOptionTrade
            // 
            this.bmnuAddOptionTrade.Caption = "Add Option Trade";
            this.bmnuAddOptionTrade.Id = 20;
            this.bmnuAddOptionTrade.ImageIndex = 13;
            this.bmnuAddOptionTrade.Name = "bmnuAddOptionTrade";
            this.bmnuAddOptionTrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuAddOptionTradeClick);
            // 
            // bmnuMCSimulations
            // 
            this.bmnuMCSimulations.Caption = "Monte Carlo Simulations Archive";
            this.bmnuMCSimulations.Id = 53;
            this.bmnuMCSimulations.LargeImageIndex = 2;
            this.bmnuMCSimulations.Name = "bmnuMCSimulations";
            this.bmnuMCSimulations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuMCSimulationsClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 54;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // bmnuImportTrades
            // 
            this.bmnuImportTrades.Caption = "Import from XLS";
            this.bmnuImportTrades.Id = 55;
            this.bmnuImportTrades.LargeImageIndex = 1;
            this.bmnuImportTrades.Name = "bmnuImportTrades";
            this.bmnuImportTrades.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmnuImportTradesClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItem8";
            this.barButtonItem8.Id = 57;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "barButtonItem9";
            this.barButtonItem9.Id = 58;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Login";
            this.barButtonItem10.Id = 63;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Loggout";
            this.barButtonItem11.Id = 64;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Connection Settings";
            this.barButtonItem12.Id = 65;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // bmnuLogIn
            // 
            this.bmnuLogIn.Caption = "Login";
            this.bmnuLogIn.Id = 66;
            this.bmnuLogIn.LargeImageIndex = 14;
            this.bmnuLogIn.Name = "bmnuLogIn";
            // 
            // LargeImageCollection_32
            // 
            this.LargeImageCollection_32.ImageSize = new System.Drawing.Size(32, 32);
            this.LargeImageCollection_32.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("LargeImageCollection_32.ImageStream")));
            this.LargeImageCollection_32.Images.SetKeyName(0, "Candles_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(1, "excel_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(2, "archive_32.png");
            this.LargeImageCollection_32.Images.SetKeyName(3, "download_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(4, "gauge_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(5, "cubes.png");
            this.LargeImageCollection_32.Images.SetKeyName(6, "hierarchy_32_32.png");
            this.LargeImageCollection_32.Images.SetKeyName(7, "index_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(8, "clearing_house_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(9, "Contact_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(10, "users_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(11, "connection_settings_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(12, "Application_Parameters.png");
            this.LargeImageCollection_32.Images.SetKeyName(13, "changeLanguage16x16.ico");
            this.LargeImageCollection_32.Images.SetKeyName(14, "login1_32x32.png");
            this.LargeImageCollection_32.Images.SetKeyName(15, "vessel_32x32.png");
            // 
            // rpMainMenu
            // 
            this.rpMainMenu.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3});
            this.rpMainMenu.Name = "rpMainMenu";
            this.rpMainMenu.Text = "Main Menu";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.bmnuViewTrades);
            this.ribbonPageGroup1.ItemLinks.Add(this.bmnuAddTCTrade, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.bmnuAddFFATrade);
            this.ribbonPageGroup1.ItemLinks.Add(this.bmnuAddCargoTrade, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.bmnuAddOptionTrade);
            this.ribbonPageGroup1.ItemLinks.Add(this.bmnuImportTrades);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Trades";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.bmnuBalticInputData, true);
            this.ribbonPageGroup3.ItemLinks.Add(this.bmnuMCSimulations);
            this.ribbonPageGroup3.ItemLinks.Add(this.bmnuRiskManagement);
            this.ribbonPageGroup3.ItemLinks.Add(this.de);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Operations";
            // 
            // rpEntitiesManagement
            // 
            this.rpEntitiesManagement.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup8});
            this.rpEntitiesManagement.Name = "rpEntitiesManagement";
            this.rpEntitiesManagement.Text = "Entities Management";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuHierarchyManagement, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuIndexes, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuBooks, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuCompanies);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuTraders, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuDesks);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuProfitCenters);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuClearingHouses, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuBankAccounts);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuLoansManagement);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuInterestRefRates);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuExchangeRates);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuVesselPools, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.bmnuContacts, true);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Entities";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.bmnuUsersManagement);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "Administration";
            // 
            // rpApplicationSettings
            // 
            this.rpApplicationSettings.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup7});
            this.rpApplicationSettings.Name = "rpApplicationSettings";
            this.rpApplicationSettings.Text = "Application Settings";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.bmnuLogIn);
            this.ribbonPageGroup4.ItemLinks.Add(this.bmnuSettings);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Connection Settings";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.bmnuChangeUILanguage);
            this.ribbonPageGroup5.ItemLinks.Add(this.bmnuApplicationParametersManagement);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Application Settings";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.bmnuChangePassword);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "User Profile";
            this.ribbonPageGroup7.Visible = false;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 721);
            this.Controls.Add(this.ribbon);
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.Text = "FreightMetrics";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.mdiManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection4BarManger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LargeImageCollection_32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager mdiManager;
        private DevExpress.XtraBars.BarSubItem bmnuApplication;
        private DevExpress.XtraBars.BarButtonItem bmnuChangeUILanguage;
        private DevExpress.XtraBars.BarButtonItem bmnuChangePassword;
        private DevExpress.XtraBars.BarButtonItem bmnuSettings;
        private DevExpress.XtraBars.BarSubItem bmnuHelp;
        private DevExpress.XtraBars.BarButtonItem bmnuAbout;
        private DevExpress.Utils.ImageCollection imageCollection4BarManger;
        private DevExpress.XtraBars.BarButtonItem bmnuApplicationParametersManagement;
        private DevExpress.XtraBars.BarSubItem bsmiImport;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarSubItem bsmiEntities;
        private DevExpress.XtraBars.BarSubItem barSubItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem bmnuViewTrades;
        private DevExpress.XtraBars.BarButtonItem bmnuCompanies;
        private DevExpress.XtraBars.BarButtonItem bmnuTraders;
        private DevExpress.XtraBars.BarButtonItem bmnuVesselPools;
        private DevExpress.XtraBars.BarButtonItem bmnuBooks;
        private DevExpress.XtraBars.BarButtonItem bmnuBankAccounts;
        private DevExpress.XtraBars.BarButtonItem bmnuProfitCenters;
        private DevExpress.XtraBars.BarButtonItem bmnuDesks;
        private DevExpress.XtraBars.BarButtonItem bmnuClearingHouses;
        private DevExpress.XtraBars.BarButtonItem bmnuIndexes;
        private DevExpress.XtraBars.BarButtonItem bmnuContacts;
        private DevExpress.XtraBars.BarButtonItem bmnuLoansManagement;
        private DevExpress.XtraBars.BarButtonItem bmnuHierarchyManagement;
        private DevExpress.XtraBars.BarButtonItem bmnuInterestRefRates;
        private DevExpress.XtraBars.BarButtonItem bmnuExchangeRates;
        private DevExpress.XtraBars.BarButtonItem bmnuBalticInputData;
        private DevExpress.XtraBars.BarButtonItem bmnuRiskManagement;
        private DevExpress.XtraBars.BarButtonItem de;
        private DevExpress.XtraBars.BarButtonItem bmnuUsersManagement;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarSubItem bsmiMonteCarlo;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpMainMenu;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpEntitiesManagement;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpApplicationSettings;
        private DevExpress.Utils.ImageCollection LargeImageCollection_32;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem bmnuAddTCTrade;
        private DevExpress.XtraBars.BarButtonItem bmnuAddFFATrade;
        private DevExpress.XtraBars.BarButtonItem bmnuAddCargoTrade;
        private DevExpress.XtraBars.BarButtonItem bmnuAddOptionTrade;
        private DevExpress.XtraBars.BarButtonItem bmnuMCSimulations;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem bmnuImportTrades;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.Utils.ImageCollection imageCollection24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem bmnuLogIn;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
    }
}