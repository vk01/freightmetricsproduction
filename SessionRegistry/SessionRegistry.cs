using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq.Mapping;
using System.Globalization;

namespace Exis.SessionRegistry
{
    public static class ServerSessionRegistry
    {
        public static string GeneralErrorMessage = "An error occurred. Please try again.";
        private static List<string> m_AllPermissions;
        private static List<string> m_UserPermissions;
        private static string m_ConnectionString;

        public static CultureInfo ServerCultureInfo { get; set; }

        public static List<string> UserPermissions
        {
            get { return m_UserPermissions; }
        }

        public static List<string> AllPermissions
        {
            get { return m_AllPermissions; }
        }

        public static object AppUser { get; set; }

        public static string User { get; set; }

        public static string ApplicationDirectory { get; set; }

        public static int InstallationPort { get; set; }
        public static string ConnectionString
        {
            get
            {
                return m_ConnectionString;
            }
            set { m_ConnectionString = value; }
        }

        public static MappingSource MappingSource { get; set; }

        public static bool HasPermission(string PermissionCode)
        {
            string pc = PermissionCode.ToUpper();
            return UserPermissions == null || UserPermissions.Count == 0 || UserPermissions.Contains(pc) ||
                   !AllPermissions.Contains(pc);
        }

        public static void initialize()
        {
            m_UserPermissions = new List<string>();
            m_AllPermissions = new List<string>();
        }
    }
}