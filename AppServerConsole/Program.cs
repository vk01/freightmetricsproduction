﻿using System;
using System.Threading;
using Exis.Domain;

namespace Exis.AppServerConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //DomainObject.InitializeDataContext();
            AppDomain.CurrentDomain.SetShadowCopyFiles();
            AppDomain.CurrentDomain.SetCachePath("Cache");
            var appService = new AppServer.AppService();
            try
            {
                //                objMonitor.LoadLicense();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Failed to validate license information." + Environment.NewLine + exc);
                Console.ReadLine();
                return;
            }
            appService.Initialize();
            var workerThread = new Thread(appService.Run);
            workerThread.Start();
            Console.ReadLine();
            appService.Stop();
            workerThread.Interrupt();
            workerThread.Join();
        }
    }
}



