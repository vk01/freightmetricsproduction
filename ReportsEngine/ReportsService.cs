﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Exis.Domain;
using Exis.ServiceInterfaces;
using Exis.SessionRegistry;
using Exis.WCFExtensions;
using Gurock.SmartInspect;

namespace Exis.ReportsEngine
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any, MaxItemsInObjectGraph = Int32.MaxValue,
        InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple,
        IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    [ErrorHandlerBehavior]
    [MessageInspectorBehavior]
    public class ReportsService : IReportsService
    {
        private readonly object smtpClientSynchObject = new object();
        public string DBConnectionString = ServerSessionRegistry.ConnectionString;
        public string exisIssueTrackerEmailAddress;
        public string exisSupportEmailAddress;
        public string installationEmailAddress;

        public SmtpClient smtpClient;

        #region IReportsService Members

        public void Test()
        {
            throw new NotImplementedException();
        }

        #endregion

        private void HandleException(string userName, Exception exc)
        {
            try
            {
                LogException(userName, exc);
                lock (smtpClientSynchObject)
                {
                    SendException(userName, exc);
                }
            }
            catch
            {
            }
        }

        private void LogException(string userName, Exception exc)
        {
            if (userName == null)
            {
                SiAuto.Main.LogException("Unhandled Exception", exc);
            }
            else
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                SiAuto.Si.GetSession(userName).LogException("Unhandled Exception", exc);
            }
        }

        private void SendException(string userName, Exception exc)
        {
            if (smtpClient != null)
            {
                if (!String.IsNullOrEmpty(exisSupportEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisSupportEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + (userName ?? "Main") + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }

                if (!String.IsNullOrEmpty(exisIssueTrackerEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisIssueTrackerEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + (userName ?? "Main") + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        LogException(userName, ex);
                    }
                }
            }
        }

        #region Nested type: ReportFormat

        private enum ReportFormat
        {
            Native = 1,
            Pdf = 2,
            Image = 3
        }

        #endregion

        #region Reports

        private XtraReport CreateReport(ReportType reportType, object dataSource,
                                        Dictionary<string, string> parameters, ReportFormat format,
                                        out MemoryStream reportStream)
        {
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(reportType.Template));
            var objXtraReport = new XtraReport();
            objXtraReport.LoadLayout(stream);
            stream.Close();

            objXtraReport.DataSource = dataSource;
            objXtraReport.RequestParameters = false;

            if (parameters != null)
            {
                foreach (var keyValuePair in parameters)
                {
                    objXtraReport.Parameters[keyValuePair.Key].Value = keyValuePair.Value;
                }
            }

            objXtraReport.ScriptReferences = new[]
                                                 {
                                                     typeof (ServerSessionRegistry).Assembly.Location,
                                                     typeof (Case).Assembly.Location
                                                 };

            objXtraReport.CreateDocument();
            reportStream = new MemoryStream();
            if (format == ReportFormat.Native)
            {
                objXtraReport.PrintingSystem.SaveDocument(reportStream,
                                                          new NativeFormatOptions
                                                              {Compressed = true, ShowOptionsBeforeSave = false});
            }
            else if (format == ReportFormat.Image)
            {
                objXtraReport.ExportToImage(reportStream,
                                            new ImageExportOptions
                                                {
                                                    ExportMode = ImageExportMode.SingleFile,
                                                    Resolution = 300,
                                                    Format = ImageFormat.Jpeg
                                                });
            }
            else if (format == ReportFormat.Pdf)
            {
                objXtraReport.ExportToPdf(reportStream, new PdfExportOptions {Compressed = true});
            }

            return objXtraReport;
        }

        private XtraReport CreateReport(ReportType reportType, object dataSource,
                                        Dictionary<string, string> parameters, ReportFormatEnum format,
                                        out MemoryStream reportStream)
        {
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(reportType.Template));
            var objXtraReport = new XtraReport();
            objXtraReport.LoadLayout(stream);
            stream.Close();

            objXtraReport.DataSource = dataSource;
            objXtraReport.RequestParameters = false;

            if (parameters != null)
            {
                foreach (var keyValuePair in parameters)
                {
                    objXtraReport.Parameters[keyValuePair.Key].Value = keyValuePair.Value;
                }
            }

            objXtraReport.ScriptReferences = new[]
                                                 {
                                                     typeof (ServerSessionRegistry).Assembly.Location,
                                                     typeof (Case).Assembly.Location
                                                 };

            objXtraReport.CreateDocument();
            reportStream = new MemoryStream();
            if (format == ReportFormatEnum.PDF)
            {
                objXtraReport.ExportToPdf(reportStream, new PdfExportOptions {Compressed = true});
            }
            else if (format == ReportFormatEnum.CSV)
            {
                objXtraReport.ExportToCsv(reportStream,
                                          new CsvExportOptions {Separator = "|", Encoding = Encoding.Unicode});
            }
            else if (format == ReportFormatEnum.Excel)
            {
                objXtraReport.ExportToXlsx(reportStream, new XlsxExportOptions());
            }
            else if (format == ReportFormatEnum.Word)
            {
                objXtraReport.ExportToRtf(reportStream, new RtfExportOptions());
            }
            return objXtraReport;
        }

        #endregion
    }
}