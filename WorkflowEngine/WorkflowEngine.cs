﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Threading;
using Devart.Data.Linq;
using Exis.Domain;
using Exis.Server.ServiceInterfaces;
using Exis.ServiceInterfaces;
using Exis.SessionRegistry;
using Gurock.SmartInspect;
using DataContext = Exis.Domain.DataContext;

namespace Exis.WorkflowEngine
{
    public class WorkflowEngine
    {
        [ThreadStatic]
        private static DataContext dataContext;
        private static string DBConnectionString;
        private static int EngineId;
        private static bool ProcessNewCases;

        public bool IsReadyStopEngine;
        public bool IsRequestStopEngine;
        public EndpointAddress address;
        public NetNamedPipeBinding ipcBinding;
        public IWorkflowServiceOneWay workflowService;

        public WorkflowEngine(int engineId, string connectionString, bool processNewCases)
        {
            EngineId = engineId;
            DBConnectionString = connectionString;
            ProcessNewCases = processNewCases;
            Init();
        }

        public void Init()
        {
            IsRequestStopEngine = false;
            try
            {
                dataContext = new DataContext(DBConnectionString);
                AppParameter appParameter =
                    dataContext.AppParameters.Where(a => a.Code == "SERVER_CULTURE_INFO_NAME").SingleOrDefault();

                if (appParameter == null)
                {
                    var myexc =
                        new ApplicationException("Failed to load 'SERVER_CULTURE_INFO_NAME' Application Parameter.");
                    SiAuto.Main.LogException(myexc);
                    throw myexc;
                }
                ServerSessionRegistry.ServerCultureInfo = new CultureInfo(appParameter.Value, false);
            }
            catch (Exception exc)
            {
                SiAuto.Main.LogException(exc);
                throw exc;
            }
            finally
            {
                if (dataContext != null) dataContext.Dispose();
            }
        }

        public void Run()
        {
            try
            {
                while (!IsRequestStopEngine)
                {
                    try
                    {
                        ProcessCases();
                    }
                    catch (Exception e)
                    {
                        SiAuto.Main.LogException(e);
                    }
                    finally
                    {
                        if (dataContext != null) dataContext.Dispose();
                        ClearCache();
                    }
//                    if (EngineId != 0)
//                    {
/*
                        for (int i = 1; i <= 5; i++)
                        {
                            if (IsRequestStopEngine)
                                break;
                            Thread.Sleep(60 * 1000);
                        }
*/
                    Sleep(5, 30000);
/*
                    }
                    else
                    {
                        Thread.Sleep(60 * 1000);
                    }
*/
                }
            }
            catch (Exception exc)
            {
                SiAuto.Main.LogException(exc);
/*
#if DEBUG
                Console.WriteLine("Unloading App Domain " + AppDomain.CurrentDomain.FriendlyName);
#endif
*/
                try
                {
                    AppDomain.Unload(AppDomain.CurrentDomain);
                }
                catch
                {
                    
                }
            }
            IsReadyStopEngine = true;
        }

        private void Sleep(int minutes, int checkIntervalMilliseconds)
        {
            var loops = (minutes*60000) / checkIntervalMilliseconds;
            for (int i = 0; i < loops; i++)
            {
                if (IsRequestStopEngine) break;
                Thread.Sleep(checkIntervalMilliseconds);
            }
        }

        private void ProcessCases()
        {
            try
            {
                if (IsRequestStopEngine)
                    return;
                SiAuto.Main.EnterMethod(Level.Debug, "Process Cases");
                dataContext = new DataContext(DBConnectionString);
                var caseIds = new List<long>();
                if (EngineId != 0)
                {
                    caseIds = Queries.WorkflowEngine_ProcessOldCases(dataContext, EngineId).ToList();
                }
                else if(ProcessNewCases)
                {
                    caseIds = Queries.WorkflowEngine_ProcessNewCases(dataContext).ToList();
                }
                dataContext.Dispose();
                dataContext = null;
                SiAuto.Main.LogDebug("Found {0} cases.", caseIds.Count.ToString());

                SiAuto.Main.ResetCheckpoint();
                foreach (long caseId in caseIds)
                {
                    if (IsRequestStopEngine)
                        return;
                    try
                    {
                        workflowService.ProcessCaseOneWay(caseId);
                    }
                    catch (Exception exc)
                    {
                        SiAuto.Main.LogException(exc);
                        workflowService = ChannelFactory<IWorkflowServiceOneWay>.CreateChannel(ipcBinding, address);
                    }
                }
            }
            catch (Exception exc)
            {
                SiAuto.Main.LogException(exc);
            }
            finally
            {
                if (dataContext != null) dataContext.Dispose();
                SiAuto.Main.LeaveMethod(Level.Debug, "Process Cases");
            }
        }

        private void ClearCache()
        {
            Assembly[] asms = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly asm in asms)
            {
                if (asm.ManifestModule.Name.ToUpper() == "Exis.DOMAINMODEL.DLL")
                {
                    Type[] types = asm.GetTypes();
                    foreach (Type type in types)
                    {
                        object cache =
                            type.GetType().GetProperty("Cache",
                                                       BindingFlags.DeclaredOnly | BindingFlags.Instance |
                                                       BindingFlags.NonPublic).GetValue(type, null);
                        cache.GetType().GetField("m_fieldInfoCache",
                                                 BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                 BindingFlags.NonPublic).SetValue(cache, null);
                        cache.GetType().GetField("m_constructorInfoCache",
                                                 BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                 BindingFlags.NonPublic).SetValue(cache, null);
                        cache.GetType().GetField("m_methodInfoCache",
                                                 BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                 BindingFlags.NonPublic).SetValue(cache, null);
                        cache.GetType().GetField("m_propertyInfoCache",
                                                 BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                 BindingFlags.NonPublic).SetValue(cache, null);
                    }
                    break;
                }
            }
        }
    }

    public static class Queries
    {
        #region Compiled Queries

        internal static Func<DataContext, long, IQueryable<long>>
            WorkflowEngine_ProcessOldCases
                =
                CompiledQuery.Compile(
                    (DataContext dataContext, long EngineId) =>
                    from objCase in dataContext.Cases
                    where objCase.Status_Str == CaseEventStatusEnum.Opened.ToString("d")
                          && (from objEvent in dataContext.Events select objEvent.CaseId).Contains(objCase.Id)
                          && (from objCaseTypeWfEngine in dataContext.CaseTypesWfEngines
                              where objCaseTypeWfEngine.WorkflowEngineId == EngineId
                              select objCaseTypeWfEngine.CaseTypeId).Contains(objCase.CaseTypeId)
                    select objCase.Id);

        internal static Func<DataContext, IQueryable<long>>
            WorkflowEngine_ProcessNewCases
                =
                CompiledQuery.Compile(
                    (DataContext dataContext) =>
                    from objCase in dataContext.Cases
                    where objCase.Status_Str == CaseEventStatusEnum.Opened.ToString("d")
                          && !(from objEvent in dataContext.Events select objEvent.CaseId).Contains(objCase.Id)
                    select objCase.Id);

        #endregion
    }
}