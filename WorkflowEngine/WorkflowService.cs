﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Threading;
using Devart.Data.Linq;
using Exis.Domain;
using Exis.RuleEngine;
using Exis.Server.ServiceInterfaces;
using Exis.ServiceInterfaces;
using Exis.SessionRegistry;
using Exis.WCFExtensions;
using Gurock.SmartInspect;
using Gurock.SmartInspect.LinqToSql;
using DataContext = Exis.Domain.DataContext;

namespace Exis.WorkflowEngine
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any, InstanceContextMode = InstanceContextMode.PerCall,
        MaxItemsInObjectGraph = Int32.MaxValue,
        ConcurrencyMode = ConcurrencyMode.Multiple,
        IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    [ErrorHandlerBehavior]
    public class WorkflowService : IWorkflowServiceReqRep, IWorkflowServiceOneWay
    {
        private DataContext dataContext;
        private Mutex LockMutex;
        private readonly object smtpClientSynchObject = new object();
        public string DBConnectionString = ServerSessionRegistry.ConnectionString;

        public string exisIssueTrackerEmailAddress;
        public string exisSupportEmailAddress;
        public string installationEmailAddress;

        public SmtpClient smtpClient;

        public static string ServicePort;

        private void HandleException(string userName, Exception exc)
        {
            try
            {
                LogException(userName, exc);
                lock (smtpClientSynchObject)
                {
                    SendException(userName, exc);
                }
            }
            catch
            {
                
            }
        }

        private void LogException(string userName, Exception exc)
        {
            if (userName == null)
            {
                SiAuto.Main.LogException("Unhandled Exception", exc);
            }
            else
            {
                if (SiAuto.Si.GetSession(userName) == null) SiAuto.Si.AddSession(userName, true);
                SiAuto.Si.GetSession(userName).LogException("Unhandled Exception", exc);
            }
        }

        private void SendException(string userName, Exception exc)
        {
            if (smtpClient != null)
            {
                if (!String.IsNullOrEmpty(exisSupportEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisSupportEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + userName + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        SiAuto.Main.LogException(new ApplicationException("Failed to send email from : " + installationEmailAddress, ex));
                    }
                }
                if (!String.IsNullOrEmpty(exisIssueTrackerEmailAddress))
                {
                    try
                    {
                        var fromAddress = new MailAddress(installationEmailAddress);
                        var ToAddress = new MailAddress(exisIssueTrackerEmailAddress);
                        var mailMessage = new MailMessage
                                              {
                                                  IsBodyHtml = false,
                                                  From = fromAddress,
                                                  Sender = fromAddress,
                                                  Subject = "EXCEPTION (" + userName + "): " + exc.Message,
                                                  Priority = MailPriority.High,
                                                  Body = exc.ToString()
                                              };
                        mailMessage.To.Add(ToAddress);
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        SiAuto.Main.LogException(new ApplicationException("Failed to send email from : " + installationEmailAddress, ex));
                    }
                }
            }
        }

        private void ProcessCaseInt(long CaseId)
        {
            try
            {
                LockMutex = new Mutex(false, "Exis.WorkflowEngine." + ServicePort + ".Mutex." + CaseId);
                if (!LockMutex.WaitOne(10000))
                {
                    throw new Exception("Case with Id = " + CaseId + ": Could not obtain lock.");
                }
                try
                {
                if (SiAuto.Si.GetSession(CaseId.ToString()) == null) SiAuto.Si.AddSession(CaseId.ToString(), true);
                dataContext = new DataContext(DBConnectionString);
                if (SiAuto.Si.Level == Level.Debug)
                    dataContext.Log = new SmartInspectLinqToSqlAdapter(SiAuto.Si.GetSession(CaseId.ToString()))
                                          {TitleLimit = 0, TitlePrefix = "WorkflowService: "};
                Case objCase = Case.WorkflowService_ProcessCaseInt(dataContext, CaseId).SingleOrDefault();
                if (objCase == null)
                {
                    SiAuto.Si.GetSession(CaseId.ToString()).LogError("Case with Id = " + CaseId +
                                                                     " does not exist.");
                    return;
                }
                if (objCase.Status == CaseEventStatusEnum.Closed || objCase.Status == CaseEventStatusEnum.Expired)
                {
                    SiAuto.Si.GetSession(CaseId.ToString()).LogError("Case with Id = " + CaseId +
                                                                     " is closed.");
                    return;
                }

                Event objCurrentEvent = null;
                Event_EventType Event_EventType = GetCurrentEvent(dataContext, objCase.Id).FirstOrDefault();
                if (Event_EventType != null)
                {
                    objCurrentEvent = Event_EventType.Event;
                    objCurrentEvent.Type = Event_EventType.EventType;
                    objCurrentEvent.context = dataContext;
                }
                objCase.context = dataContext;
                ProcessWorkflow(objCase, objCurrentEvent);
            }
            finally
            {
                if (dataContext != null) dataContext.Dispose();
                if (LockMutex != null) LockMutex.ReleaseMutex();
                if (SiAuto.Si.GetSession(CaseId.ToString()) != null)
                {
                    SiAuto.Si.DeleteSession(SiAuto.Si.GetSession(CaseId.ToString()));
                }
            }
        }
            catch (Exception e)
            {
                HandleException(null, e);
                throw;
            }
        }

        private void ProcessWorkflow(Case objCase, Event objEvent)
        {
            try
            {
                if (objEvent == null)
                {
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug("Case with Id = " + objCase.Id +
                                                                         " does not have current Event. Creating first Event.");

                    WorkflowRule objRule;
                    if (objCase.WorkflowRuleIdEntry == null)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "No entry rule defined for case {0}. Checking for entry rules.",
                            new[] {objCase.Id.ToString()});
                        objRule = GetRuleForCase(objCase);
                        if (objRule != null)
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                "Found entry with Id {0} rule for case {1}",
                                new[] {objRule.Id.ToString(), objCase.Id.ToString()});
                            objCase.WorkflowRuleIdEntry = objRule.Id;
                            dataContext.SubmitChanges();
                        }
                        else
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug("No entry rule found for case {0}",
                                                                                 new[] {objCase.Id.ToString()});
                            return;
                        }
                    }
                    else
                    {
                        objRule = WorkflowRule.GetWorkflowRule(dataContext, objCase.WorkflowRuleIdEntry).Single();
                    }

                    Appointment appointment;
                    Event newEvent = CreateNextEvent(objCase, objRule, out appointment);
                    if (objRule.Delay != null)
                    {
                        var delayValue = FunctionEvaluator.Evaluate(objRule.Delay, objEvent);
                        if (delayValue == null)
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                "Workflow rule delay evaluation returned null. Will not use delay.");
                        }
                        else if (!(delayValue is DateTime))
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogError(
                                "Workflow rule delay value is not a DateTime. Will not use delay.");
                        }
                        else
                        {
                            appointment.StartDate = (DateTime)delayValue;
                        }
                    }
                    else if (!String.IsNullOrEmpty(newEvent.Type.EffectiveDate))
                    {
                        var effValue = FunctionEvaluator.Evaluate(newEvent.Type.EffectiveDate, objEvent);
                        if (effValue == null)
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                "Event type effective date returned null. Will not use effective date.");
                        }
                        else if (!(effValue is DateTime))
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogError(
                                "Event type effective date value is not a DateTime. Will not use effective date.");
                        }
                        else
                        {
                            appointment.StartDate = (DateTime)effValue;
                        }
                    }
                    dataContext.Events.InsertOnSubmit(newEvent);
                    dataContext.Appointments.InsertOnSubmit(appointment);
                    dataContext.SubmitChanges();
                    ProcessWorkflow(objCase, newEvent);
                    return;
                }

                if (objEvent.Status == CaseEventStatusEnum.Opened)
                {
                    if (objEvent.ActStatus == EventActionStatusEnum.NoActionsOccured)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "Executing Open Actions. Case: {0}. Event: {1}",
                            new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

                        ExecuteOpenCloseActions(objEvent, objCase);
                        if (objEvent.ActStatus == EventActionStatusEnum.OpenActionCompleted)
                        {
                            dataContext.SubmitChanges();
                            ProcessWorkflow(objCase, objEvent);
                        }
                    }
                    else if (objEvent.ActStatus == EventActionStatusEnum.OpenActionCompleted ||
                             objEvent.ActStatus == EventActionStatusEnum.MainActionPending)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "Calculate Expiration Condition. Case: {0}. Event: {1}",
                            new[] {objCase.Id.ToString(), objEvent.Id.ToString()});


                        bool blExpire = MustEventExpire(objEvent, objCase);

                        if (blExpire)
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                "Expiring Event. Case: {0}. Event: {1}",
                                new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

                            objEvent.Result = CaseEventResultEnum.Expired;
                            objEvent.ActStatus = EventActionStatusEnum.MainActionCompleted;
                            dataContext.SubmitChanges();

                            ProcessWorkflow(objCase, objEvent);
                        }
                        else
                        {
                            bool blIsSystemFunction = HasEventSystemFunctionMainAction(objEvent, objCase);

                            if (blIsSystemFunction)
                            {
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                    "Executing Main Action. Case: {0}. Event: {1}",
                                    new[] {objCase.Id.ToString(), objEvent.Id.ToString()});


                                ExecuteMainAction(objEvent, objCase);
                                dataContext.SubmitChanges();
                                if (objEvent.ActStatus == EventActionStatusEnum.MainActionCompleted)
                                {
                                    ProcessWorkflow(objCase, objEvent);
                                }
                            }
                            return;
                        }
                    }
                    else if (objEvent.ActStatus == EventActionStatusEnum.MainActionCompleted)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "Executing Closing Actions. Case: {0}. Event: {1}",
                            new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

                        ExecuteOpenCloseActions(objEvent, objCase);
                        if (objEvent.ActStatus == EventActionStatusEnum.CloseActionCompleted)
                        {
                            dataContext.SubmitChanges();
                            ProcessWorkflow(objCase, objEvent);
                        }
                    }
                    else if (objEvent.ActStatus == EventActionStatusEnum.CloseActionCompleted)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug("Closing Event. Case: {0}. Event: {1}",
                                                                             new[]
                                                                                 {
                                                                                     objCase.Id.ToString(),
                                                                                     objEvent.Id.ToString()
                                                                                 });

                        objEvent.DateTo = DateTime.Now;
                        objEvent.Status = CaseEventStatusEnum.Closed;
                        dataContext.SubmitChanges();

                        ProcessWorkflow(objCase, objEvent);
                    }
                }
                else if (objEvent.Status == CaseEventStatusEnum.Closed)
                {
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug("Executing Transition. Case: {0}. Event: {1}",
                                                                         new[]
                                                                             {
                                                                                 objCase.Id.ToString(),
                                                                                 objEvent.Id.ToString()
                                                                             });

                    WorkflowRule chosenRule = GetTransitionRule(objEvent);

                    if (chosenRule == null)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug("No Transition rule found.");
                        return;
                    }
                    long chosenRuleId = chosenRule.Id;
                    if (chosenRule.Type == WorkflowRuleTypeEnum.Transition)
                    {
                        EventType transitionEventType =
                            EventType.WorkflowService_ProcessWorkflow(dataContext, chosenRule.Id).Single();

                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "Transition rule of type 'Transition' found. Rule: {0}. Next Event Type: {1}",
                            new[] {chosenRule.Id.ToString(), transitionEventType.Description});

                        Appointment appointment;
                        Event newEvent = CreateNextEvent(objCase, chosenRule, out appointment);
 if (chosenRule.Delay != null)
                        {
                            var delayValue = FunctionEvaluator.Evaluate(chosenRule.Delay, objEvent);
                            if(delayValue == null)
                            {
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                    "Workflow rule delay evaluation returned null. Will not use delay.");
                            }
                            else if (!(delayValue is DateTime))
                            {
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogError(
                                    "Workflow rule delay value is not a DateTime. Will not use delay.");
                            }
                            else
                            {
                                appointment.StartDate = (DateTime) delayValue;
                            }
                        }
                        else if (!String.IsNullOrEmpty(objEvent.Type.EffectiveDate))
                        {
                            var effValue = FunctionEvaluator.Evaluate(objEvent.Type.EffectiveDate, objEvent);
                            if (effValue == null)
                            {
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                    "Event type effective date returned null. Will not use effective date.");
                            }
                            else if (!(effValue is DateTime))
                            {
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogError(
                                    "Event type effective date value is not a DateTime. Will not use effective date.");
                            }
                            else
                            {
                                appointment.StartDate = (DateTime) effValue;
                            }
                        }
                        dataContext.Events.InsertOnSubmit(newEvent);
                        dataContext.Appointments.InsertOnSubmit(appointment);
                        dataContext.SubmitChanges();
                        ProcessWorkflow(objCase, newEvent);
                    }
                    else if (chosenRule.Type == WorkflowRuleTypeEnum.ExitWithFailure)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "Transition rule of type 'ExitWithFailure' found. Case should be closed with Failure.");

                        objCase.ActStatus = CaseActionStatusEnum.Closing;
                        objCase.WorkflowRuleIdExit = chosenRuleId;

                        if (CheckSubcaseCompletion(objEvent, objCase))
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                "Closing Case. Case: {0}. Event: {1}",
                                new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

                            objCase.Result = CaseEventResultEnum.Failure;
                            objCase.Status = CaseEventStatusEnum.Closed;
                            objCase.Chd = DateTime.Now;
                            objCase.Chuser = ServerSessionRegistry.User;
                            objCase.DateTo = DateTime.Now;

                            // TODO Fix when decision on Locks is made
                            /*
                            Lock objLock =
                                dataContext.Locks.Where(
                                    a => a.ObjType == LockedObjectTypeEnum.Case.ToString("d") && a.ObjId == objCase.Id).
                                    SingleOrDefault();
                            if (objLock != null) dataContext.Locks.DeleteOnSubmit(objLock);
                             */
                        }
                        dataContext.SubmitChanges();
                        return;
                    }
                    else if (chosenRule.Type == WorkflowRuleTypeEnum.ExitWithSuccess)
                    {
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                            "Transition rule of type 'ExitWithSuccess' found. Case should be closed with Success");

                        objCase.ActStatus = CaseActionStatusEnum.Closing;
                        objCase.WorkflowRuleIdExit = chosenRuleId;

                        if (CheckSubcaseCompletion(objEvent, objCase))
                        {
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                                "Closing Case. Case: {0}. Event: {1}",
                                new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

                            objCase.Result = CaseEventResultEnum.Success;
                            objCase.Status = CaseEventStatusEnum.Closed;
                            objCase.Chd = DateTime.Now;
                            objCase.Chuser = ServerSessionRegistry.User;
                            objCase.DateTo = DateTime.Now;

                            // TODO Fix when decision on Locks is made
                            /*
                            Lock objLock =
                                dataContext.Locks.Where(
                                    a => a.ObjType == LockedObjectTypeEnum.Case.ToString("d") && a.ObjId == objCase.Id).
                                    SingleOrDefault();
                            if (objLock != null) dataContext.Locks.DeleteOnSubmit(objLock);
                             */
                        }
                        dataContext.SubmitChanges();
                        return;
                    }
                }
            }
            catch (Exception exc)
            {
                var myexc = new ApplicationException("Exception processing workflow", exc);
                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                throw myexc;
            }
        }

        private WorkflowRule GetRuleForCase(Case objCase)
        {
            List<WorkflowRule> entryRules =
                WorkflowRule.WorkflowService_GetRuleForCase(dataContext, objCase.WorkflowDiagramVersionId).ToList();

            WorkflowRule chosenRule = null;

            foreach (WorkflowRule wr in entryRules)
            {
                if (wr.ConditionId != null)
                {
                    Condition condition = Condition.Condition_GetCondition(dataContext, wr.ConditionId).Single();
                    condition.context = dataContext;
                    bool blResult = ConditionEvaluator.ExecuteCondition(condition, objCase);
                    if (blResult)
                    {
                        chosenRule = wr;
                        break;
                    }
                }
                else
                {
                    chosenRule = wr;
                    break;
                }
            }
            return chosenRule;
        }

        private Event CreateNextEvent(Case objCase, WorkflowRule objWorkflowRule, out Appointment appointment)
        {
            DateTime currentDate = DateTime.Now;
            EventType transitionEventType =
                EventType.WorkflowService_ProcessWorkflow(dataContext, objWorkflowRule.Id).Single();

            var newEvent =
                new Event
                    {
                        Id = dataContext.GetNextId(typeof (Event)),
                        CaseId = objCase.Id,
                        EventTypeId = transitionEventType.Id,
                        WorkflowRuleId = objWorkflowRule.Id,
                        Status = CaseEventStatusEnum.Opened,
                        DateFrom = currentDate,
                        ActStatus = EventActionStatusEnum.NoActionsOccured,
                        Priority = CaseEventPriorityEnum.Normal,
                        Cruser = ServerSessionRegistry.User,
                        Crd = currentDate,
                        Chuser = ServerSessionRegistry.User,
                        Chd = currentDate,
                        Crappuser = ServerSessionRegistry.User,
                        Crappd = currentDate,
                        Chappuser = ServerSessionRegistry.User,
                        Chappd = currentDate
                    };
            newEvent.context = dataContext;
            newEvent.Type = transitionEventType;

            appointment = new Appointment
                              {
                                  Id = dataContext.GetNextId(typeof (Appointment)),
                                  RefType = AppointmentRefTypeEnum.Event,
                                  RefId = newEvent.Id,
                                  StartDate = newEvent.DateFrom,
                                  Type = 0,
                                  IsAllDay = false,
                                  Subject = transitionEventType.Description,
                                  Datf = currentDate,
                                  Status = 0,
                                  Label = 0
                              };
            appointment.context = dataContext;

            return newEvent;
        }

        public bool CheckSubcaseCompletion(Event objEvent, Case objCase)
        {
            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                "Checking if all Subcases are closed. Case: {0}. Event: {1}",
                new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

            bool blResult = true;

            List<Case> subcases = Case.WorkflowService_CheckSubcaseCompletion(dataContext, objCase.Id).ToList();

            foreach (Case objCase2 in subcases)
            {
                if (objCase2.Status != CaseEventStatusEnum.Closed)
                {
                    blResult = false;
                    break;
                }
            }

            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                "Are all subcases closed? : {0}", new[] {blResult.ToString()});
            return blResult;
        }

        private bool MustEventExpire(Event objEvent, Case objCase)
        {
            long CaseId = objCase.Id;

            if (objEvent.Result != null)
            {
                SiAuto.Si.GetSession(CaseId.ToString()).LogDebug("Event already has a Result. Event must not expire.");
                return false;
            }

            SiAuto.Si.GetSession(CaseId.ToString()).LogDebug("Checking Expiration Conditions");

            if (!String.IsNullOrEmpty(objEvent.Type.ExpirationDate))
            {
                SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                    "Event Type has expiration date application field: {0}", objEvent.Type.ExpirationDate);

                try
                {
                    var expDateValue = FunctionEvaluator.Evaluate(objEvent.Type.ExpirationDate, objEvent);
                    if(expDateValue == null)
                    {
                        SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                            "Event must not expire due to event type expiration date is null.");
                        return false;
                    }
                    if(!(expDateValue is DateTime))
                    {
                        SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                            "Event expiration date value is not of type DateTime. Event will not expire. Expiration Date Value: {0}",
                            expDateValue);
                        return false;
                    }

                    var eventTypeExpDate = (DateTime) expDateValue;
                    if (eventTypeExpDate < DateTime.Now)
                    {
                        SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                            "Event must expire due to event type expiration date: {0}", eventTypeExpDate.ToString("g"));
                        return true;
                    }
                }
                catch (Exception e)
                {
                    SiAuto.Si.GetSession(CaseId.ToString()).LogException(
                        "Expiration date evaluation threw exception. Event will not expire.", e);
                    return false;
                }
            }

            Function mainFunction =
                Function.WorkflowService_MustEventExpire(dataContext, objEvent.EventTypeId).SingleOrDefault();

            if (mainFunction != null)
            {
                if (objEvent.Type.ProcessType == EventTypeProcessEnum.System &&
                    mainFunction.Type == FunctionTypeEnum.System)
                {
                    SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                        "System Process:System Function. Event must not expire.");
                    return false;
                }
            }
            else if (objEvent.Type.ProcessType == EventTypeProcessEnum.User)
            {
                SiAuto.Si.GetSession(CaseId.ToString()).LogDebug("User Process");
                SiAuto.Si.GetSession(CaseId.ToString()).LogDebug("Checking Workflow Rule transition expiration.");

                WorkflowRule chosenRule = GetTransitionRule(objEvent);

                if (chosenRule != null)
                {
                    SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                        "Event must expire due to workflow rule transition. Chosen rule is: " + chosenRule.Description);
                    return true;
                }
                SiAuto.Si.GetSession(CaseId.ToString()).LogDebug(
                    "Event must not expire due to workflow rule transition.");
            }

            return false;
        }

        private bool HasEventSystemFunctionMainAction(Event objEvent, Case objCase)
        {
            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                "Checking if Main Action is a System Function. Case: {0}. Event: {1}",
                new[] {objCase.Id.ToString(), objEvent.Id.ToString()});

            Function mainFunction =
                Function.WorkflowService_MustEventExpire(dataContext, objEvent.EventTypeId).SingleOrDefault();

            bool blResult = objEvent.Type.ProcessType == EventTypeProcessEnum.System &&
                            (mainFunction == null || mainFunction.Type == FunctionTypeEnum.System);
            SiAuto.Si.GetSession(objCase.Id.ToString()).LogDebug(
                "Is Main Action a System Function? : {0}", new[] {blResult.ToString()});
            return blResult;
        }

        private void ExecuteOpenCloseActions(Event eventToHandle, Case objCase)
        {
            EventTypeActionTypeEnum actionType = (eventToHandle.ActStatus ==
                                                  EventActionStatusEnum.NoActionsOccured
                                                      ? EventTypeActionTypeEnum.Open
                                                      : EventTypeActionTypeEnum.Close);

            List<EventTypeAction> actions =
                EventTypeAction.WorkflowService_ExecuteOpenCloseActions(dataContext, eventToHandle.EventTypeId,
                                                                        actionType.ToString("d")).ToList();

            foreach (EventTypeAction eta in actions)
            {
                SystemFunctionReturnValueEnum returnValue;
                try
                {
                    returnValue = ExecuteEventAction(eventToHandle, objCase, eta);
                }
                catch (Exception exc)
                {
                    if (typeof (TargetInvocationException) == exc.GetType() && exc.InnerException == null)
                    {
                        exc = exc.InnerException;
                    }

                    var myexc =
                        new ApplicationException(
                            String.Format("Unexpected error '{2}' while executing action {0} for event {1}.",
                                          new[] {eta.Id.ToString(), eventToHandle.Id.ToString(), exc.ToString()}),
                            exc);
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(exc);
                    throw myexc;
                }

                if(returnValue == SystemFunctionReturnValueEnum.NoResult)
                {
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogWarning(
                        "Event type action {0} for event {1} returned NoResult. {2} actions will rerun the next time this event is processed.",
                        new[] {eta.Id.ToString(), eventToHandle.Id.ToString()}, actionType.ToString("g"));
                    return;
                }
                if (returnValue == SystemFunctionReturnValueEnum.Failure)
                {
                    var myexc =
                        new ApplicationException(String.Format("Event type action {0} for event {1} failed.",
                                                               new[] {eta.Id.ToString(), eventToHandle.Id.ToString()}));
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                    throw myexc;
                }
            }


            EventActionStatusEnum newActionStatus = (eventToHandle.ActStatus ==
                                                     EventActionStatusEnum.NoActionsOccured
                                                         ? EventActionStatusEnum.OpenActionCompleted
                                                         : EventActionStatusEnum.CloseActionCompleted);

            eventToHandle.ActStatus = newActionStatus;
        }

        private void ExecuteMainAction(Event eventToHandle, Case objCase)
        {
            if (eventToHandle.ActStatus != EventActionStatusEnum.OpenActionCompleted &&
                eventToHandle.ActStatus != EventActionStatusEnum.MainActionPending)
            {
                var myexc =
                    new ApplicationException(String.Format("Illegal call to {0} for event {1} with ActionStatus {2}.",
                                                           new[]
                                                               {
                                                                   GetType().Name, eventToHandle.Id.ToString(),
                                                                   eventToHandle.ActStatus.ToString("g")
                                                               }));
                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                throw myexc;
            }

            EventTypeActionTypeEnum actionType = EventTypeActionTypeEnum.Main;
            EventTypeAction action =
                EventTypeAction.WorkflowService_ExecuteOpenCloseActions(dataContext, eventToHandle.EventTypeId,
                                                                        actionType.ToString("d")).SingleOrDefault();

            SystemFunctionReturnValueEnum returnValue;
            if (action == null)
            {
                returnValue = SystemFunctionReturnValueEnum.Success;
            }
            else
            {
                returnValue = ExecuteEventAction(eventToHandle, objCase, action);
            }

            if (returnValue == SystemFunctionReturnValueEnum.NoResult)
            {
                eventToHandle.ActStatus = EventActionStatusEnum.MainActionPending;
            }
            else
            {
                eventToHandle.Result = (returnValue == SystemFunctionReturnValueEnum.Success
                                            ? CaseEventResultEnum.Success
                                            : CaseEventResultEnum.Failure);
                eventToHandle.ActStatus = EventActionStatusEnum.MainActionCompleted;
            }
        }

        private WorkflowRule GetTransitionRule(Event objEvent)
        {
            WorkflowRule wr = objEvent.WorkflowRule;

            List<WorkflowRule> transitionRules =
                WorkflowRule.WorkflowService_GetTransitionRule(dataContext, wr.TargetWorkflowDiagramNodeId).ToList();

            WorkflowRule chosenRule = null;

            foreach (WorkflowRule rule in transitionRules)
            {
                if (rule.ConditionId != null)
                {
                    Condition condition = Condition.Condition_GetCondition(dataContext, rule.ConditionId).Single();
                    condition.context = dataContext;
                    bool blResult = ConditionEvaluator.ExecuteCondition(condition, objEvent);
                    if (blResult)
                    {
                        chosenRule = rule;
                        break;
                    }
                }
                else
                {
                    chosenRule = rule;
                    break;
                }
            }

            return chosenRule;
        }

        private SystemFunctionReturnValueEnum ExecuteEventAction(Event objEvent, Case objCase,
                                                                 EventTypeAction eventTypeAction)
        {
            eventTypeAction.context = dataContext;
            Function function = eventTypeAction.Function;

            // get the parameters, sort them, input first, then return, then by sort
            List<ParameterMapping> parameterMappings =
                ParameterMapping.WorkflowService_ExecuteEventAction(dataContext, eventTypeAction.Id).ToList();

            // prepare parameters
            object[] parameters = prepareEventTypeActionParameters(objEvent, objCase, parameterMappings);

            // execute system function
            SystemFunctionReturnValueEnum returnValue = ExecuteSystemFunction(objCase, function, parameters);

            if (returnValue == SystemFunctionReturnValueEnum.NoResult)
            {
                // Open or Close action with NoResult is not allowed...
                /* Changed 11/2/2011: Because it now makes sense to retry and execute the action in case of open or close action with NoResult result.
                 * Event should now stay in ExecutingOpeningActions or ExecutingClosingActions respectfuly in case of NoResult.
                                if (eventTypeAction.Type == EventTypeActionTypeEnum.Main)
                                {
                                    // do nothing (event remains in the same EventActionStatusEnum)
                                    return returnValue;
                                }

                                var myexc = new ApplicationException(String.Format(
                                    "Event Type Action {0}: Invalid System Function call return value {1} for Action with id {2} and type {2}.",
                                    new[]
                                        {
                                            eventTypeAction.Id.ToString(),
                                            eventTypeAction.Function.Name,
                                            returnValue.ToString("g"),
                                            eventTypeAction.Type.ToString("g")
                                        }));
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                                throw myexc;
                */
                return returnValue;
            }

            // propagate values from output parameters to the data via the application fields
            propagateSystemFunctionOutputParameters(objEvent, objCase, eventTypeAction, parameterMappings, parameters);

            return returnValue;
        }

        private object[] prepareEventTypeActionParameters(Event objEvent, Case objCase,
                                                          List<ParameterMapping> parameterMappings)
        {
            var parameters = new object[parameterMappings.Count + 1];

            objEvent.context = dataContext;
            parameters[0] = objEvent;
            int i = 1;

            foreach (ParameterMapping mapping in parameterMappings)
            {
                object value;
                mapping.context = dataContext;
                string dataType = mapping.FunctionParameter.ValueType;
                Type conversionType;
                try
                {
                    conversionType = Type.GetType(dataType, true, true);
                }
                catch (Exception exc)
                {
                    var myexc =
                        new ApplicationException("Failed to create Type object for the qualified name:" + dataType +
                                                 ". Parameter Id:" + mapping.FunctionParameter.Id, exc);
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                    throw myexc;
                }

                value = FunctionEvaluator.Evaluate(mapping.Value, objEvent);

                if (conversionType != typeof (object))
                {
                    if (value != null && conversionType.IsEnum)
                    {
                        try
                        {
                            value = Enum.Parse(conversionType, value.ToString());
                        }
                        catch (Exception exc)
                        {
                            var myexc =
                                new ApplicationException(
                                    "Failed to convert Parameter Value to Type:" + conversionType.FullName +
                                    ". Parameter Id:" + mapping.FunctionParameter.Id + ". Original Value Type:" +
                                    value.GetType().FullName,
                                    exc);
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                            throw myexc;
                        }
                    }
                    else if (value != null && conversionType.GetInterface("IConvertible", true) != null)
                    {
                        try
                        {
                            value = Convert.ChangeType(value, conversionType,
                                                       ServerSessionRegistry.ServerCultureInfo);
                        }
                        catch (Exception exc)
                        {
                            var myexc =
                                new ApplicationException(
                                    "Failed to convert Parameter Value to Type:" + conversionType.FullName +
                                    ". Parameter Id:" + mapping.FunctionParameter.Id + ". Original Value Type:" +
                                    value.GetType().FullName,
                                    exc);
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                            throw myexc;
                        }
                    }
                    else if (value != null && conversionType == typeof (List<>).MakeGenericType(value.GetType()))
                    {
                        try
                        {
                            var list = (IList) Activator.CreateInstance(conversionType);
                            list.Add(value);
                            value = list;
                        }
                        catch (Exception exc)
                        {
                            var myexc =
                                new ApplicationException(
                                    "Failed to convert Parameter Value to Type:" + conversionType.FullName +
                                    ". Parameter Id:" + mapping.FunctionParameter.Id + ". Original Value Type:" +
                                    value.GetType().FullName,
                                    exc);
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                            throw myexc;
                        }
                    }
                    else if (value == null || conversionType.IsInstanceOfType(value))
                    {
                    }
                    else
                    {
                        var myexc =
                            new ApplicationException("Failed to convert Parameter Value to Type:" +
                                                     conversionType.FullName + ". Parameter Id:" +
                                                     mapping.FunctionParameter.Id + ". Original Value Type:" +
                                                     value.GetType().FullName);
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                        throw myexc;
                    }
                }
                parameters[i] = value;
                i++;
            }
            return parameters;
        }

        private SystemFunctionReturnValueEnum ExecuteSystemFunction(Case objCase, Function function, object[] parameters)
        {
            if (function.Type != FunctionTypeEnum.System)
            {
                var myexc =
                    new ApplicationException(String.Format("Invalid use of Middleware Function.", new String[] {}));
                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                throw myexc;
            }

            int j = function.Internal.IndexOf("/");
            String functionName = function.Internal.Substring(j + 1);

            MethodInfo mi;
            try
            {
                int i = function.Internal.IndexOf("/");
                String assemblyName = function.Internal.Substring(0, i);
                var symbolFileName = assemblyName.Substring(0, assemblyName.LastIndexOf('.')) + ".pdb";
                var assemblyBytes = File.ReadAllBytes(assemblyName);
                var symbolBytes = File.Exists(symbolFileName) ? File.ReadAllBytes(symbolFileName) : null;
/*
                Assembly assembly =
                    Assembly.LoadFile(SessionRegistry.ApplicationDirectory + assemblyName);
*/
                Assembly assembly = null;
                Assembly[] asms = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly asm in asms)
                {
                    if (asm.ManifestModule.ScopeName.ToUpper() == "Exis.WORKFLOWSYSTEMFUNCTIONS.DLL")
                    {
                        assembly = asm;
                        break;
                    }
                }
                if(assembly == null) assembly = Assembly.Load(assemblyBytes, symbolBytes);
                Type[] arrayAssemblyTypes = assembly.GetTypes();
                Type type = null;
                foreach (Type typ in arrayAssemblyTypes)
                {
                    if (typ.IsPublic && typ.IsClass && typ.Name == "SystemFunctions")
                    {
                        type = typ;
                        break;
                    }
                }
                if (type == null)
                {
                    var myexc = new ApplicationException("System Functions class not found.");
                    SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                    throw (myexc);
                }
                mi = type.GetMethod(functionName, BindingFlags.Static | BindingFlags.Public);
            }
            catch (Exception exc)
            {
                var myexc =
                    new ApplicationException(
                        String.Format("Failed to find System Function with code {0} in System Functions module.",
                                      new[] {functionName}), exc);
                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                throw myexc;
            }

            try
            {
                var returnValue = (SystemFunctionReturnValueEnum) mi.Invoke(null, parameters);
                return returnValue;
            }
            catch (Exception exc)
            {
                // get inner exception if appropriate
                if (typeof (TargetInvocationException) == exc.GetType() && exc.InnerException == null)
                {
                    exc = exc.InnerException;
                }

                var myexc = new ApplicationException("Failed to run System Function:" + functionName, exc);
                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                throw myexc;
            }
        }

        private void propagateSystemFunctionOutputParameters(Event objEvent, Case objCase, EventTypeAction action,
                                                             List<ParameterMapping> parameterMappings,
                                                             object[] parameters)
        {
            int i = 1;
            foreach (ParameterMapping mapping in parameterMappings)
            {
                object value;
                // if return parameter
                if (mapping.FunctionParameter.Type == FunctionParameterTypeEnum.Return)
                {
                    mapping.context = dataContext;
                    string dataType = mapping.FunctionParameter.ValueType;
                    Type conversionType;
                    try
                    {
                        conversionType = Type.GetType(dataType, true, true);
                    }
                    catch (Exception exc)
                    {
                        var myexc =
                            new ApplicationException("Failed to create Type object for the qualified name:" + dataType +
                                                     ". Parameter Id:" + mapping.FunctionParameter.Id, exc);
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                        throw myexc;
                    }

                    value = parameters[i];

                    if (conversionType != typeof (object))
                    {
                        if (value != null && conversionType.IsEnum)
                        {
                            try
                            {
                                value = Enum.Parse(conversionType, value.ToString());
                            }
                            catch (Exception exc)
                            {
                                var myexc =
                                    new ApplicationException(
                                        "Failed to convert Parameter Value to Type:" + conversionType.FullName +
                                        ". Parameter Id:" + mapping.FunctionParameter.Id + ". Original Value Type:" +
                                        value.GetType().FullName,
                                        exc);
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                                throw myexc;
                            }
                        }
                        else if (value != null && conversionType.GetInterface("IConvertible", true) != null)
                        {
                            try
                            {
                                value = Convert.ChangeType(value, conversionType,
                                                           ServerSessionRegistry.ServerCultureInfo);
                            }
                            catch (Exception exc)
                            {
                                var myexc =
                                    new ApplicationException(
                                        "Failed to convert Parameter Value to Type:" + conversionType.FullName +
                                        ". Parameter Id:" + mapping.FunctionParameter.Id + ". Original Value Type:" +
                                        value.GetType().FullName,
                                        exc);
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                                throw myexc;
                            }
                        }
                        else if (value != null && conversionType == typeof (List<>).MakeGenericType(value.GetType()))
                        {
                            try
                            {
                                var list = (IList) Activator.CreateInstance(conversionType);
                                list.Add(value);
                                value = list;
                            }
                            catch (Exception exc)
                            {
                                var myexc =
                                    new ApplicationException(
                                        "Failed to convert Parameter Value to Type:" + conversionType.FullName +
                                        ". Parameter Id:" + mapping.FunctionParameter.Id + ". Original Value Type:" +
                                        value.GetType().FullName,
                                        exc);
                                SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                                throw myexc;
                            }
                        }
                        else if (value == null || conversionType.IsInstanceOfType(value))
                        {
                        }
                        else
                        {
                            var myexc =
                                new ApplicationException("Failed to convert Parameter Value to Type:" +
                                                         conversionType.FullName + ". Parameter Id:" +
                                                         mapping.FunctionParameter.Id + ". Original Value Type:" +
                                                         value.GetType().FullName);
                            SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                            throw myexc;
                        }
                    }
                    try
                    {
                        AppFieldEvaluator.SetAppFieldValue(
                            FunctionEvaluator.ParseParameter(null, mapping.Value).AppField, objEvent, value);
                    }
                    catch (Exception exc)
                    {
                        var myexc =
                            new ApplicationException(
                                String.Format("Accessor exception {0}.", new[] {exc.ToString()}), exc);
                        SiAuto.Si.GetSession(objCase.Id.ToString()).LogException(myexc);
                        throw myexc;
                    }
                }
                i++;
            }
        }

        #region Implementation of IWorkflowService

        public int? ProcessCase(long CaseId)
        {
            try
            {
                ProcessCaseInt(CaseId);
                return 0;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ProcessCaseOneWay(long CaseId)
        {
            try
            {
                ProcessCaseInt(CaseId);
            }
            catch (Exception)
            {

            }
        }

        #endregion

        #region Internal Classes

        internal class Event_EventType
        {
            internal Event Event;
            internal EventType EventType;
        }

        #endregion

        #region Compiled Queries

        internal static Func<DataContext, long, IQueryable<Event_EventType>>
            GetCurrentEvent =
                CompiledQuery.Compile(
                    (DataContext dataContext, long CaseId) =>
                    from objEvent in dataContext.Events
                    from objEventType in dataContext.EventTypes
                    where objEvent.EventTypeId == objEventType.Id
                          && objEvent.CaseId == CaseId
                    orderby objEvent.Id descending
                    select new Event_EventType {Event = objEvent, EventType = objEventType});

        #endregion
    }
}