using Exis.Domain;

namespace Exis.RuleEngine
{
    public static class ConditionEvaluator
    {
        public static bool ExecuteCondition(Condition condition, object instance)
        {
            if (condition.Operator == null)
            {
                //if (condition.Expression.Value.Contains("SystemData")) instance = null;
                return (bool)FunctionEvaluator.Evaluate(condition.Expression.Value, instance);
            }
            if (condition.Operator == ConditionOperatorEnum.AND)
            {
                condition.LeftCondition.context = condition.context;
                bool blLeftResult = ExecuteCondition(condition.LeftCondition, instance);
                if (!blLeftResult) return false;
                condition.RightCondition.context = condition.context;
                return ExecuteCondition(condition.RightCondition, instance);
            }
            else
            {
                condition.LeftCondition.context = condition.context;
                bool blLeftResult = ExecuteCondition(condition.LeftCondition, instance);
                if (blLeftResult) return true;
                condition.RightCondition.context = condition.context;
                return ExecuteCondition(condition.RightCondition, instance);
            }
        }
    }
}