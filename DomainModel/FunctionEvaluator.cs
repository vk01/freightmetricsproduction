﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Exis.Domain;
using Exis.RuleEngine.Attributes;
using Exis.SessionRegistry;
using Gurock.SmartInspect;

namespace Exis.RuleEngine
{
    public static class FunctionEvaluator
    {
        public static object Evaluate(string evaluationString, object runtimeObject)
        {
            return EvaluateParameter(ParseParameter(null, evaluationString), runtimeObject);
        }

        public static object Evaluate(AppFieldFunctionParameter parameter, object runtimeObject)
        {
            return EvaluateParameter(parameter, runtimeObject);
        }

        public static object Evaluate(AppFieldFunctionParameter parameter)
        {
            return Evaluate(parameter, null);
        }

        public static AppFieldFunctionParameter ParseParameter(string parameterString)
        {
            return ParseParameter(null, parameterString);
        }

        public static AppFieldFunctionParameter ParseParameter(AppFieldFunctionParameter parentParameter, string parameterString)
        {
            parameterString = parameterString.TrimStart('[').TrimEnd(']');
            if (String.IsNullOrEmpty(parameterString)) return null;

            AppFieldFunctionParameter parameter = new AppFieldFunctionParameter(parentParameter, parameterString);
            if (parameterString.StartsWith("fev"))
            {
                parameter.ParameterType = AppFieldParameterTypeEnum.Function;
                //                parameter.RuntimeObject = runtimeObject;
                ParseFunction(parameter);
            }
            else if (parameterString.StartsWith("afev"))
            {
                parameter.ParameterType = AppFieldParameterTypeEnum.ApplicationField;
                parameter.AppField = ParseAppField(parameter);
            }
            else if (parameterString.StartsWith("lv"))
            {
                parameter.ParameterType = AppFieldParameterTypeEnum.LiteralValue;
                parameter.LiteralValue = ParseLiteralValue(parameterString);
            }
            else
            {
                throw new ApplicationException("App Field string should start with 'fev', 'afev' or 'lv'. App Field:" + parameterString);
            }
            return parameter;
        }

        private static object EvaluateParameter(AppFieldFunctionParameter parameter, object runtimeObject)
        {
            if (parameter == null) return null;
            parameter.RuntimeObject = runtimeObject;

            if (parameter.ParameterType == AppFieldParameterTypeEnum.Function)
            {
                PrepareFunction(parameter);
                return EvaluateFunction(parameter);
            }
            if (parameter.ParameterType == AppFieldParameterTypeEnum.ApplicationField)
            {
                return EvaluateApplicationField(parameter);
            }
            if (parameter.ParameterType == AppFieldParameterTypeEnum.LiteralValue)
            {
                return EvaluateLiteralValue(parameter);
            }
            return null;
        }

        private static object EvaluateFunction(AppFieldFunctionParameter parameter)
        {
            try
            {
                if (parameter.RuntimeObject != null)
                {
                    var dataContextField = parameter.RuntimeObject.GetType().GetField("context", BindingFlags.Public | BindingFlags.Instance);
                    if (dataContextField != null)
                    {
                        DataContext dataContext = (DataContext)dataContextField.GetValue(parameter.RuntimeObject);
                        if (dataContext != null)
                        {
                            dataContextField = typeof(Functions).GetField("context",
                                                                          BindingFlags.Static | BindingFlags.Public);
                            if (dataContextField != null)
                            {
                                dataContextField.SetValue(null, dataContext);
                            }
                        }
                    }
                }

                foreach (AppFieldFunctionParameter functionParameter in parameter.FunctionParameters)
                {
                    if (functionParameter.IsFeeded) continue;
                    functionParameter.Value = EvaluateParameter(functionParameter, parameter.RuntimeObject);
                    foreach (AppFieldFunctionParameter appFieldFunctionParameter in parameter.FunctionParameters)
                    {
                        if (!String.IsNullOrEmpty(appFieldFunctionParameter.TypeDefiningParameter) &&
                            appFieldFunctionParameter.TypeDefiningParameter == functionParameter.Name)
                        {
                            appFieldFunctionParameter.Type = functionParameter.GetEvaluatedType();
                        }
                    }
                }
                return parameter.Function.Invoke(null, parameter.FunctionParameters.ToArray());
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException("Exception during application field function evaluation. Application Field Function: " + parameter, e);
                throw;
            }
        }

        private static object EvaluateApplicationField(AppFieldFunctionParameter parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter.AppField))
                {
                    if (parameter.AppField.StartsWith("eval"))
                    {
                        const int startIndex = 5;
                        int length = parameter.AppField.Length - startIndex - 1;
                        return AppFieldEvaluator.GetEvaluatedString(parameter.AppField.Substring(startIndex, length),
                                                                    parameter.RuntimeObject);
                    }
                    return AppFieldEvaluator.GetAppFieldValue(parameter.AppField, parameter.RuntimeObject);
                }
                return parameter.RuntimeObject;
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException("Exception during application field evaluation. Application Field Path: " + parameter, e);
                throw;
            }
        }

        private static object EvaluateLiteralValue(AppFieldFunctionParameter parameter)
        {
            try
            {
                if (!String.IsNullOrEmpty(parameter.LiteralValue))
                {
                    if (parameter.LiteralValue.Contains("&sep;"))
                    {
                        List<object> literalValues = new List<object>();
                        foreach (string literalValue in parameter.LiteralValue.Split(new[] { "&sep;" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            literalValues.Add(ChangeType(literalValue, parameter.Type));
                        }
                        return literalValues;
                    }
                    if (parameter.Type != null)
                        return ChangeType(parameter.LiteralValue, parameter.Type);
                    return parameter.LiteralValue;
                }
                return null;
            }
            catch (Exception e)
            {
                SiAuto.Main.LogException("Exception during literal value evaluation. Literal Value: " + parameter, e);
                throw;
            }
        }

        private static object ChangeType(string value, Type conversionType)
        {
            if(conversionType.IsGenericType && conversionType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                conversionType = conversionType.GetGenericArguments()[0];
            }
            if (!String.IsNullOrEmpty(value))
            {
                if (conversionType == typeof(TimeSpan))
                {
                    return TimeSpan.Parse(value);
                }
                if (conversionType.IsEnum)
                {
                    return Enum.Parse(conversionType, value);
                }
                if (conversionType == typeof(DomainObject) || conversionType.IsSubclassOf(typeof(DomainObject)))
                {
                    var id = value.Substring(0, value.IndexOf(','));
                    var type = Type.GetType(value.Substring(value.IndexOf(',') + 1));
                    var obj = Activator.CreateInstance(type);
                    type.GetProperty("Id").SetValue(obj, Convert.ToInt64(id), null);
                    return obj;
                }
                if (conversionType == typeof(byte[]))
                {
                    return Convert.FromBase64String(value);
                }
                return Convert.ChangeType(value, conversionType, ServerSessionRegistry.ServerCultureInfo);
            }
            return null;
        }

        private static void PrepareFunction(AppFieldFunctionParameter parameter)
        {
            foreach (AppFieldFunctionParameter functionParameter in parameter.FunctionParameters)
            {
                int index = parameter.FunctionParameters.IndexOf(functionParameter);
                var methodParameter = parameter.Function.GetParameters()[index];
                functionParameter.Name = methodParameter.Name;
                RuleParameterAttribute attribute =
                    (RuleParameterAttribute)
                    methodParameter.GetCustomAttributes(typeof(RuleParameterAttribute), false)[0];
                functionParameter.Attribute = attribute;
                functionParameter.Type = attribute.Type;
                functionParameter.TypeDefiningParameter = attribute.TypeDefiningParameter;
                functionParameter.IsFeeded = attribute.IsFeeded;
            }
        }

        private static void ParseFunction(AppFieldFunctionParameter parameter)
        {
            string functionString = parameter.ParameterString;
            int functionDataStart = functionString.IndexOf('(') + 1;
            int functionDataLength = functionString.LastIndexOf(')') - functionDataStart;
            string functionData = functionString.Substring(functionDataStart, functionDataLength);
            string functionName = functionData.Substring(0, functionData.IndexOf(','));

            var functionParameters = SplitParameters(functionData.Substring(functionData.IndexOf(',') + 1));
            List<AppFieldFunctionParameter> functionParametersArray = new List<AppFieldFunctionParameter>();
            foreach (string functionParameter in functionParameters)
            {
                functionParametersArray.Add(ParseParameter(parameter, functionParameter));
            }

            MethodInfo info = FindFunction(functionName, functionParametersArray.Count);
            if (info == null)
                throw new ApplicationException(String.Format("No method with name {0} with {1} parameters was found.",
                                                             functionName, functionParametersArray.Count));
            parameter.Function = info;
            parameter.FunctionParameters = functionParametersArray;
        }

        private static string ParseAppField(AppFieldFunctionParameter parameter)
        {
            string appFieldString = parameter.ParameterString;
            int functionDataStart = appFieldString.IndexOf('(') + 1;
            int functionDataLength = appFieldString.LastIndexOf(')') - functionDataStart;
            //            parameter.AppField = appFieldString.Substring(functionDataStart, functionDataLength);
            return appFieldString.Substring(functionDataStart, functionDataLength);
            //            parameter.RuntimeObject = runtimeObject;
        }

        private static string ParseLiteralValue(string literalValueString)
        {
            int functionDataStart = literalValueString.IndexOf('(') + 1;
            int functionDataLength = literalValueString.LastIndexOf(')') - functionDataStart;
            return literalValueString.Substring(functionDataStart, functionDataLength);
        }

        private static string GetParameter(string parameterString)
        {
            string parameter = "";
            int counter = 0;
            foreach (char c in parameterString)
            {
                parameter += c;
                if (c == '[')
                    counter++;
                else if (c == ']')
                    counter--;
                if (counter == 0 && c == ',')
                {
                    if (!String.IsNullOrEmpty(parameter))
                    {
                        break;
                    }
                }
            }
            return parameter;
        }

        private static string[] SplitParameters(string parametersString)
        {
            List<string> parameters = new List<string>();
            while (!String.IsNullOrEmpty(parametersString))
            {
                string parameter = GetParameter(parametersString);
                parametersString = parametersString.Substring(parameter.Length);
                parameters.Add(parameter.Trim().TrimEnd(','));
            }
            return parameters.ToArray();
        }

        private static MethodInfo FindFunction(string functionName, int parameterCount)
        {
            foreach (MethodInfo info in typeof(Functions).GetMethods(BindingFlags.Public | BindingFlags.Static))
            {
                if (info.Name == functionName)
                {
                    if (info.GetParameters().Length == parameterCount)
                    {
                        return info;
                    }
                }
            }
            return null;
        }
    }
}
