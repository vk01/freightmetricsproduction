﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Exis.Domain.Attributes;
using Exis.RuleEngine;

namespace Exis.Domain
{
    public static class SystemData
    {
        [ThreadStatic] public static DataContext context;
        [ThreadStatic] private static Decimal m_RowCount;
        [ThreadStatic] private static Decimal m_RowIndex;
        [ThreadStatic] private static Decimal m_SubReportRowCount;
        [ThreadStatic] private static Decimal m_CurrentPageNumber;

        #region Lookup Data

        [AppFieldPropertyValue("Running Report Row Count", "The number of rows in the datasource of the current report")
        ]
        public static Decimal RowCount
        {
            get { return m_RowCount; }
            set { m_RowCount = value; }
        }

        [AppFieldPropertyValue("Running Report Row Index", "The row index in the datasource of the current report")]
        public static Decimal RowIndex
        {
            set { m_RowIndex = value; }
            get { return m_RowIndex; }
        }

        [AppFieldPropertyValue("Running Subreport Row Count",
            "The number of rows in the datasource of the subreport to be printed")]
        public static Decimal SubReportRowCount
        {
            set { m_SubReportRowCount = value; }
            get { return m_SubReportRowCount; }
        }

        [AppFieldPropertyValue("Current Page Number", "The current page number")]
        public static Decimal CurrentPageNumber
        {
            set { m_CurrentPageNumber = value; }
            get { return m_CurrentPageNumber; }
        }

        public static List<string> AllViews()
        {
            var viewNames = new List<string>();
            foreach (DataRow row in context.Connection.GetSchema("Views").Rows)
            {
                viewNames.Add(row[1].ToString());
            }
            return viewNames.OrderBy(a => a.ToString()).ToList();
        }

        public static List<string> ViewColumns(string viewName)
        {
            var columns = new List<string>();
            foreach (DataRow row in context.Connection.GetSchema("Columns", new[] {null, viewName}).Rows)
            {
                if (!columns.Contains(row[2].ToString()))
                    columns.Add(row[2].ToString());
            }
            return columns;
        }

        #endregion

        #region Configuration Data

        public static List<CaseEventStatusEnum> CaseStatuses()
        {
            return new List<CaseEventStatusEnum> {CaseEventStatusEnum.Opened, CaseEventStatusEnum.Closed};
        }

        [AppFieldMethodTypedDomain("Latest created Closed Case of Type", "Latest created Closed Case of Type.",
            "ActiveCaseTypes,Exis.Domain.SystemData,Exis.DomainModel")]
        public static Case GetLatestCreatedClosedCase(long CaseTypeId)
        {
            return (from objCase in context.Cases
                    where objCase.CaseTypeId == CaseTypeId
                          && objCase.Status_Str == CaseEventStatusEnum.Closed.ToString("d")
                    orderby objCase.DateFrom descending
                    select objCase).Take(1).SingleOrDefault();
        }

        [AppFieldMethodTypedDomain("Latest created Open Case of Type", "Latest created Open Case of Type.",
            "ActiveCaseTypes,Exis.Domain.SystemData,Exis.DomainModel")]
        public static Case GetLatestCreatedOpenCase(long CaseTypeId)
        {
            return (from objCase in context.Cases
                    where objCase.CaseTypeId == CaseTypeId
                          && objCase.Status_Str == CaseEventStatusEnum.Opened.ToString("d")
                    orderby objCase.DateFrom descending
                    select objCase).Take(1).SingleOrDefault();
        }

        private static List<CaseType> ActiveCaseTypes()
        {
            return context.CaseTypes.Where(a => a.Status_Str == "1").ToList();
        }

        [AppFieldMethodTypedDomain("Active Jobs", "Active Jobs of the system.",
            "ActiveJobs,Exis.Domain.SystemData,Exis.DomainModel")]
        public static Job GetActiveJob(long Id)
        {
            return context.Jobs.Where(a => a.Id == Id).Single();
        }

        private static List<Job> ActiveJobs()
        {
            return context.Jobs.Where(a => a.Status_Str == "0").ToList();
        }

        [AppFieldMethodSegmentation("Segmentations", "Active segmentations")]
        public static ExtendedList<DomainObject> GetActiveSegmentation(long Id)
        {
            return new ExtendedList<DomainObject>(new object[] {Id}, GetSegmentation, null);
        }

        private static object GetSegmentation(object[] parameters)
        {
            Segmentation seg = context.Segmentations.Where(a => a.Id.ToString() == parameters[0].ToString()).Single();
            seg.context = context;

            if (seg.Type == SegmentationTypeEnum.Case)
            {
                IQueryable<Case> query =
                    from objCase in context.Cases.AsExpandable()
                    where objCase.DateTo == null
                    select objCase;

                Condition condition = context.Conditions.Where(a => a.Id == seg.ConditionId).Single();
                condition.context = context;
                // Expression<Func<Case, bool>> expression = ProcessCaseCondition(condition);
                // query = query.Where(expression);
                return query.ToList();
            }
            throw new ApplicationException("Segmentation type " + seg.Type.ToString("g") + " not implemented.");
        }

        private static Expression<Func<UdfsValue, bool>> ProcessPredicate(long udfId, CompareOperator op,
                                                                          object objValue)
        {
            BinaryExpression binaryExpression;
            ParameterExpression parameter = System.Linq.Expressions.Expression.Parameter(typeof (UdfsValue), "b");

            System.Linq.Expressions.Expression convertedExpression =
                System.Linq.Expressions.Expression.Property(parameter, "Value");

            if (objValue is DateTime || objValue is Decimal)
            {
                convertedExpression = System.Linq.Expressions.Expression.Call(
                    convertedExpression,
                    typeof (String).GetMethod("Substring", new[] {typeof (int), typeof (int)}),
                    System.Linq.Expressions.Expression.Constant(0, typeof (int)),
                    System.Linq.Expressions.Expression.Constant(4000, typeof (int)));
                if (objValue is DateTime)
                {
                    convertedExpression =
                        System.Linq.Expressions.Expression.Call(
                            typeof (Convert).GetMethod("ToDateTime", new[] {typeof (String)}), convertedExpression);
                }
                else if (objValue is Decimal)
                {
                    convertedExpression =
                        System.Linq.Expressions.Expression.Call(
                            typeof (Convert).GetMethod("ToDecimal", new[] {typeof (String)}), convertedExpression);
                }
            }
            else if (objValue is Boolean)
            {
                objValue = ((bool) objValue) ? "1" : "0";
            }

            Expression<Func<UdfsValue, bool>> predicate = b => b.UdfId == udfId;
            if (op == CompareOperator.GreaterThan)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.GreaterThan(convertedExpression,
                                                                   System.Linq.Expressions.Expression.
                                                                       Constant(objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.GreaterThanOrEqual)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.GreaterThanOrEqual(convertedExpression,
                                                                          System.Linq.Expressions.Expression
                                                                              .Constant(objValue,
                                                                                        objValue.GetType()));
            }
            else if (op == CompareOperator.Equals)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.Equal(convertedExpression,
                                                             System.Linq.Expressions.Expression.Constant(
                                                                 objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.NotEquals)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.NotEqual(convertedExpression,
                                                                System.Linq.Expressions.Expression.Constant(
                                                                    objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.LessThan)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.LessThan(convertedExpression,
                                                                System.Linq.Expressions.Expression.Constant(
                                                                    objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.LessThanOrEqual)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.LessThanOrEqual(convertedExpression,
                                                                       System.Linq.Expressions.Expression.
                                                                           Constant(objValue,
                                                                                    objValue.GetType()));
            }
            else
            {
                throw new ApplicationException("Cannot evaluate comparison operator Null");
            }

            predicate =
                predicate.And(
                    System.Linq.Expressions.Expression.Lambda<Func<UdfsValue, bool>>(binaryExpression,
                                                                                     parameter));
            return predicate;
        }

        private static void GetExpressionParameters(Condition condition, out Udf udf, out object objValue,
                                                    out CompareOperator op)
        {
            string strExpressionValue = condition.Expression.Value;
            AppFieldFunctionParameter expression = FunctionEvaluator.ParseParameter(null, strExpressionValue);
            udf = null;
            objValue = null;
            op = CompareOperator.Null;

            if (expression.ParameterType == AppFieldParameterTypeEnum.Function &&
                expression.FunctionParameters.Count == 3)
            {
                if (expression.FunctionParameters[0].ParameterType == AppFieldParameterTypeEnum.ApplicationField)
                {
                    string appField = expression.FunctionParameters[0].AppField;
                    int startIndex = appField.LastIndexOf('(') + 1;
                    int length = appField.LastIndexOf(')') - startIndex;
                    string udfCode = appField.Substring(startIndex, length);
                    udf =
                        context.Udfs.Single(a => a.RefType_Str == condition.Expression.Type_Str && a.Code == udfCode);
                }
                if (expression.FunctionParameters[2].ParameterType == AppFieldParameterTypeEnum.LiteralValue)
                {
                    expression.FunctionParameters[2].Type = GetUdfType(udf);
                    objValue = FunctionEvaluator.Evaluate(expression.FunctionParameters[2], null);
                }
                if (objValue == null)
                {
                    throw new ApplicationException("Could not evaluate");
                }
                expression.FunctionParameters[1].Type = typeof (CompareOperator);
                op = (CompareOperator) FunctionEvaluator.Evaluate(expression.FunctionParameters[1], null);
            }
        }

//        private static Expression<Func<Case, bool>> ProcessCaseCondition(Condition condition)
//        {
//            if (condition.Operator == null)
//            {
//                Udf udf;
//                object objValue;
//                CompareOperator op;
//                GetExpressionParameters(condition, out udf, out objValue, out op);

//                if (udf != null)
//                {
//                    if (udf.IsDefault && udf.DefaultType == UDFDefaultTypeEnum.Runtime)
//                    {
//                        var udfExpression = FunctionEvaluator.ParseParameter(null, udf.DefaultAppField);
//                        if (udfExpression.ParameterType == AppFieldParameterTypeEnum.Function)
//                        {
//                            if (udfExpression.Function.Name == "GetDbViewValue")
//                            {
//                                var query = context.Query<Customer>(
//                                    String.Format(
//                                        @"select vw.ca_id, null CuId, null CtId, null Status, null ParId, 
//                                            null Result, null WdvId, null WrIdEntry, null WrIdExit, null RefScopeId, null ActStatus, 
//                                            null Cruser, null Crd, null Chuser, null Chd, null Crappuser, null Crappd
//                                            null Chappuser, null Chappd, null Priority, null Datf, null Datt from {0} vw where vw.{1} = '{2}'",
//                                        udfExpression.FunctionParameters[0].LiteralValue,
//                                        udfExpression.FunctionParameters[1].LiteralValue, objValue));
//                                return z => (query.AsExpandable().Select(a => a.Id)).Contains(z.Id);
//                            }
//                        }
//                    }
//                    var predicate = ProcessPredicate(udf.Id, op, objValue);
//                    return a => (context.UdfsValues.AsExpandable().Where(predicate).Select(b => b.RefId)).Contains(a.Id);
//                }
//                throw new ApplicationException("Udf was null.");
//            }
//            if (condition.Operator == ConditionOperatorEnum.AND)
//            {
//                condition.LeftCondition.context = condition.context;
//                condition.RightCondition.context = condition.context;
//                return ProcessCaseCondition(condition.LeftCondition).And(ProcessCaseCondition(condition.RightCondition));
//            }
//            condition.LeftCondition.context = condition.context;
//            condition.RightCondition.context = condition.context;
//            return ProcessCaseCondition(condition.LeftCondition).Or(ProcessCaseCondition(condition.RightCondition));
//        }

        private static Type GetUdfType(Udf udf)
        {
            switch (udf.Type)
            {
                case UDFTypeEnum.Date:
                    return typeof (DateTime);
                case UDFTypeEnum.Numeric:
                    return typeof (Decimal);
                case UDFTypeEnum.Status:
                    return typeof (Boolean);
                case UDFTypeEnum.Image:
                case UDFTypeEnum.File:
                    return typeof (byte[]);
                case UDFTypeEnum.InternalLink:
                    return typeof (DomainObject);
                default:
                    return typeof (String);
            }
        }

        private static List<Segmentation> ActiveSegmentations()
        {
            return context.Segmentations.ToList();
        }

        #endregion

        #region System Date & Time

        [AppFieldPropertyValue("Server Current Date", "The Current Server Date")]
        public static DateTime GetCurrentDate
        {
            get { return DateTime.Now; }
        }

        [AppFieldPropertyValue("Day Of The Week", "Day Of The Week")]
        public static DayOfWeek DayOfTheWeek
        {
            get
            {
                DateTime dateTime = DateTime.Now;
                return dateTime.DayOfWeek;
            }
        }

        [AppFieldPropertyValue("Day Of The Month", "Day Of The Month")]
        public static int DayOfTheMonth
        {
            get
            {
                DateTime dateTime = DateTime.Now;
                return dateTime.Day;
            }
        }

        [AppFieldPropertyValue("Day Of The Year", "Day Of The Year")]
        public static int DayOfTheYear
        {
            get
            {
                DateTime dateTime = DateTime.Now;
                return dateTime.DayOfYear;
            }
        }

        [AppFieldPropertyValue("Hour", "Hour")]
        public static int Hour
        {
            get
            {
                DateTime dateTime = DateTime.Now;
                return dateTime.Hour;
            }
        }

        [AppFieldPropertyValue("Is Today a Week Day", "Every day besides Saturday and Sunday")]
        public static bool IsTodayWeekDay()
        {
            DateTime dateTime = DateTime.Now;

            if (dateTime.DayOfWeek != DayOfWeek.Saturday && dateTime.DayOfWeek != DayOfWeek.Sunday) return true;
            return false;
        }

        [AppFieldPropertyValue("Is Today a Weekend Day", "Saturday or Sunday")]
        public static bool IsTodayWeekendDay()
        {
            DateTime dateTime = DateTime.Now;

            if (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday) return true;
            return false;
        }

        #endregion

        #region True/False

        [AppFieldPropertyValue("True value", "True value")]
        public static bool True
        {
            get { return true; }
        }

        #endregion
    }
}