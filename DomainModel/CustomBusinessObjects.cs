﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{

    [DataContract]
    public class TradeVersionInfo
    {
        [DataMember]
        public long TradeId { get; set; }
        [DataMember]
        public long TradeInfoId { get; set; }
        [DataMember]
        public DateTime DateFrom { get; set; }
        [DataMember]
        public DateTime? DateTo { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string ExternalCode { get; set; }
    }

    [DataContract]
    public class HierarchyNodeInfo
    {
        [DataMember]
        public DomainObject DomainObject { get; set; }
        [DataMember]
        public string StrObject { get; set; }
        [DataMember]
        public HierarchyNodeType HierarchyNodeType { get; set; }
    }

    [DataContract]
    public class CustomBook
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public long? ParentBookId { get; set; }
        [DataMember]
        public ActivationStatusEnum Status { get; set; }
        [DataMember]
        public BookPositionCalculatedByDaysEnum PositionCalculatedByDays { get; set; }
        [DataMember]
        public CustomBook ParentBook { get; set; }
        [DataMember]
        public List<CustomBook> ChildrenBooks { get; set; }
    }

    [DataContract]
    public class ParseExcelIndexValue
    {
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string RouteId { get; set; }
        [DataMember]
        public string ReportDesc { get; set; }
        [DataMember]
        public string ClosingPrice { get; set; }
        [DataMember]
        public string Unit { get; set; }
    }

}
