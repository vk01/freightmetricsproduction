﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using Exis.Domain;
using Exis.Domain.Attributes;

namespace Exis.RuleEngine
{
    public static class AppFieldEvaluator
    {
        private static MemberInfo GetAppFieldMember(Type baseType, string member)
        {
            MemberInfo[] info = member.Contains("(")
                                    ? baseType.GetMember(member.Substring(0, member.IndexOf('(')))
                                    : baseType.GetMember(member);

            if (info.Length == 1)
            {
                return info[0];
            }
            return null;
        }

        public static void SetAppFieldValue(string appFieldPath, object obj, object value)
        {
            List<string> appFields = new List<string>();
            Regex splitingAppFieldRegex = new Regex(@"[\w\W\S\D\s\d]+?(->)");
            Match splitMatch = splitingAppFieldRegex.Match(appFieldPath);

            while (splitMatch != Match.Empty)
            {
                appFields.Add(splitMatch.Value.Replace("->", " ").Trim());
                appFieldPath = appFieldPath.Remove(0, splitMatch.Length).Trim();
                splitMatch = splitMatch.NextMatch();
            }

            object executingObject = ExecuteAppField(appFields, obj);
            MemberInfo info = GetAppFieldMember(executingObject.GetType(), appFieldPath);
            if (info != null)
            {
                if (info is PropertyInfo)
                {
                    PropertyInfo property = (PropertyInfo) info;
                    if (property.PropertyType.IsEnum && value is string)
                    {
                        property.SetValue(executingObject, Enum.Parse(property.PropertyType, (string)value), null);
                    }
                    else
                    {
                        property.SetValue(executingObject, value, null);
                    }
                }
            }
        }

        internal static object GetAppFieldValue(string appFieldPath, object obj)
        {
            try
            {
                List<string> appFields = new List<string>();

                string[] appFieldPaths = appFieldPath.Split(new[] { "->" }, StringSplitOptions.None);
                foreach (string path in appFieldPaths)
                {
                    appFields.Add(path);
                }

                return ExecuteAppField(appFields, obj);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Could not evaluate application field " + appFieldPath, e);
            }
        }

        internal static Type GetAppFieldType(string appFieldPath, object obj)
        {
            try
            {
                List<string> appFields = new List<string>();

                string[] appFieldPaths = appFieldPath.Split(new[] { "->" }, StringSplitOptions.None);
                foreach (string path in appFieldPaths)
                {
                    appFields.Add(path);
                }
                appFields.RemoveAt(appFields.Count - 1);
                object executingObject = ExecuteAppField(appFields, obj);
                var memberInfo = GetAppFieldMember(executingObject.GetType(), appFieldPaths[appFieldPaths.Length - 1]);
                if(memberInfo is PropertyInfo)
                {
                    return ((PropertyInfo) memberInfo).PropertyType;
                }
                return ((MethodInfo) memberInfo).ReturnType;
            }
            catch (Exception e)
            {
                throw new ApplicationException("Could not evaluate application field " + appFieldPath, e);
            }
        }

        private static object ExecuteAppField(List<string> appFields, object obj)
        {
            object oRet;
//            if(obj == null)
//                throw new ApplicationException("Evaluation object is null.");

            DataContext dataContext = null;
            if (obj != null)
            {
                FieldInfo contextFieldInfo = obj.GetType().GetField("context",
                                                                    BindingFlags.Instance | BindingFlags.Public);
                if (contextFieldInfo != null && contextFieldInfo.FieldType == typeof (DataContext))
                {
                    dataContext = (DataContext) contextFieldInfo.GetValue(obj);
                }
            }

            int appFieldsCounter = 0;
            if (obj == null || obj.GetType().Name != appFields[0])
            {
                oRet = TryRunStaticAppField(appFields, dataContext);
            }
            else
            {
                object returnObject = obj;
                if (obj.GetType().Name == appFields[0])
                    appFieldsCounter++;
                while (appFields.Count > appFieldsCounter)
                {
                    object executingObject = returnObject;
                    string path = appFields[appFieldsCounter];
                    returnObject = ExecuteStep(path, executingObject, dataContext);
                    appFieldsCounter++;
                }
                oRet = returnObject;
            }
            return oRet;
        }

        private static object TryRunStaticAppField(List<string> appFields, DataContext dataContext)
        {
            string typeString = appFields[0];
            Type selType = null;
            int appFieldsCounter = 0;
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies)
            {
                if (!ass.GlobalAssemblyCache)
                {
                    //TODO must be eliminated. Only here because of evaluation in admin client
                    if (ass.ManifestModule.Name == "CRMDomainModel.dll") 
                        continue; 
                    Type[] types = ass.GetTypes();
                    foreach (Type type in types)
                    {
                        if (type.Name == typeString)
                        {
                            selType = type;
                            break;
                        }
                    }
                }
                if (selType != null)
                    break;
            }

            if (selType != null && selType.IsAbstract && selType.IsSealed)
            {
            }
            else
                throw new ApplicationException("The application fields run-time object is not set.\r\n Faild to Proccess the Application Field.");

            object returnObject = null;
            appFieldsCounter++;
            while (appFields.Count > appFieldsCounter)
            {
                string path = appFields[appFieldsCounter];
                returnObject = returnObject == null
                                   ? ExecuteStaticStep(path, selType, dataContext)
                                   : ExecuteStep(path, returnObject, dataContext);

                appFieldsCounter++;
            }
            return returnObject;
        }

        private static object ExecuteStep(string path, object obj, DataContext dataContext)
        {
            object oRet;
            if (path.Contains("("))
            {
                string name = path.Substring(0, path.IndexOf('('));
                MethodInfo method = obj.GetType().GetMethod(name);

                if (method != null)
                {
                    try
                    {
                        oRet = method.Invoke(obj, PrepareMethodParameters(method, path));
                    }
                    catch (TargetInvocationException e)
                    {
                        throw e.InnerException;
                    }
                }
                else
                {
                    throw new PropertyNotFoundException(path, obj);
                }
            }
            else
            {
                PropertyInfo property = obj.GetType().GetProperty(path);

                if (property != null)
                {
                    try
                    {
                        oRet = property.GetValue(obj, null);
                    }
                    catch (TargetInvocationException e)
                    {
                        throw e.InnerException;
                    }
                }
                else
                {
                    throw new PropertyNotFoundException(path, obj);
                }
            }
            if (oRet != null)
            {
                FieldInfo contextFieldInfo = oRet.GetType().GetField("context",
                                                                     BindingFlags.Public);
                if (contextFieldInfo != null && contextFieldInfo.FieldType == typeof(DataContext))
                {
                    contextFieldInfo.SetValue(oRet, dataContext);
                }
            }

            return oRet;
        }

        private static object ExecuteStaticStep(string path, Type type, DataContext dataContext)
        {
            object oRet = null;

            FieldInfo contextFieldInfo = type.GetField("context",
                                 BindingFlags.Static | BindingFlags.Public);
            if (contextFieldInfo != null && contextFieldInfo.FieldType == typeof(DataContext))
            {
                contextFieldInfo.SetValue(null, dataContext);
            }

            if (path.Contains("("))
            {
                string name = path.Substring(0, path.IndexOf('('));
                MethodInfo method = type.GetMethod(name);
                if (method != null)
                {
                    try
                    {
                        oRet = method.Invoke(null, PrepareMethodParameters(method, path));
                        if (method.GetCustomAttributes(typeof(AppFieldMethodUDFAttribute), false).Length == 1)
                        {
                            PropertyInfo valProperty = method.ReturnType.GetProperty("Value");
                            if (valProperty != null)
                                oRet = valProperty.GetValue(oRet, null);
                        }
                    }
                    catch (Exception e)
                    {
                        Exception ex = e;

                        while (ex.InnerException != null)
                        {
                            ex = ex.InnerException;
                        }
                        throw ex;
                    }
                }
            }
            else
            {
                PropertyInfo property = type.GetProperty(path);

                if (property != null)
                    oRet = property.GetValue(null, null);
            }
            return oRet;
        }

        private static object[] PrepareMethodParameters(MethodInfo methodInfo, string appFieldPath)
        {
            ParameterInfo[] methodParameters = methodInfo.GetParameters();
            object[] parameters = new object[methodParameters.Length];
            if (methodParameters.Length > 0)
            {
                int startindex = appFieldPath.IndexOf('(');
                string[] stringParameters =
                    appFieldPath.Substring(startindex + 1, (appFieldPath.Length - startindex) - 2).Split(
                        new[] { "," },
                        StringSplitOptions
                            .None);

                for (int i = 0; i < stringParameters.Length; i++)
                {
                    object objParameter = null;
                    if (methodParameters[i].ParameterType.IsGenericType && methodParameters[i].ParameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        if (!string.IsNullOrEmpty(stringParameters[i]))
                        {
                            if (methodParameters[i].ParameterType.GetGenericArguments()[0].IsEnum)
                            {
                                objParameter = Enum.Parse(methodParameters[i].ParameterType.GetGenericArguments()[0],
                                                          stringParameters[i]);
                            }
                            else
                            {
                                objParameter = Convert.ChangeType(stringParameters[i],
                                                                  methodParameters[i].ParameterType.GetGenericArguments()
                                                                      [0]);
                            }
                        }
                    }
                    else
                    {
                        objParameter = methodParameters[i].ParameterType.IsEnum
                                           ? Enum.Parse(methodParameters[i].ParameterType, stringParameters[i])
                                           : Convert.ChangeType(stringParameters[i], methodParameters[i].ParameterType);
                    }
                    parameters[i] = objParameter;
                }
            }
            return parameters;
        }

        internal static string GetEvaluatedString(string inputString, object runtimeObject)
        {
            string evaluatedString = "";
            while (true)
            {
                int startIndex = inputString.IndexOf('[');
                if (startIndex < 0)
                {
                    evaluatedString += inputString;
                    break;
                }
                
                evaluatedString += inputString.Substring(0, startIndex);
                inputString = inputString.Substring(startIndex);
                if (!inputString.Substring(1).StartsWith("fev") && !inputString.Substring(1).StartsWith("afev"))
                {
                    evaluatedString += inputString[0];
                    inputString = inputString.Substring(1);
                    continue;
                }

                string appField = GetAppField(inputString);
                inputString = inputString.Substring(appField.Length);
                evaluatedString += FunctionEvaluator.Evaluate(appField, runtimeObject);
            }
            return evaluatedString;
        }

        private static string GetAppField(string parameterString)
        {
            string parameter = "";
            int counter = 0;
            foreach (char c in parameterString)
            {
                parameter += c;
                if (c == '[')
                    counter++;
                else if (c == ']')
                    counter--;
                if (counter == 0)
                {
                    if (!String.IsNullOrEmpty(parameter))
                    {
                        break;
                    }
                }
            }
            return parameter;
        }
    }
}