﻿using System;

namespace Exis.RuleEngine
{
    public class RuleTypeAppField
    {
        private string m_AppFieldPath;

        private object m_LiteralValue;
        private Type m_returnedType;
        private object m_StartAppObject;
        private AppRuleTypeEnum m_ValueType;

        public RuleTypeAppField(string appField, object runtimeobj)
        {
            m_AppFieldPath = appField;
            m_ValueType = AppRuleTypeEnum.ApplicationField;
            m_StartAppObject = runtimeobj;
        }

        public RuleTypeAppField(object val)
        {
            m_LiteralValue = val;
            m_ValueType = AppRuleTypeEnum.LiteralValue;
        }

        public RuleTypeAppField(string appField, Type returnedType)
        {
            m_AppFieldPath = appField;
            m_ValueType = AppRuleTypeEnum.ApplicationField;
            m_returnedType = returnedType;
        }

        public RuleTypeAppField(object val, Type returnedType)
        {
            m_LiteralValue = val;
            m_ValueType = AppRuleTypeEnum.LiteralValue;
            m_returnedType = returnedType;
        }

        public string AppFieldPath
        {
            get { return m_AppFieldPath; }
        }

        public object StartAppObject
        {
            get { return m_StartAppObject; }
            set { m_StartAppObject = value; }
        }

        public object Value
        {
            get
            {
                if (m_ValueType == AppRuleTypeEnum.LiteralValue)
                    return m_LiteralValue;
                else if (m_ValueType == AppRuleTypeEnum.ApplicationField &&
                         m_StartAppObject != null)
                    return RunAppField();
                else
                    return null;
            }
        }

        private object RunAppField()
        {
            object obj = null;

            return obj;
        }

        public override string ToString()
        {
            return "<" + m_ValueType + ":" + m_returnedType + ":" +
                   ((m_ValueType == AppRuleTypeEnum.ApplicationField)
                        ? m_AppFieldPath
                        : m_LiteralValue.ToString()) + ">";
            ;
        }

        #region Nested type: AppRuleTypeEnum

        private enum AppRuleTypeEnum
        {
            ApplicationField = 1,
            LiteralValue = 2
        } ;

        #endregion
    }
}