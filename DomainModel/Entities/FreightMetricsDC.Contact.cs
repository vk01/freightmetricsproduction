//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// CONTACTS
    /// </summary>
    /// <LongDescription>
    /// CONTACTS
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class Contact : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private string _Name;

        private ContactTypeEnum _Type;

        private ActivationStatusEnum _Status = (ActivationStatusEnum)Enum.Parse(typeof(ActivationStatusEnum), "0");

        private string _Phone;

        private string _Email;

        private string _Fax;

        private string _Cruser;

        private System.DateTime _Crd;

        private string _Chuser;

        private System.DateTime _Chd;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnNameChanging(string value);
        partial void OnNameChanged();
        partial void OnTypeChanging(ContactTypeEnum value);
        partial void OnTypeChanged();
        partial void OnStatusChanging(ActivationStatusEnum value);
        partial void OnStatusChanged();
        partial void OnPhoneChanging(string value);
        partial void OnPhoneChanged();
        partial void OnEmailChanging(string value);
        partial void OnEmailChanged();
        partial void OnFaxChanging(string value);
        partial void OnFaxChanged();
        partial void OnCruserChanging(string value);
        partial void OnCruserChanged();
        partial void OnCrdChanging(System.DateTime value);
        partial void OnCrdChanged();
        partial void OnChuserChanging(string value);
        partial void OnChuserChanged();
        partial void OnChdChanging(System.DateTime value);
        partial void OnChdChanged();
        #endregion

        public Contact()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// Internal ID
        /// </summary>
        /// <LongDescription>
        /// Internal ID
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// Name
        /// </summary>
        /// <LongDescription>
        /// Name
        /// </LongDescription>
        [DataMember(Order=2)]
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if (this._Name != value)
                {
                    this.OnNameChanging(value);
                    this.SendPropertyChanging();
                    this._Name = value;
                    this.SendPropertyChanged("Name");
                    this.OnNameChanged();
                }
            }
        }

    
        /// <summary>
        /// 1 = Vessel Technical Manager, 2 = Bank Relationship Manager, 3 = Vessel, 4 = Vessel Master Officer, 5 = Vessel Chief Officer
        /// </summary>
        /// <LongDescription>
        /// 1 = Vessel Manager, 2 = Bank Relationship Manager
        /// </LongDescription>
        [DataMember(Order=3)]
        public ContactTypeEnum Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if (this._Type != value)
                {
                    this.OnTypeChanging(value);
                    this.SendPropertyChanging();
                    this._Type = value;
                    this.SendPropertyChanged("Type");
                    this.OnTypeChanged();
                }
            }
        }

    
        /// <summary>
        /// The status. Possible values are: 0 = Inactive, 1 = Active.
        /// </summary>
        /// <LongDescription>
        /// The status. Possible values are: 0 = Inactive, 1 = Active.
        /// </LongDescription>
        [DataMember(Order=4)]
        public ActivationStatusEnum Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                if (this._Status != value)
                {
                    this.OnStatusChanging(value);
                    this.SendPropertyChanging();
                    this._Status = value;
                    this.SendPropertyChanged("Status");
                    this.OnStatusChanged();
                }
            }
        }

    
        /// <summary>
        /// Phone
        /// </summary>
        [DataMember(Order=5)]
        public string Phone
        {
            get
            {
                return this._Phone;
            }
            set
            {
                if (this._Phone != value)
                {
                    this.OnPhoneChanging(value);
                    this.SendPropertyChanging();
                    this._Phone = value;
                    this.SendPropertyChanged("Phone");
                    this.OnPhoneChanged();
                }
            }
        }

    
        /// <summary>
        /// Email
        /// </summary>
        [DataMember(Order=6)]
        public string Email
        {
            get
            {
                return this._Email;
            }
            set
            {
                if (this._Email != value)
                {
                    this.OnEmailChanging(value);
                    this.SendPropertyChanging();
                    this._Email = value;
                    this.SendPropertyChanged("Email");
                    this.OnEmailChanged();
                }
            }
        }

    
        /// <summary>
        /// Fax
        /// </summary>
        [DataMember(Order=7)]
        public string Fax
        {
            get
            {
                return this._Fax;
            }
            set
            {
                if (this._Fax != value)
                {
                    this.OnFaxChanging(value);
                    this.SendPropertyChanging();
                    this._Fax = value;
                    this.SendPropertyChanged("Fax");
                    this.OnFaxChanged();
                }
            }
        }

    
        /// <summary>
        /// The user / system that created the record.
        /// </summary>
        /// <LongDescription>
        /// The user / system that created the record.
        /// </LongDescription>
        [DataMember(Order=8)]
        public string Cruser
        {
            get
            {
                return this._Cruser;
            }
            set
            {
                if (this._Cruser != value)
                {
                    this.OnCruserChanging(value);
                    this.SendPropertyChanging();
                    this._Cruser = value;
                    this.SendPropertyChanged("Cruser");
                    this.OnCruserChanged();
                }
            }
        }

    
        /// <summary>
        /// The date when the record has been created.
        /// </summary>
        /// <LongDescription>
        /// The date when the record has been created.
        /// </LongDescription>
        [DataMember(Order=9)]
        public System.DateTime Crd
        {
            get
            {
                return this._Crd;
            }
            set
            {
                if (this._Crd != value)
                {
                    this.OnCrdChanging(value);
                    this.SendPropertyChanging();
                    this._Crd = value;
                    this.SendPropertyChanged("Crd");
                    this.OnCrdChanged();
                }
            }
        }

    
        /// <summary>
        /// The user / system that updated the record.
        /// </summary>
        /// <LongDescription>
        /// The user / system that updated the record.
        /// </LongDescription>
        [DataMember(Order=10)]
        public string Chuser
        {
            get
            {
                return this._Chuser;
            }
            set
            {
                if (this._Chuser != value)
                {
                    this.OnChuserChanging(value);
                    this.SendPropertyChanging();
                    this._Chuser = value;
                    this.SendPropertyChanged("Chuser");
                    this.OnChuserChanged();
                }
            }
        }

    
        /// <summary>
        /// The date when the record has been updated.
        /// </summary>
        /// <LongDescription>
        /// The date when the record has been updated.
        /// </LongDescription>
        [DataMember(Order=11)]
        public System.DateTime Chd
        {
            get
            {
                return this._Chd;
            }
            set
            {
                if (this._Chd != value)
                {
                    this.OnChdChanging(value);
                    this.SendPropertyChanging();
                    this._Chd = value;
                    this.SendPropertyChanged("Chd");
                    this.OnChdChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
