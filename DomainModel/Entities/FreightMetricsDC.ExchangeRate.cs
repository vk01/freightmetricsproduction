//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 25/4/2012 12:45:39 μμ
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace Exis.Domain
{


    /// <summary>
    /// There are no comments for ExchangeRate in the schema.
    /// </summary>
    /// <LongDescription>
    /// Table that holds the exchange rates.
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class ExchangeRate : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private long _SourceCurrencyId;

        private long _TargetCurrencyId;

        private System.DateTime _Date;

        private decimal _Rate;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnSourceCurrencyIdChanging(long value);
        partial void OnSourceCurrencyIdChanged();
        partial void OnTargetCurrencyIdChanging(long value);
        partial void OnTargetCurrencyIdChanged();
        partial void OnDateChanging(System.DateTime value);
        partial void OnDateChanged();
        partial void OnRateChanging(decimal value);
        partial void OnRateChanged();
        #endregion

        public ExchangeRate()
        {
            this.Initialize();
        }

    
    /// <summary>
    /// Internal ID
    /// </summary>
    /// <LongDescription>
    /// Internal ID
    /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
    /// <summary>
    /// Source Currency ID
    /// </summary>
    /// <LongDescription>
    /// Source Currency ID
    /// </LongDescription>
        [DataMember(Order=2)]
        public long SourceCurrencyId
        {
            get
            {
                return this._SourceCurrencyId;
            }
            set
            {
                if (this._SourceCurrencyId != value)
                {
                    this.OnSourceCurrencyIdChanging(value);
                    this.SendPropertyChanging();
                    this._SourceCurrencyId = value;
                    this.SendPropertyChanged("SourceCurrencyId");
                    this.OnSourceCurrencyIdChanged();
                }
            }
        }

    
    /// <summary>
    /// Target Currency ID
    /// </summary>
    /// <LongDescription>
    /// Target Currency ID
    /// </LongDescription>
        [DataMember(Order=3)]
        public long TargetCurrencyId
        {
            get
            {
                return this._TargetCurrencyId;
            }
            set
            {
                if (this._TargetCurrencyId != value)
                {
                    this.OnTargetCurrencyIdChanging(value);
                    this.SendPropertyChanging();
                    this._TargetCurrencyId = value;
                    this.SendPropertyChanged("TargetCurrencyId");
                    this.OnTargetCurrencyIdChanged();
                }
            }
        }

    
    /// <summary>
        /// Date
    /// </summary>
    /// <LongDescription>
    /// Date
    /// </LongDescription>
        [DataMember(Order=4)]
        public System.DateTime Date
        {
            get
            {
                return this._Date;
            }
            set
            {
                if (this._Date != value)
                {
                    this.OnDateChanging(value);
                    this.SendPropertyChanging();
                    this._Date = value;
                    this.SendPropertyChanged("Date");
                    this.OnDateChanged();
                }
            }
        }

    
    /// <summary>
    /// There are no comments for Rate in the schema.
    /// </summary>
    /// <LongDescription>
    /// Rate
    /// </LongDescription>
        [DataMember(Order=5)]
        public decimal Rate
        {
            get
            {
                return this._Rate;
            }
            set
            {
                if (this._Rate != value)
                {
                    this.OnRateChanging(value);
                    this.SendPropertyChanging();
                    this._Rate = value;
                    this.SendPropertyChanged("Rate");
                    this.OnRateChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
