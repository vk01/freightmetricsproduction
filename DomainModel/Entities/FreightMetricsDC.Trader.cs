//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for Trader in the schema.
    /// </summary>
    /// <LongDescription>
    /// Table that holds information about traders.
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class Trader : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private System.Nullable<long> _ProfitCentreId;

        private System.Nullable<long> _DeskId;

        private string _Name;

        private ActivationStatusEnum _Status = (ActivationStatusEnum)Enum.Parse(typeof(ActivationStatusEnum), "0");

        private string _TradeTypes;

        private System.Nullable<int> _VarLimit;

        private System.Nullable<int> _MtmLimit;

        private System.Nullable<int> _CapitalLimit;

        private System.Nullable<int> _NotionalLimit;

        private System.Nullable<int> _DurationLimit;

        private System.Nullable<int> _ExpirationLimit;

        private System.Nullable<int> _ExposureLimit;

        private string _Cruser;

        private System.DateTime _Crd;

        private string _Chuser;

        private System.DateTime _Chd;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnProfitCentreIdChanging(System.Nullable<long> value);
        partial void OnProfitCentreIdChanged();
        partial void OnDeskIdChanging(System.Nullable<long> value);
        partial void OnDeskIdChanged();
        partial void OnNameChanging(string value);
        partial void OnNameChanged();
        partial void OnStatusChanging(ActivationStatusEnum value);
        partial void OnStatusChanged();
        partial void OnTradeTypesChanging(string value);
        partial void OnTradeTypesChanged();
        partial void OnVarLimitChanging(System.Nullable<int> value);
        partial void OnVarLimitChanged();
        partial void OnMtmLimitChanging(System.Nullable<int> value);
        partial void OnMtmLimitChanged();
        partial void OnCapitalLimitChanging(System.Nullable<int> value);
        partial void OnCapitalLimitChanged();
        partial void OnNotionalLimitChanging(System.Nullable<int> value);
        partial void OnNotionalLimitChanged();
        partial void OnDurationLimitChanging(System.Nullable<int> value);
        partial void OnDurationLimitChanged();
        partial void OnExpirationLimitChanging(System.Nullable<int> value);
        partial void OnExpirationLimitChanged();
        partial void OnExposureLimitChanging(System.Nullable<int> value);
        partial void OnExposureLimitChanged();
        partial void OnCruserChanging(string value);
        partial void OnCruserChanged();
        partial void OnCrdChanging(System.DateTime value);
        partial void OnCrdChanged();
        partial void OnChuserChanging(string value);
        partial void OnChuserChanged();
        partial void OnChdChanging(System.DateTime value);
        partial void OnChdChanged();
        #endregion

        public Trader()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// The internal ID.
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for ProfitCentreId in the schema.
        /// </summary>
        /// <LongDescription>
        /// The description.
        /// </LongDescription>
        [DataMember(Order=2)]
        public System.Nullable<long> ProfitCentreId
        {
            get
            {
                return this._ProfitCentreId;
            }
            set
            {
                if (this._ProfitCentreId != value)
                {
                    this.OnProfitCentreIdChanging(value);
                    this.SendPropertyChanging();
                    this._ProfitCentreId = value;
                    this.SendPropertyChanged("ProfitCentreId");
                    this.OnProfitCentreIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DeskId in the schema.
        /// </summary>
        /// <LongDescription>
        /// The description.
        /// </LongDescription>
        [DataMember(Order=3)]
        public System.Nullable<long> DeskId
        {
            get
            {
                return this._DeskId;
            }
            set
            {
                if (this._DeskId != value)
                {
                    this.OnDeskIdChanging(value);
                    this.SendPropertyChanging();
                    this._DeskId = value;
                    this.SendPropertyChanged("DeskId");
                    this.OnDeskIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Name in the schema.
        /// </summary>
        /// <LongDescription>
        /// Name
        /// </LongDescription>
        [DataMember(Order=4)]
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if (this._Name != value)
                {
                    this.OnNameChanging(value);
                    this.SendPropertyChanging();
                    this._Name = value;
                    this.SendPropertyChanged("Name");
                    this.OnNameChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Status in the schema.
        /// </summary>
        /// <LongDescription>
        /// The status. Possible values are: 0 = Inactive, 1 = Active.
        /// </LongDescription>
        [DataMember(Order=5)]
        public ActivationStatusEnum Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                if (this._Status != value)
                {
                    this.OnStatusChanging(value);
                    this.SendPropertyChanging();
                    this._Status = value;
                    this.SendPropertyChanged("Status");
                    this.OnStatusChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for TradeTypes in the schema.
        /// </summary>
        /// <LongDescription>
        /// A comma separated string of Trade Type values for which the Trader can enter Trades
        /// </LongDescription>
        [DataMember(Order=6)]
        public string TradeTypes
        {
            get
            {
                return this._TradeTypes;
            }
            set
            {
                if (this._TradeTypes != value)
                {
                    this.OnTradeTypesChanging(value);
                    this.SendPropertyChanging();
                    this._TradeTypes = value;
                    this.SendPropertyChanged("TradeTypes");
                    this.OnTradeTypesChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for VarLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// Value at Risk Limit
        /// </LongDescription>
        [DataMember(Order=7)]
        public System.Nullable<int> VarLimit
        {
            get
            {
                return this._VarLimit;
            }
            set
            {
                if (this._VarLimit != value)
                {
                    this.OnVarLimitChanging(value);
                    this.SendPropertyChanging();
                    this._VarLimit = value;
                    this.SendPropertyChanged("VarLimit");
                    this.OnVarLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for MtmLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// MTM Limit
        /// </LongDescription>
        [DataMember(Order=8)]
        public System.Nullable<int> MtmLimit
        {
            get
            {
                return this._MtmLimit;
            }
            set
            {
                if (this._MtmLimit != value)
                {
                    this.OnMtmLimitChanging(value);
                    this.SendPropertyChanging();
                    this._MtmLimit = value;
                    this.SendPropertyChanged("MtmLimit");
                    this.OnMtmLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for CapitalLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// Capital Limit
        /// </LongDescription>
        [DataMember(Order=9)]
        public System.Nullable<int> CapitalLimit
        {
            get
            {
                return this._CapitalLimit;
            }
            set
            {
                if (this._CapitalLimit != value)
                {
                    this.OnCapitalLimitChanging(value);
                    this.SendPropertyChanging();
                    this._CapitalLimit = value;
                    this.SendPropertyChanged("CapitalLimit");
                    this.OnCapitalLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for NotionalLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// Notional Limit
        /// </LongDescription>
        [DataMember(Order=10)]
        public System.Nullable<int> NotionalLimit
        {
            get
            {
                return this._NotionalLimit;
            }
            set
            {
                if (this._NotionalLimit != value)
                {
                    this.OnNotionalLimitChanging(value);
                    this.SendPropertyChanging();
                    this._NotionalLimit = value;
                    this.SendPropertyChanged("NotionalLimit");
                    this.OnNotionalLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DurationLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// Duration Limit
        /// </LongDescription>
        [DataMember(Order=11)]
        public System.Nullable<int> DurationLimit
        {
            get
            {
                return this._DurationLimit;
            }
            set
            {
                if (this._DurationLimit != value)
                {
                    this.OnDurationLimitChanging(value);
                    this.SendPropertyChanging();
                    this._DurationLimit = value;
                    this.SendPropertyChanged("DurationLimit");
                    this.OnDurationLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for ExpirationLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// Expiration Limit
        /// </LongDescription>
        [DataMember(Order=12)]
        public System.Nullable<int> ExpirationLimit
        {
            get
            {
                return this._ExpirationLimit;
            }
            set
            {
                if (this._ExpirationLimit != value)
                {
                    this.OnExpirationLimitChanging(value);
                    this.SendPropertyChanging();
                    this._ExpirationLimit = value;
                    this.SendPropertyChanged("ExpirationLimit");
                    this.OnExpirationLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for ExposureLimit in the schema.
        /// </summary>
        /// <LongDescription>
        /// Exposure Limit
        /// </LongDescription>
        [DataMember(Order=13)]
        public System.Nullable<int> ExposureLimit
        {
            get
            {
                return this._ExposureLimit;
            }
            set
            {
                if (this._ExposureLimit != value)
                {
                    this.OnExposureLimitChanging(value);
                    this.SendPropertyChanging();
                    this._ExposureLimit = value;
                    this.SendPropertyChanged("ExposureLimit");
                    this.OnExposureLimitChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Cruser in the schema.
        /// </summary>
        /// <LongDescription>
        /// The user / system that created the record.
        /// </LongDescription>
        [DataMember(Order=14)]
        public string Cruser
        {
            get
            {
                return this._Cruser;
            }
            set
            {
                if (this._Cruser != value)
                {
                    this.OnCruserChanging(value);
                    this.SendPropertyChanging();
                    this._Cruser = value;
                    this.SendPropertyChanged("Cruser");
                    this.OnCruserChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Crd in the schema.
        /// </summary>
        /// <LongDescription>
        /// The date when the record has been created.
        /// </LongDescription>
        [DataMember(Order=15)]
        public System.DateTime Crd
        {
            get
            {
                return this._Crd;
            }
            set
            {
                if (this._Crd != value)
                {
                    this.OnCrdChanging(value);
                    this.SendPropertyChanging();
                    this._Crd = value;
                    this.SendPropertyChanged("Crd");
                    this.OnCrdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Chuser in the schema.
        /// </summary>
        /// <LongDescription>
        /// The user / system that updated the record.
        /// </LongDescription>
        [DataMember(Order=16)]
        public string Chuser
        {
            get
            {
                return this._Chuser;
            }
            set
            {
                if (this._Chuser != value)
                {
                    this.OnChuserChanging(value);
                    this.SendPropertyChanging();
                    this._Chuser = value;
                    this.SendPropertyChanged("Chuser");
                    this.OnChuserChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Chd in the schema.
        /// </summary>
        /// <LongDescription>
        /// The date when the record has been updated.
        /// </LongDescription>
        [DataMember(Order=17)]
        public System.DateTime Chd
        {
            get
            {
                return this._Chd;
            }
            set
            {
                if (this._Chd != value)
                {
                    this.OnChdChanging(value);
                    this.SendPropertyChanging();
                    this._Chd = value;
                    this.SendPropertyChanged("Chd");
                    this.OnChdChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
