//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for MtmValue in the schema.
    /// </summary>
    /// <LongDescription>
    /// Table that holds information about MTM values.
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class MtmValue : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private long _IndexId;

        private System.DateTime _BFADate;

        private System.DateTime _MTMDate;

        private int _MTMRate;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnIndexIdChanging(long value);
        partial void OnIndexIdChanged();
        partial void OnBFADateChanging(System.DateTime value);
        partial void OnBFADateChanged();
        partial void OnMTMDateChanging(System.DateTime value);
        partial void OnMTMDateChanged();
        partial void OnMTMRateChanging(int value);
        partial void OnMTMRateChanged();
        #endregion

        public MtmValue()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// The internal ID.
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for IndexId in the schema.
        /// </summary>
        /// <LongDescription>
        /// Index ID
        /// </LongDescription>
        [DataMember(Order=2)]
        public long IndexId
        {
            get
            {
                return this._IndexId;
            }
            set
            {
                if (this._IndexId != value)
                {
                    this.OnIndexIdChanging(value);
                    this.SendPropertyChanging();
                    this._IndexId = value;
                    this.SendPropertyChanged("IndexId");
                    this.OnIndexIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for BFADate in the schema.
        /// </summary>
        /// <LongDescription>
        /// BFA Date at which the MTM for this Index is calculated.
        /// </LongDescription>
        [DataMember(Order=3)]
        public System.DateTime BFADate
        {
            get
            {
                return this._BFADate;
            }
            set
            {
                if (this._BFADate != value)
                {
                    this.OnBFADateChanging(value);
                    this.SendPropertyChanging();
                    this._BFADate = value;
                    this.SendPropertyChanged("BFADate");
                    this.OnBFADateChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for MTMDate in the schema.
        /// </summary>
        /// <LongDescription>
        /// Calculated MTM Date. Should be one value for each day of the calculation period.
        /// </LongDescription>
        [DataMember(Order=4)]
        public System.DateTime MTMDate
        {
            get
            {
                return this._MTMDate;
            }
            set
            {
                if (this._MTMDate != value)
                {
                    this.OnMTMDateChanging(value);
                    this.SendPropertyChanging();
                    this._MTMDate = value;
                    this.SendPropertyChanged("MTMDate");
                    this.OnMTMDateChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for MTMRate in the schema.
        /// </summary>
        /// <LongDescription>
        /// Calculated Rate.
        /// </LongDescription>
        [DataMember(Order=5)]
        public int MTMRate
        {
            get
            {
                return this._MTMRate;
            }
            set
            {
                if (this._MTMRate != value)
                {
                    this.OnMTMRateChanging(value);
                    this.SendPropertyChanging();
                    this._MTMRate = value;
                    this.SendPropertyChanged("MTMRate");
                    this.OnMTMRateChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
