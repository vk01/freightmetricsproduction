//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for Udf in the schema.
    /// </summary>
    /// <LongDescription>
    /// UDFS
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class Udf : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private string _Code;

        private string _Name;

        private string _Status_Str;

        private string _RefType_Str;

        private System.Nullable<long> _RefTypeId;

        private string _Type_Str;

        private string _SubType_Str;

        private string _AccessType_Str;

        private string _IsMultiple_Str;

        private string _MultipleType_Str;

        private string _IsList_Str;

        private string _ListType_Str;

        private string _ListAppField;

        private string _IsDefault_Str;

        private string _DefaultType_Str;

        private string _DefaultAppField;

        private string _Cruser;

        private System.DateTime _Crd;

        private string _Chuser;

        private System.Nullable<System.DateTime> _Chd;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnCodeChanging(string value);
        partial void OnCodeChanged();
        partial void OnNameChanging(string value);
        partial void OnNameChanged();
        partial void OnStatus_StrChanging(string value);
        partial void OnStatus_StrChanged();
        partial void OnRefType_StrChanging(string value);
        partial void OnRefType_StrChanged();
        partial void OnRefTypeIdChanging(System.Nullable<long> value);
        partial void OnRefTypeIdChanged();
        partial void OnType_StrChanging(string value);
        partial void OnType_StrChanged();
        partial void OnSubType_StrChanging(string value);
        partial void OnSubType_StrChanged();
        partial void OnAccessType_StrChanging(string value);
        partial void OnAccessType_StrChanged();
        partial void OnIsMultiple_StrChanging(string value);
        partial void OnIsMultiple_StrChanged();
        partial void OnMultipleType_StrChanging(string value);
        partial void OnMultipleType_StrChanged();
        partial void OnIsList_StrChanging(string value);
        partial void OnIsList_StrChanged();
        partial void OnListType_StrChanging(string value);
        partial void OnListType_StrChanged();
        partial void OnListAppFieldChanging(string value);
        partial void OnListAppFieldChanged();
        partial void OnIsDefault_StrChanging(string value);
        partial void OnIsDefault_StrChanged();
        partial void OnDefaultType_StrChanging(string value);
        partial void OnDefaultType_StrChanged();
        partial void OnDefaultAppFieldChanging(string value);
        partial void OnDefaultAppFieldChanged();
        partial void OnCruserChanging(string value);
        partial void OnCruserChanged();
        partial void OnCrdChanging(System.DateTime value);
        partial void OnCrdChanged();
        partial void OnChuserChanging(string value);
        partial void OnChuserChanged();
        partial void OnChdChanging(System.Nullable<System.DateTime> value);
        partial void OnChdChanged();
        #endregion

        public Udf()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// UDF_ID
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Code in the schema.
        /// </summary>
        /// <LongDescription>
        /// Code
        /// </LongDescription>
        [DataMember(Order=2)]
        public string Code
        {
            get
            {
                return this._Code;
            }
            set
            {
                if (this._Code != value)
                {
                    this.OnCodeChanging(value);
                    this.SendPropertyChanging();
                    this._Code = value;
                    this.SendPropertyChanged("Code");
                    this.OnCodeChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Name in the schema.
        /// </summary>
        /// <LongDescription>
        /// Name
        /// </LongDescription>
        [DataMember(Order=3)]
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if (this._Name != value)
                {
                    this.OnNameChanging(value);
                    this.SendPropertyChanging();
                    this._Name = value;
                    this.SendPropertyChanged("Name");
                    this.OnNameChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Status_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Activation Status. 0 = Inactive, 1 = Active
        /// </LongDescription>
        [DataMember(Order=4)]
        public string Status_Str
        {
            get
            {
                return this._Status_Str;
            }
            set
            {
                if (this._Status_Str != value)
                {
                    this.OnStatus_StrChanging(value);
                    this.SendPropertyChanging();
                    this._Status_Str = value;
                    this.SendPropertyChanged("Status_Str");
                    this.OnStatus_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for RefType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Reference Type. 1 = CustomerType, 2 = CaseType, 3 = EventType, 4 = PaymentMethod, 5 = AddressType, 6 = ContactType, 7 = DocumentType, 8 = UserType
        /// </LongDescription>
        [DataMember(Order=5)]
        public string RefType_Str
        {
            get
            {
                return this._RefType_Str;
            }
            set
            {
                if (this._RefType_Str != value)
                {
                    this.OnRefType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._RefType_Str = value;
                    this.SendPropertyChanged("RefType_Str");
                    this.OnRefType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for RefTypeId in the schema.
        /// </summary>
        /// <LongDescription>
        /// Reference Type internal Id
        /// </LongDescription>
        [DataMember(Order=6)]
        public System.Nullable<long> RefTypeId
        {
            get
            {
                return this._RefTypeId;
            }
            set
            {
                if (this._RefTypeId != value)
                {
                    this.OnRefTypeIdChanging(value);
                    this.SendPropertyChanging();
                    this._RefTypeId = value;
                    this.SendPropertyChanged("RefTypeId");
                    this.OnRefTypeIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Type_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// 1 = Date, 2 = Numeric, 3 = ShortText, 4 = LongText, 5 = Status, 7 = ExternalLink, 8 = InternalLink, 9 = Image, 10 = File
        /// </LongDescription>
        [DataMember(Order=7)]
        public string Type_Str
        {
            get
            {
                return this._Type_Str;
            }
            set
            {
                if (this._Type_Str != value)
                {
                    this.OnType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._Type_Str = value;
                    this.SendPropertyChanged("Type_Str");
                    this.OnType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for SubType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// For Type = 2 = Short Text:
        /// PhoneNumberLandline = 1,
        /// PhoneNumberMobile = 2,
        /// Email = 3,
        /// GreekName = 4,
        /// MixedName = 5,
        /// Code = 6,
        /// CreditCard = 7,
        /// MixedNameForSearch = 8,
        /// PostalCode = 9,
        /// ContactPhoneForSearch = 10,
        /// ContactNameForSearch = 11
        /// 
        /// For Type = 3 = Long Text:
        /// TXT = 1,
        /// RTF = 2,
        /// DOCX = 3,
        /// HTML = 4
        /// 
        /// For Type = 7 = External Link:
        /// URL = 1,
        /// Image = 2,
        /// File = 3,
        /// Map = 4
        /// 
        /// For Type = 8 = Internal Link:
        /// Customer = 1,
        /// Case = 2,
        /// Event = 3,
        /// Contract = 4,
        /// User = 5,
        /// Contact = 6,
        /// ContactPhone = 7
        /// 
        /// For Type = 9 = Image:
        /// JPG = 1,
        /// PNG = 2,
        /// BMP = 3,
        /// GIF = 4,
        /// TIFF = 5
        /// 
        /// For Type = 10 = File:
        /// ZIP = 1,
        /// DOC = 2,
        /// XLS = 3,
        /// RAR = 4,
        /// PDF = 5
        /// </LongDescription>
        [DataMember(Order=8)]
        public string SubType_Str
        {
            get
            {
                return this._SubType_Str;
            }
            set
            {
                if (this._SubType_Str != value)
                {
                    this.OnSubType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._SubType_Str = value;
                    this.SendPropertyChanged("SubType_Str");
                    this.OnSubType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for AccessType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Editable = 1,
        /// ReadOnly = 2,
        /// Internal = 3
        /// </LongDescription>
        [DataMember(Order=9)]
        public string AccessType_Str
        {
            get
            {
                return this._AccessType_Str;
            }
            set
            {
                if (this._AccessType_Str != value)
                {
                    this.OnAccessType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._AccessType_Str = value;
                    this.SendPropertyChanged("AccessType_Str");
                    this.OnAccessType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for IsMultiple_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// UDF_IS_MULTIPLE
        /// </LongDescription>
        [DataMember(Order=10)]
        public string IsMultiple_Str
        {
            get
            {
                return this._IsMultiple_Str;
            }
            set
            {
                if (this._IsMultiple_Str != value)
                {
                    this.OnIsMultiple_StrChanging(value);
                    this.SendPropertyChanging();
                    this._IsMultiple_Str = value;
                    this.SendPropertyChanged("IsMultiple_Str");
                    this.OnIsMultiple_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for MultipleType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Sequential = 1,
        /// Flat = 2
        /// </LongDescription>
        [DataMember(Order=11)]
        public string MultipleType_Str
        {
            get
            {
                return this._MultipleType_Str;
            }
            set
            {
                if (this._MultipleType_Str != value)
                {
                    this.OnMultipleType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._MultipleType_Str = value;
                    this.SendPropertyChanged("MultipleType_Str");
                    this.OnMultipleType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for IsList_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// UDF_IS_LIST
        /// </LongDescription>
        [DataMember(Order=12)]
        public string IsList_Str
        {
            get
            {
                return this._IsList_Str;
            }
            set
            {
                if (this._IsList_Str != value)
                {
                    this.OnIsList_StrChanging(value);
                    this.SendPropertyChanging();
                    this._IsList_Str = value;
                    this.SendPropertyChanged("IsList_Str");
                    this.OnIsList_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for ListType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Predefined = 1,
        /// Runtime = 2
        /// </LongDescription>
        [DataMember(Order=13)]
        public string ListType_Str
        {
            get
            {
                return this._ListType_Str;
            }
            set
            {
                if (this._ListType_Str != value)
                {
                    this.OnListType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._ListType_Str = value;
                    this.SendPropertyChanged("ListType_Str");
                    this.OnListType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for ListAppField in the schema.
        /// </summary>
        /// <LongDescription>
        /// UDF_LIST_APP_FIELD
        /// </LongDescription>
        [DataMember(Order=14)]
        public string ListAppField
        {
            get
            {
                return this._ListAppField;
            }
            set
            {
                if (this._ListAppField != value)
                {
                    this.OnListAppFieldChanging(value);
                    this.SendPropertyChanging();
                    this._ListAppField = value;
                    this.SendPropertyChanged("ListAppField");
                    this.OnListAppFieldChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for IsDefault_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// UDF_IS_DEFAULT
        /// </LongDescription>
        [DataMember(Order=15)]
        public string IsDefault_Str
        {
            get
            {
                return this._IsDefault_Str;
            }
            set
            {
                if (this._IsDefault_Str != value)
                {
                    this.OnIsDefault_StrChanging(value);
                    this.SendPropertyChanging();
                    this._IsDefault_Str = value;
                    this.SendPropertyChanged("IsDefault_Str");
                    this.OnIsDefault_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DefaultType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Predefined = 1,
        /// Runtime = 2,
        /// EvaluatedText = 3
        /// </LongDescription>
        [DataMember(Order=16)]
        public string DefaultType_Str
        {
            get
            {
                return this._DefaultType_Str;
            }
            set
            {
                if (this._DefaultType_Str != value)
                {
                    this.OnDefaultType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._DefaultType_Str = value;
                    this.SendPropertyChanged("DefaultType_Str");
                    this.OnDefaultType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DefaultAppField in the schema.
        /// </summary>
        /// <LongDescription>
        /// UDF_DEFAULT_APP_FIELD
        /// </LongDescription>
        [DataMember(Order=17)]
        public string DefaultAppField
        {
            get
            {
                return this._DefaultAppField;
            }
            set
            {
                if (this._DefaultAppField != value)
                {
                    this.OnDefaultAppFieldChanging(value);
                    this.SendPropertyChanging();
                    this._DefaultAppField = value;
                    this.SendPropertyChanged("DefaultAppField");
                    this.OnDefaultAppFieldChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Cruser in the schema.
        /// </summary>
        /// <LongDescription>
        /// CRUSER
        /// </LongDescription>
        [DataMember(Order=18)]
        public string Cruser
        {
            get
            {
                return this._Cruser;
            }
            set
            {
                if (this._Cruser != value)
                {
                    this.OnCruserChanging(value);
                    this.SendPropertyChanging();
                    this._Cruser = value;
                    this.SendPropertyChanged("Cruser");
                    this.OnCruserChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Crd in the schema.
        /// </summary>
        /// <LongDescription>
        /// CRD
        /// </LongDescription>
        [DataMember(Order=19)]
        public System.DateTime Crd
        {
            get
            {
                return this._Crd;
            }
            set
            {
                if (this._Crd != value)
                {
                    this.OnCrdChanging(value);
                    this.SendPropertyChanging();
                    this._Crd = value;
                    this.SendPropertyChanged("Crd");
                    this.OnCrdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Chuser in the schema.
        /// </summary>
        /// <LongDescription>
        /// CHUSER
        /// </LongDescription>
        [DataMember(Order=20)]
        public string Chuser
        {
            get
            {
                return this._Chuser;
            }
            set
            {
                if (this._Chuser != value)
                {
                    this.OnChuserChanging(value);
                    this.SendPropertyChanging();
                    this._Chuser = value;
                    this.SendPropertyChanged("Chuser");
                    this.OnChuserChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Chd in the schema.
        /// </summary>
        /// <LongDescription>
        /// CHD
        /// </LongDescription>
        [DataMember(Order=21)]
        public System.Nullable<System.DateTime> Chd
        {
            get
            {
                return this._Chd;
            }
            set
            {
                if (this._Chd != value)
                {
                    this.OnChdChanging(value);
                    this.SendPropertyChanging();
                    this._Chd = value;
                    this.SendPropertyChanged("Chd");
                    this.OnChdChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
