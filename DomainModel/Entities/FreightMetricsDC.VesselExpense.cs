//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for VesselExpense in the schema.
    /// </summary>
    /// <LongDescription>
    /// Table that holds information about vessel expenses.
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class VesselExpense : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private long _VesselId;

        private int _Age;

        private decimal _OperationalExpenses;

        private decimal _DryDockExpenses;

        private int _DryDockOffHire;

        private int _DepreciationProfile;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnVesselIdChanging(long value);
        partial void OnVesselIdChanged();
        partial void OnAgeChanging(int value);
        partial void OnAgeChanged();
        partial void OnOperationalExpensesChanging(decimal value);
        partial void OnOperationalExpensesChanged();
        partial void OnDryDockExpensesChanging(decimal value);
        partial void OnDryDockExpensesChanged();
        partial void OnDryDockOffHireChanging(int value);
        partial void OnDryDockOffHireChanged();
        partial void OnDepreciationProfileChanging(int value);
        partial void OnDepreciationProfileChanged();
        #endregion

        public VesselExpense()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// The internal ID.
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for VesselId in the schema.
        /// </summary>
        /// <LongDescription>
        /// Vessel ID
        /// </LongDescription>
        [DataMember(Order=2)]
        public long VesselId
        {
            get
            {
                return this._VesselId;
            }
            set
            {
                if (this._VesselId != value)
                {
                    this.OnVesselIdChanging(value);
                    this.SendPropertyChanging();
                    this._VesselId = value;
                    this.SendPropertyChanged("VesselId");
                    this.OnVesselIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Age in the schema.
        /// </summary>
        /// <LongDescription>
        /// Age
        /// </LongDescription>
        [DataMember(Order=3)]
        public int Age
        {
            get
            {
                return this._Age;
            }
            set
            {
                if (this._Age != value)
                {
                    this.OnAgeChanging(value);
                    this.SendPropertyChanging();
                    this._Age = value;
                    this.SendPropertyChanged("Age");
                    this.OnAgeChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for OperationalExpenses in the schema.
        /// </summary>
        /// <LongDescription>
        /// Operational Expenses
        /// </LongDescription>
        [DataMember(Order=4)]
        public decimal OperationalExpenses
        {
            get
            {
                return this._OperationalExpenses;
            }
            set
            {
                if (this._OperationalExpenses != value)
                {
                    this.OnOperationalExpensesChanging(value);
                    this.SendPropertyChanging();
                    this._OperationalExpenses = value;
                    this.SendPropertyChanged("OperationalExpenses");
                    this.OnOperationalExpensesChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DryDockExpenses in the schema.
        /// </summary>
        /// <LongDescription>
        /// Dry Dock Expenses
        /// </LongDescription>
        [DataMember(Order=5)]
        public decimal DryDockExpenses
        {
            get
            {
                return this._DryDockExpenses;
            }
            set
            {
                if (this._DryDockExpenses != value)
                {
                    this.OnDryDockExpensesChanging(value);
                    this.SendPropertyChanging();
                    this._DryDockExpenses = value;
                    this.SendPropertyChanged("DryDockExpenses");
                    this.OnDryDockExpensesChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DryDockOffHire in the schema.
        /// </summary>
        /// <LongDescription>
        /// Dry Dock Off-Hire
        /// </LongDescription>
        [DataMember(Order=6)]
        public int DryDockOffHire
        {
            get
            {
                return this._DryDockOffHire;
            }
            set
            {
                if (this._DryDockOffHire != value)
                {
                    this.OnDryDockOffHireChanging(value);
                    this.SendPropertyChanging();
                    this._DryDockOffHire = value;
                    this.SendPropertyChanged("DryDockOffHire");
                    this.OnDryDockOffHireChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for DepreciationProfile in the schema.
        /// </summary>
        /// <LongDescription>
        /// Depreciation Profile
        /// </LongDescription>
        [DataMember(Order=7)]
        public int DepreciationProfile
        {
            get
            {
                return this._DepreciationProfile;
            }
            set
            {
                if (this._DepreciationProfile != value)
                {
                    this.OnDepreciationProfileChanging(value);
                    this.SendPropertyChanging();
                    this._DepreciationProfile = value;
                    this.SendPropertyChanged("DepreciationProfile");
                    this.OnDepreciationProfileChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
