//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// Table that holds information about loan collateral assets.
    /// </summary>
    [DataContract(IsReference = true)]
    public partial class LoanCollateralAsset : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private long _LoanId;

        private LoanCollateralTypeEnum _Type = (LoanCollateralTypeEnum)Enum.Parse(typeof(LoanCollateralTypeEnum), "1");

        private System.Nullable<long> _VesselId;

        private string _Asset;

        private System.Nullable<int> _Amount;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnLoanIdChanging(long value);
        partial void OnLoanIdChanged();
        partial void OnTypeChanging(LoanCollateralTypeEnum value);
        partial void OnTypeChanged();
        partial void OnVesselIdChanging(System.Nullable<long> value);
        partial void OnVesselIdChanged();
        partial void OnAssetChanging(string value);
        partial void OnAssetChanged();
        partial void OnAmountChanging(System.Nullable<int> value);
        partial void OnAmountChanged();
        #endregion

        public LoanCollateralAsset()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// The internal ID.
        /// </summary>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// Loan ID
        /// </summary>
        [DataMember(Order=2)]
        public long LoanId
        {
            get
            {
                return this._LoanId;
            }
            set
            {
                if (this._LoanId != value)
                {
                    this.OnLoanIdChanging(value);
                    this.SendPropertyChanging();
                    this._LoanId = value;
                    this.SendPropertyChanged("LoanId");
                    this.OnLoanIdChanged();
                }
            }
        }

    
        /// <summary>
        /// Type
        /// </summary>
        [DataMember(Order=3)]
        public LoanCollateralTypeEnum Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if (this._Type != value)
                {
                    this.OnTypeChanging(value);
                    this.SendPropertyChanging();
                    this._Type = value;
                    this.SendPropertyChanged("Type");
                    this.OnTypeChanged();
                }
            }
        }

    
        /// <summary>
        /// Vessel ID
        /// </summary>
        [DataMember(Order=4)]
        public System.Nullable<long> VesselId
        {
            get
            {
                return this._VesselId;
            }
            set
            {
                if (this._VesselId != value)
                {
                    this.OnVesselIdChanging(value);
                    this.SendPropertyChanging();
                    this._VesselId = value;
                    this.SendPropertyChanged("VesselId");
                    this.OnVesselIdChanged();
                }
            }
        }

    
        /// <summary>
        /// The collateral asset when no vessels are selected as collateral
        /// </summary>
        [DataMember(Order=5)]
        public string Asset
        {
            get
            {
                return this._Asset;
            }
            set
            {
                if (this._Asset != value)
                {
                    this.OnAssetChanging(value);
                    this.SendPropertyChanging();
                    this._Asset = value;
                    this.SendPropertyChanged("Asset");
                    this.OnAssetChanged();
                }
            }
        }

    
        /// <summary>
        /// Amount for Cash and Other collateral types.
        /// </summary>
        [DataMember(Order=6)]
        public System.Nullable<int> Amount
        {
            get
            {
                return this._Amount;
            }
            set
            {
                if (this._Amount != value)
                {
                    this.OnAmountChanging(value);
                    this.SendPropertyChanging();
                    this._Amount = value;
                    this.SendPropertyChanged("Amount");
                    this.OnAmountChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
