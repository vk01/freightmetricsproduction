//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for Appointment in the schema.
    /// </summary>
    /// <LongDescription>
    /// Holds appointment information for Sales Person - User
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class Appointment : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private System.Nullable<long> _UserId;

        private int _Type;

        private System.Nullable<System.DateTime> _StartDate;

        private System.Nullable<System.DateTime> _EndDate;

        private string _IsAllDay_Str;

        private string _Subject;

        private string _Location;

        private string _Description;

        private System.Nullable<int> _Status;

        private System.Nullable<long> _Label;

        private string _Recurrence;

        private string _Reminder;

        private System.Nullable<System.DateTime> _Datf;

        private System.Nullable<System.DateTime> _Datt;

        private string _RefType_Str;

        private long _RefId;

        private string _OutlookEntryId;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnUserIdChanging(System.Nullable<long> value);
        partial void OnUserIdChanged();
        partial void OnTypeChanging(int value);
        partial void OnTypeChanged();
        partial void OnStartDateChanging(System.Nullable<System.DateTime> value);
        partial void OnStartDateChanged();
        partial void OnEndDateChanging(System.Nullable<System.DateTime> value);
        partial void OnEndDateChanged();
        partial void OnIsAllDay_StrChanging(string value);
        partial void OnIsAllDay_StrChanged();
        partial void OnSubjectChanging(string value);
        partial void OnSubjectChanged();
        partial void OnLocationChanging(string value);
        partial void OnLocationChanged();
        partial void OnDescriptionChanging(string value);
        partial void OnDescriptionChanged();
        partial void OnStatusChanging(System.Nullable<int> value);
        partial void OnStatusChanged();
        partial void OnLabelChanging(System.Nullable<long> value);
        partial void OnLabelChanged();
        partial void OnRecurrenceChanging(string value);
        partial void OnRecurrenceChanged();
        partial void OnReminderChanging(string value);
        partial void OnReminderChanged();
        partial void OnDatfChanging(System.Nullable<System.DateTime> value);
        partial void OnDatfChanged();
        partial void OnDattChanging(System.Nullable<System.DateTime> value);
        partial void OnDattChanged();
        partial void OnRefType_StrChanging(string value);
        partial void OnRefType_StrChanged();
        partial void OnRefIdChanging(long value);
        partial void OnRefIdChanged();
        partial void OnOutlookEntryIdChanging(string value);
        partial void OnOutlookEntryIdChanged();
        #endregion

        public Appointment()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// Internal ID
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for UserId in the schema.
        /// </summary>
        /// <LongDescription>
        /// User - Sales Person ID
        /// </LongDescription>
        [DataMember(Order=2)]
        public System.Nullable<long> UserId
        {
            get
            {
                return this._UserId;
            }
            set
            {
                if (this._UserId != value)
                {
                    this.OnUserIdChanging(value);
                    this.SendPropertyChanging();
                    this._UserId = value;
                    this.SendPropertyChanged("UserId");
                    this.OnUserIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Type in the schema.
        /// </summary>
        /// <LongDescription>
        /// (Control specific) = Type of the Appointment
        /// </LongDescription>
        [DataMember(Order=3)]
        public int Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if (this._Type != value)
                {
                    this.OnTypeChanging(value);
                    this.SendPropertyChanging();
                    this._Type = value;
                    this.SendPropertyChanged("Type");
                    this.OnTypeChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for StartDate in the schema.
        /// </summary>
        /// <LongDescription>
        /// Start Date and Time for the Appointment
        /// </LongDescription>
        [DataMember(Order=4)]
        public System.Nullable<System.DateTime> StartDate
        {
            get
            {
                return this._StartDate;
            }
            set
            {
                if (this._StartDate != value)
                {
                    this.OnStartDateChanging(value);
                    this.SendPropertyChanging();
                    this._StartDate = value;
                    this.SendPropertyChanged("StartDate");
                    this.OnStartDateChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for EndDate in the schema.
        /// </summary>
        /// <LongDescription>
        /// End Date and Time for the Appointment
        /// </LongDescription>
        [DataMember(Order=5)]
        public System.Nullable<System.DateTime> EndDate
        {
            get
            {
                return this._EndDate;
            }
            set
            {
                if (this._EndDate != value)
                {
                    this.OnEndDateChanging(value);
                    this.SendPropertyChanging();
                    this._EndDate = value;
                    this.SendPropertyChanged("EndDate");
                    this.OnEndDateChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for IsAllDay_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// (Control specific) - If the Appointment spans throgh a whole or more days.
        /// Values: 0 = False, 1 = True.
        /// </LongDescription>
        [DataMember(Order=6)]
        public string IsAllDay_Str
        {
            get
            {
                return this._IsAllDay_Str;
            }
            set
            {
                if (this._IsAllDay_Str != value)
                {
                    this.OnIsAllDay_StrChanging(value);
                    this.SendPropertyChanging();
                    this._IsAllDay_Str = value;
                    this.SendPropertyChanged("IsAllDay_Str");
                    this.OnIsAllDay_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Subject in the schema.
        /// </summary>
        /// <LongDescription>
        /// The Subject of the Appointment
        /// </LongDescription>
        [DataMember(Order=7)]
        public string Subject
        {
            get
            {
                return this._Subject;
            }
            set
            {
                if (this._Subject != value)
                {
                    this.OnSubjectChanging(value);
                    this.SendPropertyChanging();
                    this._Subject = value;
                    this.SendPropertyChanged("Subject");
                    this.OnSubjectChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Location in the schema.
        /// </summary>
        /// <LongDescription>
        /// The Location of the Appointment
        /// </LongDescription>
        [DataMember(Order=8)]
        public string Location
        {
            get
            {
                return this._Location;
            }
            set
            {
                if (this._Location != value)
                {
                    this.OnLocationChanging(value);
                    this.SendPropertyChanging();
                    this._Location = value;
                    this.SendPropertyChanged("Location");
                    this.OnLocationChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Description in the schema.
        /// </summary>
        /// <LongDescription>
        /// A detailed Description of the Appointment
        /// </LongDescription>
        [DataMember(Order=9)]
        public string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                if (this._Description != value)
                {
                    this.OnDescriptionChanging(value);
                    this.SendPropertyChanging();
                    this._Description = value;
                    this.SendPropertyChanged("Description");
                    this.OnDescriptionChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Status in the schema.
        /// </summary>
        /// <LongDescription>
        /// (Control specific) - Appointment Status
        /// </LongDescription>
        [DataMember(Order=10)]
        public System.Nullable<int> Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                if (this._Status != value)
                {
                    this.OnStatusChanging(value);
                    this.SendPropertyChanging();
                    this._Status = value;
                    this.SendPropertyChanged("Status");
                    this.OnStatusChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Label in the schema.
        /// </summary>
        /// <LongDescription>
        /// (Control specific) - Appointment Label
        /// </LongDescription>
        [DataMember(Order=11)]
        public System.Nullable<long> Label
        {
            get
            {
                return this._Label;
            }
            set
            {
                if (this._Label != value)
                {
                    this.OnLabelChanging(value);
                    this.SendPropertyChanging();
                    this._Label = value;
                    this.SendPropertyChanged("Label");
                    this.OnLabelChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Recurrence in the schema.
        /// </summary>
        /// <LongDescription>
        /// (Control specific) - Recurrence Information
        /// </LongDescription>
        [DataMember(Order=12)]
        public string Recurrence
        {
            get
            {
                return this._Recurrence;
            }
            set
            {
                if (this._Recurrence != value)
                {
                    this.OnRecurrenceChanging(value);
                    this.SendPropertyChanging();
                    this._Recurrence = value;
                    this.SendPropertyChanged("Recurrence");
                    this.OnRecurrenceChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Reminder in the schema.
        /// </summary>
        /// <LongDescription>
        /// (Control specific) - Reminder Info
        /// </LongDescription>
        [DataMember(Order=13)]
        public string Reminder
        {
            get
            {
                return this._Reminder;
            }
            set
            {
                if (this._Reminder != value)
                {
                    this.OnReminderChanging(value);
                    this.SendPropertyChanging();
                    this._Reminder = value;
                    this.SendPropertyChanged("Reminder");
                    this.OnReminderChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Datf in the schema.
        /// </summary>
        /// <LongDescription>
        /// DATF
        /// </LongDescription>
        [DataMember(Order=14)]
        public System.Nullable<System.DateTime> Datf
        {
            get
            {
                return this._Datf;
            }
            set
            {
                if (this._Datf != value)
                {
                    this.OnDatfChanging(value);
                    this.SendPropertyChanging();
                    this._Datf = value;
                    this.SendPropertyChanged("Datf");
                    this.OnDatfChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Datt in the schema.
        /// </summary>
        /// <LongDescription>
        /// DATT
        /// </LongDescription>
        [DataMember(Order=15)]
        public System.Nullable<System.DateTime> Datt
        {
            get
            {
                return this._Datt;
            }
            set
            {
                if (this._Datt != value)
                {
                    this.OnDattChanging(value);
                    this.SendPropertyChanging();
                    this._Datt = value;
                    this.SendPropertyChanged("Datt");
                    this.OnDattChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for RefType_Str in the schema.
        /// </summary>
        /// <LongDescription>
        /// Event = 1,
        /// Case = 2
        /// </LongDescription>
        [DataMember(Order=16)]
        public string RefType_Str
        {
            get
            {
                return this._RefType_Str;
            }
            set
            {
                if (this._RefType_Str != value)
                {
                    this.OnRefType_StrChanging(value);
                    this.SendPropertyChanging();
                    this._RefType_Str = value;
                    this.SendPropertyChanged("RefType_Str");
                    this.OnRefType_StrChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for RefId in the schema.
        /// </summary>
        /// <LongDescription>
        /// Reference internal Id
        /// </LongDescription>
        [DataMember(Order=17)]
        public long RefId
        {
            get
            {
                return this._RefId;
            }
            set
            {
                if (this._RefId != value)
                {
                    this.OnRefIdChanging(value);
                    this.SendPropertyChanging();
                    this._RefId = value;
                    this.SendPropertyChanged("RefId");
                    this.OnRefIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for OutlookEntryId in the schema.
        /// </summary>
        /// <LongDescription>
        /// OUTLOOK_ENTRY_ID
        /// </LongDescription>
        [DataMember(Order=18)]
        public string OutlookEntryId
        {
            get
            {
                return this._OutlookEntryId;
            }
            set
            {
                if (this._OutlookEntryId != value)
                {
                    this.OnOutlookEntryIdChanging(value);
                    this.SendPropertyChanging();
                    this._OutlookEntryId = value;
                    this.SendPropertyChanged("OutlookEntryId");
                    this.OnOutlookEntryIdChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
