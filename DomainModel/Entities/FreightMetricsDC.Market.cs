//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for Market in the schema.
    /// </summary>
    /// <LongDescription>
    /// Table that holds information about markets.
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class Market : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private string _Name;

        private MarketTypeEnum _Type = (MarketTypeEnum)Enum.Parse(typeof(MarketTypeEnum), "1");

        private int _Tonnes;

        private string _Cruser;

        private System.DateTime _Crd;

        private string _Chuser;

        private System.DateTime _Chd;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnNameChanging(string value);
        partial void OnNameChanged();
        partial void OnTypeChanging(MarketTypeEnum value);
        partial void OnTypeChanged();
        partial void OnTonnesChanging(int value);
        partial void OnTonnesChanged();
        partial void OnCruserChanging(string value);
        partial void OnCruserChanged();
        partial void OnCrdChanging(System.DateTime value);
        partial void OnCrdChanged();
        partial void OnChuserChanging(string value);
        partial void OnChuserChanged();
        partial void OnChdChanging(System.DateTime value);
        partial void OnChdChanged();
        #endregion

        public Market()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// The internal ID.
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Name in the schema.
        /// </summary>
        /// <LongDescription>
        /// The description.
        /// </LongDescription>
        [DataMember(Order=2)]
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if (this._Name != value)
                {
                    this.OnNameChanging(value);
                    this.SendPropertyChanging();
                    this._Name = value;
                    this.SendPropertyChanged("Name");
                    this.OnNameChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Type in the schema.
        /// </summary>
        /// <LongDescription>
        /// 1 = Capesize. 2 = Panamax. 3 = Supramax. 4 = Handysize
        /// </LongDescription>
        [DataMember(Order=3)]
        public MarketTypeEnum Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if (this._Type != value)
                {
                    this.OnTypeChanging(value);
                    this.SendPropertyChanging();
                    this._Type = value;
                    this.SendPropertyChanged("Type");
                    this.OnTypeChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Tonnes in the schema.
        /// </summary>
        /// <LongDescription>
        /// The default tonnes of a vessel for this market
        /// </LongDescription>
        [DataMember(Order=4)]
        public int Tonnes
        {
            get
            {
                return this._Tonnes;
            }
            set
            {
                if (this._Tonnes != value)
                {
                    this.OnTonnesChanging(value);
                    this.SendPropertyChanging();
                    this._Tonnes = value;
                    this.SendPropertyChanged("Tonnes");
                    this.OnTonnesChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Cruser in the schema.
        /// </summary>
        /// <LongDescription>
        /// The user / system that created the record.
        /// </LongDescription>
        [DataMember(Order=5)]
        public string Cruser
        {
            get
            {
                return this._Cruser;
            }
            set
            {
                if (this._Cruser != value)
                {
                    this.OnCruserChanging(value);
                    this.SendPropertyChanging();
                    this._Cruser = value;
                    this.SendPropertyChanged("Cruser");
                    this.OnCruserChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Crd in the schema.
        /// </summary>
        /// <LongDescription>
        /// The date when the record has been created.
        /// </LongDescription>
        [DataMember(Order=6)]
        public System.DateTime Crd
        {
            get
            {
                return this._Crd;
            }
            set
            {
                if (this._Crd != value)
                {
                    this.OnCrdChanging(value);
                    this.SendPropertyChanging();
                    this._Crd = value;
                    this.SendPropertyChanged("Crd");
                    this.OnCrdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Chuser in the schema.
        /// </summary>
        /// <LongDescription>
        /// The user / system that updated the record.
        /// </LongDescription>
        [DataMember(Order=7)]
        public string Chuser
        {
            get
            {
                return this._Chuser;
            }
            set
            {
                if (this._Chuser != value)
                {
                    this.OnChuserChanging(value);
                    this.SendPropertyChanging();
                    this._Chuser = value;
                    this.SendPropertyChanged("Chuser");
                    this.OnChuserChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Chd in the schema.
        /// </summary>
        /// <LongDescription>
        /// The date when the record has been updated.
        /// </LongDescription>
        [DataMember(Order=8)]
        public System.DateTime Chd
        {
            get
            {
                return this._Chd;
            }
            set
            {
                if (this._Chd != value)
                {
                    this.OnChdChanging(value);
                    this.SendPropertyChanging();
                    this._Chd = value;
                    this.SendPropertyChanged("Chd");
                    this.OnChdChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
