//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 05/04/2012 16:46:39
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Exis.Domain
{

    /// <summary>
    /// There are no comments for UgroupsPermission in the schema.
    /// </summary>
    /// <LongDescription>
    /// UGROUPS_PERMISSIONS
    /// </LongDescription>
    [DataContract(IsReference = true)]
    public partial class UgroupsPermission : INotifyPropertyChanging, INotifyPropertyChanged    
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(System.String.Empty);

        private long _Id;

        private long _UserGroupId;

        private long _PermissionId;
    
        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(long value);
        partial void OnIdChanged();
        partial void OnUserGroupIdChanging(long value);
        partial void OnUserGroupIdChanged();
        partial void OnPermissionIdChanging(long value);
        partial void OnPermissionIdChanged();
        #endregion

        public UgroupsPermission()
        {
            this.Initialize();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        /// <LongDescription>
        /// UGROUPS_PERM_ID
        /// </LongDescription>
        [DataMember(Order=1)]
        public long Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for UserGroupId in the schema.
        /// </summary>
        /// <LongDescription>
        /// UG_ID
        /// </LongDescription>
        [DataMember(Order=2)]
        public long UserGroupId
        {
            get
            {
                return this._UserGroupId;
            }
            set
            {
                if (this._UserGroupId != value)
                {
                    this.OnUserGroupIdChanging(value);
                    this.SendPropertyChanging();
                    this._UserGroupId = value;
                    this.SendPropertyChanged("UserGroupId");
                    this.OnUserGroupIdChanged();
                }
            }
        }

    
        /// <summary>
        /// There are no comments for PermissionId in the schema.
        /// </summary>
        /// <LongDescription>
        /// PR_ID
        /// </LongDescription>
        [DataMember(Order=3)]
        public long PermissionId
        {
            get
            {
                return this._PermissionId;
            }
            set
            {
                if (this._PermissionId != value)
                {
                    this.OnPermissionIdChanging(value);
                    this.SendPropertyChanging();
                    this._PermissionId = value;
                    this.SendPropertyChanged("PermissionId");
                    this.OnPermissionIdChanged();
                }
            }
        }
   
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, emptyChangingEventArgs);
        }

        protected virtual void SendPropertyChanging(System.String propertyName) 
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void SendPropertyChanged(System.String propertyName)
        {
             if (this.PropertyChanged != null)
                 this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            OnCreated();
        }
    
        [OnDeserializing()]
        [System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
        public void OnDeserializing(StreamingContext context)
        {
          this.Initialize();
        }
    }

}
