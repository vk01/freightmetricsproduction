﻿using System;

namespace Exis.Domain.Attributes
{
    public abstract class AppFieldBaseAttribute : Attribute
    {
        private string m_Description;
        private string m_Name;
        private string[] m_ObjectsToHide;

        internal AppFieldBaseAttribute(string Name, string Description)
        {
            m_Name = Name;
            m_Description = Description;
        }

        internal AppFieldBaseAttribute(string Name, string Description, string[] objectsToHide)
        {
            m_Name = Name;
            m_Description = Description;
            m_ObjectsToHide = objectsToHide;
        }


        public string Description
        {
            get { return m_Description; }
        }

        public string Name
        {
            get { return m_Name; }
        }

        public string[] ObjectsToHide
        {
            get { return m_ObjectsToHide; }
        }
    }
}