﻿using System;

namespace Exis.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class AppFieldPropertyValueAttribute : AppFieldBaseAttribute
    {
        public AppFieldPropertyValueAttribute(string Name, string Description)
            : base(Name, Description)
        {
        }
    }
}