﻿using System;

namespace Exis.Domain.Attributes
{
    public class AppFieldMethodUDFAttribute : AppFieldBaseAttribute
    {
        private readonly UDFRefTypeEnum m_UdfRefType;
        private bool m_IsException;

        public AppFieldMethodUDFAttribute(string name, string description, UDFRefTypeEnum UdfRefType)
            : base(name, description)
        {
            m_UdfRefType = UdfRefType;
        }

        public UDFRefTypeEnum UdfRefType
        {
            get { return m_UdfRefType; }
        }

        public bool IsException
        {
            get { return m_IsException; }
            set { m_IsException = true; }
        }
    }
}