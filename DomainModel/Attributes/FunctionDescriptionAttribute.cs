﻿using System;

namespace Exis.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Parameter)]
    public class FunctionDescriptionAttribute : Attribute
    {
        private String description;

        public FunctionDescriptionAttribute(string description)
        {
            this.description = description;
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
}
