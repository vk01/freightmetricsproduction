﻿using System;
using Exis.Domain;

namespace Exis.RuleEngine.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class RuleParameterAttribute : Attribute
    {
        private readonly string m_Name;
        private readonly AppFieldParameterTypeEnum m_RuleParameterType;
        private readonly Type m_Type;
        private readonly TypeOrListOfTypeEnum m_TypeOrListOfType;
        private bool m_IsFeed;
        private bool m_IsOptional;
        private bool m_IsHidden;
        private bool m_IsFeeded;

        private string m_Mask;
        private string m_TypeDefiningParameter;
        private int[] m_AvailableOperants;

        public string LookupValuesParameterName { get; set; }

        public string DynamicLookupValuesMethod { get; set; }
        public string DynamicLookupMethodParameter { get; set; }

        public AppFieldParameterTypeEnum RuleParameterType
        {
            get { return m_RuleParameterType; }
        }

        public Type Type
        {
            get { return m_Type; }
        }

        public bool IsFeed
        {
            get { return m_IsFeed; }
            set { m_IsFeed = value; }
        }

        public bool IsFeeded
        {
            get { return m_IsFeeded; }
            set { m_IsFeeded = value; }
        }

        public TypeOrListOfTypeEnum TypeOrListOfType
        {
            get { return m_TypeOrListOfType; }
        }

        public string TypeDefiningParameter
        {
            get { return m_TypeDefiningParameter; }
            set { m_TypeDefiningParameter = value; }
        }

        public int[] AvailableOperants
        {
            get { return m_AvailableOperants; }
            set { m_AvailableOperants = value; }
        }

        public string Mask
        {
            get { return m_Mask; }
            set { m_Mask = value; }
        }

        public bool IsOptional
        {
            get { return m_IsOptional; }
            set { m_IsOptional = value; }
        }

        public bool IsHidden
        {
            get { return m_IsHidden; }
            set
            {
                m_IsHidden = value;
                if (value)
                    m_IsOptional = true;
            }
        }

        public RuleParameterAttribute(string name, AppFieldParameterTypeEnum ruleParameterType, Type parameterType)
            : this(name, ruleParameterType, parameterType, TypeOrListOfTypeEnum.Both)
        {
        }

        public RuleParameterAttribute(string name, AppFieldParameterTypeEnum ruleParameterType, Type parameterType, TypeOrListOfTypeEnum typeOrListOfType)
        {
            m_Name = name;
            m_RuleParameterType = ruleParameterType;
            m_Type = parameterType;
            m_TypeOrListOfType = typeOrListOfType;
        }

        public override string ToString()
        {
            return m_Name;
        }
    }
}