﻿using System;

namespace Exis.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class AppFieldPropertyDomainAttribute : AppFieldBaseAttribute
    {
        public AppFieldPropertyDomainAttribute(string Name, string Description)
            : base(Name, Description)
        {
        }

        public AppFieldPropertyDomainAttribute(string Name, string Description, string[] objectsToHide)
            : base(Name, Description, objectsToHide)
        {
        }
    }
}