﻿using System;
using System.Collections.Generic;

namespace Exis.RuleEngine.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class RuleFunctionAttribute : Attribute
    {
        private readonly string m_Name;
        private readonly string m_description;
        private string m_FriendlyTextTemplate;

        private List<RuleParameterAttribute> m_Parameters = new List<RuleParameterAttribute>();

        public RuleFunctionAttribute(string Name, string descr)
        {
            m_Name = Name;
            m_description = descr;
        }

        public string Name
        {
            get { return m_Name; }
        }

        public string Description
        {
            get { return m_description; }
        }

        public List<RuleParameterAttribute> Parameters
        {
            get { return m_Parameters; }
            set { m_Parameters = value; }
        }

        public string FriendlyTextTemplate
        {
            get { return m_FriendlyTextTemplate; }
            set { m_FriendlyTextTemplate = value; }
        }

        public override string ToString()
        {
            string str = m_Name + " (";
            foreach (RuleParameterAttribute ruleParameterAttribute in m_Parameters)
            {
                str = str + ruleParameterAttribute + ",";
            }
            str = str.TrimEnd(',');
            str = str + ")";
            return str;
        }
    }
}