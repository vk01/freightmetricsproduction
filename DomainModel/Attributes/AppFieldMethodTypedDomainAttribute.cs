﻿using System;

namespace Exis.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class AppFieldMethodTypedDomainAttribute : AppFieldBaseAttribute
    {
        private readonly string m_SelectMethod;
        private readonly string m_MethodParameters;

        /// <summary>
        /// Typed domain is every domain object that requires a type to get it (eg. Addresses use AddressTypes)
        /// </summary>
        /// <param name="Name">The name to be shown</param>
        /// <param name="Description">The description of the attributed member</param>
        /// <param name="SelectMethod">The method that needs to be run to get the types. This string must have a format of "[Method Name],[Type name along with its namespace],[Assembly Name]"</param>
        public AppFieldMethodTypedDomainAttribute(string Name, string Description, string SelectMethod)
            : this(Name, Description, SelectMethod, null)
        {
        }

        public AppFieldMethodTypedDomainAttribute(string Name, string Description, string SelectMethod, string methodParameters)
            : base(Name, Description)
        {
            m_SelectMethod = SelectMethod;
            m_MethodParameters = methodParameters;
        }

        public string SelectMethod
        {
            get { return m_SelectMethod; }
        }

        public string MethodParameters
        {
            get { return m_MethodParameters; }
        }
    }
}