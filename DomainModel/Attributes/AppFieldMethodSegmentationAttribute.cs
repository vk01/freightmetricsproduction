﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exis.Domain.Attributes
{
    public class AppFieldMethodSegmentationAttribute : AppFieldBaseAttribute
    {
        public AppFieldMethodSegmentationAttribute(string Name, string Description) : base(Name, Description)
        {
        }
    }
}
