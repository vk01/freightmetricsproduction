﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exis.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class AppFieldLookupValueAttribute : AppFieldBaseAttribute
    {
        private readonly string m_SelectMethod;
        private readonly string m_SelectField;

        public AppFieldLookupValueAttribute(string Name, string Description, string SelectMethod, string SelectedField)
            : base(Name, Description)
        {
            m_SelectMethod = SelectMethod;
            m_SelectField = SelectedField;
        }

        public string SelectMethod
        {
            get { return m_SelectMethod; }
        }

        public string SelectField
        {
            get { return m_SelectField; }
        }
    }
}
