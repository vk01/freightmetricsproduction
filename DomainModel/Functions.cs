﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using Exis.Domain;
using Exis.RuleEngine.Attributes;

namespace Exis.RuleEngine
{
    public static class Functions
    {
        public static readonly DataContext context;

        #region Compare Collection

        [RuleFunction("Compare Collection Items",
            "Compares an aplication field with another aplication field or literal value by a specific operator",
            FriendlyTextTemplate = "{1} {2} occurences of {0} {3}")]
        public static bool CompareCollection(
            [RuleParameter("Collection", AppFieldParameterTypeEnum.ApplicationFieldOrFunction, typeof(Object),
                TypeOrListOfTypeEnum.ListOfType, IsFeed = true)] AppFieldFunctionParameter collection,
            [RuleParameter("Collection Operator", AppFieldParameterTypeEnum.LiteralValue,
                typeof(CollectionOperatorEnum))] AppFieldFunctionParameter collectionOperator,
            [RuleParameter("Collection Occurences", AppFieldParameterTypeEnum.LiteralValue, typeof(Int32),
                IsOptional = true, Mask = "[1-9][0-9]*")] AppFieldFunctionParameter collectionOccurences,
            [RuleParameter("Evaluation Function", AppFieldParameterTypeEnum.Function, typeof(bool), IsFeeded = true)] AppFieldFunctionParameter function)
        {
            int trueCounter = 0;
            if (collection.Value == null)
                return false;
            if (!(collection.Value is IList))
                throw new ApplicationException("Collection parameter is not a list.");

            var collectionValue = (IList)collection.Value;
            var collectionOperatorValue = (CollectionOperatorEnum)collectionOperator.Value;
            if (collectionOccurences.Value == null && collectionOperatorValue != CollectionOperatorEnum.All &&
                collectionOperatorValue != CollectionOperatorEnum.None)
            {
                throw new ApplicationException("Collection occurences is null and collection operation is not All or None");
            }

            int collectionOccurencesValue = 0;
            if (collectionOccurences.Value != null)
            {
                collectionOccurencesValue = (int)collectionOccurences.Value;
            }

            foreach (object obj in collectionValue)
            {
                //                collection.FeedValue = obj;
                var result = (bool)FunctionEvaluator.Evaluate(function, obj);
                if (result)
                    trueCounter++;
            }

            switch (collectionOperatorValue)
            {
                case CollectionOperatorEnum.All:
                    return trueCounter == collectionValue.Count;
                case CollectionOperatorEnum.AtLeast:
                    return trueCounter >= collectionOccurencesValue;
                case CollectionOperatorEnum.AtMost:
                    return trueCounter <= collectionOccurencesValue;
                case CollectionOperatorEnum.Exactly:
                    return trueCounter == collectionOccurencesValue;
                case CollectionOperatorEnum.None:
                    return trueCounter == 0;
            }
            return false;
        }

        #endregion

        #region Compare to Boolean

        [RuleFunction("Compare To Boolean",
            "Compares an application field of boolean type with another aplication field of boolean type or literal value by a specific operator"
            )]
        public static bool CompareToBoolean(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationFieldOrFunction,
                typeof(Boolean), LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(Boolean))] AppFieldFunctionParameter RightAppField)
        {
            bool bRet = false;
            if (LeftAppField.Value == null && RightAppField.Value == null)
                return true;

            if ((LeftAppField.Value == null && RightAppField.Value != null) ||
                (LeftAppField.Value != null && RightAppField.Value == null))
                return false;


            bool lValue = Convert.ToBoolean(LeftAppField.Value);
            bool rValue = Convert.ToBoolean(RightAppField.Value);

            switch ((CompareOperator)Operator.Value)
            {
                case CompareOperator.Equals:
                    bRet = lValue == rValue;
                    break;
            }
            return bRet;
        }

        #endregion

        #region Check if empty

        [RuleFunction("Application Field is empty", "Checks if the aplication field is empty",
            FriendlyTextTemplate = "{0} is empty")]
        public static bool IsEmpty(
            [RuleParameter("Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(Object))] AppFieldFunctionParameter AppField)
        {
            if (AppField.Value is DateTime && ((DateTime)AppField.Value) == DateTime.MinValue)
                return true;
            else
                return AppField.Value == null;
        }

        #endregion

        #region Compare to Enumeration

        [RuleFunction("Compare To Enumeration",
            "Compares an aplication field with another aplication field or literal value by a specific operator. The parameters must be of an enumerated type."
            )]
        public static bool CompareToEnum(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(Enum),
                LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter
                LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 7 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrLiteral, typeof(Enum),
                TypeDefiningParameter = "LeftAppField")] AppFieldFunctionParameter RightAppField)
        {
            bool bRet = false;

            if (LeftAppField.Value == null && RightAppField.Value != null)
            {
                return false;
            }
            //                throw new ApplicationException(String.Format("{0}: Left object was null",
            //                                                             MethodBase.GetCurrentMethod().Name));
            if (RightAppField.Value == null && LeftAppField.Value != null)
            {
                return false;
            }
            //                throw new ApplicationException(String.Format("{0}: Right object was null",
            //                                                             MethodBase.GetCurrentMethod().Name));

            if (RightAppField.Value == null && LeftAppField.Value == null)
            {
                return true;
            }

            if (LeftAppField.Value != null && RightAppField.Value != null)
            {
                int rightValueInt = (int) RightAppField.Value;
                int leftValueInt = (int) LeftAppField.Value;
                switch ((CompareOperator)Operator.Value)
                {
                    case CompareOperator.Equals:
                        bRet = leftValueInt == rightValueInt;
                        break;
                    case CompareOperator.NotEquals:
                        bRet = leftValueInt != rightValueInt;
                        break;
                }
            }
            return bRet;
        }

        #endregion

        #region Compare to Domain Object

        [RuleFunction("Compare To Domain Object",
            "Compares an application field with another aplication field or literal value by a specific operator. The parameters must be a domain object."
            )]
        public static bool CompareToDomainObject(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(DomainObject),
                LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 7 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrLiteral, typeof(DomainObject), TypeDefiningParameter = "LeftAppField")] AppFieldFunctionParameter
                RightAppField)
        {
            bool bRet = false;

            if (LeftAppField.Value == null)
                throw new ApplicationException(String.Format("{0}: Left object was null",
                                                             MethodBase.GetCurrentMethod().Name));
            if (RightAppField.Value == null)
                throw new ApplicationException(String.Format("{0}: Right object was null",
                                                             MethodBase.GetCurrentMethod().Name));

            Type leftValueType = LeftAppField.Value.GetType();
            Type rightValueType = RightAppField.Value.GetType();

            XmlSerializer xmlSerializer = new XmlSerializer(leftValueType);
            var stream = new MemoryStream();
            xmlSerializer.Serialize(stream, LeftAppField.Value);
            var leftValueString = Encoding.UTF8.GetString(stream.ToArray());
            stream = new MemoryStream();
            xmlSerializer.Serialize(stream, RightAppField.Value);
            var rightValueString = Encoding.UTF8.GetString(stream.ToArray());
/*
            string leftValueString = leftValueType.GetProperty("Id").GetValue(LeftAppField.Value, null) + "," +
                                     leftValueType.Name + "," + leftValueType.Assembly.GetName().Name;
*/
/*
            string rightValueString = !(RightAppField.Value is String)
                                          ? rightValueType.GetProperty("Id").GetValue(RightAppField.Value, null) + "," +
                                            rightValueType.Name + "," + rightValueType.Assembly.GetName().Name
                                          : RightAppField.Value.ToString();
*/

            switch ((CompareOperator)Operator.Value)
            {
                case CompareOperator.Equals:
                    bRet = leftValueString == rightValueString;
                    break;
                case CompareOperator.NotEquals:
                    bRet = leftValueString != rightValueString;
                    break;
            }
            return bRet;
        }

        #endregion

        #region Compare To DateTime

        [RuleFunction("Compare To DateTime",
            "Compares an aplication field with another aplication field or literal value by a specific operator")]
        public static bool CompareToDateTime(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationFieldOrFunction,
                typeof(DateTime), LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter
                LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 3, 4, 5, 6, 7 })] AppFieldFunctionParameter
                Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(DateTime))] AppFieldFunctionParameter
                RightAppField)
        {
            /*
            if (LeftAppField.Value == null && (CompareOperator) Operator.Value != CompareOperator.Equals &&
                (CompareOperator) Operator.Value != CompareOperator.NotEquals)
                throw new ApplicationException(String.Format("{0}: Left object was null and operator is {1}",
                                                             MethodBase.GetCurrentMethod().Name,
                                                             ((CompareOperator) Operator.Value).ToString("g")));
            if (RightAppField.Value == null && (CompareOperator) Operator.Value != CompareOperator.Equals &&
                (CompareOperator) Operator.Value != CompareOperator.NotEquals)
                throw new ApplicationException(String.Format("{0}: Right object was null and operator is {1}",
                                                             MethodBase.GetCurrentMethod().Name,
                                                             ((CompareOperator) Operator.Value).ToString("g")));
            */

            if (LeftAppField.Value == null || RightAppField.Value == null) return false;

            var leftDate = (DateTime)LeftAppField.Value;
            var rightDate = (DateTime)RightAppField.Value;

            switch ((CompareOperator)Operator.Value)
            {
                case CompareOperator.Equals:
                    return leftDate == rightDate;
                case CompareOperator.NotEquals:
                    return leftDate != rightDate;
                case CompareOperator.GreaterThan:
                    return leftDate > rightDate;
                case CompareOperator.GreaterThanOrEqual:
                    return leftDate >= rightDate;
                case CompareOperator.LessThan:
                    return leftDate < rightDate;
                case CompareOperator.LessThanOrEqual:
                    return leftDate <= rightDate;
                default:
                    return false;
            }
        }

        #endregion CompareToDateTime

        #region Compare Number

        [RuleFunction("Compare To Number",
            "Compares an application field with another aplication field or literal value by a specific operator")]
        public static bool CompareToNumber(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(Decimal), TypeOrListOfTypeEnum.Type,
                LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 3, 4, 5, 6, 7 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field, Function or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(Decimal))] AppFieldFunctionParameter RightAppField)
        {
            bool bRet = false;

            if (LeftAppField.Value == null && RightAppField.Value == null)
                return true;

            if ((LeftAppField.Value == null && RightAppField.Value != null) ||
                (LeftAppField.Value != null && RightAppField.Value == null))
                return false;

            var lValue = Convert.ToDecimal(LeftAppField.Value);
            var rValue = Convert.ToDecimal(RightAppField.Value);


            switch ((CompareOperator)Operator.Value)
            {
                case CompareOperator.Equals:
                    bRet = lValue.Equals(rValue);
                    break;
                case CompareOperator.NotEquals:
                    bRet = lValue != rValue;
                    break;
                case CompareOperator.GreaterThan:
                    bRet = lValue > rValue;
                    break;
                case CompareOperator.GreaterThanOrEqual:
                    bRet = lValue >= rValue;
                    break;
                case CompareOperator.LessThan:
                    bRet = lValue < rValue;
                    break;
                case CompareOperator.LessThanOrEqual:
                    bRet = lValue <= rValue;
                    break;
            }
            return bRet;
        }

        #endregion Compare Number

        #region Compare Text

        [RuleFunction("Compare To Text",
            "Compares an application field with another aplication field or literal value by a specific operator")]
        public static bool CompareToText(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationFieldOrFunction,
                typeof(String),
                LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 7 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(String))] AppFieldFunctionParameter
                RightAppField)
        {
            if (LeftAppField.Value == null && RightAppField.Value == null)
                return true;

            if ((LeftAppField.Value == null) || (RightAppField.Value == null))
                return false;

            if ((CompareOperator)Operator.Value == CompareOperator.In)
            {
                var rValues = RightAppField.Value as IList;
                if (rValues != null)
                {
                    if (LeftAppField.Value is IList)
                    {
                        var lValues = LeftAppField.Value as IList;
                        foreach (string lValue in lValues)
                        {
                            bool found = false;
                            foreach (string rValue in rValues)
                            {
                                if (lValue == rValue)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                    else
                    {
                        foreach (string rValue in rValues)
                        {
                            if (rValue == LeftAppField.Value as string)
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                }
            }
            else
            {
                var rValue = RightAppField.Value as string;
                var lValue = LeftAppField.Value as string;
                switch ((CompareOperator)Operator.Value)
                {
                    case CompareOperator.Equals:
                        return lValue == rValue;
                    case CompareOperator.NotEquals:
                        return lValue != rValue;
                }
            }
            return false;
        }

        [RuleFunction("Compare To String Length",
            "Compares the length of an application field of string type with the length of another aplication field of string type or literal value by a specific operator"
            )]
        public static bool CompareToStringLength(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(String))] AppFieldFunctionParameter
                LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 3, 4, 5, 6 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrLiteral, typeof(String))] AppFieldFunctionParameter
                RightAppField)
        {
            bool bRet = false;
            if (LeftAppField.Value == null && RightAppField.Value == null)
                return true;

            if ((LeftAppField.Value == null) || (RightAppField.Value == null))
                return false;

            int lValue;
            int rValue;

            lValue = LeftAppField.Value.ToString().Length;
            rValue = RightAppField.Value.ToString().Length;

            switch ((CompareOperator)Operator.Value)
            {
                case CompareOperator.Equals:
                    bRet = lValue == rValue;
                    break;
                case CompareOperator.NotEquals:
                    bRet = lValue != rValue;
                    break;
                case CompareOperator.GreaterThan:
                    bRet = lValue > rValue;
                    break;
                case CompareOperator.GreaterThanOrEqual:
                    bRet = lValue >= rValue;
                    break;
                case CompareOperator.LessThan:
                    bRet = lValue < rValue;
                    break;
                case CompareOperator.LessThanOrEqual:
                    bRet = lValue <= rValue;
                    break;
            }
            return bRet;
        }

        #endregion Compare Text

        #region Compare File

        [RuleFunction("Compare file contents",
            "Compares a file application field with another aplication field or literal value by a specific operator")]
        public static bool CompareFileContents(
            [RuleParameter("Left Application Field", AppFieldParameterTypeEnum.ApplicationFieldOrFunction,
                typeof(byte[]),
                LookupValuesParameterName = "RightAppField")] AppFieldFunctionParameter LeftAppField,
            [RuleParameter("Operator", AppFieldParameterTypeEnum.LiteralValue, typeof(CompareOperator),
                AvailableOperants = new[] { 1, 2, 7 })] AppFieldFunctionParameter Operator,
            [RuleParameter("Right Application Field or Literal Value",
                AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(byte[]))] AppFieldFunctionParameter
                RightAppField)
        {
            if (LeftAppField.Value == null && RightAppField.Value == null)
                return true;

            if ((LeftAppField.Value == null && RightAppField.Value != null) ||
                (LeftAppField.Value != null && RightAppField.Value == null))
                return false;

            string lValue = Convert.ToBase64String((byte[])LeftAppField.Value);
            if ((CompareOperator)Operator.Value == CompareOperator.In)
            {
                var listValues = (List<object>)RightAppField.Value;
                if (listValues != null)
                {
                    foreach (byte[] objValue in listValues)
                    {
                        if (lValue == Convert.ToBase64String(objValue))
                            return true;
                    }
                }
            }
            else
            {
                string rValue = Convert.ToBase64String((byte[])RightAppField.Value);

                switch ((CompareOperator)Operator.Value)
                {
                    case CompareOperator.Equals:
                        return lValue == rValue;
                    case CompareOperator.NotEquals:
                        return lValue != rValue;
                }
            }
            return false;
        }

        #endregion

        #region Object Functions

        #endregion

        #region String Functions

        [RuleFunction("Concatenate 3 Strings", "Places three strings one after the other.")]
        public static string ConCat2Strings(
            [RuleParameter("1st value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter str1,
            [RuleParameter("2nd value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter str2,
            [RuleParameter("3rd value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter str3)
        {
            if (str1.Value == null)
                str1.Value = "";
            if (str2.Value == null)
                str2.Value = "";
            if (str3.Value == null)
                str3.Value = "";

            return Convert.ToString(str1.Value) + Convert.ToString(str2.Value) + Convert.ToString(str3.Value);
        }

        [RuleFunction("Get String Under Condition",
            "Returns a string from an application field, literal value or function IF a condition is met.")]
        public static string GetStringUnderCondition(
            [RuleParameter("Condition Function", AppFieldParameterTypeEnum.Function, typeof(bool))] AppFieldFunctionParameter condition,
            [RuleParameter("String", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter value)
        {
            if ((bool)condition.Value) return (string)value.Value;

            return string.Empty;
        }

        [RuleFunction("Concatenate string",
            "Concatenates 2 strings, the first parameters goes before the second separated by the third parameter.",
            FriendlyTextTemplate = "Prepend {0} to {1} separated by {2}")]
        public static string ConcatStrings(
            [RuleParameter("Prepended value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral,
                typeof(String))] AppFieldFunctionParameter prependString,
            [RuleParameter("Appended value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral,
                typeof(String))] AppFieldFunctionParameter appendedString,
            [RuleParameter("Separator", AppFieldParameterTypeEnum.LiteralValue, typeof(TextAppendSeparator))] AppFieldFunctionParameter appendSeparator)
        {
            string finalText = null;
            if (prependString.Value == null)
            {
                finalText = "";
            }
            else
            {
                finalText = prependString.Value as string;
            }

            var separatorValue = (TextAppendSeparator)appendSeparator.Value;
            switch (separatorValue)
            {
                case TextAppendSeparator.Pipe:
                    finalText += "|";
                    break;
                case TextAppendSeparator.Comma:
                    finalText += ",";
                    break;
                case TextAppendSeparator.CRLF:
                    finalText += Environment.NewLine;
                    break;
            }

            if (appendedString.Value != null)
            {
                finalText += appendedString.Value as string;
            }

            return finalText;
        }

        [RuleFunction("SubString", "Gets a portion of the original string specified by the start index and the length of the desired string.")]
        public static string Substring(
            [RuleParameter("Input String", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter str,
            [RuleParameter("Start Index", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(Decimal))] AppFieldFunctionParameter startIndex,
            [RuleParameter("Length", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(Decimal))] AppFieldFunctionParameter len)
        {
            if (str.Value == null) return "";

            if (Convert.ToInt32(len.Value) > 0) return ((string)str.Value).Substring(Convert.ToInt32(startIndex.Value), Convert.ToInt32(len.Value));
            else return ((string)str.Value).Substring(Convert.ToInt32(startIndex.Value), ((string)str.Value).Length + Convert.ToInt32(len.Value));
        }

        [RuleFunction("String Length", "Gets the length of a string.")]
        public static Decimal Substring(
            [RuleParameter("Input String", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter str)
        {
            if (str.Value == null) return 0;
            return Convert.ToDecimal(((string)str.Value).Length);
        }
        #endregion

        #region Number Functions

        [RuleFunction("Concatenate number & sign",
            "Takes a sign and a number (in whatever form) and returns a signed double.")]
        public static Decimal GetDouble(
            [RuleParameter("Sign AppField", AppFieldParameterTypeEnum.ApplicationField, typeof(string))] AppFieldFunctionParameter sign,
            [RuleParameter("Number AppField", AppFieldParameterTypeEnum.ApplicationField, typeof(object))] AppFieldFunctionParameter number)
        {
            if (sign.Value == null || ((string)sign.Value != "+" && (string)sign.Value != "-"))
                sign.Value = "+";
            Decimal num = 0;
            if (number.Value == null || !Decimal.TryParse(number.Value.ToString(), out num))
                number.Value = 0;

            if ((string)sign.Value == "+")
                return Convert.ToDecimal(number.Value);
            else return 0 - Convert.ToDecimal(number.Value);
        }

        [RuleFunction("Add number to application field", "Adds a number to the application field.")]
        public static Decimal AddNumberToAppField(
            [RuleParameter("Number AppField", AppFieldParameterTypeEnum.ApplicationField, typeof(Decimal))] AppFieldFunctionParameter appNumber,
            [RuleParameter("Literal Number", AppFieldParameterTypeEnum.LiteralValue, typeof(Decimal))] AppFieldFunctionParameter litNumber)
        {
            return (Decimal)appNumber.Value + (Decimal)litNumber.Value;
        }

        [RuleFunction("Substract number from application field", "Substracts a number from the application field.")]
        public static Decimal SubstractNumberFromAppField(
            [RuleParameter("Number AppField", AppFieldParameterTypeEnum.ApplicationField, typeof(Decimal))] AppFieldFunctionParameter appNumber,
            [RuleParameter("Literal Number", AppFieldParameterTypeEnum.LiteralValue, typeof(Decimal))] AppFieldFunctionParameter litNumber)
        {
            return (Decimal)appNumber.Value - (Decimal)litNumber.Value;
        }

        [RuleFunction("Get Number Under Condition",
            "Returns a number from an application field, literal value or function IF a condition is met, ELSE return the second value.")]
        public static Decimal GetNumberUnderCondition(
            [RuleParameter("Condition Function", AppFieldParameterTypeEnum.Function, typeof(bool))] AppFieldFunctionParameter condition,
            [RuleParameter("Number If True", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(Decimal))] AppFieldFunctionParameter value1,
            [RuleParameter("Number If False", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(Decimal))] AppFieldFunctionParameter value2)
        {
            if ((bool)condition.Value) return (Decimal)value1.Value;
            else return (Decimal)value2.Value;
        }

        #endregion

        #region Date/Time Functions

        [RuleFunction("Shift Date Time", "Shifts the given date time by the given time span.",
            FriendlyTextTemplate = "{0} shifted by {1}")]
        public static DateTime ShiftDateTime(
            [RuleParameter("Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(DateTime))] AppFieldFunctionParameter appField,
            [RuleParameter("Shift Time Span", AppFieldParameterTypeEnum.LiteralValue, typeof(TimeSpan))] AppFieldFunctionParameter spanParameter)
        {
            if (appField.Value == null)
                throw new Exception("ShiftDateTime: DateTime parameter was null");

            var dateTime = (DateTime)appField.Value;
            var span = (TimeSpan)spanParameter.Value;
            return dateTime.Add(span);
        }

        [RuleFunction("Shift Date Times", "Shifts all datetimes in the given list by the given time span.",
            FriendlyTextTemplate = "{0} shifted by {1}")]
        public static List<DateTime> ShiftDateTimes(
            [RuleParameter("Application Field", AppFieldParameterTypeEnum.ApplicationField, typeof(List<DateTime>))] AppFieldFunctionParameter appField,
            [RuleParameter("Shift Time Span", AppFieldParameterTypeEnum.LiteralValue, typeof(TimeSpan))] AppFieldFunctionParameter spanParameter)
        {
            if (appField.Value == null)
                throw new Exception("ShiftDateTime: DateTime parameter was null");

            var dateTimes = (List<DateTime>)appField.Value;
            var shiftedDateTimes = new List<DateTime>();
            var span = (TimeSpan)spanParameter.Value;
            foreach (DateTime dateTime in dateTimes)
            {
                shiftedDateTimes.Add(dateTime.Add(span));
            }
            return shiftedDateTimes;
        }

        [RuleFunction("Get DateTime Under Condition",
            "Returns a DateTime from an application field, literal value or function IF a condition is met.")]
        public static DateTime GetDateTimeUnderCondition(
            [RuleParameter("Condition Function", AppFieldParameterTypeEnum.Function, typeof(bool))] AppFieldFunctionParameter condition,
            [RuleParameter("DateTime", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(DateTime))
            ] AppFieldFunctionParameter value)
        {
            if ((bool)condition.Value) return (DateTime)value.Value;

            return new DateTime();
        }

        #endregion

        #region Negation Function

        [RuleFunction("Negate", "Negates the result of the given function", FriendlyTextTemplate = "Not {0}")]
        public static bool Not(
            [RuleParameter("Function", AppFieldParameterTypeEnum.Function, typeof(bool))] AppFieldFunctionParameter
                AppField)
        {
            return !((bool)AppField.Value);
        }

        #endregion

        #region View Function

        [RuleFunction("Get View Column Value",
            "Gets a single column value from the specified view filtered by the specified value.",
            FriendlyTextTemplate = "Column {1} from view {0} where {2} equals {3}")]
        public static object GetDbViewValue(
            [RuleParameter("View Name", AppFieldParameterTypeEnum.LiteralValue, typeof(String),
                DynamicLookupValuesMethod = "AllViews,Exis.Domain.SystemData,Exis.DomainModel")] AppFieldFunctionParameter viewName,
            [RuleParameter("Value Column Name", AppFieldParameterTypeEnum.LiteralValue, typeof(String),
                DynamicLookupValuesMethod = "ViewColumns,Exis.Domain.SystemData,Exis.DomainModel",
                DynamicLookupMethodParameter = "viewName")] AppFieldFunctionParameter ValueFieldName,
            [RuleParameter("Value Type", AppFieldParameterTypeEnum.LiteralValue, typeof(UDFTypeEnum))] AppFieldFunctionParameter valueType,
            [RuleParameter("Filter Column Name", AppFieldParameterTypeEnum.LiteralValue, typeof(String),
                DynamicLookupValuesMethod = "ViewColumns,Exis.Domain.SystemData,Exis.DomainModel",
                DynamicLookupMethodParameter = "viewName")] AppFieldFunctionParameter FilterFieldName,
            [RuleParameter("Filter Value", AppFieldParameterTypeEnum.ApplicationField, typeof(object))] AppFieldFunctionParameter FilterFieldValue,
            [RuleParameter("Query Only", AppFieldParameterTypeEnum.LiteralValue, typeof(Boolean), IsHidden = true)] AppFieldFunctionParameter queryOnly
            )
        {
            Type type = typeof(string);
            switch ((UDFTypeEnum)valueType.Value)
            {
                case UDFTypeEnum.Numeric:
                    type = typeof(Decimal);
                    break;
                case UDFTypeEnum.Date:
                    type = typeof(DateTime);
                    break;
            }
            if (queryOnly.Value != null && (Boolean)queryOnly.Value)
            {
                return context.Query<object>("SELECT " + ValueFieldName.Value + " FROM " + viewName.Value);
            }
            IEnumerable a = context.ExecuteQuery(type,
                                                 "SELECT " + ValueFieldName.Value + " FROM " + viewName.Value +
                                                 " WHERE " + FilterFieldName.Value +
                                                 " = " + FilterFieldValue.Value);
            foreach (object VARIABLE in a)
            {
                return VARIABLE;
            }
            return null;
        }

        #endregion

        #region Logical Operands Functions

        [RuleFunction("Compare Boolean Values with the \"OR\" operator", "Returns true if either of the values is true."
            )]
        public static bool GetValueORValue(
            [RuleParameter("1st Value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(bool))] AppFieldFunctionParameter value1,
            [RuleParameter("2nd Value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(bool))] AppFieldFunctionParameter value2)
        {
            return (bool)value1.Value || (bool)value2.Value;
        }

        [RuleFunction("Compare Boolean Values with the \"AND\" operator", "Returns true if both values are true.")]
        public static bool GetValueANDValue(
            [RuleParameter("1st Value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(bool))] AppFieldFunctionParameter value1,
            [RuleParameter("2nd Value", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(bool))] AppFieldFunctionParameter value2)
        {
            return (bool)value1.Value && (bool)value2.Value;
        }

        #endregion

        #region Tax Registration Number Validation

        [RuleFunction("Validate Tax Registration Number", "Returns true is valid, false if not."
            )]
        public static bool IsTaxRegNoValid([RuleParameter("Tax Registration Number", AppFieldParameterTypeEnum.ApplicationFieldOrFunctionOrLiteral, typeof(string))] AppFieldFunctionParameter taxRegistrationNumber)
        {
            if (string.IsNullOrEmpty(((string)taxRegistrationNumber.Value)))
                return true;

            // Checking if all characters are digits
            long output;
            if (long.TryParse((string)taxRegistrationNumber.Value, out output) == false)
                return false;

            int iSum;
            int iRem;
            int i;
            int iLastDigit;

            if (((string)taxRegistrationNumber.Value).Length != 9) return false;

            iSum = 0;
            for (i = 1; i < ((string)taxRegistrationNumber.Value).Length; i++)
            {
                int iFirst;
                iFirst = Convert.ToInt32(((string)taxRegistrationNumber.Value).Substring(i - 1, 1));
                int iSecond;
                iSecond = Convert.ToInt32(Math.Pow(Convert.ToDouble(2), Convert.ToDouble(((string)taxRegistrationNumber.Value).Length - i)));
                iSum = iSum + (iFirst * iSecond);
            }

            if (iSum == 0) return false;

            iRem = Convert.ToInt32(Math.Floor(Convert.ToDouble(iSum / 11)) * 11);
            iLastDigit = Convert.ToInt32(((string)taxRegistrationNumber.Value).Substring(((string)taxRegistrationNumber.Value).Length - 1, 1));

            if ((iLastDigit != 0 && (iRem + iLastDigit) == iSum) ||
                (iLastDigit == 0 && (iRem == iSum || (iRem + 10) == iSum)))
                return true;
            return false;
        }

        #endregion
    }

    #region Public Enumerations

    public enum CompareOperator
    {
        Null = 0,
        Equals = 1,
        NotEquals = 2,
        LessThan = 3,
        LessThanOrEqual = 4,
        GreaterThan = 5,
        GreaterThanOrEqual = 6,
        In = 7
    }

    public enum DateTimeGranularity
    {
        None = 0,
        Date = 1,
        Time = 2,
        Days = 3,
        Month = 4,
        Year = 5
    }

    public enum NumberGranularity
    {
        None = 0,
        All = 1,
        IntegerPart = 2
    }

    public enum CollectionOperatorEnum
    {
        All = 1,
        Exactly = 2,
        AtLeast = 3,
        AtMost = 4,
        None = 5
    }

    public enum ValuesInOutList
    {
        Null = 0,
        ValuesInList = 1,
        ValuesNotInList = 2
    }

    #endregion
}