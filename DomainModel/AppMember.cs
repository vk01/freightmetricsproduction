﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Exis.Domain.Attributes;

namespace Exis.Domain
{
    [DataContract(Namespace = "Exis.Domain", IsReference = true)]
    public class AppMember
    {
        #region Private Variables

        private readonly AppFieldBaseAttribute m_Attribute;
        private readonly string m_Value;
        private bool m_CanBeSelected;
        private List<AppMember> m_Children;
        private bool m_ChildrenCreated;
        private DataContext m_Context;
        private string m_Description;
        private List<object> m_LookupValues;
        private MemberInfo m_MemberInfo;
        private object[] m_MethodParameters;
        private string m_Name;
        private AppMember m_ParentMember;
        private object m_RuntimeObject;
        private ScopeEnum m_Scope;
        private Type m_SelectorType;
        private long? m_SelectorTypeId;
        private bool m_SelectorTypeIdNeeded;

        #endregion

        #region Constructors

        private AppMember(DataContext context, Type selectorType, long? selectorTypeId, ScopeEnum scope,
                          MemberInfo memberInfo,
                          AppFieldBaseAttribute attribute, AppMember parentAppMember, string name, string description,
                          string value, object runtimeObject)
        {
            m_Context = context;
            m_SelectorType = selectorType;
            m_SelectorTypeId = selectorTypeId;

            m_MemberInfo = memberInfo;
            m_Attribute = attribute;
            m_ParentMember = parentAppMember;
            m_RuntimeObject = runtimeObject;
            m_Name = name;
            m_Description = description;
            m_Value = value;
            m_Scope = scope;

            Initialize();
        }

        private AppMember(DataContext context, Type selectorType, long? selectorTypeId, ScopeEnum scope,
                          MemberInfo memberInfo, AppFieldBaseAttribute attribute, AppMember parentAppMember)
            : this(context, selectorType, selectorTypeId, scope, memberInfo, attribute, parentAppMember,
                   null, null, null, null)
        {
        }

        public AppMember(DataContext context, Type selectorType, long? selectorTypeId)
            : this(context, selectorType, selectorTypeId, ScopeEnum.Null, null, null, null)
        {
        }

        #endregion

        #region Public Properties

        [DataMember]
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        [DataMember]
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        [DataMember]
        public List<object> LookupValues
        {
            get { return m_LookupValues; }
            set { m_LookupValues = value; }
        }

        public ICollection ListValues
        {
            get
            {
                if (IsList)
                {
                    if (m_SelectorType == typeof (Udf) && m_RuntimeObject != null)
                    {
                        var udf = (Udf) m_RuntimeObject;
                        if (udf.IsList)
                        {
                            return udf.ListValues;
                        }
                    }
                }
                return null;
            }
        }

        [DataMember]
        public List<AppMember> Children
        {
            get { return m_Children; }
            set { m_Children = value; }
        }

        [DataMember]
        public AppMember ParentMember
        {
            get { return m_ParentMember; }
            set { m_ParentMember = value; }
        }

        public AppFieldBaseAttribute Attribute
        {
            get { return m_Attribute; }
        }

        [DataMember]
        public object RuntimeObject
        {
            get { return m_RuntimeObject; }
            set { m_RuntimeObject = value; }
        }

        public DataContext DataContext
        {
            get { return m_Context; }
            set { m_Context = value; }
        }

        [DataMember]
        public bool ChildrenCreated
        {
            get { return m_ChildrenCreated; }
            set { m_ChildrenCreated = value; }
        }

        public Type SelectorType
        {
            get { return m_SelectorType; }
            set { m_SelectorType = value; }
        }

        [DataMember]
        public string SelectorTypeName
        {
            get { return (m_SelectorType != null) ? m_SelectorType.ToString() : null; }
            set { m_SelectorType = !String.IsNullOrEmpty(value) ? Type.GetType(value) : null; }
        }

        [DataMember]
        public long? SelectorTypeId
        {
            get { return m_SelectorTypeId; }
            set { m_SelectorTypeId = value; }
        }

        public MemberInfo MemberInfo
        {
            get { return m_MemberInfo; }
            set { m_MemberInfo = value; }
        }

        public string AppFieldText
        {
            get
            {
                string sRet = "";
                const string m_PathSeparator = "->";

                if (m_ParentMember != null)
                    sRet += m_ParentMember.AppFieldText;
                else
                    sRet += m_Name;

                if (m_Attribute != null)
                {
                    sRet += m_PathSeparator + m_Attribute.Name;
                }
                else if (m_ParentMember != null)
                {
                    sRet += m_PathSeparator + m_Name;
                }

                return sRet;
            }
        }

        public string AppFieldPath
        {
            get { return String.Format("[afev({0})]", GetAppFieldPath()); }
        }

        public Type AppFieldType
        {
            get
            {
                if (m_MemberInfo is PropertyInfo)
                {
                    var propertyInfo = (PropertyInfo) m_MemberInfo;
                    if (propertyInfo.PropertyType.IsArray)
                    {
                        return propertyInfo.PropertyType.GetElementType();
                    }
                    if (IsTypeList(propertyInfo.PropertyType))
                    {
                        return propertyInfo.PropertyType.GetGenericArguments()[0];
                    }
                    return propertyInfo.PropertyType;
                }
                if (m_MemberInfo is MethodInfo)
                {
                    var memberInfo = (MethodInfo) m_MemberInfo;
                    if (memberInfo.ReturnType.IsArray)
                    {
                        return memberInfo.ReturnType.GetElementType();
                    }
                    if (IsTypeList(memberInfo.ReturnType))
                    {
                        return memberInfo.ReturnType.GetGenericArguments()[0];
                    }
                    return memberInfo.ReturnType;
                }
                if (m_MemberInfo == typeof (Udf) && m_RuntimeObject != null)
                {
                    Type udfType;
                    var udf = (Udf) m_RuntimeObject;
                    switch (udf.Type)
                    {
                        case UDFTypeEnum.Date:
                            udfType = typeof (DateTime);
                            break;
                        case UDFTypeEnum.Status:
                            udfType = typeof (Boolean);
                            break;
                        case UDFTypeEnum.Numeric:
                            udfType = typeof (Decimal);
                            break;
                        case UDFTypeEnum.Image:
                        case UDFTypeEnum.File:
                            udfType = typeof (byte[]);
                            break;
                        case UDFTypeEnum.InternalLink:
                            if (udf.SubType_Str == InternalLinkUDFSubTypeEnum.User.ToString("d"))
                            {
                                udfType = typeof (User);
                            }
                            else if (udf.SubType_Str == InternalLinkUDFSubTypeEnum.Case.ToString("d"))
                            {
                                udfType = typeof (Case);
                            }
                            else if (udf.SubType_Str == InternalLinkUDFSubTypeEnum.Event.ToString("d"))
                            {
                                udfType = typeof (Event);
                            }
                            else
                            {
                                udfType = typeof (DomainObject);
                            }
                            break;
                        default:
                            udfType = typeof (String);
                            break;
                    }

                    //                    return HasMultipleValues ? typeof(List<>).MakeGenericType(udfType) : udfType;
                    return udfType;
                }
                if (m_MemberInfo == typeof (Segmentation) && m_RuntimeObject != null)
                {
                    var segmentation = (Segmentation) m_RuntimeObject;
                    Type segmentationType = null;
                    switch (segmentation.Type)
                    {
                        case SegmentationTypeEnum.Case:
                            segmentationType = typeof (Case);
                            break;
                        case SegmentationTypeEnum.Event:
                            segmentationType = typeof (Event);
                            break;
                    }

                    return segmentationType;
                }
                return m_SelectorType;
            }
        }

        public bool HasMultipleValues
        {
            get
            {
                if (m_SelectorType == typeof (Udf) && m_RuntimeObject != null)
                {
                    var udf = (Udf) m_RuntimeObject;
                    if (udf.IsMultiple)
                    {
                        return true;
                    }
                }
                if (m_SelectorType == typeof (Segmentation))
                {
                    return true;
                }
                if (m_ParentMember != null && m_ParentMember.IsTypedDomain && m_ParentMember.HasMultipleValues)
                {
                    return true;
                }
                if (m_Attribute != null)
                {
                    if (m_Attribute is AppFieldPropertyDomainAttribute || m_Attribute is AppFieldPropertyValueAttribute)
                    {
                        Type propertyType = ((PropertyInfo) m_MemberInfo).PropertyType;
                        return propertyType.IsGenericType &&
                               propertyType.GetGenericTypeDefinition() == typeof (List<>);
                    }
                    if (m_Attribute is AppFieldMethodTypedDomainAttribute)
                    {
                        Type returnType = ((MethodInfo) m_MemberInfo).ReturnType;
                        return returnType.IsGenericType &&
                               returnType.GetGenericTypeDefinition() == typeof (List<>);
                    }
                }
                return false;
            }
        }

        public bool IsList
        {
            get
            {
                if (m_SelectorType == typeof (Udf) && m_RuntimeObject != null)
                {
                    var udf = (Udf) m_RuntimeObject;
                    if (udf.IsList)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool IsLookup
        {
            get
            {
                if (m_Attribute is AppFieldLookupValueAttribute)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsDomainType
        {
            get
            {
                if (m_ParentMember == null)
                {
                    return true;
                }
                if (AppFieldType.IsSubclassOf(typeof (DomainObject)) && m_SelectorType != typeof (Segmentation))
                {
                    return true;
                }
                if (m_Attribute != null)
                {
                    if (m_Attribute is AppFieldPropertyDomainAttribute ||
                        m_Attribute is AppFieldMethodTypedDomainAttribute ||
                        m_Attribute is AppFieldMethodSegmentationAttribute)
                    {
                        return true;
                    }
                }
                if (m_MemberInfo == null)
                    return true;
                return false;
            }
        }

        [DataMember]
        public bool SelectorTypeIdNeeded
        {
            get { return m_SelectorTypeIdNeeded; }
            set { m_SelectorTypeIdNeeded = value; }
        }

        public bool IsUdfs
        {
            get { return m_Attribute is AppFieldMethodUDFAttribute; }
        }

        public bool IsUdf
        {
            get { return m_MemberInfo == typeof (Udf); }
        }

        public bool IsWritableProperty
        {
            get
            {
                return (m_Attribute is AppFieldPropertyDomainAttribute || m_Attribute is AppFieldPropertyValueAttribute) &&
                       ((PropertyInfo) m_MemberInfo).CanWrite;
            }
        }

        public bool IsTypedDomain
        {
            get { return m_Attribute is AppFieldMethodTypedDomainAttribute; }
        }

        public bool IsSegmentation
        {
            get { return m_Attribute is AppFieldMethodSegmentationAttribute; }
        }

        [DataMember]
        public bool CanBeSelected
        {
            get { return m_CanBeSelected; }
            set { m_CanBeSelected = value; }
        }

        #endregion

        #region Private Methods

        private void Initialize()
        {
            m_Children = new List<AppMember>();
            m_CanBeSelected = true;

            if (m_Attribute != null)
            {
                m_Name = m_Name ?? m_Attribute.Name;
                m_Description = m_Attribute.Description;
/*                if (m_Attribute is AppFieldLookupValueAttribute)
                {
                    CreateLookupValues((AppFieldLookupValueAttribute)m_Attribute);
                }
                else */
                if (m_Attribute is AppFieldMethodTypedDomainAttribute)
                {
                    var attribute = (AppFieldMethodTypedDomainAttribute) m_Attribute;
                    var currentMethodInfo = (MethodInfo) m_MemberInfo;
                    if (currentMethodInfo.GetParameters().Count() == 1 &&
                        !IsTypeNullable(currentMethodInfo.GetParameters()[0].ParameterType))
                    {
                        m_CanBeSelected = false;
                    }
                    if (!String.IsNullOrEmpty(attribute.MethodParameters) && m_ParentMember != null &&
                        m_ParentMember.RuntimeObject != null)
                    {
                        var methodParameters = new List<object>();
                        string[] parameters = attribute.MethodParameters.Split(',');
                        foreach (string parameter in parameters)
                        {
                            PropertyInfo propertyInfo = m_ParentMember.RuntimeObject.GetType().GetProperty(parameter);
                            if (propertyInfo != null)
                            {
                                object propertyValue = propertyInfo.GetValue(m_ParentMember.RuntimeObject, null);
                                methodParameters.Add(propertyValue);
                            }
                        }
                        m_MethodParameters = methodParameters.ToArray();
                    }
/*
                    if (m_SelectorTypeId == null)
                    {
                        string typeMethodName = ((AppFieldMethodTypedDomainAttribute)m_Attribute).SelectMethod;
                        Type typeMethod = Type.GetType(typeMethodName.Substring(typeMethodName.IndexOf(',') + 1));
                        MethodInfo typeMethodInfo =
                            typeMethod.GetMethod(typeMethodName.Substring(0, typeMethodName.IndexOf(',')),
                                                 BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                        if(typeMethodInfo.GetParameters().Length > 0)
                        {
                            m_SelectorTypeIdNeeded = true;
                        }
                    }
*/
                }
                else if (m_Attribute is AppFieldMethodSegmentationAttribute)
                {
                    m_CanBeSelected = false;
                }
            }
            else if (m_Name == null)
            {
                m_Name = m_SelectorType.Name;
            }

            AddContextToType(m_SelectorType);

            if (m_SelectorTypeId == null && IsUdfs)
            {
                m_SelectorTypeIdNeeded = true;
            }
            else if (m_SelectorTypeId != null)
            {
                if (m_SelectorType == typeof (Event))
                {
                    EventType eventType =
                        m_Context.GetChangeSet().Inserts.OfType<EventType>().SingleOrDefault(
                            a => a.Id == m_SelectorTypeId.Value) ??
                        m_Context.EventTypes.SingleOrDefault(a => a.Id == m_SelectorTypeId.Value);
                    if (eventType != null)
                        m_Scope = eventType.Scope;
                }
                else if (m_SelectorType == typeof (Case))
                {
                    CaseType caseType =
                        m_Context.GetChangeSet().Inserts.OfType<CaseType>().SingleOrDefault(
                            a => a.Id == m_SelectorTypeId.Value) ??
                        m_Context.CaseTypes.SingleOrDefault(a => a.Id == m_SelectorTypeId.Value);
                    if (caseType != null)
                        m_Scope = caseType.Scope;
                }
            }
        }

        private void CreatePropertyChildren(Type selectorType)
        {
            PropertyInfo[] members =
                selectorType.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);

            foreach (PropertyInfo propertyInfo in members)
            {
                if (IsPropertyMember(propertyInfo))
                {
                    var attribute =
                        (AppFieldBaseAttribute)
                        propertyInfo.GetCustomAttributes(typeof (AppFieldBaseAttribute), false)[0];

                    if (attribute.Name == m_Name) continue;
                    if (attribute.ObjectsToHide != null && attribute.ObjectsToHide.Contains(m_Name))
                        continue;
                    // Case Scope Filtering
                    if (m_Scope != ScopeEnum.Null && selectorType == typeof (Case))
                    {
                        switch (m_Scope)
                        {
                            case ScopeEnum.Order:
                                if (propertyInfo.Name == "ContractService" ||
                                    propertyInfo.Name == "ServiceIdentifierValue")
                                    continue;
                                break;
                            case ScopeEnum.Service:
                                if (propertyInfo.Name == "Contract" || propertyInfo.Name == "ServiceIdentifierValue")
                                    continue;
                                break;
                            case ScopeEnum.Identifier:
                                if (propertyInfo.Name == "Contract" || propertyInfo.Name == "ContractService")
                                    continue;
                                break;
                        }
                    }
                    m_Children.Add(new AppMember(m_Context, propertyInfo.PropertyType, null, m_Scope, propertyInfo,
                                                 attribute, this));
                }
            }
        }

        private void CreateMethodChildren(Type selectorType)
        {
            MethodInfo[] members =
                selectorType.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);

            foreach (MethodInfo methodInfo in members)
            {
                if (IsMethodMember(methodInfo))
                {
                    var attribute =
                        (AppFieldBaseAttribute) methodInfo.GetCustomAttributes(typeof (AppFieldBaseAttribute), false)[0];
                    m_Children.Add(new AppMember(m_Context, m_SelectorType, m_SelectorTypeId, m_Scope, methodInfo,
                                                 attribute, this));
                }
            }
        }

        private void CreateTypedDomainObjects(AppFieldMethodTypedDomainAttribute typedDomainAttribute)
        {
            var currentMethodInfo = (MethodInfo) m_MemberInfo;
            Type returnValueType = currentMethodInfo.ReturnType.IsGenericType &&
                                   currentMethodInfo.ReturnType.GetGenericTypeDefinition() == typeof (List<>)
                                       ? currentMethodInfo.ReturnType.GetGenericArguments()[0]
                                       : currentMethodInfo.ReturnType;

            string typeMethodName = typedDomainAttribute.SelectMethod;
            Type typeMethod = Type.GetType(typeMethodName.Substring(typeMethodName.IndexOf(',') + 1));
            AddContextToType(typeMethod);
            Type[] types = m_MethodParameters != null
                               ? m_MethodParameters.Select(a => a.GetType()).ToArray()
                               : Type.EmptyTypes;
            MethodInfo typeMethodInfo =
                typeMethod.GetMethod(typeMethodName.Substring(0, typeMethodName.IndexOf(',')),
                                     BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public, null, types,
                                     null);

            if (typeMethodInfo != null)
            {
                object typeValues = typeMethodInfo.Invoke(null, m_MethodParameters);
                if (typeValues.GetType().IsGenericType &&
                    typeValues.GetType().GetGenericTypeDefinition() == typeof (List<>))
                {
                    var typeListValues = (IList) typeValues;
                    if (typeValues.GetType().GetGenericArguments()[0].IsSubclassOf(typeof (DomainObject)))
                    {
                        foreach (object listValue in typeListValues)
                        {
                            var listValueId = (long) listValue.GetType().GetProperty("Id").GetValue(listValue, null);
                            m_Children.Add(new AppMember(m_Context, returnValueType, listValueId, m_Scope, null, null,
                                                         this,
                                                         listValue.ToString(), null, listValueId.ToString(), listValue));
                        }
                    }
                    else if (typeValues.GetType().GetGenericArguments()[0].IsEnum)
                    {
                        foreach (object listValue in typeListValues)
                        {
                            m_Children.Add(new AppMember(m_Context, returnValueType, null, m_Scope, null, null, this,
                                                         ((Enum) listValue).ToString("g"), null,
                                                         ((Enum) listValue).ToString("d"), listValue));
                        }
                    }
                    else
                    {
                        foreach (object listValue in typeListValues)
                        {
                            m_Children.Add(new AppMember(m_Context, returnValueType, null, m_Scope, null, null, this,
                                                         listValue.ToString(), null,
                                                         listValue.ToString(), listValue));
                        }
                    }
                }
            }
        }

        private void CreateUdfObjects(AppFieldMethodUDFAttribute attribute)
        {
            if (attribute != null)
            {
                if (attribute.IsException)
                {
                }
                else
                {
                    List<Udf> objUdfRefTypeCol = m_Context.Udfs.Where(a =>
                                                                      a.RefType_Str ==
                                                                      attribute.UdfRefType.ToString("d") &&
                                                                      a.RefTypeId == m_SelectorTypeId &&
                                                                      a.Status_Str ==
                                                                      ActivationStatusEnum.Active.ToString("d")).ToList();
                    objUdfRefTypeCol.AddRange(m_Context.GetChangeSet().Inserts.OfType<Udf>().Where(a => a.RefType_Str ==
                                                                                                        attribute.
                                                                                                            UdfRefType.
                                                                                                            ToString("d") &&
                                                                                                        a.RefTypeId ==
                                                                                                        m_SelectorTypeId &&
                                                                                                        a.Status_Str ==
                                                                                                        ActivationStatusEnum
                                                                                                            .Active.
                                                                                                            ToString("d")));
                    if (m_SelectorTypeId != null)
                        objUdfRefTypeCol.AddRange(m_Context.Udfs.Where(a =>
                                                                       a.RefType_Str ==
                                                                       attribute.UdfRefType.ToString("d") &&
                                                                       a.RefTypeId == null &&
                                                                       a.Status_Str ==
                                                                       ActivationStatusEnum.Active.ToString("d")));

                    foreach (Udf udf in objUdfRefTypeCol)
                    {
                        if (udf.Type == UDFTypeEnum.InternalLink)
                        {
                            var subType =
                                (InternalLinkUDFSubTypeEnum)
                                Enum.Parse(typeof (InternalLinkUDFSubTypeEnum), udf.SubType_Str);
                            switch (subType)
                            {
                                case InternalLinkUDFSubTypeEnum.Case:
                                    m_Children.Add(new AppMember(m_Context, typeof (Case), udf.RefTypeId, m_Scope,
                                                                 typeof (Udf),
                                                                 null,
                                                                 this, udf.Name, GetUdfDataString(udf),
                                                                 udf.Id.ToString(), udf));
                                    break;
                                case InternalLinkUDFSubTypeEnum.Event:
                                    m_Children.Add(new AppMember(m_Context, typeof (Event), udf.RefTypeId, m_Scope,
                                                                 typeof (Udf),
                                                                 null,
                                                                 this, udf.Name, GetUdfDataString(udf),
                                                                 udf.Id.ToString(), udf));
                                    break;
                                default:
                                    m_Children.Add(new AppMember(m_Context, typeof (Udf), udf.Id, m_Scope, typeof (Udf),
                                                                 null,
                                                                 this,
                                                                 udf.Name,
                                                                 GetUdfDataString(udf), udf.Id.ToString(), udf));
                                    break;
                            }
                        }
                        else
                        {
                            m_Children.Add(new AppMember(m_Context, typeof (Udf), udf.Id, m_Scope, typeof (Udf), null,
                                                         this,
                                                         udf.Name,
                                                         GetUdfDataString(udf), udf.Id.ToString(), udf));
                        }
                    }
                }
            }
        }

        private void CreateLookupValues(AppFieldLookupValueAttribute lookupAttribute)
        {
            m_LookupValues = new List<object>();
            Type lookupType =
                Type.GetType(lookupAttribute.SelectMethod.Substring(lookupAttribute.SelectMethod.IndexOf(',') + 1));
            AddContextToType(lookupType);
            MethodInfo lookupMethodInfo =
                lookupType.GetMethod(
                    lookupAttribute.SelectMethod.Substring(0, lookupAttribute.SelectMethod.IndexOf(',')),
                    BindingFlags.Static | BindingFlags.Public);
            if (lookupMethodInfo != null)
            {
                object lookupValues = lookupMethodInfo.Invoke(null, null);
                if (lookupValues.GetType().IsGenericType &&
                    lookupValues.GetType().GetGenericTypeDefinition() == typeof (List<>))
                {
                    var lookupList = (IList) lookupValues;
                    foreach (object listValue in lookupList)
                    {
                        m_LookupValues.Add(listValue);
                    }
                }
            }
        }

        private void CreateSegmentations()
        {
            foreach (Segmentation segmentation in m_Context.Segmentations.ToList())
            {
                m_Children.Add(new AppMember(m_Context, typeof (Segmentation), null, m_Scope, typeof (Segmentation),
                                             null, this,
                                             segmentation.Description, null, segmentation.Id.ToString(), segmentation));
            }
        }

        private string GetUdfDataString(Udf udf)
        {
            string udfData = udf.Code + ", " + udf.Type.ToString("g");
            if (udf.IsMultiple)
                udfData += ", " + udf.MultipleType.ToString("g") + " Multiple";
            if (udf.IsList)
            {
                if (udf.ListType == UDFListTypeEnum.Predefined)
                    udfData += ", Predefined";
                else
                    udfData += ", Runtime";
                udfData += " List";
            }
            if (udf.IsDefault)
            {
                if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    udfData += ", Predefined";
                else if (udf.DefaultType == UDFDefaultTypeEnum.Runtime)
                    udfData += ", Runtime";
                else
                    udfData += ", Evaluated";
                udfData += " Default";
            }
            return udfData;
        }

        private bool IsPropertyMember(PropertyInfo info)
        {
            var attributes = (AppFieldBaseAttribute[]) info.GetCustomAttributes(typeof (AppFieldBaseAttribute), false);
            foreach (AppFieldBaseAttribute attribute in attributes)
            {
                if (attribute is AppFieldPropertyDomainAttribute || attribute is AppFieldPropertyValueAttribute ||
                    attribute is AppFieldLookupValueAttribute)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsMethodMember(MethodInfo info)
        {
            var attributes = (AppFieldBaseAttribute[]) info.GetCustomAttributes(typeof (AppFieldBaseAttribute), false);
            foreach (AppFieldBaseAttribute attribute in attributes)
            {
                if (attribute is AppFieldMethodTypedDomainAttribute || attribute is AppFieldMethodUDFAttribute ||
                    attribute is AppFieldMethodSegmentationAttribute)
                {
                    return true;
                }
            }
            return false;
        }

        private string GetMethodString()
        {
            string sRet = "";

            sRet += m_MemberInfo.Name + "(";

            ParameterInfo[] parameters = ((MethodInfo) m_MemberInfo).GetParameters();

            foreach (ParameterInfo info in parameters)
            {
                if (info.ParameterType.IsGenericType &&
                    info.ParameterType.GetGenericTypeDefinition() == typeof (Nullable<>))
                {
                    sRet += ",";
                }
                else
                {
                    sRet += "<";
                    sRet += " " + info.ParameterType.Name + " " + info.Name;
                    sRet += ">";
                    sRet += ",";
                }
            }

            if (sRet.EndsWith(","))
                sRet = sRet.Substring(0, sRet.Length - 1);
            sRet += ")";

            return sRet;
        }

        private void AddContextToType(Type type)
        {
            FieldInfo contextField = type.GetField("context",
                                                   BindingFlags.NonPublic | BindingFlags.Public |
                                                   BindingFlags.Static | BindingFlags.Instance);
            if (contextField != null && contextField.FieldType == typeof (DataContext))
            {
                if (contextField.IsStatic || (m_RuntimeObject != null && m_RuntimeObject.GetType() == type))
                    contextField.SetValue(m_RuntimeObject, m_Context);
            }
        }

        private bool IsTypeNullable(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>);
        }

        private bool IsTypeList(Type type)
        {
            if (type == typeof (object))
            {
                return false;
            }
            if (type == null)
            {
                return false;
            }
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof (List<>))
            {
                return true;
            }
            return IsTypeList(type.BaseType);
        }

        public string GetAppFieldPath()
        {
            string sRet = "";
            const string m_PathSeparator = "->";

            if (m_ParentMember == null)
            {
                return m_Name ?? m_SelectorType.Name;
            }

            sRet += m_ParentMember.GetAppFieldPath();

            if (m_MemberInfo != null)
            {
                if (m_MemberInfo.MemberType == MemberTypes.Property)
                    sRet += m_PathSeparator + m_MemberInfo.Name;
                else if (m_MemberInfo.MemberType == MemberTypes.Method)
                {
                    sRet += m_PathSeparator + GetMethodString();
                }
            }

            if (!String.IsNullOrEmpty(m_Value))
            {
                int startIndex = sRet.LastIndexOf('(');
                if (startIndex != -1)
                {
                    sRet = sRet.Substring(0, startIndex);
                    sRet += String.Format("({0})", m_Value);
                }
                else
                {
                    sRet += m_PathSeparator + m_Value;
                }
            }
            if (m_MemberInfo == typeof (Udf) || m_MemberInfo == typeof (Segmentation))
            {
                sRet += m_PathSeparator + "Values";
            }
            return sRet;
        }

        #endregion

        #region Public Methods

        public void LoadChildren()
        {
            if (m_Attribute is AppFieldMethodUDFAttribute)
            {
                CreateUdfObjects((AppFieldMethodUDFAttribute) m_Attribute);
            }
            else if (m_Attribute is AppFieldMethodTypedDomainAttribute)
            {
                if (String.IsNullOrEmpty(m_Value))
                    CreateTypedDomainObjects((AppFieldMethodTypedDomainAttribute) m_Attribute);
            }
            else if (m_Attribute is AppFieldMethodSegmentationAttribute)
            {
                CreateSegmentations();
            }
            else
            {
                CreatePropertyChildren(m_SelectorType);
                CreateMethodChildren(m_SelectorType);
            }

            var sortedChildren = new List<AppMember>();
            sortedChildren.AddRange(m_Children.Where(a => !a.IsDomainType && !a.IsUdfs));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && !a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsUdfs));
            m_Children = sortedChildren;

            m_ChildrenCreated = true;
        }

        public void LoadChildrenUdfObjects()
        {
            CreateUdfObjects((AppFieldMethodUDFAttribute) m_Attribute);

            var sortedChildren = new List<AppMember>();
            sortedChildren.AddRange(m_Children.Where(a => !a.IsDomainType && !a.IsUdfs));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && !a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsUdfs));
            m_Children = sortedChildren;

            m_ChildrenCreated = true;
        }

        public void LoadChildrenTypedDomainObjects()
        {
            if (String.IsNullOrEmpty(m_Value))
                CreateTypedDomainObjects((AppFieldMethodTypedDomainAttribute) m_Attribute);

            var sortedChildren = new List<AppMember>();
            sortedChildren.AddRange(m_Children.Where(a => !a.IsDomainType && !a.IsUdfs));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && !a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsUdfs));
            m_Children = sortedChildren;

            m_ChildrenCreated = true;
        }

        public void LoadChildrenSegmentations()
        {
            CreateSegmentations();

            var sortedChildren = new List<AppMember>();
            sortedChildren.AddRange(m_Children.Where(a => !a.IsDomainType && !a.IsUdfs));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && !a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsUdfs));
            m_Children = sortedChildren;

            m_ChildrenCreated = true;
        }

        public void LoadChildrenOtherObjects()
        {
            CreatePropertyChildren(m_SelectorType);
            CreateMethodChildren(m_SelectorType);

            var sortedChildren = new List<AppMember>();
            sortedChildren.AddRange(m_Children.Where(a => !a.IsDomainType && !a.IsUdfs));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && !a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsDomainType && !a.IsUdfs && a.HasMultipleValues));
            sortedChildren.AddRange(m_Children.Where(a => a.IsUdfs));
            m_Children = sortedChildren;

            m_ChildrenCreated = true;
        }

        public void LoadSecondaryType(long? selectorTypeId)
        {
            m_SelectorTypeId = selectorTypeId;
            m_SelectorTypeIdNeeded = false;
        }

        public void LoadLookupValues()
        {
            CreateLookupValues((AppFieldLookupValueAttribute) m_Attribute);
        }

        public static AppMember Parse(DataContext context, string appFieldPath)
        {
            string[] members = appFieldPath.Split(new[] {"->"}, StringSplitOptions.None);
            Type selectorType = Type.GetType("Exis.Domain." + members[0]);
            var appMember = new AppMember(context, selectorType, null);
            foreach (string member in members)
            {
                if (member == members[0]) continue;
                appMember.LoadChildren();
                if (member.Contains('('))
                {
                    string parameter = member.Substring(member.IndexOf('(') + 1, member.Length - member.IndexOf('(') - 2);
                    foreach (AppMember child in appMember.Children)
                    {
                        if (child.m_MemberInfo is MethodInfo &&
                            child.m_MemberInfo.Name == member.Substring(0, member.IndexOf('(')))
                        {
                            appMember = child;
                            break;
                        }
                    }
                    if (appMember.Attribute != null)
                    {
                        if (appMember.Attribute is AppFieldMethodUDFAttribute)
                        {
                            if (parameter.Contains("<"))
                                continue;

                            Udf udf =
                                context.Udfs.Where(
                                    a =>
                                    a.Id == Convert.ToInt64(parameter)).Single();
                            appMember.m_SelectorTypeId = udf.RefTypeId;
                            appMember = new AppMember(context, typeof (Udf), udf.Id, ScopeEnum.Null, typeof (Udf), null,
                                                      appMember,
                                                      udf.Name,
                                                      udf.Code, udf.Id.ToString(), udf);
                        }
                        else if (appMember.Attribute is AppFieldMethodSegmentationAttribute)
                        {
                            Segmentation segmentation =
                                context.Segmentations.Single(a => a.Id == Convert.ToInt64(parameter));
                            appMember = new AppMember(context, typeof (Segmentation), null, ScopeEnum.Null,
                                                      typeof (Segmentation), null, appMember,
                                                      segmentation.Description, null, segmentation.Id.ToString(),
                                                      segmentation);
                        }
                        else if (appMember.Attribute is AppFieldMethodTypedDomainAttribute)
                        {
                            appMember.LoadChildren();
                            foreach (AppMember child in appMember.Children)
                            {
                                if (child.m_Value == parameter)
                                {
                                    appMember = child;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (member == "Values" &&
                        (appMember.RuntimeObject is Udf || appMember.RuntimeObject is Segmentation))
                    {
                        continue;
                    }

                    foreach (AppMember child in appMember.Children)
                    {
                        if (child.m_MemberInfo is PropertyInfo && child.m_MemberInfo.Name == member)
                        {
                            appMember = child;
                            break;
                        }
                    }
                }
            }
            return appMember;
        }

        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}