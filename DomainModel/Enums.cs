﻿using System;

namespace Exis.Domain
{

    public enum InterestReferenceRateTypeEnum
    {
        Libor = 1,
        Deposit = 2,
        RiskFree = 3
    }

    public enum InterestReferencePeriodTypeEnum
    {
        Week1 = 1,
        Weeks2 = 2,
        Weeks3 = 3,
        Month1 = 4,
        Months2 = 5,
        Year1 = 6,
        Year2 = 7,
        Months3 = 8,
        Months4 = 9,
        Months5 = 10,
        Months6 = 11,
        Months7 = 12,
        Months8 = 13,
        Months9 = 14,
        Months10 = 15,
        Months11 = 16,
        Year3 = 17,
        Year5 = 18,
        Year7 = 19,
        Year10 = 20,
        Year20 = 21,
        Year30 = 22
    }

    public enum CashFlowModelGroupOperatorEnum
    {
        Addition = 1,
        Subtraction = 2
    }


    public enum CashFlowItemSystemTypeEnum
    {
        SpotRevenue = 1,
        VoyageRevenueMinimum = 2,
        VoyageRevenueOptional = 3,
        VoyageRevenueFixedHireMinimum = 4,
        VoyageRevenueFixedHireOptional = 5,
        VoyageRevenueIndexLinkedHireMinimum = 6,
        VoyageRevenueIndexLinkedHireOptional = 7,
        VoyageRevenueProfitSharing = 8,
        VoyageCostsMinimum = 9,
        VoyageCostsOptional = 10,
        VoyageCostsFixedHireMinimum = 11,
        VoyageCostsFixedHireOptional = 12,
        VoyageCostsIndexLinkedHireMinimum = 13,
        VoyageCostsIndexLinkedHireOptional = 14,
        VoyageCostsProfitSharing = 15,
        FFAsSettlement = 16,
        FreightOptionsSettlement = 17,
        FreightOptionsPremium = 18,
        TCCommissions = 19,
        CargoCommissions = 20,
        FFACommissions = 21,
        OptionsCommissions = 22,
        VesselsOperationalExpenses = 23,
        VesselsDrydockingExpenses = 24,
        VesselsManagementFees = 25,
        VesselsCommercialFees = 26,
        VesselsScrappings = 27,
        VesselsSales = 28,
        VesselsAcquisitionCosts = 29,
        VesselsEquityInjection = 30,
        LoansCapitalRepayments = 31,
        LoansInterestExpenses = 32,
        LoansBaloonPayments = 33,
        LoansPrepayments = 34,
        LoansAdministrationFees = 35,
        LoansArrangementFees = 36,
        LoansPrepaymentFees = 37,
        LoansCommitmentFees = 38,
        LoansDrawdowns = 39
    }

    public enum LoanRepaymentFrequencyEnum
    {
        Month = 1,
        Quarter = 2,
        Semester = 3,
        Year = 4
    }

    public enum LoanInterestRateTypeEnum
    {
        Fixed = 1,
        Floating = 2
    }

    public enum LoanCollateralTypeEnum
    {
        Vessel = 1,
        Cash = 2,
        Other = 3
    }

    public enum MarketTypeEnum
    {
        Capesize = 1,
        Panamax = 2,
        Supramax = 3,
        Handysize = 4
    }

    public enum LimitMonitoringEntityTypeEnum
    {
        Trader = 1,
        ProfitCentre = 2,
        Desk = 3
    }

    public enum LimitMonitoringLimitTypeEnum
    {
        VaR = 1,
        MTM = 2,
        Capital = 3,
        Notional = 4,
        Duration = 5,
        Expiration = 6,
        Exposure = 7
    }

    public enum ContactTypeEnum
    {
        VesselManager = 1,
        BankRelationshipManager = 2,
        Vessel = 3,
        VesselMasterOfficer = 4,
        VesselChiefOfficer = 5
    }

    public enum TradeInfoPeriodDayCountTypeEnum
    {
        Thirty = 1,
        Actual = 2
    }

    public enum PeriodTypeEnum
    {
        Month = 1,
        Quarter = 2,
        Calendar = 3
    }

    public enum TradeOPTIONInfoTypeEnum
    {
        Call = 1,
        Put = 2
    }
    public enum TradeTypeWithTypeOptionInfoEnum
    {
        TC = 1,
        FFA = 2,
        Cargo = 3,
        Call = 4,
        Put = 5
    }

    public enum TradeInfoVesselAllocationTypeEnum
    {
        None = 1,
        Single = 2,
        Pool = 3
    }

    public enum TradeInfoSettlementTypeEnum
    {
        Last7Days = 1,
        AllDays = 2
    }

    public enum TradeFFAInfoPurposeEnum
    {
        Hedge = 1,
        Spec = 2
    }

    public enum TradeInfoQuantityTypeEnum
    {
        PerMonth = 1,
        Total = 2
    }

    public enum TradeInfoDirectionEnum
    {
        InOrBuy = 1,
        OutOrSell = 2
    }
    public enum TradeCompanySubtypeEnum
    {
        ShipOwning = 10
    }
    public enum TradeInfoLegOptionalStatusEnum
    {
        Pending = 1,
        Declared = 2,
        NonDeclared = 3
    }

    public enum TradeInfoLegRateTypeEnum
    {
        Fixed = 1,
        IndexLinked = 2
    }
    public enum TradeInfoCompanySubtypeEnum
    {
        ShipOwning = 10
        
    }
    public enum TradeStateEnum
    {
        Draft = 1,
        Confirmed = 2,
        Locked = 3
    }

    public enum CompanyTypeEnum
    {
        Internal = 1,
        External = 2,
        Affiliate = 3
    }

    public enum CompanySubtypeEnum
    {
        Counterparty = 1,
        Broker = 2,
        ClearingBank = 3,
        Bank = 4,
        FundManager = 5,
        TechnicalManager = 6,
        CommercialManager = 7,
        Other = 9
    }

    public enum TradeTypeEnum
    {
        TC = 1,
        FFA = 2,
        Cargo = 3,
        Option = 4
    }

    public enum LockedObjectTypeEnum
    {
        Case = 1,
        Event = 2,
        Null
    }

    public enum ReportTemplateTypeEnum
    {
        RPT = 1,
        REPX = 2,
        Null
    }

    public enum CaseEventPriorityEnum
    {
        Trivial = 1,
        Minor = 2,
        Normal = 3,
        Major = 4,
        Critical = 5,
        Null
    }

    public enum PaymentMethodTypeEnum
    {
        Debit = 1,
        Credit = 2,
        Null
    }

    public enum EventTypeSourceTypeEnum
    {
        Standard = 1,
        Predefined = 2
    }

    public enum ControlActionTypeEnum
    {
        Add, 
        Edit,
        View
    }

    public enum FormActionTypeEnum
    {
        Add,
        AddLike,
        Edit,
        View
    } ;

    public enum SearchCustomerTypeEnum
    {
        FindCustomer,
        FindDealer,
        FindParentCustomer,
        FindChildCustomer,
        FindAppointmentCustomer
    };
    
    public enum SearchCustomerResultEnum
    {
        Add,
        Edit,
        Select
    };

    public enum ProductOfferStatusEnum
    {
        Active = 1,
        Design = 0,
        Inactive = 2,
        Null
    };

    public enum WorkTimeDayTypeEnum
    {
        Sunday = 1,
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6,
        Saturday = 7,
        WorkDay = 8,
        WeekendDay = 9,
        Null
    }

    public enum WFETWaitTypeEnum
    {
        AfterOpenActions = 1,
        AfterCloseActions = 2,
        Null
    }

    public enum ContractFieldType
    {
        Manual = 1,
        System = 2,
        Null
    } ;

    public enum WFETIntervalTypeEnum
    {
        Hours = 1,
        Days = 2,
        Minutes = 3,
        Null
    }

    public enum WorkflowDiagramTypeEnum
    {
        User = 1,
        System = 2,
        Null
    }

    public enum ServiceTargetType
    {
        Mandatory = 1,
        Optional = 0,
        Null
    }

    public enum CommissionPeriodsEnum
    {
        Week = 1,
        BiWeek = 2,
        Month = 3,
        Quarter = 4,
        Semester = 5,
        Year = 6,
        Null
    }

    public enum CommProgCBDPDetailBalanceTypeEnum
    {
        Any = 0,
        Positive = 1,
        Negative = 2,
        Null
    }

    public enum CommissionProgramTargetTypeEnum
    {
        Product = 1,
        Service = 2,
        ServiceIdentifier = 3,
        Null
    }

    public enum CommissionAdjustmentJustificationEnum
    {
        Fraud = 1,
        Lead = 2,
        Other = 3,
        Null
    }

    public enum CommissionProgramCondCodeEnum
    {
        AirtimeRevenue = 1,
        CBDPMinCommission = 2,
        ChargeType = 3,
        PaymentMethod = 4,
        Product = 5,
        Service = 6,
        ActiDeactItem = 7,
        CustomerCategory = 8,
        ServiceProperty = 9,
        ServicePropertyValue = 10,
        CBDPBalance = 11
    }

    public enum CommissionProgramCalculationTypeEnum
    {
        Current = 1,
        Parent = 2,
        Null
    }

    public enum CommissionProgramTypeEnum
    {
        Loyalty = 0,
        Airtime = 1,
        CBDP = 2,
        AirtimeCustomers = 3,
        Null
    }

    public enum NewCommissionProgramTypeEnum
    {
        CBDP = 1,
        Airtime = 2,
        Null
    }

    public enum CommissionProgramTypeCalculationsEnum
    {
        Airtime_Loyalty = 1,
        CBDP = 2,
        All = 3,
        Null
    }

    public enum CommissionProgramCondTypeEnum
    {
        Loyalty = 0,
        Airtime = 1,
        CBDP = 2,
        CBDPService = 21,
        CBDPServiceProperty = 22,
        CBDPSiProperty = 23,
        CBDPPaymentMethod = 24,
        Null
    }

    public enum CommissionCalculationTypeEnum
    {
        AirtimeMonthly = 1,
        AirtimeYearly = 2,
        CBDPMonthly = 3,
        CBDPYearly = 4,
        AllMonthly = 5,
        AllYearly = 6,
        Null
    }

    public enum CommissionCalculationActionEnum
    {
        Calculation = 1,
        Recalculation = 2,
        Deletion = 3,
        Null
    }

    public enum CommissionEventActionEnum
    {
        Info = 0,
        Insert = 1,
        Modify = 2,
        Null
    }

    public enum CommissionEventTypeEnum
    {
        Activation = 1,
        Deactivation = 2,
        Null
    }

    public enum CommActionExecutionStateEnum
    {
        Idle = 1,
        ToRun = 2,
        Running = 3,
        Null
    }

    public enum CommissionEventTargetEnum
    {
        Service = 1,
        ServiceProperty = 2,
        SIProperty = 3,
        PaymentMethod = 4,
        Contract = 5,
        Null
    }

    public enum ContractLLUType
    {
        NonFullLLU = 5,
        FullLLU = 6,
        Null
    } ;

    public enum CommMonthEnum
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12,
        Null
    };

    public enum LogCommissionActionTypeEnum
    {
        InvoicesClosure = 1,
        InvoicesClosureRollback = 2,
        CommissionsCalculation = 3,
        CommissionsRecalculation = 4,
        CommissionsDeletion = 5,
        CommissionsEventsGeneration = 6,
        CommissionsCalculationSim = 7,
        CommissionsRecalculationSim = 8,
        CommissionsDeletionSim = 9,
        ReportGeneration = 10,
        ReportGenerationSim = 11,
        ReportSendMail = 12,
        Null
    }

    public enum LogCommissionActionCCTypeEnum
    {
        AirtimeCommissions = 1,
        CBDPCommissions = 2,
        AllCommissions = 3,
    //    CBDPCommissionsSummary = 4,
    //    NewOrdersWithoutCommissions = 5,
        Null
    }

    public enum LogCommissionActionResultEnum
    {
        Failure = 0,
        PartialSuccess = 1,
        Success = 2,
        Null
    }

    public enum CommissionProgramEffectiveDateTypeEnum
    {
        Activation = 0,
        Registration = 1,
        InitialRegistration = 2,
        Null
    }

    public enum WorkflowRuleDelayEnum
    {
        Minutes,
        Hours,
        Days
    }

    public enum ReportFormatEnum
    {
        PDF = 1,
        Word = 2,
        Excel = 3,
        CSV = 4
    }

    public enum ReportMailSentEnum
    {
        NotSent = 0,
        Sent = 1
    }

    public enum ReportMailDeliveredEnum
    {
        No = 0,
        Yes = 1
    }

    public enum TimeFormatEnum
    {
        mm = 1,
        hh = 2,
        dd = 3,
        hhmm = 4,
        ddhh = 5

    }

    public enum CustomerCategorizationRuleTypeEnum
    {
        TableOrView = 1,
        Null
    }

    public enum WFEXGStateEnum
    {
        Idle = 1,
        ToRun = 2,
        RunningEvents = 3,
        RunningCases = 4,
        Run = 5,
        Null
    }

    public enum WFEXGRecIntervalTypeEnum
    {
        Minutes = 1,
        Hours = 2,
        Days = 3,
        Weeks = 4,
        Months = 5,
        Null
    }

    public enum WFGroupAssocTypeEnum
    {
        CaseType = 1,
        EventType = 2,
        Null
    }

    public enum UDFFieldTypeEnum
    {
        Optional = 0,
        Mandatory = 1,
        ReadOnly = 2,
        Public = 3,
        Null
    }

    public enum ActionEnum
    {
        Insert = 1,
        Modify = 2,
        Terminate = 3,
        None = 4,
        Null
    }

    public enum TransactionTypeEnum
    {
        Invoice = 1,
        Payment = 2,
        Null
    }

    public enum CaseTypeCategoryEnum
    {
        Incoming = 1,
        Outgoing = 2,
        Null
    }

    public enum CycleStatusEnum
    {
        Closed = 0,
        Open = 1,
        Null
    }

    public enum ProductDurationTypeEnum
    {
        Months = 1,
        Days = 2,
        Null
    }

    public enum CustomerHierarchyTypeEnum
    {
        All,
        Ascendants,
        Descendants
    } ;

    public enum ActivationStatusEnum
    {
        Inactive = 0,
        Active = 1
    }

    public enum BookPositionCalculatedByDaysEnum
    {
        Quantity = 0,
        Total = 1
    }

    public enum ApplicationEntityEnum
    {
        Customer = 1,
        Address = 2,
        Contact = 3,
        PaymentMethod = 4,
        Contract = 5,
        Service = 6,
        ServiceIdentifier = 7,
        ContractCase = 8,
        ServiceCase = 9,
        ServiceIdentifierCase = 10,
        Event = 11,
        CustomerCase = 12,
        Product = 13,
        Document = 14,
        Metrics = 15,
        Segmentation = 16,
        Offer = 17,
        Case = 18,
        Appointment = 19,
        User = 20,
        Null
    }

    public enum OrderTypeEnum
    {
        NewContractOrder = 1,
        ContractUpdateOrder = 2,
        ContractCancellationOrder = 3,
        ContractReplacement = 4,
        ContractSuspension = 5,
        ContractResuming = 6,
        Null
    } ;

    public enum OrderStatusEnum
    {
        Invalid = 0,
        Valid = 1,
        Failed = 2,
        Refused = 3,
        UnderImplementation = 4,
        Completed = 5,
        PartiallyCompleted = 6,
        Null
    } ;

    public enum ContractStatusEnum
    {
        Inactive = 0,
        Active = 1,
        Suspended = 2,
        Terminated = 3,
        Null
    } ;

    public enum ContractTerminationReasonEnum
    {
        Unknown = 0,
        UserRequest = 1,
        TechnicalReason = 2,
        DueBalance = 3,
        Failed = 4,
        NoPenaltyApplied = 5,
        AlwaysApplyPenalty = 6,
        UpgradeTermination = 7,
        Null
    } ;

    public enum CaseEventStatusEnum
    {
        Opened = 0,
        Closed = 1,
        Expired = 2,
        Null
    } ;

    public enum LastEventStatusEnum
    {
        Open = 0,
        Completed = 1,
        Null
    } ;

    public enum EventStatusEnum
    {
        Open = 0,
        Closed = 1,
        Null
    } ;

    public enum CaseEventResultEnum
    {
        Failure = 0,
        Success = 1,
        Expired = 2
    } ;

    public enum CaseEventActionEnum
    {
        Opening = 0,
        Completion = 2,
        Null
    }

    public enum ScopeEnum
    {
        Customer = 0,
        Order = 1,
        Service = 2,
        Identifier = 3,
        Null
    }

    public enum CaseProcessEnum
    {
        NotProcessed = 0,
        Processed = 1,
        Null
    } ;

    public enum CaseCompletionStatusEnum
    {
        Failure = 0,
        Success = 1,
        Null
    } ;

    public enum CaseLockStatusEnum
    {
        Free = 1,
        Locked = 2,
        Null
    }

    public enum EventTypeTransitionEnum
    {
        NoWFR = 0,
        AsapWFR = 1,
        BatchWFR = 2,
        DemandWFR = 3,
        Null
    }

    public enum EventTypeProcessEnum
    {
        Null = 0,
        User = 1,
        System = 2
    }

    public enum EventActionStatusEnum
    {
        NoActionsOccured = 0,
        OpenActionCompleted = 1,
        MainActionPending = 2,
        MainActionCompleted = 3,
        CloseActionCompleted = 4,
        Null
    }

    public enum CaseActionStatusEnum
    {
        Opening = 1,
        Pending = 2,
        Closing = 3,
        Null
    }

    public enum EventTypeActionTypeEnum
    {
        Main = 1,
        Open = 2,
        Close = 3,
        Null
    }

    public enum ServicePropertyTargetEnum
    {
        ServiceFixed = 0,
        Service = 1,
        ServiceIdentifier = 2,
        Null
    } ;

    public enum ServicePropertyTypeEnum
    {
        User = 0,
        System = 1,
        Null
    } ;

    public enum ServiceIdentifierTypeEnum
    {
        User = 0,
        System = 1,
        Null
    } ;

    public enum CustomerFieldType
    {
        Manual = 1,
        System = 2,
        Null
    } ;

    public enum FunctionTypeEnum
    {
        System = 1,
        MW = 2,
        Null
    }

    public enum FunctionParameterTypeEnum
    {
        Input = 1,
        Return = 2,
        Null
    } ;

    public enum GenderEnum
    {
        Male = 1,
        Female = 2,
        Null
    } ;

    public enum TitleEnum
    {
        Mr = 1,
        Mrs = 2,
        Dr = 3,
        Null
    } ;

    public enum CustomerStatusEnum
    {
        Closed = 0,
        Active = 1,
        Suspended = 2,
        Potential = 3,
        Prospect = 4,
        Null
    } ;

    public enum CustomerStateEnum
    {
        Incomplete = 0,
        Complete = 1,
        Null
    } ;

    public enum CustomerTypeLegalStatusEnum
    {
        Individual = 1,
        LegalEntity = 2,
        Null
    } ;

    public enum ServiceDocumentAssociationLevelEnum
    {
        Product = 1,
        Service = 2,
        ServiceIdentifier = 3,
        Null
    } ;

    public enum ExpressionColumnTypeEnum
    {
        Condition = 1,
        Result = 2,
        Both = 3,
        Null
    } ;

    public enum ApplicationFieldTypeEnum
    {
        Entity = 1,
        Field = 2,
        Collection = 3,
        Null
    } ;

    public enum RuleTypeEnum
    {
        Form = 1,
        Field = 2,
        Null
    } ;

    public enum FieldRuleTargetEnum
    {
        Condition = 1,
        Action = 2,
        Standalone = 3,
        Null
    } ;

    public enum ConditionOperatorEnum
    {
        AND = 1,
        OR = 2
    } ;

    public enum FieldRuleTypeEnum
    {
        Mandatory = 1,
        Empty = 2,
        Equal = 3,
        NotEqual = 4,
        Greater = 5,
        Less = 6,
        GreaterOrEqual = 7,
        LessOrEqual = 8,
        Between = 9,
        StandardLength = 10,
        MinimumLength = 11,
        MaximumLength = 12,
        RegExp = 13,
        InList = 14,
        NotInList = 15,
        GreekChars = 16,
        GreekCharsDigits = 17,
        ExactOccurences = 18,
        MinOccurences = 19,
        MaxOccurences = 20,
        CollectionMustBeSet = 21,
        DatabaseLookup = 22,
        ValidTaxRegNo = 23,
        ValidCreditCardNo = 24,
        SystemFunction = 25,
        Null
    } ;

    public enum SystemUDFFieldRuleTypeEnum
    {
        Mandatory = 1,
        Empty = 2,
        Equal = 3,
        NotEqual = 4,
        Greater = 5,
        Less = 6,
        GreaterOrEqual = 7,
        LessOrEqual = 8,
        Between = 9,
        InList = 14,
        NotInList = 15,
        Null
    };

    public enum ValidationRuleSeverityEnum
    {
        Warning = 1,
        Error = 2,
        Null
    } ;

    public enum DealerDirectionEnum
    {
        Any = 0,
        Direct = 1,
        Indirect = 2,
        Null
    } ;

    [Flags]
    public enum VisibilityCustomerUDFFlagsEnum
    {
        Null = 0,
        Validation = 1,
        Segmentation = 2,
        Metrics = 4
    }

    [Flags]
    public enum VisibilityContractUDFFlagsEnum
    {
        Null = 0,
        Validation = 1,
        Segmentation = 2,
        Metrics = 4
    }

    [Flags]
    public enum FieldRuleTypeFlagsEnum
    {
        Null = 0,
        Date = 1,
        Numeric = 2,
        Status = 4,
        Text = 8,
        List = 16,
        NoValue = 32,
        OneValue = 64,
        RangeValues = 128,
        ListValues = 256,
        OccurValues = 512,
        LengthValue = 1024
    }

    public enum WorkflowRuleTypeEnum
    {
        Entry = 1,
        Transition = 2,
        ExitWithSuccess = 3,
        ExitWithFailure = 4
    }

    public enum WorkflowDiagramVersionTypeEnum
    {
        Design = 0,
        Production = 1,
        Null
    }

    public enum WorkflowDiagramVersionActualTypeEnum
    {
        Design = 0,
        ActiveInFuture = 1,
        Active = 2, // WorkflowDiagramVersionTypeEnum.Production && <current_date> between date_from and date_to
        ActiveInPast = 3, // WorkflowDiagramVersionTypeEnum.Production && <current_date> greater than date_to
        Null
    }

    public enum ProductStatusEnum
    {
        Design = 0,
        Active = 1,
        Inactive = 2,
        Null
    }

    public enum ValidationGroupEnum
    {
        CustomerValidation = 1,
        ContractValidation = 2,
        WorkflowOrderScope = 3,
        WorkflowServiceScope = 4,
        WorkflowIdentifierScope = 5,
        WorkflowCustomerScope = 6,
        MetricsScope = 7,
        SegmentationScope = 8,
        Null
    }

    public enum ValidationGroupObjectTypeEnum
    {
        ApplicationEntity = 1,
        ValidationRule = 2,
        Null
    }

    public enum SystemFunctionReturnValueEnum
    {
        Success = 1,
        Failure = 2,
        NoResult = 3,
        Null
    }

    public enum LogMessageTypeEnum
    {
        Debug = 0,
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public enum SentToMiddlewareStatusEnum
    {
        NotSent = 0,
        Sent = 1,
        SendPending = 2,
        Null
    }

    public enum ReportComTypeEnum
    {
        Detailed = 0,
        Summary = 1
    }

    public enum ReportComGroupByEnum
    {
        Customer = 0,
        Dealer = 1
    }

    public enum ReportComChoiceEnum
    {
        All = 0,
        Selection = 1
    }

    public enum DealerOrderTypeEnum
    {
        Regular = 0,
        CSN = 1
    }

    public enum ReportGroupingTypeEnum
    {
        Customers = 0,
        Categories = 1,
        Null
    }

    public enum CaseTypeProcessTypeEnum
    {
        Client = 1,
        Server = 2,
        Null
    }

    public enum RecordSortingEnum
    {
        CustomerCode,
        AccountName,
        Balance,
        BalanceAge,
        Null
    }

    public enum CustomerTypeEnum
    {
        Managed = 1,
        Unmanaged = 2,
        Null
    }

    public enum ReportStyleEnum
    {
        Detailed = 1,
        Summary = 2,
        Null
    }

    public enum AppointmentTypeEnum
    {
        None = 0,
        Appointment = 1,
        Reschedule = 2
    }

    public enum AppointmentLabelEnum
    {
        Pending = 0,
        Cancelled = 1,
        Rescheduled = 2,
        Completed = 3,
        Personal = 4
    }

    public enum AccountNameSearchTypeEnum
    {
        ExactMatch,
        LeftMatch,
        RightMatch,
        BothMatch
    }

    public enum UdfGroupTypeEnum
    {
        Null = 0,
        ClientView = 1,
        AppFields
    }

    public enum UdfGroupSubTypeEnum
    {
        Null = 0,
        CustManagMain = 1,
        CTINotification = 2,
        CustManagFinancial = 3,
        CustManagCasesEvents = 4,
        AllTasks = 5,
        LockedTasks = 6,
        EditEvent = 7,
        SearchCustomer = 8,
        CTIEditEvent = 9
    }

    public enum SegmentationTypeEnum
    {
        Null = 0,
        Customer = 1,
        Contract = 2,
        Case = 3,
        Event = 4
    }

    public enum ReportViewTypeEnum
    {
        Data = 1,
        Chart = 2
    }

    public enum OwnerTypeEnum
    {
        Admin = 1,
        Client = 2
    };

    public enum ReportFormModuleTypeEnum
    {
        Admin = 0,
        WinClient = 1,
        WebClient = 2,
        SilverLightClient = 3
    };

    public enum UDFInternalLinkTypeEnum
    {
        Customer = 1,
        Case = 2,
        Event = 3,
        Contract = 4,
        Property = 5,
        PaymentMethod = 6,
        User = 7
    }

    public enum UDFRefTypeEnum
    {
        Null = 0,
        CustomerType = 1,
        CaseType = 2,
        EventType = 3,
        PaymentMethod = 4,
        AddressType = 5,
        ContactType = 6,
        DocumentType = 7,
        UserType = 8,
        ContractType = 9,
        ServiceType = 10,
        IdentifierType = 11,
        InvoiceType = 12,
        PaymentType = 13,
        ProductType = 14,
        ProductTypeType = 15,
        ServiceTypeType = 16,
        IdentifierTypeType = 17,
        ChargeTypeType = 18,
        ProductPricelist = 19
    }

    public enum AppointmentRefTypeEnum
    {
        Null = 0,
        Event = 1,
        Case = 2
    }

    public enum ValidationRuleRefTypeEnum
    {
        Customer = 1,
        Contract = 2,
        Case = 3,
        Event = 4,
        Job = 5,
        Null
    }

    public enum UDFTypeEnum
    {
        Null = 0,
        Date = 1,
        Numeric = 2,
        ShortText = 3,
        LongText = 4,
        Status = 5,
        ExternalLink = 7,
        InternalLink = 8,
        Image = 9,
        File = 10
    }

    public enum UDFAccessTypeEnum
    {
        Editable = 1,
        ReadOnly = 2,
        Internal = 3
    }

    public enum UDFListTypeEnum
    {
        Null = 0,
        Predefined = 1,
        Runtime = 2
    }

    public enum UDFDefaultTypeEnum
    {
        Null = 0,
        Predefined = 1,
        Runtime = 2,
        EvaluatedText = 3
    }

    public enum UDFMultipleTypeEnum
    {
        Null = 0,
        Sequential = 1,
        Flat = 2
    }

    public enum ExternalLinkUDFSubTypeEnum
    {
        URL = 1,
        Image = 2,
        File = 3,
        Map = 4
    }

    public enum InternalLinkUDFSubTypeEnum
    {
        Customer = 1,
        Case = 2,
        Event = 3,
        Contract = 4,
        User = 5,
        Contact = 6,
        ContactPhone = 7,
        ServiceIdentifierValue = 8
    }

    public enum ImageUDFSubTypeEnum
    {
        JPG = 1,
        PNG = 2,
        BMP = 3,
        GIF = 4,
        TIFF = 5
    }

    public enum FileUDFSubTypeEnum
    {
        ZIP = 1,
        DOC = 2,
        XLS = 3,
        RAR = 4,
        PDF = 5
    }

    public enum LongTextUDFSubTypeEnum
    {
        TXT = 1,
        RTF = 2,
        DOCX = 3,
        HTML = 4
    }

    public enum ShortTextUDFSubTypeEnum
    {
        PhoneNumberLandline = 1,
        PhoneNumberMobile = 2,
        Email = 3,
        GreekName = 4,
        MixedName = 5,
        Code = 6,
        CreditCard = 7,
        MixedNameForSearch = 8,
        AddressNumber = 9,
        ContactPhoneForSearch = 10,
        ContactNameForSearch = 11,
        MixedNameEnriched = 12,
        FreeText = 13,
        IdCardNo = 14,
        PostalCode = 15
    }

    public enum ExpressionTypeEnum
    {
        Customer = 1,
        Contract = 2,
        Case = 3,
        Event = 4
    }

    public enum ConditionTypeEnum
    {
        If = 1,
        Then = 2
    }

    public enum TypeOrListOfTypeEnum
    {
        Null = 0,
        Type = 1,
        ListOfType = 2,
        Both = 3
    }

    [Flags]
    public enum AppFieldParameterTypeEnum
    {
        Null = 0,
        ApplicationField = 1,
        LiteralValue = 2,
        Function = 4,
        ApplicationFieldOrLiteral = ApplicationField | LiteralValue,
        ApplicationFieldOrFunction = ApplicationField | Function,
        ApplicationFieldOrFunctionOrLiteral = ApplicationField | Function | LiteralValue
    }

    public enum EntityActionEnum
    {
        Insert,
        Edit
    }

    public enum IVRNodeTypeEnum
    {
        Prompt = 1,
        Skill = 2,
        Action = 3,
        PromptForString
    }

    public enum IVRActionTypeEnum
    {
        Enqueue = 1,
        RecordFile = 2,
        Hangup = 3,
        ReceiveFax = 4,
        LeaveVoicemail = 5,
        RedirectToNumber = 6,
        PlayMessage = 7,
        StartSubscriberIvr = 8,
        PromptForString = 9,
        PlayMessageAndGoToNode = 10,
        Null = 11
    }

    public enum TextAppendSeparator
    {
        NoSeparator = 1,
        Comma = 2,
        Pipe = 3,
        CRLF = 4
    }

    public enum CBDPRangesReferToTypeEnum
    {
        Null = 0,
        ContractsWithSameDetails = 1,
        ContractsWithSameStatus = 2,
        ContractsWithSameStatusPlusChildren = 3,
        TotalContracts = 4,
        TotalContractsPlusChildren = 5,
        ContractsWithSameDetailsPlusChildren = 6

    }
    
    
    public enum CalculationRangeTypeEnum
    {
        CurrentDatePeriodCalculation = 1,
        InitialContractInsertionDate = 2,
        Null
    }

    public enum ContractFieldTypeEnum
    {
        Date = 1,
        Numeric = 2,
        ShortText = 3,
        LongText = 4,
        Status = 5,
        List = 6,
        XML = 7,
        Null
    }

    public enum DealerHierarchyCalculationTypeEnum
    {
        OldDealer = 1,
        NewDealer = 2,
        NoneDealers = 3,
        Null
    };

    public enum UpgradeDealerCommissionProgramTypeEnum
    {
        AirtimeCommissions = 1,
        CBDPCommissions = 2,
        Null
    } ;

    public enum UpgradeDealerCalculationTargetEnum
    {
        OldDealer = 1,
        NewDealer = 2,
        NoneDealers = 3,
        Null
    } ;

    public enum UpgradeDealerGiveCommissionToEnum
    {
        Current = 1,
        Root = 2,
        Null
    } ;

    public enum UpgradeDealerCalculationItemEnum
    {
        CBDPActivationBonus = 1,
        CBDPTargetBonus = 2,
        CBDPCancellation = 3,
        AirtimeBonus = 4,
        Null
    } ;

    public enum UpgradeDealerTransitionTypeEnum
    {
        TransitionBwithParentAToC = 1,
        TransitionAandBUnderC = 2,
        TransitionAB = 3,
        TransitionABC = 4,
        Null
    } ;

    #region Aegean
    public enum AegeanContractStatus
    {
        Submitted = 1,
        Rejected = 2, 
        Open = 3,
        Cancelled = 4,
        Active = 5,
        Terminated = 6,
        InCancellation = 7,
        Null
    }
    #endregion

    #region MC
    public enum MCStatusEnum
    {
        Running = 1,
        Completed = 2,
        Failed = 3,
        Null
    }

    #endregion
}