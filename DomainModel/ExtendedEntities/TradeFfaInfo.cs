﻿using System;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class TradeFfaInfo
    {
        [DataMember]
        public string VesselName { get; set; }

        [DataMember]
        public decimal TotalDaysOfTrade
        {
            get
            {
                return QuantityDays - ClosedOutQuantityTotal;
            }
            internal set {; }
        }
    }
}