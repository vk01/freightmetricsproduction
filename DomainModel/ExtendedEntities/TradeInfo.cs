﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class TradeInfo : DomainObject
    {
        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public decimal Ownership { get; set; }

        [DataMember]
        public decimal SubType { get; set; }

        [DataMember]
        public string CounterpartyName { get; set; }

        [DataMember]
        public string TraderName { get; set; }

        [DataMember]
        public string MarketName { get; set; }

        [DataMember]
        public int MarketTonnes { get; set; }

        [DataMember]
        public string BrokersName { get; set; }

        [DataMember]
        public string MTMFwdIndexName { get; set; }

        [DataMember]
        public TradeTcInfo TCInfo { get; set; }

        [DataMember]
        public TradeFfaInfo FFAInfo { get; set; }

        [DataMember]
        public TradeCargoInfo CargoInfo { get; set; }

        [DataMember]
        public TradeOptionInfo OptionInfo { get; set; }

        [DataMember]
        public List<TradeBrokerInfo> TradeBrokerInfos { get; set; }

        [DataMember]
        public List<TradeInfoBook> TradeInfoBooks { get; set; }

    }
}