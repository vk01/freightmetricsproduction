﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class VesselPoolInfo
    {
        [DataMember]
        public List<Vessel> Vessels { get; set; }

        public string VesselsString
        {
            get { return (Vessels.Count == 0) ? null : Vessels.Select(a => a.Name).Aggregate((i, j) => i + "," + j); }
        }
    }
}
