﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class Route
    {
        [DataMember]
        public Index Index { get; set; }

        [DataMember]
        public List<Region> Regions { get; set; }
    }
}