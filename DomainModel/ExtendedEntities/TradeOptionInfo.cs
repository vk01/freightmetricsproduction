﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class TradeOptionInfo
    {
        [DataMember]
        public string VesselName { get; set; }
    }
}
