﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class Trade : DomainObject
    {
        [DataMember]
        public TradeInfo Info { get; set; }

        [DataMember]
        public List<TradeInfo> Infos { get; set; }

        public Trade Clone()
        {
            TradeFfaInfo newFFAInfo = null;
            TradeCargoInfo newCargoInfo = null;
            TradeTcInfo newTCInfo = null;
            TradeOptionInfo newOptionInfo = null;

            var newTrade = new Trade()
                               {
                                   Chd = _Chd,
                                   Chuser = _Chuser,
                                   Crd = _Crd,
                                   Cruser = _Cruser,
                                   Id = _Id,
                                   State = _State,
                                   Status = _Status,
                                   Type = _Type
                               };

            var newTradeInfo = new TradeInfo()
                                   {
                                       Chd = Info.Chd,
                                       Chuser = Info.Chuser,
                                       Code = Info.Code,
                                       Comments = Info.Comments,
                                       CompanyId = Info.CompanyId,
                                       CounterpartyId = Info.CounterpartyId,
                                       Crd = Info.Crd,
                                       Cruser = Info.Cruser,
                                       DateFrom = Info.DateFrom,
                                       DateTo = Info.DateTo,
                                       Direction = Info.Direction,
                                       ExternalCode = Info.ExternalCode,
                                       Id = Info.Id,
                                       IsJointVenture = Info.IsJointVenture,
                                       JointVentureShare = Info.JointVentureShare,
                                       MarketId = Info.MarketId,
                                       MarketTonnes = Info.MarketTonnes,
                                       MTMFwdIndexId = Info.MTMFwdIndexId,
                                       MTMStressIndexId = Info.MTMStressIndexId,
                                       PeriodFrom = Info.PeriodFrom,
                                       PeriodTo = Info.PeriodTo,
                                       RegionId = Info.RegionId,
                                       RouteId = Info.RouteId,
                                       SignDate = Info.SignDate,
                                       StrategySet = Info.StrategySet,
                                       TradeId = Info.TradeId,
                                       TraderId = Info.TraderId,
                                       Version = Info.Version,
                                       TradeBrokerInfos = new List<TradeBrokerInfo>(),
                                       TradeInfoBooks = new List<TradeInfoBook>()
                                   };

            foreach (TradeBrokerInfo tradeBrokerInfo in Info.TradeBrokerInfos)
            {
                var newTradeBrokerInfo = new TradeBrokerInfo()
                                             {
                                                 BrokerId = tradeBrokerInfo.BrokerId,
                                                 Commission = tradeBrokerInfo.Commission,
                                                 Id = tradeBrokerInfo.Id,
                                                 TradeInfoId = tradeBrokerInfo.TradeInfoId
                                             };
                newTradeInfo.TradeBrokerInfos.Add(newTradeBrokerInfo);
            }
            foreach (TradeInfoBook tradeInfoBook in Info.TradeInfoBooks)
            {
                var newTradeInfoBook = new TradeInfoBook()
                {
                    BookId = tradeInfoBook.BookId,
                    Id = tradeInfoBook.Id,
                    TradeInfoId = tradeInfoBook.TradeInfoId
                };
                newTradeInfo.TradeInfoBooks.Add(newTradeInfoBook);
            }

            if(Info.FFAInfo != null)
            {
                newFFAInfo = new TradeFfaInfo()
                {
                    AccountId = Info.FFAInfo.AccountId,
                    AllocationType = Info.FFAInfo.AllocationType,
                    BankId = Info.FFAInfo.BankId,
                    ClearingHouseId = Info.FFAInfo.ClearingHouseId,
                    Id = Info.FFAInfo.Id,
                    IndexId = Info.FFAInfo.IndexId,
                    Period = Info.FFAInfo.Period,
                    PeriodDayCountType = Info.FFAInfo.PeriodDayCountType,
                    PeriodType = Info.FFAInfo.PeriodType,
                    Price = Info.FFAInfo.Price,
                    Purpose = Info.FFAInfo.Purpose,
                    QuantityDays = Info.FFAInfo.QuantityDays,
                    QuantityType = Info.FFAInfo.QuantityType,
                    SettlementType = Info.FFAInfo.SettlementType,
                    TradeInfoId = Info.FFAInfo.TradeInfoId,
                    VesselId = Info.FFAInfo.VesselId,
                    VesselPoolId = Info.FFAInfo.VesselPoolId,
                    ClosedOutQuantityTotal = Info.FFAInfo.ClosedOutQuantityTotal
                };
            }

            if (Info.TCInfo != null)
            {
                newTCInfo = new TradeTcInfo()
                {
                    Address = Info.TCInfo.Address,
                    BallastBonus = Info.TCInfo.BallastBonus,
                    Delivery = Info.TCInfo.Delivery,
                    Id = Info.TCInfo.Id,
                    IsBareboat = Info.TCInfo.IsBareboat,
                    Redelivery = Info.TCInfo.Redelivery,
                    TradeInfoId = Info.TCInfo.TradeInfoId,
                    VesselId = Info.TCInfo.VesselId,
                    VesselIndex = Info.TCInfo.VesselIndex,
                    Legs = new List<TradeTcInfoLeg>()
                };

                foreach (TradeTcInfoLeg tradeTcInfoLeg in Info.TCInfo.Legs)
                {
                    var newLeg = new TradeTcInfoLeg()
                    {
                        Id = tradeTcInfoLeg.Id,
                        Identifier = tradeTcInfoLeg.Identifier,
                        IndexId = tradeTcInfoLeg.IndexId,
                        IndexPercentage = tradeTcInfoLeg.IndexPercentage,
                        IsOptional = tradeTcInfoLeg.IsOptional,
                        OptionalStatus = tradeTcInfoLeg.OptionalStatus,
                        PeriodFrom = tradeTcInfoLeg.PeriodFrom,
                        PeriodTo = tradeTcInfoLeg.PeriodTo,
                        Rate = tradeTcInfoLeg.Rate,
                        RateType = tradeTcInfoLeg.RateType,
                        RedeliveryDays = tradeTcInfoLeg.RedeliveryDays,
                        TradeTcInfoId = tradeTcInfoLeg.TradeTcInfoId
                    };
                    newTCInfo.Legs.Add(newLeg);
                }
            }

            if (Info.OptionInfo != null)
            {
                newOptionInfo = new TradeOptionInfo()
                {
                    AccountId = Info.OptionInfo.AccountId,
                    AllocationType = Info.OptionInfo.AllocationType,
                    BankId = Info.OptionInfo.BankId,
                    ClearingHouseId = Info.OptionInfo.ClearingHouseId,
                    Id = Info.OptionInfo.Id,
                    IndexId = Info.OptionInfo.IndexId,
                    Period = Info.OptionInfo.Period,
                    PeriodDayCountType = Info.OptionInfo.PeriodDayCountType,
                    PeriodType = Info.OptionInfo.PeriodType,
                    ClearingFees = Info.OptionInfo.ClearingFees,
                    Premium = Info.OptionInfo.Premium,
                    PremiumDueDate = Info.OptionInfo.PremiumDueDate,
                    Strike = Info.OptionInfo.Strike,
                    Type = Info.OptionInfo.Type,
                    UserDelta = Info.OptionInfo.UserDelta,
                    UserMtm = Info.OptionInfo.UserMtm,
                    QuantityDays = Info.OptionInfo.QuantityDays,
                    QuantityType = Info.OptionInfo.QuantityType,
                    SettlementType = Info.OptionInfo.SettlementType,
                    TradeInfoId = Info.OptionInfo.TradeInfoId,
                    VesselId = Info.OptionInfo.VesselId,
                    VesselPoolId = Info.OptionInfo.VesselPoolId
                };
            }

            if (Info.CargoInfo != null)
            {
                newCargoInfo = new TradeCargoInfo()
                {
                    Address = Info.CargoInfo.Address,
                    DischargingPort = Info.CargoInfo.DischargingPort,
                    Id = Info.CargoInfo.Id,
                    TradeInfoId = Info.CargoInfo.TradeInfoId,
                    VesselId = Info.CargoInfo.VesselId,
                    VesselIndex = Info.CargoInfo.VesselIndex,
                    LoadingPort = Info.CargoInfo.LoadingPort,
                    Legs = new List<TradeCargoInfoLeg>()
                };

                foreach (TradeCargoInfoLeg tradeCargoInfoLeg in Info.CargoInfo.Legs)
                {
                    var newLeg = new TradeCargoInfoLeg()
                    {
                        Id = tradeCargoInfoLeg.Id,
                        Identifier = tradeCargoInfoLeg.Identifier,
                        IndexId = tradeCargoInfoLeg.IndexId,
                        IndexPercentage = tradeCargoInfoLeg.IndexPercentage,
                        IsOptional = tradeCargoInfoLeg.IsOptional,
                        OptionalStatus = tradeCargoInfoLeg.OptionalStatus,
                        PeriodFrom = tradeCargoInfoLeg.PeriodFrom,
                        PeriodTo = tradeCargoInfoLeg.PeriodTo,
                        Rate = tradeCargoInfoLeg.Rate,
                        RateType = tradeCargoInfoLeg.RateType,
                        Quantity = tradeCargoInfoLeg.Quantity,
                        QuantityVariation = tradeCargoInfoLeg.QuantityVariation,
                        Tce = tradeCargoInfoLeg.Tce,
                        TradeCargoInfoId = tradeCargoInfoLeg.TradeCargoInfoId
                    };
                    newCargoInfo.Legs.Add(newLeg);
                }
            }
            

            newTradeInfo.FFAInfo = newFFAInfo;
            newTradeInfo.TCInfo = newTCInfo;
            newTradeInfo.OptionInfo = newOptionInfo;
            newTradeInfo.CargoInfo = newCargoInfo;
            newTrade.Info = newTradeInfo;

            return newTrade;
        }
    }
}