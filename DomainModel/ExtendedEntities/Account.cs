﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class Account : DomainObject
    {
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
    }
}
