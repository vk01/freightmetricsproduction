﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class ExchangeRate : DomainObject
    {
        [DataMember]
        public string TargetCurrency { get; set; }

        [DataMember]
        public string SourceCurrency { get; set; }
    }
}
