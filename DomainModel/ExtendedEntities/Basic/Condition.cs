using System;
using System.Linq;
using System.Runtime.Serialization;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class Condition: DomainObject
    {
        public DataContext context;

        private Expression _Expression;
        private Condition _LeftCondition;
        private Condition _RightCondition;

        [DataMember]
        public Condition LeftCondition
        {
            get
            {
                if (_LeftCondition == null && context != null && !context.IsDisposed && _LeftConditionId != null)
                {
                    _LeftCondition = Condition_GetCondition(context, LeftConditionId).SingleOrDefault();
                    if (_LeftCondition != null) _LeftCondition.context = context;
                }
                return _LeftCondition;
            }
            set
            {
                _LeftCondition = value;
                if (value != null)
                {
                    LeftConditionId = value.Id;
                }
                else
                {
                    LeftConditionId = null;
                }
            }
        }

        [DataMember]
        public Condition RightCondition
        {
            get
            {
                if (_RightCondition == null && context != null && !context.IsDisposed && _RightConditionId != null)
                {
                    _RightCondition = Condition_GetCondition(context, RightConditionId).SingleOrDefault();
                    if (_RightCondition != null) _RightCondition.context = context;
                }
                return _RightCondition;
            }
            set
            {
                _RightCondition = value;
                if (value != null)
                {
                    RightConditionId = value.Id;
                }
                else
                {
                    RightConditionId = null;
                }
            }
        }

        [DataMember]
        public Expression Expression
        {
            get
            {
                if (_Expression == null && context != null && !context.IsDisposed && _ExprId != null)
                {
                    _Expression = Expression.GetExpression(context, ExprId).SingleOrDefault();
                    if (_Expression != null) _Expression.context = context;
                }
                return _Expression;
            }
            set
            {
                _Expression = value;
                if (value != null)
                {
                    ExprId = value.Id;
                }
                else
                {
                    ExprId = null;
                }
            }
        }

        [DataMember]
        public ConditionOperatorEnum? Operator
        {
            get
            {
                return Operator_Str == null
                           ? null
                           : (ConditionOperatorEnum?) Enum.Parse(typeof (ConditionOperatorEnum), Operator_Str);
            }
            set { Operator_Str = value == null ? null : value.Value.ToString("d"); }
        }

        public override string ToString()
        {
            if (Operator != null)
            {
                return "(" + LeftCondition + " " + Operator.Value.ToString("g") + " " + RightCondition + ")";
            }
            return Expression != null ? Expression.Name : "{undefined}";
        }

        #region Compiled Queries

        public static Func<DataContext, long?, IQueryable<Condition>> Condition_GetCondition =
            CompiledQuery.Compile(
                (DataContext dataContext, long? ConditionId) =>
                dataContext.Conditions.Where(a => a.Id == ConditionId));

        #endregion

        public static void DeleteCondition(Condition condition)
        {
            condition.context.Conditions.DeleteOnSubmit(condition);
            if (!String.IsNullOrEmpty(condition.Operator_Str))
            {
                DeleteCondition(condition.LeftCondition);
                DeleteCondition(condition.RightCondition);
            }
            else if(condition.ExprId != null)
            {
                bool expressionUsed = false;
                // Get all conditions except for current from database
                foreach (Condition exprCondition in condition.context.Conditions.Where(a => a.ExprId == condition.ExprId))
                {
                    // If this condition is not in the delete list then the expression is still used
                    if(condition.context.GetChangeSet().Deletes.OfType<Condition>().SingleOrDefault(a => a.Id == exprCondition.Id) == null)
                    {
                        expressionUsed = true;
                        break;
                    }
                }
                if(!expressionUsed)
                {
                    condition.context.Expressions.DeleteOnSubmit(condition.Expression);
                }
            }
        }

        public static Condition CloneCondition(Condition condition)
        {
            var context = condition.context;
            if (context == null || context.IsDisposed) throw new ApplicationException("Cannot clone Condition. Context is null or disposed.");

            Condition clone;
            if (!String.IsNullOrEmpty(condition.Operator_Str))
            {
                var left = CloneCondition(condition.LeftCondition);
                var right = CloneCondition(condition.RightCondition);
                clone = new Condition
                {
                    Id = context.GetNextId(typeof(Condition)),
                    context = context,
                    LeftConditionId = left.Id,
                    RightConditionId = right.Id,
                    Operator_Str = condition.Operator_Str
                };
            }
            else
            {
                clone = new Condition
                                {
                                    Id = context.GetNextId(typeof (Condition)),
                                    context = context,
                                    _ExprId = condition.ExprId
                                };
            }
            context.Conditions.InsertOnSubmit(clone);
            return clone;
        }
    }
}