﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public class UdfGroupHierarchy
    {
        public long GROUP_ID;
        public long GroupLevel;
        public Udfgroup Udfgroup;

        
        #region Static Methods
        
        public static List<UdfGroupHierarchy> GetUdfGroupHierarchy(DataContext context)
        {
            List<UdfGroupHierarchy> list = new List<UdfGroupHierarchy>();

            string SQL = "SELECT level GroupLevel, t1.UG_ID GROUP_ID, null Udfgroup " +
                              "FROM UDFGROUPS t1 " +
                              "connect by prior t1.ug_id = t1.ug_parent_ug_id " +
                              "START WITH t1.ug_parent_ug_id IS NULL " +
                              "order by level asc ";

            var queryable = context.Query<UdfGroupHierarchy>(SQL);

            var anonList = (from UdfGroupHierarchy hierarchy in queryable
                         from objGroup in context.Udfgroups
                         where hierarchy.GROUP_ID == objGroup.Id
                         select new { hierarchy, objGroup }).ToList();


            foreach (var anon in anonList)
            {
                anon.objGroup.context = context;
                anon.hierarchy.Udfgroup = anon.objGroup;
                list.Add(anon.hierarchy);
            }
            return list;
        }

        #endregion

    }
}
