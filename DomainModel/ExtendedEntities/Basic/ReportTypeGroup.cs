using System;
using System.Collections.Generic;
using System.Linq;
using Devart.Data.Linq;
using Exis.Domain.Attributes;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class ReportTypeGroup:DomainObject
    {
        public DataContext context;

        private ReportTypeGroup _ReportTypeGroup;

        public bool Visible
        {
            get { return Visible_Str == "1" ? true : false; }
            set
            {
                Visible_Str = value ? "1" : "0";
            }
        }

        public OwnerTypeEnum OwnerType
        {
            get { return (OwnerTypeEnum)Enum.Parse(typeof(OwnerTypeEnum), Owner_Str); }
            set
            {
                Owner_Str = value.ToString("d");
            }
        }

        public ReportTypeGroup ParentReportTypeGroup
        {
            get
            {
                if (_ReportTypeGroup == null && ParentReportTypeGroupId != null && context != null && !context.IsDisposed)
                {
                    _ReportTypeGroup = context.ReportTypeGroups.Where(a => a.Id == this.ParentReportTypeGroupId).Single();
                    _ReportTypeGroup.context = context;

                }
                return _ReportTypeGroup;
            }
            set
            {
                _ReportTypeGroup = value;
            }
        }

        public static bool InUse(DataContext context, Int64 ReportTypeGroupsId)
        {
            var temp = context.ReportTypesGroupsAssocs.Where(a => a.RptpgId == ReportTypeGroupsId).ToList();

            if (temp.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static List<ReportTypeGroup> GetAllChildrenByParentId(DataContext context, long id)
        {
            if (id == Int64.MinValue)
            {
               var temp = context.ReportTypeGroups.Where(a => a.ParentReportTypeGroupId == null).OrderBy(a => a.Sort).OrderBy(a => a.Name).
                        ToList();
                foreach (var reportTypeGroup in temp)
                {
                    reportTypeGroup.context = context;
                }

                return temp;

            }
            else
            {
                var temp = context.ReportTypeGroups.Where(a => a.ParentReportTypeGroupId == id).OrderBy(a => a.Sort).OrderBy(a => a.Name).
                        ToList();

               foreach (var reportTypeGroup in temp)
               {
                   reportTypeGroup.context = context;
               }

               return temp;
            }

            
        }

        public static ReportTypeGroup GetRootNodeByOwner(DataContext context, OwnerTypeEnum ownerType)
        {
            var temp =
                context.ReportTypeGroups.Where(a => a.ParentReportTypeGroupId == null && a.Owner_Str == ownerType.ToString("d")).
                    ToList();

            foreach (var reportTypeGroup in temp)
            {
                reportTypeGroup.context = context;
            }

            if (temp.Count > 0)
                return (ReportTypeGroup)temp[0];

            return null;
        }

        public static ReportTypeGroup GetByID(DataContext context, Int64 reportTypeGroupID)
        {
            var temp =
                context.ReportTypeGroups.Where(a => a.Id == reportTypeGroupID).
                    ToList();

            if (temp.Count > 0)
            {
                ((ReportTypeGroup) temp[0]).context = context;
                return (ReportTypeGroup)temp[0];
            }
              

            return null;
        }

        public static List<ReportTypeGroup> GetByReportTypeByOwner(DataContext context, long reportTypeId, OwnerTypeEnum ownerType)
        {
            var temp = (from rt in context.ReportTypes
                        from rtg in context.ReportTypeGroups
                        from rtgassoc in context.ReportTypesGroupsAssocs
                        where rt.Id == rtgassoc.RptpId && rtgassoc.RptpgId == rtg.Id && rt.Id == reportTypeId
                        && rtg.Owner_Str == ownerType.ToString("d")
                        select rtg).OrderBy(a => a.Sort).OrderBy(a => a.Name).ToList();

            foreach (var reportTypeGroup in temp)
            {
                reportTypeGroup.context = context;
            }

            return temp;
        }

        public static List<ReportTypeGroup> GetByReportType(DataContext context, long reportTypeId)
        {
            var temp = (from rt in context.ReportTypes
                        from rtg in context.ReportTypeGroups
                        from rtgassoc in context.ReportTypesGroupsAssocs
                        where rt.Id == rtgassoc.RptpId && rtgassoc.RptpgId == rtg.Id && rt.Id == reportTypeId
                        select rtg).OrderBy(a => a.Sort).OrderBy(a => a.Name).ToList();

            foreach (var reportTypeGroup in temp)
            {
                reportTypeGroup.context = context;
            }

            return temp;
        }
    }
}