﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class UdfsListValue:DomainObject
    {
        public DataContext context;
        private Udf _Udf;
        private object _TypedValue;

        [DataMember]
        public Udf Udf
        {
            get
            {
                if (_Udf == null && context != null && !context.IsDisposed)
                {
                    _Udf = (from objUdf in context.Udfs
                            where objUdf.Id == UdfId
                            select objUdf).Single();

                    _Udf.context = context;
                }
                return _Udf;
            }
            set
            {
                _Udf = value;
            }
        }

        [DataMember]
        public object TypedValue
        {
            get
            {
                if (_TypedValue == null && context != null && !context.IsDisposed)
                {
                    _TypedValue = _Udf.GetTypedValue(this.Value);
                }
                return _TypedValue;
            }
            set
            {
                _TypedValue = value;
            }
        }

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum)Enum.Parse(typeof(ActivationStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        public override string ToString()
        {
            return _Value;
        }
    }
}
