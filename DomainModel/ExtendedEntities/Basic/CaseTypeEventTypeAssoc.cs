﻿using System.Linq;

namespace Exis.Domain
{
    public partial class CaseTypeEventTypeAssoc : DomainObject
    {
        public DataContext context;

        private CaseType _CaseType;
        private EventType _EventType;

        public CaseType CaseType
        {
            get
            {
                if(_CaseType == null && context != null && !context.IsDisposed)
                {
                    _CaseType = context.CaseTypes.Single(a => a.Id == CaseTypeId);
                    _CaseType.context = context;
                }
                return _CaseType;
            }
            set
            {
                _CaseType = value;
                if(value != null)
                {
                    CaseTypeId = value.Id;
                }
            }
        }

        public EventType EventType
        {
            get
            {
                if(_EventType == null && context != null && !context.IsDisposed)
                {
                    _EventType = context.EventTypes.Single(a => a.Id == EventTypeId);
                    _EventType.context = context;
                }
                return _EventType;
            }
            set
            {
                _EventType = value;
                if(value != null)
                {
                    EventTypeId = value.Id;
                }
            }
        }
    }
}
