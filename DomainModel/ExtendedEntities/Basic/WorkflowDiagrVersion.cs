﻿using System;
using System.Collections.Generic;
using System.Linq;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class WorkflowDiagrVersion : DomainObject
    {
        public DataContext context;
        private WorkflowDiagram _WorkflowDiagram;

        public WorkflowDiagram WorkflowDiagram
        {
            get
            {
                if(_WorkflowDiagram == null && context != null && !context.IsDisposed)
                {
                    _WorkflowDiagram = context.WorkflowDiagrams.Where(a => a.Id == WorkflowDiagramId).Single();
                    _WorkflowDiagram.context = context;
                }
                return _WorkflowDiagram;
            }
            set
            {
                _WorkflowDiagram = value;
            }
        }

        public WorkflowDiagramVersionTypeEnum Status
        {
            get
            {
                return (WorkflowDiagramVersionTypeEnum) Enum.Parse(typeof (WorkflowDiagramVersionTypeEnum), _Status_Str);
            }
            set
            {
                _Status_Str = value.ToString("d");
            }
        }

        public WorkflowDiagramVersionActualTypeEnum ActualStatus
        {
            get
            {
                if (context != null && !context.IsDisposed)
                {
                    // Determining the actual status, according to the dates
                    if (Status == WorkflowDiagramVersionTypeEnum.Production &&
                        DateFrom <= DateTime.Now &&
                        (DateTo == null || DateTo > DateTime.Now))
                        return WorkflowDiagramVersionActualTypeEnum.Active;
                    if (Status == WorkflowDiagramVersionTypeEnum.Production &&
                        DateFrom > DateTime.Now &&
                        (DateTo == null || DateTo > DateTime.Now))
                        return WorkflowDiagramVersionActualTypeEnum.ActiveInFuture;
                    if ((Status == WorkflowDiagramVersionTypeEnum.Production) &&
                        (DateTo <= DateTime.Now))
                        return WorkflowDiagramVersionActualTypeEnum.ActiveInPast;
                    if (Status == WorkflowDiagramVersionTypeEnum.Design)
                        return WorkflowDiagramVersionActualTypeEnum.Design;
                }
                return WorkflowDiagramVersionActualTypeEnum.Null;
            }
        }

        public WorkflowDiagrVersion Clone(WorkflowDiagram workflowDiagram)
        {
            if(context == null || context.IsDisposed) throw new ApplicationException("Cannot clone Workflow Diagram Version. Context is null or disposed.");
            WorkflowDiagrVersion clone = new WorkflowDiagrVersion
                                             {
                                                 Id = context.GetNextId(typeof (WorkflowDiagrVersion)),
                                                 WorkflowDiagramId = workflowDiagram.Id,
                                                 Cruser = _Cruser,
                                                 Crd = DateTime.Now,
                                                 Chuser = _Chuser,
                                                 Chd = DateTime.Now,
                                                 DateFrom = DateTime.Now,
                                                 DateTo = null,
                                                 Description = "Copy of " + _Description,
                                                 Status_Str = WorkflowDiagramVersionTypeEnum.Design.ToString("d"),
                                                 Version = 0,
                                                 context = context
                                             };
            context.WorkflowDiagrVersions.InsertOnSubmit(clone);

            Dictionary<long, long> nodeMapping = new Dictionary<long, long>();
            foreach (WorkflowDiagramNode workflowDiagramNode in context.WorkflowDiagramNodes.Where(a => a.WorkflowDiagramVersionId == _Id))
            {
                var newNode = new WorkflowDiagramNode
                                  {
                                      Id = context.GetNextId(typeof (WorkflowDiagramNode)),
                                      WorkflowDiagramVersionId = clone.Id,
                                      EventTypeId = workflowDiagramNode.EventTypeId,
                                      Name = workflowDiagramNode.Name
                                  };
                nodeMapping.Add(workflowDiagramNode.Id, newNode.Id);
                context.WorkflowDiagramNodes.InsertOnSubmit(newNode);
            }

            foreach (WorkflowRule workflowRule in context.WorkflowRules.Where(a => a.WorkflowDiagramVersionId == _Id))
            {
                workflowRule.context = context;
                var newCondition = Condition.CloneCondition(workflowRule.Condition);
                var newRule = new WorkflowRule
                                  {
                                      Id = context.GetNextId(typeof (WorkflowRule)),
                                      WorkflowDiagramVersionId = clone.Id,
                                      Cruser = workflowRule.Cruser,
                                      Crd = DateTime.Now,
                                      Chuser = workflowRule.Chuser,
                                      Chd = DateTime.Now,
                                      ConditionId = newCondition.Id,
                                      Delay = workflowRule.Delay,
                                      Description = workflowRule.Description,
                                      Name = workflowRule.Name,
                                      Notes = workflowRule.Notes,
                                      Type_Str = workflowRule.Type_Str,
                                      Priority = workflowRule.Priority,
                                      WorkflowDiagramNodeId = 
                                          workflowRule.WorkflowDiagramNodeId != null
                                              ? nodeMapping[workflowRule.WorkflowDiagramNodeId.Value]
                                              : (long?) null,
                                      TargetWorkflowDiagramNodeId = 
                                          workflowRule.TargetWorkflowDiagramNodeId != null
                                              ? nodeMapping[workflowRule.TargetWorkflowDiagramNodeId.Value]
                                              : (long?) null
                                  };
                context.WorkflowRules.InsertOnSubmit(newRule);
            }
            return clone;
        }

        #region Compiled Queries

        public static Func<DataContext, long, DateTime, IQueryable<WorkflowDiagrVersion>>
            ClientsService_GetLatestActiveWorkflowDiagramVersion =
                CompiledQuery.Compile(
                    (DataContext dataContext, long workflowDiagramId, DateTime dateTimeNow) =>
                    from objWorkflowDiagramVersion in dataContext.WorkflowDiagrVersions
                    where objWorkflowDiagramVersion.WorkflowDiagramId == workflowDiagramId &&
                          objWorkflowDiagramVersion.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objWorkflowDiagramVersion.DateFrom <= dateTimeNow &&
                          (objWorkflowDiagramVersion.DateTo == null || objWorkflowDiagramVersion.DateTo >= dateTimeNow)
                    select objWorkflowDiagramVersion);

        #endregion
    }
}
