﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class ReportType:DomainObject
    {
        public DataContext context;
        private string _FormArgs;
        private string _FormName;
        private ReportTypeForm _reportTypeForm;

        [DataMember]
        public string FormName
        {
            get { return _FormName; }
            set { _FormName = value; }
        }

        [DataMember]
        public string FormArgs
        {
            get { return _FormArgs; }
            set { _FormArgs = value; }
        }

        public override string ToString()
        {
            return this.Name;
        }

        [DataMember]
        public ReportTypeForm ReportTypeForm
        {
            get
            {
                if(_reportTypeForm == null && context!=null && !context.IsDisposed)
                {
                    _reportTypeForm = (from objReportTypeForm in context.ReportTypeForms
                                       from objReportType in context.ReportTypes
                                       where
                                           objReportTypeForm.ReportTypeId == objReportType.Id &&
                                           objReportType.Id == Id
                                       select objReportTypeForm).FirstOrDefault();
                    if (_reportTypeForm != null) _reportTypeForm.context = context;
                }
                return _reportTypeForm;
            }
            set
            {
                _reportTypeForm = value;
            }
        }

        public ReportTemplateTypeEnum ReportTemplateType
        {
            get
            {
                return ReportTemplateTypeEnum.REPX;//((ReportTemplateTypeEnum)Enum.Parse(typeof(ReportTemplateTypeEnum), RTT))
            }
        }

        public static List<ReportType> GetAllByGroupId(DataContext context, long GroupId)
        {
            var temp = (from rt in context.ReportTypes
                        from rtg in context.ReportTypeGroups
                        from rtgassoc in context.ReportTypesGroupsAssocs
                        where rt.Id == rtgassoc.RptpId && rtgassoc.RptpgId == rtg.Id && rtg.Id==GroupId
                        select rt).OrderBy(a => a.Sort).OrderBy(a => a.Name).ToList();

            foreach (var reportType in temp)
            {
                reportType.context = context;
            }
           

            return temp;
        }

        public static ReportType GetByID(DataContext context, Int64 reportTypeID)
        {
            var temp =
                context.ReportTypes.Where(a => a.Id == reportTypeID).
                    ToList();

            if (temp.Count > 0)
                return (ReportType)temp[0];

            return null;
        }

        
    }
}