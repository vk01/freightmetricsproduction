﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exis.Domain
{
    public partial class UsersAppointment:DomainObject
    {
        public DataContext context;
        private User _User;
        private Appointment _Appointment;

        public User User
        {
            get
            {
                if(_User == null && context != null && !context.IsDisposed)
                {
                    _User = context.Users.Single(a => a.Id == UserId);
                }
                return _User;
            }
            set
            {
                _User = value;
                if(value != null)
                {
                    UserId = value.Id;
                }
            }
        }

        public Appointment Appointment
        {
            get
            {
                if (_Appointment == null && context != null && !context.IsDisposed)
                {
                    _Appointment = context.Appointments.Single(a => a.Id == AppointmentId);
                    _Appointment.context = context;
                }
                return _Appointment;
            }
            set
            {
                _Appointment = value;
                if(value != null)
                {
                    AppointmentId = value.Id;
                }
            }
        }
    }
}
