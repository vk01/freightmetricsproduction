﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    [KnownType(typeof (List<string>))]
    [KnownType(typeof (DateTime))]
    [KnownType(typeof (List<DateTime>))]
    [KnownType(typeof (decimal))]
    [KnownType(typeof (List<decimal>))]
    [KnownType(typeof (byte[]))]
    [KnownType(typeof (List<byte[]>))]
    [KnownType(typeof (Case))]
    [KnownType(typeof (List<Case>))]
    [KnownType(typeof (Event))]
    [KnownType(typeof (List<Event>))]
    public partial class UdfsDefaultValue:DomainObject
    {
        public DataContext context;
        private object _TypedValue;
        private Udf _Udf;

        [DataMember]
        public Udf Udf
        {
            get
            {
                if (_Udf == null && context != null && !context.IsDisposed)
                {
                    _Udf = (from objUdf in context.Udfs
                            where objUdf.Id == UdfId
                            select objUdf).Single();

                    _Udf.context = context;
                }
                return _Udf;
            }
            set { _Udf = value; }
        }

        [DataMember]
        public object TypedValue
        {
            get
            {
                if (_TypedValue == null && context != null && !context.IsDisposed)
                {
                    _TypedValue = _Udf.GetTypedValue(Value);
                }
                return _TypedValue;
            }
            set { _TypedValue = value; }
        }
    }
}