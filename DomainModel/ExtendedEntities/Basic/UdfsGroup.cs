﻿using System.Linq;

namespace Exis.Domain
{
    public partial class UdfsGroup:DomainObject
    {
        public DataContext context;
        private Udf _Udf;
        private Udfgroup _Udfgroup;

        public Udf Udf
        {
            get
            {
                if(_Udf == null && context != null && !context.IsDisposed)
                {
                    _Udf = context.Udfs.Single(a => a.Id == _UdfId);
                    _Udf.context = context;
                }
                return _Udf;
            }
            set
            {
                _Udf = value;
                if(value != null)
                {
                    UdfId = value.Id;
                }
            }
        }

        public Udfgroup Udfgroup
        {
            get
            {
                if(_Udfgroup == null && context != null && !context.IsDisposed)
                {
                    _Udfgroup = context.Udfgroups.Single(a => a.Id == UdfGroupId);
                    _Udfgroup.context = context;
                }
                return _Udfgroup;
            }
            set
            {
                _Udfgroup = value;
                if(value != null)
                {
                    UdfGroupId = value.Id;
                }
            }
        }
    }
}
