﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class AppParameter : DomainObject
    {
        public DataContext context;

        #region Compiled Queries

        public static Func<DataContext, IQueryable<AppParameter>>
            GetAllAppParameters =
                CompiledQuery.Compile(
                    (DataContext dataContext) =>
                    from objAppParameetr in dataContext.AppParameters
                    select objAppParameetr
                    );

        #endregion

        #region Static Methods

        public static AppParameter GetAppParameter(DataContext context, string key)
        {
            var data = context.AppParameters.Where(a => a.Code == key);

            return data.FirstOrDefault();
        }

        #endregion
    }
}
