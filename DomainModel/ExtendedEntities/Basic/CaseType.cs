﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class CaseType : DomainObject
    {
        public DataContext context;

        private List<Udf> _Udfs;
        private List<EventType> _associatedEventTypes;

        public ScopeEnum Scope
        {
            get
            {
                return (ScopeEnum)Enum.Parse(typeof(ScopeEnum), Scope_Str);
            }
            set
            {
                Scope_Str = value.ToString("d");
            }
        }

        public CaseEventPriorityEnum Priority
        {
            get { return (CaseEventPriorityEnum)Enum.Parse(typeof(CaseEventPriorityEnum), Priority_Str); }
            set { Priority_Str = value.ToString("d"); }
        }

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum)Enum.Parse(typeof(ActivationStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        [DataMember]
        public List<Udf> Udfs
        {
            get
            {
                if (_Udfs == null && context != null && !context.IsDisposed)
                {
                    _Udfs =
                        context.Udfs.Where(
                            a =>
                            a.RefType_Str == UDFRefTypeEnum.CaseType.ToString("d") &&
                            (a.RefTypeId == Id || a.RefTypeId == null)).
                            ToList();
                    foreach (Udf udf in _Udfs)
                    {
                        udf.context = context;
                    }
                }
                return _Udfs;
            }
            set { _Udfs = value; }
        }

        [DataMember]
        public List<EventType> AssociatedEventTypes
        {
            get
            {
                if (_associatedEventTypes == null && context != null && !context.IsDisposed)
                {
                    _associatedEventTypes = (from objAssoc in context.CaseTypeEventTypeAssocs
                                             from objEventType in context.EventTypes
                                             where objAssoc.CaseTypeId == this.Id &&
                                                   objAssoc.EventTypeId == objEventType.Id
                                             select objEventType).ToList();

                    foreach(EventType ev in _associatedEventTypes)
                    {
                        ev.context = context;
                    }
                }
                return _associatedEventTypes;
            }
            set
            {
                _associatedEventTypes = value;
            }
        }

        public override string ToString()
        {
            return _Description;
        }
    }
}
