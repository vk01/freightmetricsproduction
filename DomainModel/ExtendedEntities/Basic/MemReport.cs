﻿using System;
using System.Collections.Generic;
using Exis.Domain;

namespace Exis.Domain
{
    public class MemReport
    {
        private string m_Title;
        private ReportType m_Type;
        private List<ReportType> m_Types;
        private string m_User;
        private DateTime m_Date;
        private byte[] m_Data;
        private Int64 m_ReportGroupId;
        private ReportFormatEnum m_Format;
        private string m_FileSystemTempPath;
        private long m_Id; // It is the ID of the memory object

        public MemReport(string Title, ReportType Type, string User, DateTime Date, byte[] Data,
                      ReportFormatEnum Format)
        {
            m_Id++;
            m_Title = Title;
            m_Type = Type;
            m_User = User;
            m_Date = Date;
            m_Data = Data;
            m_Format = Format;
        }

        public MemReport(string Title, ReportType Type, Int64 ReportGroupId, string User, DateTime Date, byte[] Data,
                      ReportFormatEnum Format)
        {
            m_Id++;
            m_Title = Title;
            m_Type = Type;
            m_ReportGroupId = ReportGroupId;
            m_User = User;
            m_Date = Date;
            m_Data = Data;
            m_Format = Format;
        }

        public MemReport(string Title, List<ReportType> Types, string User, DateTime Date, byte[] Data,
                      ReportFormatEnum Format)
        {
            m_Id++;
            m_Title = Title;
            m_Types = Types;
            m_User = User;
            m_Date = Date;
            m_Data = Data;
            m_Format = Format;
        }

        public MemReport(string Title, List<ReportType> Types, Int64 ReportGroupId, string User, DateTime Date, byte[] Data,
                      ReportFormatEnum Format)
        {
            m_Id++;
            m_Title = Title;
            m_Types = Types;
            m_ReportGroupId = ReportGroupId;
            m_User = User;
            m_Date = Date;
            m_Data = Data;
            m_Format = Format;
        }

        public long ID
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }

        public string Title
        {
            get { return m_Title; }
        }

        public ReportType Type
        {
            get { return m_Type; }
        }

        public List<ReportType> Types
        {
            get { return m_Types; }
        }

        public Int64 ReportGroupId
        {
            get
            {
                return m_ReportGroupId;
            }
            set
            {
                m_ReportGroupId = value;
            }
        }

        public string User
        {
            get { return m_User; }
        }

        public DateTime Date
        {
            get { return m_Date; }
        }

        public byte[] Data
        {
            get { return m_Data; }
        }

        public ReportFormatEnum Format
        {
            get { return m_Format; }
        }

        public string FileSystemTempPath
        {
            get { return m_FileSystemTempPath; }
            set { m_FileSystemTempPath = value; }
        }
    }
}