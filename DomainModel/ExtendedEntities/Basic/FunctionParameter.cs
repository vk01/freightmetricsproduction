﻿using System;
using System.Linq;

namespace Exis.Domain
{
    public partial class FunctionParameter : DomainObject
    {
        public DataContext context;
        private Function _Function;

        public FunctionParameterTypeEnum Type
        {
            get { return (FunctionParameterTypeEnum) Enum.Parse(typeof (FunctionParameterTypeEnum), Type_Str); }
            set { Type_Str = value.ToString("d"); }
        }

        public Function Function
        {
            get
            {
                if(_Function == null && context != null && !context.IsDisposed)
                {
                    _Function = context.Functions.Where(a => a.Id == FunctionId).Single();
                    _Function.context = context;
                }
                return _Function;
            }
            set
            {
                _Function = value;
            }
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}