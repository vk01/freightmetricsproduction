﻿using System.Collections.Generic;

namespace Exis.Domain
{
    public delegate object GetValueDel(params object[] parameters);

    public delegate void SetValueDel(object[] parameters, object value);

    public class ExtendedList<T> : List<T>
    {
        protected readonly GetValueDel m_DelGetValue;
        protected readonly SetValueDel m_DelSetValue;
        protected readonly object[] m_params;

        public ExtendedList(object[] parameters, GetValueDel delGetValue, SetValueDel delSetValue)
        {
            m_params = parameters;
            m_DelGetValue = delGetValue;
            m_DelSetValue = delSetValue;
        }

        public object Values
        {
            get { return m_DelGetValue.Invoke(m_params); }
            set { m_DelSetValue.Invoke(m_params, value); }
        }
    }
}