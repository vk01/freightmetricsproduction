﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exis.Domain
{
    public partial class Ugroup:DomainObject
    {
        public DataContext context;
        private List<Permission> _Permissions;

        public List<Permission> Permissions
        {
            get
            {
                if(_Permissions == null && context != null && !context.IsDisposed)
                {
                    _Permissions = (from objUgroupPermission in context.UgroupsPermissions
                                    join objPermission in context.Permissions on objUgroupPermission.PermissionId equals
                                        objPermission.Id
                                    where objUgroupPermission.UserGroupId == Id
                                    select objPermission).ToList();
                    foreach (Permission permission in _Permissions)
                    {
                        permission.context = context;
                    }
                }
                return _Permissions;
            }
            set { _Permissions = value; }
        }
    }
}
