using System;
using System.Linq;
using System.Runtime.Serialization;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class ParameterMapping : DomainObject
    {
        private FunctionParameter _FunctionParameter;
        private EventTypeAction _EventTypeAction;
        public DataContext context;

        [DataMember]
        public FunctionParameter FunctionParameter
        {
            get
            {
                if (_FunctionParameter == null && context != null && !context.IsDisposed)
                {
                    _FunctionParameter = context.FunctionParameters.Where(a => a.Id == FunctionParameterId).SingleOrDefault();
                    _FunctionParameter.context = context;
                }
                return _FunctionParameter;
            }
            set { _FunctionParameter = value; }
        }

        public EventTypeAction EventTypeAction
        {
            get
            {
                if(_EventTypeAction == null && context != null && !context.IsDisposed)
                {
                    _EventTypeAction = context.EventTypeActions.Where(a => a.Id == EventTypeActionId).Single();
                    _EventTypeAction.context = context;
                }
                return _EventTypeAction;
            }
            set
            {
                _EventTypeAction = value;
                if(value != null)
                {
                    EventTypeActionId = value.Id;
                }
            }
        }

        #region Compiled Queries

        public static Func<DataContext, long, IQueryable<ParameterMapping>>
            WorkflowService_ExecuteEventAction =
                CompiledQuery.Compile(
                    (DataContext dataContext, long EventTypeActionId) =>
                    from parameterMapping in dataContext.ParameterMappings
                    from functionParameter in dataContext.FunctionParameters
                    where parameterMapping.FunctionParameterId == functionParameter.Id
                          && parameterMapping.EventTypeActionId == EventTypeActionId
                    orderby functionParameter.Type_Str
                    orderby functionParameter.Sort
                    select parameterMapping);

        #endregion
    }
}