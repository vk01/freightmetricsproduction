using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class User : DomainObject
    {
        [DataMember]
        public List<Book> Books { get; set; }

        [DataMember]
        public List<Trader> Traders { get; set; }
    }
}