﻿using System;
using System.Collections.Generic;
using System.Linq;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class Function : DomainObject
    {
        public DataContext context;
        private List<FunctionParameter> _FunctionParameters;

        public FunctionTypeEnum Type
        {
            get { return (FunctionTypeEnum) Enum.Parse(typeof (FunctionTypeEnum), Type_Str); }
            set { Type_Str = value.ToString("d"); }
        }

        public List<FunctionParameter> FunctionParameters
        {
            get
            {
                if (_FunctionParameters == null && context != null && !context.IsDisposed)
                {
                    _FunctionParameters = context.FunctionParameters.Where(a => a.FunctionId == _Id).ToList();
                    foreach (FunctionParameter functionParameter in _FunctionParameters)
                    {
                        functionParameter.context = context;
                    }
                }
                return _FunctionParameters;
            }
            set { _FunctionParameters = value; }
        }

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum) Enum.Parse(typeof (ActivationStatusEnum), _Status_Str); }
            set { _Status_Str = value.ToString("d"); }
        }

        public override string ToString()
        {
            return _Name;
        }

        #region Compiled Queries

        public static Func<DataContext, long, IQueryable<Function>> WorkflowService_MustEventExpire =
            CompiledQuery.Compile(
                (DataContext dataContext, long EventTypeId) =>
                from eventTypeAction in dataContext.EventTypeActions
                from function in dataContext.Functions
                where eventTypeAction.EventTypeId == EventTypeId
                      && eventTypeAction.FunctionId == function.Id
                      && eventTypeAction.Type_Str == EventTypeActionTypeEnum.Main.ToString("d")
                select function);

        #endregion
    }
}