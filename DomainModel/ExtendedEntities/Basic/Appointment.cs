using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Devart.Data.Linq;
using Exis.Domain.Attributes;

namespace Exis.Domain
{
    public partial class Appointment : DomainObject
    {
        public DataContext context;
        private Event _Event;
        private User _User;
        private List<User> _AssociatedUsers;

        [DataMember]
        public Event Event
        {
            get
            {
                if(_Event == null && context!= null && !context.IsDisposed)
                {
                    _Event = (from objEvent in context.Events
                              where objEvent.Id == RefId &&
                              RefType_Str == AppointmentRefTypeEnum.Event.ToString("d")
                              select objEvent).First();
                    
                    _Event.context = context;
                }
                return _Event;
            }
            set
            {
                _Event = value;
            }
        }

        [DataMember]
        public List<User> AssociatedUsers
        {
            get
            {
                if (_AssociatedUsers == null && context != null && !context.IsDisposed)
                {
                    _AssociatedUsers = (from usersAppointment in context.UsersAppointments
                                        join user in context.Users on usersAppointment.UserId equals user.Id
                                        select user).ToList();
                }
                return _AssociatedUsers;
            }
            set
            {
                _AssociatedUsers = value;
            }
        }

        [AppFieldPropertyDomain("Users", "Users associated with this appointment.")]
        [DataMember]
        public List<User> Users
        {
            get
            {
                if(context != null && !context.IsDisposed)
                {
                    return (from usersAppointment in context.UsersAppointments
                            join user in context.Users on usersAppointment.UserId equals user.Id
                            select user).ToList();
                }
                return null;
            }
            set
            {
                if (context != null && !context.IsDisposed)
                {
                    context.UsersAppointments.DeleteAllOnSubmit(context.UsersAppointments.Where(a => a.AppointmentId == _Id));
                    if (value != null)
                    {
                        foreach (User user in value)
                        {
                            context.UsersAppointments.InsertOnSubmit(new UsersAppointment
                                                                         {
                                                                             Id =
                                                                                 context.GetNextId(
                                                                                     typeof (UsersAppointment)),
                                                                             context = context,
                                                                             AppointmentId = _Id,
                                                                             UserId = user.Id
                                                                         });
                        }
                    }
                }
            }
        }

        public bool IsAllDay
        {
            get { return IsAllDay_Str == "1" ? true : false; }
            set { IsAllDay_Str = value ? "1" : "0"; }
        }

        public AppointmentRefTypeEnum RefType
        {
            get { return (AppointmentRefTypeEnum) Enum.Parse(typeof (AppointmentRefTypeEnum), RefType_Str); }
            set { RefType_Str = value.ToString("d"); }
        }

        #region Compiled Queries

        public static Func<DataContext, long, DateTime, IQueryable<Appointment>>
            ClientsService_GetCustomerAppointments =
                CompiledQuery.Compile(
                    (DataContext dataContext, long CustomerId, DateTime date) =>
                    from objApp in dataContext.Appointments
                    from objCase in dataContext.Cases
                    from objEvent in dataContext.Events
                    where objApp.RefId == objEvent.Id &&
                          objApp.RefType == AppointmentRefTypeEnum.Event &&
                          objEvent.CaseId == objCase.Id &&
                          objCase.CustomerId == CustomerId &&
                          ((DateTime) objApp.StartDate).Date >= date.Date
                    select objApp);

        public static Func<DataContext, long, IQueryable<Appointment>>
            ClientsService_AgentCenter_GetUserAppointments_1 =
                CompiledQuery.Compile((DataContext dataContext, long userId) =>
                                      from objAppointment in dataContext.Appointments
                                      from objUserAppointment in dataContext.UsersAppointments
                                      from objUser in dataContext.Users
                                      from objEvent in dataContext.Events
                                      from objEventType in dataContext.EventTypes
                                      where objAppointment.RefType_Str == AppointmentRefTypeEnum.Event.ToString("d") &&
                                            objAppointment.RefId == objEvent.Id &&
                                            objEvent.Status_Str == CaseEventStatusEnum.Opened.ToString("d") &&
                                            objEvent.EventTypeId == objEventType.Id &&
                                            objEventType.ProcessType_Str == EventTypeProcessEnum.User.ToString("d") &&
                                            objUserAppointment.UserId == userId &&
                                            objUserAppointment.UserId == objUser.Id &&
                                            objAppointment.Id == objUserAppointment.AppointmentId
                                      select
                                          objAppointment);

        public static Func<DataContext, long, IQueryable<Appointment>>
            ClientsService_AgentCenter_GetUserAppointments_4 =
                CompiledQuery.Compile((DataContext dataContext, long userId) =>
                                      from objAppointment in dataContext.Appointments
                                      from objUserAppointment in dataContext.UsersAppointments
                                      from objUser in dataContext.Users
                                      where objAppointment.RefType_Str == AppointmentRefTypeEnum.Null.ToString("d") &&
                                            objAppointment.RefId == null &&
                                            objUserAppointment.UserId == userId &&
                                            objUserAppointment.UserId == objUser.Id &&
                                            objAppointment.Id == objUserAppointment.AppointmentId
                                      select
                                          objAppointment);

        public static Func<DataContext, long, DateTime, IQueryable<Appointment>>
            ClientsService_GetUserPreviousLatestEventAppointment =
                CompiledQuery.Compile((DataContext dataContext, long userId, DateTime dateTime) =>
                                      from objAppointment in dataContext.Appointments
                                      from objUserAppointment in dataContext.UsersAppointments
                                      from objEvent in dataContext.Events
                                      where
                                          objAppointment.RefType_Str ==
                                          AppointmentRefTypeEnum.Event.ToString("d") &&
                                          objAppointment.RefId == objEvent.Id &&
                                          objEvent.Status_Str == CaseEventStatusEnum.Opened.ToString("d") &&
                                          objUserAppointment.UserId == userId &&
                                          objUserAppointment.AppointmentId == objAppointment.Id &&
                                          ((DateTime)objAppointment.StartDate).Date < dateTime.Date
                                      select objAppointment);

        public static Func<DataContext, long, DateTime, IQueryable<Appointment>>
            ClientsService_GetUserPreviousLatestPersonalAppointment =
                CompiledQuery.Compile((DataContext dataContext, long userId, DateTime dateTime) =>
                                      from objAppointment in dataContext.Appointments
                                      from objUserAppointment in dataContext.UsersAppointments
                                      where
                                          objAppointment.RefType_Str ==
                                          AppointmentRefTypeEnum.Null.ToString("d") &&
                                          objUserAppointment.UserId == userId &&
                                          objUserAppointment.AppointmentId == objAppointment.Id &&
                                          ((DateTime)objAppointment.StartDate).Date < dateTime.Date
                                      select objAppointment);

        public static Func<DataContext, long, DateTime, IQueryable<Appointment>>
            ClientsService_GetUserNextEarlierEventAppointment =
                CompiledQuery.Compile((DataContext dataContext, long userId, DateTime dateTime) =>
                                      from objAppointment in dataContext.Appointments
                                      from objUserAppointment in dataContext.UsersAppointments
                                      from objEvent in dataContext.Events
                                      where
                                          objAppointment.RefType_Str ==
                                          AppointmentRefTypeEnum.Event.ToString("d") &&
                                          objAppointment.RefId == objEvent.Id &&
                                          objEvent.Status_Str == CaseEventStatusEnum.Opened.ToString("d") &&
                                          objUserAppointment.UserId == userId &&
                                          objUserAppointment.AppointmentId == objAppointment.Id &&
                                          ((DateTime) objAppointment.StartDate).Date > dateTime.Date
                                      select objAppointment);

        public static Func<DataContext, long, DateTime, IQueryable<Appointment>>
            ClientsService_GetUserNextEarlierPersonalAppointment =
                CompiledQuery.Compile((DataContext dataContext, long userId, DateTime dateTime) =>
                                      from objAppointment in dataContext.Appointments
                                      from objUserAppointment in dataContext.UsersAppointments
                                      where
                                          objAppointment.RefType_Str ==
                                          AppointmentRefTypeEnum.Null.ToString("d") &&
                                          objUserAppointment.UserId == userId &&
                                          objUserAppointment.AppointmentId == objAppointment.Id &&
                                          ((DateTime) objAppointment.StartDate).Date > dateTime.Date
                                      select objAppointment);

        #endregion
    }
}