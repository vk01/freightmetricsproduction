using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Devart.Data.Linq;
using Exis.RuleEngine;
using Exis.SessionRegistry;

namespace Exis.Domain
{
    public partial class Udf:DomainObject
    {
        public DataContext context;
        [DataMember] public UdfsDefaultValue UdfDefaultValue;
        [DataMember] public UdfsValue UdfValue;


        /// <summary>
        /// The Value of the udf - Used for Customer Management - Even when Udf has Multiple values we send back only the most recently changed multiple value.
        /// </summary>
        [DataMember] public Object Value;

        /// <summary>
        /// The Values of the udf - Used for AddEditCustomer - Udf Value can be multiple
        /// </summary>
        [DataMember] public List<Object> Values;

        private List<UdfsListValue> _ListValues;
        private List<Udfgroup> _UdfGroups;
        private List<UdfsDefaultValue> _udfsDefaultValues;
        [DataMember] public Object defaultRuntimeAppFieldValue;
        [DataMember] public bool hasOnlyDefault;

        [DataMember]
        public UDFTypeEnum Type
        {
            get { return (UDFTypeEnum) Enum.Parse(typeof (UDFTypeEnum), Type_Str); }
            set { Type_Str = value.ToString("d"); }
        }

        [DataMember]
        public UDFRefTypeEnum RefType
        {
            get { return (UDFRefTypeEnum) Enum.Parse(typeof (UDFRefTypeEnum), RefType_Str); }
            set { RefType_Str = value.ToString("d"); }
        }

        [DataMember]
        public UDFMultipleTypeEnum MultipleType
        {
            get
            {
                return String.IsNullOrEmpty(MultipleType_Str)
                           ? UDFMultipleTypeEnum.Null
                           : (UDFMultipleTypeEnum) Enum.Parse(typeof (UDFMultipleTypeEnum), MultipleType_Str);
            }
            set { MultipleType_Str = value.ToString("d"); }
        }

        [DataMember]
        public UDFAccessTypeEnum AccessType
        {
            get { return (UDFAccessTypeEnum) Enum.Parse(typeof (UDFAccessTypeEnum), AccessType_Str); }
            set { AccessType_Str = value.ToString("d"); }
        }

        [DataMember]
        public UDFListTypeEnum ListType
        {
            get
            {
                return String.IsNullOrEmpty(ListType_Str)
                           ? UDFListTypeEnum.Null
                           : (UDFListTypeEnum) Enum.Parse(typeof (UDFListTypeEnum), ListType_Str);
            }
            set { ListType_Str = value.ToString("d"); }
        }

        public bool IsList
        {
            get { return IsList_Str == "1" ? true : false; }
            set { IsList_Str = value ? "1" : "0"; }
        }

        public bool IsMultiple
        {
            get { return IsMultiple_Str == "1" ? true : false; }
            set { IsMultiple_Str = value ? "1 " : "0"; }
        }

        public bool IsDefault
        {
            get { return IsDefault_Str == "1" ? true : false; }
            set { IsDefault_Str = value ? "1 " : "0"; }
        }

        public UDFDefaultTypeEnum DefaultType
        {
            get
            {
                return String.IsNullOrEmpty(DefaultType_Str)
                           ? UDFDefaultTypeEnum.Null
                           : (UDFDefaultTypeEnum) Enum.Parse(typeof (UDFDefaultTypeEnum), DefaultType_Str);
            }
            set { DefaultType_Str = value.ToString("d"); }
        }

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum) Enum.Parse(typeof (ActivationStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        [DataMember]
        public List<Udfgroup> UdfGroups
        {
            get
            {
                if (_UdfGroups == null && context != null && !context.IsDisposed)
                {
                    _UdfGroups = (from objAssoc in context.UdfsGroups
                                  from objUdf in context.Udfs
                                  from objUdfGroup in context.Udfgroups
                                  where objUdf.Id == objAssoc.UdfId &&
                                        objAssoc.Id == objUdfGroup.Id
                                  select objUdfGroup).ToList();

                    foreach (Udfgroup udfgroup in _UdfGroups)
                    {
                        udfgroup.context = context;
                    }
                }
                return _UdfGroups;
            }
            set { _UdfGroups = value; }
        }

        [DataMember]
        public List<UdfsListValue> ListValues
        {
            get
            {
                if (_ListValues == null && context != null && !context.IsDisposed)
                {
                    _ListValues = (from objListValue in context.UdfsListValues
                                   where
                                       objListValue.UdfId == Id &&
                                       objListValue.Status_Str == ActivationStatusEnum.Active.ToString("d")
                                   select objListValue).OrderBy(a=>a.Sort).ToList();
                }
                return _ListValues;
            }
            set { _ListValues = value; }
        }

        [DataMember]
        public List<UdfsDefaultValue> UdfDefaultValues
        {
            get
            {
                if (_udfsDefaultValues == null && context != null && !context.IsDisposed)
                {
                    _udfsDefaultValues = (from objUdfDefaultValue in context.UdfsDefaultValues
                                          where objUdfDefaultValue.UdfId == Id
                                          select objUdfDefaultValue).ToList();
                }
                return _udfsDefaultValues;
            }
            set { _udfsDefaultValues = value; }
        }

        #region Static Methods

        public object GetTypedValue(string value)
        {
            if (Type == UDFTypeEnum.ShortText ||
                Type == UDFTypeEnum.LongText ||
                Type == UDFTypeEnum.ExternalLink)
            {
                return Convert.ToString(value);
            }

            if (Type == UDFTypeEnum.Date)
            {
                return Convert.ToDateTime(value, ServerSessionRegistry.ServerCultureInfo);
            }
            if (Type == UDFTypeEnum.Numeric)
            {
                return Convert.ToDecimal(value, ServerSessionRegistry.ServerCultureInfo);
            }
            if (Type == UDFTypeEnum.Status)
            {
                return value == "1" ? true : false;
            }
            if (Type == UDFTypeEnum.Image || Type == UDFTypeEnum.File)
            {
                return Convert.FromBase64String(value);
            }
            if (Type == UDFTypeEnum.InternalLink)
            {
                Type type = System.Type.GetType("Exis.Domain." + Enum.GetName(typeof (InternalLinkUDFSubTypeEnum),
                                                                                    Convert.ToInt32(SubType_Str)));

                return new XmlSerializer(type).Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(value)));
            }

            return null;
        }


        public static object GetUdfValue(object instance, List<UdfsValue> inMemoryUdfValues, DataContext context,
                                         long refId, Udf udf)
        {
            List<UdfsValue> udfValues;
            if (inMemoryUdfValues.Count > 0)
            {
                udfValues = inMemoryUdfValues;
            }
            else
            {
                udfValues = UdfsValue.Udf_GetUdfValue(context, refId, udf.Id).ToList();
            }

            if (udf.Type == UDFTypeEnum.ShortText ||
                udf.Type == UDFTypeEnum.LongText ||
                udf.Type == UDFTypeEnum.ExternalLink)
            {
                if (udfValues.Count() == 0 && udf.IsDefault)
                {
                    if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    {
                        if (udf.IsMultiple)
                        {
                            return
                                context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                    a => Convert.ToString(a.Value)).
                                    ToList();
                        }
                        else
                        {
                            return context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                a => Convert.ToString(a.Value)).FirstOrDefault();
                        }
                    }
                    else
                    {
                        return AppFieldEvaluator.GetAppFieldValue(udf.DefaultAppField, instance);
                    }
                }
                else if (udfValues.Count > 0)
                {
                    if (udf.IsMultiple)
                    {
                        return udfValues.Select(a => Convert.ToString(a.Value)).ToList();
                    }
                    else
                    {
                        return udfValues.Select(a => Convert.ToString(a.Value)).FirstOrDefault();
                    }
                }
            }
            else if (udf.Type == UDFTypeEnum.Date)
            {
                if (udfValues.Count() == 0 && udf.IsDefault)
                {
                    if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    {
                        if (udf.IsMultiple)
                        {
                            return
                                context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                    a => Convert.ToDateTime(a.Value, ServerSessionRegistry.ServerCultureInfo))
                                    .
                                    ToList();
                        }
                        else
                        {
                            return context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                a => Convert.ToDateTime(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                FirstOrDefault();
                        }
                    }
                    else
                    {
                        return AppFieldEvaluator.GetAppFieldValue(udf.DefaultAppField, instance);
                    }
                }
                else if (udfValues.Count > 0)
                {
                    if (udf.IsMultiple)
                    {
                        return
                            udfValues.Select(
                                a => Convert.ToDateTime(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                ToList();
                    }
                    else
                    {
                        return
                            udfValues.Select(
                                a => Convert.ToDateTime(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                FirstOrDefault();
                    }
                }
            }
            else if (udf.Type == UDFTypeEnum.Numeric)
            {
                if (udfValues.Count() == 0 && udf.IsDefault)
                {
                    if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    {
                        if (udf.IsMultiple)
                        {
                            return
                                context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                    a => Convert.ToDecimal(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                    ToList();
                        }
                        else
                        {
                            return context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                a => Convert.ToDecimal(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                FirstOrDefault();
                        }
                    }
                    else
                    {
                        return AppFieldEvaluator.GetAppFieldValue(udf.DefaultAppField, instance);
                    }
                }
                else if (udfValues.Count > 0)
                {
                    if (udf.IsMultiple)
                    {
                        return
                            udfValues.Select(
                                a => Convert.ToDecimal(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                ToList();
                    }
                    else
                    {
                        return
                            udfValues.Select(
                                a => Convert.ToDecimal(a.Value, ServerSessionRegistry.ServerCultureInfo)).
                                FirstOrDefault();
                    }
                }
            }
            else if (udf.Type == UDFTypeEnum.Status)
            {
                if (udfValues.Count() == 0 && udf.IsDefault)
                {
                    if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    {
                        return context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                            a => Convert.ToString(a.Value)).FirstOrDefault() == "1"
                                   ? true
                                   : false;
                    }
                    else
                    {
                        return AppFieldEvaluator.GetAppFieldValue(udf.DefaultAppField, instance);
                    }
                }
                else if (udfValues.Count > 0)
                {
                    return udfValues.Select(a => Convert.ToString(a.Value)).FirstOrDefault() == "1" ? true : false;
                }
            }
            else if (udf.Type == UDFTypeEnum.File || udf.Type == UDFTypeEnum.Image)
            {
                if (udfValues.Count() == 0 && udf.IsDefault)
                {
                    if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    {
                        if (udf.IsMultiple)
                        {
                            return
                                context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                    a => Convert.FromBase64String(a.Value)).
                                    ToList();
                        }
                        else
                        {
                            return context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                a => Convert.FromBase64String(a.Value)).FirstOrDefault();
                        }
                    }
                    else
                    {
                        return AppFieldEvaluator.GetAppFieldValue(udf.DefaultAppField, instance);
                    }
                }
                else if (udfValues.Count > 0)
                {
                    if (udf.IsMultiple)
                    {
                        return udfValues.Select(a => Convert.FromBase64String(a.Value)).ToList();
                    }
                    else
                    {
                        return udfValues.Select(a => Convert.FromBase64String(a.Value)).FirstOrDefault();
                    }
                }
            }
            else if (udf.Type == UDFTypeEnum.InternalLink)
            {
                Type type =
                    System.Type.GetType("Exis.Domain." +
                                        Enum.GetName(typeof (InternalLinkUDFSubTypeEnum),
                                                     Convert.ToInt32(udf.SubType_Str)) + ",Exis.DomainModel");
                Type listType = typeof (List<>).MakeGenericType(type);
                if (udfValues.Count() == 0 && udf.IsDefault)
                {
                    if (udf.DefaultType == UDFDefaultTypeEnum.Predefined)
                    {
                        if (udf.IsMultiple)
                        {
                            var list = (IList) Activator.CreateInstance(listType);
                            foreach (
                                string value in
                                    context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                        a => Convert.ToString(a.Value)).ToList())
                            {
                                list.Add(
                                    new XmlSerializer(type).Deserialize(
                                        new MemoryStream(Encoding.UTF8.GetBytes(value))));
                            }
                            return list;
                        }
                        else
                        {
                            return
                                new XmlSerializer(type).Deserialize(
                                    new MemoryStream(
                                        Encoding.UTF8.GetBytes(
                                            context.UdfsDefaultValues.Where(a => a.UdfId == udf.Id).Select(
                                                a => Convert.ToString(a.Value)).FirstOrDefault())));
                        }
                    }
                    else
                    {
                        return AppFieldEvaluator.GetAppFieldValue(udf.DefaultAppField, instance);
                    }
                }
                else if (udfValues.Count > 0)
                {
                    if (udf.IsMultiple)
                    {
                        var list = (IList) Activator.CreateInstance(listType);
                        foreach (string value in udfValues.Select(a => Convert.ToString(a.Value)).ToList())
                        {
                            list.Add(
                                new XmlSerializer(type).Deserialize(
                                    new MemoryStream(Encoding.UTF8.GetBytes(value))));
                        }
                        return list;
                    }
                    else
                    {
                        return
                            new XmlSerializer(type).Deserialize(
                                new MemoryStream(
                                    Encoding.UTF8.GetBytes(
                                        udfValues.Select(a => Convert.ToString(a.Value)).FirstOrDefault())));
                    }
                }
            }
            return null;
        }


        public static void SetUdfValue(DataContext context, UDFRefTypeEnum refType, long refId, long? refTypeId,
                                       long udfId, object values)
        {
            Udf udf =
                context.Udfs.Where(
                    a =>
                    /*a.Code == udfCode && a.RefType_Str == refType.ToString("d") &&
                    (a.RefTypeId == refTypeId || a.RefTypeId == null)*/
                    a.Id == udfId).
                    SingleOrDefault();

            List<UdfsValue> udfValues = context.UdfsValues.Where(a => a.RefId == refId && a.UdfId == udf.Id).ToList();

            foreach (UdfsValue udfValue in udfValues)
            {
                context.UdfsValues.DeleteOnSubmit(udfValue);
            }

            if (values != null)
            {
                DateTime currentDate = DateTime.Now;

                if (values.GetType() == typeof (string))
                {
                    var newValue = new UdfsValue
                                       {
                                           Id = context.GetNextId(typeof (UdfsValue)),
                                           UdfId = udf.Id,
                                           RefId = refId,
                                           Value = values.ToString(),
                                           Chappd = currentDate,
                                           Chappuser = ServerSessionRegistry.User,
                                           context = context
                                       };
                    context.UdfsValues.InsertOnSubmit(newValue);
                }
                else if (values.GetType() == typeof (List<string>))
                {
                    if (udf.IsMultiple)
                    {
                        foreach (string value in (List<string>) values)
                        {
                            var newValue = new UdfsValue
                                               {
                                                   Id = context.GetNextId(typeof (UdfsValue)),
                                                   UdfId = udf.Id,
                                                   RefId = refId,
                                                   Value = value,
                                                   Chappd = currentDate,
                                                   Chappuser =
                                                       ServerSessionRegistry.User,
                                                   context = context
                                               };
                            context.UdfsValues.InsertOnSubmit(newValue);
                        }
                    }
                    else
                    {
                        var newValue = new UdfsValue
                                           {
                                               Id = context.GetNextId(typeof (UdfsValue)),
                                               UdfId = udf.Id,
                                               RefId = refId,
                                               Value = ((List<string>) values)[0],
                                               Chappd = currentDate,
                                               Chappuser = ServerSessionRegistry.User,
                                               context = context
                                           }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                }
                else if (values.GetType() == typeof (DateTime))
                {
                    var newValue = new UdfsValue
                                       {
                                           Id = context.GetNextId(typeof (UdfsValue)),
                                           UdfId = udf.Id,
                                           RefId = refId,
                                           Value =
                                               ((DateTime) values).ToString(
                                                   ServerSessionRegistry.ServerCultureInfo),
                                           Chappd = currentDate,
                                           Chappuser = ServerSessionRegistry.User,
                                           context = context
                                       };
                    context.UdfsValues.InsertOnSubmit(newValue);
                }
                else if (values.GetType() == typeof (List<DateTime>))
                {
                    if (udf.IsMultiple)
                    {
                        foreach (DateTime value in (List<DateTime>) values)
                        {
                            var newValue = new UdfsValue
                                               {
                                                   Id = context.GetNextId(typeof (UdfsValue)),
                                                   UdfId = udf.Id,
                                                   RefId = refId,
                                                   Value =
                                                       value.ToString(ServerSessionRegistry.ServerCultureInfo),
                                                   Chappd = currentDate,
                                                   Chappuser =
                                                       ServerSessionRegistry.User,
                                                   context = context
                                               };
                            context.UdfsValues.InsertOnSubmit(newValue);
                        }
                    }
                    else
                    {
                        var newValue = new UdfsValue
                                           {
                                               Id = context.GetNextId(typeof (UdfsValue)),
                                               UdfId = udf.Id,
                                               RefId = refId,
                                               Value =
                                                   ((List<DateTime>) values)[0].ToString(
                                                       ServerSessionRegistry.ServerCultureInfo),
                                               Chappd = currentDate,
                                               Chappuser = ServerSessionRegistry.User,
                                               context = context
                                           }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                }
                else if (values.GetType() == typeof (decimal) || values.GetType() == typeof (int) ||
                         values.GetType() == typeof (long) || values.GetType() == typeof (float) ||
                         values.GetType() == typeof (double))
                {
                    var newValue = new UdfsValue
                                       {
                                           Id = context.GetNextId(typeof (UdfsValue)),
                                           UdfId = udf.Id,
                                           RefId = refId,
                                           Value =
                                               Convert.ToDecimal(values).ToString(
                                                   ServerSessionRegistry.ServerCultureInfo),
                                           Chappd = currentDate,
                                           Chappuser = ServerSessionRegistry.User,
                                           context = context
                                       };
                    context.UdfsValues.InsertOnSubmit(newValue);
                }
                else if (values.GetType() == typeof (List<decimal>))
                {
                    if (udf.IsMultiple)
                    {
                        foreach (decimal value in (List<decimal>) values)
                        {
                            var newValue = new UdfsValue
                                               {
                                                   Id = context.GetNextId(typeof (UdfsValue)),
                                                   UdfId = udf.Id,
                                                   RefId = refId,
                                                   Value =
                                                       value.ToString(ServerSessionRegistry.ServerCultureInfo),
                                                   Chappd = currentDate,
                                                   Chappuser =
                                                       ServerSessionRegistry.User,
                                                   context = context
                                               };
                            context.UdfsValues.InsertOnSubmit(newValue);
                        }
                    }
                    else
                    {
                        var newValue = new UdfsValue
                                           {
                                               Id = context.GetNextId(typeof (UdfsValue)),
                                               UdfId = udf.Id,
                                               RefId = refId,
                                               Value =
                                                   ((List<decimal>) values)[0].ToString(
                                                       ServerSessionRegistry.ServerCultureInfo),
                                               Chappd = currentDate,
                                               Chappuser = ServerSessionRegistry.User,
                                               context = context
                                           }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                }
                else if (values.GetType() == typeof(List<int>))
                {
                    if (udf.IsMultiple)
                    {
                        foreach (int value in (List<int>)values)
                        {
                            var newValue = new UdfsValue
                            {
                                Id = context.GetNextId(typeof(UdfsValue)),
                                UdfId = udf.Id,
                                RefId = refId,
                                Value =
                                    value.ToString(ServerSessionRegistry.ServerCultureInfo),
                                Chappd = currentDate,
                                Chappuser =
                                    ServerSessionRegistry.User,
                                context = context
                            };
                            context.UdfsValues.InsertOnSubmit(newValue);
                        }
                    }
                    else
                    {
                        var newValue = new UdfsValue
                        {
                            Id = context.GetNextId(typeof(UdfsValue)),
                            UdfId = udf.Id,
                            RefId = refId,
                            Value =
                                ((List<int>)values)[0].ToString(
                                    ServerSessionRegistry.ServerCultureInfo),
                            Chappd = currentDate,
                            Chappuser = ServerSessionRegistry.User,
                            context = context
                        }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                }
                else if (values.GetType() == typeof(List<long>))
                {
                    if (udf.IsMultiple)
                    {
                        foreach (int value in (List<long>)values)
                        {
                            var newValue = new UdfsValue
                            {
                                Id = context.GetNextId(typeof(UdfsValue)),
                                UdfId = udf.Id,
                                RefId = refId,
                                Value =
                                    value.ToString(ServerSessionRegistry.ServerCultureInfo),
                                Chappd = currentDate,
                                Chappuser =
                                    ServerSessionRegistry.User,
                                context = context
                            };
                            context.UdfsValues.InsertOnSubmit(newValue);
                        }
                    }
                    else
                    {
                        var newValue = new UdfsValue
                        {
                            Id = context.GetNextId(typeof(UdfsValue)),
                            UdfId = udf.Id,
                            RefId = refId,
                            Value =
                                ((List<long>)values)[0].ToString(
                                    ServerSessionRegistry.ServerCultureInfo),
                            Chappd = currentDate,
                            Chappuser = ServerSessionRegistry.User,
                            context = context
                        }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                }
                else if (values.GetType() == typeof (bool))
                {
                    var newValue = new UdfsValue
                                       {
                                           Id = context.GetNextId(typeof (UdfsValue)),
                                           UdfId = udf.Id,
                                           RefId = refId,
                                           Value = ((bool) values) ? "1" : "0",
                                           Chappd = currentDate,
                                           Chappuser = ServerSessionRegistry.User,
                                           context = context
                                       };
                    context.UdfsValues.InsertOnSubmit(newValue);
                }
                else if (values.GetType() == typeof (byte[]))
                {
                    var newValue = new UdfsValue
                                       {
                                           Id = context.GetNextId(typeof (UdfsValue)),
                                           UdfId = udf.Id,
                                           RefId = refId,
                                           Value = Convert.ToBase64String((byte[]) values),
                                           Chappd = currentDate,
                                           Chappuser = ServerSessionRegistry.User,
                                           context = context
                                       };
                    context.UdfsValues.InsertOnSubmit(newValue);
                }
                else if (values.GetType() == typeof (List<byte[]>))
                {
                    if (udf.IsMultiple)
                    {
                        foreach (var value in (List<byte[]>) values)
                        {
                            var newValue = new UdfsValue
                                               {
                                                   Id = context.GetNextId(typeof (UdfsValue)),
                                                   UdfId = udf.Id,
                                                   RefId = refId,
                                                   Value = Convert.ToBase64String(value),
                                                   Chappd = currentDate,
                                                   Chappuser =
                                                       ServerSessionRegistry.User,
                                                   context = context
                                               };
                            context.UdfsValues.InsertOnSubmit(newValue);
                        }
                    }
                    else
                    {
                        var newValue = new UdfsValue
                                           {
                                               Id = context.GetNextId(typeof (UdfsValue)),
                                               UdfId = udf.Id,
                                               RefId = refId,
                                               Value = Convert.ToBase64String(((List<byte[]>) values)[0]),
                                               Chappd = currentDate,
                                               Chappuser = ServerSessionRegistry.User,
                                               context = context
                                           }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                }
                else
                {
//                    if (values.GetType().IsSubclassOf(typeof(List<>)))
                    if (values.GetType().IsGenericType &&
                        values.GetType().GetGenericTypeDefinition() == typeof (List<>))
                    {
                        if (values.GetType().GetGenericArguments()[0].IsSubclassOf(typeof (DomainObject)))
                        {
                            var list = (IList) values;
                            if (udf.IsMultiple)
                            {
                                foreach (object value in list)
                                {
                                    var xmlSerializer = new XmlSerializer(values.GetType().GetGenericArguments()[0]);
                                    var stream = new MemoryStream();
                                    xmlSerializer.Serialize(stream, value);

                                    var newValue = new UdfsValue
                                                       {
                                                           Id = context.GetNextId(typeof (UdfsValue)),
                                                           UdfId = udf.Id,
                                                           RefId = refId,
                                                           Value = Encoding.UTF8.GetString(stream.ToArray()),
                                                           Chappd = currentDate,
                                                           Chappuser =
                                                               ServerSessionRegistry.User,
                                                           context = context
                                                       };
                                    context.UdfsValues.InsertOnSubmit(newValue);
                                }
                            }
                            else
                            {
                                var xmlSerializer = new XmlSerializer(values.GetType().GetGenericArguments()[0]);
                                var stream = new MemoryStream();
                                xmlSerializer.Serialize(stream, list[0]);

                                var newValue = new UdfsValue
                                                   {
                                                       Id = context.GetNextId(typeof (UdfsValue)),
                                                       UdfId = udf.Id,
                                                       RefId = refId,
                                                       Value = Encoding.UTF8.GetString(stream.ToArray()),
                                                       Chappd = currentDate,
                                                       Chappuser = ServerSessionRegistry.User,
                                                       context = context
                                                   }
                                    ;
                                context.UdfsValues.InsertOnSubmit(newValue);
                            }
                        }
                    }
                    else if (values.GetType().IsSubclassOf(typeof (DomainObject)))
                    {
                        var xmlSerializer = new XmlSerializer(values.GetType());
                        var stream = new MemoryStream();
                        xmlSerializer.Serialize(stream, values);

                        var newValue = new UdfsValue
                                           {
                                               Id = context.GetNextId(typeof (UdfsValue)),
                                               UdfId = udf.Id,
                                               RefId = refId,
                                               Value = Encoding.UTF8.GetString(stream.ToArray()),
                                               Chappd = currentDate,
                                               Chappuser = ServerSessionRegistry.User,
                                               context = context
                                           }
                            ;
                        context.UdfsValues.InsertOnSubmit(newValue);
                    }
                    else
                    {
                        throw new ApplicationException("Failed to process UDF Value Type: " + values.GetType().ToString());
                    }
                }
            }
        }

        public override string ToString()
        {
            return _Name;
        }

        #endregion

        #region Compiled Queries

        public static Func<DataContext, string, string, long, IQueryable<Udf>>
            Event_GetUdfValues =
                CompiledQuery.Compile(
                    (DataContext dataContext, string UdfCode, string RefType, long RefTypeId) =>
                    from objUdf in dataContext.Udfs
                    where objUdf.Code == UdfCode
                          && objUdf.RefType_Str == RefType
                          && (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null)
                    select objUdf);

        public static Func<DataContext, string, string, IQueryable<Udf>>
            ClientsService_GetGeneralConfigurationData =
                CompiledQuery.Compile(
                    (DataContext dataContext, string ActivationStatus, string RefType) =>
                    dataContext.Udfs.Where(a => a.Status_Str == ActivationStatus &&
                                                a.RefType_Str == RefType));

        public static Func<DataContext, long, string, string, IQueryable<Udf>> ClientsService_GetUdfsByRefType =
            CompiledQuery.Compile(
                (DataContext dataContext, long RefTypeId, string refType, string groupSubType) =>
                (from objUdf in dataContext.Udfs
                 from objGroupAssoc in dataContext.UdfsGroups
                 from objGroup in dataContext.Udfgroups
                 where objGroup.SubType_Str == groupSubType &&
                       objGroup.Type_Str == UdfGroupTypeEnum.ClientView.ToString("d") &&
                       objGroup.UdfType_Str == refType &&
                       objGroupAssoc.UdfGroupId == objGroup.Id &&
                       objUdf.Id == objGroupAssoc.UdfId &&
                       (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null) &&
                       objUdf.RefType_Str == refType &&
                       objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                       objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                 select objUdf));


        public static Func<DataContext, long, string, string, IQueryable<UdfsDefaultValue>>
            ClientsService_GetUdfsByRefType_2 =
                CompiledQuery.Compile(
                    (DataContext dataContext, long RefTypeId, string refType, string groupSubType) =>
                    from objUdf in dataContext.Udfs
                    from objDefaultValue in dataContext.UdfsDefaultValues
                    from objGroupAssoc in dataContext.UdfsGroups
                    from objGroup in dataContext.Udfgroups
                    where objGroup.SubType_Str == groupSubType && //UdfGroupSubTypeEnum.CustManagMain.ToString("d") &&
                          objGroup.Type_Str == UdfGroupTypeEnum.ClientView.ToString("d") &&
                          objGroup.UdfType_Str == refType &&
                          objGroupAssoc.UdfGroupId == objGroup.Id &&
                          objUdf.Id == objGroupAssoc.UdfId &&
                          (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null) &&
                          objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objDefaultValue.UdfId
                    select objDefaultValue);

        public static Func<DataContext, string, string, IQueryable<UdfsDefaultValue>>
            ClientsService_GetUdfsByRefType_4 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string refType, string groupSubtype) =>
                    from objUdf in dataContext.Udfs
                    from objDefaultValue in dataContext.UdfsDefaultValues
                    from objGroupAssoc in dataContext.UdfsGroups
                    from objGroup in dataContext.Udfgroups
                    where objGroup.SubType_Str == groupSubtype &&
                          objGroup.Type_Str == UdfGroupTypeEnum.ClientView.ToString("d") &&
                          objGroup.UdfType_Str == refType &&
                          objGroupAssoc.UdfGroupId == objGroup.Id &&
                          objUdf.Id == objGroupAssoc.UdfId &&
                          objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objDefaultValue.UdfId
                    select objDefaultValue);


        public static Func<DataContext, long, string, string, IQueryable<UdfsListValue>>
            ClientsService_GetUdfsByRefType_3 =
                CompiledQuery.Compile(
                    (DataContext dataContext, long RefTypeId, string refType, string groupSubType) =>
                    from objUdf in dataContext.Udfs
                    from objListValue in dataContext.UdfsListValues
                    from objGroupAssoc in dataContext.UdfsGroups
                    from objGroup in dataContext.Udfgroups
                    where objGroup.SubType_Str == groupSubType &&
                          objGroup.Type_Str == UdfGroupTypeEnum.ClientView.ToString("d") &&
                          objGroup.UdfType_Str == refType &&
                          objGroupAssoc.UdfGroupId == objGroup.Id &&
                          objUdf.Id == objGroupAssoc.UdfId &&
                          (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null) &&
                          objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objListValue.UdfId &&
                          objListValue.Status_Str == ActivationStatusEnum.Active.ToString("d")
                    select objListValue);

        public static Func<DataContext, string, string, IQueryable<UdfsListValue>>
            ClientsService_GetUdfsByRefType_5 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string refType, string groupSubType) =>
                    from objUdf in dataContext.Udfs
                    from objListValue in dataContext.UdfsListValues
                    from objGroupAssoc in dataContext.UdfsGroups
                    from objGroup in dataContext.Udfgroups
                    where objGroup.SubType_Str == groupSubType &&
                          objGroup.Type_Str == UdfGroupTypeEnum.ClientView.ToString("d") &&
                          objGroup.UdfType_Str == refType &&
                          objGroupAssoc.UdfGroupId == objGroup.Id &&
                          objUdf.Id == objGroupAssoc.UdfId &&
                          objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objListValue.UdfId &&
                          objListValue.Status_Str == ActivationStatusEnum.Active.ToString("d")
                    select objListValue);


        public static Func<DataContext, IQueryable<Udf>>
            ClientsService_GetSearchCustomerConfigurationData_2 =
                CompiledQuery.Compile(
                    (DataContext dataContext) =>
                    from objGroup in dataContext.Udfgroups
                    from objAssoc in dataContext.UdfsGroups
                    from objUdf in dataContext.Udfs
                    where objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d") &&
                          (objUdf.RefType_Str == UDFRefTypeEnum.CustomerType.ToString("d") || objUdf.RefType_Str == null) &&
                          objUdf.Type_Str != UDFTypeEnum.Image.ToString("d") &&
                          objUdf.Type_Str != UDFTypeEnum.File.ToString("d") &&
                          objUdf.Type_Str != UDFTypeEnum.InternalLink.ToString("d") &&
                          objUdf.Id == objAssoc.UdfId &&
                          objAssoc.UdfGroupId == objGroup.Id &&
                          objGroup.Type_Str == UdfGroupTypeEnum.ClientView.ToString("d") &&
                          objGroup.SubType_Str == UdfGroupSubTypeEnum.SearchCustomer.ToString("d") &&
                          objGroup.UdfType_Str == UDFRefTypeEnum.CustomerType.ToString("d")
                    select objUdf);

        public static Func<DataContext, long, IQueryable<Udf>>
            ClientsService_GetMultipleUdfValues_1 =
                CompiledQuery.Compile(
                    (DataContext dataContext, long UdfValueId) =>
                    from objUdfValue in dataContext.UdfsValues
                    from objUdf in dataContext.Udfs
                    where objUdfValue.UdfId == objUdf.Id &&
                          objUdfValue.Id == UdfValueId
                    select objUdf);

        public static Func<DataContext, string, IQueryable<Udf>>
            ClientsService_GetUdfsValuesForListRefIdsNoGroup_3 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string RefType) =>
                    from objUdf in dataContext.Udfs
                    where objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d") &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d")
                          && objUdf.RefType_Str == RefType
                    select objUdf);

        public static Func<DataContext, string, IQueryable<UdfsDefaultValue>>
            ClientsService_GetUdfsByRefTypeNoGroup_4 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string refType) =>
                    from objUdf in dataContext.Udfs
                    from objDefaultValue in dataContext.UdfsDefaultValues
                    where objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objDefaultValue.UdfId
                    select objDefaultValue);

        public static Func<DataContext, string, IQueryable<UdfsListValue>>
            ClientsService_GetUdfsByRefTypeNoGroup_5 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string refType) =>
                    from objUdf in dataContext.Udfs
                    from objListValue in dataContext.UdfsListValues
                    where objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objListValue.UdfId &&
                          objListValue.Status_Str == ActivationStatusEnum.Active.ToString("d")
                    select objListValue);

        public static
            Func<DataContext, string, long, IQueryable<Udf>>
            ClientsService_GetUdfsValuesNoUDFGroup_1 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string RefType, long RefTypeId) =>
                    from objUdf in dataContext.Udfs
                    where objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d") &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d")
                          && objUdf.RefType_Str == RefType
                          && (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null)
                    select objUdf);

        public static Func<DataContext, long, string, IQueryable<UdfsDefaultValue>>
            ClientsService_GetUdfsValuesNoUDFGroup_2 =
                CompiledQuery.Compile(
                    (DataContext dataContext, long RefTypeId, string refType) =>
                    from objUdf in dataContext.Udfs
                    from objDefaultValue in dataContext.UdfsDefaultValues
                    where (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null) &&
                          objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objDefaultValue.UdfId
                    select objDefaultValue);

        public static Func<DataContext, long, string, IQueryable<UdfsListValue>>
            ClientsService_GetUdfsValuesNoUDFGroup_3 =
                CompiledQuery.Compile(
                    (DataContext dataContext, long RefTypeId, string refType) =>
                    from objUdf in dataContext.Udfs
                    from objListValue in dataContext.UdfsListValues
                    where (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null) &&
                          objUdf.RefType_Str == refType &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d") &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d")
                          && objUdf.Id == objListValue.UdfId &&
                          objListValue.Status_Str == ActivationStatusEnum.Active.ToString("d")
                    select objListValue);

        #endregion
    }
}