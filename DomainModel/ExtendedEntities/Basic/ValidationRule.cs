﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class ValidationRule:DomainObject
    {
        public DataContext context;
        private Condition _IfCondition;
        private Condition _ThenCondition;

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum) Enum.Parse(typeof (ActivationStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        public ValidationRuleSeverityEnum Severity
        {
            get { return (ValidationRuleSeverityEnum) Enum.Parse(typeof (ValidationRuleSeverityEnum), Severity_Str); }
            set { Severity_Str = value.ToString("d"); }
        }

        public ValidationRuleRefTypeEnum RefType
        {
            get { return (ValidationRuleRefTypeEnum) Enum.Parse(typeof (ValidationRuleRefTypeEnum), RefType_Str); }
            set { RefType_Str = value.ToString("d"); }
        }

        [DataMember]
        public Condition IfCondition
        {
            get
            {
                if(_IfCondition == null && context != null && !context.IsDisposed)
                {
                    _IfCondition = context.Conditions.Where(a => a.Id == IfCndId).SingleOrDefault();
                    if(_IfCondition != null) _IfCondition.context = context;
                }
                return _IfCondition;
            }
            set
            {
                _IfCondition = value;
            }
        }
        [DataMember]
        public Condition ThenCondition
        {
            get
            {
                if (_ThenCondition == null && context != null && !context.IsDisposed)
                {
                    _ThenCondition = context.Conditions.Where(a => a.Id == ThenCndId).SingleOrDefault();
                    if (_ThenCondition != null) _ThenCondition.context = context;
                }
                return _ThenCondition;
            }
            set
            {
                _ThenCondition = value;
            }
        }
    }
}