using System;
using System.Linq;
using System.Runtime.Serialization;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class WorkflowRule:DomainObject
    {
        public DataContext context;
        private Condition _Condition;
        private WorkflowDiagramNode _SourceNode;
        private WorkflowDiagramNode _TargetNode;
        private WorkflowDiagrVersion _WorkflowDiagramVersion;

        [DataMember]
        public Condition Condition
        {
            get
            {
                if (_Condition == null && context != null && !context.IsDisposed)
                {
                    _Condition = context.Conditions.Where(a => a.Id == ConditionId).SingleOrDefault();
                    if(_Condition != null)
                        _Condition.context = context;
                }
                return _Condition;
            }
            set
            {
                _Condition = value;
                if (value != null)
                    ConditionId = value.Id;
            }
        }

        public WorkflowRuleTypeEnum Type
        {
            get
            {
                return (WorkflowRuleTypeEnum)Enum.Parse(typeof(WorkflowRuleTypeEnum), Type_Str);
            }
            set
            {
                Type_Str = value.ToString("d");
            }
        }

        public WorkflowDiagramNode SourceNode
        {
            get
            {
                if(_SourceNode == null && context != null && !context.IsDisposed)
                {
                    _SourceNode = context.WorkflowDiagramNodes.Where(a => a.Id == WorkflowDiagramNodeId).SingleOrDefault();
                    if (_SourceNode != null)
                        _SourceNode.context = context;
                }
                return _SourceNode;
            }
            set
            {
                _SourceNode = value;
                if (value != null)
                    WorkflowDiagramNodeId = value.Id;
            }
        }

        public WorkflowDiagramNode TransitionNode
        {
            get
            {
                if (_TargetNode == null && context != null && !context.IsDisposed)
                {
                    _TargetNode = context.WorkflowDiagramNodes.Where(a => a.Id == TargetWorkflowDiagramNodeId).SingleOrDefault();
                    if (_TargetNode != null)
                        _TargetNode.context = context;
                }
                return _TargetNode;
            }
            set
            {
                _TargetNode = value;
                if (value != null)
                    TargetWorkflowDiagramNodeId = value.Id;
            }
        }

        public EventType SourceEventType
        {
            get
            {
                if (SourceNode != null)
                    return SourceNode.EventType;
                return null;
            }
        }

        public EventType TransitionEventType
        {
            get
            {
                if (TransitionNode != null)
                    return TransitionNode.EventType;
                return null;
            }
        }

        public WorkflowDiagrVersion WorkflowDiagramVersion
        {
            get
            {
                if(_WorkflowDiagramVersion == null && context != null && !context.IsDisposed)
                {
                    _WorkflowDiagramVersion = context.WorkflowDiagrVersions.Where(a => a.Id == WorkflowDiagramVersionId).Single();
                    _WorkflowDiagramVersion.context = context;
                }
                return _WorkflowDiagramVersion;
            }
            set { _WorkflowDiagramVersion = value; }
        }

        #region Compiled Queries

        public static Func<DataContext, long?, IQueryable<WorkflowRule>> GetWorkflowRule =
            CompiledQuery.Compile(
                (DataContext dataContext, long? WorkflowRuleId) =>
                dataContext.WorkflowRules.Where(a => a.Id == WorkflowRuleId));

        public static Func<DataContext, long?, IQueryable<WorkflowRule>> WorkflowService_GetRuleForCase =
            CompiledQuery.Compile(
                (DataContext dataContext, long? WorkflowDiagramVersionId) =>
                from objWorkflowRule in dataContext.WorkflowRules
                from objWorkflowDiagramNode in dataContext.WorkflowDiagramNodes
                from objEventType in dataContext.EventTypes
                where objWorkflowRule.TargetWorkflowDiagramNodeId == objWorkflowDiagramNode.Id
                      && objWorkflowRule.WorkflowDiagramVersionId == WorkflowDiagramVersionId
                      && objWorkflowDiagramNode.EventTypeId == objEventType.Id
                      &&
                      objWorkflowRule.Type_Str == WorkflowRuleTypeEnum.Entry.ToString("d")
                orderby objWorkflowRule.Priority
                select objWorkflowRule);

        public static Func<DataContext, long?, IOrderedQueryable<WorkflowRule>> WorkflowService_GetTransitionRule =
            CompiledQuery.Compile(
                (DataContext dataContext, long? WorkflowDiagramNodeId) =>
                dataContext.WorkflowRules.Where(a => a.WorkflowDiagramNodeId == WorkflowDiagramNodeId).OrderBy(a => a.Priority));

        #endregion
    }
}