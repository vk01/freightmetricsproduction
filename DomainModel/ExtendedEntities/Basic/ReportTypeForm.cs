﻿using System.Collections.Generic;
using System.Linq;

namespace Exis.Domain
{
    public partial class ReportTypeForm:DomainObject
    {
        public DataContext context;
        private ReportType _ReportType;

        public ReportType ReportType
        {
            get
            {
                if (_ReportType == null && context != null && !context.IsDisposed)
                {
                    _ReportType = context.ReportTypes.Where(a => a.Id == ReportTypeId).Single();
                    _ReportType.context = context;
                }
                return _ReportType;
            }
            set { _ReportType = value; }
        }

        public static List<ReportTypeForm> GetByFormNameByArgumentsByByReportTypeIdClientId(DataContext context, string FormName, string Arguments, long ReportTypeId)
        {
           var temp = 
                context.ReportTypeForms.Where(
                    a => a.FormName == FormName && a.FormArguments == Arguments && a.ReportTypeId == ReportTypeId).ToList();

            foreach (var reportTypeForm in temp)
            {
                reportTypeForm.context = context;
            }

            return temp;
        }

        public static List<ReportTypeForm> GetAllByReportTypeIdByClientId(DataContext context, long ReportTypeId)
        {
            var temp = context.ReportTypeForms.Where(a => a.ReportTypeId == ReportTypeId).ToList();

            foreach (var reportTypeForm in temp)
            {
                reportTypeForm.context = context;
            }

            return temp;
        }
    }
}