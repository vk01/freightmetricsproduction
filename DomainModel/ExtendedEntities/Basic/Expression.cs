﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Devart.Data.Linq;
using Exis.RuleEngine;

namespace Exis.Domain
{
    public partial class Expression : DomainObject
    {
        public DataContext context;
        private AppFieldFunctionParameter _AppFieldParameter;

        public AppFieldFunctionParameter AppFieldParameter
        {
            get
            {
                if(_AppFieldParameter == null && context != null && !context.IsDisposed)
                {
                    _AppFieldParameter = FunctionEvaluator.ParseParameter(null, _Value);
                }
                return _AppFieldParameter;
            }
        }

        #region Compiled Queries

        public static Func<DataContext, long?, IQueryable<Expression>> GetExpression =
            CompiledQuery.Compile(
                (DataContext dataContext, long? ExpressionId) =>
                dataContext.Expressions.Where(a => a.Id == ExpressionId));

        #endregion
    }
}
