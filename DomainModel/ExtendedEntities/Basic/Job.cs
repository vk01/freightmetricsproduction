using System;
using System.Linq;

namespace Exis.Domain
{
    public partial class Job : DomainObject
    {
        public DataContext context;
        private Condition _Condition;

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum) Enum.Parse(typeof (ActivationStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        public Condition Condition
        {
            get
            {
                if(_Condition == null && context != null && !context.IsDisposed)
                {
                    _Condition = context.Conditions.Single(a => a.Id == ConditionId);
                    _Condition.context = context;
                }
                return _Condition;
            }
        }
        public override string ToString()
        {
            return Name;
        }
    }
}