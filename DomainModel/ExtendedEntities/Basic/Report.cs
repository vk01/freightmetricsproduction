﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exis.Domain
{
    public partial class Report : DomainObject
    {
        public DataContext context;
        private ReportType _ReportType;

        public ReportType ReportType
        {
            get
            {
                if (_ReportType == null && context != null && !context.IsDisposed)
                {
                    _ReportType = context.ReportTypes.Where(a => a.Id == ReportTypeId).Single();
                    _ReportType.context = context;
                }
                return _ReportType;
            }
            set { _ReportType = value; }
        }

        public override string ToString()
        {
            return Title;
        }
    }
}