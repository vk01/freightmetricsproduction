﻿using System.Linq;

namespace Exis.Domain
{
    public partial class WorkflowDiagramNode:DomainObject
    {
        public DataContext context;
        private EventType _EventType;

        public EventType EventType
        {
            get
            {
                if(_EventType == null && context != null && !context.IsDisposed)
                {
                    _EventType = context.EventTypes.Where(a => a.Id == _EventTypeId).SingleOrDefault();
                    if (_EventType != null)
                        _EventType.context = context;
                }
                return _EventType;
            }
            set
            {
                _EventType = value;
                if (value != null)
                {
                    EventTypeId = value.Id;
                }
            }
        }
    }
}
