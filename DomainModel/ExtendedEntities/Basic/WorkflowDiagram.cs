﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class WorkflowDiagram:DomainObject
    {
        public DataContext context;
        private CaseType _CaseType;
        private WorkflowDiagrVersion _ProductionVersion;

        [DataMember]
        public CaseType CaseType
        {
            get
            {
                if (_CaseType == null && context != null && !context.IsDisposed)
                {
                    _CaseType = context.CaseTypes.Where(a => a.Id == CaseTypeId).Single();
                    _CaseType.context = context;
                }
                return _CaseType;
            }
            set
            {
                _CaseType = value;
            }
        }

        public WorkflowDiagrVersion ProductionVersion
        {
            get
            {
                if(_ProductionVersion == null && context != null && !context.IsDisposed)
                {
                    _ProductionVersion =
                        context.WorkflowDiagrVersions.SingleOrDefault(
                            a =>
                            a.WorkflowDiagramId == _Id && a.Status_Str == "1" &&
                            a.DateFrom <= DateTime.Now && (a.DateTo == null || a.DateTo > DateTime.Now));
                    if (_ProductionVersion != null)
                        _ProductionVersion.context = context;
                }
                return _ProductionVersion;
            }
        }

        public ActivationStatusEnum Status
        {
            get { return ProductionVersion != null ? ActivationStatusEnum.Active : ActivationStatusEnum.Inactive; }
        }

        public static List<WorkflowDiagram> FindByCaseType(DataContext context, Int64 caseTypeId)
        {
            var temp = context.WorkflowDiagrams.Where(a => a.CaseTypeId == caseTypeId).OrderBy(a => a.Priority).ToList();
            for (int i = 0; i < temp.Count; i++)
            {
                temp[i].context = context;
            }

            return temp;
        }

        public static List<WorkflowDiagram> GetAllProductionVersion(DataContext context)
        {

            var temp = (from wfd in context.WorkflowDiagrams
                        from wfdv in context.WorkflowDiagrVersions
                        where wfd.Id == wfdv.Id &&
                              wfdv.Status_Str == "1"
                        select wfd).ToList();

            foreach (var wfdWorkflowDiagrams in temp)
            {
                wfdWorkflowDiagrams.context = context;
            }
        
            return temp;

      }
    }
}
