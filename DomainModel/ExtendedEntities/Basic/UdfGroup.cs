﻿using System;
using System.Linq;

namespace Exis.Domain
{
    public partial class Udfgroup:DomainObject
    {
        public DataContext context;
        private Udfgroup _ParentGroup;

        public UDFRefTypeEnum UdfType
        {
            get { return (UDFRefTypeEnum)Enum.Parse(typeof(UDFRefTypeEnum), UdfType_Str); }
            set { UdfType_Str = value.ToString("d"); }
        }

        public UdfGroupTypeEnum Type
        {
            get { return (UdfGroupTypeEnum) Enum.Parse(typeof (UdfGroupTypeEnum), Type_Str); }
            set { Type_Str = value.ToString("d"); }
        }

        public UdfGroupSubTypeEnum SubType
        {
            get { return (UdfGroupSubTypeEnum) Enum.Parse(typeof (UdfGroupSubTypeEnum), SubType_Str); }
            set { SubType_Str = value.ToString("d"); }
        }

        public Udfgroup ParentGroup
        {
            get
            {
                if(_ParentGroup == null && ParentUdfGroupId != null && context != null && !context.IsDisposed)
                {
                    _ParentGroup = context.Udfgroups.Single(a => a.Id == ParentUdfGroupId);
                    _ParentGroup.context = context;
                }
                return _ParentGroup;
            }
            set
            {
                _ParentGroup = value;
                if(value != null)
                {
                    ParentUdfGroupId = value.Id;
                }
            }
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}