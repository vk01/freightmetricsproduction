﻿using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Devart.Data.Linq.Mapping;
using Exis.SessionRegistry;

namespace Exis.Domain
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Company))]
    [KnownType(typeof(Market))]
    [KnownType(typeof(Trader))]
    [KnownType(typeof(Vessel))]
    [KnownType(typeof(VesselPool))]
    [KnownType(typeof(VesselPoolInfo))]
    [KnownType(typeof(PoolInfoVessel))]
    [KnownType(typeof(Book))]
    [KnownType(typeof(Account))]
    [KnownType(typeof(ProfitCentre))]
    [KnownType(typeof(Desk))]
    [KnownType(typeof(ClearingHouse))]
    [KnownType(typeof(CompanySubtype))]
    [KnownType(typeof(Index))]
    [KnownType(typeof(IndexSpotValue))]
    [KnownType(typeof(IndexFFAValue))]
    [KnownType(typeof(IndexCustomValue))]
    [KnownType(typeof(User))]
    [KnownType(typeof(Contact))]
    [KnownType(typeof(Loan))]
    [KnownType(typeof(CashFlowModel))]
    [KnownType(typeof(CashFlowGroup))]
    [KnownType(typeof(CashFlowItem))]
    [KnownType(typeof(InterestReferenceRate))]
    [KnownType(typeof(ExchangeRate))]
    [KnownType(typeof(AppParameter))]
    public abstract class DomainObject
    {
        [XmlIgnore]
        public bool isNew { get; set; }

        [XmlIgnore]
        public bool isDirty { get; set; }

        [XmlIgnore]
        public bool isRemoved { get; set; }

        public static void InitializeDataContext()
        {
            Stream contextStream = File.OpenRead("DataContext.lqml");
            ServerSessionRegistry.MappingSource =
                XmlMappingSource.FromStream(contextStream);
            contextStream.Close();
        }
    }
}