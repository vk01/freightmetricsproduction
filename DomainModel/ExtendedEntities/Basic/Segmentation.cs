﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Serialization;
using Exis.RuleEngine;
using Exis.SessionRegistry;

namespace Exis.Domain
{
    public partial class Segmentation:DomainObject
    {
        public DataContext context;
        private Condition _Condition;

        public SegmentationTypeEnum Type
        {
            get { return (SegmentationTypeEnum) Enum.Parse(typeof (SegmentationTypeEnum), _Type_Str); }
            set { Type_Str = value.ToString("d"); }
        }

        public Condition Condition
        {
            get
            {
                if (_Condition == null && context != null && !context.IsDisposed)
                {
                    _Condition = context.Conditions.Single(a => a.Id == ConditionId);
                    _Condition.context = context;
                }
                return _Condition;
            }
            set
            {
                _Condition = value;
                if (value != null)
                {
                    ConditionId = value.Id;
                }
            }
        }

        #region Common IQueryable build methods

        private Type GetUdfType(Udf udf)
        {
            switch (udf.Type)
            {
                case UDFTypeEnum.Date:
                    return typeof (DateTime);
                case UDFTypeEnum.Numeric:
                    return typeof (Decimal);
                case UDFTypeEnum.Status:
                    return typeof (Boolean);
                case UDFTypeEnum.Image:
                case UDFTypeEnum.File:
                    return typeof (byte[]);
                case UDFTypeEnum.InternalLink:
                    return typeof (DomainObject);
                default:
                    return typeof (String);
            }
        }

        private void GetExpressionParameters(Condition condition, object runtimeObject, out Udf udf, out object objValue,
                                             out CompareOperator op, out bool checkEmpty, out bool negate)
        {
            string strExpressionValue = condition.Expression.Value;
            AppFieldFunctionParameter expression = FunctionEvaluator.ParseParameter(null, strExpressionValue);
            udf = null;
            objValue = null;
            op = CompareOperator.Null;
            checkEmpty = false;
            negate = false;

            if (expression.ParameterType == AppFieldParameterTypeEnum.Function)
            {
                AppFieldFunctionParameter function = expression;
                if (function.Function.Name == "Not")
                {
                    negate = true;
                }
                while (function.FunctionParameters[0].ParameterType == AppFieldParameterTypeEnum.Function)
                {
                    function = function.FunctionParameters[0];
                }
                if (function.FunctionParameters[0].ParameterType == AppFieldParameterTypeEnum.ApplicationField)
                {
                    string appField = function.FunctionParameters[0].AppField;
                    int startIndex = appField.LastIndexOf('(') + 1;
                    int length = appField.LastIndexOf(')') - startIndex;
                    long udfId = Convert.ToInt64(appField.Substring(startIndex, length));
                    udf = context.Udfs.Single(a => a.Id == udfId);
                }
                if (expression.FunctionParameters.Count == 3)
                {
                    if (expression.FunctionParameters[2].ParameterType == AppFieldParameterTypeEnum.LiteralValue)
                    {
                        expression.FunctionParameters[2].Type = GetUdfType(udf);
                    }
                    objValue = FunctionEvaluator.Evaluate(expression.FunctionParameters[2], runtimeObject);
                    if (objValue == null)
                    {
                        throw new ApplicationException("Could not evaluate");
                    }
                    expression.FunctionParameters[1].Type = typeof (CompareOperator);
                    op = (CompareOperator) FunctionEvaluator.Evaluate(expression.FunctionParameters[1], runtimeObject);
                }
                else
                {
                    checkEmpty = true;
                }
            }
        }

        private Expression<Func<UdfsValue, bool>> ProcessPredicateIsEmpty(long udfId)
        {
            return b => b.UdfId == udfId && b.Value == null;
        }

        private Expression<Func<UdfsValue, bool>> ProcessPredicate(long udfId, CompareOperator op, object objValue)
        {
            BinaryExpression binaryExpression;
            ParameterExpression parameter = System.Linq.Expressions.Expression.Parameter(typeof (UdfsValue), "b");

            System.Linq.Expressions.Expression convertedExpression =
                System.Linq.Expressions.Expression.Property(parameter, "Value");

            if (objValue is DateTime || objValue is Decimal)
            {
                convertedExpression = System.Linq.Expressions.Expression.Call(
                    convertedExpression,
                    typeof (String).GetMethod("Substring", new[] {typeof (int), typeof (int)}),
                    System.Linq.Expressions.Expression.Constant(0, typeof (int)),
                    System.Linq.Expressions.Expression.Constant(4000, typeof (int)));
                if (objValue is DateTime)
                {
                    ConstantExpression culture =
                        System.Linq.Expressions.Expression.Constant(ServerSessionRegistry.ServerCultureInfo);
                    convertedExpression =
                        System.Linq.Expressions.Expression.Call(
                            typeof (Convert).GetMethod("ToDateTime", new[] {typeof (String), typeof (IFormatProvider)}),
                            convertedExpression, culture);
                }
                else if (objValue is Decimal)
                {
                    convertedExpression =
                        System.Linq.Expressions.Expression.Call(
                            typeof (Convert).GetMethod("ToDecimal", new[] {typeof (String)}), convertedExpression);
                }
            }
            else if (objValue is Boolean)
            {
                objValue = ((bool) objValue) ? "1" : "0";
            }
            else if (objValue is DomainObject)
            {
                var serializer = new XmlSerializer(objValue.GetType());
                var stream = new MemoryStream();
                serializer.Serialize(stream, objValue);
                objValue = Encoding.UTF8.GetString(stream.ToArray());
            }

            Expression<Func<UdfsValue, bool>> predicate = b => b.UdfId == udfId;
            if (op == CompareOperator.GreaterThan)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.GreaterThan(convertedExpression,
                                                                   System.Linq.Expressions.Expression.
                                                                       Constant(objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.GreaterThanOrEqual)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.GreaterThanOrEqual(convertedExpression,
                                                                          System.Linq.Expressions.Expression
                                                                              .Constant(objValue,
                                                                                        objValue.GetType()));
            }
            else if (op == CompareOperator.Equals)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.Equal(convertedExpression,
                                                             System.Linq.Expressions.Expression.Constant(
                                                                 objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.NotEquals)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.NotEqual(convertedExpression,
                                                                System.Linq.Expressions.Expression.Constant(
                                                                    objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.LessThan)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.LessThan(convertedExpression,
                                                                System.Linq.Expressions.Expression.Constant(
                                                                    objValue, objValue.GetType()));
            }
            else if (op == CompareOperator.LessThanOrEqual)
            {
                binaryExpression =
                    System.Linq.Expressions.Expression.LessThanOrEqual(convertedExpression,
                                                                       System.Linq.Expressions.Expression.
                                                                           Constant(objValue,
                                                                                    objValue.GetType()));
            }
            else
            {
                throw new ApplicationException("Cannot evaluate comparison operator Null");
            }

            predicate =
                predicate.And(
                    System.Linq.Expressions.Expression.Lambda<Func<UdfsValue, bool>>(binaryExpression,
                                                                                     parameter));
            return predicate;
        }

        #endregion
    }
}