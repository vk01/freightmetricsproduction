using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Devart.Data.Linq;
using Exis.Domain.Attributes;

namespace Exis.Domain
{
    public partial class Case : DomainObject
    {
        public DataContext context;

        private Appointment _Appointment;
        private List<Appointment> _Appointments;
        private CaseType _CaseType;
        private Event _CurrentEvent;
        private Case _ParentCase;
        private List<Case> _Subcases;
        private CaseType _Type;
        private List<UdfsValue> _UdfValues;
        private List<Udf> _Udfs;

        [DataMember]
        [AppFieldLookupValue("Case Type", "This case's type.",
            "ActiveCaseTypes,Exis.Domain.SystemData,Exis.DomainModel", "")]
        public CaseType Type
        {
            get
            {
                if (_Type == null && context != null && !context.IsDisposed)
                {
                    _Type = context.CaseTypes.Where(a => a.Id == CaseTypeId).Single();
                    _Type.context = context;
                }
                return _Type;
            }
            set { _Type = value; }
        }

        [AppFieldPropertyDomain("Current Event", "The Current Event.", new[] {"Event Case"})]
        [DataMember]
        public Event CurrentEvent
        {
            get
            {
                if (_CurrentEvent == null && context != null && !context.IsDisposed)
                {
                    _CurrentEvent =
                        context.Events.Where(a => a.CaseId == Id).OrderByDescending(a => a.Id).FirstOrDefault();
                    if (_CurrentEvent != null) _CurrentEvent.context = context;
                }
                return _CurrentEvent;
            }
            set { _CurrentEvent = value; }
        }

        public List<Udf> Udfs
        {
            get
            {
                if (_Udfs == null && context != null && !context.IsDisposed)
                {
                    _Udfs =
                        context.Udfs.Where(
                            a =>
                            a.RefType_Str == UDFRefTypeEnum.CaseType.ToString("d") &&
                            (a.RefTypeId == Type.Id || a.RefTypeId == null)).
                            ToList();
                    foreach (Udf udf in _Udfs)
                    {
                        udf.context = context;
                    }
                }
                return _Udfs;
            }
            set { _Udfs = value; }
        }

        [AppFieldPropertyDomain("ParentCase", "The Case's parent case.")]
        public Case ParentCase
        {
            get
            {
                if (_ParentCase == null && context != null && !context.IsDisposed)
                {
                    _ParentCase = context.Cases.Where(a => a.Id == ParentCaseId).SingleOrDefault();
                    if (_ParentCase != null) _ParentCase.context = context;
                }
                return _ParentCase;
            }
            set { _ParentCase = value; }
        }

        [DataMember]
        public List<UdfsValue> UdfValues
        {
            get
            {
                if (_UdfValues == null && context != null && !context.IsDisposed)
                {
                    _UdfValues = (from udfValue in context.UdfsValues
                                  from udf in context.Udfs
                                  where udf.Id == udfValue.UdfId
                                        && udf.RefType_Str == UDFRefTypeEnum.CaseType.ToString("d")
                                        && udfValue.RefId == Id
                                        && (udf.RefTypeId == Type.Id || udf.RefTypeId == null)
                                  select udfValue).ToList();

                    foreach (UdfsValue udfValue in _UdfValues)
                    {
                        udfValue.context = context;
                    }
                }
                return _UdfValues;
            }
            set { _UdfValues = value; }
        }

        [AppFieldPropertyDomain("Appointments", "Case's Appointments.")]
        public List<Appointment> Appointments
        {
            get
            {
                if (_Appointments == null && context != null && !context.IsDisposed)
                {
                    _Appointments = (from objAppointment in context.Appointments
                                     where objAppointment.RefId == Id
                                           && objAppointment.RefType_Str == AppointmentRefTypeEnum.Case.ToString("d")
                                     select objAppointment).ToList();
                    foreach (Appointment appointment in _Appointments)
                    {
                        appointment.context = context;
                    }
                }
                return _Appointments;
            }
            set { _Appointments = value; }
        }

        [AppFieldPropertyDomain("Current Appointment", "The Current Appointment of the case.")]
        public Appointment CurrentAppointment
        {
            get { return Appointments.Where(a => a.Datt == null).SingleOrDefault(); }
        }

        [DataMember]
        public Appointment Appointment
        {
            get
            {
                if (_Appointment == null && context != null && !context.IsDisposed)
                {
                    _Appointment =
                        context.Appointments.Where(a => a.RefId == Id && a.RefType == AppointmentRefTypeEnum.Case).
                            Single();
                    _Appointment.context = context;
                }
                return _Appointment;
            }
            set { _Appointment = value; }
        }

        [AppFieldPropertyValue("Status", "Status")]
        public CaseEventStatusEnum Status
        {
            get { return (CaseEventStatusEnum) Enum.Parse(typeof (CaseEventStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        public CaseActionStatusEnum ActStatus
        {
            get { return (CaseActionStatusEnum) Enum.Parse(typeof (CaseActionStatusEnum), ActionStatus_Str); }
            set { ActionStatus_Str = value.ToString("d"); }
        }

        [AppFieldPropertyValue("Priority", "Priority")]
        public CaseEventPriorityEnum Priority
        {
            get { return (CaseEventPriorityEnum) Enum.Parse(typeof (CaseEventPriorityEnum), Priority_Str); }
            set { Priority_Str = value.ToString("d"); }
        }

        [AppFieldPropertyValue("Result", "Result")]
        public CaseEventResultEnum? Result
        {
            get
            {
                if (Result_Str == null)
                    return null;
                return (CaseEventResultEnum) Enum.Parse(typeof (CaseEventResultEnum), Result_Str);
            }
            set { Result_Str = value == null ? null : value.Value.ToString("d"); }
        }

        [AppFieldPropertyDomain("Subcases", "This case's subcases")]
        public List<Case> Subcases
        {
            get
            {
                if (_Subcases == null && context != null && !context.IsDisposed)
                {
                    _Subcases = context.Cases.Where(a => a.ParentCaseId == _Id).ToList();
                    foreach (Case subcase in _Subcases)
                    {
                        subcase.context = context;
                    }
                }
                return _Subcases;
            }
            set { _Subcases = value; }
        }

        public User LockUser
        {
            get
            {
                IQueryable<User> temp = from us in context.Users
                                        from lo in context.Locks
                                        where
                                            us.Id == lo.UserId && lo.ObjType_Str == LockedObjectTypeEnum.Case.ToString("d") &&
                                            lo.ObjId == Id
                                        select us;
                return temp.FirstOrDefault();
            }
        }

        public CaseType CaseType
        {
            get
            {
                if (_CaseType == null)
                {
                    _CaseType = context.CaseTypes.Where(a => a.Id == CaseTypeId).FirstOrDefault();

                    if (_CaseType != null)
                    {
                        _CaseType.context = context;
                    }
                }

                return _CaseType;
            }
        }

        public ScopeEnum Scope
        {
            get { return CaseType.Scope; }
        }

        [AppFieldMethodUDF("Udfs", "Udfs", UDFRefTypeEnum.CaseType)]
        public ExtendedList<UdfsValue> GetUdfValue(long udfId)
        {
            return new ExtendedList<UdfsValue>(new object[] {udfId}, GetUdfValues, SetUdfValues);
        }

        private object GetUdfValues(object[] parameters)
        {
            Udf udf;
            try
            {
                udf = context.Udfs.Single(a => a.Id == (long) parameters[0]);
            }
            catch
            {
                throw new UdfNotFoundException((long) parameters[0]);
            }

            List<UdfsValue> udfValues;
            if (_UdfValues != null)
            {
                udfValues = _UdfValues.Where(a => a.UdfId == udf.Id).ToList();
            }
            else
            {
                udfValues = new List<UdfsValue>();
            }

            return Udf.GetUdfValue(this, udfValues, context, Id, udf);
        }

        private void SetUdfValues(object[] parameters, object values)
        {
            Udf.SetUdfValue(context, UDFRefTypeEnum.CaseType, Id, Type.Id, (long) parameters[0], values);
        }

        public static List<Case> GetByCustomerId(DataContext context, Int64 customerId)
        {
            List<Case> temp = context.Cases.Where(a => a.CustomerId == customerId).ToList();
            for (int i = 0; i < temp.Count; i++)
            {
                temp[i].context = context;
            }

            return temp;
        }

        public static List<Case> GetAllOpenAirtimeCases(DataContext context, long caseTypeId, long startCaseId)
        {
            return context.Cases.Where(a => a.Id >= startCaseId && a.Status_Str == "0" && a.CaseTypeId == caseTypeId).ToList();
        }

        public static List<Case> GetAllCompletedAndFailedByCaseTypeId(DataContext context, long caseTypeId,
                                                                      long startCaseId)
        {
            return context.Cases.Where(a =>
                                       a.Id >= startCaseId &&
                                       a.Status_Str == "1" &&
                                       a.CaseTypeId == caseTypeId &&
                                       (
                                           a.Result_Str == "0" ||
                                           a.Result_Str == "2"
                                       )
                ).ToList();
        }

        #region Compiled Queries

        public static Func<DataContext, long, IQueryable<Case>> WorkflowService_CheckSubcaseCompletion =
            CompiledQuery.Compile(
                (DataContext dataContext, long CaseId) =>
                dataContext.Cases.Where(a => a.ParentCaseId == CaseId));

        public static Func<DataContext, long, IQueryable<Case>> WorkflowService_ProcessCaseInt =
            CompiledQuery.Compile(
                (DataContext dataContext, long CaseId) =>
                dataContext.Cases.Where(a => a.Id == CaseId));

        #endregion
    }
}