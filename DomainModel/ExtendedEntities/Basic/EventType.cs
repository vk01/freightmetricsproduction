using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class EventType : DomainObject
    {
        public DataContext context;

        private List<Udf> _Udfs;
        private List<EventTypeAction> _Actions;

        public EventTypeProcessEnum ProcessType
        {
            get { return (EventTypeProcessEnum)Enum.Parse(typeof(EventTypeProcessEnum), ProcessType_Str); }
            set { ProcessType_Str = value.ToString("d"); }
        }

        public ScopeEnum Scope
        {
            get { return (ScopeEnum)Enum.Parse(typeof(ScopeEnum), Scope_Str); }
            set { Scope_Str = value.ToString("d"); }
        }

        public ActivationStatusEnum Status
        {
            get { return (ActivationStatusEnum)Enum.Parse(typeof(ActivationStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        public CaseEventPriorityEnum Priority
        {
            get { return (CaseEventPriorityEnum)Enum.Parse(typeof(CaseEventPriorityEnum), Priority_Str); }
            set { Priority_Str = value.ToString("d"); }
        }

        [DataMember]
        public List<Udf> Udfs
        {
            get
            {
                if (_Udfs == null && context != null && !context.IsDisposed)
                {
                    _Udfs =
                        context.Udfs.Where(
                            a =>
                            a.RefType_Str == UDFRefTypeEnum.EventType.ToString("d") &&
                            (a.RefTypeId == Id || a.RefTypeId == null)).
                            ToList();
                    foreach (Udf udf in _Udfs)
                    {
                        udf.context = context;
                    }
                }
                return _Udfs;
            }
            set { _Udfs = value; }
        }

        public List<EventTypeAction> Actions
        {
            get
            {
                if (_Actions == null && context != null && !context.IsDisposed)
                {
                    _Actions = context.EventTypeActions.Where(a => a.EventTypeId == _Id).ToList();
                    foreach (EventTypeAction eventTypeAction in _Actions)
                    {
                        eventTypeAction.context = context;
                    }
                }
                return _Actions;
            }
        }

        #region Compiled Queries

        public static Func<DataContext, long, IQueryable<EventType>> WorkflowService_ProcessWorkflow =
            CompiledQuery.Compile(
                (DataContext dataContext, long WorkflowRuleId) =>
                from eventType in dataContext.EventTypes
                from workflowRule in dataContext.WorkflowRules
                from workflowNode in dataContext.WorkflowDiagramNodes
                where workflowRule.TargetWorkflowDiagramNodeId == workflowNode.Id
                      && workflowNode.EventTypeId == eventType.Id
                      && workflowRule.Id == WorkflowRuleId
                select eventType);

        #endregion

        public override string ToString()
        {
            return Description;
        }
    }
}