﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class RegularExpression : DomainObject
    {
        public DataContext context;
        #region Compiled Queries

        public static Func<DataContext, IQueryable<RegularExpression>> ClientsService_GetAll =
            CompiledQuery.Compile(
                (DataContext dataContext) =>
                dataContext.RegularExpressions);

        #endregion
    }
}