using System;
using System.Collections.Generic;
using System.Linq;
using Devart.Data.Linq;

namespace Exis.Domain
{
    public partial class EventTypeAction : DomainObject
    {
        public DataContext context;
        private Function _Function;
        private List<ParameterMapping> _ParameterMappings;

        public Function Function
        {
            get
            {
                if (_Function == null && context != null && !context.IsDisposed)
                {
                    _Function = context.Functions.Where(a => a.Id == FunctionId).Single();
                    _Function.context = context;
                }
                return _Function;
            }
            set { _Function = value; }
        }

        public EventTypeActionTypeEnum Type
        {
            get
            {
                if (!String.IsNullOrEmpty(Type_Str))
                    return (EventTypeActionTypeEnum) Enum.Parse(typeof (EventTypeActionTypeEnum), Type_Str);
                return EventTypeActionTypeEnum.Null;
            }
            set { Type_Str = value.ToString("d"); }
        }

        public List<ParameterMapping> ParameterMappings
        {
            get
            {
                if (_ParameterMappings == null && context != null && !context.IsDisposed)
                {
                    _ParameterMappings = context.ParameterMappings.Where(a => a.EventTypeActionId == _Id).ToList();
                    foreach (ParameterMapping parameterMapping in _ParameterMappings)
                    {
                        parameterMapping.context = context;
                    }
                }
                return _ParameterMappings;
            }
            set { _ParameterMappings = value; }
        }

        #region Compiled Queries

        public static Func<DataContext, long, string, IOrderedQueryable<EventTypeAction>>
            WorkflowService_ExecuteOpenCloseActions =
                CompiledQuery.Compile(
                    (DataContext dataContext, long EventTypeId, string EventTypeActionType) =>
                    dataContext.EventTypeActions.Where(
                        a => a.EventTypeId == EventTypeId && a.Type_Str == EventTypeActionType).OrderBy(a => a.Sort));

        #endregion
    }
}