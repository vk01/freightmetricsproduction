using System;
using System.Collections.Generic;
using System.Linq;
using Devart.Data.Linq;
using Exis.Domain.Attributes;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class ReportTypesGroupsAssoc:DomainObject
    {
        public DataContext context;
        private ReportType _ReportType;
        private ReportTypeGroup _ReportTypeGroup;

        public ReportType ReportType
        {
            get
            {
                if (_ReportType == null && context != null && !context.IsDisposed)
                {
                    _ReportType = context.ReportTypes.Where(a => a.Id == this.RptpId).Single();
                    _ReportType.context = context;
                    
                }
                return _ReportType;
            }
            set
            {
                _ReportType = value;
            }
        }

        public ReportTypeGroup ReportTypeGroup
        {
            get
            {
                if (_ReportTypeGroup == null &&  context != null && !context.IsDisposed)
                {
                    _ReportTypeGroup = context.ReportTypeGroups.Where(a => a.Id == this.RptpgId).Single();
                    _ReportTypeGroup.context = context;

                }
                return _ReportTypeGroup;
            }
            set
            {
                _ReportTypeGroup = value;
            }
        }

        public static ReportTypesGroupsAssoc GetByReportTypeIdByReportTypeGroupId(DataContext context, long ReportTypeId, long ReportTypeGroupId)
        {
            var temp =
                context.ReportTypesGroupsAssocs.Where(a => a.RptpgId == ReportTypeGroupId && a.RptpId == ReportTypeId).
                    ToList();

            if (temp.Count > 0)
            {
                ((ReportTypesGroupsAssoc) temp[0]).context = context;
                return (ReportTypesGroupsAssoc)temp[0];
            }
           

            return null;
        }
        
    }
}