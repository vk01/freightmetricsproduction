using System;
using System.Collections.Generic;
using System.Linq;
using Devart.Data.Linq;
using Exis.Domain.Attributes;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class Event : DomainObject
    {
        public DataContext context;

        private Appointment _Appointment;
        private Case _Case;
        private EventType _Type;
        private User _LockUser;
        private Event _PreviousEvent;

        private List<UdfsValue> _UdfValues;
        private WorkflowRule _WorkflowRule;

        public WorkflowRule WorkflowRule
        {
            get
            {
                if (_WorkflowRule == null && context != null && !context.IsDisposed)
                {
                    _WorkflowRule = WorkflowRule.GetWorkflowRule(context, WorkflowRuleId).SingleOrDefault();
                    if (_WorkflowRule != null)
                        _WorkflowRule.context = context;
                }
                return _WorkflowRule;
            }
            set { _WorkflowRule = value; }
        }

        [AppFieldPropertyDomain("Previous Event", "Gets the previous event in this event's case")]
        public Event PreviousEvent
        {
            get
            {
                if (_PreviousEvent == null && context != null && !context.IsDisposed)
                {
                    _PreviousEvent = context.Events.Where(a => a.CaseId == CaseId && a.Id < _Id).OrderByDescending(a => a.Id).FirstOrDefault();
                    if(_PreviousEvent != null)
                    {
                        _PreviousEvent.context = context;
                    }
                }
                return _PreviousEvent;
            }
            set
            {
                _PreviousEvent = value;
            }
        }

        [AppFieldPropertyDomain("Event Case", "The Case of Event.", new[] {"Current Event"})]
        [DataMember]
        public Case Case
        {
            get
            {
                if (_Case == null && context != null && !context.IsDisposed)
                {
                    _Case = context.Cases.Where(a => a.Id == CaseId).Single();
                    _Case.context = context;
                }
                return _Case;
            }
            set { _Case = value; }
        }

        [DataMember]
        public EventType Type
        {
            get
            {
                if (_Type == null && context != null && !context.IsDisposed)
                {
                    _Type = context.EventTypes.Where(a => a.Id == EventTypeId).Single();
                    _Type.context = context;
                }
                return _Type;
            }
            set { _Type = value; }
        }

        [AppFieldPropertyValue("Status", "Status")]
        public CaseEventStatusEnum Status
        {
            get { return (CaseEventStatusEnum) Enum.Parse(typeof (CaseEventStatusEnum), Status_Str); }
            set { Status_Str = value.ToString("d"); }
        }

        public EventActionStatusEnum ActStatus
        {
            get { return (EventActionStatusEnum) Enum.Parse(typeof (EventActionStatusEnum), ActionStatus_Str); }
            set { ActionStatus_Str = value.ToString("d"); }
        }

        [AppFieldPropertyValue("Result", "Result")]
        public CaseEventResultEnum? Result
        {
            get
            {
                return Result_Str == null
                           ? null
                           : (CaseEventResultEnum?) Enum.Parse(typeof (CaseEventResultEnum), Result_Str);
            }
            set { Result_Str = value == null ? null : value.Value.ToString("d"); }
        }

        [AppFieldPropertyValue("Priority", "Priority")]
        public CaseEventPriorityEnum Priority
        {
            get { return (CaseEventPriorityEnum) Enum.Parse(typeof (CaseEventPriorityEnum), Priority_Str); }
            set { Priority_Str = value.ToString("d"); }
        }

        [AppFieldPropertyDomain("Appointment", "Appointment")]
        [DataMember]
        public Appointment Appointment
        {
            get
            {
                if (_Appointment == null && context != null && !context.IsDisposed)
                {
                    _Appointment =
                        context.Appointments.Where(a => a.RefId == Id && a.RefType_Str == AppointmentRefTypeEnum.Event.ToString("d")).
                            Single();
                    _Appointment.context = context;
                }
                return _Appointment;
            }
            set { _Appointment = value; }
        }

        [DataMember]
        public List<UdfsValue> UdfValues
        {
            get
            {
                if (_UdfValues == null && context != null && !context.IsDisposed)
                {
                    _UdfValues = (from udfValue in context.UdfsValues
                                  from udf in context.Udfs
                                  where udf.Id == udfValue.UdfId
                                        && udf.RefType_Str == UDFRefTypeEnum.EventType.ToString("d")
                                        && udfValue.RefId == Id
                                        && (udf.RefTypeId == Type.Id || udf.RefTypeId == null)
                                  select udfValue).ToList();

                    foreach (UdfsValue udfValue in _UdfValues)
                    {
                        udfValue.context = context;
                    }
                }
                return _UdfValues;
            }
            set { _UdfValues = value; }
        }

        public EventType EventType
        {
            get
            {
                return context.EventTypes.Where(a => a.Id == EventTypeId).FirstOrDefault();
            }
        }

        public bool HasAppointment
        {
            get
            {
                return Appointment != null;
            }
        }

        [DataMember]
        public User LockUser
        {
            get
            {
                if (_LockUser == null && context != null && !context.IsDisposed)
                {
                    _LockUser = (from objUser in context.Users
                                 from objLock in context.Locks
                                 where objUser.Id == objLock.UserId &&
                                       objLock.ObjType_Str == LockedObjectTypeEnum.Event.ToString("d") &&
                                       objLock.ObjId == Id
                                 select objUser).SingleOrDefault();

                    if(_LockUser != null)
                    {
                       // _LockUser.context = context;
                    }
                }
                return _LockUser;
            }
            set
            {
                _LockUser = value;
            }
        }

        [AppFieldMethodUDF("Udfs", "Udfs", UDFRefTypeEnum.EventType)]
        public ExtendedList<UdfsValue> GetUdfValue(long udfId)
        {
            return new ExtendedList<UdfsValue>(new object[] {udfId}, GetUdfValues, SetUdfValues);
        }

        private object GetUdfValues(object[] parameters)
        {
            Udf udf;
            try
            {
                udf = context.Udfs.Single(a => a.Id == (long)parameters[0]);
            }
            catch
            {
                throw new UdfNotFoundException((long) parameters[0]);
            }

            List<UdfsValue> udfValues;
            if (_UdfValues != null)
            {
                udfValues = _UdfValues.Where(a => a.UdfId == udf.Id).ToList();
            }
            else
            {
                udfValues = new List<UdfsValue>();
            }
            return Udf.GetUdfValue(this, udfValues, context, Id, udf);
        }

        private void SetUdfValues(object[] parameters, object values)
        {
            Udf.SetUdfValue(context, UDFRefTypeEnum.EventType, Id, Type.Id, (long) parameters[0], values);
        }

        public static List<Event> GetByCaseId(DataContext context, Int64 caseId)
        {
            var temp = context.Events.Where(a => a.CaseId == caseId).ToList();
            for (int i = 0; i < temp.Count; i++)
            {
                temp[i].context = context;
            }

            return temp;
        }

        #region Compiled Queries

        #endregion
    }
}