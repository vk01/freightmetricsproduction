﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Devart.Data.Linq;
using Exis.SessionRegistry;

namespace Exis.Domain
{
    [KnownType(typeof (List<string>))]
    [KnownType(typeof (DateTime))]
    [KnownType(typeof (List<DateTime>))]
    [KnownType(typeof (decimal))]
    [KnownType(typeof (List<decimal>))]
    [KnownType(typeof (byte[]))]
    [KnownType(typeof (List<byte[]>))]
    [KnownType(typeof (Case))]
    [KnownType(typeof (List<Case>))]
    [KnownType(typeof (Event))]
    [KnownType(typeof (List<Event>))]
    public partial class UdfsValue:DomainObject
    {
        public DataContext context;
        private object _TypedValue;
        private Udf _Udf;

        [DataMember]
        public Udf Udf
        {
            get
            {
                if (_Udf == null && context != null && !context.IsDisposed)
                {
                    _Udf = (from objUdf in context.Udfs
                            where objUdf.Id == UdfId
                            select objUdf).Single();

                    _Udf.context = context;
                }
                return _Udf;
            }
            set { _Udf = value; }
        }

        [DataMember]
        public object TypedValue
        {
            get
            {
                if (_TypedValue == null && context != null && !context.IsDisposed)
                {
                    _TypedValue = _Udf.GetTypedValue(Value);
                }
                return _TypedValue;
            }
            set { _TypedValue = value; }
        }

        //TO BE REMOVED - REPLACED BY SetUnTypedValue
        public string GetUnTypedValue(UDFTypeEnum type, object value)
        {
            if (type == UDFTypeEnum.File || type == UDFTypeEnum.Image)
            {
                return Convert.ToBase64String(((byte[]) value));
            }
            else if (type == UDFTypeEnum.Status)
            {
                return value.ToString().Equals("True") ? "1" : "0";
            }
            else if (type == UDFTypeEnum.InternalLink)
            {
                var xmlSerializer = new XmlSerializer(value.GetType());
                var stream = new MemoryStream();
                xmlSerializer.Serialize(stream, value);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
            else
            {
                return value.ToString();
            }
        }

        public void SetUnTypedValue()
        {
            UDFTypeEnum type = Udf.Type;

            if ((type == UDFTypeEnum.File || type == UDFTypeEnum.Image) && _TypedValue.GetType() == typeof (byte[]))
            {
                _Value = Convert.ToBase64String(((byte[]) _TypedValue));
            }
            else if (type == UDFTypeEnum.Status)
            {
                if (_TypedValue.ToString().ToLower().Equals("true") || _TypedValue.ToString().Equals("1"))
                    _Value = "1";
                else
                    _Value = "0";
            }
            else if (type == UDFTypeEnum.InternalLink && _TypedValue.GetType() != typeof (string))
            {
                var xmlSerializer = new XmlSerializer(_TypedValue.GetType());
                var stream = new MemoryStream();
                xmlSerializer.Serialize(stream, _TypedValue);
                _Value = Encoding.UTF8.GetString(stream.ToArray());
            }
            else if (type == UDFTypeEnum.Date)
            {
                if (_TypedValue.GetType() == typeof (DateTime))
                {
                    _Value = ((DateTime) _TypedValue).ToString(ServerSessionRegistry.ServerCultureInfo);
                }
                else if (_TypedValue.GetType() == typeof (string))
                {
                    DateTime dt = Convert.ToDateTime(_TypedValue, ServerSessionRegistry.ServerCultureInfo);
                    _Value = dt.ToString(ServerSessionRegistry.ServerCultureInfo);
                }
            }
            else
            {
                _Value = _TypedValue.ToString();
            }
        }

        #region Compiled Queries

        public static Func<DataContext, long, long, string, IQueryable<UdfsValue>>
            ClientsService_GetCustomerManagementFormCustomerInfoData =
                CompiledQuery.Compile(
                    (DataContext dataContext, long RefId, long RefTypeId, string refType) =>
                    from objUdfValue in dataContext.UdfsValues
                    from objUdf in dataContext.Udfs
                    where objUdfValue.RefId == RefId &&
                          objUdfValue.UdfId == objUdf.Id &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d") &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d")
                          && objUdf.RefType_Str == refType
                          && (objUdf.RefTypeId == RefTypeId || objUdf.RefTypeId == null)
                    select objUdfValue);

        public static Func<DataContext, string, IQueryable<UdfsValue>>
            ClientsService_GetCustomerManagementFormCustomerInfoData_2 =
                CompiledQuery.Compile(
                    (DataContext dataContext, string refType) =>
                    from objUdfValue in dataContext.UdfsValues
                    from objUdf in dataContext.Udfs
                    where objUdfValue.UdfId == objUdf.Id &&
                          objUdf.AccessType_Str != UDFAccessTypeEnum.Internal.ToString("d") &&
                          objUdf.Status_Str == ActivationStatusEnum.Active.ToString("d")
                          && objUdf.RefType_Str == refType
                    select objUdfValue);

        public static Func<DataContext, long, IQueryable<UdfsValue>>
            ClientsService_GetUdfsValues =
                CompiledQuery.Compile(
                    (DataContext dataContext, long UdfId) =>
                    dataContext.UdfsValues.Where(a => a.UdfId == UdfId));

        public static Func<DataContext, long, long, IQueryable<UdfsValue>>
            Udf_GetUdfValue =
                CompiledQuery.Compile(
                    (DataContext dataContext, long RefId, long UdfId) =>
                    dataContext.UdfsValues.Where(a => a.RefId == RefId && a.UdfId == UdfId));

        public static Func<DataContext, long, IQueryable<UdfsValue>>
            Udf_GetUdfValuesInList =
                CompiledQuery.Compile(
                    (DataContext dataContext, long UdfId) =>
                    (from uvs in dataContext.UdfsValues
                     where uvs.UdfId == UdfId &&
                           (
                               from udflvs in dataContext.UdfsListValues
                               where udflvs.UdfId == UdfId
                               select udflvs.Value
                           ).Contains(uvs.Value)
                     select uvs));

        #endregion
    }
}