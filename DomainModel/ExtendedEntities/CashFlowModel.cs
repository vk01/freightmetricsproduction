﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class CashFlowModel : DomainObject
    {
        [DataMember]
        public List<CashFlowGroup> Groups { get; set; }
    }
}
