﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class CashFlowGroup : DomainObject
    {
        [DataMember]
        public List<CashFlowItem> Items { get; set; }
    }
}
