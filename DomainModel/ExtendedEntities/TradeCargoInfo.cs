﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class TradeCargoInfo
    {
        [DataMember]
        public List<TradeCargoInfoLeg> Legs { get; set; }

        [DataMember]
        public string VesselName { get; set; }
    }
}
