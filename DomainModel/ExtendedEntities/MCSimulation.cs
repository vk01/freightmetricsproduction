﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class MCSimulation : DomainObject
    {
        [DataMember]
        public MCSimulationInfo Info { get; set; }

        [DataMember]
        public List<MCMonthFrequency> MonthFrequency { get; set; }

        [DataMember]
        public List<MCCashBounds> Cashbound { get; set; }

        [DataMember]
        public List<MCAllValues> AllValues { get; set; } 
       
    }
}