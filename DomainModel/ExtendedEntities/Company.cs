﻿using System;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class Company : DomainObject
    {
        [DataMember]
        public string ManagerName { get; set; }

        [DataMember]
        public string SubtypeName { get; set; }

        [DataMember]
        public CompanySubtype Subtype { get; set; }

        [DataMember]
        public bool IsCounterParty { get; set; }
    }
}