﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Exis.Domain;

namespace Exis.Domain
{
    public partial class HierarchyNode
    {
        [DataMember]
        public HierarchyNodeType NodeType { get; set; }
    }
}
