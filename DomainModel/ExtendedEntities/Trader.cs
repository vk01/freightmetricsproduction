﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class Trader : DomainObject
    {
        [DataMember]
        public List<Company> Companies { get; set; }

        [DataMember]
        public List<Market> Markets { get; set; }

        [DataMember]
        public List<CustomBook> Books { get; set; }

        public string MarketsString
        {
            get { return (Markets.Count == 0) ? null : Markets.Select(a => a.Name).Aggregate((i, j) => i + "," + j); }
        }

        public string CompaniesString
        {
            get { return (Companies.Count == 0) ? null : Companies.Select(a => a.Name).Aggregate((i, j) => i + "," + j); }
        }

        public string TypesString
        {
            get { return (String.IsNullOrEmpty(TradeTypes) ? null : TradeTypesList.Select(a => a.ToString("g")).Aggregate((i, j) => i + "," + j)); }
        }

        public List<TradeTypeEnum> TradeTypesList
        {
            get { return TradeTypes.Split(',').ToList().Select(a => ((TradeTypeEnum) Enum.Parse(typeof (TradeTypeEnum), a))).ToList(); }
        }
            
        [DataMember]
        public string ProfitCentreName { get; set; }

        [DataMember]
        public string DeskName { get; set; }
    }
}