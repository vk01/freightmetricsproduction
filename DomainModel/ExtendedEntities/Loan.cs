﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class Loan : DomainObject
    {
        [DataMember]
        public List<LoanBank> LoanBanks { get; set; }

        [DataMember]
        public List<LoanCollateralAsset> LoanCollateralAssets { get; set; }

        [DataMember]
        public List<LoanTranche> LoanTranches { get; set; }

        [DataMember]
        public List<LoanBorrower> LoanBorrowers { get; set; }
        
    }
}
