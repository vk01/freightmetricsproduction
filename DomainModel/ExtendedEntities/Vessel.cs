﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class Vessel : DomainObject
    {
        public Market Market { get; set; }

        [DataMember]
        public string MarketName { get; set; }

        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public VesselInfo Info { get; set; }

        [DataMember]
        public List<VesselExpense> VesselExpenses { get; set; }

        [DataMember]
        public List<VesselBenchmarking> VesselBenchmarkings { get; set; }

        [DataMember]
        public VesselBenchmarking CurrentVesselBenchMarking { get; set; }
    }
}
