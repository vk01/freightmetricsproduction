﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Exis.Domain
{
    public partial class Book : DomainObject
    {
        [DataMember]
        public CustomBook ParentBook { get; set; }

        [DataMember]
        public List<CustomBook> ChildrenBooks { get; set; }
    }
}