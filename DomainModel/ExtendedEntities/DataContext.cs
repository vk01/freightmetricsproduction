﻿using System;
using System.Data.Linq.Mapping;
using System.IO;
using XmlMappingSource = Devart.Data.Linq.Mapping.XmlMappingSource;

namespace Exis.Domain
{
    public partial class DataContext
    {
        private static readonly MappingSource mappingSource =
            XmlMappingSource.FromStream(new FileStream("FreightMetricsDC.xml", FileMode.Open, FileAccess.Read,
                                                       FileShare.Read));

        public bool IsDisposed;

        private static MappingSource GetMappingSource()
        {
            return mappingSource;
        }

        public long GetNextId(Type type)
        {
            return Convert.ToInt64(GetId(Mapping.GetTable(type).TableName));
        }
    }
}