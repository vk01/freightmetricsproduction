﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Exis.Domain
{
    public partial class VesselPool : DomainObject
    {
        [DataMember]
        public List<Vessel> CurrentVessels { get; set; }

        public string CurrentVesselsString
        {
            get { return (CurrentVessels.Count == 0) ? null : CurrentVessels.Select(a => a.Name).Aggregate((i, j) => i + "," + j); }
        }
    }
}
