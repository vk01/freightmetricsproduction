﻿using System;

namespace Exis.Domain
{
    public class PropertyNotFoundException : Exception
    {
        public PropertyNotFoundException(string propertyName, object executingObject)
            : base("Property " + propertyName + " was not found on object " + executingObject + ".")
        {
        }
    }
}