﻿using System;

namespace Exis.Domain
{
    public class ConfigurationDataNotFoundException : Exception
    {
        public ConfigurationDataNotFoundException(string message) : base(message)
        {
        }

        public ConfigurationDataNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}