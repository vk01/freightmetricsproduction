﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exis.Domain
{
    public class UdfNotFoundException : Exception
    {
        public UdfNotFoundException(long udfId)
            : base("UDF with Id " + udfId + " was not found.")
        {
        }
    }
}