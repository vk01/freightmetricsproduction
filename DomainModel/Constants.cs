using System;

namespace Exis.Domain
{
	public class Constants
	{
		public const string EmailRegexpString = @"^([a-zA-Z0-9_\-])+(\.([a-zA-Z0-9_\-])+)*@((\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\]))|((([a-zA-Z0-9])+(([\-])+([a-zA-Z0-9])+)*\.)+([a-zA-Z])+(([\-])+([a-zA-Z0-9])+)*))$";

        public const string CaseTypesGroupAccessPermission = "case_types_group|object|access";
        public const string CaseTypePermission = "{0}|case_type|access";

        public const string CaseTypePermissionAllTasks = "{0}|case_type|all_tasks_form";
        public const string CaseTypePermissionCaseEvents = "{0}|case_type|cases_events_page";
        public const string CaseTypePermissionCustomerManagement = "{0}|case_type|customer_management_form";
        public const string CaseTypePermissionAddCase = "{0}|case_type|add_case";
        public const string CaseTypePermissionSupervisor = "{0}|case_type|supervisor_form";
        public const string CaseTypePermissionMassiveEvent = "{0}|case_type|massive_event_closure_form";
        public const string CaseTypePermissionMasiveCase = "{0}|case_type|massive_case_opening_form";
        public const string CaseTypePermissionUserInAppointment = "{0}|case_type|assign_appointment_user";

        public const string EventTypesGroupAccessPermission = "event_types|group_object|access";
        public const string EventTypePermission = "{0}|event_type|access";

        public const string EventTypePermissionAllTasks = "{0}|event_type|all_tasks_form";
        public const string EventTypePermissionCaseEvents = "{0}|event_type|cases_events_page";
        public const string EventTypePermissionCustomerManagement = "{0}|event_type|customer_management_form";
        public const string EventTypePermissionEditEvent = "{0}|event_type|edit_event";
        public const string EventTypePermissionViewEvent = "{0}|event_type|view_event";
        public const string EventTypePermissionSupervisor = "{0}|event_type|supervisor_form";
        public const string EventTypePermissionMassiveEvent = "{0}|event_type|massive_event_closure_form";
        public const string EventTypePermissionMasiveCase = "{0}|event_type|massive_case_opening_form";
        public const string EventTypePermissionAddEvent = "{0}|event_type|add_event_customer";
        public const string EventTypePermissionUserInAppointment = "{0}|event_type|assign_appointment_user";

        public const string CustomerCategoriesGroupAccessPermission = "customer_categories_group|object|access";
        public const string CustomerCategoryTypesGroupAccessPermission = "customer_category_types_group|object|access";
        public const string CustomerCategoryAccessPermission = "{0}|cust_categ|access";
        
        public const string CustomerCategoryAccessPermissionAllTasks = "{0}|cust_categ_all_tasks_form";
        public const string CustomerCategoryAccessPermissionSearch = "{0}|cust_categ|search_results";
        public const string CustomerCategoryAccessPermissionOpenCust = "{0}|cust_categ|open_customer";
        public const string CustomerCategoryAccessPermissionEditCust = "{0}|cust_categ|edit_customer";
        public const string CustomerCategoryAccessPermissionSupervisor = "{0}|cust_categ|supervisor_form";
        public const string CustomerCategoryAccessPermissionMassiveEvent = "{0}|cust_categ|massive_event_closure_form";
        public const string CustomerCategoryAccessPermissionMasiveCase = "{0}|cust_categ|massive_case_opening_form";
        public const string CustomerCategoryAccessPermissionUserInAppointment = "{0}|cust_categ|assign_appointment_user";

        public const string SalesPersonTypeAccessPermission = "IsSalesPerson|object|access";
        public const string DefaultReportFormName = "CRMAdminClient.DefaultReportForm,CRMAdminClient|CRMClient.DefaultReportForm,CRMClient|CRMWebClient.DefaultReportForm,CRMWebClient|";

        public const string CustomerCategoryTypeAccessPermission = "{0}|cust_categ_type|access";
        public const string CustomerCategoryTypeSearchCustomerPreviewPermission = "{0}|cust_categ_type|preview_search_customer_form";
        public const string CustomerCategoryTypeSearchCustomerColumnPermission = "{0}|cust_categ_type|column_search_customer_form";
        public const string CustomerCategoryTypeSupervisorPreviewPermission = "{0}|cust_categ_type|preview_supervisor_form";
        public const string CustomerCategoryTypeSupervisorColumnPermission = "{0}|cust_categ_type|column_supervisor_form";
        public const string CustomerCategoryTypeAllTasksPreviewPermission = "{0}|cust_categ_type|preview_all_tasks_form";
        public const string CustomerCategoryTypeAllTasksColumnPermission = "{0}|cust_categ_type|column_all_tasks_form";
        public const string CustomerCategoryTypeMassiveEventClosurePreviewPermission = "{0}|cust_categ_type|preview_massive_event_closure_form";
        public const string CustomerCategoryTypeMassiveEventClosureColumnPermission = "{0}|cust_categ_type|column_massive_event_closure_form";

        public const string CustomerFieldAccessPermission = "{0}|cust_field|access";
        public const string CustomerFieldCustomerManagementPermission = "{0}|cust_field|customer_management_form";
        public const string CustomerFieldSearchCustomerPreviewPermission = "{0}|cust_field|preview_search_customer_form";
        public const string CustomerFieldSearchCustomerColumnPermission = "{0}|cust_field|column_search_customer_form";
        public const string CustomerFieldSupervisorPreviewPermission = "{0}|cust_field|preview_supervisor_form";
        public const string CustomerFieldSupervisorColumnPermission = "{0}|cust_field|column_supervisor_form";
        public const string CustomerFieldAllTasksPreviewPermission = "{0}|cust_field|preview_all_tasks_form";
        public const string CustomerFieldAllTasksColumnPermission = "{0}|cust_field|column_all_tasks_form";
        public const string CustomerFieldMassiveEventClosurePreviewPermission = "{0}|cust_field|preview_massive_event_closure_form";
        public const string CustomerFieldMassiveEventClosureColumnPermission = "{0}|cust_field|column_massive_event_closure_form";

        public const string AddressTypeAccessPermission = "{0}|address_type|access";
        public const string AddressTypeSearchCustomerPreviewPermission = "{0}|address_type|preview_search_customer_form";
        public const string AddressTypeSearchCustomerColumnPermission = "{0}|address_type|column_search_customer_form";
        public const string AddressTypeSupervisorPreviewPermission = "{0}|address_type|preview_supervisor_form";
        public const string AddressTypeSupervisorColumnPermission = "{0}|address_type|column_supervisor_form";
        public const string AddressTypeAllTasksPreviewPermission = "{0}|address_type|preview_all_tasks_form";
        public const string AddressTypeAllTasksColumnPermission = "{0}|address_type|column_all_tasks_form";
        public const string AddressTypeMassiveEventClosurePreviewPermission = "{0}|address_type|preview_massive_event_closure_form";
        public const string AddressTypeMassiveEventClosureColumnPermission = "{0}|address_type|column_massive_event_closure_form";

        public const string ContactTypeAccessPermission = "{0}|contact_type|access";
        public const string ContactTypeSearchCustomerPreviewPermission = "{0}|contact_type|preview_search_customer_form";
        public const string ContactTypeSearchCustomerColumnPermission = "{0}|contact_type|column_search_customer_form";
        public const string ContactTypeSupervisorPreviewPermission = "{0}|contact_type|preview_supervisor_form";
        public const string ContactTypeSupervisorColumnPermission = "{0}|contact_type|column_supervisor_form";
        public const string ContactTypeAllTasksPreviewPermission = "{0}|contact_type|preview_all_tasks_form";
        public const string ContactTypeAllTasksColumnPermission = "{0}|contact_type|column_all_tasks_form";
        public const string ContactTypeMassiveEventClosurePreviewPermission = "{0}|contact_type|preview_massive_event_closure_form";
        public const string ContactTypeMassiveEventClosureColumnPermission = "{0}|contact_type|column_massive_event_closure_form";

        public const string ContractFieldAccessPermission = "{0}|contr_field|access";

        public const string CaseTypeFieldsGroupPermission = "{0}|case_fields_group|access";
        public const string CaseTypeFieldAccessPermission = "{0}|case_field|access";
        public const string CaseFieldViewInCaseListAsColumnPermission = "{0}|case_field|column_customer_management_form";

        public const string AppointmentViewPermission = "{0}|appointment|view";
        public const string AppointmentAddPermission = "{0}|appointment|add";
        public const string AppointmentEditPermission = "{0}|appointment|edit";

        #region CTI Reports
        public const string INTERVAL_HOURQUARTER = "Hour-Quarter";
        public const string INTERVAL_HOURQUARTER_ID = "HQ";
        public const string INTERVAL_HOUR = "Hour";
        public const string INTERVAL_HOUR_ID = "HH";
        public const string INTERVAL_DAY = "Day";
        public const string INTERVAL_DAY_ID = "DD";
        public const string INTERVAL_WEEK = "Week";
        public const string INTERVAL_WEEK_ID = "WW";
        public const string INTERVAL_MONTH = "Month";
        public const string INTERVAL_MONTH_ID = "MM";
        #endregion
    }
}
