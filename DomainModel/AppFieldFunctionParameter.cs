﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Exis.Domain;
using Exis.RuleEngine.Attributes;

namespace Exis.RuleEngine
{
    public class AppFieldFunctionParameter
    {
        private readonly string m_ParameterString;
        private object m_RuntimeObject;
        private AppFieldParameterTypeEnum m_ParameterType;
        private string m_AppField;
        private MethodInfo m_Function;
        private List<AppFieldFunctionParameter> m_FunctionParameters;
        private string m_LiteralValue;
        private Type m_Type;
        private object m_Value;
        private RuleParameterAttribute m_Attribute;
        private readonly AppFieldFunctionParameter m_ParentParameter;
        private string m_Name;
        private string m_TypeDefiningParameter;
        private bool m_IsFeeded;

        public string ParameterString
        {
            get { return m_ParameterString; }
        }

        public object RuntimeObject
        {
            get { return m_RuntimeObject; }
            internal set { m_RuntimeObject = value; }
        }

        public AppFieldFunctionParameter ParentParameter
        {
            get { return m_ParentParameter; }
        }

        public Type Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        public MethodInfo Function
        {
            get { return m_Function; }
            set { m_Function = value; }
        }

        public List<AppFieldFunctionParameter> FunctionParameters
        {
            get { return m_FunctionParameters; }
            set { m_FunctionParameters = value; }
        }

        public string AppField
        {
            get { return m_AppField; }
            set { m_AppField = value; }
        }

        public string LiteralValue
        {
            get { return m_LiteralValue; }
            set { m_LiteralValue = value; }
        }

        public AppFieldParameterTypeEnum ParameterType
        {
            get { return m_ParameterType; }
            set { m_ParameterType = value; }
        }

        public object Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        public bool IsFeeded
        {
            get { return m_IsFeeded; }
            set { m_IsFeeded = value; }
        }

        internal RuleParameterAttribute Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }

        internal string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string TypeDefiningParameter
        {
            get { return m_TypeDefiningParameter; }
            set { m_TypeDefiningParameter = value; }
        }

        public AppFieldFunctionParameter(AppFieldFunctionParameter parentParameter, string parameterString)
        {
            m_ParentParameter = parentParameter;
            m_ParameterString = parameterString;
        }

        public Type GetEvaluatedType()
        {
            if(!String.IsNullOrEmpty(m_AppField))
            {
                return AppFieldEvaluator.GetAppFieldType(m_AppField, m_RuntimeObject);
            }
            if(m_Function != null)
            {
                return m_Function.ReturnType;
            }
            return m_Type;
        }

        public override string ToString()
        {
            if (m_ParameterType == AppFieldParameterTypeEnum.ApplicationField)
            {
                return m_AppField;
            }
            if (m_ParameterType == AppFieldParameterTypeEnum.Function)
            {
                return m_Function.Name;
            }
            if (m_ParameterType == AppFieldParameterTypeEnum.LiteralValue)
            {
                return m_LiteralValue;
            }
            return null;
        }
    }
}
